.class public Lcom/squareup/padlock/Padlock;
.super Landroid/view/ViewGroup;
.source "Padlock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/padlock/Padlock$ClearButtonInfo;,
        Lcom/squareup/padlock/Padlock$CheckOrSkipButton;,
        Lcom/squareup/padlock/Padlock$ClearOrCancelButton;,
        Lcom/squareup/padlock/Padlock$ButtonInfo;,
        Lcom/squareup/padlock/Padlock$ContentType;,
        Lcom/squareup/padlock/Padlock$Line;,
        Lcom/squareup/padlock/Padlock$CanvasLine;,
        Lcom/squareup/padlock/Padlock$DeleteType;,
        Lcom/squareup/padlock/Padlock$SubmitType;,
        Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;,
        Lcom/squareup/padlock/Padlock$PinPadRightButtonState;,
        Lcom/squareup/padlock/Padlock$Key;,
        Lcom/squareup/padlock/Padlock$OnKeyPressListenerAdapter;,
        Lcom/squareup/padlock/Padlock$OnKeyPressListener;,
        Lcom/squareup/padlock/Padlock$UpdatableEnabledState;
    }
.end annotation


# static fields
.field private static final DELETE_TYPE_BACKSPACE_GLYPH:I = 0x2

.field private static final DELETE_TYPE_CLEAR_GLYPH:I = 0x0

.field private static final DELETE_TYPE_CLEAR_OR_CANCEL:I = -0x1

.field private static final DELETE_TYPE_CLEAR_TEXT:I = 0x1

.field private static final KEY_REPEAT_TIMEOUT:I = 0x64

.field private static final LEFT_BACKSPACE_SCALING_FACTOR:F = 0.75f

.field private static final LONGPRESS_TIMEOUT:I

.field private static final RESOURCE_NOT_SET:I = -0x1

.field private static final SEPARATOR:Ljava/lang/String; = "\u2009"

.field private static final SUBMIT_ADD:I = 0x1

.field private static final SUBMIT_CHECK:I = 0x2

.field private static final SUBMIT_GONE:I = 0x0

.field private static final SUBMIT_INVISIBLE:I = 0x3

.field private static final TAP_TIMEOUT:I


# instance fields
.field private final accessibilityDelegate:Lcom/squareup/padlock/PadlockAccessibilityDelegate;

.field protected final backspaceDisabledPaint:Landroid/text/TextPaint;

.field private backspaceHandler:Landroid/os/Handler;

.field protected final backspacePaint:Landroid/text/TextPaint;

.field private backspaceRunnable:Ljava/lang/Runnable;

.field protected backspaceSelector:Landroid/graphics/drawable/StateListDrawable;

.field protected final checkPaint:Landroid/text/TextPaint;

.field protected final clearFallbackGlyphDisabledPaint:Landroid/text/TextPaint;

.field protected final clearFallbackGlyphPaint:Landroid/text/TextPaint;

.field protected final clearGlyphDisabledPaint:Landroid/text/TextPaint;

.field protected final clearGlyphPaint:Landroid/text/TextPaint;

.field protected final clearTextDisabledPaint:Landroid/text/TextPaint;

.field protected final clearTextPaint:Landroid/text/TextPaint;

.field private final deleteType:I

.field protected final digitDisabledPaint:Landroid/text/TextPaint;

.field protected final digitPaint:Landroid/text/TextPaint;

.field protected digitSelector:Landroid/graphics/drawable/StateListDrawable;

.field private final drawDividerLines:Z

.field private final drawLeftLine:Z

.field private final drawRightLine:Z

.field private final drawTopLine:Z

.field private final horizontalDividerStyle:Z

.field private keyHeight:F

.field private keyWidth:F

.field private final keys:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/padlock/Padlock$Key;",
            "Lcom/squareup/padlock/Padlock$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field private lastHoveredButton:Lcom/squareup/padlock/Padlock$ButtonInfo;

.field protected final lettersDisabledPaint:Landroid/text/TextPaint;

.field protected final lettersPaint:Landroid/text/TextPaint;

.field private final lettersTypefaceSize:I

.field protected final linePaint:Landroid/graphics/Paint;

.field protected final lineSpacingExtra:F

.field private final lineWidth:I

.field private final lines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/padlock/Padlock$CanvasLine;",
            ">;"
        }
    .end annotation
.end field

.field private longPressHandler:Landroid/os/Handler;

.field protected onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

.field private final pinMode:Z

.field protected pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

.field protected pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

.field private pressedButtons:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/squareup/padlock/Padlock$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field private res:Lcom/squareup/util/Res;

.field private final showDecimal:Z

.field protected final submitDisabledPaint:Landroid/text/TextPaint;

.field protected final submitPaint:Landroid/text/TextPaint;

.field protected submitSelector:Landroid/graphics/drawable/StateListDrawable;

.field private final submitType:I

.field private final tabletMode:Z

.field private final typefaceSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 224
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, Lcom/squareup/padlock/Padlock;->LONGPRESS_TIMEOUT:I

    .line 226
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Lcom/squareup/padlock/Padlock;->TAP_TIMEOUT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 10

    .line 323
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    .line 324
    invoke-virtual {p0, v0}, Lcom/squareup/padlock/Padlock;->setWillNotDraw(Z)V

    .line 327
    new-instance v1, Lcom/squareup/util/Res$RealRes;

    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/util/Res$RealRes;-><init>(Landroid/content/res/Resources;)V

    iput-object v1, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    .line 329
    sget-object v1, Lcom/squareup/padlock/R$styleable;->Padlock:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 332
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_tabletMode:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/padlock/Padlock;->tabletMode:Z

    .line 333
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_pinMode:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    .line 334
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_drawLeftLine:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/padlock/Padlock;->drawLeftLine:Z

    .line 335
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_drawTopLine:I

    const/4 v1, 0x1

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/padlock/Padlock;->drawTopLine:Z

    .line 336
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_drawRightLine:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/padlock/Padlock;->drawRightLine:Z

    .line 337
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_drawDividerLines:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/padlock/Padlock;->drawDividerLines:Z

    .line 338
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_horizontalDividerStyle:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/padlock/Padlock;->horizontalDividerStyle:Z

    .line 339
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_showDecimal:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/padlock/Padlock;->showDecimal:Z

    .line 340
    iget-boolean p2, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    const/4 v2, 0x2

    if-eqz p2, :cond_0

    const/4 p2, 0x2

    goto :goto_0

    :cond_0
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_submitType:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    :goto_0
    iput p2, p0, Lcom/squareup/padlock/Padlock;->submitType:I

    .line 341
    iget-boolean p2, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    const/4 v3, -0x1

    if-eqz p2, :cond_1

    const/4 p2, -0x1

    goto :goto_1

    :cond_1
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_deleteType:I

    .line 343
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    :goto_1
    iput p2, p0, Lcom/squareup/padlock/Padlock;->deleteType:I

    .line 345
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_lettersTextSize:I

    invoke-virtual {p1, p2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    if-ne p2, v3, :cond_2

    .line 347
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/padlock/R$dimen;->keypad_letters_text_size:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/padlock/Padlock;->lettersTypefaceSize:I

    goto :goto_2

    .line 349
    :cond_2
    iput p2, p0, Lcom/squareup/padlock/Padlock;->lettersTypefaceSize:I

    .line 352
    :goto_2
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_buttonTextSize:I

    invoke-virtual {p1, p2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    if-ne p2, v3, :cond_4

    .line 354
    iget-boolean p2, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    if-eqz p2, :cond_3

    .line 355
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/padlock/R$dimen;->pinpad_text_size:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/padlock/Padlock;->typefaceSize:I

    goto :goto_3

    .line 357
    :cond_3
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/padlock/R$dimen;->keypad_text_size:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/padlock/Padlock;->typefaceSize:I

    goto :goto_3

    .line 360
    :cond_4
    iput p2, p0, Lcom/squareup/padlock/Padlock;->typefaceSize:I

    .line 363
    :goto_3
    iget-boolean p2, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    if-eqz p2, :cond_5

    .line 364
    sget-object p2, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->SKIP_ENABLED:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    iput-object p2, p0, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    .line 365
    sget-object p2, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CANCEL:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    iput-object p2, p0, Lcom/squareup/padlock/Padlock;->pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    .line 367
    invoke-virtual {p0, v1}, Lcom/squareup/padlock/Padlock;->setFilterTouchesWhenObscured(Z)V

    .line 371
    :cond_5
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_buttonSelector:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    check-cast p2, Landroid/graphics/drawable/StateListDrawable;

    iput-object p2, p0, Lcom/squareup/padlock/Padlock;->digitSelector:Landroid/graphics/drawable/StateListDrawable;

    .line 372
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->digitSelector:Landroid/graphics/drawable/StateListDrawable;

    if-nez p2, :cond_6

    .line 373
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/padlock/R$drawable;->keypad_selector:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    check-cast p2, Landroid/graphics/drawable/StateListDrawable;

    iput-object p2, p0, Lcom/squareup/padlock/Padlock;->digitSelector:Landroid/graphics/drawable/StateListDrawable;

    .line 375
    :cond_6
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_submitSelector:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    check-cast p2, Landroid/graphics/drawable/StateListDrawable;

    iput-object p2, p0, Lcom/squareup/padlock/Padlock;->submitSelector:Landroid/graphics/drawable/StateListDrawable;

    .line 376
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->submitSelector:Landroid/graphics/drawable/StateListDrawable;

    if-nez p2, :cond_8

    .line 377
    iget p2, p0, Lcom/squareup/padlock/Padlock;->submitType:I

    if-ne p2, v1, :cond_7

    sget p2, Lcom/squareup/padlock/R$drawable;->keypad_selector:I

    goto :goto_4

    :cond_7
    sget p2, Lcom/squareup/padlock/R$drawable;->check_selector:I

    .line 379
    :goto_4
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p2}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    check-cast p2, Landroid/graphics/drawable/StateListDrawable;

    iput-object p2, p0, Lcom/squareup/padlock/Padlock;->submitSelector:Landroid/graphics/drawable/StateListDrawable;

    .line 381
    :cond_8
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_backspaceSelector:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    check-cast p2, Landroid/graphics/drawable/StateListDrawable;

    iput-object p2, p0, Lcom/squareup/padlock/Padlock;->backspaceSelector:Landroid/graphics/drawable/StateListDrawable;

    .line 382
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->backspaceSelector:Landroid/graphics/drawable/StateListDrawable;

    if-nez p2, :cond_9

    .line 383
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/padlock/R$drawable;->keypad_selector:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    check-cast p2, Landroid/graphics/drawable/StateListDrawable;

    iput-object p2, p0, Lcom/squareup/padlock/Padlock;->backspaceSelector:Landroid/graphics/drawable/StateListDrawable;

    .line 387
    :cond_9
    sget p2, Lcom/squareup/padlock/R$styleable;->Padlock_lineColor:I

    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/padlock/R$color;->line_color:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    .line 388
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/squareup/padlock/Padlock;->linePaint:Landroid/graphics/Paint;

    .line 389
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->tabletMode:Z

    const/4 v3, 0x0

    if-eqz v0, :cond_a

    .line 391
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->linePaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 392
    iput v2, p0, Lcom/squareup/padlock/Padlock;->lineWidth:I

    goto :goto_5

    .line 395
    :cond_a
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->linePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 396
    iput v1, p0, Lcom/squareup/padlock/Padlock;->lineWidth:I

    .line 398
    :goto_5
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->linePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 401
    new-instance p2, Landroid/text/TextPaint;

    invoke-direct {p2}, Landroid/text/TextPaint;-><init>()V

    const/16 v0, 0x81

    .line 402
    invoke-virtual {p2, v0}, Landroid/text/TextPaint;->setFlags(I)V

    .line 403
    sget-object v0, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p2, v0}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 404
    iget v0, p0, Lcom/squareup/padlock/Padlock;->typefaceSize:I

    int-to-float v0, v0

    invoke-virtual {p2, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 406
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, p2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 407
    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    invoke-static {v4}, Lcom/squareup/padlock/PadlockTypeface;->getGlyphTypeface(Lcom/squareup/util/Res;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 409
    new-instance v4, Landroid/text/TextPaint;

    invoke-direct {v4, p2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 410
    iget-object v5, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    invoke-static {v5}, Lcom/squareup/glyph/GlyphTypeface;->getGlyphFont(Lcom/squareup/util/Res;)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 411
    iget-object v5, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/glyph/R$dimen;->glyph_font_size:I

    .line 412
    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    .line 411
    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 415
    sget v5, Lcom/squareup/padlock/R$styleable;->Padlock_digitColor:I

    iget-object v6, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v7, Lcom/squareup/padlock/R$color;->text_color:I

    invoke-interface {v6, v7}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v6

    invoke-virtual {p1, v5, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    .line 416
    sget v6, Lcom/squareup/padlock/R$styleable;->Padlock_digitDisabledColor:I

    iget-object v7, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v8, Lcom/squareup/padlock/R$color;->text_color_disabled:I

    .line 417
    invoke-interface {v7, v8}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v7

    .line 416
    invoke-virtual {p1, v6, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v6

    .line 418
    new-instance v7, Landroid/text/TextPaint;

    invoke-direct {v7, p2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v7, p0, Lcom/squareup/padlock/Padlock;->digitPaint:Landroid/text/TextPaint;

    .line 419
    iget-object v7, p0, Lcom/squareup/padlock/Padlock;->digitPaint:Landroid/text/TextPaint;

    invoke-virtual {v7, v5}, Landroid/text/TextPaint;->setColor(I)V

    .line 420
    new-instance v7, Landroid/text/TextPaint;

    invoke-direct {v7, p2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v7, p0, Lcom/squareup/padlock/Padlock;->digitDisabledPaint:Landroid/text/TextPaint;

    .line 421
    iget-object v7, p0, Lcom/squareup/padlock/Padlock;->digitDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v7, v6}, Landroid/text/TextPaint;->setColor(I)V

    .line 426
    iget v7, p0, Lcom/squareup/padlock/Padlock;->submitType:I

    if-ne v7, v1, :cond_b

    .line 427
    sget v1, Lcom/squareup/padlock/R$color;->add_color:I

    .line 428
    sget v7, Lcom/squareup/padlock/R$color;->add_color_disabled:I

    goto :goto_6

    .line 430
    :cond_b
    sget v1, Lcom/squareup/padlock/R$color;->check_color:I

    .line 431
    sget v7, Lcom/squareup/padlock/R$color;->check_color_disabled:I

    .line 433
    :goto_6
    sget v8, Lcom/squareup/padlock/R$styleable;->Padlock_submitColor:I

    iget-object v9, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    invoke-interface {v9, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v8, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 434
    sget v8, Lcom/squareup/padlock/R$styleable;->Padlock_submitDisabledColor:I

    iget-object v9, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    .line 435
    invoke-interface {v9, v7}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v7

    .line 434
    invoke-virtual {p1, v8, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v7

    .line 437
    new-instance v8, Landroid/text/TextPaint;

    invoke-direct {v8, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v8, p0, Lcom/squareup/padlock/Padlock;->submitPaint:Landroid/text/TextPaint;

    .line 438
    iget-object v8, p0, Lcom/squareup/padlock/Padlock;->submitPaint:Landroid/text/TextPaint;

    invoke-virtual {v8, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 439
    new-instance v8, Landroid/text/TextPaint;

    invoke-direct {v8, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v8, p0, Lcom/squareup/padlock/Padlock;->submitDisabledPaint:Landroid/text/TextPaint;

    .line 440
    iget-object v8, p0, Lcom/squareup/padlock/Padlock;->submitDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v8, v7}, Landroid/text/TextPaint;->setColor(I)V

    .line 443
    new-instance v7, Landroid/text/TextPaint;

    invoke-direct {v7, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v7, p0, Lcom/squareup/padlock/Padlock;->clearGlyphPaint:Landroid/text/TextPaint;

    .line 444
    iget-object v7, p0, Lcom/squareup/padlock/Padlock;->clearGlyphPaint:Landroid/text/TextPaint;

    invoke-virtual {v7, v5}, Landroid/text/TextPaint;->setColor(I)V

    .line 445
    new-instance v7, Landroid/text/TextPaint;

    invoke-direct {v7, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v7, p0, Lcom/squareup/padlock/Padlock;->clearGlyphDisabledPaint:Landroid/text/TextPaint;

    .line 446
    iget-object v7, p0, Lcom/squareup/padlock/Padlock;->clearGlyphDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v7, v6}, Landroid/text/TextPaint;->setColor(I)V

    .line 449
    sget v6, Lcom/squareup/padlock/R$styleable;->Padlock_clearTextColor:I

    invoke-virtual {p1, v6, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v6

    .line 450
    sget v7, Lcom/squareup/padlock/R$styleable;->Padlock_clearTextDisabledColor:I

    invoke-virtual {p1, v7, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    .line 451
    new-instance v7, Landroid/text/TextPaint;

    invoke-direct {v7, p2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v7, p0, Lcom/squareup/padlock/Padlock;->clearTextPaint:Landroid/text/TextPaint;

    .line 452
    iget-object v7, p0, Lcom/squareup/padlock/Padlock;->clearTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v7, v6}, Landroid/text/TextPaint;->setColor(I)V

    .line 453
    new-instance v7, Landroid/text/TextPaint;

    invoke-direct {v7, p2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v7, p0, Lcom/squareup/padlock/Padlock;->clearTextDisabledPaint:Landroid/text/TextPaint;

    .line 454
    iget-object v7, p0, Lcom/squareup/padlock/Padlock;->clearTextDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v7, v5}, Landroid/text/TextPaint;->setColor(I)V

    .line 456
    new-instance v7, Landroid/text/TextPaint;

    invoke-direct {v7, v4}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v7, p0, Lcom/squareup/padlock/Padlock;->clearFallbackGlyphPaint:Landroid/text/TextPaint;

    .line 457
    iget-object v7, p0, Lcom/squareup/padlock/Padlock;->clearFallbackGlyphPaint:Landroid/text/TextPaint;

    invoke-virtual {v7, v6}, Landroid/text/TextPaint;->setColor(I)V

    .line 458
    new-instance v6, Landroid/text/TextPaint;

    invoke-direct {v6, v4}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v6, p0, Lcom/squareup/padlock/Padlock;->clearFallbackGlyphDisabledPaint:Landroid/text/TextPaint;

    .line 459
    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->clearFallbackGlyphDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setColor(I)V

    .line 462
    iget-boolean v4, p0, Lcom/squareup/padlock/Padlock;->tabletMode:Z

    const/4 v5, 0x0

    if-nez v4, :cond_d

    iget v4, p0, Lcom/squareup/padlock/Padlock;->deleteType:I

    if-ne v4, v2, :cond_c

    goto :goto_7

    .line 479
    :cond_c
    iput-object v5, p0, Lcom/squareup/padlock/Padlock;->backspacePaint:Landroid/text/TextPaint;

    .line 480
    iput-object v5, p0, Lcom/squareup/padlock/Padlock;->backspaceDisabledPaint:Landroid/text/TextPaint;

    goto :goto_8

    .line 463
    :cond_d
    :goto_7
    sget v4, Lcom/squareup/padlock/R$styleable;->Padlock_backspaceColor:I

    iget-object v6, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v7, Lcom/squareup/padlock/R$color;->text_color:I

    .line 464
    invoke-interface {v6, v7}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v6

    invoke-virtual {p1, v4, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    .line 465
    sget v6, Lcom/squareup/padlock/R$styleable;->Padlock_backspaceDisabledColor:I

    iget-object v7, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v8, Lcom/squareup/padlock/R$color;->backspace_disabled:I

    .line 466
    invoke-interface {v7, v8}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v7

    .line 465
    invoke-virtual {p1, v6, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v6

    .line 467
    new-instance v7, Landroid/text/TextPaint;

    invoke-direct {v7, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v7, p0, Lcom/squareup/padlock/Padlock;->backspacePaint:Landroid/text/TextPaint;

    .line 468
    iget-object v7, p0, Lcom/squareup/padlock/Padlock;->backspacePaint:Landroid/text/TextPaint;

    invoke-virtual {v7, v4}, Landroid/text/TextPaint;->setColor(I)V

    .line 469
    new-instance v4, Landroid/text/TextPaint;

    invoke-direct {v4, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v4, p0, Lcom/squareup/padlock/Padlock;->backspaceDisabledPaint:Landroid/text/TextPaint;

    .line 470
    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->backspaceDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v4, v6}, Landroid/text/TextPaint;->setColor(I)V

    .line 472
    iget v4, p0, Lcom/squareup/padlock/Padlock;->deleteType:I

    if-ne v4, v2, :cond_e

    .line 474
    iget v2, p0, Lcom/squareup/padlock/Padlock;->typefaceSize:I

    int-to-float v2, v2

    const/high16 v4, 0x3f400000    # 0.75f

    mul-float v2, v2, v4

    .line 475
    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->backspacePaint:Landroid/text/TextPaint;

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 476
    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->backspaceDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 484
    :cond_e
    :goto_8
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v2, p0, Lcom/squareup/padlock/Padlock;->checkPaint:Landroid/text/TextPaint;

    .line 485
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    if-eqz v0, :cond_f

    sget v0, Lcom/squareup/padlock/R$styleable;->Padlock_pinSubmitColor:I

    iget-object v1, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/padlock/R$color;->pin_submit_color:I

    .line 486
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 488
    :cond_f
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->checkPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 491
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    if-eqz v0, :cond_10

    .line 492
    sget v0, Lcom/squareup/padlock/R$styleable;->Padlock_android_lineSpacingExtra:I

    iget-object v1, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/padlock/R$dimen;->keypad_line_spacing_extra:I

    .line 493
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v1

    .line 492
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/squareup/padlock/Padlock;->lineSpacingExtra:F

    .line 495
    sget v0, Lcom/squareup/padlock/R$styleable;->Padlock_lettersColor:I

    iget-object v1, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/padlock/R$color;->letters_text_color:I

    .line 496
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 497
    sget v1, Lcom/squareup/padlock/R$styleable;->Padlock_lettersDisabledColor:I

    iget-object v2, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/padlock/R$color;->letters_text_color_disabled:I

    .line 498
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v2

    .line 497
    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 500
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, p2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v2, p0, Lcom/squareup/padlock/Padlock;->lettersPaint:Landroid/text/TextPaint;

    .line 501
    iget-object v2, p0, Lcom/squareup/padlock/Padlock;->lettersPaint:Landroid/text/TextPaint;

    iget v3, p0, Lcom/squareup/padlock/Padlock;->lettersTypefaceSize:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 502
    iget-object v2, p0, Lcom/squareup/padlock/Padlock;->lettersPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 503
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, p2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/squareup/padlock/Padlock;->lettersDisabledPaint:Landroid/text/TextPaint;

    .line 504
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->lettersDisabledPaint:Landroid/text/TextPaint;

    iget v0, p0, Lcom/squareup/padlock/Padlock;->lettersTypefaceSize:I

    int-to-float v0, v0

    invoke-virtual {p2, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 505
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->lettersDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {p2, v1}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_9

    .line 507
    :cond_10
    iput v3, p0, Lcom/squareup/padlock/Padlock;->lineSpacingExtra:F

    .line 508
    iput-object v5, p0, Lcom/squareup/padlock/Padlock;->lettersPaint:Landroid/text/TextPaint;

    .line 509
    iput-object v5, p0, Lcom/squareup/padlock/Padlock;->lettersDisabledPaint:Landroid/text/TextPaint;

    .line 513
    :goto_9
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p2, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    .line 514
    new-instance p2, Ljava/util/ArrayList;

    const/16 v0, 0x9

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p2, p0, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    .line 516
    new-instance p2, Lcom/squareup/padlock/PadlockAccessibilityDelegate;

    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p2, p0, v0}, Lcom/squareup/padlock/PadlockAccessibilityDelegate;-><init>(Landroid/view/View;Ljava/util/Map;)V

    iput-object p2, p0, Lcom/squareup/padlock/Padlock;->accessibilityDelegate:Lcom/squareup/padlock/PadlockAccessibilityDelegate;

    .line 517
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->accessibilityDelegate:Lcom/squareup/padlock/PadlockAccessibilityDelegate;

    invoke-static {p0, p2}, Landroidx/core/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroidx/core/view/AccessibilityDelegateCompat;)V

    .line 519
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    invoke-virtual {p0, p2}, Lcom/squareup/padlock/Padlock;->createKeysAndLines(Lcom/squareup/util/Res;)V

    .line 521
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/padlock/Padlock;IFF)V
    .locals 0

    .line 78
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/padlock/Padlock;->handleLongPress(IFF)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/padlock/Padlock;)Ljava/util/Map;
    .locals 0

    .line 78
    iget-object p0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/padlock/Padlock;)Ljava/lang/Runnable;
    .locals 0

    .line 78
    iget-object p0, p0, Lcom/squareup/padlock/Padlock;->backspaceRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/padlock/Padlock;)Landroid/os/Handler;
    .locals 0

    .line 78
    iget-object p0, p0, Lcom/squareup/padlock/Padlock;->backspaceHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/padlock/Padlock;)Z
    .locals 0

    .line 78
    iget-boolean p0, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    return p0
.end method

.method private addClearOrBackspace()V
    .locals 3

    .line 817
    iget v0, p0, Lcom/squareup/padlock/Padlock;->deleteType:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 823
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->BACKSPACE:Lcom/squareup/padlock/Padlock$Key;

    iget-object v2, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    invoke-direct {p0, v2}, Lcom/squareup/padlock/Padlock;->buildBackspaceGlyph(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 836
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown Delete Button Type!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 827
    :cond_1
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    invoke-direct {p0, v0}, Lcom/squareup/padlock/Padlock;->buildClearText(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v0

    .line 828
    iget-object v1, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 832
    :cond_2
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    iget-object v2, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    invoke-direct {p0, v2}, Lcom/squareup/padlock/Padlock;->buildClearGlyph(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 819
    :cond_3
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    iget-object v2, p0, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    invoke-direct {p0, v2}, Lcom/squareup/padlock/Padlock;->buildClearOrCancel(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method private buildAdd(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 10

    .line 1820
    new-instance v9, Lcom/squareup/padlock/Padlock$7;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->getSubmitSelector()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/padlock/Padlock;->submitPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->submitDisabledPaint:Landroid/text/TextPaint;

    sget-object v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;->PLUS:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    invoke-virtual {v0}, Lcom/squareup/padlock/PadlockTypeface$Glyph;->getLetter()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/squareup/padlock/Padlock$ContentType;->PADLOCK_GLYPH:Lcom/squareup/padlock/Padlock$ContentType;

    sget-object v7, Lcom/squareup/padlock/Padlock$Key;->PLUS:Lcom/squareup/padlock/Padlock$Key;

    move-object v0, v9

    move-object v1, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/padlock/Padlock$7;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    return-object v9
.end method

.method private buildBackspaceGlyph(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 10

    .line 1744
    new-instance v9, Lcom/squareup/padlock/Padlock$6;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->getBackspaceSelector()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/padlock/Padlock;->backspacePaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->backspaceDisabledPaint:Landroid/text/TextPaint;

    sget-object v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;->ROUNDED_BACKSPACE:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    .line 1745
    invoke-virtual {v0}, Lcom/squareup/padlock/PadlockTypeface$Glyph;->getLetter()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/squareup/padlock/Padlock$ContentType;->PADLOCK_GLYPH:Lcom/squareup/padlock/Padlock$ContentType;

    sget-object v7, Lcom/squareup/padlock/Padlock$Key;->BACKSPACE:Lcom/squareup/padlock/Padlock$Key;

    move-object v0, v9

    move-object v1, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/padlock/Padlock$6;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    return-object v9
.end method

.method private buildCheckOrSkip(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 10

    .line 1815
    new-instance v9, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->getSubmitSelector()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/padlock/Padlock;->digitPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->digitDisabledPaint:Landroid/text/TextPaint;

    iget-object v5, p0, Lcom/squareup/padlock/Padlock;->checkPaint:Landroid/text/TextPaint;

    sget-object v6, Lcom/squareup/padlock/Padlock$ContentType;->PADLOCK_GLYPH:Lcom/squareup/padlock/Padlock$ContentType;

    sget-object v7, Lcom/squareup/padlock/Padlock$Key;->CHECK_OR_SKIP:Lcom/squareup/padlock/Padlock$Key;

    move-object v0, v9

    move-object v1, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Landroid/text/TextPaint;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    return-object v9
.end method

.method private buildClearFallbackGlyph(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 10

    .line 1804
    new-instance v9, Lcom/squareup/padlock/Padlock$ClearButtonInfo;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->getDigitSelector()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/padlock/Padlock;->clearFallbackGlyphPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->clearFallbackGlyphDisabledPaint:Landroid/text/TextPaint;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACKSPACE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 1805
    invoke-virtual {v0}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->getCharacterAsString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/squareup/padlock/Padlock$ContentType;->MARIN_GLYPH:Lcom/squareup/padlock/Padlock$ContentType;

    sget-object v7, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    move-object v0, v9

    move-object v1, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/padlock/Padlock$ClearButtonInfo;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    return-object v9
.end method

.method private buildClearGlyph(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 10

    .line 1794
    new-instance v9, Lcom/squareup/padlock/Padlock$ClearButtonInfo;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->getDigitSelector()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/padlock/Padlock;->clearGlyphPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->clearGlyphDisabledPaint:Landroid/text/TextPaint;

    sget-object v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;->C:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    .line 1795
    invoke-virtual {v0}, Lcom/squareup/padlock/PadlockTypeface$Glyph;->getLetter()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/squareup/padlock/Padlock$ContentType;->PADLOCK_GLYPH:Lcom/squareup/padlock/Padlock$ContentType;

    sget-object v7, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    move-object v0, v9

    move-object v1, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/padlock/Padlock$ClearButtonInfo;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    return-object v9
.end method

.method private buildClearOrCancel(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 9

    .line 1810
    new-instance v8, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->getDigitSelector()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/padlock/Padlock;->digitPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->digitDisabledPaint:Landroid/text/TextPaint;

    sget-object v5, Lcom/squareup/padlock/Padlock$ContentType;->PADLOCK_GLYPH:Lcom/squareup/padlock/Padlock$ContentType;

    sget-object v6, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    move-object v0, v8

    move-object v1, p0

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    return-object v8
.end method

.method private buildClearText(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 10

    .line 1799
    new-instance v9, Lcom/squareup/padlock/Padlock$ClearButtonInfo;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->getDigitSelector()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/padlock/Padlock;->clearTextPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->clearTextDisabledPaint:Landroid/text/TextPaint;

    .line 1800
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/padlock/R$string;->text_clear:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/squareup/padlock/Padlock$ContentType;->TEXT:Lcom/squareup/padlock/Padlock$ContentType;

    sget-object v7, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    move-object v0, v9

    move-object v1, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/padlock/Padlock$ClearButtonInfo;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    return-object v9
.end method

.method private buildDecimal(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 10

    .line 1733
    new-instance v9, Lcom/squareup/padlock/Padlock$5;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->getDigitSelector()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/padlock/Padlock;->digitPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->digitDisabledPaint:Landroid/text/TextPaint;

    sget-object v6, Lcom/squareup/padlock/Padlock$ContentType;->TEXT:Lcom/squareup/padlock/Padlock$ContentType;

    sget-object v7, Lcom/squareup/padlock/Padlock$Key;->DECIMAL:Lcom/squareup/padlock/Padlock$Key;

    const-string v5, "."

    move-object v0, v9

    move-object v1, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/padlock/Padlock$5;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    return-object v9
.end method

.method private buildDigit(ILcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 1

    const/4 v0, 0x0

    .line 1713
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/squareup/padlock/Padlock;->buildDigit(ILjava/lang/String;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object p1

    return-object p1
.end method

.method private buildDigit(ILjava/lang/String;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 15

    move-object v13, p0

    .line 1717
    new-instance v14, Lcom/squareup/padlock/Padlock$4;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->getDigitSelector()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    iget-object v3, v13, Lcom/squareup/padlock/Padlock;->digitPaint:Landroid/text/TextPaint;

    iget-object v4, v13, Lcom/squareup/padlock/Padlock;->digitDisabledPaint:Landroid/text/TextPaint;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v13, Lcom/squareup/padlock/Padlock;->lettersPaint:Landroid/text/TextPaint;

    iget-object v7, v13, Lcom/squareup/padlock/Padlock;->lettersDisabledPaint:Landroid/text/TextPaint;

    const-string/jumbo v0, "\u2009"

    move-object/from16 v1, p2

    .line 1718
    invoke-direct {p0, v1, v0}, Lcom/squareup/padlock/Padlock;->separatedWith(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/squareup/padlock/Padlock$ContentType;->TEXT:Lcom/squareup/padlock/Padlock$ContentType;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move/from16 v12, p1

    invoke-direct/range {v0 .. v12}, Lcom/squareup/padlock/Padlock$4;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;I)V

    return-object v14
.end method

.method private buildDoubleZero(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 13

    .line 1701
    new-instance v12, Lcom/squareup/padlock/Padlock$3;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->getDigitSelector()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/padlock/Padlock;->digitPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->digitDisabledPaint:Landroid/text/TextPaint;

    iget-object v6, p0, Lcom/squareup/padlock/Padlock;->lettersPaint:Landroid/text/TextPaint;

    iget-object v7, p0, Lcom/squareup/padlock/Padlock;->lettersDisabledPaint:Landroid/text/TextPaint;

    sget-object v9, Lcom/squareup/padlock/Padlock$ContentType;->TEXT:Lcom/squareup/padlock/Padlock$ContentType;

    sget-object v10, Lcom/squareup/padlock/Padlock$Key;->DOUBLE_ZERO:Lcom/squareup/padlock/Padlock$Key;

    const-string v5, "00"

    const/4 v8, 0x0

    move-object v0, v12

    move-object v1, p0

    move-object v11, p1

    invoke-direct/range {v0 .. v11}, Lcom/squareup/padlock/Padlock$3;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    return-object v12
.end method

.method private buildNone(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 10

    .line 1842
    new-instance v9, Lcom/squareup/padlock/Padlock$9;

    sget-object v6, Lcom/squareup/padlock/Padlock$ContentType;->TEXT:Lcom/squareup/padlock/Padlock$ContentType;

    sget-object v7, Lcom/squareup/padlock/Padlock$Key;->UNKNOWN:Lcom/squareup/padlock/Padlock$Key;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v9

    move-object v1, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/padlock/Padlock$9;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    return-object v9
.end method

.method private buildRoundedPlus(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 10

    .line 1831
    new-instance v9, Lcom/squareup/padlock/Padlock$8;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->getSubmitSelector()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/padlock/Padlock;->submitPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->submitDisabledPaint:Landroid/text/TextPaint;

    sget-object v0, Lcom/squareup/padlock/PadlockTypeface$Glyph;->ROUNDED_PLUS:Lcom/squareup/padlock/PadlockTypeface$Glyph;

    .line 1832
    invoke-virtual {v0}, Lcom/squareup/padlock/PadlockTypeface$Glyph;->getLetter()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/squareup/padlock/Padlock$ContentType;->PADLOCK_GLYPH:Lcom/squareup/padlock/Padlock$ContentType;

    sget-object v7, Lcom/squareup/padlock/Padlock$Key;->ROUNDED_PLUS:Lcom/squareup/padlock/Padlock$Key;

    move-object v0, v9

    move-object v1, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/padlock/Padlock$8;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    return-object v9
.end method

.method private buildSubmitKeyType(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 2

    .line 1856
    iget v0, p0, Lcom/squareup/padlock/Padlock;->submitType:I

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1862
    invoke-direct {p0, p1}, Lcom/squareup/padlock/Padlock;->buildCheckOrSkip(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object p1

    return-object p1

    .line 1864
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unknown Submit Button Type!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1860
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/padlock/Padlock;->buildAdd(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object p1

    return-object p1

    .line 1858
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/padlock/Padlock;->buildNone(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object p1

    return-object p1
.end method

.method private cancelOutside(Landroid/view/MotionEvent;)V
    .locals 5

    const/4 v0, 0x0

    .line 1186
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1187
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    .line 1188
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    .line 1189
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    .line 1191
    iget-object v4, p0, Lcom/squareup/padlock/Padlock;->pressedButtons:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/padlock/Padlock$ButtonInfo;

    if-eqz v4, :cond_0

    .line 1192
    invoke-virtual {v4, v2, v3}, Lcom/squareup/padlock/Padlock$ButtonInfo;->contains(FF)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1193
    invoke-virtual {v4}, Lcom/squareup/padlock/Padlock$ButtonInfo;->clearPress()V

    .line 1194
    iget-object v2, p0, Lcom/squareup/padlock/Padlock;->longPressHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1195
    iget-object v2, p0, Lcom/squareup/padlock/Padlock;->backspaceHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/squareup/padlock/Padlock;->backspaceRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1196
    iget-object v2, p0, Lcom/squareup/padlock/Padlock;->pressedButtons:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 1197
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private findButton(FF)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 3

    .line 1248
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/padlock/Padlock$ButtonInfo;

    .line 1249
    invoke-virtual {v1, p1, p2}, Lcom/squareup/padlock/Padlock$ButtonInfo;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private getBackspaceSelector()Landroid/graphics/drawable/StateListDrawable;
    .locals 1

    .line 1877
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->backspaceSelector:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    return-object v0
.end method

.method private getDigitSelector()Landroid/graphics/drawable/StateListDrawable;
    .locals 1

    .line 1869
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->digitSelector:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    return-object v0
.end method

.method private getLeftEdgeLine(FFF)Lcom/squareup/padlock/Padlock$CanvasLine;
    .locals 1

    .line 1285
    new-instance v0, Lcom/squareup/padlock/-$$Lambda$Padlock$VectWg9fBNcXsuxwNyqQkrzRDf0;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/padlock/-$$Lambda$Padlock$VectWg9fBNcXsuxwNyqQkrzRDf0;-><init>(Lcom/squareup/padlock/Padlock;FFF)V

    return-object v0
.end method

.method private getRightEdgeLine(FFF)Lcom/squareup/padlock/Padlock$CanvasLine;
    .locals 1

    .line 1281
    new-instance v0, Lcom/squareup/padlock/-$$Lambda$Padlock$pNwbfn7fVRVT86culPrur5V27HA;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/padlock/-$$Lambda$Padlock$pNwbfn7fVRVT86culPrur5V27HA;-><init>(Lcom/squareup/padlock/Padlock;FFF)V

    return-object v0
.end method

.method private getSubmitSelector()Landroid/graphics/drawable/StateListDrawable;
    .locals 1

    .line 1873
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->submitSelector:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    return-object v0
.end method

.method private getTopEdgeLine(FFF)Lcom/squareup/padlock/Padlock$CanvasLine;
    .locals 1

    .line 1289
    new-instance v0, Lcom/squareup/padlock/-$$Lambda$Padlock$-fhCz1tHiLYJ3pIQwtmz0tQPn20;

    invoke-direct {v0, p0, p1, p3, p2}, Lcom/squareup/padlock/-$$Lambda$Padlock$-fhCz1tHiLYJ3pIQwtmz0tQPn20;-><init>(Lcom/squareup/padlock/Padlock;FFF)V

    return-object v0
.end method

.method private handleLongPress(IFF)V
    .locals 1

    .line 1240
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->pressedButtons:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/padlock/Padlock$ButtonInfo;

    if-eqz p1, :cond_0

    .line 1241
    invoke-virtual {p1, p2, p3}, Lcom/squareup/padlock/Padlock$ButtonInfo;->onLongClick(FF)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 1242
    invoke-virtual {p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->clearPress()V

    .line 1243
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    :cond_0
    return-void
.end method

.method private handleTouch(JIIFF)V
    .locals 3

    if-nez p4, :cond_2

    .line 1204
    invoke-direct {p0, p5, p6}, Lcom/squareup/padlock/Padlock;->findButton(FF)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object p4

    if-eqz p4, :cond_3

    .line 1205
    invoke-direct {p0, p4}, Lcom/squareup/padlock/Padlock;->isPressed(Lcom/squareup/padlock/Padlock$ButtonInfo;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1206
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->pressedButtons:Landroid/util/SparseArray;

    invoke-virtual {v0, p3, p4}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 1207
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->BACKSPACE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1209
    iget-object p1, p0, Lcom/squareup/padlock/Padlock;->backspaceHandler:Landroid/os/Handler;

    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->backspaceRunnable:Ljava/lang/Runnable;

    sget p3, Lcom/squareup/padlock/Padlock;->TAP_TIMEOUT:I

    add-int/lit8 p3, p3, 0x64

    int-to-long v0, p3

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1211
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->tabletMode:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1215
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->longPressHandler:Landroid/os/Handler;

    float-to-int v1, p5

    float-to-int v2, p6

    .line 1216
    invoke-virtual {v0, p3, v1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p3

    sget v1, Lcom/squareup/padlock/Padlock;->TAP_TIMEOUT:I

    int-to-long v1, v1

    add-long/2addr p1, v1

    sget v1, Lcom/squareup/padlock/Padlock;->LONGPRESS_TIMEOUT:I

    int-to-long v1, v1

    add-long/2addr p1, v1

    .line 1215
    invoke-virtual {v0, p3, p1, p2}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    .line 1220
    :cond_1
    :goto_0
    invoke-virtual {p4, p5, p6}, Lcom/squareup/padlock/Padlock$ButtonInfo;->onPressed(FF)Z

    .line 1221
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    goto :goto_1

    :cond_2
    const/4 p1, 0x1

    if-ne p4, p1, :cond_3

    .line 1224
    iget-object p1, p0, Lcom/squareup/padlock/Padlock;->pressedButtons:Landroid/util/SparseArray;

    invoke-virtual {p1, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/padlock/Padlock$ButtonInfo;

    if-eqz p1, :cond_3

    .line 1226
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->longPressHandler:Landroid/os/Handler;

    invoke-virtual {p2, p3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1227
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->backspaceHandler:Landroid/os/Handler;

    iget-object p4, p0, Lcom/squareup/padlock/Padlock;->backspaceRunnable:Ljava/lang/Runnable;

    invoke-virtual {p2, p4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1228
    iget-object p2, p0, Lcom/squareup/padlock/Padlock;->pressedButtons:Landroid/util/SparseArray;

    invoke-virtual {p2, p3}, Landroid/util/SparseArray;->remove(I)V

    .line 1229
    invoke-virtual {p1, p5, p6}, Lcom/squareup/padlock/Padlock$ButtonInfo;->onClick(FF)V

    .line 1230
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    :cond_3
    :goto_1
    return-void
.end method

.method private initializePhoneLayout()V
    .locals 20

    move-object/from16 v7, p0

    .line 1007
    iget v8, v7, Lcom/squareup/padlock/Padlock;->keyWidth:F

    const/high16 v0, 0x40000000    # 2.0f

    mul-float v9, v8, v0

    const/high16 v1, 0x40400000    # 3.0f

    mul-float v10, v8, v1

    .line 1012
    iget v11, v7, Lcom/squareup/padlock/Padlock;->keyHeight:F

    mul-float v12, v11, v0

    mul-float v13, v11, v1

    const/high16 v0, 0x40800000    # 4.0f

    mul-float v14, v11, v0

    .line 1018
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->ONE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, v8, v11}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 1019
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->TWO:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v8, v1, v9, v11}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 1020
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->THREE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v9, v1, v10, v11}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 1023
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->FOUR:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v1, v11, v8, v12}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 1024
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->FIVE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v8, v11, v9, v12}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 1025
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->SIX:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v9, v11, v10, v12}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 1028
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->SEVEN:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v1, v12, v8, v13}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 1029
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->EIGHT:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v8, v12, v9, v13}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 1030
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->NINE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v9, v12, v10, v13}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 1033
    iget v0, v7, Lcom/squareup/padlock/Padlock;->deleteType:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    sget-object v0, Lcom/squareup/padlock/Padlock$Key;->BACKSPACE:Lcom/squareup/padlock/Padlock$Key;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    .line 1034
    :goto_0
    iget-object v2, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/padlock/Padlock$ButtonInfo;

    .line 1035
    invoke-virtual {v2, v1, v13, v8, v14}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 1037
    sget-object v3, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    if-ne v0, v3, :cond_1

    sub-float v3, v8, v1

    .line 1040
    invoke-virtual {v2}, Lcom/squareup/padlock/Padlock$ButtonInfo;->getContentPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {v2}, Lcom/squareup/padlock/Padlock$ButtonInfo;->getContent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 1042
    iget-object v2, v7, Lcom/squareup/padlock/Padlock;->res:Lcom/squareup/util/Res;

    invoke-direct {v7, v2}, Lcom/squareup/padlock/Padlock;->buildClearFallbackGlyph(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v2

    .line 1043
    invoke-virtual {v2, v1, v13, v8, v14}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 1044
    iget-object v3, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1048
    :cond_1
    iget v0, v7, Lcom/squareup/padlock/Padlock;->submitType:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    .line 1049
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->ZERO:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v8, v13, v9, v14}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    goto :goto_1

    :cond_2
    if-nez v0, :cond_4

    .line 1051
    iget-boolean v0, v7, Lcom/squareup/padlock/Padlock;->showDecimal:Z

    if-eqz v0, :cond_3

    .line 1052
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->ZERO:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v8, v13, v9, v14}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 1053
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->DECIMAL:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v9, v13, v10, v14}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    goto :goto_1

    .line 1055
    :cond_3
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->ZERO:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v8, v13, v10, v14}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    goto :goto_1

    .line 1058
    :cond_4
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->ZERO:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v8, v13, v9, v14}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 1059
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->SUBMIT:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v9, v13, v10, v14}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 1063
    :goto_1
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1065
    iget-boolean v0, v7, Lcom/squareup/padlock/Padlock;->horizontalDividerStyle:Z

    if-eqz v0, :cond_5

    .line 1066
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/padlock/Padlock;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/squareup/padlock/R$dimen;->keypad_horizontal_divider_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    add-float v14, v0, v1

    sub-float v15, v8, v0

    add-float/2addr v8, v0

    sub-float v16, v9, v0

    add-float/2addr v9, v0

    sub-float/2addr v10, v0

    .line 1073
    iget-object v6, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v5, Lcom/squareup/padlock/Padlock$Line;

    const/16 v17, 0x0

    move-object v0, v5

    move-object/from16 v1, p0

    move v2, v14

    move v3, v11

    move v4, v15

    move/from16 v18, v10

    move-object v10, v5

    move v5, v11

    move/from16 v19, v9

    move-object v9, v6

    move-object/from16 v6, v17

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1074
    iget-object v9, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v10, Lcom/squareup/padlock/Padlock$Line;

    const/4 v6, 0x0

    move-object v0, v10

    move v3, v12

    move v5, v12

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1075
    iget-object v9, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v10, Lcom/squareup/padlock/Padlock$Line;

    move-object v0, v10

    move v3, v13

    move v5, v13

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1076
    iget-object v9, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v10, Lcom/squareup/padlock/Padlock$Line;

    move-object v0, v10

    move v2, v8

    move v3, v11

    move/from16 v4, v16

    move v5, v11

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1077
    iget-object v9, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v10, Lcom/squareup/padlock/Padlock$Line;

    move-object v0, v10

    move v3, v12

    move v5, v12

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1078
    iget-object v9, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v10, Lcom/squareup/padlock/Padlock$Line;

    move-object v0, v10

    move v3, v13

    move v5, v13

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1079
    iget-object v8, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v9, Lcom/squareup/padlock/Padlock$Line;

    move-object v0, v9

    move/from16 v2, v19

    move v3, v11

    move/from16 v4, v18

    move v5, v11

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1080
    iget-object v8, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v9, Lcom/squareup/padlock/Padlock$Line;

    move-object v0, v9

    move v3, v12

    move v5, v12

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1081
    iget-object v8, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v9, Lcom/squareup/padlock/Padlock$Line;

    move-object v0, v9

    move v3, v13

    move v5, v13

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1084
    :cond_5
    iget-boolean v0, v7, Lcom/squareup/padlock/Padlock;->drawTopLine:Z

    if-eqz v0, :cond_6

    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    invoke-direct {v7, v1, v10, v1}, Lcom/squareup/padlock/Padlock;->getTopEdgeLine(FFF)Lcom/squareup/padlock/Padlock$CanvasLine;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1085
    :cond_6
    iget-boolean v0, v7, Lcom/squareup/padlock/Padlock;->drawLeftLine:Z

    if-eqz v0, :cond_7

    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    invoke-direct {v7, v1, v1, v14}, Lcom/squareup/padlock/Padlock;->getLeftEdgeLine(FFF)Lcom/squareup/padlock/Padlock$CanvasLine;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1086
    :cond_7
    iget-boolean v0, v7, Lcom/squareup/padlock/Padlock;->drawRightLine:Z

    if-eqz v0, :cond_8

    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    invoke-direct {v7, v10, v1, v14}, Lcom/squareup/padlock/Padlock;->getRightEdgeLine(FFF)Lcom/squareup/padlock/Padlock$CanvasLine;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1088
    :cond_8
    iget-boolean v0, v7, Lcom/squareup/padlock/Padlock;->drawDividerLines:Z

    if-eqz v0, :cond_a

    .line 1090
    iget-object v15, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v6, Lcom/squareup/padlock/Padlock$Line;

    const/16 v16, 0x0

    const/4 v2, 0x0

    move-object v0, v6

    move-object/from16 v1, p0

    move v3, v11

    move v4, v10

    move v5, v11

    move-object v11, v6

    move-object/from16 v6, v16

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v15, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1091
    iget-object v11, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v15, Lcom/squareup/padlock/Padlock$Line;

    const/4 v6, 0x0

    move-object v0, v15

    move v3, v12

    move v5, v12

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v11, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1092
    iget-object v11, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v12, Lcom/squareup/padlock/Padlock$Line;

    move-object v0, v12

    move v3, v13

    move v5, v13

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1095
    iget-object v10, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v11, Lcom/squareup/padlock/Padlock$Line;

    const/4 v3, 0x0

    move-object v0, v11

    move v2, v8

    move v4, v8

    move v5, v14

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1096
    iget v0, v7, Lcom/squareup/padlock/Padlock;->submitType:I

    if-nez v0, :cond_9

    iget-boolean v0, v7, Lcom/squareup/padlock/Padlock;->showDecimal:Z

    if-nez v0, :cond_9

    .line 1097
    iget-object v8, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v10, Lcom/squareup/padlock/Padlock$Line;

    const/4 v6, 0x0

    const/4 v3, 0x0

    move-object v0, v10

    move-object/from16 v1, p0

    move v2, v9

    move v4, v9

    move v5, v13

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1099
    :cond_9
    iget-object v8, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v10, Lcom/squareup/padlock/Padlock$Line;

    const/4 v6, 0x0

    const/4 v3, 0x0

    move-object v0, v10

    move-object/from16 v1, p0

    move v2, v9

    move v4, v9

    move v5, v14

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    :goto_2
    return-void
.end method

.method private initializeTabletLayout()V
    .locals 18

    move-object/from16 v7, p0

    .line 874
    iget v8, v7, Lcom/squareup/padlock/Padlock;->keyWidth:F

    const/high16 v0, 0x40000000    # 2.0f

    mul-float v9, v8, v0

    const/high16 v1, 0x40400000    # 3.0f

    mul-float v10, v8, v1

    const/high16 v2, 0x40800000    # 4.0f

    mul-float v11, v8, v2

    .line 880
    iget v5, v7, Lcom/squareup/padlock/Padlock;->keyHeight:F

    mul-float v12, v5, v0

    mul-float v13, v5, v1

    mul-float v14, v5, v2

    .line 886
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->ONE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, v8, v5}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 887
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->TWO:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v8, v1, v9, v5}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 888
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->THREE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v9, v1, v10, v5}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 891
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->FOUR:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v1, v5, v8, v12}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 892
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->FIVE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v8, v5, v9, v12}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 893
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->SIX:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v9, v5, v10, v12}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 896
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->SEVEN:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v1, v12, v8, v13}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 897
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->EIGHT:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v8, v12, v9, v13}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 898
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->NINE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v9, v12, v10, v13}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 900
    iget-boolean v0, v7, Lcom/squareup/padlock/Padlock;->showDecimal:Z

    if-eqz v0, :cond_0

    .line 902
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->DECIMAL:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v1, v13, v8, v14}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 903
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->ZERO:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v8, v13, v10, v14}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    goto :goto_0

    .line 906
    :cond_0
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->DOUBLE_ZERO:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v1, v13, v9, v14}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 907
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->ZERO:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v9, v13, v10, v14}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 911
    :goto_0
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->SUBMIT:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v10, v12, v11, v14}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 912
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->BACKSPACE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, v10, v1, v11, v12}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setLocation(FFFF)V

    .line 915
    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 918
    iget-boolean v0, v7, Lcom/squareup/padlock/Padlock;->drawTopLine:Z

    if-eqz v0, :cond_1

    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    invoke-direct {v7, v1, v11, v1}, Lcom/squareup/padlock/Padlock;->getTopEdgeLine(FFF)Lcom/squareup/padlock/Padlock$CanvasLine;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 919
    :cond_1
    iget-boolean v0, v7, Lcom/squareup/padlock/Padlock;->drawLeftLine:Z

    if-eqz v0, :cond_2

    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    invoke-direct {v7, v1, v1, v14}, Lcom/squareup/padlock/Padlock;->getLeftEdgeLine(FFF)Lcom/squareup/padlock/Padlock$CanvasLine;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 920
    :cond_2
    iget-boolean v0, v7, Lcom/squareup/padlock/Padlock;->drawRightLine:Z

    if-eqz v0, :cond_3

    iget-object v0, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    invoke-direct {v7, v11, v1, v14}, Lcom/squareup/padlock/Padlock;->getRightEdgeLine(FFF)Lcom/squareup/padlock/Padlock$CanvasLine;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 922
    :cond_3
    iget-boolean v0, v7, Lcom/squareup/padlock/Padlock;->drawDividerLines:Z

    if-eqz v0, :cond_5

    .line 924
    iget-object v15, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v6, Lcom/squareup/padlock/Padlock$Line;

    const/16 v16, 0x0

    const/4 v2, 0x0

    move-object v0, v6

    move-object/from16 v1, p0

    move v3, v5

    move v4, v10

    move/from16 v17, v9

    move-object v9, v6

    move-object/from16 v6, v16

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v15, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 925
    iget-object v9, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v15, Lcom/squareup/padlock/Padlock$Line;

    const/4 v6, 0x0

    move-object v0, v15

    move v3, v12

    move v4, v11

    move v5, v12

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v9, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 926
    iget-object v9, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v11, Lcom/squareup/padlock/Padlock$Line;

    move-object v0, v11

    move v3, v13

    move v4, v10

    move v5, v13

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 929
    iget-boolean v0, v7, Lcom/squareup/padlock/Padlock;->showDecimal:Z

    if-eqz v0, :cond_4

    .line 930
    iget-object v9, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v11, Lcom/squareup/padlock/Padlock$Line;

    const/4 v6, 0x0

    const/4 v3, 0x0

    move-object v0, v11

    move-object/from16 v1, p0

    move v2, v8

    move v4, v8

    move v5, v14

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 931
    iget-object v8, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v9, Lcom/squareup/padlock/Padlock$Line;

    move-object v0, v9

    move/from16 v2, v17

    move/from16 v4, v17

    move v5, v13

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 933
    :cond_4
    iget-object v9, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v11, Lcom/squareup/padlock/Padlock$Line;

    const/4 v6, 0x0

    const/4 v3, 0x0

    move-object v0, v11

    move-object/from16 v1, p0

    move v2, v8

    move v4, v8

    move v5, v13

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 934
    iget-object v8, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v9, Lcom/squareup/padlock/Padlock$Line;

    move-object v0, v9

    move/from16 v2, v17

    move/from16 v4, v17

    move v5, v14

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 937
    :goto_1
    iget-object v8, v7, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    new-instance v9, Lcom/squareup/padlock/Padlock$Line;

    const/4 v6, 0x0

    const/4 v3, 0x0

    move-object v0, v9

    move-object/from16 v1, p0

    move v2, v10

    move v4, v10

    move v5, v14

    invoke-direct/range {v0 .. v6}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    return-void
.end method

.method private isPressed(Lcom/squareup/padlock/Padlock$ButtonInfo;)Z
    .locals 1

    .line 1236
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->pressedButtons:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private isValidTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1129
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 1133
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 1134
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result p1

    if-ne p1, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private separatedWith(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    if-eqz p1, :cond_2

    .line 591
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 594
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 595
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v1, 0x1

    .line 596
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 597
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 598
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 600
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_2
    :goto_1
    return-object p1
.end method


# virtual methods
.method public createKeysAndLines(Lcom/squareup/util/Res;)V
    .locals 14

    .line 525
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->UNKNOWN:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, p1}, Lcom/squareup/padlock/Padlock;->buildNone(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    const/4 v1, 0x0

    const/16 v2, 0x9

    const/16 v3, 0x8

    const/4 v4, 0x7

    const/4 v5, 0x6

    const/4 v6, 0x5

    const/4 v7, 0x4

    const/4 v8, 0x2

    const/4 v9, 0x1

    const/4 v10, 0x3

    if-eqz v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v11, Lcom/squareup/padlock/Padlock$Key;->ONE:Lcom/squareup/padlock/Padlock$Key;

    sget-object v12, Lcom/squareup/padlock/Padlock$Key;->ONE:Lcom/squareup/padlock/Padlock$Key;

    const-string v13, ""

    invoke-direct {p0, v9, v13, v12, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILjava/lang/String;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v9

    invoke-interface {v0, v11, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 529
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v9, Lcom/squareup/padlock/Padlock$Key;->TWO:Lcom/squareup/padlock/Padlock$Key;

    sget-object v11, Lcom/squareup/padlock/Padlock$Key;->TWO:Lcom/squareup/padlock/Padlock$Key;

    const-string v12, "ABC"

    invoke-direct {p0, v8, v12, v11, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILjava/lang/String;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v8

    invoke-interface {v0, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 530
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v8, Lcom/squareup/padlock/Padlock$Key;->THREE:Lcom/squareup/padlock/Padlock$Key;

    sget-object v9, Lcom/squareup/padlock/Padlock$Key;->THREE:Lcom/squareup/padlock/Padlock$Key;

    const-string v11, "DEF"

    invoke-direct {p0, v10, v11, v9, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILjava/lang/String;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v9

    invoke-interface {v0, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v8, Lcom/squareup/padlock/Padlock$Key;->FOUR:Lcom/squareup/padlock/Padlock$Key;

    sget-object v9, Lcom/squareup/padlock/Padlock$Key;->FOUR:Lcom/squareup/padlock/Padlock$Key;

    const-string v11, "GHI"

    invoke-direct {p0, v7, v11, v9, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILjava/lang/String;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v7

    invoke-interface {v0, v8, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v7, Lcom/squareup/padlock/Padlock$Key;->FIVE:Lcom/squareup/padlock/Padlock$Key;

    sget-object v8, Lcom/squareup/padlock/Padlock$Key;->FIVE:Lcom/squareup/padlock/Padlock$Key;

    const-string v9, "JKL"

    invoke-direct {p0, v6, v9, v8, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILjava/lang/String;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v6

    invoke-interface {v0, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v6, Lcom/squareup/padlock/Padlock$Key;->SIX:Lcom/squareup/padlock/Padlock$Key;

    sget-object v7, Lcom/squareup/padlock/Padlock$Key;->SIX:Lcom/squareup/padlock/Padlock$Key;

    const-string v8, "MNO"

    invoke-direct {p0, v5, v8, v7, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILjava/lang/String;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v5

    invoke-interface {v0, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v5, Lcom/squareup/padlock/Padlock$Key;->SEVEN:Lcom/squareup/padlock/Padlock$Key;

    sget-object v6, Lcom/squareup/padlock/Padlock$Key;->SEVEN:Lcom/squareup/padlock/Padlock$Key;

    const-string v7, "PQRS"

    invoke-direct {p0, v4, v7, v6, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILjava/lang/String;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v4

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v4, Lcom/squareup/padlock/Padlock$Key;->EIGHT:Lcom/squareup/padlock/Padlock$Key;

    sget-object v5, Lcom/squareup/padlock/Padlock$Key;->EIGHT:Lcom/squareup/padlock/Padlock$Key;

    const-string v6, "TUV"

    invoke-direct {p0, v3, v6, v5, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILjava/lang/String;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v3

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v3, Lcom/squareup/padlock/Padlock$Key;->NINE:Lcom/squareup/padlock/Padlock$Key;

    sget-object v4, Lcom/squareup/padlock/Padlock$Key;->NINE:Lcom/squareup/padlock/Padlock$Key;

    const-string v5, "WXYZ"

    invoke-direct {p0, v2, v5, v4, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILjava/lang/String;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v2

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->ZERO:Lcom/squareup/padlock/Padlock$Key;

    const/4 v3, 0x0

    sget-object v4, Lcom/squareup/padlock/Padlock$Key;->ZERO:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, v1, v3, v4, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILjava/lang/String;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 539
    :cond_0
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v11, Lcom/squareup/padlock/Padlock$Key;->ONE:Lcom/squareup/padlock/Padlock$Key;

    sget-object v12, Lcom/squareup/padlock/Padlock$Key;->ONE:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, v9, v12, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v9

    invoke-interface {v0, v11, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v9, Lcom/squareup/padlock/Padlock$Key;->TWO:Lcom/squareup/padlock/Padlock$Key;

    sget-object v11, Lcom/squareup/padlock/Padlock$Key;->TWO:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, v8, v11, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v8

    invoke-interface {v0, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 541
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v8, Lcom/squareup/padlock/Padlock$Key;->THREE:Lcom/squareup/padlock/Padlock$Key;

    sget-object v9, Lcom/squareup/padlock/Padlock$Key;->THREE:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, v10, v9, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v9

    invoke-interface {v0, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 542
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v8, Lcom/squareup/padlock/Padlock$Key;->FOUR:Lcom/squareup/padlock/Padlock$Key;

    sget-object v9, Lcom/squareup/padlock/Padlock$Key;->FOUR:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, v7, v9, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v7

    invoke-interface {v0, v8, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v7, Lcom/squareup/padlock/Padlock$Key;->FIVE:Lcom/squareup/padlock/Padlock$Key;

    sget-object v8, Lcom/squareup/padlock/Padlock$Key;->FIVE:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, v6, v8, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v6

    invoke-interface {v0, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v6, Lcom/squareup/padlock/Padlock$Key;->SIX:Lcom/squareup/padlock/Padlock$Key;

    sget-object v7, Lcom/squareup/padlock/Padlock$Key;->SIX:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, v5, v7, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v5

    invoke-interface {v0, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 545
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v5, Lcom/squareup/padlock/Padlock$Key;->SEVEN:Lcom/squareup/padlock/Padlock$Key;

    sget-object v6, Lcom/squareup/padlock/Padlock$Key;->SEVEN:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, v4, v6, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v4

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v4, Lcom/squareup/padlock/Padlock$Key;->EIGHT:Lcom/squareup/padlock/Padlock$Key;

    sget-object v5, Lcom/squareup/padlock/Padlock$Key;->EIGHT:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, v3, v5, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v3

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 547
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v3, Lcom/squareup/padlock/Padlock$Key;->NINE:Lcom/squareup/padlock/Padlock$Key;

    sget-object v4, Lcom/squareup/padlock/Padlock$Key;->NINE:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, v2, v4, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v2

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v2, Lcom/squareup/padlock/Padlock$Key;->ZERO:Lcom/squareup/padlock/Padlock$Key;

    sget-object v3, Lcom/squareup/padlock/Padlock$Key;->ZERO:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, v1, v3, p1}, Lcom/squareup/padlock/Padlock;->buildDigit(ILcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    :goto_0
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->showDecimal:Z

    if-eqz v0, :cond_1

    .line 552
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->DECIMAL:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, p1}, Lcom/squareup/padlock/Padlock;->buildDecimal(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->tabletMode:Z

    if-eqz v0, :cond_2

    .line 557
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->SUBMIT:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, p1}, Lcom/squareup/padlock/Padlock;->buildRoundedPlus(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 558
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->BACKSPACE:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, p1}, Lcom/squareup/padlock/Padlock;->buildBackspaceGlyph(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 560
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->showDecimal:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->DOUBLE_ZERO:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, p1}, Lcom/squareup/padlock/Padlock;->buildDoubleZero(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 561
    :cond_2
    iget v0, p0, Lcom/squareup/padlock/Padlock;->submitType:I

    if-eqz v0, :cond_4

    if-ne v0, v10, :cond_3

    goto :goto_1

    .line 564
    :cond_3
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->SUBMIT:Lcom/squareup/padlock/Padlock$Key;

    invoke-direct {p0, p1}, Lcom/squareup/padlock/Padlock;->buildSubmitKeyType(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->addClearOrBackspace()V

    goto :goto_2

    .line 562
    :cond_4
    :goto_1
    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->addClearOrBackspace()V

    .line 568
    :cond_5
    :goto_2
    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/squareup/padlock/Padlock;->pressedButtons:Landroid/util/SparseArray;

    .line 569
    new-instance p1, Landroid/os/Handler;

    new-instance v0, Lcom/squareup/padlock/Padlock$1;

    invoke-direct {v0, p0}, Lcom/squareup/padlock/Padlock$1;-><init>(Lcom/squareup/padlock/Padlock;)V

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object p1, p0, Lcom/squareup/padlock/Padlock;->longPressHandler:Landroid/os/Handler;

    .line 579
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/squareup/padlock/Padlock;->backspaceHandler:Landroid/os/Handler;

    .line 580
    new-instance p1, Lcom/squareup/padlock/Padlock$2;

    invoke-direct {p1, p0}, Lcom/squareup/padlock/Padlock$2;-><init>(Lcom/squareup/padlock/Padlock;)V

    iput-object p1, p0, Lcom/squareup/padlock/Padlock;->backspaceRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1116
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->accessibilityDelegate:Lcom/squareup/padlock/PadlockAccessibilityDelegate;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/PadlockAccessibilityDelegate;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 1119
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public getButtonInfo(Lcom/squareup/padlock/Padlock$Key;)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 1

    .line 623
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/padlock/Padlock$ButtonInfo;

    return-object p1
.end method

.method public getLettersTypefaceSize()I
    .locals 1

    .line 786
    iget v0, p0, Lcom/squareup/padlock/Padlock;->lettersTypefaceSize:I

    return v0
.end method

.method public getLineSpaceExtra()F
    .locals 1

    .line 790
    iget v0, p0, Lcom/squareup/padlock/Padlock;->lineSpacingExtra:F

    return v0
.end method

.method public getTypefaceSize()I
    .locals 1

    .line 782
    iget v0, p0, Lcom/squareup/padlock/Padlock;->typefaceSize:I

    return v0
.end method

.method public hasAllButtonsEnabled()Z
    .locals 2

    .line 648
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/padlock/Padlock$ButtonInfo;

    .line 649
    invoke-virtual {v1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public inPinMode()Z
    .locals 1

    .line 778
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    return v0
.end method

.method public isBackspaceEnabled()Z
    .locals 2

    .line 690
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->BACKSPACE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    if-eqz v0, :cond_0

    .line 691
    invoke-virtual {v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isClearEnabled()Z
    .locals 2

    .line 677
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    if-eqz v0, :cond_0

    .line 678
    invoke-virtual {v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isDigitsEnabled()Z
    .locals 2

    .line 627
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->ZERO:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public isSubmitEnabled()Z
    .locals 2

    .line 664
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->SUBMIT:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    if-eqz v0, :cond_0

    .line 665
    invoke-virtual {v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$getLeftEdgeLine$1$Padlock(FFFLandroid/graphics/Canvas;)V
    .locals 7

    .line 1285
    iget v0, p0, Lcom/squareup/padlock/Padlock;->lineWidth:I

    int-to-float v0, v0

    add-float v4, p1, v0

    iget-object v6, p0, Lcom/squareup/padlock/Padlock;->linePaint:Landroid/graphics/Paint;

    move-object v1, p4

    move v2, p1

    move v3, p2

    move v5, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public synthetic lambda$getRightEdgeLine$0$Padlock(FFFLandroid/graphics/Canvas;)V
    .locals 7

    .line 1281
    iget v0, p0, Lcom/squareup/padlock/Padlock;->lineWidth:I

    int-to-float v0, v0

    sub-float v2, p1, v0

    iget-object v6, p0, Lcom/squareup/padlock/Padlock;->linePaint:Landroid/graphics/Paint;

    move-object v1, p4

    move v3, p2

    move v4, p1

    move v5, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public synthetic lambda$getTopEdgeLine$2$Padlock(FFFLandroid/graphics/Canvas;)V
    .locals 7

    .line 1289
    iget v0, p0, Lcom/squareup/padlock/Padlock;->lineWidth:I

    int-to-float v0, v0

    add-float v5, p2, v0

    iget-object v6, p0, Lcom/squareup/padlock/Padlock;->linePaint:Landroid/graphics/Paint;

    move-object v1, p4

    move v2, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .line 1106
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 1107
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/padlock/Padlock$ButtonInfo;

    .line 1108
    invoke-virtual {v1, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 1110
    :cond_0
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->lines:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/padlock/Padlock$CanvasLine;

    .line 1111
    invoke-interface {v1, p1}, Lcom/squareup/padlock/Padlock$CanvasLine;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 809
    iget-boolean p1, p0, Lcom/squareup/padlock/Padlock;->tabletMode:Z

    if-eqz p1, :cond_0

    .line 810
    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->initializeTabletLayout()V

    goto :goto_0

    .line 812
    :cond_0
    invoke-direct {p0}, Lcom/squareup/padlock/Padlock;->initializePhoneLayout()V

    :goto_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    .line 794
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 795
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    int-to-float v2, v0

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    .line 797
    iput v2, p0, Lcom/squareup/padlock/Padlock;->keyHeight:F

    .line 798
    iget-boolean v2, p0, Lcom/squareup/padlock/Padlock;->tabletMode:Z

    if-eqz v2, :cond_0

    int-to-float v2, v1

    div-float/2addr v2, v3

    .line 799
    iput v2, p0, Lcom/squareup/padlock/Padlock;->keyWidth:F

    goto :goto_0

    :cond_0
    int-to-float v2, v1

    const/high16 v3, 0x40400000    # 3.0f

    div-float/2addr v2, v3

    .line 801
    iput v2, p0, Lcom/squareup/padlock/Padlock;->keyWidth:F

    .line 804
    :goto_0
    invoke-static {v1, p1}, Lcom/squareup/padlock/Padlock;->resolveSize(II)I

    move-result p1

    .line 805
    invoke-static {v0, p2}, Lcom/squareup/padlock/Padlock;->resolveSize(II)I

    move-result p2

    .line 804
    invoke-virtual {p0, p1, p2}, Lcom/squareup/padlock/Padlock;->setMeasuredDimension(II)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .line 1139
    invoke-direct {p0, p1}, Lcom/squareup/padlock/Padlock;->isValidTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 1143
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 1144
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    .line 1145
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    .line 1146
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    .line 1148
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v6

    if-eqz v6, :cond_6

    if-eq v6, v1, :cond_6

    const/4 v0, 0x2

    if-eq v6, v0, :cond_5

    const/4 v0, 0x3

    if-eq v6, v0, :cond_3

    const/4 v0, 0x5

    if-eq v6, v0, :cond_2

    const/4 v0, 0x6

    if-eq v6, v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 1166
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    const/4 v6, 0x1

    move-object v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/squareup/padlock/Padlock;->handleTouch(JIIFF)V

    goto :goto_1

    .line 1163
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    const/4 v6, 0x0

    move-object v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/squareup/padlock/Padlock;->handleTouch(JIIFF)V

    goto :goto_1

    .line 1154
    :cond_3
    iget-object p1, p0, Lcom/squareup/padlock/Padlock;->pressedButtons:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/util/SparseArray;->clear()V

    .line 1155
    iget-object p1, p0, Lcom/squareup/padlock/Padlock;->longPressHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1156
    iget-object p1, p0, Lcom/squareup/padlock/Padlock;->backspaceHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1157
    iget-object p1, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    .line 1158
    invoke-virtual {v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->clearPress()V

    goto :goto_0

    .line 1160
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    goto :goto_1

    .line 1151
    :cond_5
    invoke-direct {p0, p1}, Lcom/squareup/padlock/Padlock;->cancelOutside(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 1170
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    move-object v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/squareup/padlock/Padlock;->handleTouch(JIIFF)V

    :goto_1
    return v1
.end method

.method public setAllButtonsEnabled(Z)V
    .locals 2

    .line 657
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/padlock/Padlock$ButtonInfo;

    .line 658
    invoke-virtual {v1, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    goto :goto_0

    .line 660
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    return-void
.end method

.method public setBackspaceEnabled(Z)V
    .locals 2

    .line 695
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->BACKSPACE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    if-eqz v0, :cond_0

    .line 697
    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    .line 698
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    :cond_0
    return-void
.end method

.method public setClearEnabled(Z)V
    .locals 2

    .line 682
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    if-eqz v0, :cond_0

    .line 684
    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    .line 685
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    :cond_0
    return-void
.end method

.method public setDeleteColor(I)V
    .locals 2

    .line 733
    iget v0, p0, Lcom/squareup/padlock/Padlock;->deleteType:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 742
    :cond_0
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->backspacePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_0

    .line 738
    :cond_1
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->clearTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 739
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->clearFallbackGlyphPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_0

    .line 735
    :cond_2
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->clearGlyphPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 749
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    :cond_3
    return-void
.end method

.method public setDeleteColorDisabled(I)V
    .locals 2

    .line 753
    iget v0, p0, Lcom/squareup/padlock/Padlock;->deleteType:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 762
    :cond_0
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->backspaceDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_0

    .line 758
    :cond_1
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->clearTextDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 759
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->clearFallbackGlyphDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_0

    .line 755
    :cond_2
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->clearGlyphDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 769
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    :cond_3
    return-void
.end method

.method public setDigitColor(I)V
    .locals 1

    .line 723
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->digitPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 724
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    return-void
.end method

.method public setDigitColorDisabled(I)V
    .locals 1

    .line 728
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->digitDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 729
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    return-void
.end method

.method public setDigitsEnabled(Z)V
    .locals 2

    .line 631
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->ZERO:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    .line 632
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->ONE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    .line 633
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->TWO:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    .line 634
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->THREE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    .line 635
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->FOUR:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    .line 636
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->FIVE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    .line 637
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->SIX:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    .line 638
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->SEVEN:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    .line 639
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->EIGHT:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    .line 640
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->NINE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    .line 641
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->tabletMode:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->showDecimal:Z

    if-nez v0, :cond_0

    .line 642
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->DOUBLE_ZERO:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    .line 644
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    return-void
.end method

.method public setLineColor(I)V
    .locals 1

    .line 773
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->linePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 774
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    return-void
.end method

.method public setOnKeyPressListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V
    .locals 0

    .line 619
    iput-object p1, p0, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    return-void
.end method

.method public setPinPadLeftButtonState(Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;)V
    .locals 1

    .line 714
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    if-eqz v0, :cond_0

    .line 717
    iput-object p1, p0, Lcom/squareup/padlock/Padlock;->pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    .line 718
    iget-object p1, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v0, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/padlock/Padlock$UpdatableEnabledState;

    .line 719
    invoke-interface {p1}, Lcom/squareup/padlock/Padlock$UpdatableEnabledState;->updateEnabledState()V

    return-void

    .line 715
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Pinpad left state can be only set in pinpad mode."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setPinPadRightButtonState(Lcom/squareup/padlock/Padlock$PinPadRightButtonState;)V
    .locals 1

    .line 703
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    if-eqz v0, :cond_0

    .line 707
    iput-object p1, p0, Lcom/squareup/padlock/Padlock;->pinPadRightButtonState:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    .line 709
    iget-object p1, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v0, Lcom/squareup/padlock/Padlock$Key;->SUBMIT:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;

    .line 710
    invoke-virtual {p1}, Lcom/squareup/padlock/Padlock$CheckOrSkipButton;->updateEnabledState()V

    return-void

    .line 704
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Pinpad right state can be only set in pinpad mode."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setSubmitEnabled(Z)V
    .locals 2

    .line 669
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->SUBMIT:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    if-eqz v0, :cond_0

    .line 671
    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    .line 672
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    :cond_0
    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 2

    .line 604
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->digitPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 605
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->digitDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 606
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock;->pinMode:Z

    if-eqz v0, :cond_0

    .line 607
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->lettersPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 608
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->lettersDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 611
    :cond_0
    iget v0, p0, Lcom/squareup/padlock/Padlock;->deleteType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 612
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->clearTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 613
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->clearTextDisabledPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 615
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    return-void
.end method

.method public updateRes(Lcom/squareup/util/Res;)V
    .locals 3

    .line 1180
    iget-object v0, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/padlock/Padlock$Key;

    .line 1181
    iget-object v2, p0, Lcom/squareup/padlock/Padlock;->keys:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/padlock/Padlock$ButtonInfo;

    invoke-virtual {v2, v1, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->updateRes(Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    goto :goto_0

    :cond_0
    return-void
.end method
