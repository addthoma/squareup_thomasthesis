.class public final Lcom/squareup/padlock/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/padlock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AspectRatioPadlock:[I

.field public static final AspectRatioPadlock_sq_aspectRatio:I = 0x0

.field public static final Padlock:[I

.field public static final Padlock_android_lineSpacingExtra:I = 0x0

.field public static final Padlock_backspaceColor:I = 0x1

.field public static final Padlock_backspaceDisabledColor:I = 0x2

.field public static final Padlock_backspaceSelector:I = 0x3

.field public static final Padlock_buttonSelector:I = 0x4

.field public static final Padlock_buttonTextSize:I = 0x5

.field public static final Padlock_clearTextColor:I = 0x6

.field public static final Padlock_clearTextDisabledColor:I = 0x7

.field public static final Padlock_deleteType:I = 0x8

.field public static final Padlock_digitColor:I = 0x9

.field public static final Padlock_digitDisabledColor:I = 0xa

.field public static final Padlock_drawDividerLines:I = 0xb

.field public static final Padlock_drawLeftLine:I = 0xc

.field public static final Padlock_drawRightLine:I = 0xd

.field public static final Padlock_drawTopLine:I = 0xe

.field public static final Padlock_horizontalDividerStyle:I = 0xf

.field public static final Padlock_lettersColor:I = 0x10

.field public static final Padlock_lettersDisabledColor:I = 0x11

.field public static final Padlock_lettersTextSize:I = 0x12

.field public static final Padlock_lineColor:I = 0x13

.field public static final Padlock_pinMode:I = 0x14

.field public static final Padlock_pinSubmitColor:I = 0x15

.field public static final Padlock_showDecimal:I = 0x16

.field public static final Padlock_submitColor:I = 0x17

.field public static final Padlock_submitDisabledColor:I = 0x18

.field public static final Padlock_submitSelector:I = 0x19

.field public static final Padlock_submitType:I = 0x1a

.field public static final Padlock_tabletMode:I = 0x1b


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f0403c2

    aput v2, v0, v1

    .line 136
    sput-object v0, Lcom/squareup/padlock/R$styleable;->AspectRatioPadlock:[I

    const/16 v0, 0x1c

    new-array v0, v0, [I

    .line 138
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/padlock/R$styleable;->Padlock:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1010217
        0x7f04004d
        0x7f04004e
        0x7f04004f
        0x7f040086
        0x7f04008b
        0x7f0400c1
        0x7f0400c2
        0x7f04011c
        0x7f040124
        0x7f040126
        0x7f040139
        0x7f04013a
        0x7f04013b
        0x7f04013c
        0x7f0401c4
        0x7f040278
        0x7f040279
        0x7f04027a
        0x7f04027d
        0x7f040313
        0x7f040314
        0x7f040349
        0x7f0403fe
        0x7f0403ff
        0x7f040400
        0x7f040401
        0x7f040429
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
