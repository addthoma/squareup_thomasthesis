.class public abstract Lcom/squareup/padlock/MoneyKeypadListener;
.super Lcom/squareup/padlock/BaseUpdatingKeypadListener;
.source "MoneyKeypadListener.java"


# instance fields
.field private final maxAmount:J


# direct methods
.method public constructor <init>(Lcom/squareup/padlock/Padlock;Landroid/os/Vibrator;J)V
    .locals 0

    .line 16
    invoke-direct {p0, p2, p1}, Lcom/squareup/padlock/BaseUpdatingKeypadListener;-><init>(Landroid/os/Vibrator;Lcom/squareup/padlock/Padlock;)V

    .line 17
    iput-wide p3, p0, Lcom/squareup/padlock/MoneyKeypadListener;->maxAmount:J

    .line 18
    invoke-virtual {p0}, Lcom/squareup/padlock/MoneyKeypadListener;->updateKeyStates()V

    return-void
.end method


# virtual methods
.method protected backspaceEnabled()Z
    .locals 5

    .line 76
    invoke-virtual {p0}, Lcom/squareup/padlock/MoneyKeypadListener;->getAmount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected digitsEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public abstract getAmount()J
.end method

.method public onBackspaceClicked()V
    .locals 5

    .line 46
    invoke-virtual {p0}, Lcom/squareup/padlock/MoneyKeypadListener;->getAmount()J

    move-result-wide v0

    const-wide/16 v2, 0xa

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const-wide/16 v0, 0x0

    .line 48
    invoke-virtual {p0, v0, v1}, Lcom/squareup/padlock/MoneyKeypadListener;->updateAmount(J)V

    goto :goto_0

    .line 50
    :cond_0
    div-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/squareup/padlock/MoneyKeypadListener;->updateAmount(J)V

    .line 52
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/padlock/MoneyKeypadListener;->updateKeyStates()V

    return-void
.end method

.method public onCancelClicked()V
    .locals 0

    return-void
.end method

.method public onClearClicked()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 41
    invoke-virtual {p0, v0, v1}, Lcom/squareup/padlock/MoneyKeypadListener;->updateAmount(J)V

    .line 42
    invoke-virtual {p0}, Lcom/squareup/padlock/MoneyKeypadListener;->updateKeyStates()V

    return-void
.end method

.method public onClearLongpressed()V
    .locals 2

    const-wide/16 v0, 0x32

    .line 35
    invoke-virtual {p0, v0, v1}, Lcom/squareup/padlock/MoneyKeypadListener;->vibrate(J)V

    const-wide/16 v0, 0x0

    .line 36
    invoke-virtual {p0, v0, v1}, Lcom/squareup/padlock/MoneyKeypadListener;->updateAmount(J)V

    .line 37
    invoke-virtual {p0}, Lcom/squareup/padlock/MoneyKeypadListener;->updateKeyStates()V

    return-void
.end method

.method public onDecimalClicked()V
    .locals 0

    return-void
.end method

.method public onDigitClicked(I)V
    .locals 6

    .line 26
    invoke-virtual {p0}, Lcom/squareup/padlock/MoneyKeypadListener;->getAmount()J

    move-result-wide v0

    .line 27
    iget-wide v2, p0, Lcom/squareup/padlock/MoneyKeypadListener;->maxAmount:J

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const-wide/16 v4, 0xa

    mul-long v0, v0, v4

    int-to-long v4, p1

    add-long/2addr v0, v4

    .line 29
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/padlock/MoneyKeypadListener;->updateAmount(J)V

    .line 31
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/padlock/MoneyKeypadListener;->updateKeyStates()V

    return-void
.end method

.method public onPinDigitEntered(FF)V
    .locals 0

    return-void
.end method

.method public onSkipClicked()V
    .locals 0

    return-void
.end method

.method protected submitEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public abstract updateAmount(J)V
.end method

.method public updateKeyStates()V
    .locals 0

    .line 84
    invoke-virtual {p0}, Lcom/squareup/padlock/MoneyKeypadListener;->updateBackspaceState()V

    .line 85
    invoke-virtual {p0}, Lcom/squareup/padlock/MoneyKeypadListener;->updateSubmitState()V

    .line 86
    invoke-virtual {p0}, Lcom/squareup/padlock/MoneyKeypadListener;->updateDigitsState()V

    return-void
.end method
