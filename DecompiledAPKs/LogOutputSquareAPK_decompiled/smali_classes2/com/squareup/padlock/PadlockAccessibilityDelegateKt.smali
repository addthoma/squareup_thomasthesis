.class public final Lcom/squareup/padlock/PadlockAccessibilityDelegateKt;
.super Ljava/lang/Object;
.source "PadlockAccessibilityDelegate.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u001a\u000c\u0010\u0003\u001a\u00020\u0002*\u00020\u0001H\u0000\u00a8\u0006\u0004"
    }
    d2 = {
        "toKey",
        "Lcom/squareup/padlock/Padlock$Key;",
        "",
        "toVirtualViewId",
        "padlock_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$toKey(I)Lcom/squareup/padlock/Padlock$Key;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/padlock/PadlockAccessibilityDelegateKt;->toKey(I)Lcom/squareup/padlock/Padlock$Key;

    move-result-object p0

    return-object p0
.end method

.method private static final toKey(I)Lcom/squareup/padlock/Padlock$Key;
    .locals 1

    .line 103
    invoke-static {p0}, Lcom/squareup/padlock/Padlock$Key;->valueOf(I)Lcom/squareup/padlock/Padlock$Key;

    move-result-object p0

    const-string v0, "Key.valueOf(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toVirtualViewId(Lcom/squareup/padlock/Padlock$Key;)I
    .locals 1

    const-string v0, "$this$toVirtualViewId"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock$Key;->ordinal()I

    move-result p0

    return p0
.end method
