.class public interface abstract Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;
.super Ljava/lang/Object;
.source "TransitionalAccountStatusResponseCache.java"

# interfaces
.implements Lcom/squareup/accountstatus/AccountStatusResponseCache;


# virtual methods
.method public abstract init(Lcom/squareup/accountstatus/QuietServerPreferences;)V
.end method

.method public abstract onLoggedIn()V
.end method

.method public abstract setPreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/Preferences;",
            ">;>;"
        }
    .end annotation
.end method
