.class public interface abstract Lcom/squareup/accountstatus/QuietServerPreferences;
.super Ljava/lang/Object;
.source "QuietServerPreferences.java"


# virtual methods
.method public abstract setPreferencesQuietly(Lcom/squareup/server/account/protos/PreferencesRequest;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/Preferences;",
            ">;>;"
        }
    .end annotation
.end method
