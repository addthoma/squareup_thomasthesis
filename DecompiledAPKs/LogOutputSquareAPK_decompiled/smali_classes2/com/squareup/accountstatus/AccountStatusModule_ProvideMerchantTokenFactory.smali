.class public final Lcom/squareup/accountstatus/AccountStatusModule_ProvideMerchantTokenFactory;
.super Ljava/lang/Object;
.source "AccountStatusModule_ProvideMerchantTokenFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final serviceCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/accountstatus/AccountStatusModule_ProvideMerchantTokenFactory;->serviceCacheProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/accountstatus/AccountStatusModule_ProvideMerchantTokenFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;)",
            "Lcom/squareup/accountstatus/AccountStatusModule_ProvideMerchantTokenFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/accountstatus/AccountStatusModule_ProvideMerchantTokenFactory;

    invoke-direct {v0, p0}, Lcom/squareup/accountstatus/AccountStatusModule_ProvideMerchantTokenFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideMerchantToken(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Ljava/lang/String;
    .locals 0

    .line 37
    invoke-static {p0}, Lcom/squareup/accountstatus/AccountStatusModule;->provideMerchantToken(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/accountstatus/AccountStatusModule_ProvideMerchantTokenFactory;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/accountstatus/AccountStatusModule_ProvideMerchantTokenFactory;->serviceCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-static {v0}, Lcom/squareup/accountstatus/AccountStatusModule_ProvideMerchantTokenFactory;->provideMerchantToken(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
