.class Lcom/squareup/pauses/PauseAndResumePresenter$Registration;
.super Ljava/lang/Object;
.source "PauseAndResumePresenter.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/pauses/PauseAndResumePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Registration"
.end annotation


# instance fields
.field final registrant:Lcom/squareup/pauses/PausesAndResumes;

.field final synthetic this$0:Lcom/squareup/pauses/PauseAndResumePresenter;


# direct methods
.method private constructor <init>(Lcom/squareup/pauses/PauseAndResumePresenter;Lcom/squareup/pauses/PausesAndResumes;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/pauses/PauseAndResumePresenter$Registration;->this$0:Lcom/squareup/pauses/PauseAndResumePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p2, p0, Lcom/squareup/pauses/PauseAndResumePresenter$Registration;->registrant:Lcom/squareup/pauses/PausesAndResumes;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/pauses/PauseAndResumePresenter;Lcom/squareup/pauses/PausesAndResumes;Lcom/squareup/pauses/PauseAndResumePresenter$1;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/squareup/pauses/PauseAndResumePresenter$Registration;-><init>(Lcom/squareup/pauses/PauseAndResumePresenter;Lcom/squareup/pauses/PausesAndResumes;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    .line 94
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 96
    :cond_1
    check-cast p1, Lcom/squareup/pauses/PauseAndResumePresenter$Registration;

    .line 98
    iget-object v0, p0, Lcom/squareup/pauses/PauseAndResumePresenter$Registration;->registrant:Lcom/squareup/pauses/PausesAndResumes;

    iget-object p1, p1, Lcom/squareup/pauses/PauseAndResumePresenter$Registration;->registrant:Lcom/squareup/pauses/PausesAndResumes;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/pauses/PauseAndResumePresenter$Registration;->registrant:Lcom/squareup/pauses/PausesAndResumes;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/pauses/PauseAndResumePresenter$Registration;->this$0:Lcom/squareup/pauses/PauseAndResumePresenter;

    iget-object v0, v0, Lcom/squareup/pauses/PauseAndResumePresenter;->registrations:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
