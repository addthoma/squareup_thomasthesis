.class public final Lcom/squareup/buyercheckout/BuyerCheckoutLauncher$Companion;
.super Ljava/lang/Object;
.source "BuyerCheckoutLauncher.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBuyerCheckoutLauncher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BuyerCheckoutLauncher.kt\ncom/squareup/buyercheckout/BuyerCheckoutLauncher$Companion\n+ 2 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPool$Companion\n+ 3 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPoolKt\n*L\n1#1,19:1\n334#2,2:20\n412#3:22\n*E\n*S KotlinDebug\n*F\n+ 1 BuyerCheckoutLauncher.kt\ncom/squareup/buyercheckout/BuyerCheckoutLauncher$Companion\n*L\n15#1,2:20\n15#1:22\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00042\u0006\u0010\u0008\u001a\u00020\u0005\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCheckoutLauncher$Companion;",
        "",
        "()V",
        "handle",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "state",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/buyercheckout/BuyerCheckoutLauncher$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutLauncher$Companion;

    invoke-direct {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutLauncher$Companion;-><init>()V

    sput-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutLauncher$Companion;->$$INSTANCE:Lcom/squareup/buyercheckout/BuyerCheckoutLauncher$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final handle(Lcom/squareup/buyercheckout/BuyerCheckoutState;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowPool;->Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    const-class v0, Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;

    .line 22
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v1, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-class v2, Lcom/squareup/buyercheckout/BuyerCheckoutEvent;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/tenderpayment/TenderPaymentResult;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    const-string v1, ""

    .line 21
    invoke-virtual {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object v1
.end method
