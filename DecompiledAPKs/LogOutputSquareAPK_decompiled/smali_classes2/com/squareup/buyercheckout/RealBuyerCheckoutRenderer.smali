.class public final Lcom/squareup/buyercheckout/RealBuyerCheckoutRenderer;
.super Ljava/lang/Object;
.source "RealBuyerCheckoutRenderer.kt"

# interfaces
.implements Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J2\u0010\u0003\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/RealBuyerCheckoutRenderer;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;",
        "()V",
        "render",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "state",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public render(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;

    if-nez v0, :cond_7

    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;

    if-nez v0, :cond_7

    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;

    if-nez v0, :cond_7

    .line 27
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;

    if-eqz v0, :cond_0

    .line 28
    new-instance p3, Lcom/squareup/buyercheckout/BuyerCart$ScreenData;

    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;->getShowBuyerLanguageButton()Z

    move-result p1

    invoke-direct {p3, p1}, Lcom/squareup/buyercheckout/BuyerCart$ScreenData;-><init>(Z)V

    .line 29
    invoke-static {p3, p2}, Lcom/squareup/buyercheckout/BuyerCartScreenKt;->BuyerCartScreen(Lcom/squareup/buyercheckout/BuyerCart$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 32
    :cond_0
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;

    if-eqz v0, :cond_1

    .line 33
    new-instance p3, Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;

    .line 34
    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;->getInOfflineMode()Z

    move-result v0

    .line 35
    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;->getShowBuyerLanguageButton()Z

    move-result p1

    .line 33
    invoke-direct {p3, v0, p1}, Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;-><init>(ZZ)V

    .line 37
    invoke-static {p3, p2}, Lcom/squareup/buyercheckout/PaymentPromptScreenKt;->PaymentPromptScreen(Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 40
    :cond_1
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->getTipWorkflowHandle()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    goto :goto_0

    .line 42
    :cond_3
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    if-eqz v0, :cond_5

    .line 43
    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;->getWorkflowHandle()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    check-cast p1, Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 45
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map$Entry;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    goto :goto_0

    .line 51
    :cond_5
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

    if-eqz v0, :cond_6

    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;->getFromState()Lcom/squareup/buyercheckout/BuyerCheckoutState;

    move-result-object p1

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/buyercheckout/RealBuyerCheckoutRenderer;->render(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 25
    :cond_7
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "The launcher is expected to filter "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 p1, 0x2e

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/buyercheckout/RealBuyerCheckoutRenderer;->render(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
