.class public final Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;
.super Ljava/lang/Object;
.source "RealBuyerCheckoutScreenWorkflowStarter.kt"

# interfaces
.implements Lcom/squareup/buyercheckout/BuyerCheckoutScreenWorkflowStarter;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBuyerCheckoutScreenWorkflowStarter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBuyerCheckoutScreenWorkflowStarter.kt\ncom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter\n*L\n1#1,46:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J4\u0010\u0007\u001a&\u0012\u0012\u0012\u0010\u0012\u000c\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\n0\t\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\u0008j\u0002`\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J4\u0010\u0007\u001a&\u0012\u0012\u0012\u0010\u0012\u000c\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\n0\t\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\u0008j\u0002`\r2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J4\u0010\u0007\u001a&\u0012\u0012\u0012\u0010\u0012\u000c\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\n0\t\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\u0008j\u0002`\r2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutScreenWorkflowStarter;",
        "launcher",
        "Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;",
        "buyerCheckoutRenderer",
        "Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;",
        "(Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;)V",
        "start",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutScreenWorkflow;",
        "settings",
        "Lcom/squareup/buyercheckout/BuyerCheckoutSettings;",
        "initialState",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerCheckoutRenderer:Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;

.field private final launcher:Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;


# direct methods
.method public constructor <init>(Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "launcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerCheckoutRenderer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;->launcher:Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;

    iput-object p2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;->buyerCheckoutRenderer:Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;

    return-void
.end method

.method public static final synthetic access$getBuyerCheckoutRenderer$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;)Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;->buyerCheckoutRenderer:Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;

    return-object p0
.end method

.method private final start(Lcom/squareup/buyercheckout/BuyerCheckoutState;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;*",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-direct {v0}, Lcom/squareup/workflow/legacy/WorkflowPool;-><init>()V

    .line 29
    iget-object v1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;->launcher:Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;

    .line 30
    invoke-interface {v1, p1, v0}, Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;->launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 31
    sget-object v1, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$1;->INSTANCE:Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v1}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->switchMapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 36
    new-instance v1, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2, p0, v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter$start$$inlined$let$lambda$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/coroutines/Continuation;Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;Lcom/squareup/workflow/legacy/WorkflowPool;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {p1, v1}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->mapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 42
    invoke-static {p1}, Lcom/squareup/workflow/rx1/WorkflowKt;->toRx1Workflow(Lcom/squareup/workflow/legacy/Workflow;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public start(Lcom/squareup/buyercheckout/BuyerCheckoutSettings;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutSettings;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;*",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    sget-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;->startingState(Lcom/squareup/buyercheckout/BuyerCheckoutSettings;)Lcom/squareup/buyercheckout/BuyerCheckoutState;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;->start(Lcom/squareup/buyercheckout/BuyerCheckoutState;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;*",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    sget-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;->fromSnapshot(Lokio/ByteString;)Lcom/squareup/buyercheckout/BuyerCheckoutState;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutScreenWorkflowStarter;->start(Lcom/squareup/buyercheckout/BuyerCheckoutState;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method
