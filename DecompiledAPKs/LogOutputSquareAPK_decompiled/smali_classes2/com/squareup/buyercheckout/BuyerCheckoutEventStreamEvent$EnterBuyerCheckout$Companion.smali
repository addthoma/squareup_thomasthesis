.class public final Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout$Companion;
.super Ljava/lang/Object;
.source "BuyerCheckoutEventStreamEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u0007\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout$Companion;",
        "",
        "()V",
        "of",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;",
        "customerCheckoutSettings",
        "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
        "displayItems",
        "",
        "Lcom/squareup/comms/protos/seller/DisplayItem;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final of(Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Ljava/util/List;)Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/DisplayItem;",
            ">;)",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "customerCheckoutSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "displayItems"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;

    .line 55
    invoke-interface {p1}, Lcom/squareup/buyercheckout/CustomerCheckoutSettings;->isDefaultCustomerCheckoutEnabled()Z

    move-result v1

    .line 56
    invoke-interface {p1}, Lcom/squareup/buyercheckout/CustomerCheckoutSettings;->isSkipCartScreenEnabled()Z

    move-result p1

    .line 57
    move-object v2, p2

    check-cast v2, Ljava/lang/Iterable;

    const-string p2, ", "

    move-object v3, p2

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x3e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 54
    invoke-direct {v0, v1, p1, p2}, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;-><init>(ZZLjava/lang/String;)V

    return-object v0
.end method
