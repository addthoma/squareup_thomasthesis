.class public final Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow_Factory;
.super Ljava/lang/Object;
.source "RealBuyerCheckoutV2Workflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final buyerCheckoutLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerCheckoutRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow_Factory;->buyerCheckoutLauncherProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow_Factory;->buyerCheckoutRendererProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;",
            ">;)",
            "Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;)Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow;

    invoke-direct {v0, p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow_Factory;->buyerCheckoutLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;

    iget-object v1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow_Factory;->buyerCheckoutRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;

    invoke-static {v0, v1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow_Factory;->newInstance(Lcom/squareup/buyercheckout/BuyerCheckoutLauncher;Lcom/squareup/buyercheckout/BuyerCheckoutRenderer;)Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow_Factory;->get()Lcom/squareup/buyercheckout/RealBuyerCheckoutV2Workflow;

    move-result-object v0

    return-object v0
.end method
