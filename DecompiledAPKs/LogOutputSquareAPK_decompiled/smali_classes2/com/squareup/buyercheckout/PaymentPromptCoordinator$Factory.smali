.class public final Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;
.super Ljava/lang/Object;
.source "PaymentPromptCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/buyercheckout/PaymentPromptCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ$\u0010\u000b\u001a\u00020\u000c2\u001c\u0010\r\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000fj\u0002`\u00120\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;",
        "",
        "formattedTotalProvider",
        "Lcom/squareup/ui/buyer/FormattedTotalProvider;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "transactionTypeDisplay",
        "Lcom/squareup/buyercheckout/TransactionTypeDisplay;",
        "(Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyercheckout/TransactionTypeDisplay;)V",
        "build",
        "Lcom/squareup/buyercheckout/PaymentPromptCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/buyercheckout/PaymentPromptScreen;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final formattedTotalProvider:Lcom/squareup/ui/buyer/FormattedTotalProvider;

.field private final transactionTypeDisplay:Lcom/squareup/buyercheckout/TransactionTypeDisplay;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyercheckout/TransactionTypeDisplay;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "formattedTotalProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLocaleOverride"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tutorialCore"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionTypeDisplay"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;->formattedTotalProvider:Lcom/squareup/ui/buyer/FormattedTotalProvider;

    iput-object p2, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p3, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p4, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;->transactionTypeDisplay:Lcom/squareup/buyercheckout/TransactionTypeDisplay;

    return-void
.end method


# virtual methods
.method public final build(Lio/reactivex/Observable;)Lcom/squareup/buyercheckout/PaymentPromptCoordinator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/buyercheckout/PaymentPrompt$ScreenData;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;>;)",
            "Lcom/squareup/buyercheckout/PaymentPromptCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;

    .line 51
    iget-object v3, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;->formattedTotalProvider:Lcom/squareup/ui/buyer/FormattedTotalProvider;

    .line 52
    iget-object v4, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    .line 53
    iget-object v5, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    .line 54
    iget-object v6, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;->transactionTypeDisplay:Lcom/squareup/buyercheckout/TransactionTypeDisplay;

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    .line 49
    invoke-direct/range {v1 .. v7}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyercheckout/TransactionTypeDisplay;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
