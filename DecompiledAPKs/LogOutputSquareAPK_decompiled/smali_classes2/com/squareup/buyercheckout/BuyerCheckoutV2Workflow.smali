.class public interface abstract Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;
.super Ljava/lang/Object;
.source "BuyerCheckoutV2Workflow.kt"

# interfaces
.implements Lcom/squareup/workflow/Workflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/Workflow<",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering<",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002,\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u00040\u0001\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;",
        "Lcom/squareup/workflow/Workflow;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
