.class public final Lcom/squareup/blecoroutines/RealConnection;
.super Ljava/lang/Object;
.source "RealConnection.kt"

# interfaces
.implements Lcom/squareup/blecoroutines/Connection;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealConnection.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealConnection.kt\ncom/squareup/blecoroutines/RealConnection\n+ 2 GattCallbackBroker.kt\ncom/squareup/blecoroutines/GattCallbackBroker\n*L\n1#1,166:1\n61#2:167\n61#2:168\n61#2:169\n61#2:170\n*E\n*S KotlinDebug\n*F\n+ 1 RealConnection.kt\ncom/squareup/blecoroutines/RealConnection\n*L\n47#1:167\n66#1:168\n78#1:169\n122#1:170\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0012\n\u0002\u0008\u0002\u0008\u0001\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0011\u0010\u000e\u001a\u00020\u000fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0014H\u0016J \u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\u0014H\u0002J!\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u001c\u001a\u00020\u0014H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001dJ/\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u001f2\u0006\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u001c\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\u0014H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010 J\u0019\u0010!\u001a\u00020\u000f2\u0006\u0010\u0015\u001a\u00020\u0012H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\"J1\u0010#\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\u00142\u0006\u0010$\u001a\u00020%H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010&R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/blecoroutines/RealConnection;",
        "Lcom/squareup/blecoroutines/Connection;",
        "bluetoothGatt",
        "Landroid/bluetooth/BluetoothGatt;",
        "gattCallbackBroker",
        "Lcom/squareup/blecoroutines/GattCallbackBroker;",
        "tmnTimings",
        "Lcom/squareup/tmn/TmnTimings;",
        "(Landroid/bluetooth/BluetoothGatt;Lcom/squareup/blecoroutines/GattCallbackBroker;Lcom/squareup/tmn/TmnTimings;)V",
        "onDisconnection",
        "Lkotlinx/coroutines/Deferred;",
        "",
        "getOnDisconnection",
        "()Lkotlinx/coroutines/Deferred;",
        "discoverServices",
        "",
        "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "findCharacteristic",
        "Landroid/bluetooth/BluetoothGattCharacteristic;",
        "service",
        "Ljava/util/UUID;",
        "characteristic",
        "findDescriptor",
        "Landroid/bluetooth/BluetoothGattDescriptor;",
        "serviceUuid",
        "characteristicUuid",
        "descriptorUuid",
        "readCharacteristic",
        "uuid",
        "(Ljava/util/UUID;Ljava/util/UUID;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "subscribeToCharacteristic",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "(Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "writeCharacteristic",
        "(Landroid/bluetooth/BluetoothGattCharacteristic;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "writeDescriptor",
        "value",
        "",
        "(Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;[BLkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

.field private final gattCallbackBroker:Lcom/squareup/blecoroutines/GattCallbackBroker;

.field private final tmnTimings:Lcom/squareup/tmn/TmnTimings;


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothGatt;Lcom/squareup/blecoroutines/GattCallbackBroker;Lcom/squareup/tmn/TmnTimings;)V
    .locals 1

    const-string v0, "bluetoothGatt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gattCallbackBroker"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tmnTimings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/blecoroutines/RealConnection;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    iput-object p2, p0, Lcom/squareup/blecoroutines/RealConnection;->gattCallbackBroker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    iput-object p3, p0, Lcom/squareup/blecoroutines/RealConnection;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    return-void
.end method

.method public static final synthetic access$getBluetoothGatt$p(Lcom/squareup/blecoroutines/RealConnection;)Landroid/bluetooth/BluetoothGatt;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/blecoroutines/RealConnection;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    return-object p0
.end method

.method public static final synthetic access$getTmnTimings$p(Lcom/squareup/blecoroutines/RealConnection;)Lcom/squareup/tmn/TmnTimings;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/blecoroutines/RealConnection;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    return-object p0
.end method

.method private final findDescriptor(Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;
    .locals 11

    .line 145
    invoke-virtual {p0, p1, p2}, Lcom/squareup/blecoroutines/RealConnection;->findCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v0

    .line 146
    invoke-virtual {v0, p3}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 147
    :cond_0
    new-instance v0, Lcom/squareup/blecoroutines/BleError;

    .line 148
    sget-object v2, Lcom/squareup/blecoroutines/Event;->DESCRIPTOR_MISSING:Lcom/squareup/blecoroutines/Event;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x70

    const/4 v10, 0x0

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    .line 147
    invoke-direct/range {v1 .. v10}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public discoverServices(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p1, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;

    iget v1, v0, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;->label:I

    const/high16 v2, -0x80000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    iget p1, v0, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;->label:I

    sub-int/2addr p1, v2

    iput p1, v0, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;-><init>(Lcom/squareup/blecoroutines/RealConnection;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object p1, v0, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v1

    .line 65
    iget v2, v0, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;->label:I

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v3, :cond_1

    iget-object v1, v0, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;->L$2:Ljava/lang/Object;

    check-cast v1, Lkotlin/jvm/functions/Function0;

    iget-object v1, v0, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;->L$1:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/blecoroutines/GattCallbackBroker;

    iget-object v0, v0, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/blecoroutines/RealConnection;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_1

    .line 71
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 65
    :cond_2
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/blecoroutines/RealConnection;->gattCallbackBroker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    new-instance v2, Lcom/squareup/blecoroutines/RealConnection$discoverServices$result$1;

    invoke-direct {v2, p0}, Lcom/squareup/blecoroutines/RealConnection$discoverServices$result$1;-><init>(Lcom/squareup/blecoroutines/RealConnection;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 168
    const-class v4, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnServicesDiscovered;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    iput-object p0, v0, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;->L$0:Ljava/lang/Object;

    iput-object p1, v0, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;->L$1:Ljava/lang/Object;

    iput-object v2, v0, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;->L$2:Ljava/lang/Object;

    iput v3, v0, Lcom/squareup/blecoroutines/RealConnection$discoverServices$1;->label:I

    invoke-virtual {p1, v4, v2, v0}, Lcom/squareup/blecoroutines/GattCallbackBroker;->runAndWaitForCallback(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v1, :cond_3

    return-object v1

    .line 66
    :cond_3
    :goto_1
    check-cast p1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnServicesDiscovered;

    .line 70
    invoke-virtual {p1}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnServicesDiscovered;->getStatus()I

    move-result p1

    if-nez p1, :cond_4

    .line 71
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1

    .line 70
    :cond_4
    new-instance p1, Lcom/squareup/blecoroutines/BleError;

    sget-object v1, Lcom/squareup/blecoroutines/Event;->SERVICE_DISCOVERY_FAILED:Lcom/squareup/blecoroutines/Event;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7e

    const/4 v9, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v9}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public findCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 11

    const-string v0, "service"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "characteristic"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/blecoroutines/RealConnection;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothGatt;->getService(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 160
    invoke-virtual {v0, p2}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 161
    :cond_0
    new-instance v0, Lcom/squareup/blecoroutines/BleError;

    .line 162
    sget-object v2, Lcom/squareup/blecoroutines/Event;->CHARACTERISTIC_MISSING:Lcom/squareup/blecoroutines/Event;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x78

    const/4 v10, 0x0

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    .line 161
    invoke-direct/range {v1 .. v10}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 158
    :cond_1
    new-instance p2, Lcom/squareup/blecoroutines/BleError;

    sget-object v2, Lcom/squareup/blecoroutines/Event;->SERVICE_MISSING:Lcom/squareup/blecoroutines/Event;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7c

    const/4 v10, 0x0

    move-object v1, p2

    move-object v3, p1

    invoke-direct/range {v1 .. v10}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public getOnDisconnection()Lkotlinx/coroutines/Deferred;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/Deferred<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/blecoroutines/RealConnection;->gattCallbackBroker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    invoke-virtual {v0}, Lcom/squareup/blecoroutines/GattCallbackBroker;->getOnDisconnection()Lkotlinx/coroutines/CompletableDeferred;

    move-result-object v0

    check-cast v0, Lkotlinx/coroutines/Deferred;

    return-object v0
.end method

.method public readCharacteristic(Ljava/util/UUID;Ljava/util/UUID;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/UUID;",
            "Ljava/util/UUID;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p3, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;

    if-eqz v0, :cond_0

    move-object v0, p3

    check-cast v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;

    iget v1, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->label:I

    const/high16 v2, -0x80000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    iget p3, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->label:I

    sub-int/2addr p3, v2

    iput p3, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;

    invoke-direct {v0, p0, p3}, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;-><init>(Lcom/squareup/blecoroutines/RealConnection;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object p3, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v1

    .line 73
    iget v2, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->label:I

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v3, :cond_1

    iget-object p1, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->L$5:Ljava/lang/Object;

    check-cast p1, Lkotlin/jvm/functions/Function0;

    iget-object p1, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->L$4:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/blecoroutines/GattCallbackBroker;

    iget-object p1, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->L$3:Ljava/lang/Object;

    check-cast p1, Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object p1, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->L$2:Ljava/lang/Object;

    move-object p2, p1

    check-cast p2, Ljava/util/UUID;

    iget-object p1, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->L$1:Ljava/lang/Object;

    check-cast p1, Ljava/util/UUID;

    iget-object v0, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/blecoroutines/RealConnection;

    invoke-static {p3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_1

    .line 93
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 73
    :cond_2
    invoke-static {p3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 77
    invoke-virtual {p0, p1, p2}, Lcom/squareup/blecoroutines/RealConnection;->findCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p3

    .line 78
    iget-object v2, p0, Lcom/squareup/blecoroutines/RealConnection;->gattCallbackBroker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    new-instance v4, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$result$1;

    invoke-direct {v4, p0, p3, p1, p2}, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$result$1;-><init>(Lcom/squareup/blecoroutines/RealConnection;Landroid/bluetooth/BluetoothGattCharacteristic;Ljava/util/UUID;Ljava/util/UUID;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 169
    const-class v5, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicRead;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    iput-object p0, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->L$0:Ljava/lang/Object;

    iput-object p1, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->L$1:Ljava/lang/Object;

    iput-object p2, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->L$2:Ljava/lang/Object;

    iput-object p3, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->L$3:Ljava/lang/Object;

    iput-object v2, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->L$4:Ljava/lang/Object;

    iput-object v4, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->L$5:Ljava/lang/Object;

    iput v3, v0, Lcom/squareup/blecoroutines/RealConnection$readCharacteristic$1;->label:I

    invoke-virtual {v2, v5, v4, v0}, Lcom/squareup/blecoroutines/GattCallbackBroker;->runAndWaitForCallback(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p3

    if-ne p3, v1, :cond_3

    return-object v1

    :cond_3
    :goto_1
    move-object v2, p1

    move-object v3, p2

    .line 78
    check-cast p3, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicRead;

    .line 85
    invoke-virtual {p3}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicRead;->getStatus()I

    move-result p1

    if-nez p1, :cond_4

    .line 92
    invoke-virtual {p3}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicRead;->getCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p1

    return-object p1

    .line 86
    :cond_4
    new-instance p1, Lcom/squareup/blecoroutines/BleError;

    .line 87
    sget-object v1, Lcom/squareup/blecoroutines/Event;->CHARACTERISTIC_READ_FAILED:Lcom/squareup/blecoroutines/Event;

    const/4 v4, 0x0

    .line 88
    invoke-virtual {p3}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicRead;->getStatus()I

    move-result p2

    invoke-static {p2}, Lkotlin/coroutines/jvm/internal/Boxing;->boxInt(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x68

    const/4 v9, 0x0

    move-object v0, p1

    .line 86
    invoke-direct/range {v0 .. v9}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public subscribeToCharacteristic(Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/UUID;",
            "Ljava/util/UUID;",
            "Ljava/util/UUID;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "+",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p4, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;

    if-eqz v0, :cond_0

    move-object v0, p4

    check-cast v0, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;

    iget v1, v0, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->label:I

    const/high16 v2, -0x80000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    iget p4, v0, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->label:I

    sub-int/2addr p4, v2

    iput p4, v0, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;

    invoke-direct {v0, p0, p4}, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;-><init>(Lcom/squareup/blecoroutines/RealConnection;Lkotlin/coroutines/Continuation;)V

    :goto_0
    move-object v6, v0

    iget-object p4, v6, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 95
    iget v1, v6, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->label:I

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    if-ne v1, v2, :cond_1

    iget-object p1, v6, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->L$4:Ljava/lang/Object;

    check-cast p1, Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object p1, v6, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->L$3:Ljava/lang/Object;

    check-cast p1, Ljava/util/UUID;

    iget-object p1, v6, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->L$2:Ljava/lang/Object;

    move-object p2, p1

    check-cast p2, Ljava/util/UUID;

    iget-object p1, v6, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->L$1:Ljava/lang/Object;

    check-cast p1, Ljava/util/UUID;

    iget-object p1, v6, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->L$0:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/blecoroutines/RealConnection;

    invoke-static {p4}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_1

    .line 111
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 95
    :cond_2
    invoke-static {p4}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 101
    invoke-virtual {p0, p1, p2}, Lcom/squareup/blecoroutines/RealConnection;->findCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p4

    .line 102
    iget-object v1, p0, Lcom/squareup/blecoroutines/RealConnection;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v1, p4, v2}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 107
    invoke-static {}, Lcom/squareup/blecoroutines/RealConnectionKt;->access$getENABLE_INDICATION_VALUE$p()[B

    move-result-object v5

    iput-object p0, v6, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->L$0:Ljava/lang/Object;

    iput-object p1, v6, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->L$1:Ljava/lang/Object;

    iput-object p2, v6, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->L$2:Ljava/lang/Object;

    iput-object p3, v6, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->L$3:Ljava/lang/Object;

    iput-object p4, v6, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->L$4:Ljava/lang/Object;

    iput v2, v6, Lcom/squareup/blecoroutines/RealConnection$subscribeToCharacteristic$1;->label:I

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/blecoroutines/RealConnection;->writeDescriptor(Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;[BLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_3

    return-object v0

    :cond_3
    move-object p1, p0

    :goto_1
    const p3, 0x7fffffff

    .line 109
    invoke-static {p3}, Lkotlinx/coroutines/channels/ChannelKt;->Channel(I)Lkotlinx/coroutines/channels/Channel;

    move-result-object p3

    .line 110
    iget-object p1, p1, Lcom/squareup/blecoroutines/RealConnection;->gattCallbackBroker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    move-object p4, p3

    check-cast p4, Lkotlinx/coroutines/channels/SendChannel;

    invoke-virtual {p1, p2, p4}, Lcom/squareup/blecoroutines/GattCallbackBroker;->addSubscription(Ljava/util/UUID;Lkotlinx/coroutines/channels/SendChannel;)V

    return-object p3

    .line 103
    :cond_4
    new-instance p1, Lcom/squareup/blecoroutines/BleError;

    sget-object v1, Lcom/squareup/blecoroutines/Event;->CHARACTERISTIC_SUBSCRIBE_FAILED:Lcom/squareup/blecoroutines/Event;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7e

    const/4 v9, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v9}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public writeCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p2, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;

    iget v1, v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;->label:I

    const/high16 v2, -0x80000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    iget p2, v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;->label:I

    sub-int/2addr p2, v2

    iput p2, v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;-><init>(Lcom/squareup/blecoroutines/RealConnection;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object p2, v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v1

    .line 46
    iget v2, v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;->label:I

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v3, :cond_1

    iget-object p1, v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;->L$3:Ljava/lang/Object;

    check-cast p1, Lkotlin/jvm/functions/Function0;

    iget-object p1, v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;->L$2:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/blecoroutines/GattCallbackBroker;

    iget-object p1, v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;->L$1:Ljava/lang/Object;

    check-cast p1, Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object v0, v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/blecoroutines/RealConnection;

    invoke-static {p2}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_1

    .line 63
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 46
    :cond_2
    invoke-static {p2}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 47
    iget-object p2, p0, Lcom/squareup/blecoroutines/RealConnection;->gattCallbackBroker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    new-instance v2, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$result$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$result$1;-><init>(Lcom/squareup/blecoroutines/RealConnection;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 167
    const-class v4, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicWrite;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    iput-object p0, v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;->L$0:Ljava/lang/Object;

    iput-object p1, v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;->L$1:Ljava/lang/Object;

    iput-object p2, v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;->L$2:Ljava/lang/Object;

    iput-object v2, v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;->L$3:Ljava/lang/Object;

    iput v3, v0, Lcom/squareup/blecoroutines/RealConnection$writeCharacteristic$1;->label:I

    invoke-virtual {p2, v4, v2, v0}, Lcom/squareup/blecoroutines/GattCallbackBroker;->runAndWaitForCallback(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p2

    if-ne p2, v1, :cond_3

    return-object v1

    .line 47
    :cond_3
    :goto_1
    check-cast p2, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicWrite;

    .line 57
    invoke-virtual {p2}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicWrite;->getStatus()I

    move-result v0

    if-nez v0, :cond_4

    .line 63
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1

    .line 58
    :cond_4
    new-instance v10, Lcom/squareup/blecoroutines/BleError;

    .line 59
    sget-object v1, Lcom/squareup/blecoroutines/Event;->CHARACTERISTIC_WRITE_FAILED:Lcom/squareup/blecoroutines/Event;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getService()Landroid/bluetooth/BluetoothGattService;

    move-result-object v0

    const-string v2, "characteristic.service"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v2

    .line 60
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p2}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicWrite;->getStatus()I

    move-result p1

    invoke-static {p1}, Lkotlin/coroutines/jvm/internal/Boxing;->boxInt(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x68

    const/4 v9, 0x0

    move-object v0, v10

    .line 58
    invoke-direct/range {v0 .. v9}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v10, Ljava/lang/Throwable;

    throw v10
.end method

.method final synthetic writeDescriptor(Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;[BLkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/UUID;",
            "Ljava/util/UUID;",
            "Ljava/util/UUID;",
            "[B",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v7, p4

    move-object/from16 v0, p5

    instance-of v1, v0, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;

    iget v2, v1, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->label:I

    const/high16 v3, -0x80000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    iget v0, v1, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->label:I

    sub-int/2addr v0, v3

    iput v0, v1, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;

    invoke-direct {v1, v6, v0}, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;-><init>(Lcom/squareup/blecoroutines/RealConnection;Lkotlin/coroutines/Continuation;)V

    :goto_0
    move-object v8, v1

    iget-object v0, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v9

    .line 114
    iget v1, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->label:I

    const/4 v10, 0x1

    if-eqz v1, :cond_2

    if-ne v1, v10, :cond_1

    iget-object v1, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$7:Ljava/lang/Object;

    check-cast v1, Lkotlin/jvm/functions/Function0;

    iget-object v1, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$6:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/blecoroutines/GattCallbackBroker;

    iget-object v1, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$5:Ljava/lang/Object;

    check-cast v1, Landroid/bluetooth/BluetoothGattDescriptor;

    iget-object v1, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$4:Ljava/lang/Object;

    check-cast v1, [B

    iget-object v1, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$3:Ljava/lang/Object;

    check-cast v1, Ljava/util/UUID;

    iget-object v2, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$2:Ljava/lang/Object;

    check-cast v2, Ljava/util/UUID;

    iget-object v3, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$1:Ljava/lang/Object;

    check-cast v3, Ljava/util/UUID;

    iget-object v4, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$0:Ljava/lang/Object;

    check-cast v4, Lcom/squareup/blecoroutines/RealConnection;

    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_1

    .line 138
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_2
    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 120
    invoke-direct/range {p0 .. p3}, Lcom/squareup/blecoroutines/RealConnection;->findDescriptor(Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v11

    .line 121
    invoke-virtual {v11, v7}, Landroid/bluetooth/BluetoothGattDescriptor;->setValue([B)Z

    .line 122
    iget-object v12, v6, Lcom/squareup/blecoroutines/RealConnection;->gattCallbackBroker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    new-instance v13, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$result$1;

    move-object v0, v13

    move-object/from16 v1, p0

    move-object v2, v11

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$result$1;-><init>(Lcom/squareup/blecoroutines/RealConnection;Landroid/bluetooth/BluetoothGattDescriptor;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;)V

    check-cast v13, Lkotlin/jvm/functions/Function0;

    .line 170
    const-class v0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnDescriptorWrite;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    iput-object v6, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$0:Ljava/lang/Object;

    iput-object v3, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$1:Ljava/lang/Object;

    move-object/from16 v2, p2

    iput-object v2, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$2:Ljava/lang/Object;

    move-object/from16 v1, p3

    iput-object v1, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$3:Ljava/lang/Object;

    iput-object v7, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$4:Ljava/lang/Object;

    iput-object v11, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$5:Ljava/lang/Object;

    iput-object v12, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$6:Ljava/lang/Object;

    iput-object v13, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->L$7:Ljava/lang/Object;

    iput v10, v8, Lcom/squareup/blecoroutines/RealConnection$writeDescriptor$1;->label:I

    invoke-virtual {v12, v0, v13, v8}, Lcom/squareup/blecoroutines/GattCallbackBroker;->runAndWaitForCallback(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, v9, :cond_3

    return-object v9

    :cond_3
    :goto_1
    move-object v11, v1

    move-object v10, v2

    move-object v9, v3

    .line 122
    check-cast v0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnDescriptorWrite;

    .line 131
    invoke-virtual {v0}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnDescriptorWrite;->getStatus()I

    move-result v1

    if-nez v1, :cond_4

    .line 138
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0

    .line 132
    :cond_4
    new-instance v1, Lcom/squareup/blecoroutines/BleError;

    .line 133
    sget-object v8, Lcom/squareup/blecoroutines/Event;->DESCRIPTOR_WRITE_FAILED:Lcom/squareup/blecoroutines/Event;

    .line 135
    invoke-virtual {v0}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnDescriptorWrite;->getStatus()I

    move-result v0

    invoke-static {v0}, Lkotlin/coroutines/jvm/internal/Boxing;->boxInt(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x60

    const/16 v16, 0x0

    move-object v7, v1

    .line 132
    invoke-direct/range {v7 .. v16}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method
