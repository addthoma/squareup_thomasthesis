.class public interface abstract Lcom/squareup/SposReleaseAppComponent;
.super Ljava/lang/Object;
.source "SposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/PosReleaseAppComponent;
.implements Lcom/squareup/firebase/fcm/FcmComponent;
.implements Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheetComponent;


# annotations
.annotation runtime Ldagger/Component;
    modules = {
        Lcom/squareup/SposReleaseAppComponentModule;
    }
.end annotation


# virtual methods
.method public abstract loggedInComponent()Lcom/squareup/SposReleaseLoggedInComponent;
.end method

.method public abstract loggedOutActivityComponent()Lcom/squareup/ui/root/SposReleaseLoggedOutActivityComponent;
.end method
