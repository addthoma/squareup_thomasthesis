.class public final Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;
.super Ljava/lang/Object;
.source "UpdatePreferencesTask_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/account/UpdatePreferencesTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final pendingPreferencesCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PendingPreferencesCache;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesInProgressProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;>;"
        }
    .end annotation
.end field

.field private final preferencesOnDeckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;>;"
        }
    .end annotation
.end field

.field private final quietServerPreferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/QuietServerPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/QuietServerPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PendingPreferencesCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;>;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;->quietServerPreferencesProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;->pendingPreferencesCacheProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;->preferencesInProgressProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;->preferencesOnDeckProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/QuietServerPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PendingPreferencesCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;>;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/account/UpdatePreferencesTask;",
            ">;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPendingPreferencesCache(Lcom/squareup/account/UpdatePreferencesTask;Lcom/squareup/account/PendingPreferencesCache;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/account/UpdatePreferencesTask;->pendingPreferencesCache:Lcom/squareup/account/PendingPreferencesCache;

    return-void
.end method

.method public static injectPreferencesInProgress(Lcom/squareup/account/UpdatePreferencesTask;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/UpdatePreferencesTask;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;)V"
        }
    .end annotation

    .line 72
    iput-object p1, p0, Lcom/squareup/account/UpdatePreferencesTask;->preferencesInProgress:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public static injectPreferencesOnDeck(Lcom/squareup/account/UpdatePreferencesTask;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/UpdatePreferencesTask;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;)V"
        }
    .end annotation

    .line 79
    iput-object p1, p0, Lcom/squareup/account/UpdatePreferencesTask;->preferencesOnDeck:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public static injectQuietServerPreferences(Lcom/squareup/account/UpdatePreferencesTask;Lcom/squareup/accountstatus/QuietServerPreferences;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/squareup/account/UpdatePreferencesTask;->quietServerPreferences:Lcom/squareup/accountstatus/QuietServerPreferences;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/account/UpdatePreferencesTask;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;->quietServerPreferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/QuietServerPreferences;

    invoke-static {p1, v0}, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;->injectQuietServerPreferences(Lcom/squareup/account/UpdatePreferencesTask;Lcom/squareup/accountstatus/QuietServerPreferences;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;->pendingPreferencesCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/PendingPreferencesCache;

    invoke-static {p1, v0}, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;->injectPendingPreferencesCache(Lcom/squareup/account/UpdatePreferencesTask;Lcom/squareup/account/PendingPreferencesCache;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;->preferencesInProgressProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;->injectPreferencesInProgress(Lcom/squareup/account/UpdatePreferencesTask;Lcom/squareup/settings/LocalSetting;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;->preferencesOnDeckProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;->injectPreferencesOnDeck(Lcom/squareup/account/UpdatePreferencesTask;Lcom/squareup/settings/LocalSetting;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/account/UpdatePreferencesTask;

    invoke-virtual {p0, p1}, Lcom/squareup/account/UpdatePreferencesTask_MembersInjector;->injectMembers(Lcom/squareup/account/UpdatePreferencesTask;)V

    return-void
.end method
