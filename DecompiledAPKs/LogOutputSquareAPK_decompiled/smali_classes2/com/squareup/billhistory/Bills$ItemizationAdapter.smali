.class Lcom/squareup/billhistory/Bills$ItemizationAdapter;
.super Ljava/lang/Object;
.source "Bills.java"

# interfaces
.implements Lcom/squareup/billhistory/Bills$BillItemAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/Bills;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ItemizationAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/billhistory/Bills$BillItemAdapter<",
        "Lcom/squareup/server/payment/Itemization;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/billhistory/Bills$1;)V
    .locals 0

    .line 741
    invoke-direct {p0}, Lcom/squareup/billhistory/Bills$ItemizationAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getName(Lcom/squareup/server/payment/Itemization;)Ljava/lang/String;
    .locals 0

    .line 743
    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getItemName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getName(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 741
    check-cast p1, Lcom/squareup/server/payment/Itemization;

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/Bills$ItemizationAdapter;->getName(Lcom/squareup/server/payment/Itemization;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNotes(Lcom/squareup/server/payment/Itemization;)Ljava/lang/String;
    .locals 0

    .line 747
    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getNotes()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getNotes(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 741
    check-cast p1, Lcom/squareup/server/payment/Itemization;

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/Bills$ItemizationAdapter;->getNotes(Lcom/squareup/server/payment/Itemization;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getQuantity(Lcom/squareup/server/payment/Itemization;)Ljava/math/BigDecimal;
    .locals 0

    .line 751
    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getItemQuantity()Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getQuantity(Ljava/lang/Object;)Ljava/math/BigDecimal;
    .locals 0

    .line 741
    check-cast p1, Lcom/squareup/server/payment/Itemization;

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/Bills$ItemizationAdapter;->getQuantity(Lcom/squareup/server/payment/Itemization;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method

.method public getQuantityPrecision(Lcom/squareup/util/Res;Lcom/squareup/server/payment/Itemization;)I
    .locals 0

    .line 755
    invoke-virtual {p2}, Lcom/squareup/server/payment/Itemization;->getQuantityPrecision()I

    move-result p1

    return p1
.end method

.method public bridge synthetic getQuantityPrecision(Lcom/squareup/util/Res;Ljava/lang/Object;)I
    .locals 0

    .line 741
    check-cast p2, Lcom/squareup/server/payment/Itemization;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/billhistory/Bills$ItemizationAdapter;->getQuantityPrecision(Lcom/squareup/util/Res;Lcom/squareup/server/payment/Itemization;)I

    move-result p1

    return p1
.end method

.method public getUnitAbbreviation(Lcom/squareup/util/Res;Lcom/squareup/server/payment/Itemization;)Ljava/lang/String;
    .locals 0

    .line 764
    invoke-virtual {p2}, Lcom/squareup/server/payment/Itemization;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getUnitAbbreviation(Lcom/squareup/util/Res;Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 741
    check-cast p2, Lcom/squareup/server/payment/Itemization;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/billhistory/Bills$ItemizationAdapter;->getUnitAbbreviation(Lcom/squareup/util/Res;Lcom/squareup/server/payment/Itemization;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public isCustomAmount(Lcom/squareup/server/payment/Itemization;)Z
    .locals 0

    .line 768
    iget-object p1, p1, Lcom/squareup/server/payment/Itemization;->item_id:Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic isCustomAmount(Ljava/lang/Object;)Z
    .locals 0

    .line 741
    check-cast p1, Lcom/squareup/server/payment/Itemization;

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/Bills$ItemizationAdapter;->isCustomAmount(Lcom/squareup/server/payment/Itemization;)Z

    move-result p1

    return p1
.end method

.method public isUnitPriced(Lcom/squareup/util/Res;Lcom/squareup/server/payment/Itemization;)Z
    .locals 1

    .line 759
    invoke-virtual {p2}, Lcom/squareup/server/payment/Itemization;->getUnitName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/server/payment/Itemization;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v0

    .line 760
    invoke-virtual {p2}, Lcom/squareup/server/payment/Itemization;->getQuantityPrecision()I

    move-result p2

    .line 759
    invoke-static {p1, v0, p2}, Lcom/squareup/quantity/UnitDisplayData;->of(Ljava/lang/String;Ljava/lang/String;I)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p1

    .line 760
    invoke-virtual {p1}, Lcom/squareup/quantity/UnitDisplayData;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public bridge synthetic isUnitPriced(Lcom/squareup/util/Res;Ljava/lang/Object;)Z
    .locals 0

    .line 741
    check-cast p2, Lcom/squareup/server/payment/Itemization;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/billhistory/Bills$ItemizationAdapter;->isUnitPriced(Lcom/squareup/util/Res;Lcom/squareup/server/payment/Itemization;)Z

    move-result p1

    return p1
.end method
