.class public Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;
.super Lcom/squareup/billhistory/model/TenderHistory$Builder;
.source "AdjustmentTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/model/AdjustmentTenderHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/billhistory/model/TenderHistory$Builder<",
        "Lcom/squareup/billhistory/model/AdjustmentTenderHistory;",
        "Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 8
    sget-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->ADJUSTMENT:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-direct {p0, v0}, Lcom/squareup/billhistory/model/TenderHistory$Builder;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Type;)V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/billhistory/model/AdjustmentTenderHistory;
    .locals 2

    .line 12
    new-instance v0, Lcom/squareup/billhistory/model/AdjustmentTenderHistory;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/billhistory/model/AdjustmentTenderHistory;-><init>(Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;Lcom/squareup/billhistory/model/AdjustmentTenderHistory$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/billhistory/model/TenderHistory;
    .locals 1

    .line 5
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/AdjustmentTenderHistory;

    move-result-object v0

    return-object v0
.end method
