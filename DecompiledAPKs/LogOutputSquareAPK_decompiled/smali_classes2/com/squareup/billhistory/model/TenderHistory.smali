.class public abstract Lcom/squareup/billhistory/model/TenderHistory;
.super Ljava/lang/Object;
.source "TenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/billhistory/model/TenderHistory$Builder;,
        Lcom/squareup/billhistory/model/TenderHistory$Type;
    }
.end annotation


# static fields
.field private static final RECEIPT_NUMBER_LENGTH:I = 0x4


# instance fields
.field public final amount:Lcom/squareup/protos/common/Money;

.field protected final autoGratuityAmount:Lcom/squareup/protos/common/Money;

.field public final billId:Lcom/squareup/protos/client/IdPair;

.field public final clientDetails:Lcom/squareup/protos/client/bills/ClientDetails;

.field public final completeTenderDetails:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

.field public final contactToken:Ljava/lang/String;

.field public final employeeToken:Ljava/lang/String;

.field public final id:Ljava/lang/String;

.field public final lastRefundableAt:Ljava/util/Date;

.field public final receiptNumber:Ljava/lang/String;

.field public final refundCardPresenceRequirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

.field public final sequentialTenderNumber:Ljava/lang/String;

.field public final tenderState:Lcom/squareup/protos/client/bills/Tender$State;

.field public final timestamp:Ljava/util/Date;

.field public final type:Lcom/squareup/billhistory/model/TenderHistory$Type;


# direct methods
.method protected constructor <init>(Lcom/squareup/billhistory/model/TenderHistory$Builder;)V
    .locals 1

    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 258
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$000(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    .line 259
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$100(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->billId:Lcom/squareup/protos/client/IdPair;

    .line 260
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$200(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->receiptNumber:Ljava/lang/String;

    .line 261
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$300(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->employeeToken:Ljava/lang/String;

    .line 262
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$400(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    .line 263
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$500(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    .line 264
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$600(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->autoGratuityAmount:Lcom/squareup/protos/common/Money;

    .line 265
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$700(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->completeTenderDetails:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    .line 266
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$800(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Dates;->copy(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->timestamp:Ljava/util/Date;

    .line 267
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$900(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->contactToken:Ljava/lang/String;

    .line 268
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$1000(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Dates;->copy(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->lastRefundableAt:Ljava/util/Date;

    .line 269
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$1100(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->refundCardPresenceRequirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    .line 270
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$1200(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/protos/client/bills/Tender$State;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->tenderState:Lcom/squareup/protos/client/bills/Tender$State;

    .line 271
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$1300(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->sequentialTenderNumber:Ljava/lang/String;

    .line 272
    invoke-static {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->access$1400(Lcom/squareup/billhistory/model/TenderHistory$Builder;)Lcom/squareup/protos/client/bills/ClientDetails;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistory;->clientDetails:Lcom/squareup/protos/client/bills/ClientDetails;

    return-void
.end method

.method public static fromHistoricalTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/TenderHistory;
    .locals 3

    .line 523
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_percentage:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 525
    :try_start_0
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_percentage:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Percentage;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 527
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to parse tip percentage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_percentage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    .line 531
    :goto_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-static {p0, p1, v1, v2, v0}, Lcom/squareup/billhistory/model/TenderHistory;->fromTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p0

    return-object p0
.end method

.method public static fromLegacyPaperSignatureTender(Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;Ljava/util/Locale;)Lcom/squareup/billhistory/model/TenderHistory;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 567
    new-instance v0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tender_id:Ljava/lang/String;

    .line 568
    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->id(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    new-instance v1, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->bill_id:Ljava/lang/String;

    .line 569
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/IdPair$Builder;->server_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v1

    .line 570
    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    .line 568
    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->billId(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    .line 571
    invoke-static {v1, p1}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->timestamp(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    iget-object v0, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->current_state:Lcom/squareup/protos/client/paper_signature/TenderState;

    .line 572
    invoke-static {v0}, Lcom/squareup/billhistory/model/TenderHistory;->fromTenderState(Lcom/squareup/protos/client/paper_signature/TenderState;)Lcom/squareup/protos/client/bills/Tender$State;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->tenderState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    iget-object v0, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tender_id:Ljava/lang/String;

    .line 573
    invoke-static {v0}, Lcom/squareup/billhistory/model/TenderHistory;->getReceiptNumberFromTenderId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->receiptNumber(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    iget-object v0, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->total_money:Lcom/squareup/protos/common/Money;

    .line 574
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    iget-object p0, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tip_money:Lcom/squareup/protos/common/Money;

    .line 575
    invoke-virtual {p1, p0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object p0

    .line 576
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/OtherTenderHistory;

    move-result-object p0

    return-object p0
.end method

.method public static fromTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory;
    .locals 2

    .line 537
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 538
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v1, v1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 539
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, v0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Times;->requireIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 542
    :cond_0
    invoke-static {p0, p3}, Lcom/squareup/billhistory/model/TenderHistory;->fromTenderType(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p3

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 543
    invoke-virtual {p3, v1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->id(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p3

    .line 544
    invoke-virtual {p3, p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->billId(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    iget-object p3, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    .line 545
    invoke-virtual {p1, p3}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->receiptNumber(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    .line 546
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->timestamp(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    iget-object p3, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_contact_token:Ljava/lang/String;

    .line 547
    invoke-virtual {p1, p3}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->contactToken(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    .line 548
    invoke-static {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getLastRefundableAtFromTender(Lcom/squareup/protos/client/bills/Tender;)Ljava/util/Date;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->lastRefundableAt(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    iget-object p3, p0, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/Tender$Amounts;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    .line 549
    invoke-virtual {p1, p3}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->autoGratuityAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    .line 550
    invoke-virtual {p1, p2}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    .line 551
    invoke-virtual {p1, p4}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tipPercentage(Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_state:Lcom/squareup/protos/client/bills/Tender$State;

    .line 552
    invoke-virtual {p1, p2}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tenderState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    .line 553
    invoke-virtual {p1, p2}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->completeTenderDetails(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_sequential_tender_number:Ljava/lang/String;

    .line 554
    invoke-virtual {p1, p2}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->sequentialTenderNumber(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    .line 555
    invoke-virtual {p1, p2}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->clientDetails(Lcom/squareup/protos/client/bills/ClientDetails;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    .line 557
    iget-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object p2, p2, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    if-eqz p2, :cond_1

    .line 558
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object p0, p0, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iget-object p0, p0, Lcom/squareup/protos/client/Employee;->employee_token:Ljava/lang/String;

    invoke-virtual {p1, p0}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->employeeToken(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    .line 561
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->build()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p0

    return-object p0
.end method

.method private static fromTenderState(Lcom/squareup/protos/client/paper_signature/TenderState;)Lcom/squareup/protos/client/bills/Tender$State;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 687
    :cond_0
    sget-object v1, Lcom/squareup/billhistory/model/TenderHistory$1;->$SwitchMap$com$squareup$protos$client$paper_signature$TenderState:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/TenderState;->ordinal()I

    move-result p0

    aget p0, v1, p0

    const/4 v1, 0x1

    if-eq p0, v1, :cond_2

    const/4 v1, 0x2

    if-eq p0, v1, :cond_1

    return-object v0

    .line 691
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->AWAITING_MERCHANT_TIP:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0

    .line 689
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->SETTLED:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0
.end method

.method private static fromTenderType(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 11

    .line 581
    sget-object v0, Lcom/squareup/billhistory/model/TenderHistory$1;->$SwitchMap$com$squareup$protos$client$bills$Tender$Type:[I

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    sget-object v2, Lcom/squareup/protos/client/bills/Tender$Type;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-static {v1, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    .line 676
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized tender type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 677
    new-instance p0, Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;

    invoke-direct {p0}, Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;-><init>()V

    .line 678
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p0

    return-object p0

    .line 672
    :pswitch_0
    new-instance p0, Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;

    invoke-direct {p0}, Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;-><init>()V

    .line 673
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;

    move-result-object p0

    return-object p0

    .line 668
    :pswitch_1
    new-instance p0, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;

    invoke-direct {p0}, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;-><init>()V

    .line 669
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p0

    return-object p0

    .line 660
    :pswitch_2
    new-instance v0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;-><init>()V

    .line 661
    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    .line 662
    invoke-static {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getOtherTenderName(Lcom/squareup/protos/client/bills/Tender;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->name(Ljava/lang/String;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object p1

    .line 663
    invoke-static {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getOtherTenderNote(Lcom/squareup/protos/client/bills/Tender;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->note(Ljava/lang/String;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object p1

    .line 664
    invoke-static {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getOtherTenderType(Lcom/squareup/protos/client/bills/Tender;)I

    move-result p0

    invoke-virtual {p1, p0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->tenderType(I)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object p0

    return-object p0

    .line 655
    :pswitch_3
    new-instance p0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    invoke-direct {p0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;-><init>()V

    .line 656
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p0

    check-cast p0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    sget-object p1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->CUSTOM:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 657
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->getValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->tenderType(I)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object p0

    return-object p0

    .line 650
    :pswitch_4
    new-instance p0, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;

    invoke-direct {p0}, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;-><init>()V

    .line 651
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;

    move-result-object p0

    return-object p0

    .line 646
    :pswitch_5
    new-instance p0, Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;

    invoke-direct {p0}, Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;-><init>()V

    .line 647
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p0

    return-object p0

    .line 634
    :pswitch_6
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CashTender;->amounts:Lcom/squareup/protos/client/bills/CashTender$Amounts;

    if-eqz v0, :cond_0

    .line 637
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CashTender;->amounts:Lcom/squareup/protos/client/bills/CashTender$Amounts;

    iget-object v1, v0, Lcom/squareup/protos/client/bills/CashTender$Amounts;->buyer_tendered_money:Lcom/squareup/protos/common/Money;

    .line 638
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender$Method;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/CashTender;->amounts:Lcom/squareup/protos/client/bills/CashTender$Amounts;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/CashTender$Amounts;->change_back_money:Lcom/squareup/protos/common/Money;

    goto :goto_0

    :cond_0
    move-object p0, v1

    .line 640
    :goto_0
    new-instance v0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;-><init>()V

    .line 641
    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    .line 642
    invoke-virtual {p1, v1}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->tenderedAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    move-result-object p1

    .line 643
    invoke-virtual {p1, p0}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->changeAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    move-result-object p0

    return-object p0

    .line 587
    :pswitch_7
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card;->DEFAULT_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 591
    iget-object v2, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    .line 592
    iget-object v3, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    const-string v4, ""

    if-eqz v3, :cond_5

    if-eqz v2, :cond_5

    .line 593
    iget-object v3, v2, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 594
    iget-object v5, v2, Lcom/squareup/protos/client/bills/CardTender;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    if-eqz v5, :cond_1

    .line 596
    iget-object v1, v5, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_application_preferred_name:Ljava/lang/String;

    .line 597
    iget-object v6, v5, Lcom/squareup/protos/client/bills/CardTender$Emv;->buyer_selected_account_name:Ljava/lang/String;

    .line 598
    iget-object v5, v5, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_application_id:Ljava/lang/String;

    .line 599
    iget-object v7, v2, Lcom/squareup/protos/client/bills/CardTender;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    iget-object v7, v7, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_cardholder_verification_method_used:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    goto :goto_1

    :cond_1
    move-object v5, v1

    move-object v6, v5

    move-object v7, v6

    .line 601
    :goto_1
    iget-object v8, v2, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    if-eqz v8, :cond_4

    .line 602
    iget-object v8, v2, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    .line 603
    iget-object v9, v8, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    .line 604
    iget-object v10, v8, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v10, :cond_2

    .line 605
    iget-object v0, v8, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 607
    :cond_2
    iget-object v8, v2, Lcom/squareup/protos/client/bills/CardTender;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    if-eqz v8, :cond_3

    .line 609
    iget-object v4, v2, Lcom/squareup/protos/client/bills/CardTender;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorization_code:Ljava/lang/String;

    :cond_3
    move-object v8, v7

    move-object v7, v5

    move-object v5, v1

    move-object v1, v4

    move-object v4, v9

    goto :goto_3

    :cond_4
    move-object v8, v7

    move-object v7, v5

    move-object v5, v1

    goto :goto_2

    :cond_5
    move-object v3, v1

    move-object v5, v3

    move-object v6, v5

    move-object v7, v6

    move-object v8, v7

    :goto_2
    move-object v1, v4

    .line 614
    :goto_3
    new-instance v9, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    invoke-direct {v9}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;-><init>()V

    .line 615
    invoke-virtual {v9, p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    iget-object v9, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    .line 616
    invoke-virtual {p1, v9}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->refundCardPresenceRequirement(Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    .line 617
    invoke-static {v0}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p1

    iget-object v0, v2, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 618
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->felicaBrand(Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p1

    iget-object v0, v2, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_masked_card_number:Ljava/lang/String;

    .line 619
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->felicaMaskedCardNumber(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p1

    iget-object v0, v2, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_sprwid:Ljava/lang/String;

    .line 620
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->felicaTerminalId(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p1

    .line 621
    invoke-virtual {p1, v4}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->cardSuffix(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p1

    .line 622
    invoke-static {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getBuyerName(Lcom/squareup/protos/client/bills/Tender;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->buyerName(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p1

    .line 623
    invoke-virtual {p1, v3}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->entryMethod(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p1

    .line 624
    invoke-virtual {p1, v1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->authorizationCode(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p1

    .line 625
    invoke-virtual {p1, v5}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->applicationPreferredName(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p1

    .line 626
    invoke-virtual {p1, v6}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->buyerSelectedAccountName(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p1

    .line 627
    invoke-virtual {p1, v7}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->applicationId(Ljava/lang/String;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p1

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    .line 628
    invoke-virtual {p1, p0}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p0

    .line 629
    invoke-virtual {p0, v8}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->cardholderVerificationMethod(Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object p0

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static getBuyerName(Lcom/squareup/protos/client/bills/Tender;)Ljava/lang/String;
    .locals 1

    .line 698
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    if-eqz v0, :cond_0

    .line 699
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    iget-object p0, p0, Lcom/squareup/protos/client/Buyer;->full_name:Ljava/lang/String;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static getLastRefundableAtFromTender(Lcom/squareup/protos/client/bills/Tender;)Ljava/util/Date;
    .locals 1

    .line 730
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, v0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 732
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Times;->requireIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static getOtherTenderName(Lcom/squareup/protos/client/bills/Tender;)Ljava/lang/String;
    .locals 1

    .line 705
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/OtherTender;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    if-eqz v0, :cond_0

    .line 708
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/OtherTender;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/TranslatedName;->localized_name:Ljava/lang/String;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static getOtherTenderNote(Lcom/squareup/protos/client/bills/Tender;)Ljava/lang/String;
    .locals 1

    .line 714
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    if-eqz v0, :cond_0

    .line 715
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/OtherTender;->tender_note:Ljava/lang/String;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static getOtherTenderType(Lcom/squareup/protos/client/bills/Tender;)I
    .locals 1

    .line 721
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/OtherTender;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    if-eqz v0, :cond_0

    .line 724
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/OtherTender;->other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->getValue()I

    move-result p0

    return p0

    :cond_0
    const/4 p0, -0x1

    return p0
.end method

.method private static getReceiptNumberFromTenderId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 278
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public amountExcludingTip()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 327
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-nez v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    invoke-static {v1, v0}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method protected amountWithTip(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 1

    .line 518
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->amountExcludingTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public abstract buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;
.end method

.method public getBrandedTenderGlyphGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 335
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getUnbrandedTenderGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    return-object v0
.end method

.method public getBuyerSelectedLocale(Ljava/util/Locale;)Ljava/util/Locale;
    .locals 2

    .line 505
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 507
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_selected_locale:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 506
    invoke-static {v0, p1}, Lcom/squareup/util/LocaleHelper;->getLocaleFromDelimitedString(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public getClientCalculatedTipOptions()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation

    .line 451
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 452
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    if-eqz v1, :cond_0

    .line 453
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    iget-object v0, v0, Lcom/squareup/protos/common/tipping/TippingPreferences;->client_calculated_tip_option:Ljava/util/List;

    return-object v0

    .line 455
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDescription(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 0

    .line 364
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory;->getTypeName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getImageResId()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getNote()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;
    .locals 1

    .line 477
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->completeTenderDetails:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->receipt_display_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 374
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    iget v0, v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->buyerFacingResourceId:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getRemainingBalance()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 484
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->completeTenderDetails:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->receipt_display_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->completeTenderDetails:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->receipt_display_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRemainingBalanceMinusTip()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 491
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getRemainingBalance()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 493
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getTipPercentage()Lcom/squareup/util/Percentage;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTypeName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 359
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    iget v0, v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->resourceId:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUnbrandedTenderGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 343
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-static {v0}, Lcom/squareup/billhistory/model/TenderHistory$Type;->access$1500(Lcom/squareup/billhistory/model/TenderHistory$Type;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    return-object v0
.end method

.method public getUppercaseDescription(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 0

    .line 369
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory;->getTypeName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public hasAutoGratuity()Z
    .locals 1

    .line 514
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->autoGratuityAmount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasClientCalculatedTipOptions()Z
    .locals 2

    .line 441
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 442
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    iget-object v0, v0, Lcom/squareup/protos/common/tipping/TippingPreferences;->client_calculated_tip_option:Ljava/util/List;

    .line 444
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasReceiptDisplayDetails()Z
    .locals 1

    .line 397
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isPastRefundDate(J)Z
    .locals 3

    .line 378
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->lastRefundableAt:Ljava/util/Date;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isRefundCardPresenceRequired()Z
    .locals 2

    .line 382
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->refundCardPresenceRequirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 385
    :cond_0
    sget-object v1, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->REFUND_CARD_PRESENCE_REQUIRED:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isUnsettledCardTender()Z
    .locals 2

    .line 393
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->tenderState:Lcom/squareup/protos/client/bills/Tender$State;

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$State;->AWAITING_MERCHANT_TIP:Lcom/squareup/protos/client/bills/Tender$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    sget-object v1, Lcom/squareup/billhistory/model/TenderHistory$Type;->CARD:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public shouldDisplayQuickTipOptions()Z
    .locals 2

    .line 427
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_quick_tip_options:Ljava/lang/Boolean;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 429
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "TenderHistory does not have a ReceiptDisplayDetails"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public shouldPrintSignatureLine()Z
    .locals 2

    .line 464
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->isUnsettledCardTender()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 468
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 470
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_signature_line_on_paper_receipt:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_1
    return v1
.end method

.method public shouldPrintTipEntryInput()Z
    .locals 2

    .line 409
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->isUnsettledCardTender()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 413
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 415
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_tip_options_on_paper_receipt:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public tip()Lcom/squareup/protos/common/Money;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public withSettledTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory;
    .locals 2

    .line 300
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory;->amountWithTip(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    .line 301
    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    .line 302
    invoke-virtual {p1, p2}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tipPercentage(Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/protos/client/bills/Tender$State;->SETTLED:Lcom/squareup/protos/client/bills/Tender$State;

    .line 303
    invoke-virtual {p1, p2}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tenderState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    .line 304
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->build()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p1

    return-object p1
.end method

.method public withTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory;
    .locals 2

    .line 312
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory;->amountWithTip(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    .line 313
    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    .line 314
    invoke-virtual {p1, p2}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tipPercentage(Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    .line 315
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->build()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p1

    return-object p1
.end method

.method public withoutTip()Lcom/squareup/billhistory/model/TenderHistory;
    .locals 1

    const/4 v0, 0x0

    .line 322
    invoke-virtual {p0, v0, v0}, Lcom/squareup/billhistory/model/TenderHistory;->withTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v0

    return-object v0
.end method
