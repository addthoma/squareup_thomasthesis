.class public interface abstract Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;
.super Ljava/lang/Object;
.source "AbstractActivityCardPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/AbstractActivityCardPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeIssueReceiptScreen()V
.end method

.method public abstract closeIssueRefundScreen()V
.end method

.method public abstract closeIssueRefundScreen(Lcom/squareup/billhistory/model/BillHistory;)V
.end method

.method public abstract getBill()Lcom/squareup/billhistory/model/BillHistory;
.end method

.method public abstract getPaymentIdForReceipt()Ljava/lang/String;
.end method

.method public abstract getTender()Lcom/squareup/billhistory/model/TenderHistory;
.end method

.method public abstract goBack()V
.end method

.method public abstract goBackBecauseBillIdChanged()V
.end method

.method public abstract onBillIdChanged()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onTransactionsHistoryChanged()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract printGiftReceiptForSelectedTender(Ljava/lang/String;)V
.end method

.method public abstract showIssueReceiptScreen(Ljava/lang/String;)V
.end method

.method public abstract showSelectBuyerLanguageScreen()V
.end method

.method public abstract showStartRefundScreen(Ljava/lang/String;)V
.end method
