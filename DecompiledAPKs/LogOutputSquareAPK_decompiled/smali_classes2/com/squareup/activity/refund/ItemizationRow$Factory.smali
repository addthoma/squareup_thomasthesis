.class public final Lcom/squareup/activity/refund/ItemizationRow$Factory;
.super Ljava/lang/Object;
.source "ItemizationRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/ItemizationRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u0011R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/activity/refund/ItemizationRow$Factory;",
        "",
        "parent",
        "Landroid/widget/LinearLayout;",
        "formatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Landroid/widget/LinearLayout;Lcom/squareup/text/Formatter;)V",
        "getFormatter",
        "()Lcom/squareup/text/Formatter;",
        "getParent",
        "()Landroid/widget/LinearLayout;",
        "newRow",
        "Lcom/squareup/activity/refund/ItemizationRow;",
        "details",
        "Lcom/squareup/activity/refund/ItemizationDetails;",
        "hide_amount",
        "",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final formatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final parent:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/widget/LinearLayout;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/LinearLayout;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/ItemizationRow$Factory;->parent:Landroid/widget/LinearLayout;

    iput-object p2, p0, Lcom/squareup/activity/refund/ItemizationRow$Factory;->formatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static synthetic newRow$default(Lcom/squareup/activity/refund/ItemizationRow$Factory;Lcom/squareup/activity/refund/ItemizationDetails;ZILjava/lang/Object;)Lcom/squareup/activity/refund/ItemizationRow;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 23
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/activity/refund/ItemizationRow$Factory;->newRow(Lcom/squareup/activity/refund/ItemizationDetails;Z)Lcom/squareup/activity/refund/ItemizationRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final getFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationRow$Factory;->formatter:Lcom/squareup/text/Formatter;

    return-object v0
.end method

.method public final getParent()Landroid/widget/LinearLayout;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationRow$Factory;->parent:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final newRow(Lcom/squareup/activity/refund/ItemizationDetails;Z)Lcom/squareup/activity/refund/ItemizationRow;
    .locals 3

    const-string v0, "details"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lcom/squareup/activity/refund/ItemizationRow;

    iget-object v1, p0, Lcom/squareup/activity/refund/ItemizationRow$Factory;->parent:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/squareup/activity/refund/ItemizationRow$Factory;->formatter:Lcom/squareup/text/Formatter;

    invoke-direct {v0, v1, v2}, Lcom/squareup/activity/refund/ItemizationRow;-><init>(Landroid/widget/LinearLayout;Lcom/squareup/text/Formatter;)V

    .line 26
    invoke-static {v0}, Lcom/squareup/activity/refund/ItemizationRow;->access$getNameField$p(Lcom/squareup/activity/refund/ItemizationRow;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/activity/refund/ItemizationDetails;->getName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 27
    invoke-static {v0}, Lcom/squareup/activity/refund/ItemizationRow;->access$getNoteField$p(Lcom/squareup/activity/refund/ItemizationRow;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/activity/refund/ItemizationDetails;->getNote()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    invoke-static {v0}, Lcom/squareup/activity/refund/ItemizationRow;->access$getAmountField$p(Lcom/squareup/activity/refund/ItemizationRow;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x1

    if-ne p2, v2, :cond_0

    const-string p1, ""

    .line 29
    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_0

    :cond_0
    if-nez p2, :cond_1

    .line 30
    invoke-virtual {v0}, Lcom/squareup/activity/refund/ItemizationRow;->getFormatter()Lcom/squareup/text/Formatter;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/activity/refund/ItemizationDetails;->getTotalMoney()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 28
    :goto_0
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v0

    .line 30
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
