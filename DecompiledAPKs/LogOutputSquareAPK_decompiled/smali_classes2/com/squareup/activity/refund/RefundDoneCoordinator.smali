.class public final Lcom/squareup/activity/refund/RefundDoneCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "RefundDoneCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/refund/RefundDoneCoordinator$RefundDoneHandler;,
        Lcom/squareup/activity/refund/RefundDoneCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRefundDoneCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RefundDoneCoordinator.kt\ncom/squareup/activity/refund/RefundDoneCoordinator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,162:1\n1360#2:163\n1429#2,3:164\n704#2:167\n777#2,2:168\n*E\n*S KotlinDebug\n*F\n+ 1 RefundDoneCoordinator.kt\ncom/squareup/activity/refund/RefundDoneCoordinator\n*L\n83#1:163\n83#1,3:164\n91#1:167\n91#1,2:168\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010 \n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0002$%B1\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0008\u0010\u0016\u001a\u00020\u0012H\u0002J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0010\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0018\u0010\u001c\u001a\u00020\u00182\u0006\u0010\u0004\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u0018H\u0002J\u0010\u0010\u001e\u001a\u00020\u00182\u0006\u0010\u001f\u001a\u00020\u0018H\u0002J\u0016\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u001a0!2\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0010\u0010\"\u001a\u00020\u00122\u0006\u0010\u0004\u001a\u00020\u0006H\u0002J\u0010\u0010#\u001a\u00020\u00122\u0006\u0010\u0004\u001a\u00020\u0006H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundDoneCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "data",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/activity/refund/RefundData;",
        "eventHandler",
        "Lcom/squareup/activity/refund/RefundDoneCoordinator$RefundDoneHandler;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/util/Res;Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundDoneCoordinator$RefundDoneHandler;Lcom/squareup/text/Formatter;)V",
        "actionBarView",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "doneGlyphView",
        "Lcom/squareup/marin/widgets/MarinGlyphMessage;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "configureActionBar",
        "defaultMessage",
        "",
        "tenderDetails",
        "Lcom/squareup/activity/refund/TenderDetails;",
        "formatTenderForDetails",
        "getFormattedRefundToGiftCardNameOrDefault",
        "default",
        "getLastFourForEmoney",
        "string",
        "tendersWithRefunds",
        "",
        "updateTitleAndMessage",
        "updateView",
        "Factory",
        "RefundDoneHandler",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private final data:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;"
        }
    .end annotation
.end field

.field private doneGlyphView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private final eventHandler:Lcom/squareup/activity/refund/RefundDoneCoordinator$RefundDoneHandler;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundDoneCoordinator$RefundDoneHandler;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;",
            "Lcom/squareup/activity/refund/RefundDoneCoordinator$RefundDoneHandler;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->data:Lio/reactivex/Observable;

    iput-object p3, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->eventHandler:Lcom/squareup/activity/refund/RefundDoneCoordinator$RefundDoneHandler;

    iput-object p4, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static final synthetic access$updateView(Lcom/squareup/activity/refund/RefundDoneCoordinator;Lcom/squareup/activity/refund/RefundData;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundDoneCoordinator;->updateView(Lcom/squareup/activity/refund/RefundData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 63
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 64
    sget v0, Lcom/squareup/activity/R$id;->activity_applet_issue_refund_view_glyph_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->doneGlyphView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 65
    invoke-direct {p0}, Lcom/squareup/activity/refund/RefundDoneCoordinator;->configureActionBar()V

    return-void
.end method

.method private final configureActionBar()V
    .locals 5

    .line 108
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBarView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBarView.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 104
    new-instance v2, Lcom/squareup/activity/refund/RefundDoneCoordinator$configureActionBar$1;

    iget-object v3, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->eventHandler:Lcom/squareup/activity/refund/RefundDoneCoordinator$RefundDoneHandler;

    invoke-direct {v2, v3}, Lcom/squareup/activity/refund/RefundDoneCoordinator$configureActionBar$1;-><init>(Lcom/squareup/activity/refund/RefundDoneCoordinator$RefundDoneHandler;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    new-instance v3, Lcom/squareup/activity/refund/RefundDoneCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v3, v2}, Lcom/squareup/activity/refund/RefundDoneCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 105
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/activity/R$string;->refund_complete:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 106
    iget-object v2, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->done:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 107
    new-instance v2, Lcom/squareup/activity/refund/RefundDoneCoordinator$configureActionBar$2;

    iget-object v3, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->eventHandler:Lcom/squareup/activity/refund/RefundDoneCoordinator$RefundDoneHandler;

    invoke-direct {v2, v3}, Lcom/squareup/activity/refund/RefundDoneCoordinator$configureActionBar$2;-><init>(Lcom/squareup/activity/refund/RefundDoneCoordinator$RefundDoneHandler;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    new-instance v3, Lcom/squareup/activity/refund/RefundDoneCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v3, v2}, Lcom/squareup/activity/refund/RefundDoneCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 108
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final defaultMessage(Lcom/squareup/activity/refund/TenderDetails;)Ljava/lang/CharSequence;
    .locals 3

    .line 112
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getCardBrand()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/squareup/Card$Brand;->FELICA:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->getHumanName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 113
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getFelicaBrand()Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    move-result-object v0

    sget-object v1, Lcom/squareup/activity/refund/RefundDoneCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    const-string v2, "last_four"

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 125
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getName()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/activity/R$string;->refund_felica_id:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 121
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getName()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundDoneCoordinator;->getLastFourForEmoney(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 122
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/activity/R$string;->refund_felica_quicpay:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 116
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getName()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundDoneCoordinator;->getLastFourForEmoney(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 117
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    :goto_0
    const-string/jumbo v0, "when (tenderDetails.feli\u2026ls.name\n        }\n      }"

    .line 113
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 129
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getName()Ljava/lang/CharSequence;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method private final formatTenderForDetails(Lcom/squareup/activity/refund/TenderDetails;)Ljava/lang/CharSequence;
    .locals 3

    .line 94
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getType()Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object v0

    sget-object v1, Lcom/squareup/activity/refund/RefundDoneCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/TenderHistory$Type;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 99
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getName()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/activity/R$string;->refund_tender_type_and_amount:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 96
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getName()Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "tender_type"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v1, "amount"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 98
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "Phrase.from(res.getStrin\u2026ney))\n          .format()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method private final getFormattedRefundToGiftCardNameOrDefault(Lcom/squareup/activity/refund/RefundData;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 1

    .line 153
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getDestinationGiftCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 155
    iget-object p2, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->res:Lcom/squareup/util/Res;

    sget-object v0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    .line 156
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getDestinationGiftCard()Lcom/squareup/Card;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object p1

    .line 154
    invoke-static {p2, v0, p1}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;Lcom/squareup/Card$Brand;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "formattedBrandAndUnmaske\u2026rd.unmaskedDigits\n      )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object p2, p1

    check-cast p2, Ljava/lang/CharSequence;

    :cond_0
    return-object p2
.end method

.method private final getLastFourForEmoney(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .line 137
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v1, v0, -0x4

    .line 138
    invoke-interface {p1, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private final tendersWithRefunds(Lcom/squareup/activity/refund/RefundData;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/activity/refund/RefundData;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/TenderDetails;",
            ">;"
        }
    .end annotation

    .line 91
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getTenderDetails()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 167
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 168
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/activity/refund/TenderDetails;

    .line 91
    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-gtz v7, :cond_2

    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getType()Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object v2

    sget-object v3, Lcom/squareup/billhistory/model/TenderHistory$Type;->NO_SALE:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-ne v2, v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 169
    :cond_3
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final updateTitleAndMessage(Lcom/squareup/activity/refund/RefundData;)V
    .locals 4

    .line 74
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/activity/R$string;->refunded_amount:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 75
    iget-object v1, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getDisplayRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "amount"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundDoneCoordinator;->tendersWithRefunds(Lcom/squareup/activity/refund/RefundData;)Ljava/util/List;

    move-result-object v1

    .line 79
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 82
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/utilities/R$string;->list_pattern_long_nonfinal_separator:I

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object p1

    .line 83
    check-cast v1, Ljava/lang/Iterable;

    .line 163
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 164
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 165
    check-cast v3, Lcom/squareup/activity/refund/TenderDetails;

    .line 83
    invoke-direct {p0, v3}, Lcom/squareup/activity/refund/RefundDoneCoordinator;->formatTenderForDetails(Lcom/squareup/activity/refund/TenderDetails;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 166
    :cond_0
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 83
    invoke-virtual {p1, v2}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    .line 80
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/TenderDetails;

    invoke-direct {p0, v1}, Lcom/squareup/activity/refund/RefundDoneCoordinator;->defaultMessage(Lcom/squareup/activity/refund/TenderDetails;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/squareup/activity/refund/RefundDoneCoordinator;->getFormattedRefundToGiftCardNameOrDefault(Lcom/squareup/activity/refund/RefundData;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 86
    :goto_1
    iget-object v1, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->doneGlyphView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const-string v2, "doneGlyphView"

    if-nez v1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->doneGlyphView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateView(Lcom/squareup/activity/refund/RefundData;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundDoneCoordinator;->updateTitleAndMessage(Lcom/squareup/activity/refund/RefundData;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundDoneCoordinator;->bindViews(Landroid/view/View;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundDoneCoordinator;->data:Lio/reactivex/Observable;

    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 58
    new-instance v1, Lcom/squareup/activity/refund/RefundDoneCoordinator$attach$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/activity/refund/RefundDoneCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/activity/refund/RefundDoneCoordinator$attach$1;-><init>(Lcom/squareup/activity/refund/RefundDoneCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/activity/refund/RefundDoneCoordinator$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/activity/refund/RefundDoneCoordinator$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "data.distinctUntilChange\u2026 .subscribe(::updateView)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
