.class public final Lcom/squareup/activity/refund/TenderDetailRowHolder$textWatcher$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "IssueRefundCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/activity/refund/TenderDetailRowHolder;-><init>(Landroid/view/ViewGroup;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nIssueRefundCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 IssueRefundCoordinator.kt\ncom/squareup/activity/refund/TenderDetailRowHolder$textWatcher$1\n*L\n1#1,668:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/activity/refund/TenderDetailRowHolder$textWatcher$1",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "afterTextChanged",
        "",
        "editable",
        "Landroid/text/Editable;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/activity/refund/TenderDetailRowHolder;


# direct methods
.method constructor <init>(Lcom/squareup/activity/refund/TenderDetailRowHolder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 588
    iput-object p1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder$textWatcher$1;->this$0:Lcom/squareup/activity/refund/TenderDetailRowHolder;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .line 590
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder$textWatcher$1;->this$0:Lcom/squareup/activity/refund/TenderDetailRowHolder;

    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetailRowHolder;->getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 592
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder$textWatcher$1;->this$0:Lcom/squareup/activity/refund/TenderDetailRowHolder;

    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetailRowHolder;->getEventHandler()Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/activity/refund/TenderDetailRowHolder$textWatcher$1;->this$0:Lcom/squareup/activity/refund/TenderDetailRowHolder;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/TenderDetailRowHolder;->getTenderDetail()Lcom/squareup/activity/refund/TenderDetails;

    move-result-object v1

    const-string v2, "it"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Lcom/squareup/activity/refund/TenderDetails;->copyWithRefundAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/activity/refund/TenderDetails;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;->onTenderDetailChanged(Lcom/squareup/activity/refund/TenderDetails;)V

    :cond_0
    return-void
.end method
