.class public final Lcom/squareup/activity/refund/ItemizationDetailsKt;
.super Ljava/lang/Object;
.source "ItemizationDetails.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemizationDetails.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemizationDetails.kt\ncom/squareup/activity/refund/ItemizationDetailsKt\n*L\n1#1,311:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0012\u0010\u0000\u001a\u00020\u00012\n\u0010\u0002\u001a\u00060\u0003j\u0002`\u0004\u001a\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n*\n\u0010\u000b\"\u00020\u00032\u00020\u0003\u00a8\u0006\u000c"
    }
    d2 = {
        "brandForProto",
        "Lcom/squareup/Card$Brand;",
        "protoBrand",
        "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
        "Lcom/squareup/activity/refund/ProtoBrand;",
        "tipName",
        "",
        "residualTender",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;",
        "res",
        "Lcom/squareup/util/Res;",
        "ProtoBrand",
        "activity_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final brandForProto(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;
    .locals 1

    const-string v0, "protoBrand"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 291
    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetailsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 309
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 308
    :pswitch_1
    sget-object p0, Lcom/squareup/Card$Brand;->VISA:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 307
    :pswitch_2
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 306
    :pswitch_3
    sget-object p0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 305
    :pswitch_4
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 303
    :pswitch_5
    sget-object p0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 302
    :pswitch_6
    sget-object p0, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 301
    :pswitch_7
    sget-object p0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 300
    :pswitch_8
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 298
    :pswitch_9
    sget-object p0, Lcom/squareup/Card$Brand;->EFTPOS:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 297
    :pswitch_a
    sget-object p0, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 296
    :pswitch_b
    sget-object p0, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 295
    :pswitch_c
    sget-object p0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 294
    :pswitch_d
    sget-object p0, Lcom/squareup/Card$Brand;->CASH_APP:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 293
    :pswitch_e
    sget-object p0, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 292
    :pswitch_f
    sget-object p0, Lcom/squareup/Card$Brand;->ALIPAY:Lcom/squareup/Card$Brand;

    :goto_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final tipName(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 3

    const-string v0, "residualTender"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object p0, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    .line 47
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz v0, :cond_0

    .line 49
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    .line 50
    sget v0, Lcom/squareup/activity/R$string;->tip_for_card:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const-string v2, "brand"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/activity/refund/ItemizationDetailsKt;->brandForProto(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object v1

    iget v1, v1, Lcom/squareup/text/CardBrandResources;->shortBrandNameId:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "card_brand"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 52
    iget-object p0, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    check-cast p0, Ljava/lang/CharSequence;

    const-string v0, "last_four"

    invoke-virtual {p1, v0, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 53
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 54
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 58
    :cond_0
    sget p0, Lcom/squareup/activity/R$string;->tip:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method
