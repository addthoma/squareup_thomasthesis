.class public final Lcom/squareup/activity/refund/RefundRetryEvent;
.super Lcom/squareup/activity/refund/RefundEvent;
.source "ItemizedRefundAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundRetryEvent;",
        "Lcom/squareup/activity/refund/RefundEvent;",
        "()V",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/activity/refund/RefundRetryEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 169
    new-instance v0, Lcom/squareup/activity/refund/RefundRetryEvent;

    invoke-direct {v0}, Lcom/squareup/activity/refund/RefundRetryEvent;-><init>()V

    sput-object v0, Lcom/squareup/activity/refund/RefundRetryEvent;->INSTANCE:Lcom/squareup/activity/refund/RefundRetryEvent;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 169
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Itemized Refunds: Retry"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/activity/refund/RefundEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
