.class final Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundReasons$$inlined$forEach$lambda$1;
.super Ljava/lang/Object;
.source "IssueRefundCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/activity/refund/IssueRefundCoordinator;->configureRefundReasons(Lcom/squareup/activity/refund/RefundData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "call",
        "(Lkotlin/Unit;)V",
        "com/squareup/activity/refund/IssueRefundCoordinator$configureRefundReasons$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $data$inlined:Lcom/squareup/activity/refund/RefundData;

.field final synthetic $reasonName:Ljava/lang/String;

.field final synthetic $reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

.field final synthetic $row:Landroid/widget/CheckedTextView;

.field final synthetic this$0:Lcom/squareup/activity/refund/IssueRefundCoordinator;


# direct methods
.method constructor <init>(Landroid/widget/CheckedTextView;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Lcom/squareup/activity/refund/IssueRefundCoordinator;Lcom/squareup/activity/refund/RefundData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundReasons$$inlined$forEach$lambda$1;->$row:Landroid/widget/CheckedTextView;

    iput-object p2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundReasons$$inlined$forEach$lambda$1;->$reasonName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundReasons$$inlined$forEach$lambda$1;->$reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    iput-object p4, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundReasons$$inlined$forEach$lambda$1;->this$0:Lcom/squareup/activity/refund/IssueRefundCoordinator;

    iput-object p5, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundReasons$$inlined$forEach$lambda$1;->$data$inlined:Lcom/squareup/activity/refund/RefundData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 63
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundReasons$$inlined$forEach$lambda$1;->call(Lkotlin/Unit;)V

    return-void
.end method

.method public final call(Lkotlin/Unit;)V
    .locals 2

    .line 382
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundReasons$$inlined$forEach$lambda$1;->$row:Landroid/widget/CheckedTextView;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 383
    iget-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundReasons$$inlined$forEach$lambda$1;->this$0:Lcom/squareup/activity/refund/IssueRefundCoordinator;

    invoke-static {p1}, Lcom/squareup/activity/refund/IssueRefundCoordinator;->access$getEventHandler$p(Lcom/squareup/activity/refund/IssueRefundCoordinator;)Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundReasons$$inlined$forEach$lambda$1;->$reasonName:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$configureRefundReasons$$inlined$forEach$lambda$1;->$reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    invoke-interface {p1, v0, v1}, Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;->onRefundReasonSelected(Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;)V

    return-void
.end method
