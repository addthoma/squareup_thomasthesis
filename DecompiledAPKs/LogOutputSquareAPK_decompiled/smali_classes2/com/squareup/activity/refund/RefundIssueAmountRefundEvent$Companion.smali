.class public final Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent$Companion;
.super Ljava/lang/Object;
.source "ItemizedRefundAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemizedRefundAnalytics.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemizedRefundAnalytics.kt\ncom/squareup/activity/refund/RefundIssueAmountRefundEvent$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,176:1\n704#2:177\n777#2,2:178\n*E\n*S KotlinDebug\n*F\n+ 1 ItemizedRefundAnalytics.kt\ncom/squareup/activity/refund/RefundIssueAmountRefundEvent$Companion\n*L\n120#1:177\n120#1,2:178\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent$Companion;",
        "",
        "()V",
        "of",
        "Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent;",
        "refundData",
        "Lcom/squareup/activity/refund/RefundData;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 114
    invoke-direct {p0}, Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final of(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent;
    .locals 12
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "refundData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundableAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 118
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundReason()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, ""

    .line 119
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getTenderDetails()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 177
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 178
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/activity/refund/TenderDetails;

    .line 120
    invoke-virtual {v4}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-lez v8, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_1

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 179
    :cond_3
    check-cast v2, Ljava/util/List;

    move-object v3, v2

    check-cast v3, Ljava/lang/Iterable;

    const-string p1, ","

    .line 121
    move-object v4, p1

    check-cast v4, Ljava/lang/CharSequence;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object p1, Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent$Companion$of$2;->INSTANCE:Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent$Companion$of$2;

    move-object v9, p1

    check-cast v9, Lkotlin/jvm/functions/Function1;

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 116
    new-instance v2, Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent;

    invoke-direct {v2, v0, v1, p1}, Lcom/squareup/activity/refund/RefundIssueAmountRefundEvent;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method
