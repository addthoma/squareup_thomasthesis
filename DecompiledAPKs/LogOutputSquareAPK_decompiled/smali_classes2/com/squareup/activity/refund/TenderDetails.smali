.class public final Lcom/squareup/activity/refund/TenderDetails;
.super Ljava/lang/Object;
.source "TenderDetails.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/refund/TenderDetails$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u00086\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000 T2\u00020\u0001:\u0001TB\u007f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\u000b\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0003\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0003\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u001cJ\t\u00108\u001a\u00020\u0003H\u00c6\u0003J\t\u00109\u001a\u00020\u0003H\u00c6\u0003J\t\u0010:\u001a\u00020\u0014H\u00c6\u0003J\t\u0010;\u001a\u00020\u0016H\u00c6\u0003J\t\u0010<\u001a\u00020\u0018H\u00c6\u0003J\u000b\u0010=\u001a\u0004\u0018\u00010\u001aH\u00c6\u0003J\t\u0010>\u001a\u00020\u000eH\u00c2\u0003J\t\u0010?\u001a\u00020\u0005H\u00c6\u0003J\t\u0010@\u001a\u00020\u0007H\u00c6\u0003J\t\u0010A\u001a\u00020\tH\u00c6\u0003J\t\u0010B\u001a\u00020\u000bH\u00c6\u0003J\t\u0010C\u001a\u00020\u000bH\u00c6\u0003J\t\u0010D\u001a\u00020\u000eH\u00c6\u0003J\t\u0010E\u001a\u00020\u0003H\u00c6\u0003J\t\u0010F\u001a\u00020\u0011H\u00c6\u0003J\u00a1\u0001\u0010G\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000e2\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u00112\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u00142\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u00162\u0008\u0008\u0002\u0010\u0017\u001a\u00020\u00182\n\u0008\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0008\u0008\u0002\u0010\u001b\u001a\u00020\u000eH\u00c6\u0001J\u0006\u0010H\u001a\u00020\u0000J\u0016\u0010I\u001a\u00020\u00002\u0006\u0010J\u001a\u00020\u00162\u0006\u0010K\u001a\u00020\u0018J\u000e\u0010L\u001a\u00020\u00002\u0006\u0010\u000c\u001a\u00020\u000bJ\u0013\u0010M\u001a\u00020\u000e2\u0008\u0010N\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u0006\u0010O\u001a\u00020\u000eJ\t\u0010P\u001a\u00020QH\u00d6\u0001J\u0006\u0010R\u001a\u00020\u000eJ\t\u0010S\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0019\u001a\u0004\u0018\u00010\u001a\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0011\u0010\u0017\u001a\u00020\u0018\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u000e\u0010\u001b\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u000f\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R\u0011\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R\u0011\u0010\u0012\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010\"R\u0011\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\'R\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010)R\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010+R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008,\u0010-R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008.\u0010/R\u0011\u0010\u000c\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00080\u00101R\u0011\u00102\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00083\u00101R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00084\u00101R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00085\u0010\"R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00086\u00107\u00a8\u0006U"
    }
    d2 = {
        "Lcom/squareup/activity/refund/TenderDetails;",
        "",
        "sourceTenderToken",
        "",
        "glyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "name",
        "",
        "type",
        "Lcom/squareup/billhistory/model/TenderHistory$Type;",
        "residualRefundableMoney",
        "Lcom/squareup/protos/common/Money;",
        "refundMoney",
        "editable",
        "",
        "cardBrand",
        "felicaBrand",
        "Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;",
        "cardLastFour",
        "cardEntryMethod",
        "Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
        "cardReaderType",
        "Lcom/squareup/protos/client/bills/CardData$ReaderType;",
        "cardAuthorizationData",
        "Lokio/ByteString;",
        "accountType",
        "Lcom/squareup/protos/client/bills/CardTender$AccountType;",
        "cardAuthorizationRequired",
        "(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$AccountType;Z)V",
        "getAccountType",
        "()Lcom/squareup/protos/client/bills/CardTender$AccountType;",
        "getCardAuthorizationData",
        "()Lokio/ByteString;",
        "getCardBrand",
        "()Ljava/lang/String;",
        "getCardEntryMethod",
        "()Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
        "getCardLastFour",
        "getCardReaderType",
        "()Lcom/squareup/protos/client/bills/CardData$ReaderType;",
        "getEditable",
        "()Z",
        "getFelicaBrand",
        "()Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;",
        "getGlyph",
        "()Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "getName",
        "()Ljava/lang/CharSequence;",
        "getRefundMoney",
        "()Lcom/squareup/protos/common/Money;",
        "residualAfterThisRefund",
        "getResidualAfterThisRefund",
        "getResidualRefundableMoney",
        "getSourceTenderToken",
        "getType",
        "()Lcom/squareup/billhistory/model/TenderHistory$Type;",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component15",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "copyClearingCardData",
        "copyWithCardData",
        "readerType",
        "cardData",
        "copyWithRefundAmount",
        "equals",
        "other",
        "hasCardAuthorizationData",
        "hashCode",
        "",
        "requiresCardAuthorization",
        "toString",
        "Companion",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/activity/refund/TenderDetails$Companion;


# instance fields
.field private final accountType:Lcom/squareup/protos/client/bills/CardTender$AccountType;

.field private final cardAuthorizationData:Lokio/ByteString;

.field private final cardAuthorizationRequired:Z

.field private final cardBrand:Ljava/lang/String;

.field private final cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field private final cardLastFour:Ljava/lang/String;

.field private final cardReaderType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field private final editable:Z

.field private final felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

.field private final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private final name:Ljava/lang/CharSequence;

.field private final refundMoney:Lcom/squareup/protos/common/Money;

.field private final residualAfterThisRefund:Lcom/squareup/protos/common/Money;

.field private final residualRefundableMoney:Lcom/squareup/protos/common/Money;

.field private final sourceTenderToken:Ljava/lang/String;

.field private final type:Lcom/squareup/billhistory/model/TenderHistory$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/activity/refund/TenderDetails$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/activity/refund/TenderDetails$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/activity/refund/TenderDetails;->Companion:Lcom/squareup/activity/refund/TenderDetails$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$AccountType;Z)V
    .locals 14

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    move-object/from16 v12, p13

    const-string v13, "sourceTenderToken"

    invoke-static {p1, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "glyph"

    invoke-static {v2, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "name"

    invoke-static {v3, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "type"

    invoke-static {v4, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "residualRefundableMoney"

    invoke-static {v5, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "refundMoney"

    invoke-static {v6, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "cardBrand"

    invoke-static {v7, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "felicaBrand"

    invoke-static {v8, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "cardLastFour"

    invoke-static {v9, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "cardEntryMethod"

    invoke-static {v10, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "cardReaderType"

    invoke-static {v11, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "cardAuthorizationData"

    invoke-static {v12, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lcom/squareup/activity/refund/TenderDetails;->sourceTenderToken:Ljava/lang/String;

    iput-object v2, v0, Lcom/squareup/activity/refund/TenderDetails;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object v3, v0, Lcom/squareup/activity/refund/TenderDetails;->name:Ljava/lang/CharSequence;

    iput-object v4, v0, Lcom/squareup/activity/refund/TenderDetails;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    iput-object v5, v0, Lcom/squareup/activity/refund/TenderDetails;->residualRefundableMoney:Lcom/squareup/protos/common/Money;

    iput-object v6, v0, Lcom/squareup/activity/refund/TenderDetails;->refundMoney:Lcom/squareup/protos/common/Money;

    move/from16 v1, p7

    iput-boolean v1, v0, Lcom/squareup/activity/refund/TenderDetails;->editable:Z

    iput-object v7, v0, Lcom/squareup/activity/refund/TenderDetails;->cardBrand:Ljava/lang/String;

    iput-object v8, v0, Lcom/squareup/activity/refund/TenderDetails;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    iput-object v9, v0, Lcom/squareup/activity/refund/TenderDetails;->cardLastFour:Ljava/lang/String;

    iput-object v10, v0, Lcom/squareup/activity/refund/TenderDetails;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iput-object v11, v0, Lcom/squareup/activity/refund/TenderDetails;->cardReaderType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iput-object v12, v0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationData:Lokio/ByteString;

    move-object/from16 v1, p14

    iput-object v1, v0, Lcom/squareup/activity/refund/TenderDetails;->accountType:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    move/from16 v1, p15

    iput-boolean v1, v0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationRequired:Z

    .line 51
    iget-object v1, v0, Lcom/squareup/activity/refund/TenderDetails;->residualRefundableMoney:Lcom/squareup/protos/common/Money;

    iget-object v2, v0, Lcom/squareup/activity/refund/TenderDetails;->refundMoney:Lcom/squareup/protos/common/Money;

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    const-string v2, "subtract(residualRefundableMoney, refundMoney)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/activity/refund/TenderDetails;->residualAfterThisRefund:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method private final component15()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationRequired:Z

    return v0
.end method

.method public static synthetic copy$default(Lcom/squareup/activity/refund/TenderDetails;Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$AccountType;ZILjava/lang/Object;)Lcom/squareup/activity/refund/TenderDetails;
    .locals 16

    move-object/from16 v0, p0

    move/from16 v1, p16

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/activity/refund/TenderDetails;->sourceTenderToken:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/activity/refund/TenderDetails;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_1

    :cond_1
    move-object/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/activity/refund/TenderDetails;->name:Ljava/lang/CharSequence;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/activity/refund/TenderDetails;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/activity/refund/TenderDetails;->residualRefundableMoney:Lcom/squareup/protos/common/Money;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/activity/refund/TenderDetails;->refundMoney:Lcom/squareup/protos/common/Money;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/squareup/activity/refund/TenderDetails;->editable:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/activity/refund/TenderDetails;->cardBrand:Ljava/lang/String;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/activity/refund/TenderDetails;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-object v11, v0, Lcom/squareup/activity/refund/TenderDetails;->cardLastFour:Ljava/lang/String;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget-object v12, v0, Lcom/squareup/activity/refund/TenderDetails;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    goto :goto_a

    :cond_a
    move-object/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget-object v13, v0, Lcom/squareup/activity/refund/TenderDetails;->cardReaderType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    goto :goto_b

    :cond_b
    move-object/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget-object v14, v0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationData:Lokio/ByteString;

    goto :goto_c

    :cond_c
    move-object/from16 v14, p13

    :goto_c
    and-int/lit16 v15, v1, 0x2000

    if-eqz v15, :cond_d

    iget-object v15, v0, Lcom/squareup/activity/refund/TenderDetails;->accountType:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    goto :goto_d

    :cond_d
    move-object/from16 v15, p14

    :goto_d
    and-int/lit16 v1, v1, 0x4000

    if-eqz v1, :cond_e

    iget-boolean v1, v0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationRequired:Z

    goto :goto_e

    :cond_e
    move/from16 v1, p15

    :goto_e
    move-object/from16 p1, v2

    move-object/from16 p2, v3

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move-object/from16 p11, v12

    move-object/from16 p12, v13

    move-object/from16 p13, v14

    move-object/from16 p14, v15

    move/from16 p15, v1

    invoke-virtual/range {p0 .. p15}, Lcom/squareup/activity/refund/TenderDetails;->copy(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$AccountType;Z)Lcom/squareup/activity/refund/TenderDetails;

    move-result-object v0

    return-object v0
.end method

.method public static final of(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Z)Lcom/squareup/activity/refund/TenderDetails;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/TenderDetails;->Companion:Lcom/squareup/activity/refund/TenderDetails$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/squareup/activity/refund/TenderDetails$Companion;->of(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Z)Lcom/squareup/activity/refund/TenderDetails;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->sourceTenderToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component10()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardLastFour:Ljava/lang/String;

    return-object v0
.end method

.method public final component11()Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object v0
.end method

.method public final component12()Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardReaderType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object v0
.end method

.method public final component13()Lokio/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationData:Lokio/ByteString;

    return-object v0
.end method

.method public final component14()Lcom/squareup/protos/client/bills/CardTender$AccountType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->accountType:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object v0
.end method

.method public final component2()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public final component3()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->name:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component4()Lcom/squareup/billhistory/model/TenderHistory$Type;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->residualRefundableMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component6()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->refundMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/activity/refund/TenderDetails;->editable:Z

    return v0
.end method

.method public final component8()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardBrand:Ljava/lang/String;

    return-object v0
.end method

.method public final component9()Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$AccountType;Z)Lcom/squareup/activity/refund/TenderDetails;
    .locals 17

    const-string v0, "sourceTenderToken"

    move-object/from16 v2, p1

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glyph"

    move-object/from16 v3, p2

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "residualRefundableMoney"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refundMoney"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardBrand"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "felicaBrand"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardLastFour"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardEntryMethod"

    move-object/from16 v12, p11

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderType"

    move-object/from16 v13, p12

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardAuthorizationData"

    move-object/from16 v14, p13

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/activity/refund/TenderDetails;

    move-object v1, v0

    move/from16 v8, p7

    move-object/from16 v15, p14

    move/from16 v16, p15

    invoke-direct/range {v1 .. v16}, Lcom/squareup/activity/refund/TenderDetails;-><init>(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$AccountType;Z)V

    return-object v0
.end method

.method public final copyClearingCardData()Lcom/squareup/activity/refund/TenderDetails;
    .locals 18

    move-object/from16 v0, p0

    .line 135
    sget-object v13, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x6fff

    const/16 v17, 0x0

    invoke-static/range {v0 .. v17}, Lcom/squareup/activity/refund/TenderDetails;->copy$default(Lcom/squareup/activity/refund/TenderDetails;Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$AccountType;ZILjava/lang/Object;)Lcom/squareup/activity/refund/TenderDetails;

    move-result-object v0

    return-object v0
.end method

.method public final copyWithCardData(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;)Lcom/squareup/activity/refund/TenderDetails;
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    const-string v1, "readerType"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "cardData"

    move-object/from16 v2, p2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x67ff

    const/16 v17, 0x0

    .line 130
    invoke-static/range {v0 .. v17}, Lcom/squareup/activity/refund/TenderDetails;->copy$default(Lcom/squareup/activity/refund/TenderDetails;Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$AccountType;ZILjava/lang/Object;)Lcom/squareup/activity/refund/TenderDetails;

    move-result-object v0

    return-object v0
.end method

.method public final copyWithRefundAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/activity/refund/TenderDetails;
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v6, p1

    const-string v1, "refundMoney"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x7fdf

    const/16 v17, 0x0

    .line 124
    invoke-static/range {v0 .. v17}, Lcom/squareup/activity/refund/TenderDetails;->copy$default(Lcom/squareup/activity/refund/TenderDetails;Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$AccountType;ZILjava/lang/Object;)Lcom/squareup/activity/refund/TenderDetails;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/activity/refund/TenderDetails;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/activity/refund/TenderDetails;

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->sourceTenderToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/TenderDetails;->sourceTenderToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p1, Lcom/squareup/activity/refund/TenderDetails;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->name:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/activity/refund/TenderDetails;->name:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    iget-object v1, p1, Lcom/squareup/activity/refund/TenderDetails;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->residualRefundableMoney:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/activity/refund/TenderDetails;->residualRefundableMoney:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->refundMoney:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/activity/refund/TenderDetails;->refundMoney:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/TenderDetails;->editable:Z

    iget-boolean v1, p1, Lcom/squareup/activity/refund/TenderDetails;->editable:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardBrand:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/TenderDetails;->cardBrand:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    iget-object v1, p1, Lcom/squareup/activity/refund/TenderDetails;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardLastFour:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/TenderDetails;->cardLastFour:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iget-object v1, p1, Lcom/squareup/activity/refund/TenderDetails;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardReaderType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iget-object v1, p1, Lcom/squareup/activity/refund/TenderDetails;->cardReaderType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationData:Lokio/ByteString;

    iget-object v1, p1, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationData:Lokio/ByteString;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->accountType:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    iget-object v1, p1, Lcom/squareup/activity/refund/TenderDetails;->accountType:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationRequired:Z

    iget-boolean p1, p1, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationRequired:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAccountType()Lcom/squareup/protos/client/bills/CardTender$AccountType;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->accountType:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object v0
.end method

.method public final getCardAuthorizationData()Lokio/ByteString;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationData:Lokio/ByteString;

    return-object v0
.end method

.method public final getCardBrand()Ljava/lang/String;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardBrand:Ljava/lang/String;

    return-object v0
.end method

.method public final getCardEntryMethod()Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object v0
.end method

.method public final getCardLastFour()Ljava/lang/String;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardLastFour:Ljava/lang/String;

    return-object v0
.end method

.method public final getCardReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardReaderType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object v0
.end method

.method public final getEditable()Z
    .locals 1

    .line 40
    iget-boolean v0, p0, Lcom/squareup/activity/refund/TenderDetails;->editable:Z

    return v0
.end method

.method public final getFelicaBrand()Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object v0
.end method

.method public final getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public final getName()Ljava/lang/CharSequence;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->name:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getRefundMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->refundMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getResidualAfterThisRefund()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->residualAfterThisRefund:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getResidualRefundableMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->residualRefundableMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getSourceTenderToken()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->sourceTenderToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Lcom/squareup/billhistory/model/TenderHistory$Type;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    return-object v0
.end method

.method public final hasCardAuthorizationData()Z
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationData:Lokio/ByteString;

    sget-object v1, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->sourceTenderToken:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/TenderDetails;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/TenderDetails;->name:Ljava/lang/CharSequence;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/TenderDetails;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/TenderDetails;->residualRefundableMoney:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/TenderDetails;->refundMoney:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/activity/refund/TenderDetails;->editable:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/TenderDetails;->cardBrand:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_7
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/TenderDetails;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_8
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/TenderDetails;->cardLastFour:Ljava/lang/String;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_9
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/TenderDetails;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_a
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/TenderDetails;->cardReaderType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_b
    const/4 v2, 0x0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationData:Lokio/ByteString;

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_c
    const/4 v2, 0x0

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/TenderDetails;->accountType:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationRequired:Z

    if-eqz v1, :cond_e

    const/4 v1, 0x1

    :cond_e
    add-int/2addr v0, v1

    return v0
.end method

.method public final requiresCardAuthorization()Z
    .locals 2

    .line 139
    iget-boolean v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationRequired:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationData:Lokio/ByteString;

    sget-object v1, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TenderDetails(sourceTenderToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/TenderDetails;->sourceTenderToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", glyph="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/TenderDetails;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/TenderDetails;->name:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/TenderDetails;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", residualRefundableMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/TenderDetails;->residualRefundableMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", refundMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/TenderDetails;->refundMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", editable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/activity/refund/TenderDetails;->editable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", cardBrand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/TenderDetails;->cardBrand:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", felicaBrand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/TenderDetails;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cardLastFour="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/TenderDetails;->cardLastFour:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cardEntryMethod="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/TenderDetails;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cardReaderType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/TenderDetails;->cardReaderType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cardAuthorizationData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationData:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", accountType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/TenderDetails;->accountType:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cardAuthorizationRequired="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/activity/refund/TenderDetails;->cardAuthorizationRequired:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
