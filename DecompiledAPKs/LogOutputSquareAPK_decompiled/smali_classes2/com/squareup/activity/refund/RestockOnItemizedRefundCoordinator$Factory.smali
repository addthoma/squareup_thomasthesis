.class public final Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$Factory;
.super Ljava/lang/Object;
.source "RestockOnItemizedRefundCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u001c\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bR\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$Factory;",
        "",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/text/Formatter;)V",
        "create",
        "Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;",
        "eventHandler",
        "Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/activity/refund/RefundData;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public final create(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;Lio/reactivex/Observable;)Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;)",
            "Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;"
        }
    .end annotation

    const-string v0, "eventHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;

    iget-object v1, p0, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;-><init>(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;Lio/reactivex/Observable;Lcom/squareup/text/Formatter;)V

    return-object v0
.end method
