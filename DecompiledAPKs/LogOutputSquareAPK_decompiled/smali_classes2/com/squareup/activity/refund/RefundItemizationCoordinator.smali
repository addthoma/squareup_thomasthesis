.class public final Lcom/squareup/activity/refund/RefundItemizationCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "RefundItemizationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;,
        Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRefundItemizationCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RefundItemizationCoordinator.kt\ncom/squareup/activity/refund/RefundItemizationCoordinator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,320:1\n1642#2,2:321\n1642#2,2:323\n704#2:325\n777#2,2:326\n1360#2:328\n1429#2,3:329\n1550#2,3:332\n*E\n*S KotlinDebug\n*F\n+ 1 RefundItemizationCoordinator.kt\ncom/squareup/activity/refund/RefundItemizationCoordinator\n*L\n202#1,2:321\n263#1,2:323\n271#1:325\n271#1,2:326\n273#1:328\n273#1,3:329\n296#1,3:332\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u000289BA\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0018\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)2\u0006\u0010\u0002\u001a\u00020\u0004H\u0002J\u0010\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-H\u0016J\u0010\u0010.\u001a\u00020+2\u0006\u0010,\u001a\u00020-H\u0002J\u0010\u0010/\u001a\u00020+2\u0006\u0010\u0002\u001a\u00020\u0004H\u0002J\u0010\u00100\u001a\u00020+2\u0006\u0010\u0002\u001a\u00020\u0004H\u0002J\u0010\u00101\u001a\u00020+2\u0006\u0010\u0002\u001a\u00020\u0004H\u0002J\u0010\u00102\u001a\u00020+2\u0006\u0010\u0002\u001a\u00020\u0004H\u0002J\u0010\u00103\u001a\u0002042\u0006\u0010\u0002\u001a\u00020\u0004H\u0002J\u0010\u00105\u001a\u0002042\u0006\u0010\u0002\u001a\u00020\u0004H\u0002J\u0008\u00106\u001a\u00020+H\u0002J\u0010\u00107\u001a\u00020+2\u0006\u0010\u0002\u001a\u00020\u0004H\u0002R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u001fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u001fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006:"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundItemizationCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "data",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/activity/refund/RefundData;",
        "eventHandler",
        "Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V",
        "actionBarView",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "amountButton",
        "Lcom/squareup/widgets/ScalingRadioButton;",
        "amountContainer",
        "Landroid/widget/LinearLayout;",
        "items",
        "itemsButton",
        "itemsContainer",
        "maxMoneyScrubber",
        "Lcom/squareup/money/MaxMoneyScrubber;",
        "refundAmountEditor",
        "Lcom/squareup/widgets/SelectableEditText;",
        "refundAmountHelp",
        "Lcom/squareup/widgets/MessageView;",
        "refundAmountMax",
        "refundTypeRadioGroup",
        "Landroid/widget/RadioGroup;",
        "refundedItems",
        "refundedItemsContainer",
        "taxesAndDiscountsHelp",
        "addAllItemsRow",
        "Lcom/squareup/activity/refund/ItemizationRow;",
        "rowFactory",
        "Lcom/squareup/activity/refund/ItemizationRow$Factory;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "configureActionBar",
        "configureRefundItems",
        "configureRefundType",
        "configureRefundedItems",
        "isValidItemizationOrAmount",
        "",
        "isValidRefundAmount",
        "itemSelectionChanged",
        "updateView",
        "Factory",
        "RefundItemizationEventHandler",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private amountButton:Lcom/squareup/widgets/ScalingRadioButton;

.field private amountContainer:Landroid/widget/LinearLayout;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final data:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;"
        }
    .end annotation
.end field

.field private final eventHandler:Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;

.field private items:Landroid/widget/LinearLayout;

.field private itemsButton:Lcom/squareup/widgets/ScalingRadioButton;

.field private itemsContainer:Landroid/widget/LinearLayout;

.field private maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private refundAmountEditor:Lcom/squareup/widgets/SelectableEditText;

.field private refundAmountHelp:Lcom/squareup/widgets/MessageView;

.field private refundAmountMax:Lcom/squareup/widgets/MessageView;

.field private refundTypeRadioGroup:Landroid/widget/RadioGroup;

.field private refundedItems:Landroid/widget/LinearLayout;

.field private refundedItemsContainer:Landroid/widget/LinearLayout;

.field private final res:Lcom/squareup/util/Res;

.field private taxesAndDiscountsHelp:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;",
            "Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->data:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->eventHandler:Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;

    iput-object p3, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p4, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iput-object p5, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p6, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$getEventHandler$p(Lcom/squareup/activity/refund/RefundItemizationCoordinator;)Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->eventHandler:Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;

    return-object p0
.end method

.method public static final synthetic access$getItems$p(Lcom/squareup/activity/refund/RefundItemizationCoordinator;)Landroid/widget/LinearLayout;
    .locals 1

    .line 50
    iget-object p0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->items:Landroid/widget/LinearLayout;

    if-nez p0, :cond_0

    const-string v0, "items"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRefundAmountEditor$p(Lcom/squareup/activity/refund/RefundItemizationCoordinator;)Lcom/squareup/widgets/SelectableEditText;
    .locals 1

    .line 50
    iget-object p0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundAmountEditor:Lcom/squareup/widgets/SelectableEditText;

    if-nez p0, :cond_0

    const-string v0, "refundAmountEditor"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$itemSelectionChanged(Lcom/squareup/activity/refund/RefundItemizationCoordinator;)V
    .locals 0

    .line 50
    invoke-direct {p0}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->itemSelectionChanged()V

    return-void
.end method

.method public static final synthetic access$setItems$p(Lcom/squareup/activity/refund/RefundItemizationCoordinator;Landroid/widget/LinearLayout;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->items:Landroid/widget/LinearLayout;

    return-void
.end method

.method public static final synthetic access$setRefundAmountEditor$p(Lcom/squareup/activity/refund/RefundItemizationCoordinator;Lcom/squareup/widgets/SelectableEditText;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundAmountEditor:Lcom/squareup/widgets/SelectableEditText;

    return-void
.end method

.method public static final synthetic access$updateView(Lcom/squareup/activity/refund/RefundItemizationCoordinator;Lcom/squareup/activity/refund/RefundData;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->updateView(Lcom/squareup/activity/refund/RefundData;)V

    return-void
.end method

.method private final addAllItemsRow(Lcom/squareup/activity/refund/ItemizationRow$Factory;Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/ItemizationRow;
    .locals 16

    move-object/from16 v0, p0

    .line 231
    new-instance v15, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 232
    iget-object v1, v0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/activity/R$string;->refund_select_all_items:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/activity/refund/RefundData;->getRefundableAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x7fc

    const/4 v14, 0x0

    move-object v1, v15

    .line 231
    invoke-direct/range {v1 .. v14}, Lcom/squareup/activity/refund/ItemizationDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v1, 0x1

    move-object/from16 v2, p1

    .line 230
    invoke-virtual {v2, v15, v1}, Lcom/squareup/activity/refund/ItemizationRow$Factory;->newRow(Lcom/squareup/activity/refund/ItemizationDetails;Z)Lcom/squareup/activity/refund/ItemizationRow;

    move-result-object v2

    .line 237
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/activity/refund/RefundData;->getItemizationDetails()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ne v3, v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v2, v3}, Lcom/squareup/activity/refund/ItemizationRow;->setChecked(Z)V

    .line 240
    move-object v3, v2

    check-cast v3, Landroid/view/View;

    invoke-static {v3}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v4

    new-instance v5, Lcom/squareup/activity/refund/RefundItemizationCoordinator$addAllItemsRow$1;

    invoke-direct {v5, v0, v2}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$addAllItemsRow$1;-><init>(Lcom/squareup/activity/refund/RefundItemizationCoordinator;Lcom/squareup/activity/refund/ItemizationRow;)V

    check-cast v5, Lrx/functions/Action1;

    invoke-virtual {v4, v5}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 249
    iget-object v4, v0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->items:Landroid/widget/LinearLayout;

    if-nez v4, :cond_1

    const-string v5, "items"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 251
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/activity/refund/RefundData;->getHasItemizationsFromMultipleSourceBills()Z

    move-result v4

    xor-int/2addr v1, v4

    invoke-static {v3, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-object v2
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 305
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 306
    sget v0, Lcom/squareup/activity/R$id;->refund_type_radio_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundTypeRadioGroup:Landroid/widget/RadioGroup;

    .line 307
    sget v0, Lcom/squareup/activity/R$id;->items_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ScalingRadioButton;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->itemsButton:Lcom/squareup/widgets/ScalingRadioButton;

    .line 308
    sget v0, Lcom/squareup/activity/R$id;->amount_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ScalingRadioButton;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->amountButton:Lcom/squareup/widgets/ScalingRadioButton;

    .line 309
    sget v0, Lcom/squareup/activity/R$id;->items_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->itemsContainer:Landroid/widget/LinearLayout;

    .line 310
    sget v0, Lcom/squareup/activity/R$id;->items:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->items:Landroid/widget/LinearLayout;

    .line 311
    sget v0, Lcom/squareup/activity/R$id;->refunded_items_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundedItemsContainer:Landroid/widget/LinearLayout;

    .line 312
    sget v0, Lcom/squareup/activity/R$id;->refunded_items:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundedItems:Landroid/widget/LinearLayout;

    .line 313
    sget v0, Lcom/squareup/activity/R$id;->amount_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->amountContainer:Landroid/widget/LinearLayout;

    .line 314
    sget v0, Lcom/squareup/activity/R$id;->refund_amount_editor:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundAmountEditor:Lcom/squareup/widgets/SelectableEditText;

    .line 315
    sget v0, Lcom/squareup/activity/R$id;->refund_amount_max:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundAmountMax:Lcom/squareup/widgets/MessageView;

    .line 316
    sget v0, Lcom/squareup/activity/R$id;->refund_amount_help:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundAmountHelp:Lcom/squareup/widgets/MessageView;

    .line 317
    sget v0, Lcom/squareup/activity/R$id;->items_taxes_and_discounts_help:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->taxesAndDiscountsHelp:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final configureActionBar(Lcom/squareup/activity/refund/RefundData;)V
    .locals 5

    .line 173
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBarView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBarView.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 167
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/activity/R$string;->refund_issue:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 168
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 169
    new-instance v2, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureActionBar$1;

    iget-object v3, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->eventHandler:Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;

    invoke-direct {v2, v3}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureActionBar$1;-><init>(Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    new-instance v3, Lcom/squareup/activity/refund/RefundItemizationCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v3, v2}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 170
    iget-object v2, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->next:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 171
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->isValidItemizationOrAmount(Lcom/squareup/activity/refund/RefundData;)Z

    move-result p1

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 172
    new-instance v1, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureActionBar$2;

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->eventHandler:Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;

    invoke-direct {v1, v2}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureActionBar$2;-><init>(Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/activity/refund/RefundItemizationCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {p1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 173
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final configureRefundItems(Lcom/squareup/activity/refund/RefundData;)V
    .locals 18

    move-object/from16 v6, p0

    .line 193
    iget-object v0, v6, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->items:Landroid/widget/LinearLayout;

    const-string v7, "items"

    if-nez v0, :cond_0

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 194
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/activity/refund/RefundData;->getItemizationDetails()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 197
    :cond_1
    new-instance v8, Lcom/squareup/activity/refund/ItemizationRow$Factory;

    iget-object v0, v6, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->items:Landroid/widget/LinearLayout;

    if-nez v0, :cond_2

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v1, v6, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-direct {v8, v0, v1}, Lcom/squareup/activity/refund/ItemizationRow$Factory;-><init>(Landroid/widget/LinearLayout;Lcom/squareup/text/Formatter;)V

    move-object/from16 v9, p1

    .line 199
    invoke-direct {v6, v8, v9}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->addAllItemsRow(Lcom/squareup/activity/refund/ItemizationRow$Factory;Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/ItemizationRow;

    move-result-object v10

    .line 202
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/activity/refund/RefundData;->getItemizationDetails()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 321
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/activity/refund/ItemizationDetails;

    const/4 v0, 0x2

    const/4 v13, 0x0

    const/4 v14, 0x0

    .line 203
    invoke-static {v8, v12, v14, v0, v13}, Lcom/squareup/activity/refund/ItemizationRow$Factory;->newRow$default(Lcom/squareup/activity/refund/ItemizationRow$Factory;Lcom/squareup/activity/refund/ItemizationDetails;ZILjava/lang/Object;)Lcom/squareup/activity/refund/ItemizationRow;

    move-result-object v15

    .line 204
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object v0

    .line 205
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/activity/refund/RefundData;->getItemizationDetails()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v12}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v15, v0}, Lcom/squareup/activity/refund/ItemizationRow;->setChecked(Z)V

    .line 206
    move-object v5, v15

    check-cast v5, Landroid/view/View;

    invoke-static {v5}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v4

    new-instance v16, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureRefundItems$$inlined$forEach$lambda$1;

    move-object/from16 v0, v16

    move-object v1, v15

    move-object/from16 v2, p0

    move-object v3, v8

    move-object v13, v4

    move-object/from16 v4, p1

    move-object/from16 v17, v5

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$configureRefundItems$$inlined$forEach$lambda$1;-><init>(Lcom/squareup/activity/refund/ItemizationRow;Lcom/squareup/activity/refund/RefundItemizationCoordinator;Lcom/squareup/activity/refund/ItemizationRow$Factory;Lcom/squareup/activity/refund/RefundData;Lcom/squareup/activity/refund/ItemizationRow;)V

    move-object/from16 v0, v16

    check-cast v0, Lrx/functions/Action1;

    invoke-virtual {v13, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 211
    invoke-virtual {v15, v14}, Lcom/squareup/activity/refund/ItemizationRow;->setVisibility(I)V

    .line 213
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/activity/refund/RefundData;->getHasSelectedIndices()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 214
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/activity/refund/RefundData;->getSelectedBillServerToken()Ljava/lang/String;

    move-result-object v13

    goto :goto_1

    :cond_3
    const/4 v13, 0x0

    :goto_1
    if-eqz v13, :cond_4

    .line 220
    invoke-virtual {v12}, Lcom/squareup/activity/refund/ItemizationDetails;->getSourceBillServerToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v13}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    const/4 v14, 0x1

    :cond_5
    invoke-virtual {v15, v14}, Lcom/squareup/activity/refund/ItemizationRow;->setEnabled(Z)V

    .line 221
    iget-object v0, v6, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->items:Landroid/widget/LinearLayout;

    if-nez v0, :cond_6

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    move-object/from16 v15, v17

    invoke-virtual {v0, v15}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_7
    return-void
.end method

.method private final configureRefundType(Lcom/squareup/activity/refund/RefundData;)V
    .locals 6

    .line 177
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->amountButton:Lcom/squareup/widgets/ScalingRadioButton;

    if-nez v0, :cond_0

    const-string v1, "amountButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundMode()Lcom/squareup/activity/refund/RefundMode;

    move-result-object v1

    sget-object v2, Lcom/squareup/activity/refund/RefundMode;->AMOUNT:Lcom/squareup/activity/refund/RefundMode;

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingRadioButton;->setChecked(Z)V

    .line 178
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->itemsButton:Lcom/squareup/widgets/ScalingRadioButton;

    if-nez v0, :cond_2

    const-string v1, "itemsButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundMode()Lcom/squareup/activity/refund/RefundMode;

    move-result-object v1

    sget-object v2, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingRadioButton;->setChecked(Z)V

    .line 179
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->amountContainer:Landroid/widget/LinearLayout;

    if-nez v0, :cond_4

    const-string v1, "amountContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundMode()Lcom/squareup/activity/refund/RefundMode;

    move-result-object v1

    sget-object v2, Lcom/squareup/activity/refund/RefundMode;->AMOUNT:Lcom/squareup/activity/refund/RefundMode;

    if-ne v1, v2, :cond_5

    const/4 v1, 0x1

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    :goto_2
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 180
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->itemsContainer:Landroid/widget/LinearLayout;

    if-nez v0, :cond_6

    const-string v1, "itemsContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundMode()Lcom/squareup/activity/refund/RefundMode;

    move-result-object v1

    sget-object v2, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    if-ne v1, v2, :cond_7

    const/4 v1, 0x1

    goto :goto_3

    :cond_7
    const/4 v1, 0x0

    :goto_3
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 182
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundedItemsContainer:Landroid/widget/LinearLayout;

    if-nez v0, :cond_8

    const-string v1, "refundedItemsContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast v0, Landroid/view/View;

    .line 183
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundMode()Lcom/squareup/activity/refund/RefundMode;

    move-result-object v1

    sget-object v2, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    if-ne v1, v2, :cond_9

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundedItemizationDetails()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v4

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    goto :goto_4

    :cond_9
    const/4 v1, 0x0

    .line 182
    :goto_4
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 184
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->taxesAndDiscountsHelp:Lcom/squareup/widgets/MessageView;

    const-string v1, "taxesAndDiscountsHelp"

    if-nez v0, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundMode()Lcom/squareup/activity/refund/RefundMode;

    move-result-object v2

    sget-object v5, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    if-ne v2, v5, :cond_b

    const/4 v3, 0x1

    :cond_b
    invoke-static {v0, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 185
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getHasDisabledIndices()Z

    move-result p1

    if-eqz p1, :cond_d

    .line 188
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->taxesAndDiscountsHelp:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_c

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    sget v0, Lcom/squareup/activity/R$string;->items_from_multiple_source_bills_warning:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(I)V

    :cond_d
    return-void
.end method

.method private final configureRefundedItems(Lcom/squareup/activity/refund/RefundData;)V
    .locals 6

    .line 256
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundedItems:Landroid/widget/LinearLayout;

    const-string v1, "refundedItems"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 259
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundedItemsContainer:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    const-string v2, "refundedItemsContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    sget v2, Lcom/squareup/activity/R$id;->refunded_items_title:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 258
    check-cast v0, Lcom/squareup/widgets/PreservedLabelView;

    .line 260
    iget-object v2, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/activity/R$string;->refunded_section_header:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;)V

    .line 263
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundedItemizationDetails()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 323
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 264
    iget-object v2, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundedItems:Landroid/widget/LinearLayout;

    if-nez v2, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget-object v3, Lcom/squareup/activity/refund/RefundedItemizationRow;->INSTANCE:Lcom/squareup/activity/refund/RefundedItemizationRow;

    iget-object v4, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundedItems:Landroid/widget/LinearLayout;

    if-nez v4, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v5, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v3, v0, v4, v5}, Lcom/squareup/activity/refund/RefundedItemizationRow;->newRow(Lcom/squareup/activity/refund/ItemizationDetails;Landroid/widget/LinearLayout;Lcom/squareup/text/Formatter;)Lcom/squareup/ui/account/view/LineRow;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_4
    return-void
.end method

.method private final isValidItemizationOrAmount(Lcom/squareup/activity/refund/RefundData;)Z
    .locals 2

    .line 277
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundMode()Lcom/squareup/activity/refund/RefundMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/activity/refund/RefundItemizationCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 279
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->isValidRefundAmount(Lcom/squareup/activity/refund/RefundData;)Z

    move-result v1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 278
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private final isValidRefundAmount(Lcom/squareup/activity/refund/RefundData;)Z
    .locals 7

    .line 287
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundableAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v0, v5, v1

    if-nez v0, :cond_1

    goto :goto_5

    .line 290
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundableAmount()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_5

    .line 293
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v0, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v0, v5, v1

    if-nez v0, :cond_4

    :goto_1
    const/4 v4, 0x0

    goto :goto_5

    .line 296
    :cond_4
    :goto_2
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getTenderDetails()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 332
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_6

    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    const/4 p1, 0x0

    goto :goto_4

    .line 333
    :cond_6
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_7
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/TenderDetails;

    .line 296
    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetails;->getType()Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object v1

    sget-object v2, Lcom/squareup/billhistory/model/TenderHistory$Type;->CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-ne v1, v2, :cond_8

    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->isRequired(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    goto :goto_3

    :cond_8
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_7

    const/4 p1, 0x1

    :goto_4
    if-eqz p1, :cond_9

    goto :goto_1

    :cond_9
    :goto_5
    return v4
.end method

.method private final itemSelectionChanged()V
    .locals 8

    .line 269
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->eventHandler:Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;

    .line 270
    iget-object v1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->items:Landroid/widget/LinearLayout;

    const-string v2, "items"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    const/4 v3, 0x1

    invoke-static {v3, v1}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 325
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 326
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v6

    .line 271
    iget-object v7, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->items:Landroid/widget/LinearLayout;

    if-nez v7, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v7, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_3

    check-cast v6, Lcom/squareup/activity/refund/ItemizationRow;

    invoke-virtual {v6}, Lcom/squareup/activity/refund/ItemizationRow;->getChecked()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.squareup.activity.refund.ItemizationRow"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 327
    :cond_4
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 328
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v4, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 329
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 330
    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    sub-int/2addr v4, v3

    .line 273
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 331
    :cond_5
    check-cast v1, Ljava/util/List;

    .line 269
    invoke-interface {v0, v1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;->onItemsSelected(Ljava/util/List;)V

    return-void
.end method

.method private final updateView(Lcom/squareup/activity/refund/RefundData;)V
    .locals 8

    .line 138
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->configureActionBar(Lcom/squareup/activity/refund/RefundData;)V

    .line 140
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getCanOnlyIssueAmountBasedRefund()Z

    move-result v0

    const/16 v1, 0x8

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->isExchange()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundTypeRadioGroup:Landroid/widget/RadioGroup;

    if-nez v0, :cond_1

    const-string v2, "refundTypeRadioGroup"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 145
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getItemizationDetails()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 146
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundAmountHelp:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_3

    const-string v2, "refundAmountHelp"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 149
    :cond_4
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->configureRefundType(Lcom/squareup/activity/refund/RefundData;)V

    .line 150
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->configureRefundItems(Lcom/squareup/activity/refund/RefundData;)V

    .line 151
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->configureRefundedItems(Lcom/squareup/activity/refund/RefundData;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    if-nez v0, :cond_5

    const-string v1, "maxMoneyScrubber"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundableAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/money/MaxMoneyScrubber;->setMax(Lcom/squareup/protos/common/Money;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundAmountMax:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_6

    const-string v1, "refundAmountMax"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 154
    :cond_6
    iget-object v1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/activity/R$string;->refund_amount_max:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 155
    iget-object v2, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundableAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "amount"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 156
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundAmountEditor:Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "refundAmountEditor"

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    iget-object v2, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    new-instance v3, Lcom/squareup/protos/common/Money;

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 160
    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getRefundableAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v0, :cond_8

    goto :goto_0

    :cond_8
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v2, v4

    if-nez v0, :cond_a

    .line 161
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundAmountEditor:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    iget-object v1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    new-instance v2, Lcom/squareup/protos/common/Money;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-direct {v2, v3, p1}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    :cond_a
    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 7

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 103
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->bindViews(Landroid/view/View;)V

    .line 105
    new-instance v0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$attach$1;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->eventHandler:Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;

    invoke-direct {v0, v1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$attach$1;-><init>(Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->amountButton:Lcom/squareup/widgets/ScalingRadioButton;

    if-nez v0, :cond_0

    const-string v1, "amountButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/activity/refund/RefundItemizationCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$attach$2;-><init>(Lcom/squareup/activity/refund/RefundItemizationCoordinator;Landroid/view/View;)V

    check-cast v1, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingRadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->itemsButton:Lcom/squareup/widgets/ScalingRadioButton;

    if-nez v0, :cond_1

    const-string v1, "itemsButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/activity/refund/RefundItemizationCoordinator$attach$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$attach$3;-><init>(Lcom/squareup/activity/refund/RefundItemizationCoordinator;Landroid/view/View;)V

    check-cast v1, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingRadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundAmountEditor:Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "refundAmountEditor"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 122
    :cond_2
    new-instance v2, Lcom/squareup/activity/refund/RefundAmountTextChangedListener;

    iget-object v3, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v4, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->eventHandler:Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;

    invoke-direct {v2, v3, v4}, Lcom/squareup/activity/refund/RefundAmountTextChangedListener;-><init>(Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;)V

    check-cast v2, Landroid/text/TextWatcher;

    .line 121
    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 124
    new-instance v0, Lcom/squareup/money/MaxMoneyScrubber;

    .line 125
    iget-object v2, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    check-cast v2, Lcom/squareup/money/MoneyExtractor;

    iget-object v3, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 126
    new-instance v4, Lcom/squareup/protos/common/Money;

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v4, v5, v6}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 124
    invoke-direct {v0, v2, v3, v4}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    .line 128
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->refundAmountEditor:Lcom/squareup/widgets/SelectableEditText;

    if-nez v2, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v2, Lcom/squareup/text/HasSelectableText;

    invoke-virtual {v0, v2}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v0

    .line 129
    iget-object v1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    if-nez v1, :cond_4

    const-string v2, "maxMoneyScrubber"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v1, Lcom/squareup/text/Scrubber;

    invoke-virtual {v0, v1}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->data:Lio/reactivex/Observable;

    .line 132
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 133
    new-instance v1, Lcom/squareup/activity/refund/RefundItemizationCoordinator$attach$4;

    move-object v2, p0

    check-cast v2, Lcom/squareup/activity/refund/RefundItemizationCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$attach$4;-><init>(Lcom/squareup/activity/refund/RefundItemizationCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/activity/refund/RefundItemizationCoordinator$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "data\n        .distinctUn\u2026 .subscribe(::updateView)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
