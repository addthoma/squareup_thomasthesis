.class Lcom/squareup/activity/ui/IssueReceiptCoordinator$5;
.super Lcom/squareup/text/ScrubbingTextWatcher;
.source "IssueReceiptCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/activity/ui/IssueReceiptCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/activity/ui/IssueReceiptCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator$5;->this$0:Lcom/squareup/activity/ui/IssueReceiptCoordinator;

    invoke-direct {p0, p2, p3}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 197
    invoke-super {p0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 198
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator$5;->this$0:Lcom/squareup/activity/ui/IssueReceiptCoordinator;

    invoke-static {v0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->access$000(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;->onEmailTextChanged(Ljava/lang/String;)V

    return-void
.end method
