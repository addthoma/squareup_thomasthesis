.class public final Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;
.super Lcom/squareup/activity/ui/IssueReceiptScreenData;
.source "IssueReceiptScreenData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/ui/IssueReceiptScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "IssueReceiptViewData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;
    }
.end annotation


# instance fields
.field public final emailReceiptVisible:Z

.field public final emailValid:Z

.field public final iconGlyphVisible:Z

.field public final inputsVisible:Z

.field public final printFormalReceiptVisible:Z

.field public final printReceiptVisible:Z

.field public final smsReceiptVisible:Z

.field public final smsValid:Z

.field public final subtitleVisible:Z

.field public switchLanguageVisible:Z

.field public final titleVisible:Z

.field public final upButtonTextId:I


# direct methods
.method public constructor <init>(IZZZZZZZZZZZ)V
    .locals 0

    .line 129
    invoke-direct {p0}, Lcom/squareup/activity/ui/IssueReceiptScreenData;-><init>()V

    .line 130
    iput p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->upButtonTextId:I

    .line 131
    iput-boolean p2, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->inputsVisible:Z

    .line 132
    iput-boolean p3, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->emailReceiptVisible:Z

    .line 133
    iput-boolean p4, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->smsReceiptVisible:Z

    .line 134
    iput-boolean p5, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->printReceiptVisible:Z

    .line 135
    iput-boolean p6, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->printFormalReceiptVisible:Z

    .line 136
    iput-boolean p7, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->iconGlyphVisible:Z

    .line 137
    iput-boolean p8, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->titleVisible:Z

    .line 138
    iput-boolean p9, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->subtitleVisible:Z

    .line 139
    iput-boolean p10, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->emailValid:Z

    .line 140
    iput-boolean p11, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->smsValid:Z

    .line 141
    iput-boolean p12, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->switchLanguageVisible:Z

    return-void
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;
    .locals 1

    .line 145
    new-instance v0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;-><init>(Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;)V

    return-object v0
.end method
