.class public Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;
.super Ljava/lang/Object;
.source "IssueReceiptCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/ui/IssueReceiptCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final device:Lcom/squareup/util/Device;

.field private final phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/text/InsertingScrubber;Ljavax/inject/Provider;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/text/InsertingScrubber;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;->device:Lcom/squareup/util/Device;

    .line 102
    iput-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    .line 103
    iput-object p3, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;->countryCodeProvider:Ljavax/inject/Provider;

    .line 104
    iput-object p4, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method


# virtual methods
.method public create(Lrx/Observable;Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;)Lcom/squareup/activity/ui/IssueReceiptCoordinator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/squareup/activity/ui/IssueReceiptScreenData;",
            ">;",
            "Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;",
            ")",
            "Lcom/squareup/activity/ui/IssueReceiptCoordinator;"
        }
    .end annotation

    .line 109
    new-instance v7, Lcom/squareup/activity/ui/IssueReceiptCoordinator;

    iget-object v1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;->device:Lcom/squareup/util/Device;

    iget-object v4, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    iget-object v5, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;->countryCodeProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    move-object v0, v7

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/activity/ui/IssueReceiptCoordinator;-><init>(Lcom/squareup/util/Device;Lrx/Observable;Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;Lcom/squareup/text/InsertingScrubber;Ljavax/inject/Provider;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V

    return-object v7
.end method
