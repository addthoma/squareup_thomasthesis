.class public Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;
.super Ljava/lang/Object;
.source "IssueReceiptScreenData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private emailReceiptVisible:Z

.field private emailValid:Z

.field private iconGlyphVisible:Z

.field private inputsVisible:Z

.field private printFormalReceiptVisible:Z

.field private printReceiptVisible:Z

.field private smsReceiptVisible:Z

.field private smsValid:Z

.field private subtitleVisible:Z

.field private switchLanguageVisible:Z

.field private titleVisible:Z

.field private upButtonTextId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;)V
    .locals 1

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    iget v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->upButtonTextId:I

    iput v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->upButtonTextId:I

    .line 167
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->inputsVisible:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->inputsVisible:Z

    .line 168
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->emailReceiptVisible:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->emailReceiptVisible:Z

    .line 169
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->smsReceiptVisible:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->smsReceiptVisible:Z

    .line 170
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->printReceiptVisible:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->printReceiptVisible:Z

    .line 171
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->printFormalReceiptVisible:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->printFormalReceiptVisible:Z

    .line 172
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->iconGlyphVisible:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->iconGlyphVisible:Z

    .line 173
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->titleVisible:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->titleVisible:Z

    .line 174
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->subtitleVisible:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->subtitleVisible:Z

    .line 175
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->emailValid:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->emailValid:Z

    .line 176
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->smsValid:Z

    iput-boolean v0, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->smsValid:Z

    .line 177
    iget-boolean p1, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->switchLanguageVisible:Z

    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->switchLanguageVisible:Z

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;
    .locals 14

    .line 231
    new-instance v13, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;

    iget v1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->upButtonTextId:I

    iget-boolean v2, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->inputsVisible:Z

    iget-boolean v3, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->emailReceiptVisible:Z

    iget-boolean v4, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->smsReceiptVisible:Z

    iget-boolean v5, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->printReceiptVisible:Z

    iget-boolean v6, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->printFormalReceiptVisible:Z

    iget-boolean v7, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->iconGlyphVisible:Z

    iget-boolean v8, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->titleVisible:Z

    iget-boolean v9, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->subtitleVisible:Z

    iget-boolean v10, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->emailValid:Z

    iget-boolean v11, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->smsValid:Z

    iget-boolean v12, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->switchLanguageVisible:Z

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;-><init>(IZZZZZZZZZZZ)V

    return-object v13
.end method

.method public setEmailReceiptVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;
    .locals 0

    .line 186
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->emailReceiptVisible:Z

    return-object p0
.end method

.method public setEmailValid(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;
    .locals 0

    .line 216
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->emailValid:Z

    return-object p0
.end method

.method public setIconGlyphVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;
    .locals 0

    .line 206
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->iconGlyphVisible:Z

    return-object p0
.end method

.method public setInputsVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;
    .locals 0

    .line 211
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->inputsVisible:Z

    return-object p0
.end method

.method public setPrintFormalReceiptVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;
    .locals 0

    .line 201
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->printFormalReceiptVisible:Z

    return-object p0
.end method

.method public setPrintReceiptVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;
    .locals 0

    .line 196
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->printReceiptVisible:Z

    return-object p0
.end method

.method public setSmsReceiptVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;
    .locals 0

    .line 191
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->smsReceiptVisible:Z

    return-object p0
.end method

.method public setSmsValid(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;
    .locals 0

    .line 221
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->smsValid:Z

    return-object p0
.end method

.method public setSwitchLanguageVisible(Z)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;
    .locals 0

    .line 226
    iput-boolean p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->switchLanguageVisible:Z

    return-object p0
.end method

.method public setUpButtonTextId(I)Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;
    .locals 0

    .line 181
    iput p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData$Builder;->upButtonTextId:I

    return-object p0
.end method
