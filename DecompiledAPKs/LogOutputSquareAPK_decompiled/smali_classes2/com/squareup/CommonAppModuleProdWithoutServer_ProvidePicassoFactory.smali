.class public final Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;
.super Ljava/lang/Object;
.source "CommonAppModuleProdWithoutServer_ProvidePicassoFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/picasso/Picasso;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final memoryCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Cache;",
            ">;"
        }
    .end annotation
.end field

.field private final transformerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Cache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;->contextProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;->memoryCacheProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;->transformerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Cache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;",
            ">;)",
            "Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providePicasso(Landroid/app/Application;Lcom/squareup/picasso/Cache;Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;)Lcom/squareup/picasso/Picasso;
    .locals 0

    .line 48
    invoke-static {p0, p1, p2}, Lcom/squareup/CommonAppModuleProdWithoutServer;->providePicasso(Landroid/app/Application;Lcom/squareup/picasso/Cache;Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;)Lcom/squareup/picasso/Picasso;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/picasso/Picasso;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/picasso/Picasso;
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;->memoryCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/picasso/Cache;

    iget-object v2, p0, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;->transformerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;

    invoke-static {v0, v1, v2}, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;->providePicasso(Landroid/app/Application;Lcom/squareup/picasso/Cache;Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvidePicassoFactory;->get()Lcom/squareup/picasso/Picasso;

    move-result-object v0

    return-object v0
.end method
