.class public final Lcom/squareup/bugreport/BatteryStatus$Plugged$Companion;
.super Ljava/lang/Object;
.source "BatteryStatus.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/bugreport/BatteryStatus$Plugged;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/bugreport/BatteryStatus$Plugged$Companion;",
        "",
        "()V",
        "fromBatteryManagerPluggedExtra",
        "Lcom/squareup/bugreport/BatteryStatus$Plugged;",
        "value",
        "",
        "bugreport_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/bugreport/BatteryStatus$Plugged$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromBatteryManagerPluggedExtra(I)Lcom/squareup/bugreport/BatteryStatus$Plugged;
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    .line 87
    sget-object p1, Lcom/squareup/bugreport/BatteryStatus$Plugged;->UNKNOWN:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    goto :goto_0

    .line 86
    :cond_0
    sget-object p1, Lcom/squareup/bugreport/BatteryStatus$Plugged;->WIRELESS:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    goto :goto_0

    .line 85
    :cond_1
    sget-object p1, Lcom/squareup/bugreport/BatteryStatus$Plugged;->USB:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    goto :goto_0

    .line 84
    :cond_2
    sget-object p1, Lcom/squareup/bugreport/BatteryStatus$Plugged;->AC:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    :goto_0
    return-object p1
.end method
