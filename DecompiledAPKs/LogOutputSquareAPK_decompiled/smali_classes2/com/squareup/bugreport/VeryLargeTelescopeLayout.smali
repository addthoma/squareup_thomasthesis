.class public final Lcom/squareup/bugreport/VeryLargeTelescopeLayout;
.super Lcom/mattprecious/telescope/TelescopeLayout;
.source "VeryLargeTelescopeLayout.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/bugreport/VeryLargeTelescopeLayout$Component;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nVeryLargeTelescopeLayout.kt\nKotlin\n*S Kotlin\n*F\n+ 1 VeryLargeTelescopeLayout.kt\ncom/squareup/bugreport/VeryLargeTelescopeLayout\n*L\n1#1,35:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001\u0007B\u001b\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/bugreport/VeryLargeTelescopeLayout;",
        "Lcom/mattprecious/telescope/TelescopeLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "Component",
        "bugreport_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1, v0}, Lcom/squareup/bugreport/VeryLargeTelescopeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/mattprecious/telescope/TelescopeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x1

    .line 22
    invoke-virtual {p0, p2}, Lcom/squareup/bugreport/VeryLargeTelescopeLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "context.applicationContext"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-class p2, Lcom/squareup/bugreport/VeryLargeTelescopeLayout$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/bugreport/VeryLargeTelescopeLayout$Component;

    .line 25
    invoke-interface {p1}, Lcom/squareup/bugreport/VeryLargeTelescopeLayout$Component;->veryLargeTelescopeLayoutConfig()Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig;

    move-result-object p1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/bugreport/VeryLargeTelescopeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-virtual {p0, p2}, Lcom/squareup/bugreport/VeryLargeTelescopeLayout;->setProgressColor(I)V

    .line 28
    new-instance p2, Lcom/squareup/bugreport/VeryLargeTelescopeLayout$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/bugreport/VeryLargeTelescopeLayout$1;-><init>(Lcom/squareup/bugreport/VeryLargeTelescopeLayout;Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig;)V

    check-cast p2, Lcom/mattprecious/telescope/Lens;

    invoke-virtual {p0, p2}, Lcom/squareup/bugreport/VeryLargeTelescopeLayout;->setLens(Lcom/mattprecious/telescope/Lens;)V

    .line 32
    invoke-interface {p1}, Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig;->getMode()Lcom/mattprecious/telescope/ScreenshotMode;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/squareup/bugreport/VeryLargeTelescopeLayout;->setScreenshotMode(Lcom/mattprecious/telescope/ScreenshotMode;)V

    :cond_0
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 14
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/bugreport/VeryLargeTelescopeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method
