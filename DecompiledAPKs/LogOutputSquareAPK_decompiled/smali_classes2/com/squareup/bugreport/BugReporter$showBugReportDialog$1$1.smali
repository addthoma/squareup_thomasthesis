.class final Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1$1;
.super Ljava/lang/Object;
.source "BugReporter.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/bugreport/SendResponse;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "response",
        "Lcom/squareup/bugreport/SendResponse;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1$1;

    invoke-direct {v0}, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1$1;-><init>()V

    sput-object v0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1$1;->INSTANCE:Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/bugreport/SendResponse;)V
    .locals 1

    .line 71
    invoke-virtual {p1}, Lcom/squareup/bugreport/SendResponse;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/bugreport/SendResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1$1;->accept(Lcom/squareup/bugreport/SendResponse;)V

    return-void
.end method
