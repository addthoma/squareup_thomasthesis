.class public interface abstract Lcom/squareup/backgroundjob/JobParams;
.super Ljava/lang/Object;
.source "JobParams.java"


# virtual methods
.method public abstract getEndMs()J
.end method

.method public abstract getExtras()Lcom/evernote/android/job/util/support/PersistableBundleCompat;
.end method

.method public abstract getId()I
.end method

.method public abstract getScheduledAt()J
.end method

.method public abstract getStartMs()J
.end method

.method public abstract getTag()Ljava/lang/String;
.end method
