.class public Lcom/squareup/backgroundjob/mock/MockJob;
.super Lcom/squareup/backgroundjob/BackgroundJob;
.source "MockJob.java"


# instance fields
.field private final job:Lcom/evernote/android/job/Job;

.field private final mockId:Ljava/util/UUID;


# direct methods
.method constructor <init>(Lcom/evernote/android/job/Job;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)V
    .locals 0

    .line 25
    invoke-direct {p0, p2}, Lcom/squareup/backgroundjob/BackgroundJob;-><init>(Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)V

    .line 26
    iput-object p1, p0, Lcom/squareup/backgroundjob/mock/MockJob;->job:Lcom/evernote/android/job/Job;

    .line 27
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/backgroundjob/mock/MockJob;->mockId:Ljava/util/UUID;

    return-void
.end method


# virtual methods
.method public canceled()Z
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/backgroundjob/mock/MockJob;->isCanceled()Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    .line 45
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 47
    :cond_1
    check-cast p1, Lcom/squareup/backgroundjob/mock/MockJob;

    .line 48
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockJob;->mockId:Ljava/util/UUID;

    iget-object p1, p1, Lcom/squareup/backgroundjob/mock/MockJob;->mockId:Ljava/util/UUID;

    invoke-virtual {v0, p1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public getJob()Lcom/evernote/android/job/Job;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockJob;->job:Lcom/evernote/android/job/Job;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockJob;->mockId:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->hashCode()I

    move-result v0

    return v0
.end method

.method public runJob(Lcom/squareup/backgroundjob/JobParams;)Lcom/evernote/android/job/Job$Result;
    .locals 0

    .line 40
    sget-object p1, Lcom/evernote/android/job/Job$Result;->SUCCESS:Lcom/evernote/android/job/Job$Result;

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "job{mockId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/backgroundjob/mock/MockJob;->mockId:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", id=unknown, finished="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    invoke-virtual {p0}, Lcom/squareup/backgroundjob/mock/MockJob;->isFinished()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", result=unknown, canceled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/backgroundjob/mock/MockJob;->isCanceled()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", periodic=unknown, class="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tag=unknown}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
