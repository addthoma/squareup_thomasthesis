.class public final Lcom/squareup/CommonLoggedInModule_ProvidePaperSignatureCacheSettingsFactory;
.super Ljava/lang/Object;
.source "CommonLoggedInModule_ProvidePaperSignatureCacheSettingsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/LocalSetting<",
        "Ljava/util/Map<",
        "Lcom/squareup/protos/client/IdPair;",
        "Lcom/squareup/protos/common/Money;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/CommonLoggedInModule_ProvidePaperSignatureCacheSettingsFactory;->preferencesProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/CommonLoggedInModule_ProvidePaperSignatureCacheSettingsFactory;->gsonProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/CommonLoggedInModule_ProvidePaperSignatureCacheSettingsFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)",
            "Lcom/squareup/CommonLoggedInModule_ProvidePaperSignatureCacheSettingsFactory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/CommonLoggedInModule_ProvidePaperSignatureCacheSettingsFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/CommonLoggedInModule_ProvidePaperSignatureCacheSettingsFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providePaperSignatureCacheSettings(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation

    .line 45
    invoke-static {p0, p1}, Lcom/squareup/CommonLoggedInModule;->providePaperSignatureCacheSettings(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/LocalSetting;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/CommonLoggedInModule_ProvidePaperSignatureCacheSettingsFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/CommonLoggedInModule_ProvidePaperSignatureCacheSettingsFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/gson/Gson;

    invoke-static {v0, v1}, Lcom/squareup/CommonLoggedInModule_ProvidePaperSignatureCacheSettingsFactory;->providePaperSignatureCacheSettings(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/CommonLoggedInModule_ProvidePaperSignatureCacheSettingsFactory;->get()Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method
