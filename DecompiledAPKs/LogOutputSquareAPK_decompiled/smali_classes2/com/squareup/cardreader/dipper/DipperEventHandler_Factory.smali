.class public final Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;
.super Ljava/lang/Object;
.source "DipperEventHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/dipper/DipperEventHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final activeCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final dippedCardTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final localCardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcReaderHasConnectedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final r12HasConnectedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final r6HasConnectedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final readerEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final secureSessionServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionService;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final userCountryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 86
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 87
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 88
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 89
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 90
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 91
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 92
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 93
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 94
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 95
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->localCardReaderListenersProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 96
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->nfcReaderHasConnectedProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 97
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->r6HasConnectedProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 98
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->r12HasConnectedProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 99
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->readerEventLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 100
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 101
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 102
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->secureSessionServiceProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 103
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 104
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->userCountryCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    .line 130
    new-instance v20, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;

    move-object/from16 v0, v20

    invoke-direct/range {v0 .. v19}, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v20
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/cardreader/SecureSessionService;Lcom/squareup/payment/Transaction;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/DipperEventHandler;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/log/ReaderEventLogger;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/cardreader/SecureSessionService;",
            "Lcom/squareup/payment/Transaction;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Lcom/squareup/cardreader/dipper/DipperEventHandler;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    .line 143
    new-instance v20, Lcom/squareup/cardreader/dipper/DipperEventHandler;

    move-object/from16 v0, v20

    invoke-direct/range {v0 .. v19}, Lcom/squareup/cardreader/dipper/DipperEventHandler;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/cardreader/SecureSessionService;Lcom/squareup/payment/Transaction;Ljavax/inject/Provider;)V

    return-object v20
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/dipper/DipperEventHandler;
    .locals 21

    move-object/from16 v0, p0

    .line 109
    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/payment/TenderInEdit;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/cardreader/DippedCardTracker;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->localCardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->nfcReaderHasConnectedProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/settings/LocalSetting;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->r6HasConnectedProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/settings/LocalSetting;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->r12HasConnectedProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/settings/LocalSetting;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->readerEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/log/ReaderEventLogger;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/thread/executor/MainThread;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->secureSessionServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/cardreader/SecureSessionService;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->userCountryCodeProvider:Ljavax/inject/Provider;

    move-object/from16 v20, v1

    invoke-static/range {v2 .. v20}, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/cardreader/SecureSessionService;Lcom/squareup/payment/Transaction;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/DipperEventHandler;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/DipperEventHandler_Factory;->get()Lcom/squareup/cardreader/dipper/DipperEventHandler;

    move-result-object v0

    return-object v0
.end method
