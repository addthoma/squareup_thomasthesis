.class Lcom/squareup/cardreader/dipper/DipperEventHandler$TimeoutRunnable;
.super Lcom/squareup/util/RunnableOnce;
.source "DipperEventHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/dipper/DipperEventHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TimeoutRunnable"
.end annotation


# instance fields
.field private final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field final synthetic this$0:Lcom/squareup/cardreader/dipper/DipperEventHandler;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/dipper/DipperEventHandler;Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 498
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler$TimeoutRunnable;->this$0:Lcom/squareup/cardreader/dipper/DipperEventHandler;

    invoke-direct {p0}, Lcom/squareup/util/RunnableOnce;-><init>()V

    .line 499
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler$TimeoutRunnable;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/dipper/DipperEventHandler;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/dipper/DipperEventHandler$1;)V
    .locals 0

    .line 495
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/dipper/DipperEventHandler$TimeoutRunnable;-><init>(Lcom/squareup/cardreader/dipper/DipperEventHandler;Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method


# virtual methods
.method protected runOnce()V
    .locals 3

    .line 503
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler$TimeoutRunnable;->this$0:Lcom/squareup/cardreader/dipper/DipperEventHandler;

    invoke-static {v0}, Lcom/squareup/cardreader/dipper/DipperEventHandler;->access$100(Lcom/squareup/cardreader/dipper/DipperEventHandler;)Lcom/squareup/log/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler$TimeoutRunnable;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->INIT_READER_CONNECTION_TIMEOUT:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 504
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler$TimeoutRunnable;->this$0:Lcom/squareup/cardreader/dipper/DipperEventHandler;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/dipper/DipperEventHandler;->access$202(Lcom/squareup/cardreader/dipper/DipperEventHandler;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    return-void
.end method
