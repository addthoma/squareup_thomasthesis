.class Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FailureToReconnectRunner;
.super Ljava/lang/Object;
.source "FirmwareUpdateDispatcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FailureToReconnectRunner"
.end annotation


# instance fields
.field private final address:Ljava/lang/String;

.field private final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field final synthetic this$0:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 510
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FailureToReconnectRunner;->this$0:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 511
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FailureToReconnectRunner;->address:Ljava/lang/String;

    .line 512
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FailureToReconnectRunner;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 516
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FailureToReconnectRunner;->this$0:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FailureToReconnectRunner;->address:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->access$600(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Ljava/lang/String;)V

    .line 517
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FailureToReconnectRunner;->this$0:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    invoke-static {v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->access$700(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

    .line 518
    iget-object v2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FailureToReconnectRunner;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v1, v2}, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;->onReaderFailedToConnectAfterRebootingFwup(Lcom/squareup/cardreader/CardReaderInfo;)V

    goto :goto_0

    :cond_0
    return-void
.end method
