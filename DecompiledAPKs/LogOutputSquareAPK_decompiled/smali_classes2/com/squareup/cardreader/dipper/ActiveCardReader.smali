.class public Lcom/squareup/cardreader/dipper/ActiveCardReader;
.super Ljava/lang/Object;
.source "ActiveCardReader.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;
.implements Lmortar/Scoped;


# instance fields
.field private activeCardReader:Lcom/squareup/cardreader/CardReader;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderHub;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    return-void
.end method


# virtual methods
.method public cancelPayment()V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->activeCardReader:Lcom/squareup/cardreader/CardReader;

    if-eqz v0, :cond_0

    .line 116
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->cancelPayment()V

    :cond_0
    return-void
.end method

.method public cancelPaymentsOnNonActiveCardReaders()V
    .locals 4

    .line 101
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    .line 102
    iget-object v2, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->activeCardReader:Lcom/squareup/cardreader/CardReader;

    if-eqz v2, :cond_1

    .line 103
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->activeCardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v3}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/cardreader/CardReaderId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 108
    :cond_1
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 109
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->cancelPayment()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public getActiveCardReader()Lcom/squareup/cardreader/CardReader;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->activeCardReader:Lcom/squareup/cardreader/CardReader;

    return-object v0
.end method

.method public getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->activeCardReader:Lcom/squareup/cardreader/CardReader;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getMostRecentActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object v0
.end method

.method public hasActiveCardReader()Z
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->activeCardReader:Lcom/squareup/cardreader/CardReader;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->activeCardReader:Lcom/squareup/cardreader/CardReader;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderId;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isOtherActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z
    .locals 1

    .line 57
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->isActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    .line 128
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 132
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {p1, p0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method

.method public setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z
    .locals 5

    .line 66
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 67
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    const-string v1, "cardReaderId"

    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    new-array v4, v2, [Ljava/lang/Object;

    aput-object p1, v4, v1

    const-string v1, "There is no attached card reader with id %s"

    .line 68
    invoke-static {v3, v1, v4}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 70
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->activeCardReader:Lcom/squareup/cardreader/CardReader;

    if-nez v1, :cond_1

    .line 71
    iput-object v0, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->activeCardReader:Lcom/squareup/cardreader/CardReader;

    .line 72
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return v2

    .line 77
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderId;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z
    .locals 2

    .line 86
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const-string v0, "cardReaderId"

    .line 87
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 89
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 92
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderId;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 93
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ActiveCardReader;->activeCardReader:Lcom/squareup/cardreader/CardReader;

    const/4 p1, 0x1

    return p1

    :cond_1
    return v1
.end method
