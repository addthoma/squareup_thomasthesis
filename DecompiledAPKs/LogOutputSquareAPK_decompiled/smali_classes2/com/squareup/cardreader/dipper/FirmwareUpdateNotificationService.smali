.class public Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;
.super Landroid/app/Service;
.source "FirmwareUpdateNotificationService.java"

# interfaces
.implements Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService$Component;
    }
.end annotation


# instance fields
.field firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

.field private notificationManager:Landroid/app/NotificationManager;

.field notificationWrapper:Lcom/squareup/notification/NotificationWrapper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private createNotification()Landroid/app/Notification;
    .locals 3

    const-string v0, "notification"

    .line 109
    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->notificationManager:Landroid/app/NotificationManager;

    .line 111
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    sget-object v1, Lcom/squareup/notification/Channels;->UPDATES:Lcom/squareup/notification/Channels;

    invoke-virtual {v0, p0, v1}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cardreader/R$string;->firmware_update_notification_title:I

    .line 112
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    const-string v1, "progress"

    .line 113
    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setCategory(Ljava/lang/String;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 114
    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v2, 0x1

    .line 115
    invoke-virtual {v0, v2}, Landroidx/core/app/NotificationCompat$Builder;->setOngoing(Z)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    const/16 v2, 0x64

    .line 116
    invoke-virtual {v0, v2, v1, v1}, Landroidx/core/app/NotificationCompat$Builder;->setProgress(IIZ)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    .line 117
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    invoke-virtual {v0}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method private updateProgressNotification(I)V
    .locals 3

    .line 121
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/16 v1, 0x64

    const/4 v2, 0x0

    .line 126
    invoke-virtual {v0, v1, p1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setProgress(IIZ)Landroidx/core/app/NotificationCompat$Builder;

    .line 127
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->percentageFormatter:Lcom/squareup/text/Formatter;

    invoke-static {p1}, Lcom/squareup/util/Percentage;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    .line 128
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->notificationManager:Landroid/app/NotificationManager;

    sget v0, Lcom/squareup/cardreader/R$id;->firmware_update_notification_service:I

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    .line 129
    invoke-virtual {v1}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 128
    invoke-virtual {p1, v0, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreate()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 48
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "FirmwareUpdateNotificationService#onCreate %d"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    invoke-static {}, Lcom/squareup/mortar/AppContextWrapper;->appContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService$Component;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService$Component;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService$Component;->inject(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;)V

    .line 50
    sget v0, Lcom/squareup/cardreader/R$id;->firmware_update_notification_service:I

    invoke-direct {p0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->createNotification()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->startForeground(ILandroid/app/Notification;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->addFirmwareUpdateListener(Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;)V

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 55
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "FirmwareUpdateNotificationService#onDestroy %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->stopSelf()V

    .line 58
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->removeFirmwareUpdateListener(Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;)V

    const/4 v0, 0x0

    .line 59
    iput-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->notificationManager:Landroid/app/NotificationManager;

    .line 60
    iput-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->notificationBuilder:Landroidx/core/app/NotificationCompat$Builder;

    return-void
.end method

.method public onFirmwareManifestServerResponseFailure(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 75
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    const-string v0, "FirmwareUpdateNotificationService#onFirmwareManifestServerResponseFailure %d"

    .line 74
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->stopSelf()V

    return-void
.end method

.method public onFirmwareManifestServerResponseSuccess(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 70
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    const-string v0, "FirmwareUpdateNotificationService#onFirmwareManifestServerResponseSuccess %d"

    .line 69
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onFirmwareUpdateAborted(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 0

    .line 93
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->stopSelf()V

    return-void
.end method

.method public onFirmwareUpdateComplete(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 0

    .line 89
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->stopSelf()V

    return-void
.end method

.method public onFirmwareUpdateError(Lcom/squareup/cardreader/CardReaderInfo;ZLcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V
    .locals 0

    .line 98
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->stopSelf()V

    return-void
.end method

.method public onFirmwareUpdateProgress(Lcom/squareup/cardreader/CardReaderInfo;ZIIZ)V
    .locals 0

    .line 84
    invoke-direct {p0, p3}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->updateProgressNotification(I)V

    return-void
.end method

.method public onFirmwareUpdateStarted(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 0

    return-void
.end method

.method public onReaderFailedToConnectAfterRebootingFwup(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 102
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->stopSelf()V

    return-void
.end method

.method public onSendFirmwareManifestToServer(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 65
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    const-string v0, "FirmwareUpdateNotificationService#onSendFirmwareManifestToServer %d"

    .line 64
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
