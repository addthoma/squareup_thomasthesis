.class public abstract Lcom/squareup/cardreader/dipper/CardReaderUIDipperModule;
.super Ljava/lang/Object;
.source "CardReaderUIDipperModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideEmvDipScreenHandler(Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;)Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
