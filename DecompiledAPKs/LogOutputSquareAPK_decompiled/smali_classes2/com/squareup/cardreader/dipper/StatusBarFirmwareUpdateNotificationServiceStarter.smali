.class public Lcom/squareup/cardreader/dipper/StatusBarFirmwareUpdateNotificationServiceStarter;
.super Ljava/lang/Object;
.source "StatusBarFirmwareUpdateNotificationServiceStarter.java"

# interfaces
.implements Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationServiceStarter;


# instance fields
.field private final application:Landroid/app/Application;


# direct methods
.method constructor <init>(Landroid/app/Application;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/StatusBarFirmwareUpdateNotificationServiceStarter;->application:Landroid/app/Application;

    return-void
.end method


# virtual methods
.method public startFirmwareUpdateNotificationService()V
    .locals 2

    .line 18
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/StatusBarFirmwareUpdateNotificationServiceStarter;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;

    invoke-static {v0, v1}, Lcom/squareup/util/Contexts;->startForegroundService(Landroid/content/Context;Ljava/lang/Class;)V

    return-void
.end method
