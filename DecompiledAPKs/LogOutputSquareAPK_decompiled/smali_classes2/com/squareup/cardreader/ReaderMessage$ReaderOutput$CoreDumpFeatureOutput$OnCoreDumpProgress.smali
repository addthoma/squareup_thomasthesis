.class public final Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnCoreDumpProgress"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\'\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput;",
        "bytesReceived",
        "",
        "bytesSoFar",
        "bytesTotal",
        "(III)V",
        "getBytesReceived",
        "()I",
        "getBytesSoFar",
        "getBytesTotal",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bytesReceived:I

.field private final bytesSoFar:I

.field private final bytesTotal:I


# direct methods
.method public constructor <init>(III)V
    .locals 1

    const/4 v0, 0x0

    .line 294
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesReceived:I

    iput p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesSoFar:I

    iput p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesTotal:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;IIIILjava/lang/Object;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesReceived:I

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesSoFar:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesTotal:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->copy(III)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesReceived:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesSoFar:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesTotal:I

    return v0
.end method

.method public final copy(III)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;
    .locals 1

    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;-><init>(III)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesReceived:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesReceived:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesSoFar:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesSoFar:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesTotal:I

    iget p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesTotal:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBytesReceived()I
    .locals 1

    .line 291
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesReceived:I

    return v0
.end method

.method public final getBytesSoFar()I
    .locals 1

    .line 292
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesSoFar:I

    return v0
.end method

.method public final getBytesTotal()I
    .locals 1

    .line 293
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesTotal:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesReceived:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesSoFar:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesTotal:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnCoreDumpProgress(bytesReceived="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesReceived:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", bytesSoFar="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesSoFar:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", bytesTotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput$OnCoreDumpProgress;->bytesTotal:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
