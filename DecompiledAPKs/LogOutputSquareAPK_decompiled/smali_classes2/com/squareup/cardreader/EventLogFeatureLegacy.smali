.class public Lcom/squareup/cardreader/EventLogFeatureLegacy;
.super Ljava/lang/Object;
.source "EventLogFeatureLegacy.java"

# interfaces
.implements Lcom/squareup/cardreader/EventLogFeature;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/EventLogFeatureLegacy$EventLogFeatureListener;
    }
.end annotation


# instance fields
.field private apiListener:Lcom/squareup/cardreader/EventLogFeatureLegacy$EventLogFeatureListener;

.field private eventLogFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

.field private final eventlogFeatureNative:Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;

.field private final sessionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;",
            ")V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/cardreader/EventLogFeatureLegacy;->sessionProvider:Ljavax/inject/Provider;

    .line 19
    iput-object p2, p0, Lcom/squareup/cardreader/EventLogFeatureLegacy;->eventlogFeatureNative:Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;

    return-void
.end method


# virtual methods
.method public initialize(Lcom/squareup/cardreader/EventLogFeatureLegacy$EventLogFeatureListener;)V
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/cardreader/EventLogFeatureLegacy;->eventLogFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    .line 30
    iput-object p1, p0, Lcom/squareup/cardreader/EventLogFeatureLegacy;->apiListener:Lcom/squareup/cardreader/EventLogFeatureLegacy$EventLogFeatureListener;

    .line 31
    iget-object p1, p0, Lcom/squareup/cardreader/EventLogFeatureLegacy;->eventlogFeatureNative:Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;

    iget-object v0, p0, Lcom/squareup/cardreader/EventLogFeatureLegacy;->sessionProvider:Ljavax/inject/Provider;

    .line 32
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    .line 31
    invoke-interface {p1, v0, p0}, Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;->eventlog_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/EventLogFeatureLegacy;->eventLogFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    return-void

    .line 27
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "apiListener cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 24
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "EventLogFeature is already initialized!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onEventLogReceived(JIILjava/lang/String;)V
    .locals 8

    .line 50
    iget-object v0, p0, Lcom/squareup/cardreader/EventLogFeatureLegacy;->apiListener:Lcom/squareup/cardreader/EventLogFeatureLegacy$EventLogFeatureListener;

    new-instance v7, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;

    move-object v1, v7

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;-><init>(JIILjava/lang/String;)V

    invoke-interface {v0, v7}, Lcom/squareup/cardreader/EventLogFeatureLegacy$EventLogFeatureListener;->onEventLogReceived(Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;)V

    return-void
.end method

.method public resetEventLogFeature()V
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/cardreader/EventLogFeatureLegacy;->eventLogFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    if-eqz v0, :cond_0

    .line 37
    iget-object v1, p0, Lcom/squareup/cardreader/EventLogFeatureLegacy;->eventlogFeatureNative:Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;->cr_eventlog_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;)Lcom/squareup/cardreader/lcr/CrEventLogResult;

    .line 38
    iget-object v0, p0, Lcom/squareup/cardreader/EventLogFeatureLegacy;->eventlogFeatureNative:Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/EventLogFeatureLegacy;->eventLogFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;->cr_eventlog_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;)Lcom/squareup/cardreader/lcr/CrEventLogResult;

    const/4 v0, 0x0

    .line 39
    iput-object v0, p0, Lcom/squareup/cardreader/EventLogFeatureLegacy;->eventLogFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    .line 40
    iput-object v0, p0, Lcom/squareup/cardreader/EventLogFeatureLegacy;->apiListener:Lcom/squareup/cardreader/EventLogFeatureLegacy$EventLogFeatureListener;

    :cond_0
    return-void
.end method
