.class public interface abstract Lcom/squareup/cardreader/SingleCardreaderMessenger;
.super Ljava/lang/Object;
.source "CardreaderMessengerInterface.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0007\u001a\u00020\u0008H&J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH&R\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/cardreader/SingleCardreaderMessenger;",
        "",
        "responses",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;",
        "getResponses",
        "()Lio/reactivex/Observable;",
        "connectionId",
        "Lcom/squareup/cardreader/CardreaderConnectionId;",
        "send",
        "",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract connectionId()Lcom/squareup/cardreader/CardreaderConnectionId;
.end method

.method public abstract getResponses()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;",
            ">;"
        }
    .end annotation
.end method

.method public abstract send(Lcom/squareup/cardreader/ReaderMessage$ReaderInput;)V
.end method
