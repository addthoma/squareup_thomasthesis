.class public final Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;
.super Ljava/lang/Object;
.source "LcrModule_ProvidePaymentFeatureFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/PaymentFeatureLegacy;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final delegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureV2;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private final tmnTimingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureV2;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->sessionProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p2, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->paymentFeatureNativeProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->tmnTimingsProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p7, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->delegateProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureV2;",
            ">;)",
            "Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;"
        }
    .end annotation

    .line 60
    new-instance v8, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static providePaymentFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;Lcom/squareup/tmn/TmnTimings;Lcom/squareup/cardreader/PaymentFeatureV2;)Lcom/squareup/cardreader/PaymentFeatureLegacy;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;",
            "Lcom/squareup/tmn/TmnTimings;",
            "Lcom/squareup/cardreader/PaymentFeatureV2;",
            ")",
            "Lcom/squareup/cardreader/PaymentFeatureLegacy;"
        }
    .end annotation

    .line 68
    invoke-static/range {p0 .. p6}, Lcom/squareup/cardreader/LcrModule;->providePaymentFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;Lcom/squareup/tmn/TmnTimings;Lcom/squareup/cardreader/PaymentFeatureV2;)Lcom/squareup/cardreader/PaymentFeatureLegacy;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/PaymentFeatureLegacy;
    .locals 7

    .line 51
    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->sessionProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v2, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v3, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    iget-object v4, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->paymentFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v5, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->tmnTimingsProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/tmn/TmnTimings;

    iget-object v6, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->delegateProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/cardreader/PaymentFeatureV2;

    invoke-static/range {v0 .. v6}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->providePaymentFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;Lcom/squareup/tmn/TmnTimings;Lcom/squareup/cardreader/PaymentFeatureV2;)Lcom/squareup/cardreader/PaymentFeatureLegacy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->get()Lcom/squareup/cardreader/PaymentFeatureLegacy;

    move-result-object v0

    return-object v0
.end method
