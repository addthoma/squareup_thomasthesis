.class Lcom/squareup/cardreader/CoreDumpFeatureLegacy;
.super Ljava/lang/Object;
.source "CoreDumpFeatureLegacy.java"

# interfaces
.implements Lcom/squareup/cardreader/CoreDumpFeature;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;
    }
.end annotation


# instance fields
.field private apiListener:Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;

.field private coreDumpFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

.field private final coredumpFeatureNative:Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;

.field private final sessionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;",
            ")V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->sessionProvider:Ljavax/inject/Provider;

    .line 18
    iput-object p2, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coredumpFeatureNative:Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;

    return-void
.end method


# virtual methods
.method public checkForCoreDump()V
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coredumpFeatureNative:Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coreDumpFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;->cr_coredump_get_info(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    return-void
.end method

.method public eraseCoreDump()V
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coredumpFeatureNative:Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coreDumpFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;->cr_coredump_erase(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    return-void
.end method

.method public initialize(Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;)V
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coreDumpFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    .line 29
    iput-object p1, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;

    .line 31
    iget-object p1, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coredumpFeatureNative:Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;

    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->sessionProvider:Ljavax/inject/Provider;

    .line 32
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    .line 31
    invoke-interface {p1, v0, p0}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;->coredump_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coreDumpFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    return-void

    .line 26
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "apiListener cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 23
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "CoreDumpFeature is already initialized!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onCoreDumpErased()V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;

    invoke-interface {v0}, Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;->onCoreDumpErased()V

    return-void
.end method

.method public onCoreDumpInfo(Z)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;->onCoreDumpExists(Z)V

    return-void
.end method

.method public onCoreDumpProgress(III)V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 82
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "Core dump progress: %d / %d"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;->onCoreDumpProgress(III)V

    return-void
.end method

.method public onCoreDumpReceived([B[B)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;->onCoreDumpReceived([B[B)V

    return-void
.end method

.method public onCoreDumpTriggered(Z)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;->onCoreDumpTriggered(Z)V

    return-void
.end method

.method public resetCoreDumpFeature()V
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coreDumpFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    if-eqz v0, :cond_0

    .line 54
    iget-object v1, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coredumpFeatureNative:Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;->cr_coredump_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    .line 55
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coredumpFeatureNative:Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coreDumpFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;->cr_coredump_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const/4 v0, 0x0

    .line 56
    iput-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coreDumpFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    .line 57
    iput-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;

    :cond_0
    return-void
.end method

.method public retrieveCoreDump()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coredumpFeatureNative:Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coreDumpFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;->cr_coredump_get_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    return-void
.end method

.method public triggerCoreDump()V
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coredumpFeatureNative:Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->coreDumpFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;->coredump_trigger_dump(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)V

    return-void
.end method
