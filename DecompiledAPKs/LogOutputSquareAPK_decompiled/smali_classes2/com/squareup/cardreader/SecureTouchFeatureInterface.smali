.class public interface abstract Lcom/squareup/cardreader/SecureTouchFeatureInterface;
.super Ljava/lang/Object;
.source "SecureTouchFeatureInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/SecureTouchFeatureInterface$NoOp;
    }
.end annotation


# virtual methods
.method public abstract initialize(Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;)V
.end method

.method public abstract onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
.end method

.method public abstract reset()V
.end method
