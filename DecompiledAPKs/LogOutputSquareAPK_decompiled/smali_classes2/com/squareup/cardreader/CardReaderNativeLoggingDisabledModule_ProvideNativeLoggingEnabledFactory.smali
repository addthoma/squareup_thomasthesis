.class public final Lcom/squareup/cardreader/CardReaderNativeLoggingDisabledModule_ProvideNativeLoggingEnabledFactory;
.super Ljava/lang/Object;
.source "CardReaderNativeLoggingDisabledModule_ProvideNativeLoggingEnabledFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderNativeLoggingDisabledModule_ProvideNativeLoggingEnabledFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/cardreader/CardReaderNativeLoggingDisabledModule_ProvideNativeLoggingEnabledFactory;
    .locals 1

    .line 21
    invoke-static {}, Lcom/squareup/cardreader/CardReaderNativeLoggingDisabledModule_ProvideNativeLoggingEnabledFactory$InstanceHolder;->access$000()Lcom/squareup/cardreader/CardReaderNativeLoggingDisabledModule_ProvideNativeLoggingEnabledFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideNativeLoggingEnabled()Z
    .locals 1

    .line 25
    invoke-static {}, Lcom/squareup/cardreader/CardReaderNativeLoggingDisabledModule;->provideNativeLoggingEnabled()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 1

    .line 17
    invoke-static {}, Lcom/squareup/cardreader/CardReaderNativeLoggingDisabledModule_ProvideNativeLoggingEnabledFactory;->provideNativeLoggingEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderNativeLoggingDisabledModule_ProvideNativeLoggingEnabledFactory;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
