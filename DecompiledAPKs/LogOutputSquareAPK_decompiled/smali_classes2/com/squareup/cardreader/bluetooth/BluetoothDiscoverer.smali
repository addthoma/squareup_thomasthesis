.class public Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;
.super Ljava/lang/Object;
.source "BluetoothDiscoverer.java"

# interfaces
.implements Lcom/squareup/cardreader/WirelessSearcher;


# static fields
.field private static final RESTART_DELAY:I = 0x1388


# instance fields
.field private final bluetoothAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private bluetoothDeviceReplaySubject:Lrx/subjects/ReplaySubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/ReplaySubject<",
            "Lcom/squareup/cardreader/WirelessConnection;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private runnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/thread/executor/MainThread;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Lcom/squareup/thread/executor/MainThread;",
            ")V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer$1;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer$1;-><init>(Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;)V

    iput-object v0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->runnable:Ljava/lang/Runnable;

    .line 37
    iput-object p1, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    .line 39
    iput-object p3, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;)Ljavax/inject/Provider;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public getBondedDevices()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/cardreader/WirelessConnection;",
            ">;"
        }
    .end annotation

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 75
    iget-object v1, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v1}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    return-object v0

    .line 78
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v1}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBluetooth()Z

    move-result v1

    if-nez v1, :cond_1

    return-object v0

    .line 82
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 83
    invoke-static {v2}, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;->deviceIsMiuraDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 84
    new-instance v3, Lcom/squareup/cardreader/ble/RealBleScanResult;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, Lcom/squareup/cardreader/ble/RealBleScanResult;-><init>(Landroid/bluetooth/BluetoothDevice;Z)V

    .line 85
    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method public isSearching()Z
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothDeviceReplaySubject:Lrx/subjects/ReplaySubject;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method onDeviceFound(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->isSearching()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothDeviceReplaySubject:Lrx/subjects/ReplaySubject;

    invoke-virtual {v0, p1}, Lrx/subjects/ReplaySubject;->onNext(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method onDiscoveryFinished()V
    .locals 4

    .line 98
    invoke-virtual {p0}, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->isSearching()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->runnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method onDiscoveryStarted()V
    .locals 0

    return-void
.end method

.method public startSearch()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/cardreader/WirelessConnection;",
            ">;"
        }
    .end annotation

    .line 57
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 58
    invoke-virtual {p0}, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->isSearching()Z

    move-result v0

    if-nez v0, :cond_2

    .line 61
    iget-object v0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBluetooth()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Started Bluetooth Scan"

    .line 67
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    .line 69
    invoke-static {}, Lrx/subjects/ReplaySubject;->create()Lrx/subjects/ReplaySubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothDeviceReplaySubject:Lrx/subjects/ReplaySubject;

    .line 70
    iget-object v0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothDeviceReplaySubject:Lrx/subjects/ReplaySubject;

    sget-object v1, Lcom/squareup/cardreader/bluetooth/-$$Lambda$RbwlOrpjzKss02xMapKDOvgtTHA;->INSTANCE:Lcom/squareup/cardreader/bluetooth/-$$Lambda$RbwlOrpjzKss02xMapKDOvgtTHA;

    invoke-virtual {v0, v1}, Lrx/subjects/ReplaySubject;->distinct(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0

    .line 65
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bluetooth not supported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bluetooth not enabled."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Scan already started."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public stopSearch()V
    .locals 2

    .line 43
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 44
    iget-object v0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->runnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 46
    iget-object v0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothDeviceReplaySubject:Lrx/subjects/ReplaySubject;

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v0}, Lrx/subjects/ReplaySubject;->onCompleted()V

    const/4 v0, 0x0

    .line 48
    iput-object v0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->bluetoothDeviceReplaySubject:Lrx/subjects/ReplaySubject;

    :cond_0
    return-void
.end method
