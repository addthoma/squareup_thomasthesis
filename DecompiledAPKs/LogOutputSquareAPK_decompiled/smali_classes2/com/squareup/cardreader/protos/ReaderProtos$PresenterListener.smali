.class public final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PresenterListener"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$ProtoAdapter_PresenterListener;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 3652
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$ProtoAdapter_PresenterListener;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$ProtoAdapter_PresenterListener;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 3657
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;-><init>(Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;)V
    .locals 1

    .line 3661
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p1}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 3674
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 3675
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;

    .line 3676
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 3681
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$Builder;
    .locals 2

    .line 3666
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$Builder;-><init>()V

    .line 3667
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 3651
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 3686
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PresenterListener{"

    .line 3687
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
