.class final Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$ProtoAdapter_SendMessageToReader;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SendMessageToReader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 8293
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8344
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;-><init>()V

    .line 8345
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 8346
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/16 v4, 0x5e

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 8367
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 8364
    :pswitch_0
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->on_core_dump_data_sent(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto :goto_0

    .line 8363
    :pswitch_1
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->on_tamper_data_sent(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto :goto_0

    .line 8362
    :pswitch_2
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reset(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto :goto_0

    .line 8361
    :pswitch_3
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->request_power_status(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto :goto_0

    .line 8360
    :pswitch_4
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reinitialize_secure_session(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto :goto_0

    .line 8359
    :pswitch_5
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->initialize_card_reader_features(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto :goto_0

    .line 8358
    :pswitch_6
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->select_application(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto :goto_0

    .line 8357
    :pswitch_7
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_timeout(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto :goto_0

    .line 8356
    :pswitch_8
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_server_error(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto :goto_0

    .line 8355
    :pswitch_9
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_network_error(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto/16 :goto_0

    .line 8354
    :pswitch_a
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_client_error(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto/16 :goto_0

    .line 8353
    :pswitch_b
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->enable_swipe_passthrough(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto/16 :goto_0

    .line 8352
    :pswitch_c
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_arpc(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto/16 :goto_0

    .line 8351
    :pswitch_d
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->cancel_payment(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto/16 :goto_0

    .line 8350
    :pswitch_e
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->start_payment(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto/16 :goto_0

    .line 8349
    :pswitch_f
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_secure_session_message_from_server(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto/16 :goto_0

    .line 8348
    :pswitch_10
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_firmware_update_response(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto/16 :goto_0

    .line 8365
    :cond_0
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->start_refund(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    goto/16 :goto_0

    .line 8371
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 8372
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8291
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$ProtoAdapter_SendMessageToReader;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8321
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8322
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8323
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8324
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8325
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8326
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8327
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8328
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8329
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8330
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8331
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8332
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8333
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8334
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8335
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    const/16 v2, 0x5e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8336
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8337
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8338
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8339
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8291
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$ProtoAdapter_SendMessageToReader;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;)I
    .locals 4

    .line 8298
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    const/16 v3, 0xf

    .line 8299
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    const/16 v3, 0xe

    .line 8300
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    const/16 v3, 0x10

    .line 8301
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    const/16 v3, 0x11

    .line 8302
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    const/4 v3, 0x1

    .line 8303
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    const/16 v3, 0xd

    .line 8304
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    const/4 v3, 0x2

    .line 8305
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    const/4 v3, 0x7

    .line 8306
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    const/16 v3, 0x8

    .line 8307
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    const/16 v3, 0x9

    .line 8308
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    const/16 v3, 0xa

    .line 8309
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    const/4 v3, 0x6

    .line 8310
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    const/4 v3, 0x3

    .line 8311
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    const/16 v3, 0x5e

    .line 8312
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    const/4 v3, 0x4

    .line 8313
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    const/16 v3, 0xb

    .line 8314
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    const/4 v3, 0x5

    .line 8315
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8316
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 8291
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$ProtoAdapter_SendMessageToReader;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;
    .locals 2

    .line 8377
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    move-result-object p1

    .line 8378
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    .line 8379
    :cond_0
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    .line 8380
    :cond_1
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    .line 8381
    :cond_2
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    .line 8382
    :cond_3
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    .line 8383
    :cond_4
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    .line 8384
    :cond_5
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    .line 8385
    :cond_6
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    .line 8386
    :cond_7
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8387
    :cond_8
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8388
    :cond_9
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8389
    :cond_a
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8390
    :cond_b
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    .line 8391
    :cond_c
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    .line 8392
    :cond_d
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    .line 8393
    :cond_e
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    .line 8394
    :cond_f
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    if-eqz v0, :cond_10

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    .line 8395
    :cond_10
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    if-eqz v0, :cond_11

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    .line 8396
    :cond_11
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 8397
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8291
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$ProtoAdapter_SendMessageToReader;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;

    move-result-object p1

    return-object p1
.end method
