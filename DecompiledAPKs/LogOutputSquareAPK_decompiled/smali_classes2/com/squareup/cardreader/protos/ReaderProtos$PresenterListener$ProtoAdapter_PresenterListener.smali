.class final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$ProtoAdapter_PresenterListener;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PresenterListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 5887
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5902
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$Builder;-><init>()V

    .line 5903
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 5904
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 5907
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 5911
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 5912
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5885
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$ProtoAdapter_PresenterListener;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5897
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5885
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$ProtoAdapter_PresenterListener;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;)I
    .locals 0

    .line 5892
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 5885
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$ProtoAdapter_PresenterListener;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;
    .locals 0

    .line 5917
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$Builder;

    move-result-object p1

    .line 5918
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 5919
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 5885
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$ProtoAdapter_PresenterListener;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;

    move-result-object p1

    return-object p1
.end method
