.class public final Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public asset:Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3248
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public asset(Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess$Builder;
    .locals 0

    .line 3252
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess$Builder;->asset:Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    return-object p0
.end method

.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;
    .locals 3

    .line 3258
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess$Builder;->asset:Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;-><init>(Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3245
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    move-result-object v0

    return-object v0
.end method
