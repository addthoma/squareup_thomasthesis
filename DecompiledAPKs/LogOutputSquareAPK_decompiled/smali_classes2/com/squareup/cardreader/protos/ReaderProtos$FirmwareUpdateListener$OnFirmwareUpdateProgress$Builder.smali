.class public final Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public asset:Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

.field public percent_complete:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3125
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public asset(Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress$Builder;
    .locals 0

    .line 3134
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress$Builder;->asset:Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    return-object p0
.end method

.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;
    .locals 4

    .line 3140
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress$Builder;->percent_complete:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress$Builder;->asset:Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;-><init>(Ljava/lang/Integer;Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3120
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    move-result-object v0

    return-object v0
.end method

.method public percent_complete(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress$Builder;
    .locals 0

    .line 3129
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress$Builder;->percent_complete:Ljava/lang/Integer;

    return-object p0
.end method
