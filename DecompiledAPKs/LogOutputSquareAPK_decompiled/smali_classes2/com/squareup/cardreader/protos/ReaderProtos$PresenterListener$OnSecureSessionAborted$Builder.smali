.class public final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public reason:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4839
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;
    .locals 3

    .line 4849
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$Builder;->reason:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;-><init>(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4836
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    move-result-object v0

    return-object v0
.end method

.method public reason(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$Builder;
    .locals 0

    .line 4843
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$Builder;->reason:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    return-object p0
.end method
