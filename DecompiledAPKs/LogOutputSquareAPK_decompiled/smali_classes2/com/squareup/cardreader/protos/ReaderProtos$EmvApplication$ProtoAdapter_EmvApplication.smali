.class final Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$ProtoAdapter_EmvApplication;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_EmvApplication"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1904
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1923
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;-><init>()V

    .line 1924
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1925
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 1930
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1928
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;->label(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;

    goto :goto_0

    .line 1927
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;->adf_name(Lokio/ByteString;)Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;

    goto :goto_0

    .line 1934
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1935
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1902
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$ProtoAdapter_EmvApplication;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1916
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->adf_name:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1917
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->label:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1918
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1902
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$ProtoAdapter_EmvApplication;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;)I
    .locals 4

    .line 1909
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->adf_name:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->label:Ljava/lang/String;

    const/4 v3, 0x2

    .line 1910
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1911
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1902
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$ProtoAdapter_EmvApplication;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;)Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;
    .locals 0

    .line 1940
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;

    move-result-object p1

    .line 1941
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1942
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1902
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$ProtoAdapter_EmvApplication;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;)Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    move-result-object p1

    return-object p1
.end method
