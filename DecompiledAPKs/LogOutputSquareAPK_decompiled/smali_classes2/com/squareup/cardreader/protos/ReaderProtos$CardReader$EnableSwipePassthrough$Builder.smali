.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public enable:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1114
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;
    .locals 3

    .line 1124
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;->enable:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1111
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    move-result-object v0

    return-object v0
.end method

.method public enable(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;
    .locals 0

    .line 1118
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;->enable:Ljava/lang/Boolean;

    return-object p0
.end method
