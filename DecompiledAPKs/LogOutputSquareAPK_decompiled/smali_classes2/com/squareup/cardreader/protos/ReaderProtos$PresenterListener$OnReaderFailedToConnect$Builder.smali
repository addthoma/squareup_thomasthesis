.class public final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3917
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;
    .locals 2

    .line 3922
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;-><init>(Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3916
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    move-result-object v0

    return-object v0
.end method
