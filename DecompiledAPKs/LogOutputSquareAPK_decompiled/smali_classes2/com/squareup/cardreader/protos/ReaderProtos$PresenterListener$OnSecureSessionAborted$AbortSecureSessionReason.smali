.class public final enum Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;
.super Ljava/lang/Enum;
.source "ReaderProtos.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AbortSecureSessionReason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason$ProtoAdapter_AbortSecureSessionReason;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CLIENT_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

.field public static final enum NETWORK_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

.field public static final enum SERVER_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 4854
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    const/4 v1, 0x0

    const-string v2, "CLIENT_ERROR"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->CLIENT_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    .line 4856
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    const/4 v2, 0x1

    const-string v3, "NETWORK_ERROR"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->NETWORK_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    .line 4858
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    const/4 v3, 0x2

    const-string v4, "SERVER_ERROR"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->SERVER_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    .line 4853
    sget-object v4, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->CLIENT_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->NETWORK_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->SERVER_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    .line 4860
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason$ProtoAdapter_AbortSecureSessionReason;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason$ProtoAdapter_AbortSecureSessionReason;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 4864
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 4865
    iput p3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 4875
    :cond_0
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->SERVER_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    return-object p0

    .line 4874
    :cond_1
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->NETWORK_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    return-object p0

    .line 4873
    :cond_2
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->CLIENT_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;
    .locals 1

    .line 4853
    const-class v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;
    .locals 1

    .line 4853
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 4882
    iget v0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted$AbortSecureSessionReason;->value:I

    return v0
.end method
