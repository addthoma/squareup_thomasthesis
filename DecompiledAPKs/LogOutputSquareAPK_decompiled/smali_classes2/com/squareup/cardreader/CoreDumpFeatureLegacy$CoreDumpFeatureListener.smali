.class public interface abstract Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;
.super Ljava/lang/Object;
.source "CoreDumpFeatureLegacy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CoreDumpFeatureLegacy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CoreDumpFeatureListener"
.end annotation


# virtual methods
.method public abstract onCoreDumpErased()V
.end method

.method public abstract onCoreDumpExists(Z)V
.end method

.method public abstract onCoreDumpProgress(III)V
.end method

.method public abstract onCoreDumpReceived([B[B)V
.end method

.method public abstract onCoreDumpTriggered(Z)V
.end method
