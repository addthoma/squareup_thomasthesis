.class public Lcom/squareup/cardreader/UnsetCardReaderListeners;
.super Ljava/lang/Object;
.source "UnsetCardReaderListeners.java"

# interfaces
.implements Lcom/squareup/squarewave/EventDataListener;
.implements Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;
.implements Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;
.implements Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;
.implements Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;
.implements Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;
.implements Lcom/squareup/cardreader/EmvListener;
.implements Lcom/squareup/cardreader/NfcListener;
.implements Lcom/squareup/cardreader/PaymentCompletionListener;
.implements Lcom/squareup/cardreader/FirmwareUpdater$Listener;
.implements Lcom/squareup/cardreader/LibraryLoadErrorListener;
.implements Lcom/squareup/cardreader/MagSwipeListener;
.implements Lcom/squareup/cardreader/CardReaderStatusListener;
.implements Lcom/squareup/cardreader/PinRequestListener;
.implements Lcom/squareup/cardreader/ReaderEventLogger;


# static fields
.field public static UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 48
    new-instance v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;

    invoke-direct {v0}, Lcom/squareup/cardreader/UnsetCardReaderListeners;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public initialize()V
    .locals 0

    return-void
.end method

.method public initializingSecureSession(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V
    .locals 0

    return-void
.end method

.method public logBatteryInfo(ILcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public logBleConnectionAction(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/cardreader/ble/BleAction;)V
    .locals 0

    return-void
.end method

.method public logBleConnectionEnqueued(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/BleConnectType;)V
    .locals 0

    return-void
.end method

.method public logBleConnectionStateChange(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/dipper/events/BleConnectionState;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public logBleDisconnectedEvent(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;ZLcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;)V
    .locals 0

    return-void
.end method

.method public logBleReaderForceUnpaired(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 0

    return-void
.end method

.method public logBluetoothStatusChanged(Ljava/lang/String;ZZ)V
    .locals 0

    return-void
.end method

.method public logCirqueTamperStatus(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/protos/client/bills/CardData$ReaderType;I)V
    .locals 0

    return-void
.end method

.method public logCommsRateUpdated(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public logCommsVersionAcquired(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 0

    return-void
.end method

.method public logCoreDumpResult(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 0

    return-void
.end method

.method public logEvent(ILcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V
    .locals 0

    return-void
.end method

.method public logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V
    .locals 0

    return-void
.end method

.method public logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;)V
    .locals 0

    return-void
.end method

.method public logEvent(Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;)V
    .locals 0

    return-void
.end method

.method public logFirmwareAssetVersionInfo(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V
    .locals 0

    return-void
.end method

.method public logPaymentFeatureEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V
    .locals 0

    return-void
.end method

.method public logReaderError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsReaderError;)V
    .locals 0

    return-void
.end method

.method public logSecureSessionResult(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
    .locals 0

    return-void
.end method

.method public logSecureSessionRevoked(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 0

    return-void
.end method

.method public logSerialNumberReceived(Lcom/squareup/cardreader/WirelessConnection;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public logSystemCapabilities(ZLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrsCapability;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public logTamperData(ILcom/squareup/cardreader/CardReaderInfo;[B)V
    .locals 0

    return-void
.end method

.method public logTamperResult(ILcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrTamperStatus;)V
    .locals 0

    return-void
.end method

.method public logTmsCountryCode(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/CountryCode;Lcom/squareup/CountryCode;)V
    .locals 0

    return-void
.end method

.method public onAccountSelectionRequired(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method public onAudioReaderFailedToConnect()V
    .locals 0

    return-void
.end method

.method public onAudioReaderFailedToConnect(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onAudioRequest(Lcom/squareup/cardreader/lcr/CrsTmnAudio;)V
    .locals 0

    return-void
.end method

.method public onAudioVisualRequest(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V
    .locals 1

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "onAudioVisualRequest request came, but no active listener is set!"

    .line 426
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onBatteryDead(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onBatteryUpdate(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onCapabilitiesReceived(ZLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrsCapability;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onCardError()V
    .locals 0

    return-void
.end method

.method public onCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onCardPresenceChange(Z)V
    .locals 0

    return-void
.end method

.method public onCardReaderBackendInitialized()V
    .locals 0

    return-void
.end method

.method public onCardReaderBackendInitialized(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onCardRemovedDuringPayment()V
    .locals 0

    return-void
.end method

.method public onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 485
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardInfo;->getName()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Got a cardholder name (\"%s\"), but no active listener is set!"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onChargeCycleCountReceived(I)V
    .locals 0

    return-void
.end method

.method public onCommsRateUpdated(Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;)V
    .locals 0

    return-void
.end method

.method public onCommsVersionAcquired(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 0

    return-void
.end method

.method public onCoreDump(Lcom/squareup/cardreader/CardReader;[B[B)V
    .locals 0

    return-void
.end method

.method public onCoreDumpErased()V
    .locals 0

    return-void
.end method

.method public onCoreDumpExists(Z)V
    .locals 0

    return-void
.end method

.method public onCoreDumpProgress(III)V
    .locals 0

    return-void
.end method

.method public onCoreDumpReceived([B[B)V
    .locals 0

    return-void
.end method

.method public onCoreDumpTriggered(Z)V
    .locals 0

    return-void
.end method

.method public onDeviceUnsupported(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onDisplayRequest(Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onFirmwareUpdateAssetSuccess(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;)V
    .locals 0

    return-void
.end method

.method public onFirmwareUpdateComplete(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onFirmwareUpdateError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V
    .locals 0

    return-void
.end method

.method public onFirmwareUpdateError(Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V
    .locals 0

    return-void
.end method

.method public onFirmwareUpdateProgress(I)V
    .locals 0

    return-void
.end method

.method public onFirmwareUpdateProgress(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;I)V
    .locals 0

    return-void
.end method

.method public onFirmwareUpdateStarted(Lcom/squareup/cardreader/CardReaderInfo;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onFirmwareUpdateSuccess()V
    .locals 0

    return-void
.end method

.method public onFirmwareVersionReceived(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onFullCommsEstablished(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onHardwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 0

    return-void
.end method

.method public onHardwarePinRequested(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 1

    .line 383
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Hardware Pin requested with unset listener"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onHardwareSerialNumberReceived(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onInitFirmwareUpdateRequired(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onInitRegisterUpdateRequired(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onInitializationComplete(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onLibrariesFailedToLoad()V
    .locals 0

    return-void
.end method

.method public onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 0

    return-void
.end method

.method public onMagFallbackSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 0

    return-void
.end method

.method public onMagFallbackSwipeSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    return-void
.end method

.method public onMagSwipeFailed(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 0

    return-void
.end method

.method public onMagSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 0

    return-void
.end method

.method public onMagSwipePassthrough(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    return-void
.end method

.method public onMagSwipePassthrough(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    return-void
.end method

.method public onMagSwipeSuccess(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    return-void
.end method

.method public onMagSwipeSuccess(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 1

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "Magswipe success, but no active listener is set!"

    .line 475
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onMagswipeFallbackSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 0

    return-void
.end method

.method public onManifestReceived([BZLcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 0

    return-void
.end method

.method public onNfcActionRequired()V
    .locals 0

    return-void
.end method

.method public onNfcActionRequired(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onNfcAuthorizationRequestReceived(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/CardInfo;)V
    .locals 0

    return-void
.end method

.method public onNfcCardBlocked()V
    .locals 0

    return-void
.end method

.method public onNfcCardBlocked(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onNfcCardDeclined()V
    .locals 0

    return-void
.end method

.method public onNfcCardDeclined(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onNfcCollision()V
    .locals 0

    return-void
.end method

.method public onNfcCollisionDetected(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onNfcInterfaceUnavailable()V
    .locals 0

    return-void
.end method

.method public onNfcInterfaceUnavailable(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onNfcLimitExceededInsertCard()V
    .locals 0

    return-void
.end method

.method public onNfcLimitExceededInsertCard(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onNfcLimitExceededTryAnotherCard()V
    .locals 0

    return-void
.end method

.method public onNfcLimitExceededTryAnotherCard(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onNfcPresentCardAgain()V
    .locals 0

    return-void
.end method

.method public onNfcPresentCardAgain(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onNfcProcessingError()V
    .locals 0

    return-void
.end method

.method public onNfcProcessingError(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onNfcSeePaymentDeviceForInstructions()V
    .locals 0

    return-void
.end method

.method public onNfcTimedOut(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    return-void
.end method

.method public onNfcTryAnotherCard()V
    .locals 0

    return-void
.end method

.method public onNfcTryAnotherCard(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onNfcUnlockDevice(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onNfcUnlockPaymentDevice()V
    .locals 0

    return-void
.end method

.method public onPaymentApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    return-void
.end method

.method public onPaymentContinuingDueToSwipe()V
    .locals 0

    return-void
.end method

.method public onPaymentDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    return-void
.end method

.method public onPaymentNfcTimedOut(Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    return-void
.end method

.method public onPaymentReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    return-void
.end method

.method public onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    return-void
.end method

.method public onPaymentTerminatedDueToSwipe(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    return-void
.end method

.method public onPowerStatus(Lcom/squareup/cardreader/CardReaderBatteryInfo;)V
    .locals 0

    return-void
.end method

.method public onReaderError(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onReaderError(Lcom/squareup/cardreader/lcr/CrsReaderError;)V
    .locals 0

    return-void
.end method

.method public onReaderReady()V
    .locals 0

    return-void
.end method

.method public onReaderReady(Lcom/squareup/cardreader/lcr/CrCardreaderType;)V
    .locals 0

    return-void
.end method

.method public onReceiveEventData(Lcom/squareup/squarewave/gum/EventData;)V
    .locals 0

    return-void
.end method

.method public onRequestTapCard()V
    .locals 0

    return-void
.end method

.method public onRequestTapCard(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onSecureSessionAborted(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V
    .locals 0

    return-void
.end method

.method public onSecureSessionDenied(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 0

    return-void
.end method

.method public onSecureSessionDenied(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 0

    return-void
.end method

.method public onSecureSessionError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
    .locals 0

    return-void
.end method

.method public onSecureSessionError(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
    .locals 0

    return-void
.end method

.method public onSecureSessionInvalid()V
    .locals 0

    return-void
.end method

.method public onSecureSessionInvalid(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onSecureSessionSendToServer([B)V
    .locals 0

    return-void
.end method

.method public onSecureSessionValid()V
    .locals 0

    return-void
.end method

.method public onSecureSessionValid(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onSecureTouchDisabled()V
    .locals 2

    .line 396
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onSecureTouchDisabled, but nobody is listening!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onSecureTouchEnabled()V
    .locals 2

    .line 392
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onSecureTouchEnabled, but nobody is listening!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onSecureTouchPinPadEvent(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V
    .locals 1

    .line 388
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Secure Touch event, but nobody is listening!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onSigRequested()V
    .locals 0

    return-void
.end method

.method public onSmartPaymentApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "Payment approved, but no active listener is set!"

    .line 446
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onSmartPaymentDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "Payment declined, but no active listener is set!"

    .line 458
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onSmartPaymentReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "Payment reversed, but no active listener is set!"

    .line 452
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onSmartPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "Payment canceled, but no active listener is set!"

    .line 463
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onSoftwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PinRequestData;)V
    .locals 0

    .line 641
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Pin request listener not set. (Townsend)"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onSoftwarePinRequested(Lcom/squareup/cardreader/PinRequestData;)V
    .locals 1

    .line 400
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Software Pin requested with unset listener"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
    .locals 0

    return-void
.end method

.method public onTamperData(Lcom/squareup/cardreader/CardReader;[B)V
    .locals 0

    return-void
.end method

.method public onTamperData([B)V
    .locals 0

    return-void
.end method

.method public onTamperStatus(Lcom/squareup/cardreader/lcr/CrTamperStatus;)V
    .locals 0

    return-void
.end method

.method public onTmnDataToTmn(Ljava/lang/String;[B)V
    .locals 0

    return-void
.end method

.method public onTmnTransactionComplete(Lcom/squareup/cardreader/lcr/TmnTransactionResult;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    return-void
.end method

.method public onTmnWriteNotify(II[B)V
    .locals 0

    return-void
.end method

.method public onTmsCountryCode(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onTmsCountryCode(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onUseChipCard()V
    .locals 0

    return-void
.end method

.method public onUseChipCard(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onUseChipCardDuringFallback()V
    .locals 0

    return-void
.end method

.method public onVersionInfo([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V
    .locals 0

    return-void
.end method

.method public sendAuthorization([BZLcom/squareup/cardreader/CardInfo;)V
    .locals 0

    return-void
.end method

.method public sendContactlessAuthorization([BLcom/squareup/cardreader/CardInfo;)V
    .locals 0

    return-void
.end method

.method public sendFirmwareManifestToServer(Lcom/squareup/cardreader/CardReader;[BLcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 0

    return-void
.end method

.method public sendSecureSessionMessageToServer(Lcom/squareup/cardreader/CardReader;[B)V
    .locals 0

    return-void
.end method

.method public sendTmnAuthorization([B)V
    .locals 0

    return-void
.end method
