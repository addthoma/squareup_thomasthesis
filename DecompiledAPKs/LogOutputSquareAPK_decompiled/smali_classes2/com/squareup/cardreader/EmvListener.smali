.class public interface abstract Lcom/squareup/cardreader/EmvListener;
.super Ljava/lang/Object;
.source "EmvListener.java"


# virtual methods
.method public abstract onAccountSelectionRequired(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onCardError()V
.end method

.method public abstract onCardRemovedDuringPayment()V
.end method

.method public abstract onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
.end method

.method public abstract onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
.end method

.method public abstract onMagFallbackSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
.end method

.method public abstract onMagFallbackSwipeSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
.end method

.method public abstract onSigRequested()V
.end method

.method public abstract onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
.end method

.method public abstract onUseChipCardDuringFallback()V
.end method

.method public abstract sendAuthorization([BZLcom/squareup/cardreader/CardInfo;)V
.end method
