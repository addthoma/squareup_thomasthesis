.class public Lcom/squareup/cardreader/lcr/BleBackendNativeJNI;
.super Ljava/lang/Object;
.source "BleBackendNativeJNI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native ble_received_data_from_characteristic_ack_vector(JI)J
.end method

.method public static final native ble_received_data_from_characteristic_data(J[B)J
.end method

.method public static final native ble_received_data_from_characteristic_mtu(JI)J
.end method

.method public static final native cr_comms_backend_ble_alloc()J
.end method

.method public static final native cr_comms_backend_ble_free(J)J
.end method

.method public static final native cr_comms_backend_ble_shutdown(J)V
.end method

.method public static final native initialize_backend_ble(J[BLjava/lang/Object;)J
.end method
