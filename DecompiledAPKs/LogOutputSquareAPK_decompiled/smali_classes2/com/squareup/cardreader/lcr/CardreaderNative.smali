.class public Lcom/squareup/cardreader/lcr/CardreaderNative;
.super Ljava/lang/Object;
.source "CardreaderNative.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cardreader_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;
    .locals 2

    .line 29
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;)J

    move-result-wide v0

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;)J

    move-result-wide p0

    invoke-static {v0, v1, p0, p1, p2}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->cardreader_initialize(JJLjava/lang/Object;)J

    move-result-wide p0

    const-wide/16 v0, 0x0

    cmp-long p2, p0, v0

    if-nez p2, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 30
    :cond_0
    new-instance p2, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    const/4 v0, 0x0

    invoke-direct {p2, p0, p1, v0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;-><init>(JZ)V

    move-object p0, p2

    :goto_0
    return-object p0
.end method

.method public static cardreader_initialize_rpc(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;
    .locals 3

    .line 34
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->cardreader_initialize_rpc(JLjava/lang/Object;)J

    move-result-wide p0

    const-wide/16 v0, 0x0

    cmp-long v2, p0, v0

    if-nez v2, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 35
    :cond_0
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;-><init>(JZ)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method

.method public static cr_cardreader_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
    .locals 2

    .line 17
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->cr_cardreader_free(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrCardreaderResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_cardreader_notify_reader_plugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
    .locals 2

    .line 21
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->cr_cardreader_notify_reader_plugged(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrCardreaderResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_cardreader_notify_reader_unplugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
    .locals 2

    .line 25
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->cr_cardreader_notify_reader_unplugged(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrCardreaderResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_cardreader_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
    .locals 2

    .line 13
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->cr_cardreader_term(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrCardreaderResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    move-result-object p0

    return-object p0
.end method

.method public static process_rpc_callback()V
    .locals 0

    .line 39
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->process_rpc_callback()V

    return-void
.end method
