.class public Lcom/squareup/cardreader/lcr/PowerFeatureNativePassthrough;
.super Ljava/lang/Object;
.source "PowerFeatureNativePassthrough.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cr_power_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;
    .locals 0

    .line 14
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/PowerFeatureNative;->cr_power_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_power_get_battery_voltage(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;
    .locals 0

    .line 19
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/PowerFeatureNative;->cr_power_get_battery_voltage(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_power_off(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;
    .locals 0

    .line 24
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/PowerFeatureNative;->cr_power_off(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_power_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;
    .locals 0

    .line 9
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/PowerFeatureNative;->cr_power_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;

    move-result-object p1

    return-object p1
.end method

.method public power_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;
    .locals 0

    .line 30
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/PowerFeatureNative;->power_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    move-result-object p1

    return-object p1
.end method
