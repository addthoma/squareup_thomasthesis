.class public Lcom/squareup/cardreader/lcr/CoredumpFeatureNativePassthrough;
.super Ljava/lang/Object;
.source "CoredumpFeatureNativePassthrough.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public coredump_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;
    .locals 0

    .line 35
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;->coredump_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    move-result-object p1

    return-object p1
.end method

.method public coredump_trigger_dump(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)V
    .locals 0

    .line 40
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;->coredump_trigger_dump(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)V

    return-void
.end method

.method public cr_coredump_erase(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
    .locals 0

    .line 29
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;->cr_coredump_erase(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_coredump_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
    .locals 0

    .line 14
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;->cr_coredump_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_coredump_get_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
    .locals 0

    .line 24
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;->cr_coredump_get_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_coredump_get_info(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
    .locals 0

    .line 19
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;->cr_coredump_get_info(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_coredump_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
    .locals 0

    .line 9
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;->cr_coredump_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    move-result-object p1

    return-object p1
.end method
