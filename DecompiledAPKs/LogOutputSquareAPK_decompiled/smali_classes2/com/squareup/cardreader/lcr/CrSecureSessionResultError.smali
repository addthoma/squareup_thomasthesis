.class public Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;
.super Ljava/lang/Object;
.source "CrSecureSessionResultError.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 75
    invoke-static {}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->new_CrSecureSessionResultError()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;-><init>(JZ)V

    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCPtr:J

    return-void
.end method

.method protected static getCPtr(Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;)J
    .locals 2

    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    .line 21
    :cond_0
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCPtr:J

    :goto_0
    return-wide v0
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 5

    monitor-enter p0

    .line 25
    :try_start_0
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCPtr:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    .line 26
    iget-boolean v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCMemOwn:Z

    if-nez v0, :cond_0

    .line 30
    iput-wide v2, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCPtr:J

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 27
    iput-boolean v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCMemOwn:Z

    .line 28
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "C++ destructor does not have public access"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getError()Lcom/squareup/cardreader/lcr/CrSecureSessionServerError;
    .locals 2

    .line 47
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->CrSecureSessionResultError_error_get(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;)I

    move-result v0

    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CrSecureSessionServerError;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionServerError;

    move-result-object v0

    return-object v0
.end method

.method public getLocalizedDescription()Ljava/lang/String;
    .locals 2

    .line 71
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->CrSecureSessionResultError_localizedDescription_get(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocalizedTitle()Ljava/lang/String;
    .locals 2

    .line 63
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->CrSecureSessionResultError_localizedTitle_get(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResult()Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 2

    .line 39
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->CrSecureSessionResultError_result_get(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;)I

    move-result v0

    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object v0

    return-object v0
.end method

.method public getUxHint()Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;
    .locals 2

    .line 55
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->CrSecureSessionResultError_uxHint_get(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;)I

    move-result v0

    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    move-result-object v0

    return-object v0
.end method

.method public setError(Lcom/squareup/cardreader/lcr/CrSecureSessionServerError;)V
    .locals 2

    .line 43
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCPtr:J

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionServerError;->swigValue()I

    move-result p1

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->CrSecureSessionResultError_error_set(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;I)V

    return-void
.end method

.method public setLocalizedDescription(Ljava/lang/String;)V
    .locals 2

    .line 67
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->CrSecureSessionResultError_localizedDescription_set(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;Ljava/lang/String;)V

    return-void
.end method

.method public setLocalizedTitle(Ljava/lang/String;)V
    .locals 2

    .line 59
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->CrSecureSessionResultError_localizedTitle_set(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;Ljava/lang/String;)V

    return-void
.end method

.method public setResult(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
    .locals 2

    .line 35
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCPtr:J

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigValue()I

    move-result p1

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->CrSecureSessionResultError_result_set(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;I)V

    return-void
.end method

.method public setUxHint(Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 2

    .line 51
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->swigCPtr:J

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;->swigValue()I

    move-result p1

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;->CrSecureSessionResultError_uxHint_set(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;I)V

    return-void
.end method
