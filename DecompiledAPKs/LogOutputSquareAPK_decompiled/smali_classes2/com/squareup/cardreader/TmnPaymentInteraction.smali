.class public final Lcom/squareup/cardreader/TmnPaymentInteraction;
.super Lcom/squareup/cardreader/PaymentInteraction;
.source "PaymentInteraction.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u001f2\u00020\u0001:\u0001\u001fB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J1\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u0007H\u00d6\u0001R\u0014\u0010\u0008\u001a\u00020\tX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/cardreader/TmnPaymentInteraction;",
        "Lcom/squareup/cardreader/PaymentInteraction;",
        "requestType",
        "Lcom/squareup/cardreader/lcr/CrsTmnRequestType;",
        "brandId",
        "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        "transactionId",
        "",
        "amountAuthorized",
        "",
        "(Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V",
        "getAmountAuthorized",
        "()J",
        "getBrandId",
        "()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        "getRequestType",
        "()Lcom/squareup/cardreader/lcr/CrsTmnRequestType;",
        "getTransactionId",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "Companion",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;


# instance fields
.field private final amountAuthorized:J

.field private final brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field private final requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

.field private final transactionId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/cardreader/TmnPaymentInteraction;->Companion:Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 1

    const-string v0, "requestType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "brandId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionId"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 47
    invoke-direct {p0, p4, p5, v0}, Lcom/squareup/cardreader/PaymentInteraction;-><init>(JLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    iput-object p2, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    iput-object p3, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->transactionId:Ljava/lang/String;

    iput-wide p4, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->amountAuthorized:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/TmnPaymentInteraction;Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;JILjava/lang/Object;)Lcom/squareup/cardreader/TmnPaymentInteraction;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->transactionId:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/squareup/cardreader/TmnPaymentInteraction;->getAmountAuthorized()J

    move-result-wide p4

    :cond_3
    move-wide v1, p4

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-wide p6, v1

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/cardreader/TmnPaymentInteraction;->copy(Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)Lcom/squareup/cardreader/TmnPaymentInteraction;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/cardreader/lcr/CrsTmnRequestType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    return-object v0
.end method

.method public final component2()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()J
    .locals 2

    invoke-virtual {p0}, Lcom/squareup/cardreader/TmnPaymentInteraction;->getAmountAuthorized()J

    move-result-wide v0

    return-wide v0
.end method

.method public final copy(Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)Lcom/squareup/cardreader/TmnPaymentInteraction;
    .locals 7

    const-string v0, "requestType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "brandId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionId"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/TmnPaymentInteraction;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardreader/TmnPaymentInteraction;-><init>(Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/TmnPaymentInteraction;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/TmnPaymentInteraction;

    iget-object v0, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    iget-object v1, p1, Lcom/squareup/cardreader/TmnPaymentInteraction;->requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    iget-object v1, p1, Lcom/squareup/cardreader/TmnPaymentInteraction;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->transactionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/cardreader/TmnPaymentInteraction;->transactionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/cardreader/TmnPaymentInteraction;->getAmountAuthorized()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/TmnPaymentInteraction;->getAmountAuthorized()J

    move-result-wide v2

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getAmountAuthorized()J
    .locals 2

    .line 46
    iget-wide v0, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->amountAuthorized:J

    return-wide v0
.end method

.method public final getBrandId()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    return-object v0
.end method

.method public final getRequestType()Lcom/squareup/cardreader/lcr/CrsTmnRequestType;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    return-object v0
.end method

.method public final getTransactionId()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->transactionId:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/cardreader/TmnPaymentInteraction;->getAmountAuthorized()J

    move-result-wide v1

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TmnPaymentInteraction(requestType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", brandId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transactionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/TmnPaymentInteraction;->transactionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", amountAuthorized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/cardreader/TmnPaymentInteraction;->getAmountAuthorized()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
