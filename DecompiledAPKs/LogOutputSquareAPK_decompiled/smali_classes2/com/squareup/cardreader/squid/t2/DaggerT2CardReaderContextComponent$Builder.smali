.class public final Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$Builder;
.super Ljava/lang/Object;
.source "DaggerT2CardReaderContextComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private cardReaderContextParent:Lcom/squareup/cardreader/CardReaderContextParent;

.field private cardReaderModule:Lcom/squareup/cardreader/CardReaderModule;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$1;)V
    .locals 0

    .line 357
    invoke-direct {p0}, Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/squid/t2/T2CardReaderContextComponent;
    .locals 4

    .line 394
    iget-object v0, p0, Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$Builder;->cardReaderModule:Lcom/squareup/cardreader/CardReaderModule;

    const-class v1, Lcom/squareup/cardreader/CardReaderModule;

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkBuilderRequirement(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 395
    iget-object v0, p0, Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$Builder;->cardReaderContextParent:Lcom/squareup/cardreader/CardReaderContextParent;

    const-class v1, Lcom/squareup/cardreader/CardReaderContextParent;

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkBuilderRequirement(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 396
    new-instance v0, Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent;

    iget-object v1, p0, Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$Builder;->cardReaderModule:Lcom/squareup/cardreader/CardReaderModule;

    iget-object v2, p0, Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$Builder;->cardReaderContextParent:Lcom/squareup/cardreader/CardReaderContextParent;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent;-><init>(Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/CardReaderContextParent;Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$1;)V

    return-object v0
.end method

.method public cardReaderContextParent(Lcom/squareup/cardreader/CardReaderContextParent;)Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$Builder;
    .locals 0

    .line 389
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderContextParent;

    iput-object p1, p0, Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$Builder;->cardReaderContextParent:Lcom/squareup/cardreader/CardReaderContextParent;

    return-object p0
.end method

.method public cardReaderModule(Lcom/squareup/cardreader/CardReaderModule;)Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$Builder;
    .locals 0

    .line 375
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderModule;

    iput-object p1, p0, Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$Builder;->cardReaderModule:Lcom/squareup/cardreader/CardReaderModule;

    return-object p0
.end method

.method public clockModule(Lcom/squareup/android/util/ClockModule;)Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 384
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public noopMinesweeperModule(Lcom/squareup/ms/NoopMinesweeperModule;)Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 370
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method
