.class public final Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscoveryBroadcastReceiverFactory;
.super Ljava/lang/Object;
.source "GlobalBleModule_ProvideBluetoothDiscoveryBroadcastReceiverFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;",
        ">;"
    }
.end annotation


# instance fields
.field private final bluetoothDiscovererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/ble/GlobalBleModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscoveryBroadcastReceiverFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule;

    .line 26
    iput-object p2, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscoveryBroadcastReceiverFactory;->bluetoothDiscovererProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscoveryBroadcastReceiverFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;",
            ">;)",
            "Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscoveryBroadcastReceiverFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscoveryBroadcastReceiverFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscoveryBroadcastReceiverFactory;-><init>(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideBluetoothDiscoveryBroadcastReceiver(Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;)Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;
    .locals 0

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/ble/GlobalBleModule;->provideBluetoothDiscoveryBroadcastReceiver(Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;)Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscoveryBroadcastReceiverFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscoveryBroadcastReceiverFactory;->bluetoothDiscovererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscoveryBroadcastReceiverFactory;->provideBluetoothDiscoveryBroadcastReceiver(Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;)Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscoveryBroadcastReceiverFactory;->get()Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;

    move-result-object v0

    return-object v0
.end method
