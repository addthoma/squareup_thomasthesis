.class Lcom/squareup/cardreader/ble/BleConnectionStateMachine$CommsRunner;
.super Ljava/lang/Object;
.source "BleConnectionStateMachine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleConnectionStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CommsRunner"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)V
    .locals 0

    .line 721
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$CommsRunner;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/ble/BleConnectionStateMachine$1;)V
    .locals 0

    .line 721
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$CommsRunner;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 723
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$CommsRunner;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->access$800(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)Lcom/squareup/cardreader/ble/BleSender;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    sget-object v2, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_CONN_INTERVAL:Ljava/util/UUID;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/cardreader/ble/BleSender;->readCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)V

    .line 724
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$CommsRunner;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->access$800(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)Lcom/squareup/cardreader/ble/BleSender;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    sget-object v2, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_COMMS_VERSION:Ljava/util/UUID;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/cardreader/ble/BleSender;->readCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)V

    return-void
.end method
