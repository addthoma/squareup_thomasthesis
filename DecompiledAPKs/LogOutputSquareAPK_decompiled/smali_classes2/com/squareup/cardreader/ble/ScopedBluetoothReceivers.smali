.class public Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;
.super Ljava/lang/Object;
.source "ScopedBluetoothReceivers.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final bleBondingBroadcastReceiver:Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

.field private final bluetoothDiscoveryBroadcastReceiver:Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;

.field private final bluetoothStatusReceiver:Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->context:Landroid/content/Context;

    .line 22
    iput-object p2, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->bleBondingBroadcastReceiver:Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    .line 23
    iput-object p3, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->bluetoothDiscoveryBroadcastReceiver:Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;

    .line 24
    iput-object p4, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->bluetoothStatusReceiver:Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 28
    iget-object p1, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->bleBondingBroadcastReceiver:Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->context:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;->initialize(Landroid/content/Context;)V

    .line 29
    iget-object p1, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->bluetoothDiscoveryBroadcastReceiver:Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->context:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;->initialize(Landroid/content/Context;)V

    .line 30
    iget-object p1, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->bluetoothStatusReceiver:Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->context:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->initialize(Landroid/content/Context;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->bleBondingBroadcastReceiver:Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;->destroy(Landroid/content/Context;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->bluetoothDiscoveryBroadcastReceiver:Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;->destroy(Landroid/content/Context;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->bluetoothStatusReceiver:Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->destroy(Landroid/content/Context;)V

    return-void
.end method
