.class final Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "ConnectionNegotiator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/ble/RealConnectionNegotiator;->negotiateConnection(Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lcom/squareup/blecoroutines/Connection;Landroid/content/Context;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Ljava/lang/String;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001*\u00020\u0003H\u008a@\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "kotlin.jvm.PlatformType",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.cardreader.ble.RealConnectionNegotiator$negotiateConnection$serialNumber$1"
    f = "ConnectionNegotiator.kt"
    i = {
        0x0
    }
    l = {
        0x59
    }
    m = "invokeSuspend"
    n = {
        "$this$withTimeout"
    }
    s = {
        "L$0"
    }
.end annotation


# instance fields
.field final synthetic $connection:Lcom/squareup/blecoroutines/Connection;

.field L$0:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method constructor <init>(Lcom/squareup/blecoroutines/Connection;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;->$connection:Lcom/squareup/blecoroutines/Connection;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;->$connection:Lcom/squareup/blecoroutines/Connection;

    invoke-direct {v0, v1, p2}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;-><init>(Lcom/squareup/blecoroutines/Connection;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 87
    iget v1, p0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;->label:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_0

    .line 90
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 87
    :cond_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    .line 88
    iget-object v1, p0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;->$connection:Lcom/squareup/blecoroutines/Connection;

    .line 89
    sget-object v3, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_DEVICE_INFORMATION_SERVICE:Ljava/util/UUID;

    const-string v4, "UUID_DEVICE_INFORMATION_SERVICE"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_SERIAL_NUMBER:Ljava/util/UUID;

    const-string v5, "UUID_CHARACTERISTIC_SERIAL_NUMBER"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;->L$0:Ljava/lang/Object;

    iput v2, p0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;->label:I

    invoke-interface {v1, v3, v4, p0}, Lcom/squareup/blecoroutines/Connection;->readCharacteristic(Ljava/util/UUID;Ljava/util/UUID;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    .line 87
    :cond_2
    :goto_0
    check-cast p1, Landroid/bluetooth/BluetoothGattCharacteristic;

    const/4 v0, 0x0

    .line 90
    invoke-virtual {p1, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getStringValue(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
