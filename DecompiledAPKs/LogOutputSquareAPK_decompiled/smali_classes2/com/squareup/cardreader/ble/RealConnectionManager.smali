.class public final Lcom/squareup/cardreader/ble/RealConnectionManager;
.super Ljava/lang/Object;
.source "ConnectionManager.kt"

# interfaces
.implements Lcom/squareup/cardreader/ble/ConnectionManager;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nConnectionManager.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ConnectionManager.kt\ncom/squareup/cardreader/ble/RealConnectionManager\n+ 2 WhileSelect.kt\nkotlinx/coroutines/selects/WhileSelectKt\n+ 3 Select.kt\nkotlinx/coroutines/selects/SelectKt\n*L\n1#1,106:1\n31#2:107\n32#2:117\n191#3,9:108\n*E\n*S KotlinDebug\n*F\n+ 1 ConnectionManager.kt\ncom/squareup/cardreader/ble/RealConnectionManager\n*L\n49#1:107\n49#1:117\n49#1,9:108\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J?\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u000e\u001a\u00020\u000fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/RealConnectionManager;",
        "Lcom/squareup/cardreader/ble/ConnectionManager;",
        "()V",
        "manageConnection",
        "",
        "connection",
        "Lcom/squareup/blecoroutines/Connection;",
        "negotiatedConnection",
        "Lcom/squareup/cardreader/ble/NegotiatedConnection;",
        "events",
        "Lcom/squareup/cardreader/ble/RealConnectionEvents;",
        "sendMessageChannel",
        "Lkotlinx/coroutines/channels/Channel;",
        "Lcom/squareup/cardreader/ble/SendMessage;",
        "executionEnv",
        "Lcom/squareup/cardreader/ble/Timeouts;",
        "(Lcom/squareup/blecoroutines/Connection;Lcom/squareup/cardreader/ble/NegotiatedConnection;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lkotlinx/coroutines/channels/Channel;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public manageConnection(Lcom/squareup/blecoroutines/Connection;Lcom/squareup/cardreader/ble/NegotiatedConnection;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lkotlinx/coroutines/channels/Channel;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blecoroutines/Connection;",
            "Lcom/squareup/cardreader/ble/NegotiatedConnection;",
            "Lcom/squareup/cardreader/ble/RealConnectionEvents;",
            "Lkotlinx/coroutines/channels/Channel<",
            "Lcom/squareup/cardreader/ble/SendMessage;",
            ">;",
            "Lcom/squareup/cardreader/ble/Timeouts;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p6

    instance-of v2, v1, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;

    if-eqz v2, :cond_0

    move-object v2, v1

    check-cast v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;

    iget v3, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->label:I

    const/high16 v4, -0x80000000

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    iget v1, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->label:I

    sub-int/2addr v1, v4

    iput v1, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->label:I

    move-object/from16 v3, p0

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;

    move-object/from16 v3, p0

    invoke-direct {v2, v3, v1}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;-><init>(Lcom/squareup/cardreader/ble/RealConnectionManager;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object v1, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v4

    .line 38
    iget v5, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->label:I

    const/4 v6, 0x1

    if-eqz v5, :cond_2

    if-ne v5, v6, :cond_1

    iget-object v0, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$7:Ljava/lang/Object;

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object v5, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$6:Ljava/lang/Object;

    check-cast v5, Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object v7, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$5:Ljava/lang/Object;

    check-cast v7, Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v8, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$4:Ljava/lang/Object;

    check-cast v8, Lkotlinx/coroutines/channels/Channel;

    iget-object v9, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$3:Ljava/lang/Object;

    check-cast v9, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v10, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$2:Ljava/lang/Object;

    check-cast v10, Lcom/squareup/cardreader/ble/NegotiatedConnection;

    iget-object v11, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$1:Ljava/lang/Object;

    check-cast v11, Lcom/squareup/blecoroutines/Connection;

    iget-object v12, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$0:Ljava/lang/Object;

    check-cast v12, Lcom/squareup/cardreader/ble/RealConnectionManager;

    invoke-static {v1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object v13, v7

    move-object v14, v8

    move-object v15, v9

    move-object/from16 v24, v11

    move-object v11, v0

    move-object v0, v1

    move-object v1, v12

    move-object v12, v5

    move-object v5, v10

    move-object v10, v4

    move-object/from16 v4, v24

    goto/16 :goto_4

    .line 96
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_2
    invoke-static {v1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 46
    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    const-string v5, "UUID_LCR_SERVICE"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v7, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_WRITE_DATA:Ljava/util/UUID;

    const-string v8, "UUID_CHARACTERISTIC_WRITE_DATA"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1, v7}, Lcom/squareup/blecoroutines/Connection;->findCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v1

    .line 48
    sget-object v7, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    invoke-static {v7, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v5, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_CONNECTION_CONTROL:Ljava/util/UUID;

    const-string v8, "UUID_CHARACTERISTIC_CONNECTION_CONTROL"

    invoke-static {v5, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v7, v5}, Lcom/squareup/blecoroutines/Connection;->findCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v5

    move-object/from16 v15, p3

    move-object/from16 v14, p4

    move-object/from16 v13, p5

    move-object v12, v1

    move-object v1, v3

    move-object v10, v4

    move-object v11, v5

    move-object/from16 v5, p2

    move-object v4, v0

    .line 108
    :goto_1
    iput-object v1, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$0:Ljava/lang/Object;

    iput-object v4, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$1:Ljava/lang/Object;

    iput-object v5, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$2:Ljava/lang/Object;

    iput-object v15, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$3:Ljava/lang/Object;

    iput-object v14, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$4:Ljava/lang/Object;

    iput-object v13, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$5:Ljava/lang/Object;

    iput-object v12, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$6:Ljava/lang/Object;

    iput-object v11, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->L$7:Ljava/lang/Object;

    iput v6, v2, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$1;->label:I

    .line 109
    new-instance v9, Lkotlinx/coroutines/selects/SelectBuilderImpl;

    invoke-direct {v9, v2}, Lkotlinx/coroutines/selects/SelectBuilderImpl;-><init>(Lkotlin/coroutines/Continuation;)V

    .line 111
    :try_start_0
    move-object v0, v9

    check-cast v0, Lkotlinx/coroutines/selects/SelectBuilder;

    .line 50
    invoke-interface {v4}, Lcom/squareup/blecoroutines/Connection;->getOnDisconnection()Lkotlinx/coroutines/Deferred;

    move-result-object v7

    invoke-interface {v7}, Lkotlinx/coroutines/Deferred;->getOnAwait()Lkotlinx/coroutines/selects/SelectClause1;

    move-result-object v7

    new-instance v8, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$2$1;

    const/4 v6, 0x0

    invoke-direct {v8, v6}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$2$1;-><init>(Lkotlin/coroutines/Continuation;)V

    check-cast v8, Lkotlin/jvm/functions/Function2;

    invoke-interface {v0, v7, v8}, Lkotlinx/coroutines/selects/SelectBuilder;->invoke(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V

    .line 53
    invoke-virtual {v5}, Lcom/squareup/cardreader/ble/NegotiatedConnection;->getMtuChannel()Lkotlinx/coroutines/channels/ReceiveChannel;

    move-result-object v6

    invoke-interface {v6}, Lkotlinx/coroutines/channels/ReceiveChannel;->getOnReceive()Lkotlinx/coroutines/selects/SelectClause1;

    move-result-object v6

    new-instance v16, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v8, 0x0

    move-object/from16 v7, v16

    move-object/from16 v17, v9

    move-object v9, v4

    move-object/from16 v18, v10

    move-object v10, v5

    move-object/from16 v19, v11

    move-object v11, v15

    move-object/from16 v20, v12

    move-object v12, v14

    move-object/from16 v21, v13

    move-object/from16 v22, v14

    move-object/from16 v14, v20

    move-object/from16 v23, v15

    move-object/from16 v15, v19

    :try_start_1
    invoke-direct/range {v7 .. v15}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;-><init>(Lkotlin/coroutines/Continuation;Lcom/squareup/blecoroutines/Connection;Lcom/squareup/cardreader/ble/NegotiatedConnection;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lkotlinx/coroutines/channels/Channel;Lcom/squareup/cardreader/ble/Timeouts;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    move-object/from16 v7, v16

    check-cast v7, Lkotlin/jvm/functions/Function2;

    invoke-interface {v0, v6, v7}, Lkotlinx/coroutines/selects/SelectBuilder;->invoke(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V

    .line 57
    invoke-virtual {v5}, Lcom/squareup/cardreader/ble/NegotiatedConnection;->getDataChannel()Lkotlinx/coroutines/channels/ReceiveChannel;

    move-result-object v6

    invoke-interface {v6}, Lkotlinx/coroutines/channels/ReceiveChannel;->getOnReceive()Lkotlinx/coroutines/selects/SelectClause1;

    move-result-object v6

    new-instance v16, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$2;

    const/4 v8, 0x0

    move-object/from16 v7, v16

    move-object v9, v4

    move-object v10, v5

    move-object/from16 v11, v23

    move-object/from16 v12, v22

    move-object/from16 v13, v21

    move-object/from16 v14, v20

    move-object/from16 v15, v19

    invoke-direct/range {v7 .. v15}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$2;-><init>(Lkotlin/coroutines/Continuation;Lcom/squareup/blecoroutines/Connection;Lcom/squareup/cardreader/ble/NegotiatedConnection;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lkotlinx/coroutines/channels/Channel;Lcom/squareup/cardreader/ble/Timeouts;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    move-object/from16 v7, v16

    check-cast v7, Lkotlin/jvm/functions/Function2;

    invoke-interface {v0, v6, v7}, Lkotlinx/coroutines/selects/SelectBuilder;->invoke(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V

    .line 61
    invoke-interface/range {v22 .. v22}, Lkotlinx/coroutines/channels/Channel;->getOnReceive()Lkotlinx/coroutines/selects/SelectClause1;

    move-result-object v6

    new-instance v16, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;

    const/4 v8, 0x0

    move-object/from16 v7, v16

    move-object v9, v4

    move-object v10, v5

    move-object/from16 v11, v23

    move-object/from16 v12, v22

    move-object/from16 v13, v21

    move-object/from16 v14, v20

    move-object/from16 v15, v19

    invoke-direct/range {v7 .. v15}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$3;-><init>(Lkotlin/coroutines/Continuation;Lcom/squareup/blecoroutines/Connection;Lcom/squareup/cardreader/ble/NegotiatedConnection;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lkotlinx/coroutines/channels/Channel;Lcom/squareup/cardreader/ble/Timeouts;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    move-object/from16 v7, v16

    check-cast v7, Lkotlin/jvm/functions/Function2;

    invoke-interface {v0, v6, v7}, Lkotlinx/coroutines/selects/SelectBuilder;->invoke(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v6, v17

    goto :goto_3

    :catchall_0
    move-exception v0

    move-object/from16 v6, v17

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object/from16 v18, v10

    move-object/from16 v19, v11

    move-object/from16 v20, v12

    move-object/from16 v21, v13

    move-object/from16 v22, v14

    move-object/from16 v23, v15

    move-object v6, v9

    .line 113
    :goto_2
    invoke-virtual {v6, v0}, Lkotlinx/coroutines/selects/SelectBuilderImpl;->handleBuilderException(Ljava/lang/Throwable;)V

    .line 115
    :goto_3
    invoke-virtual {v6}, Lkotlinx/coroutines/selects/SelectBuilderImpl;->getResult()Ljava/lang/Object;

    move-result-object v0

    .line 108
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v6

    if-ne v0, v6, :cond_3

    invoke-static {v2}, Lkotlin/coroutines/jvm/internal/DebugProbesKt;->probeCoroutineSuspended(Lkotlin/coroutines/Continuation;)V

    :cond_3
    move-object/from16 v6, v18

    if-ne v0, v6, :cond_4

    return-object v6

    :cond_4
    move-object v10, v6

    move-object/from16 v11, v19

    move-object/from16 v12, v20

    move-object/from16 v13, v21

    move-object/from16 v14, v22

    move-object/from16 v15, v23

    .line 116
    :goto_4
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v6, 0x1

    goto/16 :goto_1

    .line 96
    :cond_5
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method
