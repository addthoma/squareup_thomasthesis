.class public final Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;
.super Ljava/lang/Object;
.source "GlobalBleModule_ProvideBluetoothStatusReceiverFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;",
        ">;"
    }
.end annotation


# instance fields
.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/ble/GlobalBleModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule;

    .line 33
    iput-object p2, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;)",
            "Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;-><init>(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideBluetoothStatusReceiver(Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;
    .locals 0

    .line 53
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/cardreader/ble/GlobalBleModule;->provideBluetoothStatusReceiver(Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/BluetoothUtils;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;->provideBluetoothStatusReceiver(Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothStatusReceiverFactory;->get()Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    move-result-object v0

    return-object v0
.end method
