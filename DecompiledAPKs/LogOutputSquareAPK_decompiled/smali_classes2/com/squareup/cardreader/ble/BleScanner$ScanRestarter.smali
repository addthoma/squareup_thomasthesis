.class Lcom/squareup/cardreader/ble/BleScanner$ScanRestarter;
.super Ljava/lang/Object;
.source "BleScanner.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScanRestarter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/ble/BleScanner;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/ble/BleScanner;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleScanner$ScanRestarter;->this$0:Lcom/squareup/cardreader/ble/BleScanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/ble/BleScanner;Lcom/squareup/cardreader/ble/BleScanner$1;)V
    .locals 0

    .line 113
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleScanner$ScanRestarter;-><init>(Lcom/squareup/cardreader/ble/BleScanner;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Restarting scanning to find new readers"

    .line 115
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner$ScanRestarter;->this$0:Lcom/squareup/cardreader/ble/BleScanner;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleScanner;->access$100(Lcom/squareup/cardreader/ble/BleScanner;)V

    return-void
.end method
