.class final Lcom/squareup/cardreader/ble/BleSender$InitializeHelper$1;
.super Ljava/lang/Object;
.source "BleSender.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public initialize()Landroid/bluetooth/BluetoothGatt;
    .locals 2

    .line 403
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not initialized with an InitializeHelper!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V
    .locals 1

    .line 407
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Not initialized with an InitializeHelper!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
