.class public Lcom/squareup/cardreader/ble/BleSender;
.super Ljava/lang/Object;
.source "BleSender.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;
.implements Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;,
        Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;,
        Lcom/squareup/cardreader/ble/BleSender$WriteCharacteristic;,
        Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;,
        Lcom/squareup/cardreader/ble/BleSender$DiscoverServices;,
        Lcom/squareup/cardreader/ble/BleSender$Disconnect;,
        Lcom/squareup/cardreader/ble/BleSender$CreateConnection;,
        Lcom/squareup/cardreader/ble/BleSender$GattAction;
    }
.end annotation


# static fields
.field private static final RSSI_POLL_INTERVAL:J = 0x3e8L


# instance fields
.field private actionPending:Z

.field private final cardReaderListener:Lcom/squareup/cardreader/CardReaderListeners;

.field private final cardReaderPauseAndResumer:Lcom/squareup/cardreader/CardReaderPauseAndResumer;

.field private gatt:Landroid/bluetooth/BluetoothGatt;

.field private initializeHelper:Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final queue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lcom/squareup/cardreader/ble/BleSender$GattAction;",
            ">;"
        }
    .end annotation
.end field

.field private readRemoteRssiRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderPauseAndResumer;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderListeners;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleSender;->cardReaderPauseAndResumer:Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    .line 60
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleSender;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 61
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleSender;->cardReaderListener:Lcom/squareup/cardreader/CardReaderListeners;

    .line 62
    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleSender;->queue:Ljava/util/Queue;

    const/4 p1, 0x0

    .line 63
    iput-boolean p1, p0, Lcom/squareup/cardreader/ble/BleSender;->actionPending:Z

    .line 64
    new-instance p1, Lcom/squareup/cardreader/ble/-$$Lambda$BleSender$ssfvKRNQgagJhZAfk_m87KJ4FEA;

    invoke-direct {p1, p0}, Lcom/squareup/cardreader/ble/-$$Lambda$BleSender$ssfvKRNQgagJhZAfk_m87KJ4FEA;-><init>(Lcom/squareup/cardreader/ble/BleSender;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleSender;->readRemoteRssiRunnable:Ljava/lang/Runnable;

    .line 66
    sget-object p1, Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;->explodingInitializeHelper:Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleSender;->setInitializeHelper(Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/cardreader/ble/BleSender;)Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleSender;->initializeHelper:Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/cardreader/ble/BleSender;)Landroid/bluetooth/BluetoothGatt;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleSender;->gatt:Landroid/bluetooth/BluetoothGatt;

    return-object p0
.end method

.method static synthetic access$602(Lcom/squareup/cardreader/ble/BleSender;Landroid/bluetooth/BluetoothGatt;)Landroid/bluetooth/BluetoothGatt;
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleSender;->gatt:Landroid/bluetooth/BluetoothGatt;

    return-object p1
.end method

.method static synthetic access$700(Lcom/squareup/cardreader/ble/BleSender;)Lcom/squareup/cardreader/CardReaderListeners;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleSender;->cardReaderListener:Lcom/squareup/cardreader/CardReaderListeners;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 0

    .line 45
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/ble/BleSender;->getCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/ble/BleSender;->reconnectDueToGetCharacteristicFailure(Ljava/util/UUID;Ljava/util/UUID;)V

    return-void
.end method

.method private getCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 3

    .line 173
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->gatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->getServices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 177
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->gatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothGatt;->getService(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->cardReaderListener:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    sget-object v2, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICE_NOT_SUPPORTED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 180
    invoke-static {v2, p1, p2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Ljava/util/UUID;Ljava/util/UUID;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    return-object v1

    .line 184
    :cond_0
    invoke-virtual {v0, p2}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v0

    if-nez v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->cardReaderListener:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    sget-object v2, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_GET_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 187
    invoke-static {v2, p1, p2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Ljava/util/UUID;Ljava/util/UUID;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    return-object v1

    :cond_1
    return-object v0

    .line 174
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Services haven\'t been discovered yet, must do that first!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static synthetic lambda$ssfvKRNQgagJhZAfk_m87KJ4FEA(Lcom/squareup/cardreader/ble/BleSender;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleSender;->readRemoteRssi()V

    return-void
.end method

.method private performAction(Lcom/squareup/cardreader/ble/BleSender$GattAction;)V
    .locals 3

    const/4 v0, 0x1

    .line 159
    iput-boolean v0, p0, Lcom/squareup/cardreader/ble/BleSender;->actionPending:Z

    new-array v0, v0, [Ljava/lang/Object;

    .line 161
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "perform action %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 162
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/cardreader/ble/-$$Lambda$13TsnIPDj4XTZ2AqNlr9R_LlZnE;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ble/-$$Lambda$13TsnIPDj4XTZ2AqNlr9R_LlZnE;-><init>(Lcom/squareup/cardreader/ble/BleSender$GattAction;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private declared-synchronized queueAction(Lcom/squareup/cardreader/ble/BleSender$GattAction;)V
    .locals 1

    monitor-enter p0

    .line 151
    :try_start_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->queue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/BleSender;->actionPending:Z

    if-nez v0, :cond_0

    .line 152
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleSender;->performAction(Lcom/squareup/cardreader/ble/BleSender$GattAction;)V

    goto :goto_0

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->queue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private readRemoteRssi()V
    .locals 4

    .line 378
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->gatt:Landroid/bluetooth/BluetoothGatt;

    if-eqz v0, :cond_0

    .line 379
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->readRemoteRssi()Z

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleSender;->readRemoteRssiRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private reconnectDueToGetCharacteristicFailure(Ljava/util/UUID;Ljava/util/UUID;)V
    .locals 4

    .line 389
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->cardReaderListener:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_GET_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 390
    invoke-static {v1, p1, p2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Ljava/util/UUID;Ljava/util/UUID;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 391
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->initializeHelper:Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;

    new-instance v1, Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    aput-object p2, v2, p1

    const-string p1, "Failed to get service %s or characteristic %s"

    .line 392
    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;-><init>(Ljava/lang/String;)V

    .line 391
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    return-void
.end method

.method private setInitializeHelper(Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;)V
    .locals 0

    .line 385
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleSender;->initializeHelper:Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;

    return-void
.end method


# virtual methods
.method public beginMonitoringRemoteRssi()V
    .locals 4

    .line 115
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleSender;->readRemoteRssiRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public closeConnection()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Queuing close connection"

    .line 119
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleSender;->readRemoteRssiRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->cardReaderPauseAndResumer:Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->removeStartsAndStops(Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;)V

    .line 122
    new-instance v0, Lcom/squareup/cardreader/ble/BleSender$Disconnect;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/cardreader/ble/BleSender$Disconnect;-><init>(Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/ble/BleSender$1;)V

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ble/BleSender;->queueAction(Lcom/squareup/cardreader/ble/BleSender$GattAction;)V

    return-void
.end method

.method public discoverServices()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Queueing discover services"

    .line 87
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    new-instance v0, Lcom/squareup/cardreader/ble/BleSender$DiscoverServices;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/cardreader/ble/BleSender$DiscoverServices;-><init>(Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/ble/BleSender$1;)V

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ble/BleSender;->queueAction(Lcom/squareup/cardreader/ble/BleSender$GattAction;)V

    return-void
.end method

.method public enableNotifications(Ljava/util/UUID;Ljava/util/UUID;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const-string v1, "Queueing enable notifications on characteristic: %s"

    .line 102
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    new-instance v0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;-><init>(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;Lcom/squareup/cardreader/ble/BleSender$1;)V

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ble/BleSender;->queueAction(Lcom/squareup/cardreader/ble/BleSender$GattAction;)V

    return-void
.end method

.method public forgetBond()V
    .locals 3

    .line 147
    sget-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_CONNECTION_CONTROL:Ljava/util/UUID;

    sget-object v2, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->FORGET_BOND:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    invoke-virtual {v2}, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->value()[B

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/squareup/cardreader/ble/BleSender;->writeData(Ljava/util/UUID;Ljava/util/UUID;[B)V

    return-void
.end method

.method public initialize(Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;)V
    .locals 3

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "Clearing the queue and adding create connection"

    .line 78
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleSender;->setInitializeHelper(Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;)V

    .line 80
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleSender;->cardReaderPauseAndResumer:Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-virtual {v1, p0}, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->addStartsAndStops(Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;)V

    .line 81
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleSender;->queue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    .line 82
    iput-boolean v0, p0, Lcom/squareup/cardreader/ble/BleSender;->actionPending:Z

    .line 83
    new-instance v0, Lcom/squareup/cardreader/ble/BleSender$CreateConnection;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/cardreader/ble/BleSender$CreateConnection;-><init>(Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;Lcom/squareup/cardreader/ble/BleSender$1;)V

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ble/BleSender;->queueAction(Lcom/squareup/cardreader/ble/BleSender$GattAction;)V

    return-void
.end method

.method public declared-synchronized onActionCompleted()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 126
    :try_start_0
    iput-boolean v0, p0, Lcom/squareup/cardreader/ble/BleSender;->actionPending:Z

    .line 128
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->queue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BleSender$GattAction;

    if-eqz v0, :cond_0

    .line 130
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ble/BleSender;->performAction(Lcom/squareup/cardreader/ble/BleSender$GattAction;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onStart()V
    .locals 0

    .line 74
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleSender;->beginMonitoringRemoteRssi()V

    return-void
.end method

.method public onStop()V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleSender;->readRemoteRssiRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method public readAckVector()V
    .locals 2

    .line 139
    sget-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_ACK_VECTOR:Ljava/util/UUID;

    invoke-virtual {p0, v0, v1}, Lcom/squareup/cardreader/ble/BleSender;->readCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)V

    return-void
.end method

.method public readCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const-string v1, "Queueing read on characteristic: %s"

    .line 92
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    new-instance v0, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;-><init>(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;Lcom/squareup/cardreader/ble/BleSender$1;)V

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ble/BleSender;->queueAction(Lcom/squareup/cardreader/ble/BleSender$GattAction;)V

    return-void
.end method

.method public readMtu()V
    .locals 2

    .line 143
    sget-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_MTU:Ljava/util/UUID;

    invoke-virtual {p0, v0, v1}, Lcom/squareup/cardreader/ble/BleSender;->readCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)V

    return-void
.end method

.method public writeData(Ljava/util/UUID;Ljava/util/UUID;[B)V
    .locals 8

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const-string v1, "Queueing write data on characteristic: %s"

    .line 97
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    new-instance v0, Lcom/squareup/cardreader/ble/BleSender$WriteCharacteristic;

    const/4 v7, 0x0

    move-object v2, v0

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v2 .. v7}, Lcom/squareup/cardreader/ble/BleSender$WriteCharacteristic;-><init>(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;[BLcom/squareup/cardreader/ble/BleSender$1;)V

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ble/BleSender;->queueAction(Lcom/squareup/cardreader/ble/BleSender$GattAction;)V

    return-void
.end method

.method public writeData([B)V
    .locals 2

    .line 135
    sget-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_WRITE_DATA:Ljava/util/UUID;

    invoke-virtual {p0, v0, v1, p1}, Lcom/squareup/cardreader/ble/BleSender;->writeData(Ljava/util/UUID;Ljava/util/UUID;[B)V

    return-void
.end method
