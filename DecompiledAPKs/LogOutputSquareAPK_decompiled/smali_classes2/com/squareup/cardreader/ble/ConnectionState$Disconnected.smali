.class public final Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;
.super Lcom/squareup/cardreader/ble/ConnectionState;
.source "ConnectionState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/ConnectionState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Disconnected"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0017\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u0010\u0010\u001d\u001a\u0004\u0018\u00010\tH\u00c6\u0003\u00a2\u0006\u0002\u0010\u0012J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003JH\u0010\u001f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00c6\u0001\u00a2\u0006\u0002\u0010 J\u0013\u0010!\u001a\u00020\u00032\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u00d6\u0003J\t\u0010$\u001a\u00020\tH\u00d6\u0001J\t\u0010%\u001a\u00020&H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0015\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\n\n\u0002\u0010\u0013\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0018\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0015\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;",
        "Lcom/squareup/cardreader/ble/ConnectionState;",
        "errorBeforeConnection",
        "",
        "bleError",
        "Lcom/squareup/blecoroutines/BleError;",
        "connectionError",
        "Lcom/squareup/cardreader/ble/ConnectionError;",
        "disconnectStatus",
        "",
        "loggingStateAtDisconnection",
        "Lcom/squareup/cardreader/ble/R12State;",
        "(ZLcom/squareup/blecoroutines/BleError;Lcom/squareup/cardreader/ble/ConnectionError;Ljava/lang/Integer;Lcom/squareup/cardreader/ble/R12State;)V",
        "getBleError",
        "()Lcom/squareup/blecoroutines/BleError;",
        "getConnectionError",
        "()Lcom/squareup/cardreader/ble/ConnectionError;",
        "getDisconnectStatus",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getErrorBeforeConnection",
        "()Z",
        "getLoggingStateAtDisconnection",
        "()Lcom/squareup/cardreader/ble/R12State;",
        "timedOutDisconnecting",
        "getTimedOutDisconnecting",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "(ZLcom/squareup/blecoroutines/BleError;Lcom/squareup/cardreader/ble/ConnectionError;Ljava/lang/Integer;Lcom/squareup/cardreader/ble/R12State;)Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bleError:Lcom/squareup/blecoroutines/BleError;

.field private final connectionError:Lcom/squareup/cardreader/ble/ConnectionError;

.field private final disconnectStatus:Ljava/lang/Integer;

.field private final errorBeforeConnection:Z

.field private final loggingStateAtDisconnection:Lcom/squareup/cardreader/ble/R12State;

.field private final timedOutDisconnecting:Z


# direct methods
.method public constructor <init>(ZLcom/squareup/blecoroutines/BleError;Lcom/squareup/cardreader/ble/ConnectionError;Ljava/lang/Integer;Lcom/squareup/cardreader/ble/R12State;)V
    .locals 1

    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ble/ConnectionState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->errorBeforeConnection:Z

    iput-object p2, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->bleError:Lcom/squareup/blecoroutines/BleError;

    iput-object p3, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->connectionError:Lcom/squareup/cardreader/ble/ConnectionError;

    iput-object p4, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->disconnectStatus:Ljava/lang/Integer;

    iput-object p5, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->loggingStateAtDisconnection:Lcom/squareup/cardreader/ble/R12State;

    .line 24
    iget-object p1, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->disconnectStatus:Ljava/lang/Integer;

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->timedOutDisconnecting:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;ZLcom/squareup/blecoroutines/BleError;Lcom/squareup/cardreader/ble/ConnectionError;Ljava/lang/Integer;Lcom/squareup/cardreader/ble/R12State;ILjava/lang/Object;)Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-boolean p1, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->errorBeforeConnection:Z

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->bleError:Lcom/squareup/blecoroutines/BleError;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->connectionError:Lcom/squareup/cardreader/ble/ConnectionError;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->disconnectStatus:Ljava/lang/Integer;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->loggingStateAtDisconnection:Lcom/squareup/cardreader/ble/R12State;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->copy(ZLcom/squareup/blecoroutines/BleError;Lcom/squareup/cardreader/ble/ConnectionError;Ljava/lang/Integer;Lcom/squareup/cardreader/ble/R12State;)Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->errorBeforeConnection:Z

    return v0
.end method

.method public final component2()Lcom/squareup/blecoroutines/BleError;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->bleError:Lcom/squareup/blecoroutines/BleError;

    return-object v0
.end method

.method public final component3()Lcom/squareup/cardreader/ble/ConnectionError;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->connectionError:Lcom/squareup/cardreader/ble/ConnectionError;

    return-object v0
.end method

.method public final component4()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->disconnectStatus:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component5()Lcom/squareup/cardreader/ble/R12State;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->loggingStateAtDisconnection:Lcom/squareup/cardreader/ble/R12State;

    return-object v0
.end method

.method public final copy(ZLcom/squareup/blecoroutines/BleError;Lcom/squareup/cardreader/ble/ConnectionError;Ljava/lang/Integer;Lcom/squareup/cardreader/ble/R12State;)Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;
    .locals 7

    new-instance v6, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    move-object v0, v6

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;-><init>(ZLcom/squareup/blecoroutines/BleError;Lcom/squareup/cardreader/ble/ConnectionError;Ljava/lang/Integer;Lcom/squareup/cardreader/ble/R12State;)V

    return-object v6
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->errorBeforeConnection:Z

    iget-boolean v1, p1, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->errorBeforeConnection:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->bleError:Lcom/squareup/blecoroutines/BleError;

    iget-object v1, p1, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->bleError:Lcom/squareup/blecoroutines/BleError;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->connectionError:Lcom/squareup/cardreader/ble/ConnectionError;

    iget-object v1, p1, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->connectionError:Lcom/squareup/cardreader/ble/ConnectionError;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->disconnectStatus:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->disconnectStatus:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->loggingStateAtDisconnection:Lcom/squareup/cardreader/ble/R12State;

    iget-object p1, p1, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->loggingStateAtDisconnection:Lcom/squareup/cardreader/ble/R12State;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBleError()Lcom/squareup/blecoroutines/BleError;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->bleError:Lcom/squareup/blecoroutines/BleError;

    return-object v0
.end method

.method public final getConnectionError()Lcom/squareup/cardreader/ble/ConnectionError;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->connectionError:Lcom/squareup/cardreader/ble/ConnectionError;

    return-object v0
.end method

.method public final getDisconnectStatus()Ljava/lang/Integer;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->disconnectStatus:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getErrorBeforeConnection()Z
    .locals 1

    .line 18
    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->errorBeforeConnection:Z

    return v0
.end method

.method public final getLoggingStateAtDisconnection()Lcom/squareup/cardreader/ble/R12State;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->loggingStateAtDisconnection:Lcom/squareup/cardreader/ble/R12State;

    return-object v0
.end method

.method public final getTimedOutDisconnecting()Z
    .locals 1

    .line 24
    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->timedOutDisconnecting:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->errorBeforeConnection:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->bleError:Lcom/squareup/blecoroutines/BleError;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->connectionError:Lcom/squareup/cardreader/ble/ConnectionError;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->disconnectStatus:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->loggingStateAtDisconnection:Lcom/squareup/cardreader/ble/R12State;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Disconnected(errorBeforeConnection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->errorBeforeConnection:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", bleError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->bleError:Lcom/squareup/blecoroutines/BleError;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", connectionError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->connectionError:Lcom/squareup/cardreader/ble/ConnectionError;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", disconnectStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->disconnectStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", loggingStateAtDisconnection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->loggingStateAtDisconnection:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
