.class final Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;
.super Lkotlin/coroutines/jvm/internal/ContinuationImpl;
.source "ConnectionNegotiator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/ble/RealConnectionNegotiator;->negotiateConnection(Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lcom/squareup/blecoroutines/Connection;Landroid/content/Context;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u0096@"
    }
    d2 = {
        "negotiateConnection",
        "",
        "device",
        "Landroid/bluetooth/BluetoothDevice;",
        "events",
        "Lcom/squareup/cardreader/ble/RealConnectionEvents;",
        "connection",
        "Lcom/squareup/blecoroutines/Connection;",
        "context",
        "Landroid/content/Context;",
        "timeouts",
        "Lcom/squareup/cardreader/ble/Timeouts;",
        "continuation",
        "Lkotlin/coroutines/Continuation;",
        "Lcom/squareup/cardreader/ble/NegotiatedConnection;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.cardreader.ble.RealConnectionNegotiator"
    f = "ConnectionNegotiator.kt"
    i = {
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x2,
        0x2,
        0x2,
        0x2,
        0x2,
        0x2,
        0x2,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3,
        0x4,
        0x4,
        0x4,
        0x4,
        0x4,
        0x4,
        0x4,
        0x4,
        0x4,
        0x5,
        0x5,
        0x5,
        0x5,
        0x5,
        0x5,
        0x5,
        0x5,
        0x5,
        0x6,
        0x6,
        0x6,
        0x6,
        0x6,
        0x6,
        0x6,
        0x6,
        0x6,
        0x7,
        0x7,
        0x7,
        0x7,
        0x7,
        0x7,
        0x7,
        0x7,
        0x7,
        0x7,
        0x8,
        0x8,
        0x8,
        0x8,
        0x8,
        0x8,
        0x8,
        0x8,
        0x8,
        0x8,
        0x9,
        0x9,
        0x9,
        0x9,
        0x9,
        0x9,
        0x9,
        0x9,
        0x9,
        0x9,
        0xa,
        0xa,
        0xa,
        0xa,
        0xa,
        0xa,
        0xa,
        0xa,
        0xa,
        0xa,
        0xb,
        0xb,
        0xb,
        0xb,
        0xb,
        0xb,
        0xb,
        0xb,
        0xb,
        0xb,
        0xb,
        0xc,
        0xc,
        0xc,
        0xc,
        0xc,
        0xc,
        0xc,
        0xc,
        0xc,
        0xc,
        0xc,
        0xc
    }
    l = {
        0x4b,
        0x4e,
        0x57,
        0x60,
        0x65,
        0x6c,
        0x75,
        0x7b,
        0x7e,
        0x81,
        0x86,
        0x8c,
        0x93
    }
    m = "negotiateConnection"
    n = {
        "this",
        "device",
        "events",
        "connection",
        "context",
        "timeouts",
        "this",
        "device",
        "events",
        "connection",
        "context",
        "timeouts",
        "this",
        "device",
        "events",
        "connection",
        "context",
        "timeouts",
        "serviceVersion",
        "this",
        "device",
        "events",
        "connection",
        "context",
        "timeouts",
        "serviceVersion",
        "serialNumber",
        "this",
        "device",
        "events",
        "connection",
        "context",
        "timeouts",
        "serviceVersion",
        "serialNumber",
        "androidThinksWeAreBonded",
        "this",
        "device",
        "events",
        "connection",
        "context",
        "timeouts",
        "serviceVersion",
        "serialNumber",
        "readerBondStatus",
        "this",
        "device",
        "events",
        "connection",
        "context",
        "timeouts",
        "serviceVersion",
        "serialNumber",
        "readerBondStatus",
        "this",
        "device",
        "events",
        "connection",
        "context",
        "timeouts",
        "serviceVersion",
        "serialNumber",
        "readerBondStatus",
        "connectionControl",
        "this",
        "device",
        "events",
        "connection",
        "context",
        "timeouts",
        "serviceVersion",
        "serialNumber",
        "readerBondStatus",
        "connectionControl",
        "this",
        "device",
        "events",
        "connection",
        "context",
        "timeouts",
        "serviceVersion",
        "serialNumber",
        "readerBondStatus",
        "connectionControl",
        "this",
        "device",
        "events",
        "connection",
        "context",
        "timeouts",
        "serviceVersion",
        "serialNumber",
        "readerBondStatus",
        "connectionControl",
        "this",
        "device",
        "events",
        "connection",
        "context",
        "timeouts",
        "serviceVersion",
        "serialNumber",
        "readerBondStatus",
        "connectionControl",
        "commsVersion",
        "this",
        "device",
        "events",
        "connection",
        "context",
        "timeouts",
        "serviceVersion",
        "serialNumber",
        "readerBondStatus",
        "connectionControl",
        "commsVersion",
        "mtuChannel"
    }
    s = {
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$7",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$7",
        "I$0",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$7",
        "L$8",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$7",
        "L$8",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$7",
        "L$8",
        "L$9",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$7",
        "L$8",
        "L$9",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$7",
        "L$8",
        "L$9",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$7",
        "L$8",
        "L$9",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$7",
        "L$8",
        "L$9",
        "L$10",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$7",
        "L$8",
        "L$9",
        "L$10",
        "L$11"
    }
.end annotation


# instance fields
.field I$0:I

.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field L$10:Ljava/lang/Object;

.field L$11:Ljava/lang/Object;

.field L$2:Ljava/lang/Object;

.field L$3:Ljava/lang/Object;

.field L$4:Ljava/lang/Object;

.field L$5:Ljava/lang/Object;

.field L$6:Ljava/lang/Object;

.field L$7:Ljava/lang/Object;

.field L$8:Ljava/lang/Object;

.field L$9:Ljava/lang/Object;

.field label:I

.field synthetic result:Ljava/lang/Object;

.field final synthetic this$0:Lcom/squareup/cardreader/ble/RealConnectionNegotiator;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/ble/RealConnectionNegotiator;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->this$0:Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-direct {p0, p2}, Lkotlin/coroutines/jvm/internal/ContinuationImpl;-><init>(Lkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->result:Ljava/lang/Object;

    iget p1, p0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    const/high16 v0, -0x80000000

    or-int/2addr p1, v0

    iput p1, p0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->this$0:Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;->negotiateConnection(Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lcom/squareup/blecoroutines/Connection;Landroid/content/Context;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
