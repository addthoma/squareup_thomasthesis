.class public interface abstract Lcom/squareup/cardreader/SecureSessionFeature;
.super Ljava/lang/Object;
.source "SecureSessionFeature.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0005H&J\u0008\u0010\t\u001a\u00020\u0003H&J\u0018\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH&J \u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0011H&\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/cardreader/SecureSessionFeature;",
        "",
        "onPinRequested",
        "",
        "canSkip",
        "",
        "cardInfo",
        "Lcom/squareup/cardreader/CardInfo;",
        "finalPinAttempt",
        "onSecureSessionInvalid",
        "onSecureSessionSendToServer",
        "msgType",
        "",
        "packet",
        "",
        "onSecureSessionValid",
        "sessionId",
        "",
        "readerTransactionCount",
        "readerUtcEpochTime",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract onPinRequested(ZLcom/squareup/cardreader/CardInfo;Z)V
.end method

.method public abstract onSecureSessionInvalid()V
.end method

.method public abstract onSecureSessionSendToServer(I[B)V
.end method

.method public abstract onSecureSessionValid(JJJ)V
.end method
