.class public final Lcom/squareup/cardreader/LcrModule_ProvideCoredumpFeatureNativeFactory;
.super Ljava/lang/Object;
.source "LcrModule_ProvideCoredumpFeatureNativeFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/LcrModule_ProvideCoredumpFeatureNativeFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/cardreader/LcrModule_ProvideCoredumpFeatureNativeFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideCoredumpFeatureNativeFactory$InstanceHolder;->access$000()Lcom/squareup/cardreader/LcrModule_ProvideCoredumpFeatureNativeFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideCoredumpFeatureNative()Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/cardreader/LcrModule;->provideCoredumpFeatureNative()Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideCoredumpFeatureNativeFactory;->provideCoredumpFeatureNative()Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/LcrModule_ProvideCoredumpFeatureNativeFactory;->get()Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;

    move-result-object v0

    return-object v0
.end method
