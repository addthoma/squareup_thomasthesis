.class public final Lcom/squareup/cardreader/CardReaderPowerMonitor_Factory;
.super Ljava/lang/Object;
.source "CardReaderPowerMonitor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderPowerMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor_Factory;->clockProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderPowerMonitor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)",
            "Lcom/squareup/cardreader/CardReaderPowerMonitor_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/cardreader/CardReaderPowerMonitor_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/CardReaderPowerMonitor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/util/Clock;)Lcom/squareup/cardreader/CardReaderPowerMonitor;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/cardreader/CardReaderPowerMonitor;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/CardReaderPowerMonitor;-><init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/util/Clock;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderPowerMonitor;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Clock;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/CardReaderPowerMonitor_Factory;->newInstance(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/util/Clock;)Lcom/squareup/cardreader/CardReaderPowerMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderPowerMonitor_Factory;->get()Lcom/squareup/cardreader/CardReaderPowerMonitor;

    move-result-object v0

    return-object v0
.end method
