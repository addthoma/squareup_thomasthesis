.class public abstract Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "FirmwareUpdateFeatureOutput"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$OnVersionInfo;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$UpdateProgress;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$UpdateComplete;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$OnFirmwareUpdateTmsCountryChanges;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$FirmwareUpdateResult;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0006\u0003\u0004\u0005\u0006\u0007\u0008B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0006\t\n\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;",
        "()V",
        "FirmwareUpdateResult",
        "OnFirmwareUpdateTmsCountryChanges",
        "OnVersionInfo",
        "ReceivedManifest",
        "UpdateComplete",
        "UpdateProgress",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$OnVersionInfo;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$UpdateProgress;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$UpdateComplete;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$OnFirmwareUpdateTmsCountryChanges;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$FirmwareUpdateResult;",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 308
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 308
    invoke-direct {p0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput;-><init>()V

    return-void
.end method
