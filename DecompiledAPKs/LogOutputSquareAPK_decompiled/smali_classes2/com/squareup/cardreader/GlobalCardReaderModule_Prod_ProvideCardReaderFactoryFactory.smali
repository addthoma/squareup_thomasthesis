.class public final Lcom/squareup/cardreader/GlobalCardReaderModule_Prod_ProvideCardReaderFactoryFactory;
.super Ljava/lang/Object;
.source "GlobalCardReaderModule_Prod_ProvideCardReaderFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;

.field private final realCardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderFactory;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_Prod_ProvideCardReaderFactoryFactory;->module:Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;

    .line 25
    iput-object p2, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_Prod_ProvideCardReaderFactoryFactory;->realCardReaderFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalCardReaderModule_Prod_ProvideCardReaderFactoryFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderFactory;",
            ">;)",
            "Lcom/squareup/cardreader/GlobalCardReaderModule_Prod_ProvideCardReaderFactoryFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/cardreader/GlobalCardReaderModule_Prod_ProvideCardReaderFactoryFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/GlobalCardReaderModule_Prod_ProvideCardReaderFactoryFactory;-><init>(Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCardReaderFactory(Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/RealCardReaderFactory;)Lcom/squareup/cardreader/CardReaderFactory;
    .locals 0

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;->provideCardReaderFactory(Lcom/squareup/cardreader/RealCardReaderFactory;)Lcom/squareup/cardreader/CardReaderFactory;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderFactory;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderFactory;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_Prod_ProvideCardReaderFactoryFactory;->module:Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;

    iget-object v1, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_Prod_ProvideCardReaderFactoryFactory;->realCardReaderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/RealCardReaderFactory;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/GlobalCardReaderModule_Prod_ProvideCardReaderFactoryFactory;->provideCardReaderFactory(Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;Lcom/squareup/cardreader/RealCardReaderFactory;)Lcom/squareup/cardreader/CardReaderFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalCardReaderModule_Prod_ProvideCardReaderFactoryFactory;->get()Lcom/squareup/cardreader/CardReaderFactory;

    move-result-object v0

    return-object v0
.end method
