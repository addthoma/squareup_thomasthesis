.class public interface abstract Lcom/squareup/cardreader/BlePairingListener;
.super Ljava/lang/Object;
.source "BlePairingListener.java"


# virtual methods
.method public abstract onPairingFailed(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleErrorType;)V
.end method

.method public abstract onPairingSuccess(Lcom/squareup/cardreader/WirelessConnection;)V
.end method

.method public abstract onReaderForceUnPair(Lcom/squareup/cardreader/WirelessConnection;)V
.end method
