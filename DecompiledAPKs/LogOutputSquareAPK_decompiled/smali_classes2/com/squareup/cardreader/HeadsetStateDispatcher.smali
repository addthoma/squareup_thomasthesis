.class public Lcom/squareup/cardreader/HeadsetStateDispatcher;
.super Ljava/lang/Object;
.source "HeadsetStateDispatcher.java"

# interfaces
.implements Lcom/squareup/wavpool/swipe/Headset$Listener;
.implements Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;


# instance fields
.field private cardReaderContext:Lcom/squareup/cardreader/CardReaderContext;

.field private final cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final headsetConnectionState:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;"
        }
    .end annotation
.end field

.field private headsetEnabled:Z

.field private final libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

.field private final rootBus:Lcom/squareup/badbus/BadEventSink;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/loader/LibraryLoader;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->headsetConnectionState:Ljavax/inject/Provider;

    .line 44
    iput-object p2, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    .line 45
    iput-object p3, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 46
    iput-object p4, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    .line 47
    iput-object p5, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->rootBus:Lcom/squareup/badbus/BadEventSink;

    .line 48
    iput-object p6, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private externalListener()Lcom/squareup/cardreader/LibraryLoadErrorListener;
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getLibraryLoadErrorListener()Lcom/squareup/cardreader/LibraryLoadErrorListener;

    move-result-object v0

    return-object v0
.end method

.method private onHeadsetConnected()V
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->cardReaderContext:Lcom/squareup/cardreader/CardReaderContext;

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderFactory;->forAudio()Lcom/squareup/cardreader/CardReaderContext;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->cardReaderContext:Lcom/squareup/cardreader/CardReaderContext;

    :cond_0
    return-void
.end method

.method private onHeadsetDisconnected()V
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->cardReaderContext:Lcom/squareup/cardreader/CardReaderContext;

    if-eqz v0, :cond_0

    .line 125
    iget-object v1, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v0, v0, Lcom/squareup/cardreader/CardReaderContext;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/CardReaderFactory;->destroy(Lcom/squareup/cardreader/CardReaderId;)V

    const/4 v0, 0x0

    .line 126
    iput-object v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->cardReaderContext:Lcom/squareup/cardreader/CardReaderContext;

    :cond_0
    return-void
.end method

.method private performHeadsetWork(Lcom/squareup/wavpool/swipe/HeadsetConnectionState;)V
    .locals 3

    .line 88
    iget-boolean v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->headsetEnabled:Z

    if-nez v0, :cond_0

    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0}, Lcom/squareup/cardreader/loader/LibraryLoader;->hasLoadError()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->isReaderConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    invoke-direct {p0}, Lcom/squareup/cardreader/HeadsetStateDispatcher;->externalListener()Lcom/squareup/cardreader/LibraryLoadErrorListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/cardreader/LibraryLoadErrorListener;->onLibrariesFailedToLoad()V

    return-void

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0}, Lcom/squareup/cardreader/loader/LibraryLoader;->isLoaded()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_2

    .line 97
    iget-object p1, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {p1, p0}, Lcom/squareup/cardreader/loader/LibraryLoader;->addLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V

    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "Native libs are not yet loaded - ignoring headset info for now."

    .line 98
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 103
    :cond_2
    iget-object v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->DISABLE_MAGSTRIPE_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "Ignoring audio data - magstripe disabled by feature flag"

    .line 104
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 108
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->isReaderConnected()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 109
    invoke-direct {p0}, Lcom/squareup/cardreader/HeadsetStateDispatcher;->onHeadsetConnected()V

    goto :goto_0

    .line 111
    :cond_4
    invoke-direct {p0}, Lcom/squareup/cardreader/HeadsetStateDispatcher;->onHeadsetDisconnected()V

    .line 114
    :goto_0
    iget-object v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->rootBus:Lcom/squareup/badbus/BadEventSink;

    invoke-interface {v0, p1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public onHeadsetConnectionChanged(Lcom/squareup/wavpool/swipe/HeadsetConnectionState;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "HeadsetStateDispatcher: headset connection changed: %s"

    .line 82
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/HeadsetStateDispatcher;->performHeadsetWork(Lcom/squareup/wavpool/swipe/HeadsetConnectionState;)V

    return-void
.end method

.method public onLibrariesFailedToLoad(Ljava/lang/String;)V
    .locals 0

    .line 78
    iget-object p1, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {p1, p0}, Lcom/squareup/cardreader/loader/LibraryLoader;->removeLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V

    return-void
.end method

.method public onLibrariesLoaded()V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/loader/LibraryLoader;->removeLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "HeadsetStateDispatcher: native libraries have been loaded!"

    .line 73
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->headsetConnectionState:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/HeadsetStateDispatcher;->performHeadsetWork(Lcom/squareup/wavpool/swipe/HeadsetConnectionState;)V

    return-void
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    .line 56
    iput-boolean v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->headsetEnabled:Z

    .line 57
    invoke-direct {p0}, Lcom/squareup/cardreader/HeadsetStateDispatcher;->onHeadsetDisconnected()V

    return-void
.end method

.method public setHeadsetEnabled(Ljava/lang/Boolean;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "HeadsetStateDispatcher#setHeadsetEnabled(%s)"

    .line 65
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->headsetEnabled:Z

    .line 67
    iget-object p1, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher;->headsetConnectionState:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/HeadsetStateDispatcher;->performHeadsetWork(Lcom/squareup/wavpool/swipe/HeadsetConnectionState;)V

    return-void
.end method
