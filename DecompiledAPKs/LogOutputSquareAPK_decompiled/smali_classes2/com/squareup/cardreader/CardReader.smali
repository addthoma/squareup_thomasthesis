.class public interface abstract Lcom/squareup/cardreader/CardReader;
.super Ljava/lang/Object;
.source "CardReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;
    }
.end annotation


# virtual methods
.method public abstract abortSecureSession(Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V
.end method

.method public abstract ackTmnWriteNotify()V
.end method

.method public abstract cancelPayment()V
.end method

.method public abstract cancelTmnRequest()V
.end method

.method public abstract enableSwipePassthrough(Z)V
.end method

.method public abstract forget()V
.end method

.method public abstract getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;
.end method

.method public abstract getId()Lcom/squareup/cardreader/CardReaderId;
.end method

.method public abstract identify()V
.end method

.method public abstract initializeFeatures(IILcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V
.end method

.method public abstract onCoreDumpDataSent()V
.end method

.method public abstract onPinBypass()V
.end method

.method public abstract onPinDigitEntered(I)V
.end method

.method public abstract onPinPadReset()V
.end method

.method public abstract onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
.end method

.method public abstract onTamperDataSent()V
.end method

.method public abstract powerOff()V
.end method

.method public abstract processARPC([B)V
.end method

.method public abstract processFirmwareUpdateResponse(Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;)V
.end method

.method public abstract processSecureSessionMessageFromServer([B)V
.end method

.method public abstract reinitializeSecureSession()V
.end method

.method public abstract requestPowerStatus()V
.end method

.method public abstract reset()V
.end method

.method public abstract selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V
.end method

.method public abstract selectApplication(Lcom/squareup/cardreader/EmvApplication;)V
.end method

.method public abstract sendPowerupHint(I)V
.end method

.method public abstract sendTmnDataToReader([B)V
.end method

.method public abstract startPayment(JJ)V
.end method

.method public abstract startRefund(JJ)V
.end method

.method public abstract startTmnCheckBalance(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
.end method

.method public abstract startTmnMiryo([B)V
.end method

.method public abstract startTmnPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
.end method

.method public abstract startTmnRefund(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
.end method

.method public abstract startTmnTestPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
.end method

.method public abstract submitPinBlock()V
.end method
