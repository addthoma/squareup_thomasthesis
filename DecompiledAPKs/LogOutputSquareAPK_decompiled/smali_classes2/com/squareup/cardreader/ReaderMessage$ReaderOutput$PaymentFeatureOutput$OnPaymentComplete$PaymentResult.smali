.class public final enum Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;
.super Ljava/lang/Enum;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PaymentResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u000b\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;",
        "",
        "(Ljava/lang/String;I)V",
        "Terminated",
        "SuccessIccApprove",
        "SuccessIccApproveWithSignature",
        "FailureIccDecline",
        "FailureIccReverse",
        "SuccessMagstripe",
        "SuccessMagstripeSchemeFallback",
        "SuccessMagstripeTechnicalFallback",
        "TimedOut",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

.field public static final enum FailureIccDecline:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

.field public static final enum FailureIccReverse:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

.field public static final enum SuccessIccApprove:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

.field public static final enum SuccessIccApproveWithSignature:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

.field public static final enum SuccessMagstripe:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

.field public static final enum SuccessMagstripeSchemeFallback:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

.field public static final enum SuccessMagstripeTechnicalFallback:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

.field public static final enum Terminated:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

.field public static final enum TimedOut:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    const/4 v2, 0x0

    const-string v3, "Terminated"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->Terminated:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    const/4 v2, 0x1

    const-string v3, "SuccessIccApprove"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->SuccessIccApprove:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    const/4 v2, 0x2

    const-string v3, "SuccessIccApproveWithSignature"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->SuccessIccApproveWithSignature:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    const/4 v2, 0x3

    const-string v3, "FailureIccDecline"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->FailureIccDecline:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    const/4 v2, 0x4

    const-string v3, "FailureIccReverse"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->FailureIccReverse:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    const/4 v2, 0x5

    const-string v3, "SuccessMagstripe"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->SuccessMagstripe:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    const/4 v2, 0x6

    const-string v3, "SuccessMagstripeSchemeFallback"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->SuccessMagstripeSchemeFallback:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    const/4 v2, 0x7

    const-string v3, "SuccessMagstripeTechnicalFallback"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->SuccessMagstripeTechnicalFallback:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    const/16 v2, 0x8

    const-string v3, "TimedOut"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->TimedOut:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->$VALUES:[Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 367
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;
    .locals 1

    const-class v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;
    .locals 1

    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->$VALUES:[Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    return-object v0
.end method
