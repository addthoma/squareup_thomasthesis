.class public Lcom/squareup/cardreader/X2BranCardReaderInfoReceiver;
.super Ljava/lang/Object;
.source "X2BranCardReaderInfoReceiver.java"


# instance fields
.field private final branCardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/cardreader/X2BranCardReaderInfoReceiver;->branCardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-void
.end method


# virtual methods
.method public processMessageFromReader(Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;)V
    .locals 3

    .line 22
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/squareup/cardreader/X2BranCardReaderInfoReceiver;->branCardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;->card_reader_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderInfo;->updateWithProto(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;)V

    return-void

    .line 25
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
