.class final Lcom/squareup/cardreader/CardreaderMessenger$responses$1;
.super Ljava/lang/Object;
.source "CardreaderMessengerInterface.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/CardreaderMessenger$DefaultImpls;->responses(Lcom/squareup/cardreader/CardreaderMessenger;Lcom/squareup/cardreader/CardreaderConnectionId;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lcom/squareup/cardreader/ReaderPayload<",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/cardreader/ReaderPayload;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $cardreaderId:Lcom/squareup/cardreader/CardreaderConnectionId;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CardreaderConnectionId;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cardreader/CardreaderMessenger$responses$1;->$cardreaderId:Lcom/squareup/cardreader/CardreaderConnectionId;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Lcom/squareup/cardreader/ReaderPayload;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ReaderPayload<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderPayload;->getReaderId()Lcom/squareup/cardreader/CardreaderConnectionId;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/CardreaderMessenger$responses$1;->$cardreaderId:Lcom/squareup/cardreader/CardreaderConnectionId;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/cardreader/ReaderPayload;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/CardreaderMessenger$responses$1;->test(Lcom/squareup/cardreader/ReaderPayload;)Z

    move-result p1

    return p1
.end method
