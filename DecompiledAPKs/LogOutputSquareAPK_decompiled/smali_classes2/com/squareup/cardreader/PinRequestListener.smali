.class public interface abstract Lcom/squareup/cardreader/PinRequestListener;
.super Ljava/lang/Object;
.source "PinRequestListener.java"


# virtual methods
.method public abstract onHardwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
.end method

.method public abstract onSoftwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PinRequestData;)V
.end method
