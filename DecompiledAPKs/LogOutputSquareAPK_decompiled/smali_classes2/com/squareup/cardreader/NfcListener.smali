.class public interface abstract Lcom/squareup/cardreader/NfcListener;
.super Ljava/lang/Object;
.source "NfcListener.java"


# virtual methods
.method public abstract onAudioVisualRequest(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V
.end method

.method public abstract onNfcActionRequired(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onNfcAuthorizationRequestReceived(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/CardInfo;)V
.end method

.method public abstract onNfcCardBlocked(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onNfcCardDeclined(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onNfcCollisionDetected(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onNfcInterfaceUnavailable(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onNfcLimitExceededInsertCard(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onNfcLimitExceededTryAnotherCard(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onNfcPresentCardAgain(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onNfcProcessingError(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onNfcTimedOut(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V
.end method

.method public abstract onNfcTryAnotherCard(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onNfcUnlockDevice(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onRequestTapCard(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method
