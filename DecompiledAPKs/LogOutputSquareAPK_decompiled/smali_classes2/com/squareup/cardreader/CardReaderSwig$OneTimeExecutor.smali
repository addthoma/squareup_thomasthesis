.class Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;
.super Ljava/lang/Object;
.source "CardReaderSwig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderSwig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OneTimeExecutor"
.end annotation


# instance fields
.field private final sentOneTimeMessages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/cardreader/CardReaderSwig;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CardReaderSwig;)V
    .locals 0

    .line 897
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 898
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;->sentOneTimeMessages:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method execute(Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;Ljava/lang/Runnable;)V
    .locals 1

    .line 906
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$1200(Lcom/squareup/cardreader/CardReaderSwig;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 910
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;->sentOneTimeMessages:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 911
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderSwig;->access$1300(Lcom/squareup/cardreader/CardReaderSwig;)Ljava/util/concurrent/Executor;

    move-result-object p1

    invoke-interface {p1, p2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method reset()V
    .locals 1

    .line 902
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;->sentOneTimeMessages:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method
