.class public final Lcom/squareup/cardreader/SecureSessionFeatureV2;
.super Ljava/lang/Object;
.source "SecureSessionFeatureV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/CanReset;
.implements Lcom/squareup/cardreader/SecureSessionFeature;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSecureSessionFeatureV2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SecureSessionFeatureV2.kt\ncom/squareup/cardreader/SecureSessionFeatureV2\n*L\n1#1,153:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002B\u001b\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J \u0010\u0019\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0016H\u0016J\u0008\u0010\u001e\u001a\u00020\u0010H\u0016J\u0018\u0010\u001f\u001a\u00020\u00102\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0016J \u0010$\u001a\u00020\u00102\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020&2\u0006\u0010(\u001a\u00020&H\u0016J\u0012\u0010)\u001a\u0004\u0018\u00010\u00142\u0006\u0010*\u001a\u00020#H\u0002J\u0008\u0010+\u001a\u00020\u0010H\u0002J\u0008\u0010,\u001a\u00020\u0010H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/cardreader/SecureSessionFeatureV2;",
        "Lcom/squareup/cardreader/CanReset;",
        "Lcom/squareup/cardreader/SecureSessionFeature;",
        "posSender",
        "Lcom/squareup/cardreader/SendsToPos;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput;",
        "cardreaderProvider",
        "Lcom/squareup/cardreader/CardreaderPointerProvider;",
        "(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V",
        "featurePointer",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;",
        "getFeaturePointer",
        "()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;",
        "setFeaturePointer",
        "(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)V",
        "handleMessage",
        "",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage;",
        "initialize",
        "Lcom/squareup/cardreader/lcr/CrSecureSessionResult;",
        "isReaderSdkApp",
        "",
        "minesweeperTicket",
        "",
        "onPinRequested",
        "canSkip",
        "cardInfo",
        "Lcom/squareup/cardreader/CardInfo;",
        "finalPinAttempt",
        "onSecureSessionInvalid",
        "onSecureSessionSendToServer",
        "msgType",
        "",
        "packet",
        "",
        "onSecureSessionValid",
        "sessionId",
        "",
        "readerTransactionCount",
        "readerUtcEpochTime",
        "processServerMessage",
        "data",
        "readThingFromDisk",
        "resetIfInitilized",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

.field public featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

.field private final posSender:Lcom/squareup/cardreader/SendsToPos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput;",
            ">;",
            "Lcom/squareup/cardreader/CardreaderPointerProvider;",
            ")V"
        }
    .end annotation

    const-string v0, "posSender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardreaderProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    iput-object p2, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    return-void
.end method

.method private final initialize(ZLjava/lang/Object;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardreaderPointerProvider;->cardreaderPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    .line 59
    invoke-static {v0, p0, p2}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->securesession_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    move-result-object p2

    const-string v0, "SecureSessionFeatureNati\u2026minesweeperTicket\n      )"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-eqz p1, :cond_0

    .line 63
    invoke-direct {p0}, Lcom/squareup/cardreader/SecureSessionFeatureV2;->readThingFromDisk()V

    .line 65
    :cond_0
    sget-object p1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    return-object p1
.end method

.method private final processServerMessage([B)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 7

    .line 76
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->securesession_recv_server_message(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;[B)Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;

    move-result-object p1

    .line 77
    new-instance v6, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionResult;

    const-string v0, "result"

    .line 78
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->getResult()Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object v1

    const-string v0, "result.result"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->getError()Lcom/squareup/cardreader/lcr/CrSecureSessionServerError;

    move-result-object v2

    const-string v0, "result.error"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->getUxHint()Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    move-result-object v3

    const-string v0, "result.uxHint"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->getLocalizedTitle()Ljava/lang/String;

    move-result-object v4

    const-string v0, "result.localizedTitle"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->getLocalizedDescription()Ljava/lang/String;

    move-result-object v5

    const-string p1, "result.localizedDescription"

    invoke-static {v5, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v6

    .line 77
    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionResult;-><init>(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;Lcom/squareup/cardreader/lcr/CrSecureSessionServerError;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    iget-object p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    check-cast v6, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {p1, v6}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method private final readThingFromDisk()V
    .locals 4

    .line 95
    :try_start_0
    sget-object v0, Lcom/squareup/sdk/reader/Client;->WBID:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 96
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 97
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v0

    .line 98
    new-array v0, v0, [B

    .line 100
    :goto_0
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayInputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 101
    iget-object v2, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-nez v2, :cond_0

    const-string v3, "featurePointer"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v2, v0}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->set_kb(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_1
    return-void
.end method


# virtual methods
.method public final getFeaturePointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final handleMessage(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage;)V
    .locals 3

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage$Initialize;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage$Initialize;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage$Initialize;->isReaderSdkApp()Z

    move-result v1

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage$Initialize;->getMinesweeperTicket()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/squareup/cardreader/SecureSessionFeatureV2;->initialize(ZLjava/lang/Object;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object v0

    goto :goto_0

    .line 36
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage$ProcessServerMessage;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage$ProcessServerMessage;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage$ProcessServerMessage;->getData()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/SecureSessionFeatureV2;->processServerMessage([B)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object v0

    goto :goto_0

    .line 37
    :cond_1
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage$PinPadReset;

    const-string v1, "featurePointer"

    if-eqz v0, :cond_3

    .line 38
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->pin_reset(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)V

    .line 39
    sget-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    goto :goto_0

    .line 41
    :cond_3
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage$OnPinDigitEntered;

    if-eqz v0, :cond_5

    .line 42
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    move-object v1, p1

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage$OnPinDigitEntered;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage$OnPinDigitEntered;->getDigit()I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->pin_add_digit(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;I)Z

    .line 43
    sget-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    goto :goto_0

    .line 45
    :cond_5
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage$SubmitPinBlock;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->pin_submit(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object v0

    goto :goto_0

    .line 46
    :cond_7
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage$OnPinBypass;

    if-eqz v0, :cond_a

    .line 47
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-nez v0, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->cr_securesession_feature_pin_bypass(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_9

    .line 52
    iget-object v1, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$SecureSessionResult;

    invoke-direct {v2, v0, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$SecureSessionResult;-><init>(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureSessionFeatureMessage;)V

    check-cast v2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v1, v2}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    :cond_9
    return-void

    .line 47
    :cond_a
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public onPinRequested(ZLcom/squareup/cardreader/CardInfo;Z)V
    .locals 3

    const-string v0, "cardInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    new-instance v0, Lcom/squareup/cardreader/CardDescription;

    invoke-virtual {p2}, Lcom/squareup/cardreader/CardInfo;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/cardreader/CardInfo;->getLast4()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/squareup/cardreader/CardInfo;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, v1, v2, p2}, Lcom/squareup/cardreader/CardDescription;-><init>(Lcom/squareup/Card$Brand;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object p2, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 147
    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnPinRequested;

    invoke-direct {v1, p1, v0, p3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnPinRequested;-><init>(ZLcom/squareup/cardreader/CardDescription;Z)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 146
    invoke-interface {p2, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onSecureSessionInvalid()V
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    sget-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionInvalid;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionInvalid;

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onSecureSessionSendToServer(I[B)V
    .locals 3

    const-string v0, "packet"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionMessageType;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionMessageType;

    move-result-object p1

    .line 117
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionSendToServer;

    const-string v2, "type"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionSendToServer;-><init>(Lcom/squareup/cardreader/lcr/CrSecureSessionMessageType;[B)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onSecureSessionValid(JJJ)V
    .locals 9

    .line 126
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 127
    new-instance v8, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;

    move-object v1, v8

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureSessionFeatureOutput$OnSecureSessionValid;-><init>(JJJ)V

    check-cast v8, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 126
    invoke-interface {v0, v8}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public resetIfInitilized()V
    .locals 2

    .line 69
    move-object v0, p0

    check-cast v0, Lcom/squareup/cardreader/SecureSessionFeatureV2;

    iget-object v0, v0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-eqz v0, :cond_2

    .line 70
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    const-string v1, "featurePointer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->cr_securesession_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 71
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->cr_securesession_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    :cond_2
    return-void
.end method

.method public final setFeaturePointer(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iput-object p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    return-void
.end method
