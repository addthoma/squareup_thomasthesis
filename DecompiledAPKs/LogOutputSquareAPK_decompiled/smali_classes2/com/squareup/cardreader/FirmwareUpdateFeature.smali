.class public interface abstract Lcom/squareup/cardreader/FirmwareUpdateFeature;
.super Ljava/lang/Object;
.source "FirmwareUpdateFeature.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u001b\u0010\u0006\u001a\u00020\u00032\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H&\u00a2\u0006\u0002\u0010\nJ \u0010\u000b\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H&J\u0010\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0011H&J\u0010\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u0011H&\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/cardreader/FirmwareUpdateFeature;",
        "",
        "onFirmwareUpdateTmsCountryChanges",
        "",
        "countryCode",
        "",
        "onVersionInfo",
        "firmwareVersions",
        "",
        "Lcom/squareup/cardreader/FirmwareAssetVersionInfo;",
        "([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V",
        "receivedManifest",
        "manifest",
        "",
        "requiredUpdate",
        "",
        "result",
        "",
        "updateComplete",
        "updateProgress",
        "percentComplete",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract onFirmwareUpdateTmsCountryChanges(Ljava/lang/String;)V
.end method

.method public abstract onVersionInfo([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V
.end method

.method public abstract receivedManifest([BZI)V
.end method

.method public abstract updateComplete(I)V
.end method

.method public abstract updateProgress(I)V
.end method
