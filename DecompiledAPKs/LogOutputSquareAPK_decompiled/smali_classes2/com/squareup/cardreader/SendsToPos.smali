.class public interface abstract Lcom/squareup/cardreader/SendsToPos;
.super Ljava/lang/Object;
.source "SendsToPos.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<FeatureOutput:",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008f\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0003J\u0015\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/cardreader/SendsToPos;",
        "FeatureOutput",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;",
        "",
        "sendResponseToPos",
        "",
        "message",
        "(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TFeatureOutput;)V"
        }
    .end annotation
.end method
