.class public Lcom/squareup/cardreader/RealCardReaderListeners;
.super Ljava/lang/Object;
.source "RealCardReaderListeners.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderListeners;


# instance fields
.field blePairingListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/cardreader/BlePairingListener;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderDataEventRelay:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/dipper/events/CardReaderDataEvent;",
            ">;"
        }
    .end annotation
.end field

.field cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

.field private final dipperEventRelay:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/dipper/events/DipperEvent;",
            ">;"
        }
    .end annotation
.end field

.field emvListener:Lcom/squareup/cardreader/EmvListener;

.field firmwareUpdateListener:Lcom/squareup/cardreader/FirmwareUpdater$Listener;

.field private final isSecureTouchEnabledRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field libraryLoadErrorListener:Lcom/squareup/cardreader/LibraryLoadErrorListener;

.field magSwipeListener:Lcom/squareup/cardreader/MagSwipeListener;

.field nfcListener:Lcom/squareup/cardreader/NfcListener;

.field paymentCompletionListener:Lcom/squareup/cardreader/PaymentCompletionListener;

.field private final paymentFeatureMessages:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/cardreader/PaymentFeatureMessageWrapper;",
            ">;"
        }
    .end annotation
.end field

.field pinRequestListener:Lcom/squareup/cardreader/PinRequestListener;

.field readerEventLogger:Lcom/squareup/cardreader/ReaderEventLogger;

.field private final secureTouchEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final tmnEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/dipper/events/TmnEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->dipperEventRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 20
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->cardReaderDataEventRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 22
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->paymentFeatureMessages:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 24
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->tmnEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 26
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->secureTouchEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 32
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->isSecureTouchEnabledRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 45
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->blePairingListeners:Ljava/util/Set;

    .line 46
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->libraryLoadErrorListener:Lcom/squareup/cardreader/LibraryLoadErrorListener;

    .line 47
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    .line 48
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->firmwareUpdateListener:Lcom/squareup/cardreader/FirmwareUpdater$Listener;

    .line 49
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->magSwipeListener:Lcom/squareup/cardreader/MagSwipeListener;

    .line 50
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->emvListener:Lcom/squareup/cardreader/EmvListener;

    .line 51
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->nfcListener:Lcom/squareup/cardreader/NfcListener;

    .line 52
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->paymentCompletionListener:Lcom/squareup/cardreader/PaymentCompletionListener;

    .line 53
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->pinRequestListener:Lcom/squareup/cardreader/PinRequestListener;

    .line 54
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->readerEventLogger:Lcom/squareup/cardreader/ReaderEventLogger;

    return-void
.end method


# virtual methods
.method public addBlePairingListener(Lcom/squareup/cardreader/BlePairingListener;)V
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->blePairingListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public cardReaderDataEvents()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/dipper/events/CardReaderDataEvent;",
            ">;"
        }
    .end annotation

    .line 181
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->cardReaderDataEventRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public dipperEvents()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/dipper/events/DipperEvent;",
            ">;"
        }
    .end annotation

    .line 177
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->dipperEventRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public getBlePairingListeners()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/cardreader/BlePairingListener;",
            ">;"
        }
    .end annotation

    .line 157
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->blePairingListeners:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getCardReaderStatusListener()Lcom/squareup/cardreader/CardReaderStatusListener;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    return-object v0
.end method

.method public getEmvListener()Lcom/squareup/cardreader/EmvListener;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->emvListener:Lcom/squareup/cardreader/EmvListener;

    return-object v0
.end method

.method public getFirmwareUpdateListener()Lcom/squareup/cardreader/FirmwareUpdater$Listener;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->firmwareUpdateListener:Lcom/squareup/cardreader/FirmwareUpdater$Listener;

    return-object v0
.end method

.method public getLibraryLoadErrorListener()Lcom/squareup/cardreader/LibraryLoadErrorListener;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->libraryLoadErrorListener:Lcom/squareup/cardreader/LibraryLoadErrorListener;

    return-object v0
.end method

.method public getMagSwipeListener()Lcom/squareup/cardreader/MagSwipeListener;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->magSwipeListener:Lcom/squareup/cardreader/MagSwipeListener;

    return-object v0
.end method

.method public getNfcListener()Lcom/squareup/cardreader/NfcListener;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->nfcListener:Lcom/squareup/cardreader/NfcListener;

    return-object v0
.end method

.method public getPaymentCompletionListener()Lcom/squareup/cardreader/PaymentCompletionListener;
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->paymentCompletionListener:Lcom/squareup/cardreader/PaymentCompletionListener;

    return-object v0
.end method

.method public getPinRequestListener()Lcom/squareup/cardreader/PinRequestListener;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->pinRequestListener:Lcom/squareup/cardreader/PinRequestListener;

    return-object v0
.end method

.method public getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->readerEventLogger:Lcom/squareup/cardreader/ReaderEventLogger;

    return-object v0
.end method

.method public isSecureTouchEnabled()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 223
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->isSecureTouchEnabledRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public logPaymentFeatureEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->readerEventLogger:Lcom/squareup/cardreader/ReaderEventLogger;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ReaderEventLogger;->logPaymentFeatureEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V

    return-void
.end method

.method public paymentFeatureMessages()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/cardreader/PaymentFeatureMessageWrapper;",
            ">;"
        }
    .end annotation

    .line 227
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->paymentFeatureMessages:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->hide()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public publishCardReaderDataEvent(Lcom/squareup/dipper/events/CardReaderDataEvent;)V
    .locals 1

    .line 204
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 205
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->cardReaderDataEventRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public publishDipperEvent(Lcom/squareup/dipper/events/DipperEvent;)V
    .locals 1

    .line 199
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 200
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->dipperEventRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public publishPaymentFeatureMessage(Lcom/squareup/cardreader/PaymentFeatureMessageWrapper;)V
    .locals 1

    .line 243
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->paymentFeatureMessages:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public publishSecureTouchDisabled()V
    .locals 2

    .line 239
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->isSecureTouchEnabledRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public publishSecureTouchEnabled()V
    .locals 2

    .line 235
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->isSecureTouchEnabledRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public publishSecureTouchEvent(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->secureTouchEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public publishTmnEvent(Lcom/squareup/dipper/events/TmnEvent;)V
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->tmnEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public removeBlePairingListener(Lcom/squareup/cardreader/BlePairingListener;)V
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->blePairingListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public secureTouchEvents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
            ">;"
        }
    .end annotation

    .line 209
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->secureTouchEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public setCardReaderStatusListener(Lcom/squareup/cardreader/CardReaderStatusListener;)V
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    return-void
.end method

.method public setEmvListener(Lcom/squareup/cardreader/EmvListener;)V
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->emvListener:Lcom/squareup/cardreader/EmvListener;

    return-void
.end method

.method public setFirmwareUpdateListener(Lcom/squareup/cardreader/FirmwareUpdater$Listener;)V
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->firmwareUpdateListener:Lcom/squareup/cardreader/FirmwareUpdater$Listener;

    return-void
.end method

.method public setLibraryLoadErrorListener(Lcom/squareup/cardreader/LibraryLoadErrorListener;)V
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->libraryLoadErrorListener:Lcom/squareup/cardreader/LibraryLoadErrorListener;

    return-void
.end method

.method public setMagSwipeListener(Lcom/squareup/cardreader/MagSwipeListener;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->magSwipeListener:Lcom/squareup/cardreader/MagSwipeListener;

    return-void
.end method

.method public setNfcListener(Lcom/squareup/cardreader/NfcListener;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->nfcListener:Lcom/squareup/cardreader/NfcListener;

    return-void
.end method

.method public setPaymentCompletionListener(Lcom/squareup/cardreader/PaymentCompletionListener;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->paymentCompletionListener:Lcom/squareup/cardreader/PaymentCompletionListener;

    return-void
.end method

.method public setPinRequestListener(Lcom/squareup/cardreader/PinRequestListener;)V
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->pinRequestListener:Lcom/squareup/cardreader/PinRequestListener;

    return-void
.end method

.method public setReaderEventLogger(Lcom/squareup/cardreader/ReaderEventLogger;)V
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->readerEventLogger:Lcom/squareup/cardreader/ReaderEventLogger;

    return-void
.end method

.method public tmnEvents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent;",
            ">;"
        }
    .end annotation

    .line 186
    iget-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->tmnEventRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public unsetCardReaderStatusListener()V
    .locals 1

    .line 79
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    return-void
.end method

.method public unsetEmvListener()V
    .locals 1

    .line 91
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->emvListener:Lcom/squareup/cardreader/EmvListener;

    return-void
.end method

.method public unsetFirmwareUpdateListener()V
    .locals 1

    .line 140
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->firmwareUpdateListener:Lcom/squareup/cardreader/FirmwareUpdater$Listener;

    return-void
.end method

.method public unsetLibraryLoadErrorListener()V
    .locals 1

    .line 153
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->libraryLoadErrorListener:Lcom/squareup/cardreader/LibraryLoadErrorListener;

    return-void
.end method

.method public unsetMagSwipeListener()V
    .locals 1

    .line 66
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->magSwipeListener:Lcom/squareup/cardreader/MagSwipeListener;

    return-void
.end method

.method public unsetNfcListener()V
    .locals 1

    .line 103
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->nfcListener:Lcom/squareup/cardreader/NfcListener;

    return-void
.end method

.method public unsetPaymentCompletionListener()V
    .locals 1

    .line 116
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->paymentCompletionListener:Lcom/squareup/cardreader/PaymentCompletionListener;

    return-void
.end method

.method public unsetPinRequestListener()V
    .locals 1

    .line 128
    sget-object v0, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v0, p0, Lcom/squareup/cardreader/RealCardReaderListeners;->pinRequestListener:Lcom/squareup/cardreader/PinRequestListener;

    return-void
.end method
