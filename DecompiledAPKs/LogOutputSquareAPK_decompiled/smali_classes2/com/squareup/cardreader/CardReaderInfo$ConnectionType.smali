.class public final enum Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;
.super Ljava/lang/Enum;
.source "CardReaderInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConnectionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

.field public static final enum AUDIO:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

.field public static final enum BLE:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

.field public static final enum BLUETOOTH:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

.field public static final enum RPC:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

.field public static final enum UNKNOWN:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 742
    new-instance v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->UNKNOWN:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    new-instance v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    const/4 v2, 0x1

    const-string v3, "AUDIO"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->AUDIO:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    new-instance v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    const/4 v3, 0x2

    const-string v4, "BLE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->BLE:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    new-instance v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    const/4 v4, 0x3

    const-string v5, "BLUETOOTH"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->BLUETOOTH:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    new-instance v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    const/4 v5, 0x4

    const-string v6, "RPC"

    invoke-direct {v0, v6, v5}, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->RPC:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    .line 741
    sget-object v6, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->UNKNOWN:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->AUDIO:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->BLE:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->BLUETOOTH:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->RPC:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->$VALUES:[Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 741
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;
    .locals 1

    .line 741
    const-class v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;
    .locals 1

    .line 741
    sget-object v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->$VALUES:[Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    return-object v0
.end method
