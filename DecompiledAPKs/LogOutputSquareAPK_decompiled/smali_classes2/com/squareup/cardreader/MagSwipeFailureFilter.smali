.class public interface abstract Lcom/squareup/cardreader/MagSwipeFailureFilter;
.super Ljava/lang/Object;
.source "MagSwipeFailureFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/MagSwipeFailureFilter$NeverFilterMagSwipeFailures;,
        Lcom/squareup/cardreader/MagSwipeFailureFilter$NeverFilterMagSwipeFailuresModule;
    }
.end annotation


# virtual methods
.method public abstract setEnabled(Z)V
.end method

.method public abstract shouldFilter()Z
.end method
