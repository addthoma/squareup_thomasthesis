.class public final Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper_Factory;
.super Ljava/lang/Object;
.source "CapitalPlanScreenMapper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;)",
            "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Landroid/content/res/Resources;)Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Landroid/content/res/Resources;",
            ")",
            "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;-><init>(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Landroid/content/res/Resources;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {v0, v1, v2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper_Factory;->newInstance(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Landroid/content/res/Resources;)Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper_Factory;->get()Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;

    move-result-object v0

    return-object v0
.end method
