.class final Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCapitalPlanWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->render(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;

.field final synthetic this$0:Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;->this$0:Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;

    iput-object p2, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;->$props:Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 46
    invoke-virtual {p0}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 93
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;->this$0:Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;

    invoke-static {v0}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->access$getAnalytics$p(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;)Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logClickCapitalManagePlan()V

    .line 95
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;->$props:Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;

    invoke-virtual {v0}, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;->getPlanUrl()Ljava/lang/String;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;->$props:Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;

    invoke-virtual {v1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;->getId()Ljava/lang/String;

    move-result-object v1

    .line 98
    iget-object v2, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;->this$0:Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;

    invoke-static {v2}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->access$getMultipassOtkHelper$p(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;)Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;->authAndRedirectToUrl(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v2

    .line 100
    new-instance v3, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4$1;-><init>(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    .line 99
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
