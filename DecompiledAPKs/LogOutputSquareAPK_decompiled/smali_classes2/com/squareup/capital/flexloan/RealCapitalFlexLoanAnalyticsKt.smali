.class public final Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalyticsKt;
.super Ljava/lang/Object;
.source "RealCapitalFlexLoanAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0016\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000c\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\r\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000e\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000f\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0010\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0011\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0012\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0013\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0014\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0015\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0016\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "CLICK_CAPITAL_ACTIVE_OFFER_ROW",
        "",
        "CLICK_CAPITAL_ACTIVE_PLAN_ROW",
        "CLICK_CAPITAL_CHOOSE_OFFER",
        "CLICK_CAPITAL_CLOSED_PLAN_ROW",
        "CLICK_CAPITAL_MANAGE_PLAN",
        "CLICK_CAPITAL_PAST_DUE_PLAN_ROW",
        "CLICK_CAPITAL_PENDING_APPLICATION_ROW",
        "ES2_CATALOG_NAME",
        "VIEW_CAPITAL_ACTIVE_PLAN",
        "VIEW_CAPITAL_BALANCE_APPLET_ROW",
        "VIEW_CAPITAL_BALANCE_APPLET_ROW_ERROR",
        "VIEW_CAPITAL_CLOSED_PLAN",
        "VIEW_CAPITAL_FLEX_OFFER",
        "VIEW_CAPITAL_PAST_DUE_PLAN",
        "VIEW_CAPITAL_PENDING_APPLICATION",
        "VIEW_CAPITAL_PLAN_ERROR",
        "VIEW_CAPITAL_ROW_ACTIVE_OFFER",
        "VIEW_CAPITAL_ROW_ACTIVE_PLAN",
        "VIEW_CAPITAL_ROW_CLOSED_PLAN",
        "VIEW_CAPITAL_ROW_INELIGIBLE",
        "VIEW_CAPITAL_ROW_PENDING_APPLICATION",
        "VIEW_CAPITAL_SESSION_BRIDGE_ERROR",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CLICK_CAPITAL_ACTIVE_OFFER_ROW:Ljava/lang/String; = "Capital: Active Offer Row"

.field private static final CLICK_CAPITAL_ACTIVE_PLAN_ROW:Ljava/lang/String; = "Capital: Active Plan Row"

.field private static final CLICK_CAPITAL_CHOOSE_OFFER:Ljava/lang/String; = "Capital: Choose Offer"

.field private static final CLICK_CAPITAL_CLOSED_PLAN_ROW:Ljava/lang/String; = "Capital: Closed Plan Row"

.field private static final CLICK_CAPITAL_MANAGE_PLAN:Ljava/lang/String; = "Capital: Manage Plan"

.field private static final CLICK_CAPITAL_PAST_DUE_PLAN_ROW:Ljava/lang/String; = "Capital: Past Due Plan Row"

.field private static final CLICK_CAPITAL_PENDING_APPLICATION_ROW:Ljava/lang/String; = "Capital: Pending Application Row"

.field private static final ES2_CATALOG_NAME:Ljava/lang/String; = "capital_pos"

.field private static final VIEW_CAPITAL_ACTIVE_PLAN:Ljava/lang/String; = "Capital: View Active Plan"

.field private static final VIEW_CAPITAL_BALANCE_APPLET_ROW:Ljava/lang/String; = "Deposits: View Capital"

.field private static final VIEW_CAPITAL_BALANCE_APPLET_ROW_ERROR:Ljava/lang/String; = "Deposits: View Capital Error"

.field private static final VIEW_CAPITAL_CLOSED_PLAN:Ljava/lang/String; = "Capital: View Closed Plan"

.field private static final VIEW_CAPITAL_FLEX_OFFER:Ljava/lang/String; = "Capital: View Offer"

.field private static final VIEW_CAPITAL_PAST_DUE_PLAN:Ljava/lang/String; = "Capital: View Past Due Plan"

.field private static final VIEW_CAPITAL_PENDING_APPLICATION:Ljava/lang/String; = "Capital: View Pending Application"

.field private static final VIEW_CAPITAL_PLAN_ERROR:Ljava/lang/String; = "Capital: View Plan Error"

.field private static final VIEW_CAPITAL_ROW_ACTIVE_OFFER:Ljava/lang/String; = "Deposits: View Capital Offer Row"

.field private static final VIEW_CAPITAL_ROW_ACTIVE_PLAN:Ljava/lang/String; = "Deposits: View Capital Active Plan Row"

.field private static final VIEW_CAPITAL_ROW_CLOSED_PLAN:Ljava/lang/String; = "Deposits: View Capital Closed Plan Row"

.field private static final VIEW_CAPITAL_ROW_INELIGIBLE:Ljava/lang/String; = "Deposits: View Capital Ineligible Row"

.field private static final VIEW_CAPITAL_ROW_PENDING_APPLICATION:Ljava/lang/String; = "Deposits: View Capital Application Row"

.field private static final VIEW_CAPITAL_SESSION_BRIDGE_ERROR:Ljava/lang/String; = "Capital: View Session Bridge Error"
