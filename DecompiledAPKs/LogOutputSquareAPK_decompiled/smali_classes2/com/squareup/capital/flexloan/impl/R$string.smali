.class public final Lcom/squareup/capital/flexloan/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/capital/flexloan/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final capital_application_pending_body_message:I = 0x7f1202b6

.field public static final capital_application_pending_body_title:I = 0x7f1202b7

.field public static final capital_application_pending_hero_message:I = 0x7f1202b8

.field public static final capital_application_pending_hero_title:I = 0x7f1202b9

.field public static final capital_error_message:I = 0x7f1202ba

.field public static final capital_error_title:I = 0x7f1202bb

.field public static final capital_manage_plan:I = 0x7f1202bc

.field public static final capital_no_offer_message:I = 0x7f1202bd

.field public static final capital_no_offer_title:I = 0x7f1202be

.field public static final capital_offer_choose_amount_action:I = 0x7f1202bf

.field public static final capital_offer_loan_up_to:I = 0x7f1202c0

.field public static final capital_offer_screen_body_credit_score:I = 0x7f1202c1

.field public static final capital_offer_screen_body_funding:I = 0x7f1202c2

.field public static final capital_offer_screen_body_payback:I = 0x7f1202c3

.field public static final capital_offer_screen_loading:I = 0x7f1202c4

.field public static final capital_offer_screen_loan_offer:I = 0x7f1202c5

.field public static final capital_offer_screen_motivation_text:I = 0x7f1202c6

.field public static final capital_offer_screen_reason_easy_application:I = 0x7f1202c7

.field public static final capital_offer_screen_reason_fast_funding:I = 0x7f1202c8

.field public static final capital_offer_screen_reason_payback:I = 0x7f1202c9

.field public static final capital_plan_complete:I = 0x7f1202ca

.field public static final capital_plan_screen_min_payment:I = 0x7f1202cb

.field public static final capital_plan_screen_outstanding:I = 0x7f1202cc

.field public static final capital_plan_screen_past_due:I = 0x7f1202cd

.field public static final capital_plan_screen_total_paid:I = 0x7f1202ce

.field public static final capital_plan_screen_total_remaining:I = 0x7f1202cf

.field public static final square_capital:I = 0x7f12186f

.field public static final square_capital_flex_plan_due_date:I = 0x7f121870

.field public static final square_capital_flex_plan_overdue_since:I = 0x7f121871

.field public static final square_capital_flex_plan_percent_paid:I = 0x7f121872

.field public static final square_capital_flex_status_offer:I = 0x7f121876

.field public static final square_go_dashboard:I = 0x7f121893


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
