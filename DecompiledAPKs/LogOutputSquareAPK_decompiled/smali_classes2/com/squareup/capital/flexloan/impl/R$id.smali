.class public final Lcom/squareup/capital/flexloan/impl/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/capital/flexloan/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final capital_application_pending_body_message:I = 0x7f0a02a3

.field public static final capital_application_pending_body_title:I = 0x7f0a02a4

.field public static final capital_application_pending_hero_message:I = 0x7f0a02a5

.field public static final capital_application_pending_hero_title:I = 0x7f0a02a6

.field public static final capital_application_pending_icon:I = 0x7f0a02a7

.field public static final capital_error_message_view:I = 0x7f0a02a8

.field public static final capital_loading_bar:I = 0x7f0a02a9

.field public static final capital_no_offer_icon:I = 0x7f0a02aa

.field public static final capital_no_offer_message:I = 0x7f0a02ab

.field public static final capital_no_offer_title:I = 0x7f0a02ac

.field public static final capital_offer_choose_action:I = 0x7f0a02ad

.field public static final capital_offer_hero_amount:I = 0x7f0a02ae

.field public static final capital_offer_hero_icon:I = 0x7f0a02af

.field public static final capital_offer_hero_text:I = 0x7f0a02b0

.field public static final current_plan_value:I = 0x7f0a0520

.field public static final current_plan_value_image:I = 0x7f0a0521

.field public static final current_plan_value_text:I = 0x7f0a0522

.field public static final percentage_paid:I = 0x7f0a0c0e

.field public static final pie_chart:I = 0x7f0a0c2a

.field public static final pie_chart_complete_label:I = 0x7f0a0c2b

.field public static final plan_action_button:I = 0x7f0a0c30

.field public static final plan_amount_outstanding_image:I = 0x7f0a0c31

.field public static final plan_amount_outstanding_text:I = 0x7f0a0c32

.field public static final plan_amount_outstanding_value:I = 0x7f0a0c33

.field public static final plan_payment_amount_image:I = 0x7f0a0c34

.field public static final plan_payment_amount_section:I = 0x7f0a0c35

.field public static final plan_payment_amount_sub_text:I = 0x7f0a0c36

.field public static final plan_payment_amount_text:I = 0x7f0a0c37

.field public static final plan_payment_amount_value:I = 0x7f0a0c38


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
