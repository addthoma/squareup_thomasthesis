.class final Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCapitalOfferWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->render(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/capital/flexloan/offer/CapitalOfferScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;

.field final synthetic this$0:Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$3;->this$0:Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;

    iput-object p2, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$3;->$props:Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$3;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 49
    iget-object v0, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$3;->this$0:Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;

    invoke-static {v0}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->access$getAnalytics$p(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;)Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;->logClickCapitalChooseOffer()V

    .line 51
    iget-object v0, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$3;->$props:Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;

    invoke-virtual {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;->getCustomer()Lcom/squareup/protos/capital/external/business/models/Customer;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    .line 52
    iget-object v1, v0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->rfc3986_offer_selection_url:Ljava/lang/String;

    .line 54
    iget-object v2, p0, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$3;->this$0:Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;

    invoke-static {v2}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;->access$getMultipassOtkHelper$p(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;)Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;

    move-result-object v2

    const-string v3, "targetUrl"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;->authAndRedirectToUrl(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v2

    .line 55
    new-instance v3, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$3$1;

    invoke-direct {v3, p0, v1, v0}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$3$1;-><init>(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow$render$3;Ljava/lang/String;Lcom/squareup/protos/capital/external/business/models/PreviewOffer;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
