.class public abstract Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;
.super Ljava/lang/Object;
.source "RealCapitalFlexLoanWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$DoNothing;,
        Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$GoBack;,
        Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$RefreshData;,
        Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowApplicationPending;,
        Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowError;,
        Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowFlexOffer;,
        Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowNoOffer;,
        Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowPlan;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0008\u0008\t\n\u000b\u000c\r\u000e\u000fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0019\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u00a2\u0006\u0002\u0010\u0007\u0082\u0001\u0008\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;",
        "",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;",
        "DoNothing",
        "GoBack",
        "RefreshData",
        "ShowApplicationPending",
        "ShowError",
        "ShowFlexOffer",
        "ShowNoOffer",
        "ShowPlan",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$DoNothing;",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$GoBack;",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$RefreshData;",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowApplicationPending;",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowError;",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowFlexOffer;",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowNoOffer;",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowPlan;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 179
    invoke-direct {p0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 179
    invoke-virtual {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;",
            ">;)",
            "Lkotlin/Unit;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    sget-object v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$DoNothing;->INSTANCE:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$DoNothing;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 183
    :cond_0
    sget-object v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$GoBack;->INSTANCE:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$GoBack;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    goto/16 :goto_0

    .line 184
    :cond_1
    sget-object v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$RefreshData;->INSTANCE:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$RefreshData;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 185
    sget-object v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$FetchingFlexStatus;->INSTANCE:Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$FetchingFlexStatus;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 188
    :cond_2
    instance-of v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowApplicationPending;

    if-eqz v0, :cond_3

    .line 189
    new-instance v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$ApplicationPending;

    move-object v2, p0

    check-cast v2, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowApplicationPending;

    invoke-virtual {v2}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowApplicationPending;->getFinancingRequest()Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$FinancingRequest;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$ApplicationPending;-><init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$FinancingRequest;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 192
    :cond_3
    instance-of v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowError;

    if-eqz v0, :cond_4

    .line 193
    new-instance v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$CapitalError;

    move-object v2, p0

    check-cast v2, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowError;

    invoke-virtual {v2}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowError;->getFailure()Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Failure;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$CapitalError;-><init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Failure;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 196
    :cond_4
    instance-of v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowFlexOffer;

    if-eqz v0, :cond_5

    .line 197
    new-instance v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$OfferAvailable;

    move-object v2, p0

    check-cast v2, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowFlexOffer;

    invoke-virtual {v2}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowFlexOffer;->getOffer()Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$OfferAvailable;-><init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 200
    :cond_5
    instance-of v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowNoOffer;

    if-eqz v0, :cond_6

    .line 201
    sget-object v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$NoOffer;->INSTANCE:Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$NoOffer;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 204
    :cond_6
    instance-of v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowPlan;

    if-eqz v0, :cond_7

    .line 205
    new-instance v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$FlexPlanExists;

    move-object v2, p0

    check-cast v2, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowPlan;

    invoke-virtual {v2}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowPlan;->getPlanId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowPlan;->getPlanType()Lcom/squareup/capital/flexloan/CapitalFlexPlanType;

    move-result-object v4

    invoke-virtual {v2}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowPlan;->getPlanUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v3, v4, v2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$FlexPlanExists;-><init>(Ljava/lang/String;Lcom/squareup/capital/flexloan/CapitalFlexPlanType;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    :goto_0
    return-object v1

    .line 206
    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
