.class final Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore$fetchFlexPlan$1;
.super Ljava/lang/Object;
.source "RealCapitalFlexLoanRequestDataStore.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->fetchFlexPlan(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;


# direct methods
.method constructor <init>(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore$fetchFlexPlan$1;->this$0:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;",
            ">;)",
            "Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore$fetchFlexPlan$1;->this$0:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;

    invoke-static {v0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->access$handlePlanRequestSuccess(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;)Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;

    move-result-object p1

    goto :goto_0

    .line 43
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore$fetchFlexPlan$1;->this$0:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {v0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->access$handleRetrievePlanRequestFailure(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore$fetchFlexPlan$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;

    move-result-object p1

    return-object p1
.end method
