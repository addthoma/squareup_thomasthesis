.class public interface abstract Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;
.super Ljava/lang/Object;
.source "CapitalFlexLoanWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner$ParentComponent;,
        Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowRunner<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008f\u0018\u0000 \u00042\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0004\u0005J\u0008\u0010\u0003\u001a\u00020\u0002H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "",
        "startWorkflow",
        "Companion",
        "ParentComponent",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner$Companion;

    sput-object v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;->Companion:Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner$Companion;

    return-void
.end method


# virtual methods
.method public abstract startWorkflow()V
.end method
