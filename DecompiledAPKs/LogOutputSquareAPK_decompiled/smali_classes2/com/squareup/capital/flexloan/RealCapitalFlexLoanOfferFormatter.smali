.class public final Lcom/squareup/capital/flexloan/RealCapitalFlexLoanOfferFormatter;
.super Ljava/lang/Object;
.source "RealCapitalFlexLoanOfferFormatter.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u000e\u0008\u0001\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanOfferFormatter;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "res",
        "Landroid/content/res/Resources;",
        "(Lcom/squareup/text/Formatter;Landroid/content/res/Resources;)V",
        "offerLimit",
        "",
        "offer",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Landroid/content/res/Resources;)V
    .locals 1
    .param p1    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/Shorter;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanOfferFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanOfferFormatter;->res:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public offerLimit(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;)Ljava/lang/String;
    .locals 2

    const-string v0, "offer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanOfferFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;->getCustomer()Lcom/squareup/protos/capital/external/business/models/Customer;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->max_financed_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 19
    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanOfferFormatter;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/capital/flexloan/impl/R$string;->square_capital_flex_status_offer:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "loan_amount_available"

    .line 20
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 21
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 22
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
