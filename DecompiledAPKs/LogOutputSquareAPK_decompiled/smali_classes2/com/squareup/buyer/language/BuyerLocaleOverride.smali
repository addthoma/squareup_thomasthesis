.class public interface abstract Lcom/squareup/buyer/language/BuyerLocaleOverride;
.super Ljava/lang/Object;
.source "BuyerLocaleOverride.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012H\'J\u0008\u0010\u0014\u001a\u00020\u0015H&J\u0010\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0003H&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0018\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0012\u0010\u000b\u001a\u00020\u000cX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u0012\u0010\u000f\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0005\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "Lmortar/Scoped;",
        "buyerLocale",
        "Ljava/util/Locale;",
        "getBuyerLocale",
        "()Ljava/util/Locale;",
        "buyerMoneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "getBuyerMoneyFormatter",
        "()Lcom/squareup/text/Formatter;",
        "buyerRes",
        "Lcom/squareup/util/Res;",
        "getBuyerRes",
        "()Lcom/squareup/util/Res;",
        "deviceLocale",
        "getDeviceLocale",
        "localeOverrideFactory",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "reset",
        "",
        "updateLocale",
        "locale",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getBuyerLocale()Ljava/util/Locale;
.end method

.method public abstract getBuyerMoneyFormatter()Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getBuyerRes()Lcom/squareup/util/Res;
.end method

.method public abstract getDeviceLocale()Ljava/util/Locale;
.end method

.method public abstract localeOverrideFactory()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ">;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation
.end method

.method public abstract reset()V
.end method

.method public abstract updateLocale(Ljava/util/Locale;)V
.end method
