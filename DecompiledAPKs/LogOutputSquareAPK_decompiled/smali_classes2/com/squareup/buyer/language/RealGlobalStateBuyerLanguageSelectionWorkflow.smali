.class public final Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealGlobalStateBuyerLanguageSelectionWorkflow.kt"

# interfaces
.implements Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/buyer/language/Exit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\u0018\u00002\u00020\u000126\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0002B!\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0008\u0008\u0003\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010JB\u0010\u0011\u001a<\u0012(\u0012&\u0012\u0004\u0012\u00020\u0003\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u00050\u0013\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0012j\u0002`\u0014H\u0016JK\u0010\u0015\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0016\u001a\u00020\u00032\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00040\u0018H\u0016\u00a2\u0006\u0002\u0010\u001aR\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;",
        "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "",
        "Lcom/squareup/buyer/language/Exit;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "buyerLanguageSelectionWorkflow",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflow;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "(Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflow;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lkotlinx/coroutines/CoroutineDispatcher;)V",
        "asLegacyLauncher",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionLegacyLauncher;",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerLanguageSelectionWorkflow:Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflow;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;


# direct methods
.method public constructor <init>(Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflow;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lkotlinx/coroutines/CoroutineDispatcher;)V
    .locals 1
    .param p3    # Lkotlinx/coroutines/CoroutineDispatcher;
        .annotation runtime Lcom/squareup/thread/Main$Immediate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "buyerLanguageSelectionWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLocaleOverride"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;->buyerLanguageSelectionWorkflow:Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflow;

    iput-object p2, p0, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p3, p0, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflow;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 22
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getUnconfined()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p3

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;-><init>(Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflow;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lkotlinx/coroutines/CoroutineDispatcher;)V

    return-void
.end method

.method public static final synthetic access$getBuyerLocaleOverride$p(Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;)Lcom/squareup/buyer/language/BuyerLocaleOverride;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-object p0
.end method


# virtual methods
.method public asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lkotlin/Unit;",
            "Lcom/squareup/buyer/language/Exit;",
            ">;"
        }
    .end annotation

    .line 47
    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    iget-object v1, p0, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacyintegration/LegacyLauncherKt;->createLegacyLauncher(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object p1, p0, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;->buyerLanguageSelectionWorkflow:Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 35
    new-instance v2, Lcom/squareup/buyer/language/BuyerLanguageSelectionProps;

    iget-object p1, p0, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {p1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getDeviceLocale()Ljava/util/Locale;

    move-result-object p1

    invoke-direct {v2, p1}, Lcom/squareup/buyer/language/BuyerLanguageSelectionProps;-><init>(Ljava/util/Locale;)V

    .line 36
    new-instance p1, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow$render$1;-><init>(Lcom/squareup/buyer/language/RealGlobalStateBuyerLanguageSelectionWorkflow;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p2

    .line 33
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 42
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    const/4 p2, 0x0

    .line 43
    invoke-static {p1, p2}, Lcom/squareup/workflow/LayeredScreenKt;->withPersistence(Ljava/util/Map;Z)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
