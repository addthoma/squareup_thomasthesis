.class public interface abstract Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;
.super Ljava/lang/Object;
.source "GlobalStateBuyerLanguageSelectionWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/Workflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/Workflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/buyer/language/Exit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use [BuyerLanguageSelectionWorkflow] instead."
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008g\u0018\u0000 \r26\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001:\u0001\rJB\u0010\t\u001a<\u0012(\u0012&\u0012\u0004\u0012\u00020\u0002\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u00040\u000b\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\nj\u0002`\u000cH\'\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
        "Lcom/squareup/workflow/Workflow;",
        "",
        "Lcom/squareup/buyer/language/Exit;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "asLegacyLauncher",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionLegacyLauncher;",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;->$$INSTANCE:Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;

    sput-object v0, Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;->Companion:Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;

    return-void
.end method


# virtual methods
.method public abstract asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lkotlin/Unit;",
            "Lcom/squareup/buyer/language/Exit;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Upgrade consumers to com.squareup.workflow.Workflow"
    .end annotation
.end method
