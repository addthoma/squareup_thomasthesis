.class public final Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener;
.super Ljava/lang/Object;
.source "AppsFlyerDeepLinkListener.kt"

# interfaces
.implements Lcom/appsflyer/AppsFlyerConversionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener$UriHelper;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0001\u000eB\u0017\u0008\u0007\u0012\u000e\u0008\u0001\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u001c\u0010\u0006\u001a\u00020\u00072\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\tH\u0016J\u0010\u0010\n\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u0004H\u0016J\u001c\u0010\u000c\u001a\u00020\u00072\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\tH\u0016J\u0010\u0010\r\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u0004H\u0016R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener;",
        "Lcom/appsflyer/AppsFlyerConversionListener;",
        "deferredDeepLinkPreference",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "(Lcom/f2prateek/rx/preferences2/Preference;)V",
        "onAppOpenAttribution",
        "",
        "conversionData",
        "",
        "onAttributionFailure",
        "errorMessage",
        "onInstallConversionDataLoaded",
        "onInstallConversionFailure",
        "UriHelper",
        "ad-analytics_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final deferredDeepLinkPreference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/f2prateek/rx/preferences2/Preference;)V
    .locals 1
    .param p1    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/ui/main/PosIntentParserModule$DeferredDeepLinkPreference;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "deferredDeepLinkPreference"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener;->deferredDeepLinkPreference:Lcom/f2prateek/rx/preferences2/Preference;

    return-void
.end method


# virtual methods
.method public onAppOpenAttribution(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "conversionData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    new-instance v0, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener$UriHelper;

    invoke-direct {v0, p1}, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener$UriHelper;-><init>(Ljava/util/Map;)V

    .line 15
    invoke-virtual {v0}, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener$UriHelper;->isValid()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 16
    iget-object p1, p0, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener;->deferredDeepLinkPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-virtual {v0}, Lcom/squareup/adanalytics/AppsFlyerDeepLinkListener$UriHelper;->getUriString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onAttributionFailure(Ljava/lang/String;)V
    .locals 1

    const-string v0, "errorMessage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onInstallConversionDataLoaded(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "conversionData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onInstallConversionFailure(Ljava/lang/String;)V
    .locals 1

    const-string v0, "errorMessage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
