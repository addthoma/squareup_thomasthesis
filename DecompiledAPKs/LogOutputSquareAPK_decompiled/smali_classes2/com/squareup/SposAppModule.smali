.class public abstract Lcom/squareup/SposAppModule;
.super Ljava/lang/Object;
.source "SposAppModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/payment/AccountStatusStoreAndForwardModule;,
        Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheetModule;,
        Lcom/squareup/bugreport/DefaultVeryLargeTelescopeLayoutConfigModule;,
        Lcom/squareup/checkoutflow/emoney/wiring/EmoneyPaymentProcessingScreenModule;,
        Lcom/squareup/cardreader/ExternalCardReaderDiscoveryModule;,
        Lcom/squareup/googlepay/client/GooglePayClientModule;,
        Lcom/squareup/loggedout/LoggedOutFeatureModule$LoggedOutStarterModule;,
        Lcom/squareup/cardreader/MagSwipeFailureFilter$NeverFilterMagSwipeFailuresModule;,
        Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule;,
        Lcom/squareup/ui/loggedout/LandingCanceledListener$NoLandingCanceledListenerModule;,
        Lcom/squareup/securetouch/NoSecureTouchFeatureModule;,
        Lcom/squareup/singlesignon/NoSingleSignOnModule;,
        Lcom/squareup/x2/NoSquareDeviceRootModule;,
        Lcom/squareup/statusbar/event/NoStatusBarAppModule;,
        Lcom/squareup/statusbar/workers/NoStatusBarWorkerModule;,
        Lcom/squareup/securetouch/NoTouchReportingModule;,
        Lcom/squareup/onlinestore/analytics/OnlineStoreAnalyticsModule;,
        Lcom/squareup/safetynetrecaptcha/impl/wiring/SafetyNetRecaptchaModule;,
        Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationControllerModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideDipperUiErrorTypeSelector()Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 78
    sget-object v0, Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$UseDefaultDisplayType;->INSTANCE:Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$UseDefaultDisplayType;

    return-object v0
.end method

.method static provideFeatureFlagFeatures(Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;)Lcom/squareup/settings/server/FeatureFlagFeatures;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 91
    sget-object v0, Lcom/squareup/settings/server/FeatureServiceVertical$SPos;->INSTANCE:Lcom/squareup/settings/server/FeatureServiceVertical$SPos;

    invoke-virtual {p0, v0}, Lcom/squareup/settings/server/FeatureFlagFeatures$Factory;->create(Lcom/squareup/settings/server/FeatureServiceVertical;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object p0

    return-object p0
.end method

.method static provideOpenTicketsAsSavedCarts()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const/4 v0, 0x0

    .line 95
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method abstract bindViewCustomerConfiguration(Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration;)Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideLandingScreenSelector(Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;)Lcom/squareup/ui/loggedout/LandingScreenSelector;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideLearnMoreTourPager(Lcom/squareup/pos/loggedout/PosLearnMoreTourPager;)Lcom/squareup/ui/tour/LearnMoreTourPager;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideOnboardingCountriesProvider(Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;)Lcom/squareup/loggedout/SupportedCountriesProvider;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideServicesCustomization(Lcom/squareup/appointmentsapi/NoServicesCustomization;)Lcom/squareup/appointmentsapi/ServicesCustomization;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
