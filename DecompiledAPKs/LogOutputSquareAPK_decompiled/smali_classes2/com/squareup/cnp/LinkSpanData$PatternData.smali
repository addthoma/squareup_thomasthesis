.class public final Lcom/squareup/cnp/LinkSpanData$PatternData;
.super Ljava/lang/Object;
.source "LinkSpanData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cnp/LinkSpanData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PatternData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u00002\u00020\u0001B+\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J1\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\n\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/cnp/LinkSpanData$PatternData;",
        "",
        "patternId",
        "",
        "key",
        "",
        "urlId",
        "clickableTextId",
        "(ILjava/lang/String;II)V",
        "getClickableTextId",
        "()I",
        "getKey",
        "()Ljava/lang/String;",
        "getPatternId",
        "getUrlId",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clickableTextId:I

.field private final key:Ljava/lang/String;

.field private final patternId:I

.field private final urlId:I


# direct methods
.method public constructor <init>(ILjava/lang/String;II)V
    .locals 1

    const-string v0, "key"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->patternId:I

    iput-object p2, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->key:Ljava/lang/String;

    iput p3, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->urlId:I

    iput p4, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->clickableTextId:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cnp/LinkSpanData$PatternData;ILjava/lang/String;IIILjava/lang/Object;)Lcom/squareup/cnp/LinkSpanData$PatternData;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget p1, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->patternId:I

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->key:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->urlId:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget p4, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->clickableTextId:I

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/cnp/LinkSpanData$PatternData;->copy(ILjava/lang/String;II)Lcom/squareup/cnp/LinkSpanData$PatternData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->patternId:I

    return v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->key:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->urlId:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->clickableTextId:I

    return v0
.end method

.method public final copy(ILjava/lang/String;II)Lcom/squareup/cnp/LinkSpanData$PatternData;
    .locals 1

    const-string v0, "key"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cnp/LinkSpanData$PatternData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/cnp/LinkSpanData$PatternData;-><init>(ILjava/lang/String;II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cnp/LinkSpanData$PatternData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cnp/LinkSpanData$PatternData;

    iget v0, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->patternId:I

    iget v1, p1, Lcom/squareup/cnp/LinkSpanData$PatternData;->patternId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->key:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/cnp/LinkSpanData$PatternData;->key:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->urlId:I

    iget v1, p1, Lcom/squareup/cnp/LinkSpanData$PatternData;->urlId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->clickableTextId:I

    iget p1, p1, Lcom/squareup/cnp/LinkSpanData$PatternData;->clickableTextId:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getClickableTextId()I
    .locals 1

    .line 26
    iget v0, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->clickableTextId:I

    return v0
.end method

.method public final getKey()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->key:Ljava/lang/String;

    return-object v0
.end method

.method public final getPatternId()I
    .locals 1

    .line 23
    iget v0, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->patternId:I

    return v0
.end method

.method public final getUrlId()I
    .locals 1

    .line 25
    iget v0, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->urlId:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->patternId:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->key:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->urlId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->clickableTextId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PatternData(patternId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->patternId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", urlId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->urlId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", clickableTextId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cnp/LinkSpanData$PatternData;->clickableTextId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
