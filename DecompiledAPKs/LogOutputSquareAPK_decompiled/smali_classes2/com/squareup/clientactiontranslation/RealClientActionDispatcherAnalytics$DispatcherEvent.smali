.class abstract Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "RealClientActionDispatcherAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "DispatcherEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent$UrlNotHandled;,
        Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent$UrlHandled;,
        Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent$NoTranslatorFound;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u00020\u0001:\u0003\u000b\u000c\rB\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u00020\u00088\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u0082\u0001\u0003\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent;",
        "Lcom/squareup/analytics/event/v1/ActionEvent;",
        "eventName",
        "Lcom/squareup/analytics/RegisterActionName;",
        "clientAction",
        "Lcom/squareup/protos/client/ClientAction;",
        "(Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/protos/client/ClientAction;)V",
        "clientActionAsString",
        "",
        "getClientActionAsString",
        "()Ljava/lang/String;",
        "NoTranslatorFound",
        "UrlHandled",
        "UrlNotHandled",
        "Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent$UrlNotHandled;",
        "Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent$UrlHandled;",
        "Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent$NoTranslatorFound;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clientActionAsString:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "client_action"
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/protos/client/ClientAction;)V
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/analytics/EventNamedAction;

    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 44
    invoke-virtual {p2}, Lcom/squareup/protos/client/ClientAction;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "clientAction.toString()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent;->clientActionAsString:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/protos/client/ClientAction;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/protos/client/ClientAction;)V

    return-void
.end method


# virtual methods
.method public final getClientActionAsString()Ljava/lang/String;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/clientactiontranslation/RealClientActionDispatcherAnalytics$DispatcherEvent;->clientActionAsString:Ljava/lang/String;

    return-object v0
.end method
