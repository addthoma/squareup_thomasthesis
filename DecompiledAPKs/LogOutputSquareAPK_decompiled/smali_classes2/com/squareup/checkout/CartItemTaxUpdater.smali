.class public final Lcom/squareup/checkout/CartItemTaxUpdater;
.super Ljava/lang/Object;
.source "CartItemTaxUpdater.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;,
        Lcom/squareup/checkout/CartItemTaxUpdater$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0004\u0018\u0000 \u00032\u00020\u0001:\u0002\u0003\u0004B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/checkout/CartItemTaxUpdater;",
        "",
        "()V",
        "Companion",
        "IndexedCartItem",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkout/CartItemTaxUpdater$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkout/CartItemTaxUpdater$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkout/CartItemTaxUpdater$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkout/CartItemTaxUpdater;->Companion:Lcom/squareup/checkout/CartItemTaxUpdater$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final updateAppliedTaxes(Lcom/squareup/checkout/Cart$Builder;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/Cart$Builder;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/checkout/Tax;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/checkout/CartItemTaxUpdater;->Companion:Lcom/squareup/checkout/CartItemTaxUpdater$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/checkout/CartItemTaxUpdater$Companion;->updateAppliedTaxes(Lcom/squareup/checkout/Cart$Builder;Ljava/util/Map;)V

    return-void
.end method
