.class public Lcom/squareup/checkout/Tax$Builder;
.super Lcom/squareup/checkout/Adjustment$Builder;
.source "Tax.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/Tax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/checkout/Adjustment$Builder<",
        "Lcom/squareup/checkout/Tax$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field appliesToCustomAmounts:Z

.field cogsTypeId:Ljava/lang/String;

.field feeProto:Lcom/squareup/api/items/Fee;

.field type:Lcom/squareup/api/items/Fee$AdjustmentType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 159
    invoke-direct {p0}, Lcom/squareup/checkout/Adjustment$Builder;-><init>()V

    const/4 v0, 0x1

    .line 162
    iput-boolean v0, p0, Lcom/squareup/checkout/Tax$Builder;->appliesToCustomAmounts:Z

    return-void
.end method

.method public static from(Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;)Lcom/squareup/checkout/Tax$Builder;
    .locals 2

    .line 205
    new-instance v0, Lcom/squareup/checkout/Tax$Builder;

    invoke-direct {v0}, Lcom/squareup/checkout/Tax$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    .line 206
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->id(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Tax$Builder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->name:Ljava/lang/String;

    .line 207
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->name(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Tax$Builder;

    sget-object v1, Lcom/squareup/api/items/Fee$AdjustmentType;->TAX:Lcom/squareup/api/items/Fee$AdjustmentType;

    .line 208
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->type(Lcom/squareup/api/items/Fee$AdjustmentType;)Lcom/squareup/checkout/Tax$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_id:Ljava/lang/String;

    .line 209
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->typeId(Ljava/lang/String;)Lcom/squareup/checkout/Tax$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/api/items/Fee;->DEFAULT_INCLUSION_TYPE:Lcom/squareup/api/items/Fee$InclusionType;

    .line 210
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->inclusionType(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Tax$Builder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/squareup/api/items/Fee;->DEFAULT_CALCULATION_PHASE:Lcom/squareup/api/items/CalculationPhase;

    .line 213
    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Tax$Builder;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    sget-object v1, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    .line 216
    invoke-static {p0, v1}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/checkout/Tax$Builder;->percentage(Lcom/squareup/util/Percentage;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p0

    check-cast p0, Lcom/squareup/checkout/Tax$Builder;

    return-object p0
.end method

.method public static from(Lcom/squareup/protos/client/bills/FeeLineItem;)Lcom/squareup/checkout/Tax$Builder;
    .locals 4

    .line 183
    iget-object v0, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    .line 184
    new-instance v1, Lcom/squareup/checkout/Tax$Builder;

    invoke-direct {v1}, Lcom/squareup/checkout/Tax$Builder;-><init>()V

    iget-object v2, v0, Lcom/squareup/api/items/Fee;->id:Ljava/lang/String;

    .line 185
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/Tax$Builder;->id(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Tax$Builder;

    iget-object v2, v0, Lcom/squareup/api/items/Fee;->name:Ljava/lang/String;

    .line 186
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/Tax$Builder;->name(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Tax$Builder;

    iget-object v2, v0, Lcom/squareup/api/items/Fee;->adjustment_type:Lcom/squareup/api/items/Fee$AdjustmentType;

    sget-object v3, Lcom/squareup/api/items/Fee$AdjustmentType;->TAX:Lcom/squareup/api/items/Fee$AdjustmentType;

    .line 187
    invoke-static {v2, v3}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/items/Fee$AdjustmentType;

    invoke-virtual {v1, v2}, Lcom/squareup/checkout/Tax$Builder;->type(Lcom/squareup/api/items/Fee$AdjustmentType;)Lcom/squareup/checkout/Tax$Builder;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/api/items/Fee;->fee_type_id:Ljava/lang/String;

    .line 188
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/Tax$Builder;->typeId(Ljava/lang/String;)Lcom/squareup/checkout/Tax$Builder;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/api/items/Fee;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    .line 189
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/Tax$Builder;->inclusionType(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Tax$Builder;

    iget-object v2, v0, Lcom/squareup/api/items/Fee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 190
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/Tax$Builder;->phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Tax$Builder;

    iget-object v2, v0, Lcom/squareup/api/items/Fee;->percentage:Ljava/lang/String;

    sget-object v3, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    .line 191
    invoke-static {v2, v3}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/checkout/Tax$Builder;->percentage(Lcom/squareup/util/Percentage;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Tax$Builder;

    iget-object v2, v0, Lcom/squareup/api/items/Fee;->enabled:Ljava/lang/Boolean;

    sget-object v3, Lcom/squareup/api/items/Fee;->DEFAULT_ENABLED:Ljava/lang/Boolean;

    .line 192
    invoke-static {v2, v3}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/checkout/Tax$Builder;->enabled(Z)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Tax$Builder;

    iget-object v2, v0, Lcom/squareup/api/items/Fee;->applies_to_custom_amounts:Ljava/lang/Boolean;

    sget-object v3, Lcom/squareup/api/items/Fee;->DEFAULT_APPLIES_TO_CUSTOM_AMOUNTS:Ljava/lang/Boolean;

    .line 193
    invoke-static {v2, v3}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/checkout/Tax$Builder;->appliesToCustomAmounts(Z)Lcom/squareup/checkout/Tax$Builder;

    move-result-object v1

    .line 195
    invoke-virtual {v1, v0}, Lcom/squareup/checkout/Tax$Builder;->feeProto(Lcom/squareup/api/items/Fee;)Lcom/squareup/checkout/Tax$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 196
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->idPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Tax$Builder;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/FeeLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 197
    invoke-virtual {v0, p0}, Lcom/squareup/checkout/Tax$Builder;->tryParseCreateAt(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object p0

    check-cast p0, Lcom/squareup/checkout/Tax$Builder;

    return-object p0
.end method

.method public static from(Lcom/squareup/shared/catalog/models/CatalogTax;)Lcom/squareup/checkout/Tax$Builder;
    .locals 3

    .line 167
    new-instance v0, Lcom/squareup/checkout/Tax$Builder;

    invoke-direct {v0}, Lcom/squareup/checkout/Tax$Builder;-><init>()V

    .line 168
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->id(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Tax$Builder;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 169
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->createdAt(Ljava/util/Date;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Tax$Builder;

    .line 170
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->name(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Tax$Builder;

    .line 171
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->getAdjustmentType()Lcom/squareup/api/items/Fee$AdjustmentType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->type(Lcom/squareup/api/items/Fee$AdjustmentType;)Lcom/squareup/checkout/Tax$Builder;

    move-result-object v0

    .line 172
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->getTypeId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->typeId(Ljava/lang/String;)Lcom/squareup/checkout/Tax$Builder;

    move-result-object v0

    .line 173
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->getInclusionType()Lcom/squareup/api/items/Fee$InclusionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->inclusionType(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Tax$Builder;

    .line 174
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->getCalculationPhase()Lcom/squareup/api/items/CalculationPhase;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Tax$Builder;

    .line 175
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->getPercentage()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-static {v1, v2}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->percentage(Lcom/squareup/util/Percentage;)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Tax$Builder;

    .line 176
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->enabled(Z)Lcom/squareup/checkout/Adjustment$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Tax$Builder;

    .line 177
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->appliesForCustomTypes()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Tax$Builder;->appliesToCustomAmounts(Z)Lcom/squareup/checkout/Tax$Builder;

    move-result-object v0

    .line 178
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->object()Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/Fee;

    invoke-virtual {v0, p0}, Lcom/squareup/checkout/Tax$Builder;->feeProto(Lcom/squareup/api/items/Fee;)Lcom/squareup/checkout/Tax$Builder;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public appliesToCustomAmounts(Z)Lcom/squareup/checkout/Tax$Builder;
    .locals 0

    .line 235
    iput-boolean p1, p0, Lcom/squareup/checkout/Tax$Builder;->appliesToCustomAmounts:Z

    return-object p0
.end method

.method public build()Lcom/squareup/checkout/Tax;
    .locals 3

    .line 240
    iget-object v0, p0, Lcom/squareup/checkout/Tax$Builder;->percentage:Lcom/squareup/util/Percentage;

    const-string v1, "percentage"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 241
    iget-object v0, p0, Lcom/squareup/checkout/Tax$Builder;->id:Ljava/lang/String;

    const-string v1, "id"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 242
    iget-object v0, p0, Lcom/squareup/checkout/Tax$Builder;->type:Lcom/squareup/api/items/Fee$AdjustmentType;

    const-string v1, "type"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 243
    iget-object v0, p0, Lcom/squareup/checkout/Tax$Builder;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    const-string v1, "inclusionType"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 244
    iget-object v0, p0, Lcom/squareup/checkout/Tax$Builder;->phase:Lcom/squareup/api/items/CalculationPhase;

    const-string v1, "phase"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 247
    iget-object v0, p0, Lcom/squareup/checkout/Tax$Builder;->feeProto:Lcom/squareup/api/items/Fee;

    const/4 v1, 0x0

    if-nez v0, :cond_2

    .line 248
    new-instance v0, Lcom/squareup/api/items/Fee$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Fee$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/checkout/Tax$Builder;->id:Ljava/lang/String;

    .line 249
    invoke-virtual {v0, v2}, Lcom/squareup/api/items/Fee$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/checkout/Tax$Builder;->name:Ljava/lang/String;

    .line 250
    invoke-virtual {v0, v2}, Lcom/squareup/api/items/Fee$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/checkout/Tax$Builder;->type:Lcom/squareup/api/items/Fee$AdjustmentType;

    .line 251
    invoke-virtual {v0, v2}, Lcom/squareup/api/items/Fee$Builder;->adjustment_type(Lcom/squareup/api/items/Fee$AdjustmentType;)Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/checkout/Tax$Builder;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    .line 252
    invoke-virtual {v0, v2}, Lcom/squareup/api/items/Fee$Builder;->inclusion_type(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/squareup/checkout/Tax$Builder;->appliesToCustomAmounts:Z

    .line 253
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/api/items/Fee$Builder;->applies_to_custom_amounts(Ljava/lang/Boolean;)Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/checkout/Tax$Builder;->phase:Lcom/squareup/api/items/CalculationPhase;

    .line 254
    invoke-virtual {v0, v2}, Lcom/squareup/api/items/Fee$Builder;->calculation_phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/squareup/checkout/Tax$Builder;->enabled:Z

    .line 255
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/api/items/Fee$Builder;->enabled(Ljava/lang/Boolean;)Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/checkout/Tax$Builder;->cogsTypeId:Ljava/lang/String;

    .line 256
    invoke-virtual {v0, v2}, Lcom/squareup/api/items/Fee$Builder;->fee_type_id(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/checkout/Tax$Builder;->percentage:Lcom/squareup/util/Percentage;

    if-nez v2, :cond_0

    move-object v2, v1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/squareup/checkout/Tax$Builder;->percentage:Lcom/squareup/util/Percentage;

    .line 257
    invoke-virtual {v2}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v0, v2}, Lcom/squareup/api/items/Fee$Builder;->percentage(Ljava/lang/String;)Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/checkout/Tax$Builder;->amount:Lcom/squareup/protos/common/Money;

    if-nez v2, :cond_1

    move-object v2, v1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/squareup/checkout/Tax$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 258
    invoke-static {v2}, Lcom/squareup/shared/catalog/utils/Dineros;->toDinero(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object v2

    :goto_1
    invoke-virtual {v0, v2}, Lcom/squareup/api/items/Fee$Builder;->amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    .line 259
    invoke-virtual {v0}, Lcom/squareup/api/items/Fee$Builder;->build()Lcom/squareup/api/items/Fee;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/Tax$Builder;->feeProto:Lcom/squareup/api/items/Fee;

    .line 262
    :cond_2
    new-instance v0, Lcom/squareup/checkout/Tax;

    invoke-direct {v0, p0, v1}, Lcom/squareup/checkout/Tax;-><init>(Lcom/squareup/checkout/Tax$Builder;Lcom/squareup/checkout/Tax$1;)V

    return-object v0
.end method

.method public feeProto(Lcom/squareup/api/items/Fee;)Lcom/squareup/checkout/Tax$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/checkout/Tax$Builder;->feeProto:Lcom/squareup/api/items/Fee;

    return-object p0
.end method

.method public type(Lcom/squareup/api/items/Fee$AdjustmentType;)Lcom/squareup/checkout/Tax$Builder;
    .locals 1

    const-string v0, "type"

    .line 225
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/items/Fee$AdjustmentType;

    iput-object p1, p0, Lcom/squareup/checkout/Tax$Builder;->type:Lcom/squareup/api/items/Fee$AdjustmentType;

    return-object p0
.end method

.method public typeId(Ljava/lang/String;)Lcom/squareup/checkout/Tax$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/checkout/Tax$Builder;->cogsTypeId:Ljava/lang/String;

    return-object p0
.end method
