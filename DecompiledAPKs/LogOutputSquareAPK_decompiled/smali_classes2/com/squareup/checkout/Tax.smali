.class public Lcom/squareup/checkout/Tax;
.super Lcom/squareup/checkout/Adjustment;
.source "Tax.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/Tax$Builder;
    }
.end annotation


# instance fields
.field public final appliesToCustomAmounts:Z

.field public final cogsTypeId:Ljava/lang/String;

.field private final feeProto:Lcom/squareup/api/items/Fee;

.field public final type:Lcom/squareup/api/items/Fee$AdjustmentType;


# direct methods
.method private constructor <init>(Lcom/squareup/checkout/Tax$Builder;)V
    .locals 1

    .line 45
    invoke-direct {p0, p1}, Lcom/squareup/checkout/Adjustment;-><init>(Lcom/squareup/checkout/Adjustment$Builder;)V

    .line 47
    iget-object v0, p1, Lcom/squareup/checkout/Tax$Builder;->type:Lcom/squareup/api/items/Fee$AdjustmentType;

    iput-object v0, p0, Lcom/squareup/checkout/Tax;->type:Lcom/squareup/api/items/Fee$AdjustmentType;

    .line 48
    iget-object v0, p1, Lcom/squareup/checkout/Tax$Builder;->cogsTypeId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/checkout/Tax;->cogsTypeId:Ljava/lang/String;

    .line 49
    iget-boolean v0, p1, Lcom/squareup/checkout/Tax$Builder;->appliesToCustomAmounts:Z

    iput-boolean v0, p0, Lcom/squareup/checkout/Tax;->appliesToCustomAmounts:Z

    .line 50
    iget-object p1, p1, Lcom/squareup/checkout/Tax$Builder;->feeProto:Lcom/squareup/api/items/Fee;

    iput-object p1, p0, Lcom/squareup/checkout/Tax;->feeProto:Lcom/squareup/api/items/Fee;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/checkout/Tax$Builder;Lcom/squareup/checkout/Tax$1;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/checkout/Tax;-><init>(Lcom/squareup/checkout/Tax$Builder;)V

    return-void
.end method

.method public static mapCatalogTaxes(Ljava/util/Collection;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 59
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 60
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogTax;

    .line 61
    invoke-static {v1}, Lcom/squareup/checkout/Tax$Builder;->from(Lcom/squareup/shared/catalog/models/CatalogTax;)Lcom/squareup/checkout/Tax$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/checkout/Tax$Builder;->build()Lcom/squareup/checkout/Tax;

    move-result-object v1

    .line 62
    iget-object v2, v1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public amountAppliesPerItemQuantity()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public asFeeProto()Lcom/squareup/api/items/Fee;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/checkout/Tax;->feeProto:Lcom/squareup/api/items/Fee;

    return-object v0
.end method

.method public asRequestAdjustment(Lcom/squareup/protos/common/Money;Ljava/util/List;)Lcom/squareup/server/payment/value/Adjustment;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/ItemizedAdjustment;",
            ">;)",
            "Lcom/squareup/server/payment/value/Adjustment;"
        }
    .end annotation

    .line 122
    iget-object p2, p0, Lcom/squareup/checkout/Tax;->type:Lcom/squareup/api/items/Fee$AdjustmentType;

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lcom/squareup/api/items/Fee$AdjustmentType;->name()Ljava/lang/String;

    move-result-object p2

    :goto_0
    if-eqz p2, :cond_1

    .line 123
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    :cond_1
    move-object v2, p2

    .line 125
    new-instance p2, Lcom/squareup/server/payment/value/Adjustment;

    iget-object v1, p0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/checkout/Tax;->cogsTypeId:Ljava/lang/String;

    const-string v0, "collected"

    .line 126
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    move-object v4, p1

    check-cast v4, Lcom/squareup/protos/common/Money;

    iget-object p1, p0, Lcom/squareup/checkout/Tax;->phase:Lcom/squareup/api/items/CalculationPhase;

    if-nez p1, :cond_2

    const/4 p1, 0x0

    const/4 v5, 0x0

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/squareup/checkout/Tax;->phase:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {p1}, Lcom/squareup/api/items/CalculationPhase;->getValue()I

    move-result p1

    move v5, p1

    :goto_1
    iget-object v6, p0, Lcom/squareup/checkout/Tax;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    iget-object v7, p0, Lcom/squareup/checkout/Tax;->name:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/checkout/Tax;->percentage:Lcom/squareup/util/Percentage;

    iget-object v9, p0, Lcom/squareup/checkout/Tax;->amount:Lcom/squareup/protos/common/Money;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p2

    invoke-direct/range {v0 .. v11}, Lcom/squareup/server/payment/value/Adjustment;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ILcom/squareup/api/items/Fee$InclusionType;Ljava/lang/String;Lcom/squareup/util/Percentage;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/lang/String;)V

    return-object p2
.end method

.method public buildFeeLineItem(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/FeeLineItem;
    .locals 3

    .line 140
    new-instance v0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/checkout/Tax;->idPair:Lcom/squareup/protos/client/IdPair;

    .line 141
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->fee_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkout/Tax;->createdAt:Ljava/util/Date;

    .line 142
    invoke-static {v1}, Lcom/squareup/checkout/util/ISO8601Dates;->tryBuildISO8601Date(Ljava/util/Date;)Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/checkout/Tax;->feeProto:Lcom/squareup/api/items/Fee;

    .line 144
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;->fee(Lcom/squareup/api/items/Fee;)Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;

    move-result-object v1

    .line 145
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    move-result-object v1

    .line 143
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->write_only_backing_details(Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 147
    new-instance v1, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;-><init>()V

    .line 148
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;

    move-result-object p1

    .line 149
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->applied_to_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;

    move-result-object p1

    .line 150
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->after_application_total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;

    move-result-object p1

    .line 151
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    move-result-object p1

    .line 147
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    .line 153
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem;

    move-result-object p1

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 85
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/checkout/Adjustment;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 87
    :cond_1
    check-cast p1, Lcom/squareup/checkout/Tax;

    .line 89
    iget-object v1, p0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkout/Tax;->type:Lcom/squareup/api/items/Fee$AdjustmentType;

    iget-object v3, p1, Lcom/squareup/checkout/Tax;->type:Lcom/squareup/api/items/Fee$AdjustmentType;

    .line 90
    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkout/Tax;->cogsTypeId:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/checkout/Tax;->cogsTypeId:Ljava/lang/String;

    .line 91
    invoke-static {v1, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 95
    iget-object v1, p0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/checkout/Tax;->type:Lcom/squareup/api/items/Fee$AdjustmentType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/checkout/Tax;->cogsTypeId:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public subtotalType()Lcom/squareup/checkout/SubtotalType;
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/checkout/Tax;->type:Lcom/squareup/api/items/Fee$AdjustmentType;

    sget-object v1, Lcom/squareup/api/items/Fee$AdjustmentType;->TAX:Lcom/squareup/api/items/Fee$AdjustmentType;

    if-ne v0, v1, :cond_2

    .line 71
    iget-object v0, p0, Lcom/squareup/checkout/Tax;->feeProto:Lcom/squareup/api/items/Fee;

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, v0, Lcom/squareup/api/items/Fee;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    sget-object v1, Lcom/squareup/api/items/Fee$InclusionType;->INCLUSIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne v0, v1, :cond_0

    .line 73
    sget-object v0, Lcom/squareup/checkout/SubtotalType;->INCLUSIVE_TAX:Lcom/squareup/checkout/SubtotalType;

    return-object v0

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkout/Tax;->feeProto:Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    sget-object v1, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne v0, v1, :cond_1

    .line 75
    sget-object v0, Lcom/squareup/checkout/SubtotalType;->TAX:Lcom/squareup/checkout/SubtotalType;

    return-object v0

    .line 78
    :cond_1
    sget-object v0, Lcom/squareup/checkout/SubtotalType;->TAX:Lcom/squareup/checkout/SubtotalType;

    return-object v0

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderTax{id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", name=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/checkout/Tax;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", percentage="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/checkout/Tax;->percentage:Lcom/squareup/util/Percentage;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", rate="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/checkout/Tax;->rate:Ljava/math/BigDecimal;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", amount="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/checkout/Tax;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", type="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/checkout/Tax;->type:Lcom/squareup/api/items/Fee$AdjustmentType;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", typeId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/checkout/Tax;->cogsTypeId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", inclusionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/Tax;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/Tax;->phase:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkout/Tax;->enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", appliesToCustomItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkout/Tax;->appliesToCustomAmounts:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
