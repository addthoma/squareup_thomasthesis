.class public final Lcom/squareup/checkout/ReturnCartItem;
.super Ljava/lang/Object;
.source "ReturnCartItem.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/ReturnCartItem$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReturnCartItem.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReturnCartItem.kt\ncom/squareup/checkout/ReturnCartItem\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,115:1\n1360#2:116\n1429#2,3:117\n1360#2:120\n1429#2,3:121\n*E\n*S KotlinDebug\n*F\n+ 1 ReturnCartItem.kt\ncom/squareup/checkout/ReturnCartItem\n*L\n45#1:116\n45#1,3:117\n64#1:120\n64#1,3:121\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eJ\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000eJ\u0006\u0010\u0012\u001a\u00020\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/checkout/ReturnCartItem;",
        "",
        "cartItem",
        "Lcom/squareup/checkout/CartItem;",
        "totalAmountCollected",
        "Lcom/squareup/protos/common/Money;",
        "returnItemization",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
        "(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)V",
        "getCartItem",
        "()Lcom/squareup/checkout/CartItem;",
        "getTotalAmountCollected",
        "()Lcom/squareup/protos/common/Money;",
        "getReturnDiscounts",
        "",
        "Lcom/squareup/checkout/ReturnDiscount;",
        "getReturnTaxes",
        "Lcom/squareup/checkout/ReturnTax;",
        "toReturnItemizationProto",
        "Companion",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkout/ReturnCartItem$Companion;


# instance fields
.field private final cartItem:Lcom/squareup/checkout/CartItem;

.field private final returnItemization:Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

.field private final totalAmountCollected:Lcom/squareup/protos/common/Money;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkout/ReturnCartItem$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkout/ReturnCartItem$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkout/ReturnCartItem;->Companion:Lcom/squareup/checkout/ReturnCartItem$Companion;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkout/ReturnCartItem;->cartItem:Lcom/squareup/checkout/CartItem;

    iput-object p2, p0, Lcom/squareup/checkout/ReturnCartItem;->totalAmountCollected:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/checkout/ReturnCartItem;->returnItemization:Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/checkout/ReturnCartItem;-><init>(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)V

    return-void
.end method


# virtual methods
.method public final getCartItem()Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCartItem;->cartItem:Lcom/squareup/checkout/CartItem;

    return-object v0
.end method

.method public final getReturnDiscounts()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnDiscount;",
            ">;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCartItem;->cartItem:Lcom/squareup/checkout/CartItem;

    .line 46
    iget-object v1, v0, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 116
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 117
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 118
    check-cast v3, Lcom/squareup/checkout/Discount;

    .line 49
    iget-object v4, v0, Lcom/squareup/checkout/CartItem;->itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/squareup/checkout/CartItem;->itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;

    iget-object v5, v3, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    goto :goto_1

    :cond_0
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_2

    .line 53
    new-instance v4, Lcom/squareup/checkout/ReturnDiscount;

    const-string v5, "discount"

    .line 54
    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/squareup/checkout/CartItem;->itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;

    iget-object v6, v3, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->longValue()J

    move-result-wide v5

    .line 53
    invoke-direct {v4, v3, v5, v6}, Lcom/squareup/checkout/ReturnDiscount;-><init>(Lcom/squareup/checkout/Discount;J)V

    .line 55
    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 47
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The applied amount for a return adjustment was missing from the amounts map."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 119
    :cond_3
    check-cast v2, Ljava/util/List;

    return-object v2
.end method

.method public final getReturnTaxes()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnTax;",
            ">;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCartItem;->cartItem:Lcom/squareup/checkout/CartItem;

    .line 65
    iget-object v1, v0, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 120
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 121
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 122
    check-cast v3, Lcom/squareup/checkout/Tax;

    .line 68
    iget-object v4, v0, Lcom/squareup/checkout/CartItem;->itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/squareup/checkout/CartItem;->itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;

    iget-object v5, v3, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    goto :goto_1

    :cond_0
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_2

    .line 72
    new-instance v4, Lcom/squareup/checkout/ReturnTax;

    const-string v5, "tax"

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/squareup/checkout/CartItem;->itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;

    iget-object v6, v3, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->longValue()J

    move-result-wide v5

    invoke-direct {v4, v3, v5, v6}, Lcom/squareup/checkout/ReturnTax;-><init>(Lcom/squareup/checkout/Tax;J)V

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 66
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The applied amount for a return adjustment was missing from the amounts map."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 123
    :cond_3
    check-cast v2, Ljava/util/List;

    return-object v2
.end method

.method public final getTotalAmountCollected()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCartItem;->totalAmountCollected:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final toReturnItemizationProto()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCartItem;->returnItemization:Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    return-object v0
.end method
