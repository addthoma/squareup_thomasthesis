.class public final Lcom/squareup/checkout/v2/launcher/RealCheckoutAppletWorkflowRunner;
.super Lcom/squareup/container/WorkflowV2Runner;
.source "RealCheckoutAppletWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/WorkflowV2Runner<",
        "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
        ">;",
        "Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u0008\u0012\u0004\u0012\u00020\u00030\u0002B\u001f\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u000eH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/checkout/v2/launcher/RealCheckoutAppletWorkflowRunner;",
        "Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner;",
        "Lcom/squareup/container/WorkflowV2Runner;",
        "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
        "viewFactory",
        "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowViewFactory;",
        "workflow",
        "Lcom/squareup/checkout/v2/CheckoutAppletWorkflow;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "(Lcom/squareup/checkout/v2/CheckoutAppletWorkflowViewFactory;Lcom/squareup/checkout/v2/CheckoutAppletWorkflow;Lcom/squareup/ui/main/PosContainer;)V",
        "getWorkflow",
        "()Lcom/squareup/checkout/v2/CheckoutAppletWorkflow;",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "startWorkflow",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final workflow:Lcom/squareup/checkout/v2/CheckoutAppletWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/checkout/v2/CheckoutAppletWorkflowViewFactory;Lcom/squareup/checkout/v2/CheckoutAppletWorkflow;Lcom/squareup/ui/main/PosContainer;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    sget-object v0, Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner;->Companion:Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p3}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/WorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/checkout/v2/launcher/RealCheckoutAppletWorkflowRunner;->workflow:Lcom/squareup/checkout/v2/CheckoutAppletWorkflow;

    iput-object p3, p0, Lcom/squareup/checkout/v2/launcher/RealCheckoutAppletWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/checkout/v2/CheckoutAppletWorkflow;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/checkout/v2/launcher/RealCheckoutAppletWorkflowRunner;->workflow:Lcom/squareup/checkout/v2/CheckoutAppletWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/checkout/v2/launcher/RealCheckoutAppletWorkflowRunner;->getWorkflow()Lcom/squareup/checkout/v2/CheckoutAppletWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-super {p0, p1}, Lcom/squareup/container/WorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 25
    invoke-virtual {p0}, Lcom/squareup/checkout/v2/launcher/RealCheckoutAppletWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/checkout/v2/launcher/RealCheckoutAppletWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/checkout/v2/launcher/RealCheckoutAppletWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/checkout/v2/launcher/RealCheckoutAppletWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public startWorkflow()V
    .locals 0

    .line 20
    invoke-virtual {p0}, Lcom/squareup/checkout/v2/launcher/RealCheckoutAppletWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
