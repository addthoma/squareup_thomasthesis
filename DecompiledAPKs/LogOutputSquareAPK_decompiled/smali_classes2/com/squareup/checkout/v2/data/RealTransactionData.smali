.class public final Lcom/squareup/checkout/v2/data/RealTransactionData;
.super Ljava/lang/Object;
.source "RealTransactionData.kt"

# interfaces
.implements Lcom/squareup/checkout/v2/data/transaction/TransactionData;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0008\u0010\u0012\u001a\u00020\u000fH\u0016R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00070\tX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/checkout/v2/data/RealTransactionData;",
        "Lcom/squareup/checkout/v2/data/transaction/TransactionData;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "(Lcom/squareup/payment/Transaction;)V",
        "_cartData",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/checkout/v2/data/transaction/model/CartData;",
        "cartData",
        "Lio/reactivex/Observable;",
        "getCartData",
        "()Lio/reactivex/Observable;",
        "cartDataMapper",
        "Lcom/squareup/checkout/v2/data/domain/CartDataMapper;",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final _cartData:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/checkout/v2/data/transaction/model/CartData;",
            ">;"
        }
    .end annotation
.end field

.field private final cartData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/checkout/v2/data/transaction/model/CartData;",
            ">;"
        }
    .end annotation
.end field

.field private final cartDataMapper:Lcom/squareup/checkout/v2/data/domain/CartDataMapper;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkout/v2/data/RealTransactionData;->transaction:Lcom/squareup/payment/Transaction;

    .line 25
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v0, "BehaviorRelay.create()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkout/v2/data/RealTransactionData;->_cartData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 26
    new-instance p1, Lcom/squareup/checkout/v2/data/domain/CartDataMapper;

    invoke-direct {p1}, Lcom/squareup/checkout/v2/data/domain/CartDataMapper;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkout/v2/data/RealTransactionData;->cartDataMapper:Lcom/squareup/checkout/v2/data/domain/CartDataMapper;

    .line 28
    iget-object p1, p0, Lcom/squareup/checkout/v2/data/RealTransactionData;->_cartData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->hide()Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "_cartData.hide()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkout/v2/data/RealTransactionData;->cartData:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getCartDataMapper$p(Lcom/squareup/checkout/v2/data/RealTransactionData;)Lcom/squareup/checkout/v2/data/domain/CartDataMapper;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/checkout/v2/data/RealTransactionData;->cartDataMapper:Lcom/squareup/checkout/v2/data/domain/CartDataMapper;

    return-object p0
.end method

.method public static final synthetic access$getTransaction$p(Lcom/squareup/checkout/v2/data/RealTransactionData;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/checkout/v2/data/RealTransactionData;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method public static final synthetic access$get_cartData$p(Lcom/squareup/checkout/v2/data/RealTransactionData;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/checkout/v2/data/RealTransactionData;->_cartData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method


# virtual methods
.method public getCartData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/checkout/v2/data/transaction/model/CartData;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/checkout/v2/data/RealTransactionData;->cartData:Lio/reactivex/Observable;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/checkout/v2/data/RealTransactionData;->_cartData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/checkout/v2/data/RealTransactionData;->cartDataMapper:Lcom/squareup/checkout/v2/data/domain/CartDataMapper;

    iget-object v2, p0, Lcom/squareup/checkout/v2/data/RealTransactionData;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v2

    const-string v3, "transaction.items"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/squareup/checkout/v2/data/domain/CartDataMapper;->map(Ljava/util/List;)Lcom/squareup/checkout/v2/data/transaction/model/CartData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/checkout/v2/data/RealTransactionData;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->cartChanges()Lrx/Observable;

    move-result-object v0

    const-string v1, "transaction.cartChanges()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v1, Lcom/squareup/checkout/v2/data/RealTransactionData$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/checkout/v2/data/RealTransactionData$onEnterScope$1;-><init>(Lcom/squareup/checkout/v2/data/RealTransactionData;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
