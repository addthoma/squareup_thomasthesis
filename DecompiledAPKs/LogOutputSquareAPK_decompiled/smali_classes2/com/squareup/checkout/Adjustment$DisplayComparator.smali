.class public Lcom/squareup/checkout/Adjustment$DisplayComparator;
.super Ljava/lang/Object;
.source "Adjustment.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/Adjustment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DisplayComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/checkout/Adjustment;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/squareup/checkout/Adjustment;Lcom/squareup/checkout/Adjustment;)I
    .locals 5

    .line 249
    invoke-static {}, Lcom/squareup/checkout/Adjustment;->access$000()Ljava/text/Collator;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/checkout/Adjustment;->name:Ljava/lang/String;

    iget-object v2, p2, Lcom/squareup/checkout/Adjustment;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    .line 255
    :cond_0
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment;->rate:Ljava/math/BigDecimal;

    iget-object v1, p2, Lcom/squareup/checkout/Adjustment;->rate:Ljava/math/BigDecimal;

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->eq(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x1

    if-nez v0, :cond_3

    .line 256
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment;->rate:Ljava/math/BigDecimal;

    if-nez v0, :cond_1

    return v2

    .line 257
    :cond_1
    iget-object v0, p2, Lcom/squareup/checkout/Adjustment;->rate:Ljava/math/BigDecimal;

    if-nez v0, :cond_2

    return v1

    .line 258
    :cond_2
    iget-object p2, p2, Lcom/squareup/checkout/Adjustment;->rate:Ljava/math/BigDecimal;

    iget-object p1, p1, Lcom/squareup/checkout/Adjustment;->rate:Ljava/math/BigDecimal;

    invoke-virtual {p2, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p1

    return p1

    .line 262
    :cond_3
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p2, Lcom/squareup/checkout/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 263
    iget-object v0, p1, Lcom/squareup/checkout/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_4

    return v2

    .line 264
    :cond_4
    iget-object v0, p2, Lcom/squareup/checkout/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_5

    return v1

    .line 265
    :cond_5
    iget-object p1, p1, Lcom/squareup/checkout/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object p1, p2, Lcom/squareup/checkout/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    cmp-long v0, v3, p1

    if-lez v0, :cond_6

    goto :goto_0

    :cond_6
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 269
    :cond_7
    iget-object p1, p1, Lcom/squareup/checkout/Adjustment;->id:Ljava/lang/String;

    iget-object p2, p2, Lcom/squareup/checkout/Adjustment;->id:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 245
    check-cast p1, Lcom/squareup/checkout/Adjustment;

    check-cast p2, Lcom/squareup/checkout/Adjustment;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkout/Adjustment$DisplayComparator;->compare(Lcom/squareup/checkout/Adjustment;Lcom/squareup/checkout/Adjustment;)I

    move-result p1

    return p1
.end method
