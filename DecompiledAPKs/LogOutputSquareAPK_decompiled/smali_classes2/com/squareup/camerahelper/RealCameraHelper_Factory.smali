.class public final Lcom/squareup/camerahelper/RealCameraHelper_Factory;
.super Ljava/lang/Object;
.source "RealCameraHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/camerahelper/RealCameraHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityResultHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final appProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/camerahelper/RealCameraHelper_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/camerahelper/RealCameraHelper_Factory;->appProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/camerahelper/RealCameraHelper_Factory;->activityResultHandlerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/camerahelper/RealCameraHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;)",
            "Lcom/squareup/camerahelper/RealCameraHelper_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/camerahelper/RealCameraHelper_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/camerahelper/RealCameraHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/log/OhSnapLogger;Landroid/app/Application;Lcom/squareup/ui/ActivityResultHandler;)Lcom/squareup/camerahelper/RealCameraHelper;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/camerahelper/RealCameraHelper;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/camerahelper/RealCameraHelper;-><init>(Lcom/squareup/log/OhSnapLogger;Landroid/app/Application;Lcom/squareup/ui/ActivityResultHandler;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/camerahelper/RealCameraHelper;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/camerahelper/RealCameraHelper_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/OhSnapLogger;

    iget-object v1, p0, Lcom/squareup/camerahelper/RealCameraHelper_Factory;->appProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/camerahelper/RealCameraHelper_Factory;->activityResultHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/ActivityResultHandler;

    invoke-static {v0, v1, v2}, Lcom/squareup/camerahelper/RealCameraHelper_Factory;->newInstance(Lcom/squareup/log/OhSnapLogger;Landroid/app/Application;Lcom/squareup/ui/ActivityResultHandler;)Lcom/squareup/camerahelper/RealCameraHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/camerahelper/RealCameraHelper_Factory;->get()Lcom/squareup/camerahelper/RealCameraHelper;

    move-result-object v0

    return-object v0
.end method
