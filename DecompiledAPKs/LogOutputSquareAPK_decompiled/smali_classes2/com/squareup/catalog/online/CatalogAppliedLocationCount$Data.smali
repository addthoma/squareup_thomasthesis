.class public final Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;
.super Lcom/squareup/catalog/online/CatalogAppliedLocationCount;
.source "CatalogAppliedLocationCount.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/catalog/online/CatalogAppliedLocationCount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\n\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u00032\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;",
        "Lcom/squareup/catalog/online/CatalogAppliedLocationCount;",
        "activeAtCurrentLocation",
        "",
        "appliedLocationCount",
        "",
        "(ZI)V",
        "getActiveAtCurrentLocation",
        "()Z",
        "getAppliedLocationCount",
        "()I",
        "component1",
        "component2",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final activeAtCurrentLocation:Z

.field private final appliedLocationCount:I


# direct methods
.method public constructor <init>(ZI)V
    .locals 1

    const/4 v0, 0x0

    .line 10
    invoke-direct {p0, v0}, Lcom/squareup/catalog/online/CatalogAppliedLocationCount;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->activeAtCurrentLocation:Z

    iput p2, p0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->appliedLocationCount:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;ZIILjava/lang/Object;)Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-boolean p1, p0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->activeAtCurrentLocation:Z

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->appliedLocationCount:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->copy(ZI)Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->activeAtCurrentLocation:Z

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->appliedLocationCount:I

    return v0
.end method

.method public final copy(ZI)Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;
    .locals 1

    new-instance v0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    invoke-direct {v0, p1, p2}, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;-><init>(ZI)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    iget-boolean v0, p0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->activeAtCurrentLocation:Z

    iget-boolean v1, p1, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->activeAtCurrentLocation:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->appliedLocationCount:I

    iget p1, p1, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->appliedLocationCount:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActiveAtCurrentLocation()Z
    .locals 1

    .line 8
    iget-boolean v0, p0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->activeAtCurrentLocation:Z

    return v0
.end method

.method public final getAppliedLocationCount()I
    .locals 1

    .line 9
    iget v0, p0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->appliedLocationCount:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-boolean v0, p0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->activeAtCurrentLocation:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->appliedLocationCount:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Data(activeAtCurrentLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->activeAtCurrentLocation:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", appliedLocationCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->appliedLocationCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
