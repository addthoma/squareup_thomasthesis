.class public interface abstract Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;
.super Ljava/lang/Object;
.source "CatalogAppliedLocationCountFetcher.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher$SharedScope;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u000fJ\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0018\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
        "Lmortar/Scoped;",
        "activeLocationCount",
        "",
        "getActiveLocationCount",
        "()I",
        "appliedLocationCount",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/catalog/online/CatalogAppliedLocationCount;",
        "getAppliedLocationCount",
        "()Lio/reactivex/Observable;",
        "fetchAppliedLocationCount",
        "",
        "objectCogsId",
        "",
        "SharedScope",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract fetchAppliedLocationCount(Ljava/lang/String;)V
.end method

.method public abstract getActiveLocationCount()I
.end method

.method public abstract getAppliedLocationCount()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCount;",
            ">;"
        }
    .end annotation
.end method
