.class public final synthetic Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

.field private final synthetic f$1:Lcom/squareup/shared/catalog/models/CatalogItem;

.field private final synthetic f$2:Lcom/squareup/catalog/EditItemVariationsState;

.field private final synthetic f$3:Z

.field private final synthetic f$4:Lcom/squareup/cogs/Cogs;

.field private final synthetic f$5:Ljava/lang/String;

.field private final synthetic f$6:Z


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/catalog/CatalogServiceEndpoint;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;->f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

    iput-object p2, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;->f$1:Lcom/squareup/shared/catalog/models/CatalogItem;

    iput-object p3, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;->f$2:Lcom/squareup/catalog/EditItemVariationsState;

    iput-boolean p4, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;->f$3:Z

    iput-object p5, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;->f$4:Lcom/squareup/cogs/Cogs;

    iput-object p6, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;->f$5:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;->f$6:Z

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 8

    iget-object v0, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;->f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

    iget-object v1, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;->f$1:Lcom/squareup/shared/catalog/models/CatalogItem;

    iget-object v2, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;->f$2:Lcom/squareup/catalog/EditItemVariationsState;

    iget-boolean v3, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;->f$3:Z

    iget-object v4, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;->f$4:Lcom/squareup/cogs/Cogs;

    iget-object v5, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;->f$5:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$W0AYMJ3cQ3_FmZmcXQDRyHuWxwA;->f$6:Z

    move-object v7, p1

    check-cast v7, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual/range {v0 .. v7}, Lcom/squareup/catalog/CatalogServiceEndpoint;->lambda$null$2$CatalogServiceEndpoint(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method
