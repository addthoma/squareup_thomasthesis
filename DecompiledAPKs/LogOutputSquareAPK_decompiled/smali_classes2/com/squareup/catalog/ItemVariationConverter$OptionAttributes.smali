.class Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;
.super Ljava/lang/Object;
.source "ItemVariationConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/catalog/ItemVariationConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OptionAttributes"
.end annotation


# instance fields
.field public final optionDefinition:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final optionValueDefinition:Lcom/squareup/catalog/CatalogAttributeDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/catalog/CatalogAttributeDefinition;Lcom/squareup/catalog/CatalogAttributeDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    iput-object p1, p0, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;->optionDefinition:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 181
    iput-object p2, p0, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;->optionValueDefinition:Lcom/squareup/catalog/CatalogAttributeDefinition;

    return-void
.end method

.method public static of(Lcom/squareup/catalog/CatalogAttributeDefinition;Lcom/squareup/catalog/CatalogAttributeDefinition;)Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;"
        }
    .end annotation

    .line 188
    new-instance v0, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;

    invoke-direct {v0, p0, p1}, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;-><init>(Lcom/squareup/catalog/CatalogAttributeDefinition;Lcom/squareup/catalog/CatalogAttributeDefinition;)V

    return-object v0
.end method
