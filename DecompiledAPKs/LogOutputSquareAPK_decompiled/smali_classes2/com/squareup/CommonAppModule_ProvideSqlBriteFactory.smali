.class public final Lcom/squareup/CommonAppModule_ProvideSqlBriteFactory;
.super Ljava/lang/Object;
.source "CommonAppModule_ProvideSqlBriteFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/CommonAppModule_ProvideSqlBriteFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/sqlbrite3/SqlBrite;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/CommonAppModule_ProvideSqlBriteFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideSqlBriteFactory$InstanceHolder;->access$000()Lcom/squareup/CommonAppModule_ProvideSqlBriteFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideSqlBrite()Lcom/squareup/sqlbrite3/SqlBrite;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/CommonAppModule;->provideSqlBrite()Lcom/squareup/sqlbrite3/SqlBrite;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sqlbrite3/SqlBrite;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/sqlbrite3/SqlBrite;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideSqlBriteFactory;->provideSqlBrite()Lcom/squareup/sqlbrite3/SqlBrite;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/CommonAppModule_ProvideSqlBriteFactory;->get()Lcom/squareup/sqlbrite3/SqlBrite;

    move-result-object v0

    return-object v0
.end method
