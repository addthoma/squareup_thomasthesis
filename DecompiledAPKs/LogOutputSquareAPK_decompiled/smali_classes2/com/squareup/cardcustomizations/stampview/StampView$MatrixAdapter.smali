.class public final Lcom/squareup/cardcustomizations/stampview/StampView$MatrixAdapter;
.super Ljava/lang/Object;
.source "StampView.kt"

# interfaces
.implements Lkotlinx/android/parcel/Parceler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardcustomizations/stampview/StampView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MatrixAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlinx/android/parcel/Parceler<",
        "Landroid/graphics/Matrix;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u00c6\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u001c\u0010\u0007\u001a\u00020\u0008*\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\nH\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/cardcustomizations/stampview/StampView$MatrixAdapter;",
        "Lkotlinx/android/parcel/Parceler;",
        "Landroid/graphics/Matrix;",
        "()V",
        "create",
        "parcel",
        "Landroid/os/Parcel;",
        "write",
        "",
        "flags",
        "",
        "card-customization_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/cardcustomizations/stampview/StampView$MatrixAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 392
    new-instance v0, Lcom/squareup/cardcustomizations/stampview/StampView$MatrixAdapter;

    invoke-direct {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$MatrixAdapter;-><init>()V

    sput-object v0, Lcom/squareup/cardcustomizations/stampview/StampView$MatrixAdapter;->INSTANCE:Lcom/squareup/cardcustomizations/stampview/StampView$MatrixAdapter;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/os/Parcel;)Landroid/graphics/Matrix;
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 395
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readFloatArray([F)V

    .line 396
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    .line 397
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->setValues([F)V

    return-object p1
.end method

.method public bridge synthetic create(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 392
    invoke-virtual {p0, p1}, Lcom/squareup/cardcustomizations/stampview/StampView$MatrixAdapter;->create(Landroid/os/Parcel;)Landroid/graphics/Matrix;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Landroid/graphics/Matrix;
    .locals 0

    .line 392
    invoke-static {p0, p1}, Lkotlinx/android/parcel/Parceler$DefaultImpls;->newArray(Lkotlinx/android/parcel/Parceler;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Landroid/graphics/Matrix;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 392
    invoke-virtual {p0, p1}, Lcom/squareup/cardcustomizations/stampview/StampView$MatrixAdapter;->newArray(I)[Landroid/graphics/Matrix;

    move-result-object p1

    return-object p1
.end method

.method public write(Landroid/graphics/Matrix;Landroid/os/Parcel;I)V
    .locals 0

    const-string p3, "receiver$0"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "parcel"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 p3, 0x9

    new-array p3, p3, [F

    .line 403
    invoke-virtual {p1, p3}, Landroid/graphics/Matrix;->getValues([F)V

    .line 404
    invoke-virtual {p2, p3}, Landroid/os/Parcel;->writeFloatArray([F)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Landroid/os/Parcel;I)V
    .locals 0

    .line 392
    check-cast p1, Landroid/graphics/Matrix;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/cardcustomizations/stampview/StampView$MatrixAdapter;->write(Landroid/graphics/Matrix;Landroid/os/Parcel;I)V

    return-void
.end method
