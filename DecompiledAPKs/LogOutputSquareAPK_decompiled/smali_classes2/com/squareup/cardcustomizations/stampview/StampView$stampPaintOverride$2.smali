.class final Lcom/squareup/cardcustomizations/stampview/StampView$stampPaintOverride$2;
.super Lkotlin/jvm/internal/Lambda;
.source "StampView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardcustomizations/stampview/StampView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Landroid/graphics/Paint;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Landroid/graphics/Paint;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardcustomizations/stampview/StampView;


# direct methods
.method constructor <init>(Lcom/squareup/cardcustomizations/stampview/StampView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$stampPaintOverride$2;->this$0:Lcom/squareup/cardcustomizations/stampview/StampView;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Landroid/graphics/Paint;
    .locals 2

    .line 68
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$stampPaintOverride$2;->this$0:Lcom/squareup/cardcustomizations/stampview/StampView;

    invoke-static {v1}, Lcom/squareup/cardcustomizations/stampview/StampView;->access$getStampPaint$p(Lcom/squareup/cardcustomizations/stampview/StampView;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView$stampPaintOverride$2;->invoke()Landroid/graphics/Paint;

    move-result-object v0

    return-object v0
.end method
