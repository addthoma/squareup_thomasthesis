.class final Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$2;
.super Ljava/lang/Object;
.source "StampView.kt"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardcustomizations/stampview/StampView;->animateToBounds(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/Rect;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/animation/ValueAnimator;",
        "kotlin.jvm.PlatformType",
        "onAnimationUpdate"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# instance fields
.field final synthetic $currentScale:F

.field final synthetic $magnitude:F

.field final synthetic $this_animateToBounds:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;


# direct methods
.method constructor <init>(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;FF)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$2;->$this_animateToBounds:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    iput p2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$2;->$currentScale:F

    iput p3, p0, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$2;->$magnitude:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .line 368
    iget v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$2;->$currentScale:F

    const/4 v1, 0x1

    int-to-float v1, v1

    const-string v2, "it"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    sub-float/2addr v1, v2

    mul-float v0, v0, v1

    iget v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$2;->$magnitude:F

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p1

    mul-float v1, v1, p1

    add-float/2addr v0, v1

    .line 369
    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$2;->$this_animateToBounds:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object p1

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v0, v0}, Landroid/graphics/PointF;-><init>(FF)V

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$2;->$this_animateToBounds:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->bounds()Landroid/graphics/RectF;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/cardcustomizations/geometry/RectKt;->centerPoint(Landroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lcom/squareup/cardcustomizations/geometry/MatrixKt;->updateScale(Landroid/graphics/Matrix;Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    return-void
.end method
