.class Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "SignatureView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardcustomizations/signature/SignatureView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;",
            ">;"
        }
    .end annotation
.end field

.field private static final gson:Lcom/google/gson/Gson;


# instance fields
.field private final signature:Lcom/squareup/cardcustomizations/signature/Signature;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 310
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    sput-object v0, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;->gson:Lcom/google/gson/Gson;

    .line 324
    new-instance v0, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState$1;

    invoke-direct {v0}, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState$1;-><init>()V

    sput-object v0, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 335
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 337
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    .line 338
    sget-object v0, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;->gson:Lcom/google/gson/Gson;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1, v1}, Lcom/squareup/cardcustomizations/signature/Signature;->decode(Lcom/google/gson/Gson;Ljava/lang/String;Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;Lcom/squareup/cardcustomizations/signature/Signature;)V
    .locals 0

    .line 315
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 316
    iput-object p2, p0, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    return-void
.end method

.method static synthetic access$000()Lcom/google/gson/Gson;
    .locals 1

    .line 305
    sget-object v0, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;->gson:Lcom/google/gson/Gson;

    return-object v0
.end method

.method static synthetic access$100(Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;)Lcom/squareup/cardcustomizations/signature/Signature;
    .locals 0

    .line 305
    iget-object p0, p0, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    return-object p0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 320
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 321
    iget-object p2, p0, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;->signature:Lcom/squareup/cardcustomizations/signature/Signature;

    sget-object v0, Lcom/squareup/cardcustomizations/signature/SignatureView$SavedState;->gson:Lcom/google/gson/Gson;

    invoke-virtual {p2, v0}, Lcom/squareup/cardcustomizations/signature/Signature;->encode(Lcom/google/gson/Gson;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
