.class public Lcom/squareup/cardcustomizations/signature/Spliner;
.super Ljava/lang/Object;
.source "Spliner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;
    }
.end annotation


# instance fields
.field private final beziers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;",
            ">;"
        }
    .end annotation
.end field

.field private final dirtyRect:Landroid/graphics/Rect;

.field private final points:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/signature/Point;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->points:Ljava/util/List;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->beziers:Ljava/util/List;

    .line 30
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->dirtyRect:Landroid/graphics/Rect;

    return-void
.end method

.method private addArtificialPoint()V
    .locals 7

    .line 126
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->points:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->points:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardcustomizations/signature/Point;

    .line 128
    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->points:Ljava/util/List;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardcustomizations/signature/Point;

    .line 130
    new-instance v3, Lcom/squareup/cardcustomizations/signature/Point;

    iget v4, v0, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    const/high16 v5, 0x40000000    # 2.0f

    mul-float v4, v4, v5

    iget v6, v2, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    sub-float/2addr v4, v6

    iget v0, v0, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    mul-float v0, v0, v5

    iget v2, v2, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    sub-float/2addr v0, v2

    invoke-direct {v3, v4, v0}, Lcom/squareup/cardcustomizations/signature/Point;-><init>(FF)V

    .line 131
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->points:Ljava/util/List;

    invoke-interface {v0, v1, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-void
.end method

.method private static computeBSpline([F)[F
    .locals 6

    .line 102
    array-length v0, p0

    .line 105
    filled-new-array {v0, v0}, [I

    move-result-object v1

    const-class v2, F

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[F

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    const/high16 v3, 0x3f800000    # 1.0f

    if-lez v2, :cond_0

    .line 107
    aget-object v4, v1, v2

    add-int/lit8 v5, v2, -0x1

    aput v3, v4, v5

    .line 108
    :cond_0
    aget-object v4, v1, v2

    const/high16 v5, 0x40800000    # 4.0f

    aput v5, v4, v2

    add-int/lit8 v4, v0, -0x1

    if-ge v2, v4, :cond_1

    .line 109
    aget-object v4, v1, v2

    add-int/lit8 v5, v2, 0x1

    aput v3, v4, v5

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 113
    :cond_2
    invoke-static {v1, p0}, Lcom/squareup/cardcustomizations/signature/Spliner;->solve([[F[F)[F

    move-result-object p0

    return-object p0
.end method

.method private varargs expandDirtyRect([Lcom/squareup/cardcustomizations/signature/Point;)V
    .locals 5

    .line 187
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    .line 188
    iget-object v3, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->dirtyRect:Landroid/graphics/Rect;

    iget v4, v2, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    float-to-int v4, v4

    iget v2, v2, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    float-to-int v2, v2

    invoke-virtual {v3, v4, v2}, Landroid/graphics/Rect;->union(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static solve([[F[F)[F
    .locals 9

    .line 153
    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    add-int/lit8 v2, v1, 0x1

    move v3, v2

    :goto_1
    if-ge v3, v0, :cond_1

    .line 161
    aget-object v4, p0, v3

    aget v4, v4, v1

    aget-object v5, p0, v1

    aget v5, v5, v1

    div-float/2addr v4, v5

    move v5, v1

    :goto_2
    if-ge v5, v0, :cond_0

    .line 163
    aget-object v6, p0, v3

    aget v7, v6, v5

    aget-object v8, p0, v1

    aget v8, v8, v5

    mul-float v8, v8, v4

    sub-float/2addr v7, v8

    aput v7, v6, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 165
    :cond_0
    aget v5, p1, v3

    aget v6, p1, v1

    mul-float v4, v4, v6

    sub-float/2addr v5, v4

    aput v5, p1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    move v1, v2

    goto :goto_0

    .line 170
    :cond_2
    new-array v1, v0, [F

    add-int/lit8 v2, v0, -0x1

    :goto_3
    if-ltz v2, :cond_4

    const/4 v3, 0x0

    add-int/lit8 v4, v2, 0x1

    :goto_4
    if-ge v4, v0, :cond_3

    .line 174
    aget-object v5, p0, v2

    aget v5, v5, v4

    aget v6, v1, v4

    mul-float v5, v5, v6

    add-float/2addr v3, v5

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 176
    :cond_3
    aget v4, p1, v2

    sub-float/2addr v4, v3

    aget-object v3, p0, v2

    aget v3, v3, v2

    div-float/2addr v4, v3

    aput v4, v1, v2

    add-int/lit8 v2, v2, -0x1

    goto :goto_3

    :cond_4
    return-object v1
.end method


# virtual methods
.method public addPoint(Lcom/squareup/cardcustomizations/signature/Point;)V
    .locals 12

    .line 34
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->points:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->points:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/cardcustomizations/signature/Spliner;->addArtificialPoint()V

    .line 39
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->points:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v1, 0x4

    if-ge p1, v1, :cond_1

    return-void

    .line 41
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->points:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v1

    const/4 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->points:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {p1, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    .line 48
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v3, v2, -0x2

    .line 53
    new-array v4, v3, [F

    const/4 v5, 0x1

    .line 54
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/cardcustomizations/signature/Point;

    iget v6, v6, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    const/high16 v7, 0x40c00000    # 6.0f

    mul-float v6, v6, v7

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/cardcustomizations/signature/Point;

    iget v8, v8, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    sub-float/2addr v6, v8

    aput v6, v4, v1

    const/4 v6, 0x1

    :goto_0
    add-int/lit8 v8, v3, -0x1

    if-ge v6, v8, :cond_2

    .line 56
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/cardcustomizations/signature/Point;

    iget v8, v8, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    mul-float v8, v8, v7

    aput v8, v4, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 58
    :cond_2
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/cardcustomizations/signature/Point;

    iget v6, v6, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    mul-float v6, v6, v7

    add-int/lit8 v9, v2, -0x1

    .line 59
    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/cardcustomizations/signature/Point;

    iget v10, v10, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    sub-float/2addr v6, v10

    aput v6, v4, v8

    .line 60
    invoke-static {v4}, Lcom/squareup/cardcustomizations/signature/Spliner;->computeBSpline([F)[F

    move-result-object v4

    .line 63
    new-array v6, v3, [F

    .line 64
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/cardcustomizations/signature/Point;

    iget v10, v10, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    mul-float v10, v10, v7

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/cardcustomizations/signature/Point;

    iget v11, v11, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    sub-float/2addr v10, v11

    aput v10, v6, v1

    const/4 v1, 0x1

    :goto_1
    if-ge v1, v8, :cond_3

    .line 66
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/cardcustomizations/signature/Point;

    iget v10, v10, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    mul-float v10, v10, v7

    aput v10, v6, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 68
    :cond_3
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardcustomizations/signature/Point;

    iget v1, v1, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    mul-float v1, v1, v7

    .line 69
    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/cardcustomizations/signature/Point;

    iget v7, v7, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    sub-float/2addr v1, v7

    aput v1, v6, v8

    .line 70
    invoke-static {v6}, Lcom/squareup/cardcustomizations/signature/Spliner;->computeBSpline([F)[F

    move-result-object v1

    add-int/lit8 v2, v2, -0x3

    .line 75
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardcustomizations/signature/Point;

    .line 76
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardcustomizations/signature/Point;

    .line 78
    new-instance v3, Lcom/squareup/cardcustomizations/signature/Point;

    array-length v6, v4

    sub-int/2addr v6, v0

    aget v6, v4, v6

    array-length v7, v1

    sub-int/2addr v7, v0

    aget v0, v1, v7

    invoke-direct {v3, v6, v0}, Lcom/squareup/cardcustomizations/signature/Point;-><init>(FF)V

    .line 79
    new-instance v0, Lcom/squareup/cardcustomizations/signature/Point;

    array-length v6, v4

    sub-int/2addr v6, v5

    aget v4, v4, v6

    array-length v6, v1

    sub-int/2addr v6, v5

    aget v1, v1, v6

    invoke-direct {v0, v4, v1}, Lcom/squareup/cardcustomizations/signature/Point;-><init>(FF)V

    .line 81
    invoke-virtual {v3, v0}, Lcom/squareup/cardcustomizations/signature/Point;->oneThirdTo(Lcom/squareup/cardcustomizations/signature/Point;)Lcom/squareup/cardcustomizations/signature/Point;

    move-result-object v1

    .line 82
    invoke-virtual {v3, v0}, Lcom/squareup/cardcustomizations/signature/Point;->twoThirdsTo(Lcom/squareup/cardcustomizations/signature/Point;)Lcom/squareup/cardcustomizations/signature/Point;

    move-result-object v0

    .line 84
    iget-object v3, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->beziers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 87
    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->beziers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    sub-int/2addr v3, v5

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;

    .line 88
    iget-object v3, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->beziers:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 89
    invoke-static {v2}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->access$000(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)Lcom/squareup/cardcustomizations/signature/Point;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/squareup/cardcustomizations/signature/Point;->halfWayTo(Lcom/squareup/cardcustomizations/signature/Point;)Lcom/squareup/cardcustomizations/signature/Point;

    move-result-object v3

    .line 91
    iget-object v4, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->beziers:Ljava/util/List;

    new-instance v6, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;

    invoke-static {v2}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->access$100(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)Lcom/squareup/cardcustomizations/signature/Point;

    move-result-object v7

    invoke-static {v2}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->access$200(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)Lcom/squareup/cardcustomizations/signature/Point;

    move-result-object v8

    invoke-static {v2}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->access$000(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)Lcom/squareup/cardcustomizations/signature/Point;

    move-result-object v2

    invoke-direct {v6, v7, v3, v8, v2}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;-><init>(Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->beziers:Ljava/util/List;

    new-instance v4, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;

    invoke-direct {v4, v3, p1, v1, v0}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;-><init>(Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 94
    :cond_4
    iget-object v3, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->beziers:Ljava/util/List;

    new-instance v4, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;

    invoke-direct {v4, v2, p1, v1, v0}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;-><init>(Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    :goto_2
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->beziers:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, v5

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;

    invoke-virtual {p0, p1}, Lcom/squareup/cardcustomizations/signature/Spliner;->expandDirtyRect(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)V

    return-void
.end method

.method public expandDirtyRect(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/cardcustomizations/signature/Point;

    .line 183
    invoke-static {p1}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->access$100(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)Lcom/squareup/cardcustomizations/signature/Point;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->access$300(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)Lcom/squareup/cardcustomizations/signature/Point;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p1}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->access$200(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)Lcom/squareup/cardcustomizations/signature/Point;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {p1}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->access$000(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)Lcom/squareup/cardcustomizations/signature/Point;

    move-result-object p1

    const/4 v1, 0x3

    aput-object p1, v0, v1

    invoke-direct {p0, v0}, Lcom/squareup/cardcustomizations/signature/Spliner;->expandDirtyRect([Lcom/squareup/cardcustomizations/signature/Point;)V

    return-void
.end method

.method public getBeziers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;",
            ">;"
        }
    .end annotation

    .line 215
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->beziers:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDirtyRect()Landroid/graphics/Rect;
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->dirtyRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public isDirty()Z
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->dirtyRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public resetDirty()V
    .locals 2

    .line 210
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Spliner;->dirtyRect:Landroid/graphics/Rect;

    const/4 v1, 0x0

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    return-void
.end method
