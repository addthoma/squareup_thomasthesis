.class public final Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;
.super Ljava/lang/Object;
.source "DouglasPeuckerStrokeSimplifier.kt"

# interfaces
.implements Lcom/squareup/cardcustomizations/signature/GlyphPainter;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDouglasPeuckerStrokeSimplifier.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DouglasPeuckerStrokeSimplifier.kt\ncom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,117:1\n8739#2:118\n9056#2,3:119\n8739#2:122\n9056#2,3:123\n8739#2:126\n9056#2,3:127\n8739#2:130\n9056#2,3:131\n*E\n*S KotlinDebug\n*F\n+ 1 DouglasPeuckerStrokeSimplifier.kt\ncom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier\n*L\n110#1:118\n110#1,3:119\n111#1:122\n111#1,3:123\n112#1:126\n112#1,3:127\n113#1:130\n113#1,3:131\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0008\u0007\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0017H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J!\u0010\u001d\u001a\u00020\u00082\u0012\u0010\u0015\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u001f0\u001e\"\u00020\u001fH\u0002\u00a2\u0006\u0002\u0010 J\u0008\u0010!\u001a\u00020\u001bH\u0016J\u0008\u0010\"\u001a\u00020#H\u0016J\u0010\u0010$\u001a\u00020\u001b2\u0006\u0010%\u001a\u00020&H\u0016J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016H\u0016J,\u0010\'\u001a\u00020\u001b*\u00020\u00122\u0006\u0010(\u001a\u00020\u001f2\u0006\u0010)\u001a\u00020\u001f2\u0006\u0010*\u001a\u00020\u001f2\u0006\u0010+\u001a\u00020\u001fH\u0002J$\u0010,\u001a\u00020\u001b*\u00020\u00122\u0006\u0010)\u001a\u00020\u001f2\u0006\u0010*\u001a\u00020\u001f2\u0006\u0010+\u001a\u00020\u001fH\u0002J\u0014\u0010-\u001a\u00020\u001b*\u00020\u00122\u0006\u0010\u001c\u001a\u00020\u001fH\u0002R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0017\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;",
        "Lcom/squareup/cardcustomizations/signature/GlyphPainter;",
        "canvas",
        "Landroid/graphics/Canvas;",
        "paint",
        "Landroid/graphics/Paint;",
        "(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V",
        "boundingBox",
        "Landroid/graphics/RectF;",
        "getBoundingBox",
        "()Landroid/graphics/RectF;",
        "setBoundingBox",
        "(Landroid/graphics/RectF;)V",
        "getCanvas",
        "()Landroid/graphics/Canvas;",
        "getPaint",
        "()Landroid/graphics/Paint;",
        "path",
        "Landroid/graphics/Path;",
        "getPath",
        "()Landroid/graphics/Path;",
        "points",
        "Ljava/util/ArrayList;",
        "Lcom/squareup/cardcustomizations/signature/Point$Timestamped;",
        "getPoints",
        "()Ljava/util/ArrayList;",
        "addPoint",
        "",
        "point",
        "boundsOfPoints",
        "",
        "Lcom/squareup/cardcustomizations/signature/Point;",
        "([Lcom/squareup/cardcustomizations/signature/Point;)Landroid/graphics/RectF;",
        "finish",
        "getPointCount",
        "",
        "invalidate",
        "view",
        "Landroid/view/View;",
        "bezier",
        "start",
        "control1",
        "control2",
        "end",
        "cubicTo",
        "moveTo",
        "card-customization_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# instance fields
.field private boundingBox:Landroid/graphics/RectF;

.field private final canvas:Landroid/graphics/Canvas;

.field private final paint:Landroid/graphics/Paint;

.field private final path:Landroid/graphics/Path;

.field private final points:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/squareup/cardcustomizations/signature/Point$Timestamped;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paint"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->canvas:Landroid/graphics/Canvas;

    iput-object p2, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->paint:Landroid/graphics/Paint;

    .line 14
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    .line 15
    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->path:Landroid/graphics/Path;

    return-void
.end method

.method private final bezier(Landroid/graphics/Path;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;)V
    .locals 0

    .line 91
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 92
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->moveTo(Landroid/graphics/Path;Lcom/squareup/cardcustomizations/signature/Point;)V

    .line 93
    invoke-direct {p0, p1, p3, p4, p5}, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->cubicTo(Landroid/graphics/Path;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;)V

    .line 94
    iget-object p2, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->canvas:Landroid/graphics/Canvas;

    iget-object p3, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->paint:Landroid/graphics/Paint;

    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 95
    new-instance p2, Landroid/graphics/RectF;

    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    const/4 p3, 0x0

    .line 96
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 97
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->boundingBox:Landroid/graphics/RectF;

    invoke-static {p1, p2}, Lcom/squareup/cardcustomizations/signature/Rects;->unionWith(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->boundingBox:Landroid/graphics/RectF;

    return-void
.end method

.method private final varargs boundsOfPoints([Lcom/squareup/cardcustomizations/signature/Point;)Landroid/graphics/RectF;
    .locals 8

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 119
    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, p1, v3

    .line 110
    iget v4, v4, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 121
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 110
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->min(Ljava/lang/Iterable;)Ljava/lang/Float;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    .line 122
    :goto_1
    new-instance v3, Ljava/util/ArrayList;

    array-length v4, p1

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 123
    array-length v4, p1

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v4, :cond_2

    aget-object v6, p1, v5

    .line 111
    iget v6, v6, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 125
    :cond_2
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 111
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->min(Ljava/lang/Iterable;)Ljava/lang/Float;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    goto :goto_3

    :cond_3
    const/4 v3, 0x0

    .line 126
    :goto_3
    new-instance v4, Ljava/util/ArrayList;

    array-length v5, p1

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 127
    array-length v5, p1

    const/4 v6, 0x0

    :goto_4
    if-ge v6, v5, :cond_4

    aget-object v7, p1, v6

    .line 112
    iget v7, v7, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 129
    :cond_4
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 112
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->max(Ljava/lang/Iterable;)Ljava/lang/Float;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    goto :goto_5

    :cond_5
    const/4 v4, 0x0

    .line 130
    :goto_5
    new-instance v5, Ljava/util/ArrayList;

    array-length v6, p1

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v5, Ljava/util/Collection;

    .line 131
    array-length v6, p1

    :goto_6
    if-ge v2, v6, :cond_6

    aget-object v7, p1, v2

    .line 113
    iget v7, v7, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 133
    :cond_6
    check-cast v5, Ljava/util/List;

    check-cast v5, Ljava/lang/Iterable;

    .line 113
    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->max(Ljava/lang/Iterable;)Ljava/lang/Float;

    move-result-object p1

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 109
    :cond_7
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1, v0, v3, v4, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object p1
.end method

.method private final cubicTo(Landroid/graphics/Path;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;)V
    .locals 7

    .line 105
    iget v1, p2, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    iget v2, p2, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    iget v3, p3, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    iget v4, p3, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    iget v5, p4, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    iget v6, p4, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    return-void
.end method

.method private final moveTo(Landroid/graphics/Path;Lcom/squareup/cardcustomizations/signature/Point;)V
    .locals 1

    .line 101
    iget v0, p2, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    iget p2, p2, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Path;->moveTo(FF)V

    return-void
.end method


# virtual methods
.method public addPoint(Lcom/squareup/cardcustomizations/signature/Point$Timestamped;)V
    .locals 9

    const-string v0, "point"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;

    move-object v1, p1

    check-cast v1, Lcom/squareup/cardcustomizations/signature/Point;

    invoke-virtual {v0, v1}, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->distanceTo(Lcom/squareup/cardcustomizations/signature/Point;)F

    move-result v0

    const/high16 v1, 0x40400000    # 3.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    return-void

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    const/4 v0, 0x5

    if-lt p1, v0, :cond_3

    add-int/lit8 v1, p1, -0x2

    .line 30
    rem-int/lit8 v1, v1, 0x3

    if-eqz v1, :cond_1

    goto :goto_1

    .line 37
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    add-int/lit8 v2, p1, -0x4

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "points[size - 4]"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;

    .line 38
    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    add-int/lit8 v3, p1, -0x3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "points[size - 3]"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;

    if-le p1, v0, :cond_2

    .line 42
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    add-int/lit8 p1, p1, -0x6

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;

    move-object v0, v1

    check-cast v0, Lcom/squareup/cardcustomizations/signature/Point;

    invoke-virtual {p1, v0}, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->halfWayTo(Lcom/squareup/cardcustomizations/signature/Point;)Lcom/squareup/cardcustomizations/signature/Point;

    move-result-object p1

    const-string v0, "points[size - 6].halfWayTo(control1)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 44
    :cond_2
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    const-string v0, "points[0]"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/cardcustomizations/signature/Point;

    :goto_0
    move-object v5, p1

    .line 47
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    check-cast p1, Ljava/util/List;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardcustomizations/signature/Point;

    invoke-virtual {v2, p1}, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->halfWayTo(Lcom/squareup/cardcustomizations/signature/Point;)Lcom/squareup/cardcustomizations/signature/Point;

    move-result-object v8

    .line 49
    iget-object v4, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->path:Landroid/graphics/Path;

    move-object v6, v1

    check-cast v6, Lcom/squareup/cardcustomizations/signature/Point;

    move-object v7, v2

    check-cast v7, Lcom/squareup/cardcustomizations/signature/Point;

    const-string p1, "end"

    invoke-static {v8, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->bezier(Landroid/graphics/Path;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public boundingBox()Landroid/graphics/RectF;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->boundingBox:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    :goto_0
    return-object v0
.end method

.method public finish()V
    .locals 13

    .line 57
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const-string v1, "points[0]"

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 58
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->boundingBox:Landroid/graphics/RectF;

    new-array v3, v3, [Lcom/squareup/cardcustomizations/signature/Point;

    iget-object v4, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/squareup/cardcustomizations/signature/Point;

    aput-object v4, v3, v2

    invoke-direct {p0, v3}, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->boundsOfPoints([Lcom/squareup/cardcustomizations/signature/Point;)Landroid/graphics/RectF;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/cardcustomizations/signature/Rects;->unionWith(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->boundingBox:Landroid/graphics/RectF;

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->paint:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 61
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    const/high16 v3, 0x3fc00000    # 1.5f

    mul-float v1, v1, v3

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 62
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->canvas:Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;

    iget v3, v3, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->x:F

    iget-object v4, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;

    iget v2, v2, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->y:F

    invoke-virtual {v1, v3, v2, v0}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v4, 0x5

    if-ge v0, v4, :cond_1

    .line 70
    iget-object v4, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/squareup/cardcustomizations/signature/Point;

    .line 71
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    check-cast v1, Ljava/util/List;

    new-instance v5, Lkotlin/ranges/IntRange;

    sub-int/2addr v0, v3

    invoke-direct {v5, v3, v0}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-static {v1, v5}, Lkotlin/collections/CollectionsKt;->slice(Ljava/util/List;Lkotlin/ranges/IntRange;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v0, -0x2

    .line 73
    rem-int/lit8 v4, v1, 0x3

    sub-int/2addr v1, v4

    .line 74
    iget-object v4, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;

    iget-object v5, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    add-int/2addr v1, v3

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/cardcustomizations/signature/Point;

    invoke-virtual {v4, v5}, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->halfWayTo(Lcom/squareup/cardcustomizations/signature/Point;)Lcom/squareup/cardcustomizations/signature/Point;

    move-result-object v4

    const-string v5, "points[realStart - 1].ha\u2026To(points[realStart + 1])"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object v5, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    check-cast v5, Ljava/util/List;

    new-instance v6, Lkotlin/ranges/IntRange;

    sub-int/2addr v0, v3

    invoke-direct {v6, v1, v0}, Lkotlin/ranges/IntRange;-><init>(II)V

    invoke-static {v5, v6}, Lkotlin/collections/CollectionsKt;->slice(Ljava/util/List;Lkotlin/ranges/IntRange;)Ljava/util/List;

    move-result-object v0

    :goto_0
    move-object v6, v4

    .line 78
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    check-cast v1, Ljava/util/List;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;

    .line 80
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v3, :cond_2

    .line 81
    iget-object v7, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->canvas:Landroid/graphics/Canvas;

    iget v8, v6, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    iget v9, v6, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    iget v10, v1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->x:F

    iget v11, v1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->y:F

    iget-object v12, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->paint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->boundingBox:Landroid/graphics/RectF;

    new-array v4, v5, [Lcom/squareup/cardcustomizations/signature/Point;

    aput-object v6, v4, v2

    check-cast v1, Lcom/squareup/cardcustomizations/signature/Point;

    aput-object v1, v4, v3

    invoke-direct {p0, v4}, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->boundsOfPoints([Lcom/squareup/cardcustomizations/signature/Point;)Landroid/graphics/RectF;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/cardcustomizations/signature/Rects;->unionWith(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->boundingBox:Landroid/graphics/RectF;

    goto :goto_1

    .line 83
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v5, :cond_3

    .line 84
    iget-object v5, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->path:Landroid/graphics/Path;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Lcom/squareup/cardcustomizations/signature/Point;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/cardcustomizations/signature/Point;

    move-object v9, v1

    check-cast v9, Lcom/squareup/cardcustomizations/signature/Point;

    move-object v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->bezier(Landroid/graphics/Path;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;)V

    goto :goto_1

    .line 85
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x3

    if-ne v2, v4, :cond_4

    .line 86
    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->path:Landroid/graphics/Path;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v7, v3

    check-cast v7, Lcom/squareup/cardcustomizations/signature/Point;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/cardcustomizations/signature/Point;

    move-object v9, v1

    check-cast v9, Lcom/squareup/cardcustomizations/signature/Point;

    move-object v4, p0

    move-object v5, v2

    invoke-direct/range {v4 .. v9}, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->bezier(Landroid/graphics/Path;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;Lcom/squareup/cardcustomizations/signature/Point;)V

    :cond_4
    :goto_1
    return-void
.end method

.method public final getBoundingBox()Landroid/graphics/RectF;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->boundingBox:Landroid/graphics/RectF;

    return-object v0
.end method

.method public final getCanvas()Landroid/graphics/Canvas;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->canvas:Landroid/graphics/Canvas;

    return-object v0
.end method

.method public final getPaint()Landroid/graphics/Paint;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->paint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public final getPath()Landroid/graphics/Path;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->path:Landroid/graphics/Path;

    return-object v0
.end method

.method public getPointCount()I
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getPoints()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/squareup/cardcustomizations/signature/Point$Timestamped;",
            ">;"
        }
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    return-object v0
.end method

.method public invalidate(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public points()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/squareup/cardcustomizations/signature/Point$Timestamped;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points:Ljava/util/ArrayList;

    return-object v0
.end method

.method public bridge synthetic points()Ljava/util/List;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->points()Ljava/util/ArrayList;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final setBoundingBox(Landroid/graphics/RectF;)V
    .locals 0

    .line 16
    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;->boundingBox:Landroid/graphics/RectF;

    return-void
.end method
