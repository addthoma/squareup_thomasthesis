.class final Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onDirectDebitInfo$1;
.super Lkotlin/jvm/internal/Lambda;
.source "GetDirectDebitInfoWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;->onDirectDebitInfo(Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;",
        "-",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $directDebitInfo:Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;


# direct methods
.method constructor <init>(Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onDirectDebitInfo$1;->$directDebitInfo:Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onDirectDebitInfo$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;",
            "-",
            "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    new-instance v0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Success;

    .line 101
    iget-object v1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onDirectDebitInfo$1;->$directDebitInfo:Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;

    check-cast v1, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;->getIdempotenceKey()Ljava/lang/String;

    move-result-object v2

    .line 102
    iget-object v1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onDirectDebitInfo$1;->$directDebitInfo:Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;

    check-cast v1, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;->getName()Ljava/lang/String;

    move-result-object v3

    .line 103
    iget-object v1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onDirectDebitInfo$1;->$directDebitInfo:Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;

    check-cast v1, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;->getEmail()Ljava/lang/String;

    move-result-object v4

    .line 104
    iget-object v1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onDirectDebitInfo$1;->$directDebitInfo:Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;

    check-cast v1, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;->getOrganization()Ljava/lang/String;

    move-result-object v5

    .line 105
    iget-object v1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onDirectDebitInfo$1;->$directDebitInfo:Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;

    check-cast v1, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;->getAddress()Ljava/lang/String;

    move-result-object v6

    .line 106
    iget-object v1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onDirectDebitInfo$1;->$directDebitInfo:Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;

    check-cast v1, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;->getReference()Ljava/lang/String;

    move-result-object v7

    move-object v1, v0

    .line 100
    invoke-direct/range {v1 .. v7}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Success;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
