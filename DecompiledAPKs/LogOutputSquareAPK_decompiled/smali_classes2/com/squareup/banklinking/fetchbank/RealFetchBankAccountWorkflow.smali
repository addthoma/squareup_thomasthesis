.class public final Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealFetchBankAccountWorkflow.kt"

# interfaces
.implements Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealFetchBankAccountWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealFetchBankAccountWorkflow.kt\ncom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,135:1\n32#2,12:136\n85#3:148\n240#4:149\n276#5:150\n149#6,5:151\n*E\n*S KotlinDebug\n*F\n+ 1 RealFetchBankAccountWorkflow.kt\ncom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow\n*L\n82#1,12:136\n90#1:148\n90#1:149\n90#1:150\n118#1,5:151\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0001%B\u000f\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ@\u0010\u000e\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t2\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00102\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u00132\u0008\u0008\u0002\u0010\u0014\u001a\u00020\u00132\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u0016H\u0002J\u001f\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u00032\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016\u00a2\u0006\u0002\u0010\u001bJ\u001c\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002JS\u0010 \u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u0018\u001a\u00020\u00032\u0006\u0010\u001e\u001a\u00020\u00042\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\"H\u0016\u00a2\u0006\u0002\u0010#J\u0010\u0010$\u001a\u00020\u001a2\u0006\u0010\u001e\u001a\u00020\u0004H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "bankAccountSettings",
        "Lcom/squareup/banklinking/BankAccountSettings;",
        "(Lcom/squareup/banklinking/BankAccountSettings;)V",
        "fetchBankAccountScreen",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action;",
        "title",
        "",
        "message",
        "retryable",
        "",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;",
        "onBankSettingsState",
        "Lcom/squareup/workflow/WorkflowAction;",
        "state",
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/banklinking/BankAccountSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bankAccountSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    return-void
.end method

.method public static final synthetic access$onBankSettingsState(Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;->onBankSettingsState(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final fetchBankAccountScreen(Lcom/squareup/workflow/Sink;Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/workflow/legacy/Screen;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 112
    new-instance v6, Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;

    .line 116
    new-instance v0, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$fetchBankAccountScreen$1;

    invoke-direct {v0, p1}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$fetchBankAccountScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 117
    new-instance v0, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$fetchBankAccountScreen$2;

    invoke-direct {v0, p1}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$fetchBankAccountScreen$2;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function0;

    move-object v0, v6

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    .line 112
    invoke-direct/range {v0 .. v5}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v6, Lcom/squareup/workflow/legacy/V2Screen;

    .line 152
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 153
    const-class p2, Lcom/squareup/banklinking/fetchbank/FetchBankAccountScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string p3, ""

    invoke-static {p2, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 154
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 152
    invoke-direct {p1, p2, v6, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method static synthetic fetchBankAccountScreen$default(Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;Lcom/squareup/workflow/Sink;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1

    and-int/lit8 p6, p5, 0x2

    const-string v0, ""

    if-eqz p6, :cond_0

    move-object p2, v0

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    move-object p3, v0

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    const/4 p4, 0x0

    .line 110
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;->fetchBankAccountScreen(Lcom/squareup/workflow/Sink;Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    return-object p0
.end method

.method private final onBankSettingsState(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;",
            "Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;",
            ">;"
        }
    .end annotation

    .line 124
    iget-object v0, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    sget-object v1, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 131
    new-instance v0, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$ShowErrorMessage;

    iget-object p1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-direct {v0, p1}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$ShowErrorMessage;-><init>(Lcom/squareup/receiving/FailureMessage;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_1

    .line 132
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected latestBankAccountState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 126
    :cond_2
    new-instance v0, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$FinishWithBankSettingsState;

    .line 127
    iget-boolean v1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->requiresPassword:Z

    .line 128
    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->bankAccount()Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, v2, Lcom/squareup/protos/client/bankaccount/BankAccount;->primary_institution_number:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    .line 129
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->verificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p1

    .line 126
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$FinishWithBankSettingsState;-><init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    :goto_1
    return-object v0
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 136
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 141
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 143
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 144
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 145
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 147
    :cond_3
    check-cast v1, Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 82
    :cond_4
    sget-object p1, Lcom/squareup/banklinking/fetchbank/FetchBankAccountState$FetchingBankAccount;->INSTANCE:Lcom/squareup/banklinking/fetchbank/FetchBankAccountState$FetchingBankAccount;

    move-object v1, p1

    check-cast v1, Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;->render(Lkotlin/Unit;Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;",
            "-",
            "Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object v7, p0

    move-object/from16 v0, p2

    const-string v1, "props"

    move-object v2, p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "context"

    move-object/from16 v2, p3

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    sget-object v1, Lcom/squareup/banklinking/fetchbank/FetchBankAccountState$FetchingBankAccount;->INSTANCE:Lcom/squareup/banklinking/fetchbank/FetchBankAccountState$FetchingBankAccount;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    iget-object v0, v7, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v0}, Lcom/squareup/banklinking/BankAccountSettings;->refresh()Lio/reactivex/Single;

    move-result-object v0

    .line 148
    sget-object v1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$render$$inlined$asWorker$1;

    const/4 v3, 0x0

    invoke-direct {v1, v0, v3}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 149
    invoke-static {v1}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 150
    const-class v1, Lcom/squareup/banklinking/BankAccountSettings$State;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v9, v3

    check-cast v9, Lcom/squareup/workflow/Worker;

    const/4 v10, 0x0

    .line 90
    new-instance v0, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$render$1;

    invoke-direct {v0, p0}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$render$1;-><init>(Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;)V

    move-object v11, v0

    check-cast v11, Lkotlin/jvm/functions/Function1;

    const/4 v12, 0x2

    const/4 v13, 0x0

    move-object/from16 v8, p3

    invoke-static/range {v8 .. v13}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 94
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;->fetchBankAccountScreen$default(Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;Lcom/squareup/workflow/Sink;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v0

    goto :goto_0

    .line 96
    :cond_0
    instance-of v1, v0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountState$CouldNotFetchBankAccount;

    if-eqz v1, :cond_1

    .line 97
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v1

    .line 98
    check-cast v0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountState$CouldNotFetchBankAccount;

    invoke-virtual {v0}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountState$CouldNotFetchBankAccount;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 99
    invoke-virtual {v0}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountState$CouldNotFetchBankAccount;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 100
    invoke-virtual {v0}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountState$CouldNotFetchBankAccount;->getRetryable()Z

    move-result v0

    .line 96
    invoke-direct {p0, v1, v2, v3, v0}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;->fetchBankAccountScreen(Lcom/squareup/workflow/Sink;Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v0

    .line 102
    :goto_0
    sget-object v1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {v0, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    return-object v0

    .line 96
    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public snapshotState(Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;->snapshotState(Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
