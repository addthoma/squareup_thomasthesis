.class public final Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealLinkBankAccountWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/showresult/ShowResultWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/showresult/ShowResultWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p5, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/showresult/ShowResultWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;)",
            "Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;"
        }
    .end annotation

    .line 46
    new-instance v6, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;Lcom/squareup/banklinking/showresult/ShowResultWorkflow;Lcom/squareup/banklinking/BankAccountSettings;)Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;
    .locals 7

    .line 52
    new-instance v6, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;-><init>(Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;Lcom/squareup/banklinking/showresult/ShowResultWorkflow;Lcom/squareup/banklinking/BankAccountSettings;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;
    .locals 5

    .line 39
    iget-object v0, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;

    iget-object v1, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;

    iget-object v2, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;

    iget-object v3, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/banklinking/showresult/ShowResultWorkflow;

    iget-object v4, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/banklinking/BankAccountSettings;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;->newInstance(Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;Lcom/squareup/banklinking/showresult/ShowResultWorkflow;Lcom/squareup/banklinking/BankAccountSettings;)Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow_Factory;->get()Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;

    move-result-object v0

    return-object v0
.end method
