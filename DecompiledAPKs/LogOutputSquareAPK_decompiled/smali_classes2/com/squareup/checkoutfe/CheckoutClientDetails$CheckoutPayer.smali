.class public final Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;
.super Lcom/squareup/wire/Message;
.source "CheckoutClientDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/CheckoutClientDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CheckoutPayer"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$ProtoAdapter_CheckoutPayer;,
        Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;",
        "Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final coupon:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.coupons.Coupon#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;"
        }
    .end annotation
.end field

.field public final loyalty_status:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.AddedTender$LoyaltyStatus#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;",
            ">;"
        }
    .end annotation
.end field

.field public final receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.AddedTender$ReceiptDetails#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 101
    new-instance v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$ProtoAdapter_CheckoutPayer;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$ProtoAdapter_CheckoutPayer;-><init>()V

    sput-object v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;",
            ">;)V"
        }
    .end annotation

    .line 127
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;-><init>(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 132
    sget-object v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 133
    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    const-string p1, "coupon"

    .line 134
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->coupon:Ljava/util/List;

    const-string p1, "loyalty_status"

    .line 135
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->loyalty_status:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 151
    :cond_0
    instance-of v1, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 152
    :cond_1
    check-cast p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    .line 153
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    iget-object v3, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    .line 154
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->coupon:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->coupon:Ljava/util/List;

    .line 155
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->loyalty_status:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->loyalty_status:Ljava/util/List;

    .line 156
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 161
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 163
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 164
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 165
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->coupon:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->loyalty_status:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;
    .locals 2

    .line 140
    new-instance v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;-><init>()V

    .line 141
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    iput-object v1, v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    .line 142
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->coupon:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->coupon:Ljava/util/List;

    .line 143
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->loyalty_status:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->loyalty_status:Ljava/util/List;

    .line 144
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->newBuilder()Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    if-eqz v1, :cond_0

    const-string v1, ", receipt_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 176
    :cond_0
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->coupon:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", coupon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->coupon:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 177
    :cond_1
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->loyalty_status:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", loyalty_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->loyalty_status:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CheckoutPayer{"

    .line 178
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
