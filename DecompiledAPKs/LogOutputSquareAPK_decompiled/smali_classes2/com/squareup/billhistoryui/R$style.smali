.class public final Lcom/squareup/billhistoryui/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistoryui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final TextAppearance_TransactionDetails_Tender_CashierRow:I = 0x7f1302a0

.field public static final TextAppearance_TransactionDetails_Tender_CustomerRow_Initials:I = 0x7f1302a1

.field public static final TextAppearance_TransactionDetails_Tender_CustomerRow_Name:I = 0x7f1302a2

.field public static final TextAppearance_TransactionsHistory_SearchBar:I = 0x7f1302a3

.field public static final TextAppearance_TransactionsHistory_SmartLine_Subtitle:I = 0x7f1302a4

.field public static final TextAppearance_TransactionsHistory_SmartLine_Title:I = 0x7f1302a5

.field public static final TextAppearance_TransactionsHistory_SmartLine_Value:I = 0x7f1302a6

.field public static final TextAppearance_TransactionsHistory_StickyHeaders:I = 0x7f1302a7

.field public static final TextAppearance_TransactionsHistory_TopMessagePanel_Message:I = 0x7f1302a8

.field public static final TextAppearance_TransactionsHistory_TopMessagePanel_TapDisabledMessage:I = 0x7f1302a9

.field public static final TextAppearance_TransactionsHistory_TopMessagePanel_TapEnabledMessage:I = 0x7f1302aa

.field public static final TextAppearance_TransactionsHistory_TopMessagePanel_Title:I = 0x7f1302ab

.field public static final Widget_Marin_HorizontalDividers:I = 0x7f13048d

.field public static final Widget_Noho_TransactionsHistory_SearchBar:I = 0x7f1305ac


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
