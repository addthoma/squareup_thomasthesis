.class public Lcom/squareup/ApiUrlSelector;
.super Ljava/lang/Object;
.source "ApiUrlSelector.java"

# interfaces
.implements Lcom/squareup/http/RedirectUrlSelector;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;
    }
.end annotation


# instance fields
.field private baseUrlIndex:I

.field private baseUrls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lokhttp3/HttpUrl;",
            ">;"
        }
    .end annotation
.end field

.field private currentUrls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final defaultUrls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private lastChangeTimeStamp:J

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field protected urlDurationInMillis:J


# direct methods
.method public constructor <init>(Ljava/util/List;JLcom/squareup/log/OhSnapLogger;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;J",
            "Lcom/squareup/log/OhSnapLogger;",
            ")V"
        }
    .end annotation

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ApiUrlSelector;->defaultUrls:Ljava/util/List;

    .line 95
    iput-object p4, p0, Lcom/squareup/ApiUrlSelector;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 96
    iput-wide p2, p0, Lcom/squareup/ApiUrlSelector;->urlDurationInMillis:J

    .line 97
    invoke-direct {p0, p1}, Lcom/squareup/ApiUrlSelector;->setUrls(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ApiUrlSelector;)Ljava/util/List;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/ApiUrlSelector;->defaultUrls:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ApiUrlSelector;Ljava/util/List;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/ApiUrlSelector;->setUrls(Ljava/util/List;)V

    return-void
.end method

.method private getCurrentBaseUrl()Lokhttp3/HttpUrl;
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/ApiUrlSelector;->baseUrls:Ljava/util/List;

    iget v1, p0, Lcom/squareup/ApiUrlSelector;->baseUrlIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/HttpUrl;

    return-object v0
.end method

.method private hasChanged(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 155
    iget-object v0, p0, Lcom/squareup/ApiUrlSelector;->currentUrls:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 156
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/ApiUrlSelector;->currentUrls:Ljava/util/List;

    .line 157
    invoke-interface {v0, p1}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private hasCurrentUrlFailed(Lokhttp3/HttpUrl;)Z
    .locals 3

    .line 138
    invoke-direct {p0}, Lcom/squareup/ApiUrlSelector;->getCurrentBaseUrl()Lokhttp3/HttpUrl;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 140
    invoke-virtual {p1}, Lokhttp3/HttpUrl;->scheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lokhttp3/HttpUrl;->scheme()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    invoke-virtual {p1}, Lokhttp3/HttpUrl;->host()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Lokhttp3/HttpUrl;->host()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private isCurrentUrlExpired(J)Z
    .locals 4

    .line 134
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/squareup/ApiUrlSelector;->lastChangeTimeStamp:J

    sub-long/2addr v0, v2

    cmp-long v2, v0, p1

    if-lez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private moveToNext()V
    .locals 4

    .line 149
    iget v0, p0, Lcom/squareup/ApiUrlSelector;->baseUrlIndex:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/squareup/ApiUrlSelector;->baseUrls:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/ApiUrlSelector;->baseUrlIndex:I

    .line 150
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/ApiUrlSelector;->lastChangeTimeStamp:J

    .line 151
    iget-object v0, p0, Lcom/squareup/ApiUrlSelector;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->API:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "API url changed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/squareup/ApiUrlSelector;->getCurrentBaseUrl()Lokhttp3/HttpUrl;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void
.end method

.method private setUrls(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 125
    invoke-direct {p0, p1}, Lcom/squareup/ApiUrlSelector;->hasChanged(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ApiUrlSelector;->currentUrls:Ljava/util/List;

    .line 127
    invoke-static {p1}, Lcom/squareup/ApiUrlSelector;->toBaseUrls(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ApiUrlSelector;->baseUrls:Ljava/util/List;

    const/4 p1, 0x0

    .line 128
    iput p1, p0, Lcom/squareup/ApiUrlSelector;->baseUrlIndex:I

    .line 129
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/ApiUrlSelector;->lastChangeTimeStamp:J

    :cond_0
    return-void
.end method

.method private static toBaseUrl(Ljava/lang/String;)Lokhttp3/HttpUrl;
    .locals 2

    .line 169
    invoke-static {p0}, Lokhttp3/HttpUrl;->parse(Ljava/lang/String;)Lokhttp3/HttpUrl;

    move-result-object p0

    .line 170
    new-instance v0, Lokhttp3/HttpUrl$Builder;

    invoke-direct {v0}, Lokhttp3/HttpUrl$Builder;-><init>()V

    .line 171
    invoke-virtual {p0}, Lokhttp3/HttpUrl;->scheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/HttpUrl$Builder;->scheme(Ljava/lang/String;)Lokhttp3/HttpUrl$Builder;

    move-result-object v0

    .line 172
    invoke-virtual {p0}, Lokhttp3/HttpUrl;->host()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lokhttp3/HttpUrl$Builder;->host(Ljava/lang/String;)Lokhttp3/HttpUrl$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lokhttp3/HttpUrl$Builder;->build()Lokhttp3/HttpUrl;

    move-result-object p0

    return-object p0
.end method

.method private static toBaseUrls(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lokhttp3/HttpUrl;",
            ">;"
        }
    .end annotation

    .line 161
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 162
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 163
    invoke-static {v1}, Lcom/squareup/ApiUrlSelector;->toBaseUrl(Ljava/lang/String;)Lokhttp3/HttpUrl;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public getBaseUrl()Lokhttp3/HttpUrl;
    .locals 2

    .line 101
    iget-wide v0, p0, Lcom/squareup/ApiUrlSelector;->urlDurationInMillis:J

    invoke-direct {p0, v0, v1}, Lcom/squareup/ApiUrlSelector;->isCurrentUrlExpired(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-direct {p0}, Lcom/squareup/ApiUrlSelector;->moveToNext()V

    .line 104
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ApiUrlSelector;->getCurrentBaseUrl()Lokhttp3/HttpUrl;

    move-result-object v0

    return-object v0
.end method

.method protected getBaseUrls()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lokhttp3/HttpUrl;",
            ">;"
        }
    .end annotation

    .line 117
    iget-object v0, p0, Lcom/squareup/ApiUrlSelector;->baseUrls:Ljava/util/List;

    return-object v0
.end method

.method protected getCurrentIndex()I
    .locals 1

    .line 121
    iget v0, p0, Lcom/squareup/ApiUrlSelector;->baseUrlIndex:I

    return v0
.end method

.method public onUnknownHostError(Lokhttp3/HttpUrl;)V
    .locals 4

    .line 108
    invoke-direct {p0, p1}, Lcom/squareup/ApiUrlSelector;->hasCurrentUrlFailed(Lokhttp3/HttpUrl;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 109
    invoke-direct {p0}, Lcom/squareup/ApiUrlSelector;->moveToNext()V

    .line 110
    invoke-direct {p0}, Lcom/squareup/ApiUrlSelector;->getCurrentBaseUrl()Lokhttp3/HttpUrl;

    move-result-object p1

    invoke-virtual {p1}, Lokhttp3/HttpUrl;->toString()Ljava/lang/String;

    move-result-object p1

    .line 111
    iget-object v0, p0, Lcom/squareup/ApiUrlSelector;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->API:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UnknownHostException for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/ApiUrlSelector;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->API:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "API url changed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
