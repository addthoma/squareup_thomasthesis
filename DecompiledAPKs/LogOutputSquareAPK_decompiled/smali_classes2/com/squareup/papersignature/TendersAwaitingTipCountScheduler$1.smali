.class Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$1;
.super Ljava/lang/Object;
.source "TendersAwaitingTipCountScheduler.java"

# interfaces
.implements Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->doRequestCount()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
        "Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;


# direct methods
.method constructor <init>(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;)V
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$1;->this$0:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "Request failed, rescheduling..."

    .line 202
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 203
    iget-object p1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$1;->this$0:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    invoke-static {p1}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->access$100(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;)V

    return-void
.end method

.method public onSuccess(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Got tender count: %s"

    .line 194
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 195
    iget-object v0, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$1;->this$0:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    iget-object v1, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_count:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;

    invoke-static {v0, v1, p1}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->access$000(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;ILjava/lang/Integer;)V

    .line 197
    iget-object p1, p0, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$1;->this$0:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    invoke-static {p1}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->access$100(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;)V

    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .line 191
    check-cast p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$1;->onSuccess(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)V

    return-void
.end method
