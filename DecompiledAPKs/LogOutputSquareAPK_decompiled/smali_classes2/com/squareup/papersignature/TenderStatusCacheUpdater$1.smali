.class Lcom/squareup/papersignature/TenderStatusCacheUpdater$1;
.super Ljava/lang/Object;
.source "TenderStatusCacheUpdater.java"

# interfaces
.implements Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/papersignature/TenderStatusCacheUpdater;->requestStatusUpdate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
        "Ljava/util/List<",
        "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/papersignature/TenderStatusCacheUpdater;


# direct methods
.method constructor <init>(Lcom/squareup/papersignature/TenderStatusCacheUpdater;)V
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater$1;->this$0:Lcom/squareup/papersignature/TenderStatusCacheUpdater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 87
    iget-object p1, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater$1;->this$0:Lcom/squareup/papersignature/TenderStatusCacheUpdater;

    invoke-static {p1}, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->access$100(Lcom/squareup/papersignature/TenderStatusCacheUpdater;)V

    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .line 79
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/TenderStatusCacheUpdater$1;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;",
            ">;)V"
        }
    .end annotation

    .line 81
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater$1;->this$0:Lcom/squareup/papersignature/TenderStatusCacheUpdater;

    invoke-static {v0, p1}, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->access$000(Lcom/squareup/papersignature/TenderStatusCacheUpdater;Ljava/util/List;)V

    .line 82
    iget-object p1, p0, Lcom/squareup/papersignature/TenderStatusCacheUpdater$1;->this$0:Lcom/squareup/papersignature/TenderStatusCacheUpdater;

    invoke-static {p1}, Lcom/squareup/papersignature/TenderStatusCacheUpdater;->access$100(Lcom/squareup/papersignature/TenderStatusCacheUpdater;)V

    return-void
.end method
