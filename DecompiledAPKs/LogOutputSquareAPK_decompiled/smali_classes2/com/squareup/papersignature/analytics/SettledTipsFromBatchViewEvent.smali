.class public Lcom/squareup/papersignature/analytics/SettledTipsFromBatchViewEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "SettledTipsFromBatchViewEvent.java"


# instance fields
.field public final filtered_transactions:Z

.field public final num_transactions_not_settled:I

.field public final num_transactions_settled:I

.field public final quick_tip_options_not_shown_count:I

.field public final quick_tip_options_not_used_count:I

.field public final quick_tip_options_used_count:I

.field public final sort_order:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZIIIIILjava/lang/String;)V
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->SETTLED_TIPS_FROM_BATCH_VIEW:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 23
    iput-boolean p1, p0, Lcom/squareup/papersignature/analytics/SettledTipsFromBatchViewEvent;->filtered_transactions:Z

    .line 24
    iput p2, p0, Lcom/squareup/papersignature/analytics/SettledTipsFromBatchViewEvent;->num_transactions_settled:I

    .line 25
    iput p3, p0, Lcom/squareup/papersignature/analytics/SettledTipsFromBatchViewEvent;->num_transactions_not_settled:I

    .line 26
    iput p4, p0, Lcom/squareup/papersignature/analytics/SettledTipsFromBatchViewEvent;->quick_tip_options_not_shown_count:I

    .line 27
    iput p5, p0, Lcom/squareup/papersignature/analytics/SettledTipsFromBatchViewEvent;->quick_tip_options_not_used_count:I

    .line 28
    iput p6, p0, Lcom/squareup/papersignature/analytics/SettledTipsFromBatchViewEvent;->quick_tip_options_used_count:I

    .line 29
    iput-object p7, p0, Lcom/squareup/papersignature/analytics/SettledTipsFromBatchViewEvent;->sort_order:Ljava/lang/String;

    return-void
.end method
