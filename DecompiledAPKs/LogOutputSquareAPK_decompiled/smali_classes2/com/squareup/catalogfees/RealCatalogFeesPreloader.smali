.class public Lcom/squareup/catalogfees/RealCatalogFeesPreloader;
.super Ljava/lang/Object;
.source "RealCatalogFeesPreloader.java"

# interfaces
.implements Lcom/squareup/catalogfees/CatalogFeesPreloader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final eventSink:Lcom/squareup/badbus/BadEventSink;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader;->cogsProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader;->eventSink:Lcom/squareup/badbus/BadEventSink;

    .line 31
    iput-object p3, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/catalogfees/RealCatalogFeesPreloader;Lcom/squareup/shared/catalog/CatalogResult;Ljava/lang/Runnable;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/squareup/catalogfees/RealCatalogFeesPreloader;->announceResult(Lcom/squareup/shared/catalog/CatalogResult;Ljava/lang/Runnable;)V

    return-void
.end method

.method private announceResult(Lcom/squareup/shared/catalog/CatalogResult;Ljava/lang/Runnable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;",
            ">;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .line 67
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;

    .line 68
    iget-object v0, p1, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;->taxes:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 69
    iget-object v1, p1, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;->taxRules:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 70
    iget-object p1, p1, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;->discounts:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    .line 71
    iget-object v2, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v3, Lcom/squareup/settings/server/FeesUpdate;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v1, p1, v4}, Lcom/squareup/settings/server/FeesUpdate;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)V

    invoke-interface {v2, v3}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    if-eqz p2, :cond_0

    .line 72
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method public static synthetic lambda$Tbrl83bGEIyLVymknco29T8cshI(Lcom/squareup/catalogfees/RealCatalogFeesPreloader;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/catalogfees/RealCatalogFeesPreloader;->loadInBackground(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$loadAndPost$0()V
    .locals 0

    return-void
.end method

.method private loadInBackground(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;
    .locals 3

    .line 57
    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllTaxes()Ljava/util/List;

    move-result-object v0

    .line 58
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 59
    iget-object v2, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldUseConditionalTaxes()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllTaxRules()Ljava/util/List;

    move-result-object v1

    .line 62
    :cond_0
    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllFixedDiscounts()Ljava/util/List;

    move-result-object p1

    .line 63
    new-instance v2, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;

    invoke-direct {v2, v0, v1, p1}, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v2
.end method


# virtual methods
.method public loadAndPost()V
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/catalogfees/-$$Lambda$RealCatalogFeesPreloader$5x3QTpnikM0Akei2j0VlwwpTLRI;->INSTANCE:Lcom/squareup/catalogfees/-$$Lambda$RealCatalogFeesPreloader$5x3QTpnikM0Akei2j0VlwwpTLRI;

    invoke-virtual {p0, v0}, Lcom/squareup/catalogfees/RealCatalogFeesPreloader;->loadAndPost(Ljava/lang/Runnable;)V

    return-void
.end method

.method public loadAndPost(Ljava/lang/Runnable;)V
    .locals 2

    .line 43
    new-instance v0, Lcom/squareup/catalogfees/-$$Lambda$RealCatalogFeesPreloader$Tbrl83bGEIyLVymknco29T8cshI;

    invoke-direct {v0, p0}, Lcom/squareup/catalogfees/-$$Lambda$RealCatalogFeesPreloader$Tbrl83bGEIyLVymknco29T8cshI;-><init>(Lcom/squareup/catalogfees/RealCatalogFeesPreloader;)V

    .line 44
    new-instance v1, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$1;-><init>(Lcom/squareup/catalogfees/RealCatalogFeesPreloader;Ljava/lang/Runnable;)V

    .line 53
    iget-object p1, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cogs/Cogs;

    invoke-interface {p1, v0, v1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method
