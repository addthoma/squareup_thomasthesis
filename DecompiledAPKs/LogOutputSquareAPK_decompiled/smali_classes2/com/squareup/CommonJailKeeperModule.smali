.class public abstract Lcom/squareup/CommonJailKeeperModule;
.super Ljava/lang/Object;
.source "CommonJailKeeperModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/jailkeeper/JailKeeperServicesModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideTileAppearanceSettingsAsJailKeeperService(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)Lcom/squareup/jailkeeper/JailKeeperService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/CommonJailKeeperModule$1;

    invoke-direct {v0, p0}, Lcom/squareup/CommonJailKeeperModule$1;-><init>(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V

    return-object v0
.end method


# virtual methods
.method public abstract bindEmployeeCacheUpdaterAsJailKeeperService(Lcom/squareup/permissions/EmployeeCacheUpdater;)Lcom/squareup/jailkeeper/JailKeeperService;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
