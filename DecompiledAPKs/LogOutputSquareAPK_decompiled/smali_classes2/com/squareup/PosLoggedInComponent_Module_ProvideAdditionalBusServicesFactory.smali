.class public final Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;
.super Ljava/lang/Object;
.source "PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;",
        ">;"
    }
.end annotation


# instance fields
.field private final paymentNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PaymentNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private final printerScoutsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterScoutsProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketAutoIdentifiersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/RealTicketAutoIdentifiers;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterScoutsProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PaymentNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/RealTicketAutoIdentifiers;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;->printerScoutsProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;->transactionProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;->paymentNotifierProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;->ticketAutoIdentifiersProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterScoutsProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PaymentNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/RealTicketAutoIdentifiers;",
            ">;)",
            "Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideAdditionalBusServices(Lcom/squareup/print/PrinterScoutsProvider;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/pending/PaymentNotifier;Lcom/squareup/print/RealTicketAutoIdentifiers;)Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;
    .locals 0

    .line 54
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/PosLoggedInComponent$Module;->provideAdditionalBusServices(Lcom/squareup/print/PrinterScoutsProvider;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/pending/PaymentNotifier;Lcom/squareup/print/RealTicketAutoIdentifiers;)Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;
    .locals 4

    .line 41
    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;->printerScoutsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/PrinterScoutsProvider;

    iget-object v1, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/Transaction;

    iget-object v2, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;->paymentNotifierProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/pending/PaymentNotifier;

    iget-object v3, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;->ticketAutoIdentifiersProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/print/RealTicketAutoIdentifiers;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;->provideAdditionalBusServices(Lcom/squareup/print/PrinterScoutsProvider;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/pending/PaymentNotifier;Lcom/squareup/print/RealTicketAutoIdentifiers;)Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalBusServicesFactory;->get()Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;

    move-result-object v0

    return-object v0
.end method
