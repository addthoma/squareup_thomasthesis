.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/activity/IssueReceiptScreen$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "IRS2_ComponentImpl"
.end annotation


# instance fields
.field private issueReceiptScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/IssueReceiptScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;)V
    .locals 0

    .line 61609
    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61611
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->initialize()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 61606
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;)V

    return-void
.end method

.method private initialize()V
    .locals 14

    .line 61616
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->access$219400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$54800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$10700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object v5

    invoke-static {}, Lcom/squareup/thread/MainThreadModule_ProvideMainThreadEnforcerFactory;->create()Lcom/squareup/thread/MainThreadModule_ProvideMainThreadEnforcerFactory;

    move-result-object v6

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$17600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$21000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v8

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$27900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v9

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$53600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v10

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v11

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$14200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v12

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$22000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v13

    invoke-static/range {v1 .. v13}, Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/IssueReceiptScreenRunner_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->issueReceiptScreenRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public customLocale()Lcom/squareup/buyer/language/BuyerLocaleOverride;
    .locals 1

    .line 61633
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$22000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-object v0
.end method

.method public inject(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V
    .locals 0

    return-void
.end method

.method public issueReceiptCoordinatorFactory()Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;
    .locals 5

    .line 61625
    new-instance v0, Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$21300(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/text/scrubber/PhoneNumberScrubber;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$10700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$22000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;-><init>(Lcom/squareup/util/Device;Lcom/squareup/text/InsertingScrubber;Ljavax/inject/Provider;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V

    return-object v0
.end method

.method public issueReceiptScreenRunner()Lcom/squareup/ui/activity/IssueReceiptScreenRunner;
    .locals 1

    .line 61621
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$EISV2_ComponentImpl$ViewCustomerFromServicesComponentImpl$IRS2_ComponentImpl;->issueReceiptScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/IssueReceiptScreenRunner;

    return-object v0
.end method
