.class public final Lcom/squareup/adjustinventory/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/adjustinventory/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final adjust_inventory_saving:I = 0x7f1200a8

.field public static final adjust_stock:I = 0x7f1200a9

.field public static final adjust_stock_error_message_add_stock:I = 0x7f1200aa

.field public static final adjust_stock_error_message_recount_stock:I = 0x7f1200ab

.field public static final adjust_stock_error_message_remove_stock:I = 0x7f1200ac

.field public static final adjust_stock_error_title_add_stock:I = 0x7f1200ad

.field public static final adjust_stock_error_title_recount_stock:I = 0x7f1200ae

.field public static final adjust_stock_error_title_remove_stock:I = 0x7f1200af

.field public static final adjust_stock_help_text:I = 0x7f1200b0

.field public static final adjust_stock_reason_damage:I = 0x7f1200b1

.field public static final adjust_stock_reason_loss:I = 0x7f1200b2

.field public static final adjust_stock_reason_receive:I = 0x7f1200b3

.field public static final adjust_stock_reason_recount:I = 0x7f1200b4

.field public static final adjust_stock_reason_return:I = 0x7f1200b5

.field public static final adjust_stock_reason_theft:I = 0x7f1200b6

.field public static final adjust_stock_select_reason_uppercase:I = 0x7f1200b7

.field public static final adjust_stock_specify_number_add_stock:I = 0x7f1200b8

.field public static final adjust_stock_specify_number_help_text_new_stock:I = 0x7f1200b9

.field public static final adjust_stock_specify_number_help_text_previous_stock:I = 0x7f1200ba

.field public static final adjust_stock_specify_number_in_stock:I = 0x7f1200bb

.field public static final adjust_stock_specify_number_remove_stock:I = 0x7f1200bc

.field public static final adjust_stock_specify_number_unit_cost:I = 0x7f1200bd


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
