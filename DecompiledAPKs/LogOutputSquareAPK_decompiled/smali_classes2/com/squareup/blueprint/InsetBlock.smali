.class public final Lcom/squareup/blueprint/InsetBlock;
.super Lcom/squareup/blueprint/Block;
.source "InsetBlock.kt"

# interfaces
.implements Lcom/squareup/blueprint/BlueprintContext;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C:",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/blueprint/Block<",
        "TC;TP;>;",
        "Lcom/squareup/blueprint/BlueprintContext<",
        "TC;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInsetBlock.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InsetBlock.kt\ncom/squareup/blueprint/InsetBlock\n*L\n1#1,132:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0014\n\u0002\u0010\u000b\n\u0002\u0008\u0018\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u00052\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u00020\u00070\u0006Be\u0012\u0006\u0010\u0008\u001a\u00028\u0001\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\n\u0012\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\n\u0012\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\n\u0012\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\n\u0012\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\n\u0012\u0016\u0008\u0002\u0010\u0010\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0011J\u001c\u00104\u001a\u00020\u00072\u0012\u0010$\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00070\u0005H\u0016J%\u00105\u001a\u00020\u00072\u0006\u00106\u001a\u00028\u00002\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u000208H\u0016\u00a2\u0006\u0002\u0010:J \u0010;\u001a\u0002082\u0006\u0010<\u001a\u00020\u00022\u0006\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u000208H\u0016J \u0010@\u001a\u0002082\u0006\u0010<\u001a\u00020\u00022\u0006\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u000208H\u0016J\u000e\u0010A\u001a\u00028\u0001H\u00c6\u0003\u00a2\u0006\u0002\u0010,J\t\u0010B\u001a\u00020\nH\u00c6\u0003J\t\u0010C\u001a\u00020\nH\u00c6\u0003J\u000b\u0010D\u001a\u0004\u0018\u00010\nH\u00c6\u0003J\u000b\u0010E\u001a\u0004\u0018\u00010\nH\u00c6\u0003J\u000b\u0010F\u001a\u0004\u0018\u00010\nH\u00c6\u0003J\u000b\u0010G\u001a\u0004\u0018\u00010\nH\u00c6\u0003J\u0017\u0010H\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005H\u00c2\u0003J \u0010I\u001a\u00020J2\u0006\u0010<\u001a\u00020\u00022\u0006\u0010K\u001a\u00020J2\u0006\u0010L\u001a\u00020MH\u0016J \u0010N\u001a\u00020J2\u0006\u0010<\u001a\u00020\u00022\u0006\u0010K\u001a\u00020J2\u0006\u0010L\u001a\u00020OH\u0016J\u0080\u0001\u0010P\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0008\u0008\u0002\u0010\u0008\u001a\u00028\u00012\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\n2\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\n2\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\n2\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\n2\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\n2\u0016\u0008\u0002\u0010\u0010\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005H\u00c6\u0001\u00a2\u0006\u0002\u0010QJ\u0008\u0010R\u001a\u00020\u0007H\u0016J\u0013\u0010S\u001a\u00020\u001f2\u0008\u0010T\u001a\u0004\u0018\u00010\u0004H\u00d6\u0003J\t\u0010U\u001a\u000208H\u00d6\u0001J\u0018\u0010V\u001a\u00020\u001f2\u000e\u0010W\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0005H\u0016J\u0010\u0010X\u001a\u00020J2\u0006\u0010<\u001a\u00020\u0002H\u0016J\t\u0010Y\u001a\u00020ZH\u00d6\u0001J\u0010\u0010[\u001a\u00020J2\u0006\u0010<\u001a\u00020\u0002H\u0016J\u0014\u0010\\\u001a\u000208*\u00020\n2\u0006\u00106\u001a\u00020\u0002H\u0002R\u001c\u0010\u000f\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u0014\u0010\u0016\u001a\u00020\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0013R\u0014\u0010\u0018\u001a\u00020\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u0013R\u0014\u0010\u001a\u001a\u00020\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u0013R\u0014\u0010\u001c\u001a\u00020\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001d\u0010\u0013R\u0014\u0010\u001e\u001a\u00020\u001f8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008 \u0010!R\u0014\u0010\"\u001a\u00020\u001f8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008#\u0010!R\u001d\u0010$\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00070\u00058F\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010&R\u001c\u0010\r\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\'\u0010\u0013\"\u0004\u0008(\u0010\u0015R\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008)\u0010\u0013\"\u0004\u0008*\u0010\u0015R\u001c\u0010\u0010\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\u00028\u0001X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010-\u001a\u0004\u0008+\u0010,R\u001c\u0010\u000c\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008.\u0010\u0013\"\u0004\u0008/\u0010\u0015R\u001c\u0010\u000e\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00080\u0010\u0013\"\u0004\u00081\u0010\u0015R\u001a\u0010\u000b\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00082\u0010\u0013\"\u0004\u00083\u0010\u0015\u00a8\u0006]"
    }
    d2 = {
        "Lcom/squareup/blueprint/InsetBlock;",
        "C",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P",
        "",
        "Lcom/squareup/blueprint/Block;",
        "Lcom/squareup/blueprint/BlueprintContext;",
        "",
        "params",
        "horizontalInset",
        "Lcom/squareup/resources/DimenModel;",
        "verticalInset",
        "startInset",
        "endInset",
        "topInset",
        "bottomInset",
        "initialElement",
        "(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/Block;)V",
        "getBottomInset",
        "()Lcom/squareup/resources/DimenModel;",
        "setBottomInset",
        "(Lcom/squareup/resources/DimenModel;)V",
        "concreteBottomInset",
        "getConcreteBottomInset",
        "concreteEndInset",
        "getConcreteEndInset",
        "concreteStartInset",
        "getConcreteStartInset",
        "concreteTopInset",
        "getConcreteTopInset",
        "dependableHorizontally",
        "",
        "getDependableHorizontally",
        "()Z",
        "dependableVertically",
        "getDependableVertically",
        "element",
        "getElement",
        "()Lcom/squareup/blueprint/Block;",
        "getEndInset",
        "setEndInset",
        "getHorizontalInset",
        "setHorizontalInset",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "getStartInset",
        "setStartInset",
        "getTopInset",
        "setTopInset",
        "getVerticalInset",
        "setVerticalInset",
        "add",
        "buildViews",
        "updateContext",
        "width",
        "",
        "height",
        "(Lcom/squareup/blueprint/UpdateContext;II)V",
        "chainHorizontally",
        "context",
        "chainInfo",
        "Lcom/squareup/blueprint/ChainInfo;",
        "previousMargin",
        "chainVertically",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "connectHorizontally",
        "Lcom/squareup/blueprint/IdsAndMargins;",
        "previousIds",
        "align",
        "Lcom/squareup/blueprint/HorizontalAlign;",
        "connectVertically",
        "Lcom/squareup/blueprint/VerticalAlign;",
        "copy",
        "(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/Block;)Lcom/squareup/blueprint/InsetBlock;",
        "createParams",
        "equals",
        "other",
        "hashCode",
        "isSameLayoutAs",
        "block",
        "startIds",
        "toString",
        "",
        "topIds",
        "toOffset",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private bottomInset:Lcom/squareup/resources/DimenModel;

.field private endInset:Lcom/squareup/resources/DimenModel;

.field private horizontalInset:Lcom/squareup/resources/DimenModel;

.field private initialElement:Lcom/squareup/blueprint/Block;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private startInset:Lcom/squareup/resources/DimenModel;

.field private topInset:Lcom/squareup/resources/DimenModel;

.field private verticalInset:Lcom/squareup/resources/DimenModel;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/Block;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "horizontalInset"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "verticalInset"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Lcom/squareup/blueprint/Block;-><init>()V

    iput-object p1, p0, Lcom/squareup/blueprint/InsetBlock;->params:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/blueprint/InsetBlock;->horizontalInset:Lcom/squareup/resources/DimenModel;

    iput-object p3, p0, Lcom/squareup/blueprint/InsetBlock;->verticalInset:Lcom/squareup/resources/DimenModel;

    iput-object p4, p0, Lcom/squareup/blueprint/InsetBlock;->startInset:Lcom/squareup/resources/DimenModel;

    iput-object p5, p0, Lcom/squareup/blueprint/InsetBlock;->endInset:Lcom/squareup/resources/DimenModel;

    iput-object p6, p0, Lcom/squareup/blueprint/InsetBlock;->topInset:Lcom/squareup/resources/DimenModel;

    iput-object p7, p0, Lcom/squareup/blueprint/InsetBlock;->bottomInset:Lcom/squareup/resources/DimenModel;

    iput-object p8, p0, Lcom/squareup/blueprint/InsetBlock;->initialElement:Lcom/squareup/blueprint/Block;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 12

    move/from16 v0, p9

    and-int/lit8 v1, v0, 0x8

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 36
    move-object v1, v2

    check-cast v1, Lcom/squareup/resources/DimenModel;

    move-object v7, v1

    goto :goto_0

    :cond_0
    move-object/from16 v7, p4

    :goto_0
    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_1

    .line 37
    move-object v1, v2

    check-cast v1, Lcom/squareup/resources/DimenModel;

    move-object v8, v1

    goto :goto_1

    :cond_1
    move-object/from16 v8, p5

    :goto_1
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_2

    .line 38
    move-object v1, v2

    check-cast v1, Lcom/squareup/resources/DimenModel;

    move-object v9, v1

    goto :goto_2

    :cond_2
    move-object/from16 v9, p6

    :goto_2
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_3

    .line 39
    move-object v1, v2

    check-cast v1, Lcom/squareup/resources/DimenModel;

    move-object v10, v1

    goto :goto_3

    :cond_3
    move-object/from16 v10, p7

    :goto_3
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_4

    .line 40
    move-object v0, v2

    check-cast v0, Lcom/squareup/blueprint/Block;

    move-object v11, v0

    goto :goto_4

    :cond_4
    move-object/from16 v11, p8

    :goto_4
    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v3 .. v11}, Lcom/squareup/blueprint/InsetBlock;-><init>(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method private final component8()Lcom/squareup/blueprint/Block;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->initialElement:Lcom/squareup/blueprint/Block;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/blueprint/InsetBlock;Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/Block;ILjava/lang/Object;)Lcom/squareup/blueprint/InsetBlock;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getParams()Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/blueprint/InsetBlock;->horizontalInset:Lcom/squareup/resources/DimenModel;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/blueprint/InsetBlock;->verticalInset:Lcom/squareup/resources/DimenModel;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/blueprint/InsetBlock;->startInset:Lcom/squareup/resources/DimenModel;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/blueprint/InsetBlock;->endInset:Lcom/squareup/resources/DimenModel;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/blueprint/InsetBlock;->topInset:Lcom/squareup/resources/DimenModel;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/blueprint/InsetBlock;->bottomInset:Lcom/squareup/resources/DimenModel;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/blueprint/InsetBlock;->initialElement:Lcom/squareup/blueprint/Block;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/blueprint/InsetBlock;->copy(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/Block;)Lcom/squareup/blueprint/InsetBlock;

    move-result-object v0

    return-object v0
.end method

.method private final getConcreteBottomInset()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->bottomInset:Lcom/squareup/resources/DimenModel;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->verticalInset:Lcom/squareup/resources/DimenModel;

    :goto_0
    return-object v0
.end method

.method private final getConcreteEndInset()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->endInset:Lcom/squareup/resources/DimenModel;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->horizontalInset:Lcom/squareup/resources/DimenModel;

    :goto_0
    return-object v0
.end method

.method private final getConcreteStartInset()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->startInset:Lcom/squareup/resources/DimenModel;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->horizontalInset:Lcom/squareup/resources/DimenModel;

    :goto_0
    return-object v0
.end method

.method private final getConcreteTopInset()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->topInset:Lcom/squareup/resources/DimenModel;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->verticalInset:Lcom/squareup/resources/DimenModel;

    :goto_0
    return-object v0
.end method

.method private final toOffset(Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/UpdateContext;)I
    .locals 1

    .line 130
    invoke-virtual {p2}, Lcom/squareup/blueprint/UpdateContext;->getConstraintLayout()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object p2

    invoke-virtual {p2}, Landroidx/constraintlayout/widget/ConstraintLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v0, "updateContext.constraintLayout.context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/squareup/resources/DimenModel;->toOffset(Landroid/content/Context;)I

    move-result p1

    return p1
.end method


# virtual methods
.method public add(Lcom/squareup/blueprint/Block;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "element"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iput-object p1, p0, Lcom/squareup/blueprint/InsetBlock;->initialElement:Lcom/squareup/blueprint/Block;

    return-void
.end method

.method public buildViews(Lcom/squareup/blueprint/UpdateContext;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;II)V"
        }
    .end annotation

    const-string v0, "updateContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/blueprint/Block;->buildViews(Lcom/squareup/blueprint/UpdateContext;II)V

    return-void
.end method

.method public chainHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chainInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    .line 105
    invoke-direct {p0}, Lcom/squareup/blueprint/InsetBlock;->getConcreteStartInset()Lcom/squareup/resources/DimenModel;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/squareup/blueprint/InsetBlock;->toOffset(Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/UpdateContext;)I

    move-result v1

    add-int/2addr p3, v1

    .line 102
    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/blueprint/Block;->chainHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I

    move-result p2

    .line 107
    invoke-direct {p0}, Lcom/squareup/blueprint/InsetBlock;->getConcreteEndInset()Lcom/squareup/resources/DimenModel;

    move-result-object p3

    invoke-direct {p0, p3, p1}, Lcom/squareup/blueprint/InsetBlock;->toOffset(Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/UpdateContext;)I

    move-result p1

    add-int/2addr p2, p1

    return p2
.end method

.method public chainVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chainInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    .line 118
    invoke-direct {p0}, Lcom/squareup/blueprint/InsetBlock;->getConcreteTopInset()Lcom/squareup/resources/DimenModel;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/squareup/blueprint/InsetBlock;->toOffset(Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/UpdateContext;)I

    move-result v1

    add-int/2addr p3, v1

    .line 115
    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/blueprint/Block;->chainVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I

    move-result p2

    .line 120
    invoke-direct {p0}, Lcom/squareup/blueprint/InsetBlock;->getConcreteBottomInset()Lcom/squareup/resources/DimenModel;

    move-result-object p3

    invoke-direct {p0, p3, p1}, Lcom/squareup/blueprint/InsetBlock;->toOffset(Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/UpdateContext;)I

    move-result p1

    add-int/2addr p2, p1

    return p2
.end method

.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/resources/DimenModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->horizontalInset:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final component3()Lcom/squareup/resources/DimenModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->verticalInset:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final component4()Lcom/squareup/resources/DimenModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->startInset:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final component5()Lcom/squareup/resources/DimenModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->endInset:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final component6()Lcom/squareup/resources/DimenModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->topInset:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final component7()Lcom/squareup/resources/DimenModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->bottomInset:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previousIds"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "align"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-direct {p0}, Lcom/squareup/blueprint/InsetBlock;->getConcreteStartInset()Lcom/squareup/resources/DimenModel;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/squareup/blueprint/InsetBlock;->toOffset(Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/UpdateContext;)I

    move-result v0

    invoke-interface {p2, v0}, Lcom/squareup/blueprint/IdsAndMargins;->withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    .line 82
    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    .line 83
    invoke-direct {p0}, Lcom/squareup/blueprint/InsetBlock;->getConcreteEndInset()Lcom/squareup/resources/DimenModel;

    move-result-object p3

    invoke-direct {p0, p3, p1}, Lcom/squareup/blueprint/InsetBlock;->toOffset(Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/UpdateContext;)I

    move-result p1

    invoke-interface {p2, p1}, Lcom/squareup/blueprint/IdsAndMargins;->withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p1

    return-object p1
.end method

.method public connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previousIds"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "align"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-direct {p0}, Lcom/squareup/blueprint/InsetBlock;->getConcreteTopInset()Lcom/squareup/resources/DimenModel;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/squareup/blueprint/InsetBlock;->toOffset(Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/UpdateContext;)I

    move-result v0

    invoke-interface {p2, v0}, Lcom/squareup/blueprint/IdsAndMargins;->withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    .line 92
    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    .line 93
    invoke-direct {p0}, Lcom/squareup/blueprint/InsetBlock;->getConcreteBottomInset()Lcom/squareup/resources/DimenModel;

    move-result-object p3

    invoke-direct {p0, p3, p1}, Lcom/squareup/blueprint/InsetBlock;->toOffset(Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/UpdateContext;)I

    move-result p1

    invoke-interface {p2, p1}, Lcom/squareup/blueprint/IdsAndMargins;->withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p1

    return-object p1
.end method

.method public final copy(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/Block;)Lcom/squareup/blueprint/InsetBlock;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/blueprint/InsetBlock<",
            "TC;TP;>;"
        }
    .end annotation

    const-string v0, "params"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "horizontalInset"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "verticalInset"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/blueprint/InsetBlock;

    move-object v1, v0

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/squareup/blueprint/InsetBlock;-><init>(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/Block;)V

    return-object v0
.end method

.method public bridge synthetic createParams()Ljava/lang/Object;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->createParams()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public createParams()V
    .locals 0

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/blueprint/InsetBlock;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/blueprint/InsetBlock;

    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/InsetBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->horizontalInset:Lcom/squareup/resources/DimenModel;

    iget-object v1, p1, Lcom/squareup/blueprint/InsetBlock;->horizontalInset:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->verticalInset:Lcom/squareup/resources/DimenModel;

    iget-object v1, p1, Lcom/squareup/blueprint/InsetBlock;->verticalInset:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->startInset:Lcom/squareup/resources/DimenModel;

    iget-object v1, p1, Lcom/squareup/blueprint/InsetBlock;->startInset:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->endInset:Lcom/squareup/resources/DimenModel;

    iget-object v1, p1, Lcom/squareup/blueprint/InsetBlock;->endInset:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->topInset:Lcom/squareup/resources/DimenModel;

    iget-object v1, p1, Lcom/squareup/blueprint/InsetBlock;->topInset:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->bottomInset:Lcom/squareup/resources/DimenModel;

    iget-object v1, p1, Lcom/squareup/blueprint/InsetBlock;->bottomInset:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->initialElement:Lcom/squareup/blueprint/Block;

    iget-object p1, p1, Lcom/squareup/blueprint/InsetBlock;->initialElement:Lcom/squareup/blueprint/Block;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBottomInset()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->bottomInset:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public getDependableHorizontally()Z
    .locals 1

    .line 72
    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/blueprint/Block;->getDependableHorizontally()Z

    move-result v0

    return v0
.end method

.method public getDependableVertically()Z
    .locals 1

    .line 73
    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/blueprint/Block;->getDependableVertically()Z

    move-result v0

    return v0
.end method

.method public final getElement()Lcom/squareup/blueprint/Block;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->initialElement:Lcom/squareup/blueprint/Block;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0
.end method

.method public final getEndInset()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->endInset:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final getHorizontalInset()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->horizontalInset:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public final getStartInset()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->startInset:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final getTopInset()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->topInset:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final getVerticalInset()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->verticalInset:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/blueprint/InsetBlock;->horizontalInset:Lcom/squareup/resources/DimenModel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/blueprint/InsetBlock;->verticalInset:Lcom/squareup/resources/DimenModel;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/blueprint/InsetBlock;->startInset:Lcom/squareup/resources/DimenModel;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/blueprint/InsetBlock;->endInset:Lcom/squareup/resources/DimenModel;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/blueprint/InsetBlock;->topInset:Lcom/squareup/resources/DimenModel;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/blueprint/InsetBlock;->bottomInset:Lcom/squareup/resources/DimenModel;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/blueprint/InsetBlock;->initialElement:Lcom/squareup/blueprint/Block;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "**>;)Z"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-super {p0, p1}, Lcom/squareup/blueprint/Block;->isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/blueprint/InsetBlock;

    .line 59
    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->horizontalInset:Lcom/squareup/resources/DimenModel;

    iget-object v3, p1, Lcom/squareup/blueprint/InsetBlock;->horizontalInset:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->verticalInset:Lcom/squareup/resources/DimenModel;

    iget-object v3, p1, Lcom/squareup/blueprint/InsetBlock;->verticalInset:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->startInset:Lcom/squareup/resources/DimenModel;

    iget-object v3, p1, Lcom/squareup/blueprint/InsetBlock;->startInset:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->endInset:Lcom/squareup/resources/DimenModel;

    iget-object v3, p1, Lcom/squareup/blueprint/InsetBlock;->endInset:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->topInset:Lcom/squareup/resources/DimenModel;

    iget-object v3, p1, Lcom/squareup/blueprint/InsetBlock;->topInset:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/InsetBlock;->bottomInset:Lcom/squareup/resources/DimenModel;

    iget-object v3, p1, Lcom/squareup/blueprint/InsetBlock;->bottomInset:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/InsetBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/Block;->isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public final setBottomInset(Lcom/squareup/resources/DimenModel;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/blueprint/InsetBlock;->bottomInset:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public final setEndInset(Lcom/squareup/resources/DimenModel;)V
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/squareup/blueprint/InsetBlock;->endInset:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public final setHorizontalInset(Lcom/squareup/resources/DimenModel;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iput-object p1, p0, Lcom/squareup/blueprint/InsetBlock;->horizontalInset:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public final setStartInset(Lcom/squareup/resources/DimenModel;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/blueprint/InsetBlock;->startInset:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public final setTopInset(Lcom/squareup/resources/DimenModel;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/blueprint/InsetBlock;->topInset:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public final setVerticalInset(Lcom/squareup/resources/DimenModel;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lcom/squareup/blueprint/InsetBlock;->verticalInset:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/Block;->startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/blueprint/InsetBlock;->getConcreteStartInset()Lcom/squareup/resources/DimenModel;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/squareup/blueprint/InsetBlock;->toOffset(Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/UpdateContext;)I

    move-result p1

    invoke-interface {v0, p1}, Lcom/squareup/blueprint/IdsAndMargins;->withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InsetBlock(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", horizontalInset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/blueprint/InsetBlock;->horizontalInset:Lcom/squareup/resources/DimenModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", verticalInset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/blueprint/InsetBlock;->verticalInset:Lcom/squareup/resources/DimenModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", startInset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/blueprint/InsetBlock;->startInset:Lcom/squareup/resources/DimenModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", endInset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/blueprint/InsetBlock;->endInset:Lcom/squareup/resources/DimenModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", topInset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/blueprint/InsetBlock;->topInset:Lcom/squareup/resources/DimenModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", bottomInset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/blueprint/InsetBlock;->bottomInset:Lcom/squareup/resources/DimenModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", initialElement="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/blueprint/InsetBlock;->initialElement:Lcom/squareup/blueprint/Block;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0}, Lcom/squareup/blueprint/InsetBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/Block;->topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/blueprint/InsetBlock;->getConcreteTopInset()Lcom/squareup/resources/DimenModel;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/squareup/blueprint/InsetBlock;->toOffset(Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/UpdateContext;)I

    move-result p1

    invoke-interface {v0, p1}, Lcom/squareup/blueprint/IdsAndMargins;->withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p1

    return-object p1
.end method
