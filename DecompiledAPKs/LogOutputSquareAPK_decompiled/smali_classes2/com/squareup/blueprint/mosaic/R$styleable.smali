.class public final Lcom/squareup/blueprint/mosaic/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/blueprint/mosaic/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final ColorStateListItem:[I

.field public static final ColorStateListItem_alpha:I = 0x2

.field public static final ColorStateListItem_android_alpha:I = 0x1

.field public static final ColorStateListItem_android_color:I = 0x0

.field public static final ConstraintLayout_Layout:[I

.field public static final ConstraintLayout_Layout_android_maxHeight:I = 0x2

.field public static final ConstraintLayout_Layout_android_maxWidth:I = 0x1

.field public static final ConstraintLayout_Layout_android_minHeight:I = 0x4

.field public static final ConstraintLayout_Layout_android_minWidth:I = 0x3

.field public static final ConstraintLayout_Layout_android_orientation:I = 0x0

.field public static final ConstraintLayout_Layout_barrierAllowsGoneWidgets:I = 0x5

.field public static final ConstraintLayout_Layout_barrierDirection:I = 0x6

.field public static final ConstraintLayout_Layout_chainUseRtl:I = 0x7

.field public static final ConstraintLayout_Layout_constraintSet:I = 0x8

.field public static final ConstraintLayout_Layout_constraint_referenced_ids:I = 0x9

.field public static final ConstraintLayout_Layout_layout_constrainedHeight:I = 0xa

.field public static final ConstraintLayout_Layout_layout_constrainedWidth:I = 0xb

.field public static final ConstraintLayout_Layout_layout_constraintBaseline_creator:I = 0xc

.field public static final ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf:I = 0xd

.field public static final ConstraintLayout_Layout_layout_constraintBottom_creator:I = 0xe

.field public static final ConstraintLayout_Layout_layout_constraintBottom_toBottomOf:I = 0xf

.field public static final ConstraintLayout_Layout_layout_constraintBottom_toTopOf:I = 0x10

.field public static final ConstraintLayout_Layout_layout_constraintCircle:I = 0x11

.field public static final ConstraintLayout_Layout_layout_constraintCircleAngle:I = 0x12

.field public static final ConstraintLayout_Layout_layout_constraintCircleRadius:I = 0x13

.field public static final ConstraintLayout_Layout_layout_constraintDimensionRatio:I = 0x14

.field public static final ConstraintLayout_Layout_layout_constraintEnd_toEndOf:I = 0x15

.field public static final ConstraintLayout_Layout_layout_constraintEnd_toStartOf:I = 0x16

.field public static final ConstraintLayout_Layout_layout_constraintGuide_begin:I = 0x17

.field public static final ConstraintLayout_Layout_layout_constraintGuide_end:I = 0x18

.field public static final ConstraintLayout_Layout_layout_constraintGuide_percent:I = 0x19

.field public static final ConstraintLayout_Layout_layout_constraintHeight_default:I = 0x1a

.field public static final ConstraintLayout_Layout_layout_constraintHeight_max:I = 0x1b

.field public static final ConstraintLayout_Layout_layout_constraintHeight_min:I = 0x1c

.field public static final ConstraintLayout_Layout_layout_constraintHeight_percent:I = 0x1d

.field public static final ConstraintLayout_Layout_layout_constraintHorizontal_bias:I = 0x1e

.field public static final ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle:I = 0x1f

.field public static final ConstraintLayout_Layout_layout_constraintHorizontal_weight:I = 0x20

.field public static final ConstraintLayout_Layout_layout_constraintLeft_creator:I = 0x21

.field public static final ConstraintLayout_Layout_layout_constraintLeft_toLeftOf:I = 0x22

.field public static final ConstraintLayout_Layout_layout_constraintLeft_toRightOf:I = 0x23

.field public static final ConstraintLayout_Layout_layout_constraintRight_creator:I = 0x24

.field public static final ConstraintLayout_Layout_layout_constraintRight_toLeftOf:I = 0x25

.field public static final ConstraintLayout_Layout_layout_constraintRight_toRightOf:I = 0x26

.field public static final ConstraintLayout_Layout_layout_constraintStart_toEndOf:I = 0x27

.field public static final ConstraintLayout_Layout_layout_constraintStart_toStartOf:I = 0x28

.field public static final ConstraintLayout_Layout_layout_constraintTop_creator:I = 0x29

.field public static final ConstraintLayout_Layout_layout_constraintTop_toBottomOf:I = 0x2a

.field public static final ConstraintLayout_Layout_layout_constraintTop_toTopOf:I = 0x2b

.field public static final ConstraintLayout_Layout_layout_constraintVertical_bias:I = 0x2c

.field public static final ConstraintLayout_Layout_layout_constraintVertical_chainStyle:I = 0x2d

.field public static final ConstraintLayout_Layout_layout_constraintVertical_weight:I = 0x2e

.field public static final ConstraintLayout_Layout_layout_constraintWidth_default:I = 0x2f

.field public static final ConstraintLayout_Layout_layout_constraintWidth_max:I = 0x30

.field public static final ConstraintLayout_Layout_layout_constraintWidth_min:I = 0x31

.field public static final ConstraintLayout_Layout_layout_constraintWidth_percent:I = 0x32

.field public static final ConstraintLayout_Layout_layout_editor_absoluteX:I = 0x33

.field public static final ConstraintLayout_Layout_layout_editor_absoluteY:I = 0x34

.field public static final ConstraintLayout_Layout_layout_goneMarginBottom:I = 0x35

.field public static final ConstraintLayout_Layout_layout_goneMarginEnd:I = 0x36

.field public static final ConstraintLayout_Layout_layout_goneMarginLeft:I = 0x37

.field public static final ConstraintLayout_Layout_layout_goneMarginRight:I = 0x38

.field public static final ConstraintLayout_Layout_layout_goneMarginStart:I = 0x39

.field public static final ConstraintLayout_Layout_layout_goneMarginTop:I = 0x3a

.field public static final ConstraintLayout_Layout_layout_optimizationLevel:I = 0x3b

.field public static final ConstraintLayout_placeholder:[I

.field public static final ConstraintLayout_placeholder_content:I = 0x0

.field public static final ConstraintLayout_placeholder_emptyVisibility:I = 0x1

.field public static final ConstraintSet:[I

.field public static final ConstraintSet_android_alpha:I = 0xd

.field public static final ConstraintSet_android_elevation:I = 0x1a

.field public static final ConstraintSet_android_id:I = 0x1

.field public static final ConstraintSet_android_layout_height:I = 0x4

.field public static final ConstraintSet_android_layout_marginBottom:I = 0x8

.field public static final ConstraintSet_android_layout_marginEnd:I = 0x18

.field public static final ConstraintSet_android_layout_marginLeft:I = 0x5

.field public static final ConstraintSet_android_layout_marginRight:I = 0x7

.field public static final ConstraintSet_android_layout_marginStart:I = 0x17

.field public static final ConstraintSet_android_layout_marginTop:I = 0x6

.field public static final ConstraintSet_android_layout_width:I = 0x3

.field public static final ConstraintSet_android_maxHeight:I = 0xa

.field public static final ConstraintSet_android_maxWidth:I = 0x9

.field public static final ConstraintSet_android_minHeight:I = 0xc

.field public static final ConstraintSet_android_minWidth:I = 0xb

.field public static final ConstraintSet_android_orientation:I = 0x0

.field public static final ConstraintSet_android_rotation:I = 0x14

.field public static final ConstraintSet_android_rotationX:I = 0x15

.field public static final ConstraintSet_android_rotationY:I = 0x16

.field public static final ConstraintSet_android_scaleX:I = 0x12

.field public static final ConstraintSet_android_scaleY:I = 0x13

.field public static final ConstraintSet_android_transformPivotX:I = 0xe

.field public static final ConstraintSet_android_transformPivotY:I = 0xf

.field public static final ConstraintSet_android_translationX:I = 0x10

.field public static final ConstraintSet_android_translationY:I = 0x11

.field public static final ConstraintSet_android_translationZ:I = 0x19

.field public static final ConstraintSet_android_visibility:I = 0x2

.field public static final ConstraintSet_barrierAllowsGoneWidgets:I = 0x1b

.field public static final ConstraintSet_barrierDirection:I = 0x1c

.field public static final ConstraintSet_chainUseRtl:I = 0x1d

.field public static final ConstraintSet_constraint_referenced_ids:I = 0x1e

.field public static final ConstraintSet_layout_constrainedHeight:I = 0x1f

.field public static final ConstraintSet_layout_constrainedWidth:I = 0x20

.field public static final ConstraintSet_layout_constraintBaseline_creator:I = 0x21

.field public static final ConstraintSet_layout_constraintBaseline_toBaselineOf:I = 0x22

.field public static final ConstraintSet_layout_constraintBottom_creator:I = 0x23

.field public static final ConstraintSet_layout_constraintBottom_toBottomOf:I = 0x24

.field public static final ConstraintSet_layout_constraintBottom_toTopOf:I = 0x25

.field public static final ConstraintSet_layout_constraintCircle:I = 0x26

.field public static final ConstraintSet_layout_constraintCircleAngle:I = 0x27

.field public static final ConstraintSet_layout_constraintCircleRadius:I = 0x28

.field public static final ConstraintSet_layout_constraintDimensionRatio:I = 0x29

.field public static final ConstraintSet_layout_constraintEnd_toEndOf:I = 0x2a

.field public static final ConstraintSet_layout_constraintEnd_toStartOf:I = 0x2b

.field public static final ConstraintSet_layout_constraintGuide_begin:I = 0x2c

.field public static final ConstraintSet_layout_constraintGuide_end:I = 0x2d

.field public static final ConstraintSet_layout_constraintGuide_percent:I = 0x2e

.field public static final ConstraintSet_layout_constraintHeight_default:I = 0x2f

.field public static final ConstraintSet_layout_constraintHeight_max:I = 0x30

.field public static final ConstraintSet_layout_constraintHeight_min:I = 0x31

.field public static final ConstraintSet_layout_constraintHeight_percent:I = 0x32

.field public static final ConstraintSet_layout_constraintHorizontal_bias:I = 0x33

.field public static final ConstraintSet_layout_constraintHorizontal_chainStyle:I = 0x34

.field public static final ConstraintSet_layout_constraintHorizontal_weight:I = 0x35

.field public static final ConstraintSet_layout_constraintLeft_creator:I = 0x36

.field public static final ConstraintSet_layout_constraintLeft_toLeftOf:I = 0x37

.field public static final ConstraintSet_layout_constraintLeft_toRightOf:I = 0x38

.field public static final ConstraintSet_layout_constraintRight_creator:I = 0x39

.field public static final ConstraintSet_layout_constraintRight_toLeftOf:I = 0x3a

.field public static final ConstraintSet_layout_constraintRight_toRightOf:I = 0x3b

.field public static final ConstraintSet_layout_constraintStart_toEndOf:I = 0x3c

.field public static final ConstraintSet_layout_constraintStart_toStartOf:I = 0x3d

.field public static final ConstraintSet_layout_constraintTop_creator:I = 0x3e

.field public static final ConstraintSet_layout_constraintTop_toBottomOf:I = 0x3f

.field public static final ConstraintSet_layout_constraintTop_toTopOf:I = 0x40

.field public static final ConstraintSet_layout_constraintVertical_bias:I = 0x41

.field public static final ConstraintSet_layout_constraintVertical_chainStyle:I = 0x42

.field public static final ConstraintSet_layout_constraintVertical_weight:I = 0x43

.field public static final ConstraintSet_layout_constraintWidth_default:I = 0x44

.field public static final ConstraintSet_layout_constraintWidth_max:I = 0x45

.field public static final ConstraintSet_layout_constraintWidth_min:I = 0x46

.field public static final ConstraintSet_layout_constraintWidth_percent:I = 0x47

.field public static final ConstraintSet_layout_editor_absoluteX:I = 0x48

.field public static final ConstraintSet_layout_editor_absoluteY:I = 0x49

.field public static final ConstraintSet_layout_goneMarginBottom:I = 0x4a

.field public static final ConstraintSet_layout_goneMarginEnd:I = 0x4b

.field public static final ConstraintSet_layout_goneMarginLeft:I = 0x4c

.field public static final ConstraintSet_layout_goneMarginRight:I = 0x4d

.field public static final ConstraintSet_layout_goneMarginStart:I = 0x4e

.field public static final ConstraintSet_layout_goneMarginTop:I = 0x4f

.field public static final CoordinatorLayout:[I

.field public static final CoordinatorLayout_Layout:[I

.field public static final CoordinatorLayout_Layout_android_layout_gravity:I = 0x0

.field public static final CoordinatorLayout_Layout_layout_anchor:I = 0x1

.field public static final CoordinatorLayout_Layout_layout_anchorGravity:I = 0x2

.field public static final CoordinatorLayout_Layout_layout_behavior:I = 0x3

.field public static final CoordinatorLayout_Layout_layout_dodgeInsetEdges:I = 0x4

.field public static final CoordinatorLayout_Layout_layout_insetEdge:I = 0x5

.field public static final CoordinatorLayout_Layout_layout_keyline:I = 0x6

.field public static final CoordinatorLayout_keylines:I = 0x0

.field public static final CoordinatorLayout_statusBarBackground:I = 0x1

.field public static final FontFamily:[I

.field public static final FontFamilyFont:[I

.field public static final FontFamilyFont_android_font:I = 0x0

.field public static final FontFamilyFont_android_fontStyle:I = 0x2

.field public static final FontFamilyFont_android_fontVariationSettings:I = 0x4

.field public static final FontFamilyFont_android_fontWeight:I = 0x1

.field public static final FontFamilyFont_android_ttcIndex:I = 0x3

.field public static final FontFamilyFont_font:I = 0x5

.field public static final FontFamilyFont_fontStyle:I = 0x6

.field public static final FontFamilyFont_fontVariationSettings:I = 0x7

.field public static final FontFamilyFont_fontWeight:I = 0x8

.field public static final FontFamilyFont_ttcIndex:I = 0x9

.field public static final FontFamily_fontProviderAuthority:I = 0x0

.field public static final FontFamily_fontProviderCerts:I = 0x1

.field public static final FontFamily_fontProviderFetchStrategy:I = 0x2

.field public static final FontFamily_fontProviderFetchTimeout:I = 0x3

.field public static final FontFamily_fontProviderPackage:I = 0x4

.field public static final FontFamily_fontProviderQuery:I = 0x5

.field public static final GradientColor:[I

.field public static final GradientColorItem:[I

.field public static final GradientColorItem_android_color:I = 0x0

.field public static final GradientColorItem_android_offset:I = 0x1

.field public static final GradientColor_android_centerColor:I = 0x7

.field public static final GradientColor_android_centerX:I = 0x3

.field public static final GradientColor_android_centerY:I = 0x4

.field public static final GradientColor_android_endColor:I = 0x1

.field public static final GradientColor_android_endX:I = 0xa

.field public static final GradientColor_android_endY:I = 0xb

.field public static final GradientColor_android_gradientRadius:I = 0x5

.field public static final GradientColor_android_startColor:I = 0x0

.field public static final GradientColor_android_startX:I = 0x8

.field public static final GradientColor_android_startY:I = 0x9

.field public static final GradientColor_android_tileMode:I = 0x6

.field public static final GradientColor_android_type:I = 0x2

.field public static final LinearConstraintLayout:[I

.field public static final LinearConstraintLayout_android_orientation:I = 0x0

.field public static final RecyclerView:[I

.field public static final RecyclerView_android_clipToPadding:I = 0x1

.field public static final RecyclerView_android_descendantFocusability:I = 0x2

.field public static final RecyclerView_android_orientation:I = 0x0

.field public static final RecyclerView_fastScrollEnabled:I = 0x3

.field public static final RecyclerView_fastScrollHorizontalThumbDrawable:I = 0x4

.field public static final RecyclerView_fastScrollHorizontalTrackDrawable:I = 0x5

.field public static final RecyclerView_fastScrollVerticalThumbDrawable:I = 0x6

.field public static final RecyclerView_fastScrollVerticalTrackDrawable:I = 0x7

.field public static final RecyclerView_layoutManager:I = 0x8

.field public static final RecyclerView_reverseLayout:I = 0x9

.field public static final RecyclerView_spanCount:I = 0xa

.field public static final RecyclerView_stackFromEnd:I = 0xb


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [I

    .line 243
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/blueprint/mosaic/R$styleable;->ColorStateListItem:[I

    const/16 v0, 0x3c

    new-array v0, v0, [I

    .line 247
    fill-array-data v0, :array_1

    sput-object v0, Lcom/squareup/blueprint/mosaic/R$styleable;->ConstraintLayout_Layout:[I

    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 308
    fill-array-data v1, :array_2

    sput-object v1, Lcom/squareup/blueprint/mosaic/R$styleable;->ConstraintLayout_placeholder:[I

    const/16 v1, 0x50

    new-array v1, v1, [I

    .line 311
    fill-array-data v1, :array_3

    sput-object v1, Lcom/squareup/blueprint/mosaic/R$styleable;->ConstraintSet:[I

    new-array v1, v0, [I

    .line 392
    fill-array-data v1, :array_4

    sput-object v1, Lcom/squareup/blueprint/mosaic/R$styleable;->CoordinatorLayout:[I

    const/4 v1, 0x7

    new-array v1, v1, [I

    .line 395
    fill-array-data v1, :array_5

    sput-object v1, Lcom/squareup/blueprint/mosaic/R$styleable;->CoordinatorLayout_Layout:[I

    const/4 v1, 0x6

    new-array v1, v1, [I

    .line 403
    fill-array-data v1, :array_6

    sput-object v1, Lcom/squareup/blueprint/mosaic/R$styleable;->FontFamily:[I

    const/16 v1, 0xa

    new-array v1, v1, [I

    .line 410
    fill-array-data v1, :array_7

    sput-object v1, Lcom/squareup/blueprint/mosaic/R$styleable;->FontFamilyFont:[I

    const/16 v1, 0xc

    new-array v2, v1, [I

    .line 421
    fill-array-data v2, :array_8

    sput-object v2, Lcom/squareup/blueprint/mosaic/R$styleable;->GradientColor:[I

    new-array v0, v0, [I

    .line 434
    fill-array-data v0, :array_9

    sput-object v0, Lcom/squareup/blueprint/mosaic/R$styleable;->GradientColorItem:[I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const v3, 0x10100c4

    aput v3, v0, v2

    .line 437
    sput-object v0, Lcom/squareup/blueprint/mosaic/R$styleable;->LinearConstraintLayout:[I

    new-array v0, v1, [I

    .line 439
    fill-array-data v0, :array_a

    sput-object v0, Lcom/squareup/blueprint/mosaic/R$styleable;->RecyclerView:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x10101a5
        0x101031f
        0x7f04002f
    .end array-data

    :array_1
    .array-data 4
        0x10100c4
        0x101011f
        0x1010120
        0x101013f
        0x1010140
        0x7f040056
        0x7f040057
        0x7f04009d
        0x7f0400ef
        0x7f0400f0
        0x7f04023f
        0x7f040240
        0x7f040241
        0x7f040242
        0x7f040243
        0x7f040244
        0x7f040245
        0x7f040246
        0x7f040247
        0x7f040248
        0x7f040249
        0x7f04024a
        0x7f04024b
        0x7f04024c
        0x7f04024d
        0x7f04024e
        0x7f04024f
        0x7f040250
        0x7f040251
        0x7f040252
        0x7f040253
        0x7f040254
        0x7f040255
        0x7f040256
        0x7f040257
        0x7f040258
        0x7f040259
        0x7f04025a
        0x7f04025b
        0x7f04025c
        0x7f04025d
        0x7f04025e
        0x7f04025f
        0x7f040260
        0x7f040261
        0x7f040262
        0x7f040263
        0x7f040264
        0x7f040265
        0x7f040266
        0x7f040267
        0x7f04026a
        0x7f04026b
        0x7f04026c
        0x7f04026d
        0x7f04026e
        0x7f04026f
        0x7f040270
        0x7f040271
        0x7f040275
    .end array-data

    :array_2
    .array-data 4
        0x7f0400f1
        0x7f040155
    .end array-data

    :array_3
    .array-data 4
        0x10100c4
        0x10100d0
        0x10100dc
        0x10100f4
        0x10100f5
        0x10100f7
        0x10100f8
        0x10100f9
        0x10100fa
        0x101011f
        0x1010120
        0x101013f
        0x1010140
        0x101031f
        0x1010320
        0x1010321
        0x1010322
        0x1010323
        0x1010324
        0x1010325
        0x1010326
        0x1010327
        0x1010328
        0x10103b5
        0x10103b6
        0x10103fa
        0x1010440
        0x7f040056
        0x7f040057
        0x7f04009d
        0x7f0400f0
        0x7f04023f
        0x7f040240
        0x7f040241
        0x7f040242
        0x7f040243
        0x7f040244
        0x7f040245
        0x7f040246
        0x7f040247
        0x7f040248
        0x7f040249
        0x7f04024a
        0x7f04024b
        0x7f04024c
        0x7f04024d
        0x7f04024e
        0x7f04024f
        0x7f040250
        0x7f040251
        0x7f040252
        0x7f040253
        0x7f040254
        0x7f040255
        0x7f040256
        0x7f040257
        0x7f040258
        0x7f040259
        0x7f04025a
        0x7f04025b
        0x7f04025c
        0x7f04025d
        0x7f04025e
        0x7f04025f
        0x7f040260
        0x7f040261
        0x7f040262
        0x7f040263
        0x7f040264
        0x7f040265
        0x7f040266
        0x7f040267
        0x7f04026a
        0x7f04026b
        0x7f04026c
        0x7f04026d
        0x7f04026e
        0x7f04026f
        0x7f040270
        0x7f040271
    .end array-data

    :array_4
    .array-data 4
        0x7f040233
        0x7f0403eb
    .end array-data

    :array_5
    .array-data 4
        0x10100b3
        0x7f04023a
        0x7f04023b
        0x7f04023c
        0x7f040268
        0x7f040272
        0x7f040274
    .end array-data

    :array_6
    .array-data 4
        0x7f04018d
        0x7f04018e
        0x7f04018f
        0x7f040190
        0x7f040191
        0x7f040192
    .end array-data

    :array_7
    .array-data 4
        0x1010532
        0x1010533
        0x101053f
        0x101056f
        0x1010570
        0x7f04018b
        0x7f040193
        0x7f040194
        0x7f040195
        0x7f04048b
    .end array-data

    :array_8
    .array-data 4
        0x101019d
        0x101019e
        0x10101a1
        0x10101a2
        0x10101a3
        0x10101a4
        0x1010201
        0x101020b
        0x1010510
        0x1010511
        0x1010512
        0x1010513
    .end array-data

    :array_9
    .array-data 4
        0x10101a5
        0x1010514
    .end array-data

    :array_a
    .array-data 4
        0x10100c4
        0x10100eb
        0x10100f1
        0x7f04017e
        0x7f04017f
        0x7f040180
        0x7f040181
        0x7f040182
        0x7f040239
        0x7f04032f
        0x7f04035d
        0x7f0403d9
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
