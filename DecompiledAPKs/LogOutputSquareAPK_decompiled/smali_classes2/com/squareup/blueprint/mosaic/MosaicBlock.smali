.class public final Lcom/squareup/blueprint/mosaic/MosaicBlock;
.super Lcom/squareup/blueprint/ViewBlock;
.source "MosaicBlock.kt"

# interfaces
.implements Lcom/squareup/mosaic/core/UiModelContext;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/blueprint/ViewBlock<",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
        "TP;>;",
        "Lcom/squareup/mosaic/core/UiModelContext<",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMosaicBlock.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MosaicBlock.kt\ncom/squareup/blueprint/mosaic/MosaicBlock\n*L\n1#1,65:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00010\u00032\u0008\u0012\u0004\u0012\u00020\u00060\u0005B\u001f\u0008\u0001\u0012\u0006\u0010\u0007\u001a\u00028\u0000\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u00a2\u0006\u0002\u0010\u000bJ\u0016\u0010\u001b\u001a\u00020\u001c2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0011H\u0016J \u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\r2\u0006\u0010 \u001a\u00020\rH\u0016J\u0008\u0010!\u001a\u00020\u0006H\u0016J\u0018\u0010\"\u001a\u00020\t2\u000e\u0010#\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030$H\u0016R\u0014\u0010\u000c\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0014\u001a\u0004\u0008\u0012\u0010\u0013R\u001a\u0010\u0008\u001a\u00020\tX\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\"\u0004\u0008\u0017\u0010\u0018R\u001a\u0010\n\u001a\u00020\tX\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u0016\"\u0004\u0008\u001a\u0010\u0018\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/blueprint/mosaic/MosaicBlock;",
        "P",
        "",
        "Lcom/squareup/blueprint/ViewBlock;",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
        "params",
        "wrapHorizontally",
        "",
        "wrapVertically",
        "(Ljava/lang/Object;ZZ)V",
        "id",
        "",
        "getId",
        "()I",
        "model",
        "Lcom/squareup/mosaic/core/UiModel;",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "getWrapHorizontally",
        "()Z",
        "setWrapHorizontally",
        "(Z)V",
        "getWrapVertically",
        "setWrapVertically",
        "add",
        "",
        "buildView",
        "updateContext",
        "width",
        "height",
        "createParams",
        "isSameLayoutAs",
        "block",
        "Lcom/squareup/blueprint/Block;",
        "blueprint-mosaic_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private model:Lcom/squareup/mosaic/core/UiModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
            ">;"
        }
    .end annotation
.end field

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private wrapHorizontally:Z

.field private wrapVertically:Z


# direct methods
.method public constructor <init>(Ljava/lang/Object;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;ZZ)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/squareup/blueprint/ViewBlock;-><init>()V

    iput-object p1, p0, Lcom/squareup/blueprint/mosaic/MosaicBlock;->params:Ljava/lang/Object;

    iput-boolean p2, p0, Lcom/squareup/blueprint/mosaic/MosaicBlock;->wrapHorizontally:Z

    iput-boolean p3, p0, Lcom/squareup/blueprint/mosaic/MosaicBlock;->wrapVertically:Z

    return-void
.end method


# virtual methods
.method public add(Lcom/squareup/mosaic/core/UiModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
            ">;)V"
        }
    .end annotation

    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iput-object p1, p0, Lcom/squareup/blueprint/mosaic/MosaicBlock;->model:Lcom/squareup/mosaic/core/UiModel;

    return-void
.end method

.method public bridge synthetic buildView(Lcom/squareup/blueprint/UpdateContext;II)V
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/blueprint/mosaic/MosaicBlock;->buildView(Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;II)V

    return-void
.end method

.method public buildView(Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;II)V
    .locals 2

    const-string v0, "updateContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicBlock;->model:Lcom/squareup/mosaic/core/UiModel;

    if-nez v0, :cond_0

    const-string v1, "model"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1, v0, p2, p3}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->addModel(Lcom/squareup/mosaic/core/UiModel;II)V

    return-void
.end method

.method public createParams()Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;
    .locals 2

    .line 59
    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;-><init>(I)V

    return-object v0
.end method

.method public bridge synthetic createParams()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/blueprint/mosaic/MosaicBlock;->createParams()Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicBlock;->model:Lcom/squareup/mosaic/core/UiModel;

    if-nez v0, :cond_0

    const-string v1, "model"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    invoke-virtual {v0}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->getViewId()I

    move-result v0

    return v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicBlock;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public getWrapHorizontally()Z
    .locals 1

    .line 32
    iget-boolean v0, p0, Lcom/squareup/blueprint/mosaic/MosaicBlock;->wrapHorizontally:Z

    return v0
.end method

.method public getWrapVertically()Z
    .locals 1

    .line 33
    iget-boolean v0, p0, Lcom/squareup/blueprint/mosaic/MosaicBlock;->wrapVertically:Z

    return v0
.end method

.method public isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "**>;)Z"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-super {p0, p1}, Lcom/squareup/blueprint/ViewBlock;->isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 45
    invoke-virtual {p0}, Lcom/squareup/blueprint/mosaic/MosaicBlock;->getWrapHorizontally()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/mosaic/MosaicBlock;->getWrapHorizontally()Z

    move-result v3

    if-ne v0, v3, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/mosaic/MosaicBlock;->getWrapVertically()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/mosaic/MosaicBlock;->getWrapVertically()Z

    move-result p1

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public setWrapHorizontally(Z)V
    .locals 0

    .line 32
    iput-boolean p1, p0, Lcom/squareup/blueprint/mosaic/MosaicBlock;->wrapHorizontally:Z

    return-void
.end method

.method public setWrapVertically(Z)V
    .locals 0

    .line 33
    iput-boolean p1, p0, Lcom/squareup/blueprint/mosaic/MosaicBlock;->wrapVertically:Z

    return-void
.end method
