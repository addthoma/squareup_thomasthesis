.class public final Lcom/squareup/blueprint/LinearBlock$extend$1;
.super Ljava/lang/Object;
.source "LinearBlock.kt"

# interfaces
.implements Lcom/squareup/blueprint/BlueprintContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/blueprint/LinearBlock;->extend(Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/blueprint/BlueprintContext<",
        "TC;",
        "Lcom/squareup/blueprint/LinearBlock$Params;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLinearBlock.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LinearBlock.kt\ncom/squareup/blueprint/LinearBlock$extend$1\n*L\n1#1,93:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00020\u0001J\u001c\u0010\u0003\u001a\u00020\u00042\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0002H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "com/squareup/blueprint/LinearBlock$extend$1",
        "Lcom/squareup/blueprint/BlueprintContext;",
        "Lcom/squareup/blueprint/LinearBlock$Params;",
        "add",
        "",
        "element",
        "Lcom/squareup/blueprint/Block;",
        "createParams",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/blueprint/LinearBlock;


# direct methods
.method constructor <init>(Lcom/squareup/blueprint/LinearBlock;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 78
    iput-object p1, p0, Lcom/squareup/blueprint/LinearBlock$extend$1;->this$0:Lcom/squareup/blueprint/LinearBlock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Lcom/squareup/blueprint/Block;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;)V"
        }
    .end annotation

    const-string v0, "element"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/blueprint/LinearBlock$extend$1;->this$0:Lcom/squareup/blueprint/LinearBlock;

    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/LinearBlock;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public createParams()Lcom/squareup/blueprint/LinearBlock$Params;
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/blueprint/LinearBlock$extend$1;->this$0:Lcom/squareup/blueprint/LinearBlock;

    invoke-virtual {v0}, Lcom/squareup/blueprint/LinearBlock;->createParams()Lcom/squareup/blueprint/LinearBlock$Params;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/blueprint/LinearBlock$Params;->setExtend(Z)V

    return-object v0
.end method

.method public bridge synthetic createParams()Ljava/lang/Object;
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/blueprint/LinearBlock$extend$1;->createParams()Lcom/squareup/blueprint/LinearBlock$Params;

    move-result-object v0

    return-object v0
.end method
