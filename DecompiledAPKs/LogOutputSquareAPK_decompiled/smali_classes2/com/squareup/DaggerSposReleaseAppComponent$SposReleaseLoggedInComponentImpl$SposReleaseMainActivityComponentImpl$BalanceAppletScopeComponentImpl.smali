.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/balance/BalanceAppletScopeComponent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BalanceAppletScopeComponentImpl"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;
    }
.end annotation


# instance fields
.field private balanceAppletScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private balanceScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private cancelingBizbankWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private cancelingSquareCardWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private enableSquareCardPrivateDataWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private maybeCancelBizbankWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private notificationDisplayPreferencesWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private notificationPreferencesFetchingWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private notificationPreferencesUpdatingWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private notificationPreferencesWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private provideAppletMasterViewPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletMasterViewPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private provideBalanceAppletSectionsListPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private providesResetPasscodeSquareCardTwoFactorAuthWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private providesShowPrivateSquareCardDataTwoFactorAuthWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private realAddMoneyAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private realCancelSquareCardAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private realNotificationPreferencesRemoteDataStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore;",
            ">;"
        }
    .end annotation
.end field

.field private realOrderSquareCardStampRequesterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;",
            ">;"
        }
    .end annotation
.end field

.field private realSquareCardResetPinWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private realViewBillingAddressWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private resetPasscodeTwoFactorDataStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/twofactorauth/datastore/ResetPasscodeTwoFactorDataStore;",
            ">;"
        }
    .end annotation
.end field

.field private showPrivateCardDataTwoFactorDataStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;",
            ">;"
        }
    .end annotation
.end field

.field private squareCardActivityRequesterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;",
            ">;"
        }
    .end annotation
.end field

.field private squareCardAnalyticsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private submitFeedbackWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private submittingCardFeedbackWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

.field private transferToBankRequesterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)V
    .locals 0

    .line 14726
    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14728
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->initialize()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 14671
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)V

    return-void
.end method

.method static synthetic access$51400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;)Ljavax/inject/Provider;
    .locals 0

    .line 14671
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->provideBalanceAppletSectionsListPresenterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$51500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;)Ljavax/inject/Provider;
    .locals 0

    .line 14671
    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->provideAppletMasterViewPresenterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method private getAddMoneyInternalViewFactory()Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory;
    .locals 2

    .line 14882
    new-instance v0, Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getEnterAmountCoordinatorFactory()Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$Factory;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory;-><init>(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$Factory;)V

    return-object v0
.end method

.method private getAddMoneyViewFactory()Lcom/squareup/ui/balance/addmoney/AddMoneyViewFactory;
    .locals 3

    .line 14885
    new-instance v0, Lcom/squareup/ui/balance/addmoney/AddMoneyViewFactory;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getAddMoneyInternalViewFactory()Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/debitcard/RealLinkDebitCardViewFactory;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/balance/addmoney/AddMoneyViewFactory;-><init>(Lcom/squareup/ui/balance/addmoney/AddMoneyInternalViewFactory;Lcom/squareup/debitcard/LinkDebitCardViewFactory;)V

    return-object v0
.end method

.method private getAddMoneyWorkflow()Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;
    .locals 4

    .line 14897
    new-instance v0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealPendingSourceWorkflow()Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;

    invoke-direct {v2}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;-><init>()V

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealSubmitAmountWorkflow()Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;-><init>(Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceWorkflow;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountWorkflow;Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountWorkflow;)V

    return-object v0
.end method

.method private getAddPhoneNumberViewFactory()Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;
    .locals 2

    .line 14776
    new-instance v0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getEnterPhoneNumberCoordinatorFactory()Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;-><init>(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;)V

    return-object v0
.end method

.method private getAddPhoneNumberWorkflow()Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;
    .locals 2

    .line 14851
    new-instance v0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/balance/core/server/bizbank/BizbankService;

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;-><init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V

    return-object v0
.end method

.method private getAuthSquareCardReactor()Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;
    .locals 5

    .line 14789
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$16400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/balance/core/server/bizbank/BizbankService;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealAuthSquareCardAnalytics()Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/balance/core/server/bizbank/BizbankService;Lio/reactivex/Scheduler;Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)V

    return-object v0
.end method

.method private getBusinessNameFormatter()Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;
    .locals 2

    .line 14748
    new-instance v0, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;-><init>(Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method private getCancelSquareCardWorkflow()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;
    .locals 10

    .line 14848
    new-instance v9, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    invoke-static {}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow_Factory;->create()Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow_Factory;

    move-result-object v1

    invoke-static {}, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow_Factory;->create()Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow_Factory;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->cancelingSquareCardWorkflowProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow_Factory;->create()Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow_Factory;

    move-result-object v4

    invoke-static {}, Lcom/squareup/balance/squarecard/cancel/failed/CancelSquareCardFailedWorkflow_Factory;->create()Lcom/squareup/balance/squarecard/cancel/failed/CancelSquareCardFailedWorkflow_Factory;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->maybeCancelBizbankWorkflowProvider:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->submitFeedbackWorkflowProvider:Ljavax/inject/Provider;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealCancelSquareCardAnalytics()Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;)V

    return-object v9
.end method

.method private getCapitalPlanScreenMapper()Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;
    .locals 4

    .line 14900
    new-instance v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$15200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$23600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/text/DateFormat;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$27300(Lcom/squareup/DaggerSposReleaseAppComponent;)Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;-><init>(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Landroid/content/res/Resources;)V

    return-object v0
.end method

.method private getDayAndDateFormatter()Lcom/squareup/text/DayAndDateFormatter;
    .locals 5

    .line 14915
    new-instance v0, Lcom/squareup/text/DayAndDateFormatter;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->provideClock()Lcom/squareup/util/Clock;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$23700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/text/DateFormat;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/text/DayAndDateFormatter;-><init>(Lcom/squareup/util/Clock;Ljavax/inject/Provider;Lcom/squareup/util/Res;Ljava/text/DateFormat;)V

    return-object v0
.end method

.method private getEnterAmountCoordinatorFactory()Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$Factory;
    .locals 4

    .line 14879
    new-instance v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-virtual {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->currencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$Factory;-><init>(Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method

.method private getEnterPhoneNumberCoordinatorFactory()Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;
    .locals 2

    .line 14773
    new-instance v0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$21300(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/text/scrubber/PhoneNumberScrubber;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;-><init>(Lcom/squareup/text/InsertingScrubber;)V

    return-object v0
.end method

.method private getManageSquareCardStateFactory()Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;
    .locals 7

    .line 14854
    new-instance v6, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealOrderSquareCardScreenWorkflowStarter()Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealSquareCardOrderedWorkflowStarter()Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealSquareCardActivatedWorkflow()Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    move-result-object v3

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getCancelSquareCardWorkflow()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    move-result-object v4

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getAddPhoneNumberWorkflow()Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)V

    return-object v6
.end method

.method private getOrderConfirmationCoordinatorFactory()Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;
    .locals 2

    .line 14742
    new-instance v0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;

    invoke-static {}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderConfirmationConfigurationFactory;->provideOrderConfirmationConfiguration()Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;-><init>(Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;)V

    return-object v0
.end method

.method private getOrderCoordinatorFactory()Lcom/squareup/mailorder/OrderCoordinator$Factory;
    .locals 5

    .line 14732
    new-instance v0, Lcom/squareup/mailorder/OrderCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$21300(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/text/scrubber/PhoneNumberScrubber;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$47300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/register/widgets/GlassSpinner;

    move-result-object v3

    invoke-static {}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderCoordinatorConfigurationFactory;->provideOrderCoordinatorConfiguration()Lcom/squareup/mailorder/OrderCoordinator$Configuration;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/mailorder/OrderCoordinator$Factory;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/util/Res;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/mailorder/OrderCoordinator$Configuration;)V

    return-object v0
.end method

.method private getOrderReactor()Lcom/squareup/mailorder/OrderReactor;
    .locals 4

    .line 14805
    new-instance v0, Lcom/squareup/mailorder/OrderReactor;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getOrderServiceHelper()Lcom/squareup/mailorder/OrderServiceHelper;

    move-result-object v1

    invoke-static {}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderReactorConfigurationFactory;->provideOrderReactorConfiguration()Lcom/squareup/mailorder/OrderReactor$Configuration;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealOrderSquareCardAnalytics()Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/mailorder/OrderReactor;-><init>(Lcom/squareup/mailorder/OrderServiceHelper;Lcom/squareup/mailorder/OrderReactor$Configuration;Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-object v0
.end method

.method private getOrderServiceHelper()Lcom/squareup/mailorder/OrderServiceHelper;
    .locals 7

    .line 14799
    new-instance v6, Lcom/squareup/mailorder/OrderServiceHelper;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$16400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$18000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/merchantprofile/MerchantProfileUpdater;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$27300(Lcom/squareup/DaggerSposReleaseAppComponent;)Landroid/content/res/Resources;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$16600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Lcom/squareup/receiving/FailureMessageFactory;

    move-result-object v4

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getOrderSquareCardServiceHelper()Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/mailorder/OrderServiceHelper;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/merchantprofile/MerchantProfileUpdater;Landroid/content/res/Resources;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;)V

    return-object v6
.end method

.method private getOrderSquareCardBusinessNameCoordinatorFactory()Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;
    .locals 2

    .line 14752
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getBusinessNameFormatter()Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;-><init>(Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;)V

    return-object v0
.end method

.method private getOrderSquareCardReactor()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;
    .locals 7

    .line 14817
    new-instance v6, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealOrderSquareCardSerializer()Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/balance/core/server/bizbank/BizbankService;

    invoke-static {}, Lcom/squareup/jvm/util/UniqueModule_ProvideUniqueFactory;->provideUnique()Lcom/squareup/util/Unique;

    move-result-object v3

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealOrderSquareCardAnalytics()Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;

    move-result-object v4

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealOrderSquareCardStampRequester()Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;-><init>(Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/util/Unique;Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;)V

    return-object v6
.end method

.method private getOrderSquareCardServiceHelper()Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;
    .locals 2

    .line 14796
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/balance/core/server/bizbank/BizbankService;

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;-><init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V

    return-object v0
.end method

.method private getOrderSquareCardSignatureCoordinatorFactory()Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;
    .locals 8

    .line 14756
    new-instance v7, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-static {}, Lcom/squareup/thread/MainThreadModule_ProvideMainThreadEnforcerFactory;->provideMainThreadEnforcer()Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$47400(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/balance/core/RenderSignatureScheduler;

    move-result-object v3

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideComputationSchedulerFactory;->provideComputationScheduler()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideGsonFactory;->provideGson()Lcom/google/gson/Gson;

    move-result-object v5

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getBusinessNameFormatter()Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/balance/core/RenderSignatureScheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;)V

    return-object v7
.end method

.method private getOrderSquareCardStampsDialogFactoryFactory()Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;
    .locals 2

    .line 14760
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$47500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/recycler/RecyclerFactory;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;-><init>(Lcom/squareup/recycler/RecyclerFactory;)V

    return-object v0
.end method

.method private getOrderSquareCardViewFactory()Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;
    .locals 4

    .line 14763
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getOrderSquareCardBusinessNameCoordinatorFactory()Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getOrderSquareCardSignatureCoordinatorFactory()Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getOrderSquareCardStampsDialogFactoryFactory()Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;)V

    return-object v0
.end method

.method private getOrderWorkflowViewFactoryFactory()Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;
    .locals 5

    .line 14745
    new-instance v0, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getOrderCoordinatorFactory()Lcom/squareup/mailorder/OrderCoordinator$Factory;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getSelectCorrectedAddressCoordinatorFactory()Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getUnverifiedAddressCoordinatorFactory()Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;

    move-result-object v3

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getOrderConfirmationCoordinatorFactory()Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;-><init>(Lcom/squareup/mailorder/OrderCoordinator$Factory;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;)V

    return-object v0
.end method

.method private getRealAddMoneyAnalytics()Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;
    .locals 2

    .line 14891
    new-instance v0, Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/Analytics;

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;-><init>(Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method

.method private getRealAuthSquareCardAnalytics()Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;
    .locals 2

    .line 14786
    new-instance v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getSquareCardAnalyticsLogger()Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;-><init>(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V

    return-object v0
.end method

.method private getRealAuthSquareCardScreenWorkflowStarter()Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;
    .locals 2

    .line 14793
    new-instance v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getAuthSquareCardReactor()Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;)V

    return-object v0
.end method

.method private getRealCancelSquareCardAnalytics()Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;
    .locals 2

    .line 14845
    new-instance v0, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getSquareCardAnalyticsLogger()Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;-><init>(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V

    return-object v0
.end method

.method private getRealCapitalFlexLoanWorkflow()Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;
    .locals 9

    .line 14909
    new-instance v8, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealCapitalPlanWorkflow()Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealCapitalOfferWorkflow()Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;

    move-result-object v2

    new-instance v3, Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow;

    invoke-direct {v3}, Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow;-><init>()V

    new-instance v4, Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow;

    invoke-direct {v4}, Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow;-><init>()V

    new-instance v5, Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow;

    invoke-direct {v5}, Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow;-><init>()V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;

    move-result-object v6

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;-><init>(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;)V

    return-object v8
.end method

.method private getRealCapitalFlexLoanWorkflowRunner()Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;
    .locals 4

    .line 14912
    new-instance v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;

    new-instance v1, Lcom/squareup/capital/flexloan/RealCapitalWorkflowViewFactory;

    invoke-direct {v1}, Lcom/squareup/capital/flexloan/RealCapitalWorkflowViewFactory;-><init>()V

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealCapitalFlexLoanWorkflow()Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/ui/main/PosContainer;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;-><init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;Lcom/squareup/ui/main/PosContainer;)V

    return-object v0
.end method

.method private getRealCapitalOfferWorkflow()Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;
    .locals 7

    .line 14906
    new-instance v6, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$15200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$600(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/AppBootstrapModule;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideActivityListenerFactory;->provideActivityListener(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/ActivityListener;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;-><init>(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lcom/squareup/text/Formatter;Lcom/squareup/ActivityListener;)V

    return-object v6
.end method

.method private getRealCapitalPlanWorkflow()Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;
    .locals 8

    .line 14903
    new-instance v7, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;

    move-result-object v1

    new-instance v2, Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow;

    invoke-direct {v2}, Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow;-><init>()V

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getCapitalPlanScreenMapper()Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;-><init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;)V

    return-object v7
.end method

.method private getRealManageSquareCardAnalytics()Lcom/squareup/balance/squarecard/RealManageSquareCardAnalytics;
    .locals 2

    .line 14857
    new-instance v0, Lcom/squareup/balance/squarecard/RealManageSquareCardAnalytics;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getSquareCardAnalyticsLogger()Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/RealManageSquareCardAnalytics;-><init>(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V

    return-object v0
.end method

.method private getRealManageSquareCardWorkflow()Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;
    .locals 10

    .line 14860
    new-instance v9, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getManageSquareCardStateFactory()Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$16400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v3

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealManageSquareCardAnalytics()Lcom/squareup/balance/squarecard/RealManageSquareCardAnalytics;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/ui/settings/RealSettingsAppletGateway;

    move-result-object v6

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/instantdeposit/InstantDepositRunner;

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;-><init>(Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;Lcom/squareup/balance/squarecard/ManageSquareCardAnalytics;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/settings/SettingsAppletGateway;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Lcom/squareup/instantdeposit/InstantDepositRunner;)V

    return-object v9
.end method

.method private getRealManageSquareCardWorkflowViewFactory()Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;
    .locals 8

    .line 14780
    new-instance v7, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getOrderWorkflowViewFactoryFactory()Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;

    move-result-object v1

    new-instance v2, Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;

    invoke-direct {v2}, Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;-><init>()V

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getOrderSquareCardViewFactory()Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;

    move-result-object v3

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getSquareCardOrderedWorkflowViewFactory()Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;

    move-result-object v4

    new-instance v5, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardViewFactory;

    invoke-direct {v5}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardViewFactory;-><init>()V

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getAddPhoneNumberViewFactory()Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;-><init>(Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardViewFactory;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;)V

    return-object v7
.end method

.method private getRealOrderScreenWorkflowStarter()Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;
    .locals 4

    .line 14808
    new-instance v0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$16400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getOrderReactor()Lcom/squareup/mailorder/OrderReactor;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/mailorder/OrderReactor;)V

    return-object v0
.end method

.method private getRealOrderSquareCardAnalytics()Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;
    .locals 2

    .line 14802
    new-instance v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getSquareCardAnalyticsLogger()Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;-><init>(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V

    return-object v0
.end method

.method private getRealOrderSquareCardScreenWorkflowStarter()Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;
    .locals 3

    .line 14821
    new-instance v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getOrderSquareCardReactor()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor;Lcom/squareup/util/Device;)V

    return-object v0
.end method

.method private getRealOrderSquareCardSerializer()Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;
    .locals 3

    .line 14811
    new-instance v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealAuthSquareCardScreenWorkflowStarter()Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealOrderScreenWorkflowStarter()Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;Lcom/squareup/mailorder/OrderScreenWorkflowStarter;)V

    return-object v0
.end method

.method private getRealOrderSquareCardStampRequester()Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;
    .locals 3

    .line 14814
    new-instance v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/balance/core/server/bizbank/BizbankService;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;-><init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method private getRealPendingSourceWorkflow()Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow;
    .locals 2

    .line 14888
    new-instance v0, Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow;-><init>(Lcom/squareup/debitcard/LinkDebitCardWorkflow;)V

    return-object v0
.end method

.method private getRealSquareCardActivatedAnalytics()Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;
    .locals 2

    .line 14833
    new-instance v0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getSquareCardAnalyticsLogger()Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;-><init>(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V

    return-object v0
.end method

.method private getRealSquareCardActivatedWorkflow()Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;
    .locals 13

    .line 14842
    new-instance v12, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$47900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/secure/SecureScopeManager;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealSquareCardActivatedAnalytics()Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v4

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getSquareCardAddToGooglePayHelper()Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    iget-object v7, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->realSquareCardResetPinWorkflowProvider:Ljavax/inject/Provider;

    iget-object v8, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->realViewBillingAddressWorkflowProvider:Ljavax/inject/Provider;

    iget-object v9, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->notificationPreferencesWorkflowProvider:Ljavax/inject/Provider;

    iget-object v10, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->enableSquareCardPrivateDataWorkflowProvider:Ljavax/inject/Provider;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getSquareCardPrivateCardDetailsDataStore()Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;-><init>(Lcom/squareup/secure/SecureScopeManager;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;)V

    return-object v12
.end method

.method private getRealSquareCardOrderedAnalytics()Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;
    .locals 2

    .line 14824
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getSquareCardAnalyticsLogger()Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;-><init>(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V

    return-object v0
.end method

.method private getRealSquareCardOrderedWorkflowStarter()Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;
    .locals 3

    .line 14830
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getSquareCardOrderedReactor()Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/util/Device;)V

    return-object v0
.end method

.method private getRealSubmitAmountWorkflow()Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;
    .locals 5

    .line 14894
    new-instance v0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$40600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/balance/core/server/transfers/TransfersService;

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$16400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v3

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealAddMoneyAnalytics()Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;-><init>(Lcom/squareup/text/Formatter;Lcom/squareup/balance/core/server/transfers/TransfersService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;)V

    return-object v0
.end method

.method private getSelectCorrectedAddressCoordinatorFactory()Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;
    .locals 3

    .line 14736
    new-instance v0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;

    invoke-static {}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideCorrectedAddressConfigurationFactory;->provideCorrectedAddressConfiguration()Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$47300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/register/widgets/GlassSpinner;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;-><init>(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-object v0
.end method

.method private getSquareCardActivationCardConfirmationCoordinatorFactory()Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;
    .locals 3

    .line 14767
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$25100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$47600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;-><init>(Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;)V

    return-object v0
.end method

.method private getSquareCardAddToGooglePayHelper()Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;
    .locals 4

    .line 14836
    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$600(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/AppBootstrapModule;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$47800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/googlepay/GooglePay;

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/balance/core/server/bizbank/BizbankService;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;-><init>(Landroid/app/Application;Lcom/squareup/googlepay/GooglePay;Lcom/squareup/balance/core/server/bizbank/BizbankService;)V

    return-object v0
.end method

.method private getSquareCardAnalyticsLogger()Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;
    .locals 2

    .line 14783
    new-instance v0, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/Analytics;

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;-><init>(Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method

.method private getSquareCardOrderedReactor()Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;
    .locals 7

    .line 14827
    new-instance v6, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/balance/core/server/bizbank/BizbankService;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$47700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/payment/CardConverter;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v3

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealSquareCardOrderedAnalytics()Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$10000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;-><init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/payment/CardConverter;Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V

    return-object v6
.end method

.method private getSquareCardOrderedWorkflowViewFactory()Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;
    .locals 2

    .line 14770
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getSquareCardActivationCardConfirmationCoordinatorFactory()Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;)V

    return-object v0
.end method

.method private getSquareCardPrivateCardDetailsDataStore()Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;
    .locals 2

    .line 14839
    new-instance v0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/balance/core/server/bizbank/BizbankService;

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;-><init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V

    return-object v0
.end method

.method private getTransferReportsDetailLayoutRunnerFactory()Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;
    .locals 3

    .line 14867
    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$47500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/recycler/RecyclerFactory;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;-><init>(Lcom/squareup/text/Formatter;Lcom/squareup/recycler/RecyclerFactory;)V

    return-object v0
.end method

.method private getTransferReportsDetailWorkflow()Lcom/squareup/transferreports/TransferReportsDetailWorkflow;
    .locals 5

    .line 14873
    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/transferreports/RealTransferReportsLoader;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$16700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/feetutorial/RealFeeTutorial;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;-><init>(Lcom/squareup/transferreports/TransferReportsLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/feetutorial/FeeTutorial;)V

    return-object v0
.end method

.method private getTransferReportsLayoutRunnerFactory()Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;
    .locals 4

    .line 14863
    new-instance v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$23300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/text/DateFormat;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$47500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/recycler/RecyclerFactory;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;-><init>(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/recycler/RecyclerFactory;)V

    return-object v0
.end method

.method private getTransferReportsViewFactoryFactory()Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;
    .locals 3

    .line 14870
    new-instance v0, Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getTransferReportsLayoutRunnerFactory()Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getTransferReportsDetailLayoutRunnerFactory()Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;-><init>(Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;)V

    return-object v0
.end method

.method private getTransferReportsWorkflow()Lcom/squareup/transferreports/TransferReportsWorkflow;
    .locals 7

    .line 14876
    new-instance v6, Lcom/squareup/transferreports/TransferReportsWorkflow;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/transferreports/RealTransferReportsLoader;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/instantdeposit/InstantDepositRunner;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/BrowserLauncher;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getTransferReportsDetailWorkflow()Lcom/squareup/transferreports/TransferReportsDetailWorkflow;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/transferreports/TransferReportsWorkflow;-><init>(Lcom/squareup/transferreports/TransferReportsLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/transferreports/TransferReportsDetailWorkflow;)V

    return-object v6
.end method

.method private getUnverifiedAddressCoordinatorFactory()Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;
    .locals 3

    .line 14739
    new-instance v0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$47300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/register/widgets/GlassSpinner;

    move-result-object v1

    invoke-static {}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideUnverifiedAddressConfigurationFactory;->provideUnverifiedAddressConfiguration()Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;-><init>(Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;)V

    return-object v0
.end method

.method private initialize()V
    .locals 29

    move-object/from16 v0, p0

    .line 14919
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/balance/squarecard/twofactorauth/datastore/ResetPasscodeTwoFactorDataStore_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/twofactorauth/datastore/ResetPasscodeTwoFactorDataStore_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->resetPasscodeTwoFactorDataStoreProvider:Ljavax/inject/Provider;

    .line 14920
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->resetPasscodeTwoFactorDataStoreProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule_Companion_ProvidesResetPasscodeSquareCardTwoFactorAuthWorkflowFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule_Companion_ProvidesResetPasscodeSquareCardTwoFactorAuthWorkflowFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->providesResetPasscodeSquareCardTwoFactorAuthWorkflowProvider:Ljavax/inject/Provider;

    .line 14921
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->providesResetPasscodeSquareCardTwoFactorAuthWorkflowProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->realSquareCardResetPinWorkflowProvider:Ljavax/inject/Provider;

    .line 14922
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->realViewBillingAddressWorkflowProvider:Ljavax/inject/Provider;

    .line 14923
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$17600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->realNotificationPreferencesRemoteDataStoreProvider:Ljavax/inject/Provider;

    .line 14924
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->realNotificationPreferencesRemoteDataStoreProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->notificationPreferencesFetchingWorkflowProvider:Ljavax/inject/Provider;

    .line 14925
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->realNotificationPreferencesRemoteDataStoreProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow_Factory;->create()Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow_Factory;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->notificationPreferencesUpdatingWorkflowProvider:Ljavax/inject/Provider;

    .line 14926
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->realNotificationPreferencesRemoteDataStoreProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->notificationPreferencesUpdatingWorkflowProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->notificationDisplayPreferencesWorkflowProvider:Ljavax/inject/Provider;

    .line 14927
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->notificationPreferencesFetchingWorkflowProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->notificationDisplayPreferencesWorkflowProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow_Factory;->create()Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow_Factory;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->notificationPreferencesWorkflowProvider:Ljavax/inject/Provider;

    .line 14928
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->showPrivateCardDataTwoFactorDataStoreProvider:Ljavax/inject/Provider;

    .line 14929
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->showPrivateCardDataTwoFactorDataStoreProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule_Companion_ProvidesShowPrivateSquareCardDataTwoFactorAuthWorkflowFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule_Companion_ProvidesShowPrivateSquareCardDataTwoFactorAuthWorkflowFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->providesShowPrivateSquareCardDataTwoFactorAuthWorkflowProvider:Ljavax/inject/Provider;

    .line 14930
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->providesShowPrivateSquareCardDataTwoFactorAuthWorkflowProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->enableSquareCardPrivateDataWorkflowProvider:Ljavax/inject/Provider;

    .line 14931
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->cancelingSquareCardWorkflowProvider:Ljavax/inject/Provider;

    .line 14932
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->cancelingBizbankWorkflowProvider:Ljavax/inject/Provider;

    .line 14933
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->squareCardAnalyticsLoggerProvider:Ljavax/inject/Provider;

    .line 14934
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->squareCardAnalyticsLoggerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->realCancelSquareCardAnalyticsProvider:Ljavax/inject/Provider;

    .line 14935
    invoke-static {}, Lcom/squareup/balance/squarecard/cancelbizbank/offer/OfferToDeactivateBizbankWorkflow_Factory;->create()Lcom/squareup/balance/squarecard/cancelbizbank/offer/OfferToDeactivateBizbankWorkflow_Factory;

    move-result-object v2

    invoke-static {}, Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow_Factory;->create()Lcom/squareup/balance/squarecard/cancelbizbank/confirm/ConfirmBizbankStatusWorkflow_Factory;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->cancelingBizbankWorkflowProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/balance/squarecard/cancelbizbank/success/CancelBizbankSuccessWorkflow_Factory;->create()Lcom/squareup/balance/squarecard/cancelbizbank/success/CancelBizbankSuccessWorkflow_Factory;

    move-result-object v5

    invoke-static {}, Lcom/squareup/balance/squarecard/cancelbizbank/failed/CancelBizbankFailedWorkflow_Factory;->create()Lcom/squareup/balance/squarecard/cancelbizbank/failed/CancelBizbankFailedWorkflow_Factory;

    move-result-object v6

    iget-object v7, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->realCancelSquareCardAnalyticsProvider:Ljavax/inject/Provider;

    invoke-static/range {v2 .. v7}, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankWorkflow_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->maybeCancelBizbankWorkflowProvider:Ljavax/inject/Provider;

    .line 14936
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->submittingCardFeedbackWorkflowProvider:Ljavax/inject/Provider;

    .line 14937
    invoke-static {}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow_Factory;->create()Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow_Factory;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->submittingCardFeedbackWorkflowProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/balance/squarecard/cancel/feedback/success/SubmitFeedbackSuccessWorkflow_Factory;->create()Lcom/squareup/balance/squarecard/cancel/feedback/success/SubmitFeedbackSuccessWorkflow_Factory;

    move-result-object v3

    invoke-static {}, Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow_Factory;->create()Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow_Factory;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->submitFeedbackWorkflowProvider:Ljavax/inject/Provider;

    .line 14938
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$40600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$17600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$17500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$30600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->transferToBankRequesterProvider:Ljavax/inject/Provider;

    .line 14939
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->realOrderSquareCardStampRequesterProvider:Ljavax/inject/Provider;

    .line 14940
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->squareCardActivityRequesterProvider:Ljavax/inject/Provider;

    .line 14941
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/ui/balance/addmoney/RealAddMoneyAnalytics_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->realAddMoneyAnalyticsProvider:Ljavax/inject/Provider;

    .line 14942
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$6000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->transferToBankRequesterProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->realOrderSquareCardStampRequesterProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->squareCardActivityRequesterProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v8

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v9

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v10

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$9200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v11

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$14900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v12

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v13

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v14

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$7300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v15

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$17600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v16

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v17

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v18

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v19

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v20

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$16700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v21

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$25700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v22

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v23

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$50000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v24

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$50100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v25

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v26

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->realAddMoneyAnalyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v27, v1

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$10700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v28

    invoke-static/range {v2 .. v28}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    .line 14943
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$50200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/balance/BalanceAppletScopeRunner_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/ui/balance/BalanceAppletScopeRunner_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceAppletScopeRunnerProvider:Ljavax/inject/Provider;

    .line 14944
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$50200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$7300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v6, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$14600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v8

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$9200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v9

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$30600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v10

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v11

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v12

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v13

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$50300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v14

    invoke-static/range {v2 .. v14}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->provideBalanceAppletSectionsListPresenterProvider:Ljavax/inject/Provider;

    .line 14945
    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$50400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$50200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$50500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideAppletMasterViewPresenterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideAppletMasterViewPresenterFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->provideAppletMasterViewPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private injectAddressLayout(Lcom/squareup/address/AddressLayout;)Lcom/squareup/address/AddressLayout;
    .locals 1

    .line 15034
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$51200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/address/AddressLayoutRunner;

    invoke-static {p1, v0}, Lcom/squareup/address/AddressLayout_MembersInjector;->injectRunner(Lcom/squareup/address/AddressLayout;Lcom/squareup/address/AddressLayoutRunner;)V

    return-object p1
.end method


# virtual methods
.method public addMoneyWorkflowRunner()Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;
    .locals 5

    .line 14962
    new-instance v0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getAddMoneyViewFactory()Lcom/squareup/ui/balance/addmoney/AddMoneyViewFactory;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getAddMoneyWorkflow()Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/ui/main/PosContainer;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$AddMoneyWorkflowResultHandler;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;-><init>(Lcom/squareup/ui/balance/addmoney/AddMoneyViewFactory;Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$AddMoneyWorkflowResultHandler;)V

    return-object v0
.end method

.method public balanceAppletScopeRunner()Lcom/squareup/ui/balance/BalanceAppletScopeRunner;
    .locals 1

    .line 14978
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceAppletScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/BalanceAppletScopeRunner;

    return-object v0
.end method

.method public balanceMasterScreenComponent()Lcom/squareup/ui/balance/BalanceMasterScreen$Component;
    .locals 2

    .line 15030
    new-instance v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V

    return-object v0
.end method

.method public balanceScopeRunner()Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;
    .locals 1

    .line 14982
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;

    return-object v0
.end method

.method public balanceTransactionDetailCoordinator()Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;
    .locals 2

    .line 14990
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;)V

    return-object v0
.end method

.method public balanceTransactionsCoordinator()Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;
    .locals 8

    .line 14986
    new-instance v7, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$23400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/text/DateFormat;

    move-result-object v4

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getDayAndDateFormatter()Lcom/squareup/text/DayAndDateFormatter;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$47500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/recycler/RecyclerFactory;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/text/DayAndDateFormatter;Lcom/squareup/recycler/RecyclerFactory;)V

    return-object v7
.end method

.method public capitalWorkflowRunner()Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;
    .locals 1

    .line 14966
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealCapitalFlexLoanWorkflowRunner()Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public confirmTransferCoordinator()Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;
    .locals 4

    .line 14998
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/text/Formatter;

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48100(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/ui/settings/RealSettingsAppletGateway;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator_Factory;->newInstance(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/text/Formatter;Lcom/squareup/ui/settings/SettingsAppletGateway;)Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public features()Lcom/squareup/settings/server/Features;
    .locals 1

    .line 15018
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v0

    return-object v0
.end method

.method public inject(Lcom/squareup/address/AddressLayout;)V
    .locals 0

    .line 14950
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->injectAddressLayout(Lcom/squareup/address/AddressLayout;)Lcom/squareup/address/AddressLayout;

    return-void
.end method

.method public instantDepositResultCoordinator()Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;
    .locals 2

    .line 15006
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;-><init>(Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;)V

    return-object v0
.end method

.method public linkBankAccountDialogRunner()Lcom/squareup/ui/balance/bizbanking/transfer/LinkBankAccountDialog$Runner;
    .locals 1

    .line 15014
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/bizbanking/transfer/LinkBankAccountDialog$Runner;

    return-object v0
.end method

.method public linkBankAccountWorkflowRunner()Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner;
    .locals 5

    .line 15026
    new-instance v0, Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$50900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/banklinking/RealLinkBankAccountViewFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$51000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/ui/main/PosContainer;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner$LinkBankAccountWorkflowResultHandler;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner;-><init>(Lcom/squareup/banklinking/LinkBankAccountViewFactory;Lcom/squareup/banklinking/LinkBankAccountWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner$LinkBankAccountWorkflowResultHandler;)V

    return-object v0
.end method

.method public linkDebitCardDialogRunner()Lcom/squareup/ui/balance/bizbanking/transfer/LinkDebitCardDialog$Runner;
    .locals 1

    .line 15010
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/bizbanking/transfer/LinkDebitCardDialog$Runner;

    return-object v0
.end method

.method public linkDebitCardWorkflowRunner()Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner;
    .locals 5

    .line 15022
    new-instance v0, Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/debitcard/RealLinkDebitCardViewFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$48800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/ui/main/PosContainer;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner$LinkDebitCardWorkflowResultHandler;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner;-><init>(Lcom/squareup/debitcard/LinkDebitCardViewFactory;Lcom/squareup/debitcard/LinkDebitCardWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner$LinkDebitCardWorkflowResultHandler;)V

    return-object v0
.end method

.method public manageSquareCardWorkflowRunner()Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner;
    .locals 5

    .line 14954
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealManageSquareCardWorkflowViewFactory()Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getRealManageSquareCardWorkflow()Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/ui/main/PosContainer;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner$ManageSquareCardWorkflowResultHandler;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner;-><init>(Lcom/squareup/balance/squarecard/ManageSquareCardWorkflowViewFactory;Lcom/squareup/balance/squarecard/ManageSquareCardWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner$ManageSquareCardWorkflowResultHandler;)V

    return-object v0
.end method

.method public priceChangeDialogRunner()Lcom/squareup/instantdeposit/PriceChangeDialog$Runner;
    .locals 1

    .line 14974
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/instantdeposit/PriceChangeDialog$Runner;

    return-object v0
.end method

.method public transferReportsWorkflowRunnerFactory()Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Factory;
    .locals 4

    .line 14958
    new-instance v0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Factory;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getTransferReportsViewFactoryFactory()Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->getTransferReportsWorkflow()Lcom/squareup/transferreports/TransferReportsWorkflow;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/ui/main/PosContainer;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Factory;-><init>(Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;Lcom/squareup/transferreports/TransferReportsWorkflow;Lcom/squareup/ui/main/PosContainer;)V

    return-object v0
.end method

.method public transferResultCoordinator()Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;
    .locals 2

    .line 15002
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    invoke-static {v0, v1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator_Factory;->newInstance(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;Lcom/squareup/text/Formatter;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public transferToBankCoordinator()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;
    .locals 4

    .line 14994
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->balanceScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$50800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/money/MoneyLocaleHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-virtual {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->currencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator_Factory;->newInstance(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;Lcom/squareup/text/Formatter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public unifiedActivityCardSpendWorkflowRunner()Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;
    .locals 4

    .line 14970
    new-instance v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/ui/main/PosContainer;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$50600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$50700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lcom/squareup/balance/activity/ui/RealBalanceActivityViewFactory$Factory;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;-><init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;Lcom/squareup/balance/activity/ui/BalanceActivityViewFactory;)V

    return-object v0
.end method
