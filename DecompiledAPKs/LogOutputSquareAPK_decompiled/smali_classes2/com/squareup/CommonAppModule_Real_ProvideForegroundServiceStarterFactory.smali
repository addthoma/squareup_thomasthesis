.class public final Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;
.super Ljava/lang/Object;
.source "CommonAppModule_Real_ProvideForegroundServiceStarterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/foregroundservice/ForegroundServiceStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationWrapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;->applicationProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;->resProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;->notificationWrapperProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;->mainThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)",
            "Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;"
        }
    .end annotation

    .line 53
    new-instance v6, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static provideForegroundServiceStarter(Landroid/app/Application;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/util/Res;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/foregroundservice/ForegroundServiceStarter;
    .locals 0

    .line 59
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/CommonAppModule$Real;->provideForegroundServiceStarter(Landroid/app/Application;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/util/Res;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/foregroundservice/ForegroundServiceStarter;
    .locals 5

    .line 46
    iget-object v0, p0, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/log/OhSnapLogger;

    iget-object v2, p0, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;->notificationWrapperProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/notification/NotificationWrapper;

    iget-object v4, p0, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/thread/executor/MainThread;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;->provideForegroundServiceStarter(Landroid/app/Application;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/util/Res;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/CommonAppModule_Real_ProvideForegroundServiceStarterFactory;->get()Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    move-result-object v0

    return-object v0
.end method
