.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$SES_ComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/onboarding/SilentExitScreen$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SES_ComponentImpl"
.end annotation


# instance fields
.field private presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/SilentExitScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;)V
    .locals 0

    .line 10740
    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$SES_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10742
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$SES_ComponentImpl;->initialize()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 10737
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$SES_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;)V

    return-void
.end method

.method private initialize()V
    .locals 1

    .line 10747
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$SES_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->access$20900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/onboarding/SilentExitScreen_Presenter_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/SilentExitScreen_Presenter_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$SES_ComponentImpl;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private injectSilentExitView(Lcom/squareup/ui/onboarding/SilentExitView;)Lcom/squareup/ui/onboarding/SilentExitView;
    .locals 1

    .line 10755
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$SES_ComponentImpl;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/SilentExitScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/SilentExitView_MembersInjector;->injectPresenter(Lcom/squareup/ui/onboarding/SilentExitView;Lcom/squareup/ui/onboarding/SilentExitScreen$Presenter;)V

    return-object p1
.end method


# virtual methods
.method public inject(Lcom/squareup/ui/onboarding/SilentExitView;)V
    .locals 0

    .line 10752
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$SES_ComponentImpl;->injectSilentExitView(Lcom/squareup/ui/onboarding/SilentExitView;)Lcom/squareup/ui/onboarding/SilentExitView;

    return-void
.end method
