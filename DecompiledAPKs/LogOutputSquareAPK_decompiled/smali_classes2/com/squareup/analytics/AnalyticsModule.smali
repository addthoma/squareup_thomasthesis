.class public abstract Lcom/squareup/analytics/AnalyticsModule;
.super Ljava/lang/Object;
.source "AnalyticsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideAnalytics(Lcom/squareup/analytics/EventStreamAnalytics;)Lcom/squareup/analytics/Analytics;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
