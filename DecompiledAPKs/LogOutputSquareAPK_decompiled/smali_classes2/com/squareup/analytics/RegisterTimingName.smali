.class public final enum Lcom/squareup/analytics/RegisterTimingName;
.super Ljava/lang/Enum;
.source "RegisterTimingName.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/analytics/RegisterTimingName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/analytics/RegisterTimingName;

.field public static final enum BUNDLE_PERSISTENCE_TIME:Lcom/squareup/analytics/RegisterTimingName;

.field public static final enum CART_INTERACTION_ALL_DISCOUNTS:Lcom/squareup/analytics/RegisterTimingName;

.field public static final enum CART_INTERACTION_ALL_TAXES:Lcom/squareup/analytics/RegisterTimingName;

.field public static final enum CART_INTERACTION_DISCOUNT:Lcom/squareup/analytics/RegisterTimingName;

.field public static final enum CART_INTERACTION_ITEM:Lcom/squareup/analytics/RegisterTimingName;

.field public static final enum ITEMS_SYNC:Lcom/squareup/analytics/RegisterTimingName;

.field public static final enum MAIN_THREAD_BLOCKED:Lcom/squareup/analytics/RegisterTimingName;

.field public static final enum OPEN_TICKETS_LIST_RESPONSE_TO_LIST_UI:Lcom/squareup/analytics/RegisterTimingName;

.field public static final enum OPEN_TICKETS_TICKET_IN_CART_UI:Lcom/squareup/analytics/RegisterTimingName;

.field public static final enum OPEN_TICKETS_TICKET_SELECTED_TO_CART_UI:Lcom/squareup/analytics/RegisterTimingName;

.field public static final enum SCREEN_TIMED_OUT:Lcom/squareup/analytics/RegisterTimingName;

.field public static final enum SERVER_CALL:Lcom/squareup/analytics/RegisterTimingName;

.field public static final enum STARTUP_TIME_TO_INTERACT:Lcom/squareup/analytics/RegisterTimingName;

.field public static final enum TRANSACTION_TIME:Lcom/squareup/analytics/RegisterTimingName;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 9
    new-instance v0, Lcom/squareup/analytics/RegisterTimingName;

    const/4 v1, 0x0

    const-string v2, "MAIN_THREAD_BLOCKED"

    const-string v3, "Main Thread Blocked V2"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/analytics/RegisterTimingName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->MAIN_THREAD_BLOCKED:Lcom/squareup/analytics/RegisterTimingName;

    .line 10
    new-instance v0, Lcom/squareup/analytics/RegisterTimingName;

    const/4 v2, 0x1

    const-string v3, "SERVER_CALL"

    const-string v4, "Server API Call"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/analytics/RegisterTimingName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->SERVER_CALL:Lcom/squareup/analytics/RegisterTimingName;

    .line 11
    new-instance v0, Lcom/squareup/analytics/RegisterTimingName;

    const/4 v3, 0x2

    const-string v4, "STARTUP_TIME_TO_INTERACT"

    const-string v5, "Startup Time To Interact"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/analytics/RegisterTimingName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->STARTUP_TIME_TO_INTERACT:Lcom/squareup/analytics/RegisterTimingName;

    .line 12
    new-instance v0, Lcom/squareup/analytics/RegisterTimingName;

    const/4 v4, 0x3

    const-string v5, "CART_INTERACTION_ITEM"

    const-string v6, "Cart Interaction: Item"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/analytics/RegisterTimingName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->CART_INTERACTION_ITEM:Lcom/squareup/analytics/RegisterTimingName;

    .line 13
    new-instance v0, Lcom/squareup/analytics/RegisterTimingName;

    const/4 v5, 0x4

    const-string v6, "CART_INTERACTION_DISCOUNT"

    const-string v7, "Cart Interaction: Discount"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/analytics/RegisterTimingName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->CART_INTERACTION_DISCOUNT:Lcom/squareup/analytics/RegisterTimingName;

    .line 14
    new-instance v0, Lcom/squareup/analytics/RegisterTimingName;

    const/4 v6, 0x5

    const-string v7, "CART_INTERACTION_ALL_DISCOUNTS"

    const-string v8, "Cart Interaction: All Discounts"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/analytics/RegisterTimingName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->CART_INTERACTION_ALL_DISCOUNTS:Lcom/squareup/analytics/RegisterTimingName;

    .line 15
    new-instance v0, Lcom/squareup/analytics/RegisterTimingName;

    const/4 v7, 0x6

    const-string v8, "CART_INTERACTION_ALL_TAXES"

    const-string v9, "Cart Interaction: All Taxes"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/analytics/RegisterTimingName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->CART_INTERACTION_ALL_TAXES:Lcom/squareup/analytics/RegisterTimingName;

    .line 16
    new-instance v0, Lcom/squareup/analytics/RegisterTimingName;

    const/4 v8, 0x7

    const-string v9, "ITEMS_SYNC"

    const-string v10, "Items Sync"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/analytics/RegisterTimingName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->ITEMS_SYNC:Lcom/squareup/analytics/RegisterTimingName;

    .line 17
    new-instance v0, Lcom/squareup/analytics/RegisterTimingName;

    const/16 v9, 0x8

    const-string v10, "OPEN_TICKETS_TICKET_IN_CART_UI"

    const-string v11, "Open Tickets: Ticket In Cart UI"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/analytics/RegisterTimingName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->OPEN_TICKETS_TICKET_IN_CART_UI:Lcom/squareup/analytics/RegisterTimingName;

    .line 18
    new-instance v0, Lcom/squareup/analytics/RegisterTimingName;

    const/16 v10, 0x9

    const-string v11, "OPEN_TICKETS_LIST_RESPONSE_TO_LIST_UI"

    const-string v12, "Open Tickets: List Response to Updated Ticket List UI"

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/analytics/RegisterTimingName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->OPEN_TICKETS_LIST_RESPONSE_TO_LIST_UI:Lcom/squareup/analytics/RegisterTimingName;

    .line 19
    new-instance v0, Lcom/squareup/analytics/RegisterTimingName;

    const/16 v11, 0xa

    const-string v12, "OPEN_TICKETS_TICKET_SELECTED_TO_CART_UI"

    const-string v13, "Open Tickets: Ticket Selected to Updated Cart UI"

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/analytics/RegisterTimingName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->OPEN_TICKETS_TICKET_SELECTED_TO_CART_UI:Lcom/squareup/analytics/RegisterTimingName;

    .line 20
    new-instance v0, Lcom/squareup/analytics/RegisterTimingName;

    const/16 v12, 0xb

    const-string v13, "SCREEN_TIMED_OUT"

    const-string v14, "Screen Timed Out"

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/analytics/RegisterTimingName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->SCREEN_TIMED_OUT:Lcom/squareup/analytics/RegisterTimingName;

    .line 21
    new-instance v0, Lcom/squareup/analytics/RegisterTimingName;

    const/16 v13, 0xc

    const-string v14, "TRANSACTION_TIME"

    const-string v15, "Transaction Metrics: time to complete transaction"

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/analytics/RegisterTimingName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->TRANSACTION_TIME:Lcom/squareup/analytics/RegisterTimingName;

    .line 22
    new-instance v0, Lcom/squareup/analytics/RegisterTimingName;

    const/16 v14, 0xd

    const-string v15, "BUNDLE_PERSISTENCE_TIME"

    const-string v13, "Persistent Bundle Metrics: time to save a bundle"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/analytics/RegisterTimingName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->BUNDLE_PERSISTENCE_TIME:Lcom/squareup/analytics/RegisterTimingName;

    const/16 v0, 0xe

    new-array v0, v0, [Lcom/squareup/analytics/RegisterTimingName;

    .line 8
    sget-object v13, Lcom/squareup/analytics/RegisterTimingName;->MAIN_THREAD_BLOCKED:Lcom/squareup/analytics/RegisterTimingName;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->SERVER_CALL:Lcom/squareup/analytics/RegisterTimingName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->STARTUP_TIME_TO_INTERACT:Lcom/squareup/analytics/RegisterTimingName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->CART_INTERACTION_ITEM:Lcom/squareup/analytics/RegisterTimingName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->CART_INTERACTION_DISCOUNT:Lcom/squareup/analytics/RegisterTimingName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->CART_INTERACTION_ALL_DISCOUNTS:Lcom/squareup/analytics/RegisterTimingName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->CART_INTERACTION_ALL_TAXES:Lcom/squareup/analytics/RegisterTimingName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->ITEMS_SYNC:Lcom/squareup/analytics/RegisterTimingName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->OPEN_TICKETS_TICKET_IN_CART_UI:Lcom/squareup/analytics/RegisterTimingName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->OPEN_TICKETS_LIST_RESPONSE_TO_LIST_UI:Lcom/squareup/analytics/RegisterTimingName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->OPEN_TICKETS_TICKET_SELECTED_TO_CART_UI:Lcom/squareup/analytics/RegisterTimingName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->SCREEN_TIMED_OUT:Lcom/squareup/analytics/RegisterTimingName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->TRANSACTION_TIME:Lcom/squareup/analytics/RegisterTimingName;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->BUNDLE_PERSISTENCE_TIME:Lcom/squareup/analytics/RegisterTimingName;

    aput-object v1, v0, v14

    sput-object v0, Lcom/squareup/analytics/RegisterTimingName;->$VALUES:[Lcom/squareup/analytics/RegisterTimingName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput-object p3, p0, Lcom/squareup/analytics/RegisterTimingName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/analytics/RegisterTimingName;
    .locals 1

    .line 8
    const-class v0, Lcom/squareup/analytics/RegisterTimingName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/analytics/RegisterTimingName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/analytics/RegisterTimingName;
    .locals 1

    .line 8
    sget-object v0, Lcom/squareup/analytics/RegisterTimingName;->$VALUES:[Lcom/squareup/analytics/RegisterTimingName;

    invoke-virtual {v0}, [Lcom/squareup/analytics/RegisterTimingName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/analytics/RegisterTimingName;

    return-object v0
.end method
