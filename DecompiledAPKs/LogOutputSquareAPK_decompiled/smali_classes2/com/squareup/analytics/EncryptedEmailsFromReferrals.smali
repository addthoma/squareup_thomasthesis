.class public Lcom/squareup/analytics/EncryptedEmailsFromReferrals;
.super Ljava/lang/Object;
.source "EncryptedEmailsFromReferrals.java"


# instance fields
.field private final encryptedEmails:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/analytics/EncryptedEmailsFromReferrals;->encryptedEmails:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method


# virtual methods
.method public encryptedEmails()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/analytics/EncryptedEmailsFromReferrals;->encryptedEmails:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public postNewEmail(Ljava/lang/String;)V
    .locals 1

    .line 27
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 28
    iget-object v0, p0, Lcom/squareup/analytics/EncryptedEmailsFromReferrals;->encryptedEmails:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
