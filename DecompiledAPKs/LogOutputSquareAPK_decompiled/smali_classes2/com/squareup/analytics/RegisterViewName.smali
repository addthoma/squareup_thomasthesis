.class public final enum Lcom/squareup/analytics/RegisterViewName;
.super Ljava/lang/Enum;
.source "RegisterViewName.java"

# interfaces
.implements Lcom/squareup/analytics/EventNamedView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/analytics/RegisterViewName;",
        ">;",
        "Lcom/squareup/analytics/EventNamedView;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACCOUNT_HOME:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_ACTIVATE_VIA_WEB:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_ACTIVATE_YOUR_ACCOUNT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_BUSINESS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_BUSINESS_ADDRESS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_CATEGORY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_CONTACTLESS_ORDER_COMFIRMATION_PAID:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_CONTACTLESS_PAYMENT_INPUT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_CONTACTLESS_PAYMENT_PAID:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_CONTACTLESS_SHIPPING_CONFIRMATION:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_CONTACTLESS_SHIPPING_PAID:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_CONTACTLESS_SHIPPING_PAID_R12_ONLY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_CONTACTLESS_UPSELL_PAID:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_EDIT_SHIPPING:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_END:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_GET_READER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_INITIAL_SHIPPING:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_INTENT_HOW:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_INTENT_WHERE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_LINK_BANK:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_LINK_BANK_COMPLETE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_NEW_ACTIVATE_YOUR_ACCOUNT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_PERSONAL:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_QUIZ:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_RETRY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_SEND_READER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_SUBCATEGORY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVATION_VERIFY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ACTIVITY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum BIZBANK_BALANCE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CARD_ON_FILE_LINK_CARD_REQUEST:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CARD_ON_FILE_LINK_CARD_REQUEST_FAILURE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CARD_ON_FILE_LINK_CARD_REQUEST_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CASH_MANAGEMENT_END_DRAWER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CASH_PAYMENT_TUTORIAL_DECLINE_DIALOG:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CASH_PAYMENT_TUTORIAL_FINISH_DIALOG:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CHECKOUT_KEYPAD_VIEW:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CHECKOUT_LIBRARY_VIEW:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum COLLECT_CASH_DIALOG:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum COUPONS_REDEEM_MULTIPLE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum COUPONS_REDEEM_NONE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum COUPONS_REDEEM_ONE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum COUPONS_SEARCH:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CREATE_ITEM_TUTORIAL_ADD_ITEM_TO_GRID:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CREATE_ITEM_TUTORIAL_FIRST_ITEM_CREATED:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CREATE_ITEM_TUTORIAL_ITEM_SETUP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CREATE_ITEM_TUTORIAL_SHOW_MODAL_DONE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CREATE_ITEM_TUTORIAL_SHOW_MODAL_VIA_ONBOARDING:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CREATE_ITEM_TUTORIAL_SHOW_MODAL_VIA_SUPPORT_APPLET:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CREATE_ITEM_TUTORIAL_SHOW_SKIP_MODAL:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_ADDING_CUSTOMERS_TO_GROUP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_ADD_CUSTOMERS_TO_GROUP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_ALL_FREQUENT_ITEMS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_ALL_NOTES:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_BILL_HISTORY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_CHOOSE_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_CHOOSE_FILTER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_CHOOSE_FILTERS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_CHOOSE_GROUPS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_CONVERSATION:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_CREATE_GROUP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_CREATE_NOTE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_CUSTOMER_ACTIVITY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_CUSTOMER_CUSTOMER_EMAIL:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_CUSTOMER_DIP_PROCCESSING:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_CUSTOMER_SAVE_CARD:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_CUSTOMER_VERIFY_ZIPCODE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_DELETING_CUSTOMERS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_EDIT_FILTER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_MERGE_CUSTOMERS_CONFIRMATION:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_MERGING_CUSTOMERS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_MERGING_DUPLICATES:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_MESSAGING_CREATE_CONVERSATION:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_MESSAGING_SHOW_CONVERSATION:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_MESSAGING_SHOW_CONVERSATION_LIST:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_REMINDER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_RESOLVE_DUPLICATES:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_REVIEW_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_SELECT_LOYALTY_PHONE_NUMBER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_SEND_MESSAGE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_UPDATE_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_V2_DIRECTORY_BULK_DELETE_CONFIRM:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_V2_DIRECTORY_CONTACTS_LIST:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_V2_DIRECTORY_CREATE_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_V2_DIRECTORY_CUSTOMER_PROFILE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_V2_DIRECTORY_CUSTOMER_PROFILE_CARD:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_V2_DIRECTORY_EDIT_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_V2_DIRECTORY_GROUPS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_V2_DIRECTORY_GROUPS_MANUAL_GROUP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_V2_DIRECTORY_GROUPS_SMART_GROUP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_V2_DIRECTORY_GROUPS_UPDATE_GROUP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CRM_VIEW_NOTE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CUSTOMER_CHECKOUT_BUYER_CART:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum CUSTOMER_CHECKOUT_PAYMENT_PROMPT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum DELEGATED_LOCKOUT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum DEMO:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum DEPOSITS_REPORT_DETAIL:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum DEPOSITS_REPORT_SUMMARY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum EMPLOYEES_APPLET_CREATE_EMPLOYEE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum EMPLOYEES_APPLET_MASTER_SCREEN:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum EMPLOYEES_APPLET_UPDATE_EMPLOYEE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum FEATURE_TOUR:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum FEE_TUTORIAL_DIP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum FEE_TUTORIAL_DIP_JCB:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum FEE_TUTORIAL_INTERAC:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum FEE_TUTORIAL_MANUAL:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum FEE_TUTORIAL_SWIPE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum FEE_TUTORIAL_TAP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum GAP_TIME_EDUCATION:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum HELP_FLOW_HISTORY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum HELP_FLOW_ORDER_READER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum HELP_FLOW_ORDER_READER_MAGSTRIPE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum HELP_FLOW_ORDER_READER_MAGSTRIPE_CONFIRM:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum HELP_FLOW_REFERRAL:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum HELP_FLOW_RETAIL_MAP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum INSTANT_DEPOSIT_DEPOSITS_REPORT_DEPOSITING_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum INSTANT_DEPOSIT_DEPOSITS_REPORT_DISPLAYED_AVAILABLE_BALANCE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum INSTANT_DEPOSIT_DEPOSITS_REPORT_DISPLAYED_ERROR:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum INSTANT_DEPOSIT_HEADER_DEPOSITING_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum INSTANT_DEPOSIT_HEADER_DISPLAYED_SHOWING_AVAILABLE_BALANCE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum INSTANT_DEPOSIT_HEADER_DISPLAYED_SHOWING_ERROR:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum INSTANT_DEPOSIT_LINK_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum INSTANT_DEPOSIT_RESEND_EMAIL_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum INVOICES_APPLET_INVOICE_DETAILS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum INVOICES_BILL_HISTORY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum INVOICES_CREATION_FORM:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum INVOICES_LIST:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum INVOICES_PROMPT_IDV:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ISSUE_CONTACTLESS_CARD_PRESENT_REFUND:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ISSUE_REFUND:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ITEMS_APPLET_EDIT_CATEGORY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ITEMS_APPLET_EDIT_DISCOUNT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ITEMS_APPLET_EDIT_ITEM:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ITEMS_APPLET_EDIT_MODIFIER_SET:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ITEMS_APPLET_MASTER_SCREEN:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum LEARN_ABOURT_SQUARE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum LOYALTY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum LOYALTY_ENROLL_BUYER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum LOYALTY_ENROLL_MERCHANT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum LOYALTY_REPORT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum LOYALTY_STATUS_PROGRESS_REWARD_BUYER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum LOYALTY_STATUS_PROGRESS_REWARD_MERCHANT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum MESSAGE_CENTER_LIST:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum MESSAGE_CENTER_MESSAGE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum O1_READER_DEPRECATION_NOTICE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ONBOARDING_VERTICAL_SELECTION:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ORDERS_BILL_HISTORY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum ORDER_ENTRY_ITEM_SUGGESTIONS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_AUTHORIZING:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_CARD:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_CASH:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_CHOOSE_COF_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_COUPON:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_EMAIL_COLLECTION:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_EMV_AUTHORIZING:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_EMV_BEGIN:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_EMV_CONFIRM:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_EMV_ENTER_PIN:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_EMV_SELECT_ACCOUNT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_ENTER_TICKET_AFTER_AUTH:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_ENTER_TICKET_BEFORE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_INVOICE_PAYMENT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_MAIN:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_OTHER_TENDER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_PROCESSING:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_RECEIPT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_SIGNATURE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_SPLIT_TENDER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_SPLIT_TENDER_CASH:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_SPLIT_TENDER_OTHER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_SPLIT_TENDER_THIRD_PARTY_CARD:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_THIRD_PARTY_CARD:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_FLOW_TIP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_TUTORIAL_BANNER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_TUTORIAL_DECLINE_DIALOG:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYMENT_TUTORIAL_FINISH_DIALOG:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PAYROLL_UPSELL:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PREFILL_EMAIL_ADDRESS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum PRINT_ERROR_POPUP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum R12_MEET_THE_READER_MODAL:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum R12_MULTIPAGE_WALKTHROUGH_PAGE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum R12_NATIVE_ORDER_ADDRESS_PAGE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum R12_NATIVE_ORDER_CHARGE_PAGE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum R12_NATIVE_ORDER_DETAILS_PAGE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum R12_NATIVE_ORDER_STATUS_PAGE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum REISSUE_RECEIPT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum REPORTS_CURRENT_DRAWER_CASH_MANAGEMENT_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum REPORTS_CUSTOM_SALES_REPORT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum REPORTS_DEFAULT_SALES_REPORT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SALES_REPORT_VIEW_SALES_REPORT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SCALE_ERROR:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELECT_CATEGORY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_CART:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_CART_CONTAINER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_CART_DISCOUNTS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_CART_TAXES:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_CONFIGURE_ITEM_COMP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_CONFIGURE_ITEM_DETAIL:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_CONFIGURE_ITEM_PRICE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_CONFIGURE_ITEM_VOID:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_DISCOUNT_ENTRY_MONEY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_DISCOUNT_ENTRY_PERCENT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_FAVORITE_PAGE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_GIFT_CARD_ACTIVATION:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_ITEM_LIST:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_KEYPAD_PANEL:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_LIBRARY_LIST:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_PERMISSION_PASSCODE_POPUP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_PRICE_ENTRY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_TICKET_COMP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SELLER_FLOW_TICKET_VOID:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SERVICES_TAX:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_BANK_ACCOUNT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_BARCODE_SCANNERS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_CASH:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_CASH_DRAWERS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_CURRENT_DRAWER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_CUSTOMER_DISPLAY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_CUSTOMER_MANAGEMENT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_DRAWER_HISTORY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_DRAWER_REPORT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_EDIT_TICKET_GROUP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_EMAIL_COLLECTION:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_EMPLOYEE_MANAGEMENT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_GIFTCARDS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_INSTANT_DEPOSIT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_INSTANT_DEPOSIT_LINK_CARD:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_INSTANT_DEPOSIT_LINK_CARD_RESULT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_LIST:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_LOYALTY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_OFFLINE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_ONLINE_CHECKOUT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_OPEN_TICKET:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_PAID:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_PRINTER_EDIT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_PRINTER_LIST:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_PUBLIC_PROFILE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_PUBLIC_PROFILE_ADDRESS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_SALES_SUMMARY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_SHARED_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_SIGNATURE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_STAND:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_SUPPORT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_SURCHARGING:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_SWIPE_CHIP_CARDS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_SWIPE_CHIP_CARDS_ENABLE_SCREEN:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_TAX_EDIT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_TAX_LIST:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_TILE_APPEARANCE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_TIPPING:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SETTINGS_USE_PREDEFINED_TICKETS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SPLIT_TENDER_WARNING_DIALOG:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum START_SHIFT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SUPPORT_ABOUT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SUPPORT_ANNOUNCEMENTS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SUPPORT_ANNOUNCEMENT_DETAILS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SUPPORT_CONTACT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SUPPORT_HARDWARE_DISCLAIMER:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SUPPORT_HELP:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SUPPORT_HELP_JEDI:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SUPPORT_LEGAL:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SUPPORT_LIBRARIES:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SUPPORT_LICENSES:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SUPPORT_MESSAGES:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SUPPORT_TROUBLESHOOTING:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SUPPORT_TUTORIALS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SYSTEM_PERMISSIONS_ABOUT_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SYSTEM_PERMISSIONS_ENABLE_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum SYSTEM_PERMISSIONS_PERMISSION_DENIED:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TAXES_CREATE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_ERROR_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_ERROR_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_ERROR_DEFAULT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_ERROR_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_ERROR_NO_INTERNET:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_ERROR_START_BREAK:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_LOADING:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_PASSCODE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_REMINDER_SELECT_ACTION_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_REMINDER_SELECT_ACTION_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_REMINDER_SUCCESS_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_REMINDER_SUCCESS_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_SELECT_ACTION_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_SELECT_ACTION_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_SELECT_ACTION_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_SELECT_ACTION_JOBS_LIST:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_SELECT_ACTION_LIST_BREAKS:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_SELECT_ACTION_START_BREAK_OR_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_SUCCESS_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_SUCCESS_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_SUCCESS_CLOCK_OUT_SUMMARY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_SUCCESS_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TIMECARDS_SUCCESS_START_BREAK:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum TRANSACTION_DETAIL:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum UNKNOWN:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_COUNTRY:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_CREATE:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_CREATE_PASSWORD:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_DEVICE_CODE_SIGN_IN:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_LANDING:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_RESET:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_SIGN_IN:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_SIGN_IN_APP_LANDING:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_SPLASH:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_SPLASH_PAGE_1:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_SPLASH_PAGE_2:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_SPLASH_PAGE_3:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_SPLASH_PAGE_4:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_WORLD_LANDING:Lcom/squareup/analytics/RegisterViewName;

.field public static final enum WELCOME_X2_LANDING:Lcom/squareup/analytics/RegisterViewName;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 9
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/4 v1, 0x0

    const-string v2, "ACCOUNT_HOME"

    const-string v3, "Settings Account Settings"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACCOUNT_HOME:Lcom/squareup/analytics/RegisterViewName;

    .line 10
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/4 v2, 0x1

    const-string v3, "ACTIVITY"

    const-string v4, "Settings All Activity"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVITY:Lcom/squareup/analytics/RegisterViewName;

    .line 11
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/4 v3, 0x2

    const-string v4, "ACTIVATION_ACTIVATE_VIA_WEB"

    const-string v5, "Activation Flow Activate Via Web"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_ACTIVATE_VIA_WEB:Lcom/squareup/analytics/RegisterViewName;

    .line 12
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/4 v4, 0x3

    const-string v5, "ACTIVATION_ACTIVATE_YOUR_ACCOUNT"

    const-string v6, "Activation Flow Activate Your Account"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_ACTIVATE_YOUR_ACCOUNT:Lcom/squareup/analytics/RegisterViewName;

    .line 13
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/4 v5, 0x4

    const-string v6, "ACTIVATION_BUSINESS"

    const-string v7, "Activation Flow Business Information"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_BUSINESS:Lcom/squareup/analytics/RegisterViewName;

    .line 14
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/4 v6, 0x5

    const-string v7, "ACTIVATION_BUSINESS_ADDRESS"

    const-string v8, "Onboard: Activation Flow Business Address"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_BUSINESS_ADDRESS:Lcom/squareup/analytics/RegisterViewName;

    .line 15
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/4 v7, 0x6

    const-string v8, "ACTIVATION_CATEGORY"

    const-string v9, "Activation Flow MCC Category"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CATEGORY:Lcom/squareup/analytics/RegisterViewName;

    .line 16
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/4 v8, 0x7

    const-string v9, "ACTIVATION_EDIT_SHIPPING"

    const-string v10, "Activation Flow Edit Shipping Information"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_EDIT_SHIPPING:Lcom/squareup/analytics/RegisterViewName;

    .line 17
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/16 v9, 0x8

    const-string v10, "ACTIVATION_END"

    const-string v11, "Activation Flow Congratulations"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_END:Lcom/squareup/analytics/RegisterViewName;

    .line 18
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/16 v10, 0x9

    const-string v11, "ACTIVATION_GET_READER"

    const-string v12, "Activation Get Reader"

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_GET_READER:Lcom/squareup/analytics/RegisterViewName;

    .line 19
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/16 v11, 0xa

    const-string v12, "ACTIVATION_INITIAL_SHIPPING"

    const-string v13, "Activation Flow Initial Shipping Information"

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_INITIAL_SHIPPING:Lcom/squareup/analytics/RegisterViewName;

    .line 20
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/16 v12, 0xb

    const-string v13, "ACTIVATION_INTENT_HOW"

    const-string v14, "Activation Product Intent Payment Type"

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_INTENT_HOW:Lcom/squareup/analytics/RegisterViewName;

    .line 21
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/16 v13, 0xc

    const-string v14, "ACTIVATION_INTENT_WHERE"

    const-string v15, "Activation Product Intent Location"

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_INTENT_WHERE:Lcom/squareup/analytics/RegisterViewName;

    .line 22
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/16 v14, 0xd

    const-string v15, "ACTIVATION_LINK_BANK"

    const-string v13, "Activation Flow Link Bank Account"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_LINK_BANK:Lcom/squareup/analytics/RegisterViewName;

    .line 23
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const/16 v13, 0xe

    const-string v15, "ACTIVATION_LINK_BANK_COMPLETE"

    const-string v14, "Activation Flow Link Bank Account Complete"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_LINK_BANK_COMPLETE:Lcom/squareup/analytics/RegisterViewName;

    .line 24
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v14, "ACTIVATION_NEW_ACTIVATE_YOUR_ACCOUNT"

    const/16 v15, 0xf

    const-string v13, "Activation Flow New Activate Your Account"

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_NEW_ACTIVATE_YOUR_ACCOUNT:Lcom/squareup/analytics/RegisterViewName;

    .line 25
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ACTIVATION_PERSONAL"

    const/16 v14, 0x10

    const-string v15, "Activation Flow Personal Information"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_PERSONAL:Lcom/squareup/analytics/RegisterViewName;

    .line 26
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ACTIVATION_QUIZ"

    const/16 v14, 0x11

    const-string v15, "Activation Flow Identity Quiz"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_QUIZ:Lcom/squareup/analytics/RegisterViewName;

    .line 27
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ACTIVATION_CONTACTLESS_ORDER_COMFIRMATION_PAID"

    const/16 v14, 0x12

    const-string v15, "Activation Flow Paid R12 Payment Confirmation with No Rebate"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CONTACTLESS_ORDER_COMFIRMATION_PAID:Lcom/squareup/analytics/RegisterViewName;

    .line 29
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ACTIVATION_CONTACTLESS_PAYMENT_INPUT"

    const/16 v14, 0x13

    const-string v15, "Activation Flow Paid R12 Payment Input"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CONTACTLESS_PAYMENT_INPUT:Lcom/squareup/analytics/RegisterViewName;

    .line 30
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ACTIVATION_CONTACTLESS_SHIPPING_CONFIRMATION"

    const/16 v14, 0x14

    const-string v15, "Activation Flow Paid R12 Shipping Confirmation"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CONTACTLESS_SHIPPING_CONFIRMATION:Lcom/squareup/analytics/RegisterViewName;

    .line 31
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ACTIVATION_CONTACTLESS_SHIPPING_PAID"

    const/16 v14, 0x15

    const-string v15, "Activation Flow Paid R12 Shipping Info"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CONTACTLESS_SHIPPING_PAID:Lcom/squareup/analytics/RegisterViewName;

    .line 32
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ACTIVATION_CONTACTLESS_SHIPPING_PAID_R12_ONLY"

    const/16 v14, 0x16

    const-string v15, "Activation Flow Paid R12 Shipping Info with no R4 shipped"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CONTACTLESS_SHIPPING_PAID_R12_ONLY:Lcom/squareup/analytics/RegisterViewName;

    .line 34
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ACTIVATION_CONTACTLESS_UPSELL_PAID"

    const/16 v14, 0x17

    const-string v15, "Activation Flow Paid R12 Pre-order with No Rebate"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CONTACTLESS_UPSELL_PAID:Lcom/squareup/analytics/RegisterViewName;

    .line 35
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ACTIVATION_CONTACTLESS_PAYMENT_PAID"

    const/16 v14, 0x18

    const-string v15, "Activation Flow Paid R12 Payment with No Rebate"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CONTACTLESS_PAYMENT_PAID:Lcom/squareup/analytics/RegisterViewName;

    .line 36
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ACTIVATION_RETRY"

    const/16 v14, 0x19

    const-string v15, "Activation Flow Retry"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_RETRY:Lcom/squareup/analytics/RegisterViewName;

    .line 37
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ACTIVATION_SEND_READER"

    const/16 v14, 0x1a

    const-string v15, "Activation Flow Send Reader"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_SEND_READER:Lcom/squareup/analytics/RegisterViewName;

    .line 38
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ACTIVATION_SUBCATEGORY"

    const/16 v14, 0x1b

    const-string v15, "Activation Flow MCC Subcategory"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_SUBCATEGORY:Lcom/squareup/analytics/RegisterViewName;

    .line 39
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ACTIVATION_VERIFY"

    const/16 v14, 0x1c

    const-string v15, "Activation Flow Verify SSN"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_VERIFY:Lcom/squareup/analytics/RegisterViewName;

    .line 40
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "BIZBANK_BALANCE"

    const/16 v14, 0x1d

    const-string v15, "Bizbank: Balance"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->BIZBANK_BALANCE:Lcom/squareup/analytics/RegisterViewName;

    .line 41
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CARD_ON_FILE_LINK_CARD_REQUEST"

    const/16 v14, 0x1e

    const-string v15, "Link Card Request Authorization"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CARD_ON_FILE_LINK_CARD_REQUEST:Lcom/squareup/analytics/RegisterViewName;

    .line 42
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CARD_ON_FILE_LINK_CARD_REQUEST_FAILURE"

    const/16 v14, 0x1f

    const-string v15, "Link Card Request Failure"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CARD_ON_FILE_LINK_CARD_REQUEST_FAILURE:Lcom/squareup/analytics/RegisterViewName;

    .line 43
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CARD_ON_FILE_LINK_CARD_REQUEST_SUCCESS"

    const/16 v14, 0x20

    const-string v15, "Link Card Request Success"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CARD_ON_FILE_LINK_CARD_REQUEST_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

    .line 44
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CASH_MANAGEMENT_END_DRAWER"

    const/16 v14, 0x21

    const-string v15, "Cash Management: End Drawer View"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CASH_MANAGEMENT_END_DRAWER:Lcom/squareup/analytics/RegisterViewName;

    .line 45
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CASH_PAYMENT_TUTORIAL_DECLINE_DIALOG"

    const/16 v14, 0x22

    const-string v15, "Cash Payment Tutorial Decline Bookend"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CASH_PAYMENT_TUTORIAL_DECLINE_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    .line 46
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CASH_PAYMENT_TUTORIAL_FINISH_DIALOG"

    const/16 v14, 0x23

    const-string v15, "Cash Payment Tutorial Finish Bookend"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CASH_PAYMENT_TUTORIAL_FINISH_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    .line 47
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CHECKOUT_KEYPAD_VIEW"

    const/16 v14, 0x24

    const-string v15, "Checkout: Keypad Tab Viewed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CHECKOUT_KEYPAD_VIEW:Lcom/squareup/analytics/RegisterViewName;

    .line 48
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CHECKOUT_LIBRARY_VIEW"

    const/16 v14, 0x25

    const-string v15, "Checkout: Library Tab Viewed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CHECKOUT_LIBRARY_VIEW:Lcom/squareup/analytics/RegisterViewName;

    .line 49
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "COLLECT_CASH_DIALOG"

    const/16 v14, 0x26

    const-string v15, "Accidental Cash Modal: Shown"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->COLLECT_CASH_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    .line 50
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "COUPONS_REDEEM_MULTIPLE"

    const/16 v14, 0x27

    const-string v15, "Coupons: Redeem Multiple"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->COUPONS_REDEEM_MULTIPLE:Lcom/squareup/analytics/RegisterViewName;

    .line 51
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "COUPONS_REDEEM_NONE"

    const/16 v14, 0x28

    const-string v15, "Coupons: Redeem None"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->COUPONS_REDEEM_NONE:Lcom/squareup/analytics/RegisterViewName;

    .line 52
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "COUPONS_REDEEM_ONE"

    const/16 v14, 0x29

    const-string v15, "Coupons: Redeem One"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->COUPONS_REDEEM_ONE:Lcom/squareup/analytics/RegisterViewName;

    .line 53
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "COUPONS_SEARCH"

    const/16 v14, 0x2a

    const-string v15, "Coupons: Search"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->COUPONS_SEARCH:Lcom/squareup/analytics/RegisterViewName;

    .line 54
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CREATE_ITEM_TUTORIAL_ADD_ITEM_TO_GRID"

    const/16 v14, 0x2b

    const-string v15, "Items Tutorial: Add to Item Grid"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_ADD_ITEM_TO_GRID:Lcom/squareup/analytics/RegisterViewName;

    .line 55
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CREATE_ITEM_TUTORIAL_FIRST_ITEM_CREATED"

    const/16 v14, 0x2c

    const-string v15, "Items Tutorial: You\'ve Created Your First Item"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_FIRST_ITEM_CREATED:Lcom/squareup/analytics/RegisterViewName;

    .line 56
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CREATE_ITEM_TUTORIAL_ITEM_SETUP"

    const/16 v14, 0x2d

    const-string v15, "Items Tutorial: Item Setup"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_ITEM_SETUP:Lcom/squareup/analytics/RegisterViewName;

    .line 57
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CREATE_ITEM_TUTORIAL_SHOW_MODAL_DONE"

    const/16 v14, 0x2e

    const-string v15, "Items Tutorial: You\'ve Created Your First Item"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_SHOW_MODAL_DONE:Lcom/squareup/analytics/RegisterViewName;

    .line 58
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CREATE_ITEM_TUTORIAL_SHOW_MODAL_VIA_ONBOARDING"

    const/16 v14, 0x2f

    const-string v15, "Items Tutorial: Show Modal Via Onboarding"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_SHOW_MODAL_VIA_ONBOARDING:Lcom/squareup/analytics/RegisterViewName;

    .line 59
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CREATE_ITEM_TUTORIAL_SHOW_MODAL_VIA_SUPPORT_APPLET"

    const/16 v14, 0x30

    const-string v15, "Items Tutorial: Show Modal Via Support Applet"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_SHOW_MODAL_VIA_SUPPORT_APPLET:Lcom/squareup/analytics/RegisterViewName;

    .line 61
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CREATE_ITEM_TUTORIAL_SHOW_SKIP_MODAL"

    const/16 v14, 0x31

    const-string v15, "Items Tutorial: Exit Tutorial Modal Shown"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_SHOW_SKIP_MODAL:Lcom/squareup/analytics/RegisterViewName;

    .line 62
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_ADD_CUSTOMERS_TO_GROUP"

    const/16 v14, 0x32

    const-string v15, "CRM: Add Customers To Group"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_ADD_CUSTOMERS_TO_GROUP:Lcom/squareup/analytics/RegisterViewName;

    .line 63
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_ADDING_CUSTOMERS_TO_GROUP"

    const/16 v14, 0x33

    const-string v15, "CRM: Adding Customers To Group"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_ADDING_CUSTOMERS_TO_GROUP:Lcom/squareup/analytics/RegisterViewName;

    .line 64
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_ALL_NOTES"

    const/16 v14, 0x34

    const-string v15, "Customer Management: All Notes Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_ALL_NOTES:Lcom/squareup/analytics/RegisterViewName;

    .line 65
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_ALL_FREQUENT_ITEMS"

    const/16 v14, 0x35

    const-string v15, "CRM: All Frequent Items Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_ALL_FREQUENT_ITEMS:Lcom/squareup/analytics/RegisterViewName;

    .line 66
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_CHOOSE_FILTER"

    const/16 v14, 0x36

    const-string v15, "CRM Filtering: Select filter modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_FILTER:Lcom/squareup/analytics/RegisterViewName;

    .line 67
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_CHOOSE_FILTERS"

    const/16 v14, 0x37

    const-string v15, "CRM Filtering: Applied filters modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_FILTERS:Lcom/squareup/analytics/RegisterViewName;

    .line 68
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_CHOOSE_CUSTOMER"

    const/16 v14, 0x38

    const-string v15, "Customer Management: Choose Customer Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    .line 69
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_CHOOSE_GROUPS"

    const/16 v14, 0x39

    const-string v15, "Customer Management: Choose Groups Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_GROUPS:Lcom/squareup/analytics/RegisterViewName;

    .line 70
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_CONVERSATION"

    const/16 v14, 0x3a

    const-string v15, "Customer Management: Conversation Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CONVERSATION:Lcom/squareup/analytics/RegisterViewName;

    .line 71
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_CREATE_GROUP"

    const/16 v14, 0x3b

    const-string v15, "Customer Management: Create Group Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CREATE_GROUP:Lcom/squareup/analytics/RegisterViewName;

    .line 72
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_CREATE_NOTE"

    const/16 v14, 0x3c

    const-string v15, "Customer Management: Create Note Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CREATE_NOTE:Lcom/squareup/analytics/RegisterViewName;

    .line 73
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_CUSTOMER_ACTIVITY"

    const/16 v14, 0x3d

    const-string v15, "Customer Management: Customer Activity Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CUSTOMER_ACTIVITY:Lcom/squareup/analytics/RegisterViewName;

    .line 74
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_DELETING_CUSTOMERS"

    const/16 v14, 0x3e

    const-string v15, "CRM: Deleting Customers"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_DELETING_CUSTOMERS:Lcom/squareup/analytics/RegisterViewName;

    .line 75
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_EDIT_FILTER"

    const/16 v14, 0x3f

    const-string v15, "CRM Filtering: Edit filter modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_EDIT_FILTER:Lcom/squareup/analytics/RegisterViewName;

    .line 76
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_RESOLVE_DUPLICATES"

    const/16 v14, 0x40

    const-string v15, "CRM: Resolve Duplicates"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_RESOLVE_DUPLICATES:Lcom/squareup/analytics/RegisterViewName;

    .line 77
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_REVIEW_CUSTOMER"

    const/16 v14, 0x41

    const-string v15, "Customer Management: Review Customer Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_REVIEW_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    .line 78
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_UPDATE_CUSTOMER"

    const/16 v14, 0x42

    const-string v15, "Customer Engagement: Edit Contact Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_UPDATE_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    .line 79
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_CUSTOMER_SAVE_CARD"

    const/16 v14, 0x43

    const-string v15, "Customer Management: Save Card to Customer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CUSTOMER_SAVE_CARD:Lcom/squareup/analytics/RegisterViewName;

    .line 80
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_CUSTOMER_DIP_PROCCESSING"

    const/16 v14, 0x44

    const-string v15, "Customer Management: Reading Information from Dipped Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CUSTOMER_DIP_PROCCESSING:Lcom/squareup/analytics/RegisterViewName;

    .line 81
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_CUSTOMER_CUSTOMER_EMAIL"

    const/16 v14, 0x45

    const-string v15, "Customer Management: Enter Customer Email"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CUSTOMER_CUSTOMER_EMAIL:Lcom/squareup/analytics/RegisterViewName;

    .line 82
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_CUSTOMER_VERIFY_ZIPCODE"

    const/16 v14, 0x46

    const-string v15, "Customer Management: Verify Zip Code"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CUSTOMER_VERIFY_ZIPCODE:Lcom/squareup/analytics/RegisterViewName;

    .line 83
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_MERGE_CUSTOMERS_CONFIRMATION"

    const/16 v14, 0x47

    const-string v15, "CRM: Merge Customers Confirmation"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_MERGE_CUSTOMERS_CONFIRMATION:Lcom/squareup/analytics/RegisterViewName;

    .line 84
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_MERGING_CUSTOMERS"

    const/16 v14, 0x48

    const-string v15, "CRM: Merging Customers"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_MERGING_CUSTOMERS:Lcom/squareup/analytics/RegisterViewName;

    .line 85
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_MERGING_DUPLICATES"

    const/16 v14, 0x49

    const-string v15, "CRM: Merging Duplicates"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_MERGING_DUPLICATES:Lcom/squareup/analytics/RegisterViewName;

    .line 86
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_SELECT_LOYALTY_PHONE_NUMBER"

    const/16 v14, 0x4a

    const-string v15, "CRM: Select Loyalty Phone Number For Merge"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_SELECT_LOYALTY_PHONE_NUMBER:Lcom/squareup/analytics/RegisterViewName;

    .line 87
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_SEND_MESSAGE"

    const/16 v14, 0x4b

    const-string v15, "Customer Management: Send Message Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_SEND_MESSAGE:Lcom/squareup/analytics/RegisterViewName;

    .line 88
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_VIEW_NOTE"

    const/16 v14, 0x4c

    const-string v15, "Customer Management: View Note Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_VIEW_NOTE:Lcom/squareup/analytics/RegisterViewName;

    .line 89
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_REMINDER"

    const/16 v14, 0x4d

    const-string v15, "Customer Management: Reminder Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_REMINDER:Lcom/squareup/analytics/RegisterViewName;

    .line 90
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_BILL_HISTORY"

    const/16 v14, 0x4e

    const-string v15, "CRM: Bill History"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_BILL_HISTORY:Lcom/squareup/analytics/RegisterViewName;

    .line 91
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_MESSAGING_CREATE_CONVERSATION"

    const/16 v14, 0x4f

    const-string v15, "CRM Messaging: Create Conversation: View"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_MESSAGING_CREATE_CONVERSATION:Lcom/squareup/analytics/RegisterViewName;

    .line 92
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_MESSAGING_SHOW_CONVERSATION"

    const/16 v14, 0x50

    const-string v15, "CRM Messaging: Show Conversation: View"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_MESSAGING_SHOW_CONVERSATION:Lcom/squareup/analytics/RegisterViewName;

    .line 93
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_MESSAGING_SHOW_CONVERSATION_LIST"

    const/16 v14, 0x51

    const-string v15, "CRM Messaging: Conversation List: View"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_MESSAGING_SHOW_CONVERSATION_LIST:Lcom/squareup/analytics/RegisterViewName;

    .line 94
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_V2_DIRECTORY_BULK_DELETE_CONFIRM"

    const/16 v14, 0x52

    const-string v15, "Directory:Contact List:Bulk Delete"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_BULK_DELETE_CONFIRM:Lcom/squareup/analytics/RegisterViewName;

    .line 95
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_V2_DIRECTORY_CONTACTS_LIST"

    const/16 v14, 0x53

    const-string v15, "Directory:Contacts List"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_CONTACTS_LIST:Lcom/squareup/analytics/RegisterViewName;

    .line 96
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_V2_DIRECTORY_CREATE_CUSTOMER"

    const/16 v14, 0x54

    const-string v15, "Directory:Customer Profile:Create Customer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_CREATE_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    .line 97
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_V2_DIRECTORY_CUSTOMER_PROFILE"

    const/16 v14, 0x55

    const-string v15, "Directory:Customer Profile"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_CUSTOMER_PROFILE:Lcom/squareup/analytics/RegisterViewName;

    .line 98
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_V2_DIRECTORY_CUSTOMER_PROFILE_CARD"

    const/16 v14, 0x56

    const-string v15, "Directory:Customer Profile Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_CUSTOMER_PROFILE_CARD:Lcom/squareup/analytics/RegisterViewName;

    .line 99
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_V2_DIRECTORY_EDIT_CUSTOMER"

    const/16 v14, 0x57

    const-string v15, "Directory:Customer Profile:Edit Customer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_EDIT_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    .line 100
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_V2_DIRECTORY_GROUPS"

    const/16 v14, 0x58

    const-string v15, "Directory:Groups"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_GROUPS:Lcom/squareup/analytics/RegisterViewName;

    .line 101
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_V2_DIRECTORY_GROUPS_MANUAL_GROUP"

    const/16 v14, 0x59

    const-string v15, "Directory:Groups:Selected Manual Group"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_GROUPS_MANUAL_GROUP:Lcom/squareup/analytics/RegisterViewName;

    .line 102
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_V2_DIRECTORY_GROUPS_SMART_GROUP"

    const/16 v14, 0x5a

    const-string v15, "Directory:Groups:Selected Smart Group"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_GROUPS_SMART_GROUP:Lcom/squareup/analytics/RegisterViewName;

    .line 103
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CRM_V2_DIRECTORY_GROUPS_UPDATE_GROUP"

    const/16 v14, 0x5b

    const-string v15, "Directory:Groups:Update Group"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_GROUPS_UPDATE_GROUP:Lcom/squareup/analytics/RegisterViewName;

    .line 104
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CUSTOMER_CHECKOUT_BUYER_CART"

    const/16 v14, 0x5c

    const-string v15, "Customer Checkout: View Buyer Cart Screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CUSTOMER_CHECKOUT_BUYER_CART:Lcom/squareup/analytics/RegisterViewName;

    .line 105
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "CUSTOMER_CHECKOUT_PAYMENT_PROMPT"

    const/16 v14, 0x5d

    const-string v15, "Customer Checkout: View Payment Prompt Screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->CUSTOMER_CHECKOUT_PAYMENT_PROMPT:Lcom/squareup/analytics/RegisterViewName;

    .line 106
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "DELEGATED_LOCKOUT"

    const/16 v14, 0x5e

    const-string v15, "Delegated Lockout Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->DELEGATED_LOCKOUT:Lcom/squareup/analytics/RegisterViewName;

    .line 107
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "DEMO"

    const/16 v14, 0x5f

    const-string v15, "Demo offered"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->DEMO:Lcom/squareup/analytics/RegisterViewName;

    .line 108
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "DEPOSITS_REPORT_SUMMARY"

    const/16 v14, 0x60

    const-string v15, "Deposits Report: Summary"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->DEPOSITS_REPORT_SUMMARY:Lcom/squareup/analytics/RegisterViewName;

    .line 109
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "DEPOSITS_REPORT_DETAIL"

    const/16 v14, 0x61

    const-string v15, "Deposits Report: Detail"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->DEPOSITS_REPORT_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    .line 110
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "EMPLOYEES_APPLET_MASTER_SCREEN"

    const/16 v14, 0x62

    const-string v15, "Employees Applet: Master Screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->EMPLOYEES_APPLET_MASTER_SCREEN:Lcom/squareup/analytics/RegisterViewName;

    .line 111
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "EMPLOYEES_APPLET_CREATE_EMPLOYEE"

    const/16 v14, 0x63

    const-string v15, "Employees Applet: Create Employee Sheet"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->EMPLOYEES_APPLET_CREATE_EMPLOYEE:Lcom/squareup/analytics/RegisterViewName;

    .line 112
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "EMPLOYEES_APPLET_UPDATE_EMPLOYEE"

    const/16 v14, 0x64

    const-string v15, "Employees Applet: Update Employee Sheet"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->EMPLOYEES_APPLET_UPDATE_EMPLOYEE:Lcom/squareup/analytics/RegisterViewName;

    .line 113
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "FEATURE_TOUR"

    const/16 v14, 0x65

    const-string v15, "Feature Tour"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->FEATURE_TOUR:Lcom/squareup/analytics/RegisterViewName;

    .line 114
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "FEE_TUTORIAL_DIP"

    const/16 v14, 0x66

    const-string v15, "Fee Tutorial - Dip or Tap"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_DIP:Lcom/squareup/analytics/RegisterViewName;

    .line 115
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "FEE_TUTORIAL_DIP_JCB"

    const/16 v14, 0x67

    const-string v15, "Fee Tutorial - Dip JCB"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_DIP_JCB:Lcom/squareup/analytics/RegisterViewName;

    .line 116
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "FEE_TUTORIAL_INTERAC"

    const/16 v14, 0x68

    const-string v15, "Fee Tutorial - Interac"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_INTERAC:Lcom/squareup/analytics/RegisterViewName;

    .line 117
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "FEE_TUTORIAL_MANUAL"

    const/16 v14, 0x69

    const-string v15, "Fee Tutorial - Keyed-in"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_MANUAL:Lcom/squareup/analytics/RegisterViewName;

    .line 118
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "FEE_TUTORIAL_SWIPE"

    const/16 v14, 0x6a

    const-string v15, "Fee Tutorial - Swipe"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_SWIPE:Lcom/squareup/analytics/RegisterViewName;

    .line 119
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "FEE_TUTORIAL_TAP"

    const/16 v14, 0x6b

    const-string v15, "Fee Tutorial - Tap"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_TAP:Lcom/squareup/analytics/RegisterViewName;

    .line 120
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "GAP_TIME_EDUCATION"

    const/16 v14, 0x6c

    const-string v15, "Gap Time Education"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->GAP_TIME_EDUCATION:Lcom/squareup/analytics/RegisterViewName;

    .line 121
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "HELP_FLOW_HISTORY"

    const/16 v14, 0x6d

    const-string v15, "Help Flow: Order History"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_HISTORY:Lcom/squareup/analytics/RegisterViewName;

    .line 122
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "HELP_FLOW_ORDER_READER"

    const/16 v14, 0x6e

    const-string v15, "Help Flow: Order Reader"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_ORDER_READER:Lcom/squareup/analytics/RegisterViewName;

    .line 123
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "HELP_FLOW_ORDER_READER_MAGSTRIPE"

    const/16 v14, 0x6f

    const-string v15, "Help Flow: Order Reader Magstripe"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_ORDER_READER_MAGSTRIPE:Lcom/squareup/analytics/RegisterViewName;

    .line 124
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "HELP_FLOW_ORDER_READER_MAGSTRIPE_CONFIRM"

    const/16 v14, 0x70

    const-string v15, "Help Flow: Order Reader Magstripe Confirm"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_ORDER_READER_MAGSTRIPE_CONFIRM:Lcom/squareup/analytics/RegisterViewName;

    .line 125
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "HELP_FLOW_REFERRAL"

    const/16 v14, 0x71

    const-string v15, "Help Flow: Referral"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_REFERRAL:Lcom/squareup/analytics/RegisterViewName;

    .line 126
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "HELP_FLOW_RETAIL_MAP"

    const/16 v14, 0x72

    const-string v15, "Help Flow: Retail Map"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_RETAIL_MAP:Lcom/squareup/analytics/RegisterViewName;

    .line 127
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "INSTANT_DEPOSIT_DEPOSITS_REPORT_DEPOSITING_SUCCESS"

    const/16 v14, 0x73

    const-string v15, "Instant Deposit: Deposits Report Depositing Success"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_DEPOSITS_REPORT_DEPOSITING_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

    .line 129
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "INSTANT_DEPOSIT_DEPOSITS_REPORT_DISPLAYED_AVAILABLE_BALANCE"

    const/16 v14, 0x74

    const-string v15, "Instant Deposit: Deposits Report Displayed Available Balance"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_DEPOSITS_REPORT_DISPLAYED_AVAILABLE_BALANCE:Lcom/squareup/analytics/RegisterViewName;

    .line 131
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "INSTANT_DEPOSIT_DEPOSITS_REPORT_DISPLAYED_ERROR"

    const/16 v14, 0x75

    const-string v15, "Instant Deposit: Deposits Report Displayed Error"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_DEPOSITS_REPORT_DISPLAYED_ERROR:Lcom/squareup/analytics/RegisterViewName;

    .line 133
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "INSTANT_DEPOSIT_HEADER_DEPOSITING_SUCCESS"

    const/16 v14, 0x76

    const-string v15, "Instant Deposit: Header Depositing Success"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_HEADER_DEPOSITING_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

    .line 134
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "INSTANT_DEPOSIT_HEADER_DISPLAYED_SHOWING_AVAILABLE_BALANCE"

    const/16 v14, 0x77

    const-string v15, "Instant Deposit: Header Displayed Showing Available Balance"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_HEADER_DISPLAYED_SHOWING_AVAILABLE_BALANCE:Lcom/squareup/analytics/RegisterViewName;

    .line 136
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "INSTANT_DEPOSIT_HEADER_DISPLAYED_SHOWING_ERROR"

    const/16 v14, 0x78

    const-string v15, "Instant Deposit: Header Displayed Showing Error"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_HEADER_DISPLAYED_SHOWING_ERROR:Lcom/squareup/analytics/RegisterViewName;

    .line 137
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "INSTANT_DEPOSIT_LINK_SUCCESS"

    const/16 v14, 0x79

    const-string v15, "Instant Deposit: Link Success"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_LINK_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

    .line 138
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "INSTANT_DEPOSIT_RESEND_EMAIL_SUCCESS"

    const/16 v14, 0x7a

    const-string v15, "Instant Deposit: Resend Email Success"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_RESEND_EMAIL_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

    .line 139
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "INVOICES_APPLET_INVOICE_DETAILS"

    const/16 v14, 0x7b

    const-string v15, "Invoices: Invoice Details"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->INVOICES_APPLET_INVOICE_DETAILS:Lcom/squareup/analytics/RegisterViewName;

    .line 140
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "INVOICES_BILL_HISTORY"

    const/16 v14, 0x7c

    const-string v15, "Invoices: Bill History"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->INVOICES_BILL_HISTORY:Lcom/squareup/analytics/RegisterViewName;

    .line 141
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "INVOICES_CREATION_FORM"

    const/16 v14, 0x7d

    const-string v15, "Invoices: Invoice Creation Form"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->INVOICES_CREATION_FORM:Lcom/squareup/analytics/RegisterViewName;

    .line 142
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "INVOICES_LIST"

    const/16 v14, 0x7e

    const-string v15, "Invoices: Invoices List"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->INVOICES_LIST:Lcom/squareup/analytics/RegisterViewName;

    .line 143
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "INVOICES_PROMPT_IDV"

    const/16 v14, 0x7f

    const-string v15, "Invoices: Prompt IDV"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->INVOICES_PROMPT_IDV:Lcom/squareup/analytics/RegisterViewName;

    .line 144
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ISSUE_CONTACTLESS_CARD_PRESENT_REFUND"

    const/16 v14, 0x80

    const-string v15, "Issue Contactless Card Present Refund View"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ISSUE_CONTACTLESS_CARD_PRESENT_REFUND:Lcom/squareup/analytics/RegisterViewName;

    .line 145
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ISSUE_REFUND"

    const/16 v14, 0x81

    const-string v15, "Issue Refund View"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ISSUE_REFUND:Lcom/squareup/analytics/RegisterViewName;

    .line 146
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ITEMS_APPLET_EDIT_CATEGORY"

    const/16 v14, 0x82

    const-string v15, "Items Applet: Edit Category Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_EDIT_CATEGORY:Lcom/squareup/analytics/RegisterViewName;

    .line 147
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ITEMS_APPLET_EDIT_DISCOUNT"

    const/16 v14, 0x83

    const-string v15, "Items Applet: Edit Discount Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_EDIT_DISCOUNT:Lcom/squareup/analytics/RegisterViewName;

    .line 148
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ITEMS_APPLET_EDIT_ITEM"

    const/16 v14, 0x84

    const-string v15, "Items Applet: Edit Item Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_EDIT_ITEM:Lcom/squareup/analytics/RegisterViewName;

    .line 149
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ITEMS_APPLET_EDIT_MODIFIER_SET"

    const/16 v14, 0x85

    const-string v15, "Items Applet: Edit Modifier Set Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_EDIT_MODIFIER_SET:Lcom/squareup/analytics/RegisterViewName;

    .line 150
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ITEMS_APPLET_MASTER_SCREEN"

    const/16 v14, 0x86

    const-string v15, "Items Applet: Master List"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_MASTER_SCREEN:Lcom/squareup/analytics/RegisterViewName;

    .line 151
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "LEARN_ABOURT_SQUARE"

    const/16 v14, 0x87

    const-string v15, "Learn about Square offered"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->LEARN_ABOURT_SQUARE:Lcom/squareup/analytics/RegisterViewName;

    .line 152
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "LOYALTY"

    const/16 v14, 0x88

    const-string v15, "Loyalty Enrollment and Redemption"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->LOYALTY:Lcom/squareup/analytics/RegisterViewName;

    .line 153
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "LOYALTY_ENROLL_BUYER"

    const/16 v14, 0x89

    const-string v15, "Loyalty 2: Enroll Screen: Buyer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_ENROLL_BUYER:Lcom/squareup/analytics/RegisterViewName;

    .line 154
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "LOYALTY_ENROLL_MERCHANT"

    const/16 v14, 0x8a

    const-string v15, "Loyalty 2: Enroll Screen: Merchant"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_ENROLL_MERCHANT:Lcom/squareup/analytics/RegisterViewName;

    .line 155
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "LOYALTY_REPORT"

    const/16 v14, 0x8b

    const-string v15, "Loyalty Report"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_REPORT:Lcom/squareup/analytics/RegisterViewName;

    .line 156
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "LOYALTY_STATUS_PROGRESS_REWARD_BUYER"

    const/16 v14, 0x8c

    const-string v15, "Loyalty 2: Status Screen: Progress To Reward: Buyer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_STATUS_PROGRESS_REWARD_BUYER:Lcom/squareup/analytics/RegisterViewName;

    .line 157
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "LOYALTY_STATUS_PROGRESS_REWARD_MERCHANT"

    const/16 v14, 0x8d

    const-string v15, "Loyalty 2: Status Screen: Progress To Reward: Merchant"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_STATUS_PROGRESS_REWARD_MERCHANT:Lcom/squareup/analytics/RegisterViewName;

    .line 158
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "MESSAGE_CENTER_LIST"

    const/16 v14, 0x8e

    const-string v15, "Settings Messages"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->MESSAGE_CENTER_LIST:Lcom/squareup/analytics/RegisterViewName;

    .line 159
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "MESSAGE_CENTER_MESSAGE"

    const/16 v14, 0x8f

    const-string v15, "Message Center Message"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->MESSAGE_CENTER_MESSAGE:Lcom/squareup/analytics/RegisterViewName;

    .line 160
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "O1_READER_DEPRECATION_NOTICE"

    const/16 v14, 0x90

    const-string v15, "O1 Reader Deprecation Notice"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->O1_READER_DEPRECATION_NOTICE:Lcom/squareup/analytics/RegisterViewName;

    .line 161
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ORDER_ENTRY_ITEM_SUGGESTIONS"

    const/16 v14, 0x91

    const-string v15, "Item Suggestions: View Item Suggestions"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ORDER_ENTRY_ITEM_SUGGESTIONS:Lcom/squareup/analytics/RegisterViewName;

    .line 162
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ORDERS_BILL_HISTORY"

    const/16 v14, 0x92

    const-string v15, "Orders: Bill History"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ORDERS_BILL_HISTORY:Lcom/squareup/analytics/RegisterViewName;

    .line 163
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "ONBOARDING_VERTICAL_SELECTION"

    const/16 v14, 0x93

    const-string v15, "Onboarding: Vertical Selection"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->ONBOARDING_VERTICAL_SELECTION:Lcom/squareup/analytics/RegisterViewName;

    .line 164
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_AUTHORIZING"

    const/16 v14, 0x94

    const-string v15, "Payment Flow: Authorizing"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_AUTHORIZING:Lcom/squareup/analytics/RegisterViewName;

    .line 165
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_CARD"

    const/16 v14, 0x95

    const-string v15, "Payment Flow: Card Payment Option"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_CARD:Lcom/squareup/analytics/RegisterViewName;

    .line 166
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_CASH"

    const/16 v14, 0x96

    const-string v15, "Payment Flow: Cash Payment Option"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_CASH:Lcom/squareup/analytics/RegisterViewName;

    .line 167
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_CHOOSE_COF_CUSTOMER"

    const/16 v14, 0x97

    const-string v15, "Payment Flow: Choose Card on File Customer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_CHOOSE_COF_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    .line 168
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_COUPON"

    const/16 v14, 0x98

    const-string v15, "Payment Flow: Coupon Selection After Authorizing"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_COUPON:Lcom/squareup/analytics/RegisterViewName;

    .line 169
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_EMAIL_COLLECTION"

    const/16 v14, 0x99

    const-string v15, "Payment Flow: Email Collection"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_EMAIL_COLLECTION:Lcom/squareup/analytics/RegisterViewName;

    .line 170
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_EMV_AUTHORIZING"

    const/16 v14, 0x9a

    const-string v15, "Payment Flow: EMV Authorizing"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_EMV_AUTHORIZING:Lcom/squareup/analytics/RegisterViewName;

    .line 171
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_EMV_BEGIN"

    const/16 v14, 0x9b

    const-string v15, "Payment Flow: EMV Begin Transaction"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_EMV_BEGIN:Lcom/squareup/analytics/RegisterViewName;

    .line 172
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_EMV_CONFIRM"

    const/16 v14, 0x9c

    const-string v15, "Payment Flow: EMV Confirm Amount"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_EMV_CONFIRM:Lcom/squareup/analytics/RegisterViewName;

    .line 173
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_EMV_ENTER_PIN"

    const/16 v14, 0x9d

    const-string v15, "Payment Flow: EMV Enter PIN"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_EMV_ENTER_PIN:Lcom/squareup/analytics/RegisterViewName;

    .line 174
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_EMV_SELECT_ACCOUNT"

    const/16 v14, 0x9e

    const-string v15, "Payment Flow: EMV Select Account"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_EMV_SELECT_ACCOUNT:Lcom/squareup/analytics/RegisterViewName;

    .line 175
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_ENTER_TICKET_AFTER_AUTH"

    const/16 v14, 0x9f

    const-string v15, "Payment Flow: Enter Ticket Identifier After Authorizing"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_ENTER_TICKET_AFTER_AUTH:Lcom/squareup/analytics/RegisterViewName;

    .line 176
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_ENTER_TICKET_BEFORE"

    const/16 v14, 0xa0

    const-string v15, "Payment Flow: Enter Ticket Identifier Before Payment Method"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_ENTER_TICKET_BEFORE:Lcom/squareup/analytics/RegisterViewName;

    .line 177
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_INVOICE_PAYMENT"

    const/16 v14, 0xa1

    const-string v15, "Payment Flow: Invoice Payment Option"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_INVOICE_PAYMENT:Lcom/squareup/analytics/RegisterViewName;

    .line 178
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_MAIN"

    const/16 v14, 0xa2

    const-string v15, "Payment Flow: Main"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_MAIN:Lcom/squareup/analytics/RegisterViewName;

    .line 179
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_OTHER_TENDER"

    const/16 v14, 0xa3

    const-string v15, "Payment Flow: Other Tender"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_OTHER_TENDER:Lcom/squareup/analytics/RegisterViewName;

    .line 180
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_PROCESSING"

    const/16 v14, 0xa4

    const-string v15, "Payment Flow: Processing"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_PROCESSING:Lcom/squareup/analytics/RegisterViewName;

    .line 181
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_RECEIPT"

    const/16 v14, 0xa5

    const-string v15, "Payment Flow: Receipt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_RECEIPT:Lcom/squareup/analytics/RegisterViewName;

    .line 182
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_SIGNATURE"

    const/16 v14, 0xa6

    const-string v15, "Payment Flow: Signature"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_SIGNATURE:Lcom/squareup/analytics/RegisterViewName;

    .line 183
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_SPLIT_TENDER"

    const/16 v14, 0xa7

    const-string v15, "Payment Flow: Split Tender"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_SPLIT_TENDER:Lcom/squareup/analytics/RegisterViewName;

    .line 184
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_SPLIT_TENDER_CASH"

    const/16 v14, 0xa8

    const-string v15, "Payment Flow: Split Tender Cash Options"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_SPLIT_TENDER_CASH:Lcom/squareup/analytics/RegisterViewName;

    .line 185
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_SPLIT_TENDER_OTHER"

    const/16 v14, 0xa9

    const-string v15, "Payment Flow: Split Tender Other Tender"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_SPLIT_TENDER_OTHER:Lcom/squareup/analytics/RegisterViewName;

    .line 186
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_SPLIT_TENDER_THIRD_PARTY_CARD"

    const/16 v14, 0xaa

    const-string v15, "Payment Flow: Split Tender Third Party Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_SPLIT_TENDER_THIRD_PARTY_CARD:Lcom/squareup/analytics/RegisterViewName;

    .line 187
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_THIRD_PARTY_CARD"

    const/16 v14, 0xab

    const-string v15, "Payment Flow: Third Party Card Payment Method"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_THIRD_PARTY_CARD:Lcom/squareup/analytics/RegisterViewName;

    .line 188
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_FLOW_TIP"

    const/16 v14, 0xac

    const-string v15, "Payment Flow: Tip"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_TIP:Lcom/squareup/analytics/RegisterViewName;

    .line 189
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_TUTORIAL_BANNER"

    const/16 v14, 0xad

    const-string v15, "Payment Tutorial Banner"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_TUTORIAL_BANNER:Lcom/squareup/analytics/RegisterViewName;

    .line 190
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_TUTORIAL_DECLINE_DIALOG"

    const/16 v14, 0xae

    const-string v15, "Payment Tutorial Decline Bookend"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_TUTORIAL_DECLINE_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    .line 191
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYMENT_TUTORIAL_FINISH_DIALOG"

    const/16 v14, 0xaf

    const-string v15, "Payment Tutorial Finish Bookend"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_TUTORIAL_FINISH_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    .line 192
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PAYROLL_UPSELL"

    const/16 v14, 0xb0

    const-string v15, "Upsell Screen for Payroll"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYROLL_UPSELL:Lcom/squareup/analytics/RegisterViewName;

    .line 193
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PREFILL_EMAIL_ADDRESS"

    const/16 v14, 0xb1

    const-string v15, "Prefill email address offered"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PREFILL_EMAIL_ADDRESS:Lcom/squareup/analytics/RegisterViewName;

    .line 194
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "PRINT_ERROR_POPUP"

    const/16 v14, 0xb2

    const-string v15, "Print Error Popup"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->PRINT_ERROR_POPUP:Lcom/squareup/analytics/RegisterViewName;

    .line 195
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "R12_MEET_THE_READER_MODAL"

    const/16 v14, 0xb3

    const-string v15, "Meet the Reader Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->R12_MEET_THE_READER_MODAL:Lcom/squareup/analytics/RegisterViewName;

    .line 196
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "R12_MULTIPAGE_WALKTHROUGH_PAGE"

    const/16 v14, 0xb4

    const-string v15, "Multipage Walkthrough - Page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->R12_MULTIPAGE_WALKTHROUGH_PAGE:Lcom/squareup/analytics/RegisterViewName;

    .line 197
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "R12_NATIVE_ORDER_ADDRESS_PAGE"

    const/16 v14, 0xb5

    const-string v15, "R12 Native Order Address Page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->R12_NATIVE_ORDER_ADDRESS_PAGE:Lcom/squareup/analytics/RegisterViewName;

    .line 198
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "R12_NATIVE_ORDER_CHARGE_PAGE"

    const/16 v14, 0xb6

    const-string v15, "R12 Native Order Charge Page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->R12_NATIVE_ORDER_CHARGE_PAGE:Lcom/squareup/analytics/RegisterViewName;

    .line 199
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "R12_NATIVE_ORDER_DETAILS_PAGE"

    const/16 v14, 0xb7

    const-string v15, "R12 Native Order Details Page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->R12_NATIVE_ORDER_DETAILS_PAGE:Lcom/squareup/analytics/RegisterViewName;

    .line 200
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "R12_NATIVE_ORDER_STATUS_PAGE"

    const/16 v14, 0xb8

    const-string v15, "R12 Native Order Status Page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->R12_NATIVE_ORDER_STATUS_PAGE:Lcom/squareup/analytics/RegisterViewName;

    .line 201
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "REISSUE_RECEIPT"

    const/16 v14, 0xb9

    const-string v15, "Reissue Receipt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->REISSUE_RECEIPT:Lcom/squareup/analytics/RegisterViewName;

    .line 202
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "REPORTS_CURRENT_DRAWER_CASH_MANAGEMENT_SETTINGS"

    const/16 v14, 0xba

    const-string v15, "Reports: Cash Management Settings"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->REPORTS_CURRENT_DRAWER_CASH_MANAGEMENT_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

    .line 203
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "REPORTS_CUSTOM_SALES_REPORT"

    const/16 v14, 0xbb

    const-string v15, "Reports: Custom Sales Report "

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->REPORTS_CUSTOM_SALES_REPORT:Lcom/squareup/analytics/RegisterViewName;

    .line 204
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "REPORTS_DEFAULT_SALES_REPORT"

    const/16 v14, 0xbc

    const-string v15, "Reports: Default Sales Report"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->REPORTS_DEFAULT_SALES_REPORT:Lcom/squareup/analytics/RegisterViewName;

    .line 205
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SALES_REPORT_VIEW_SALES_REPORT"

    const/16 v14, 0xbd

    const-string v15, "Sales Report: View Sales Report"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SALES_REPORT_VIEW_SALES_REPORT:Lcom/squareup/analytics/RegisterViewName;

    .line 206
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SCALE_ERROR"

    const/16 v14, 0xbe

    const-string v15, "Scale: Error"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SCALE_ERROR:Lcom/squareup/analytics/RegisterViewName;

    .line 207
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELECT_CATEGORY"

    const/16 v14, 0xbf

    const-string v15, "Select Category View"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELECT_CATEGORY:Lcom/squareup/analytics/RegisterViewName;

    .line 208
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW"

    const/16 v14, 0xc0

    const-string v15, "Seller Flow"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW:Lcom/squareup/analytics/RegisterViewName;

    .line 209
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_CART"

    const/16 v14, 0xc1

    const-string v15, "Seller Flow: Cart"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART:Lcom/squareup/analytics/RegisterViewName;

    .line 210
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_CART_CONTAINER"

    const/16 v14, 0xc2

    const-string v15, "Seller Flow: Cart Container"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_CONTAINER:Lcom/squareup/analytics/RegisterViewName;

    .line 211
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_CART_DISCOUNTS"

    const/16 v14, 0xc3

    const-string v15, "Seller Flow: Cart Discounts"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_DISCOUNTS:Lcom/squareup/analytics/RegisterViewName;

    .line 212
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_CART_TAXES"

    const/16 v14, 0xc4

    const-string v15, "Seller Flow: Cart Taxes"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_TAXES:Lcom/squareup/analytics/RegisterViewName;

    .line 213
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_CONFIGURE_ITEM_COMP"

    const/16 v14, 0xc5

    const-string v15, "Seller Flow: Configure Item Comp"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_COMP:Lcom/squareup/analytics/RegisterViewName;

    .line 214
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_CONFIGURE_ITEM_DETAIL"

    const/16 v14, 0xc6

    const-string v15, "Seller Flow: Configure Item Detail"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    .line 215
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_CONFIGURE_ITEM_PRICE"

    const/16 v14, 0xc7

    const-string v15, "Seller Flow: Configure Item Price"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_PRICE:Lcom/squareup/analytics/RegisterViewName;

    .line 216
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_CONFIGURE_ITEM_VOID"

    const/16 v14, 0xc8

    const-string v15, "Seller Flow: Configure Item Void"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_VOID:Lcom/squareup/analytics/RegisterViewName;

    .line 217
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_DISCOUNT_ENTRY_MONEY"

    const/16 v14, 0xc9

    const-string v15, "Seller Flow: Discount Entry Money"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_DISCOUNT_ENTRY_MONEY:Lcom/squareup/analytics/RegisterViewName;

    .line 218
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_DISCOUNT_ENTRY_PERCENT"

    const/16 v14, 0xca

    const-string v15, "Seller Flow: Discount Entry Percent"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_DISCOUNT_ENTRY_PERCENT:Lcom/squareup/analytics/RegisterViewName;

    .line 219
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_FAVORITE_PAGE"

    const/16 v14, 0xcb

    const-string v15, "Seller Flow: Favorite Page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_FAVORITE_PAGE:Lcom/squareup/analytics/RegisterViewName;

    .line 220
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_GIFT_CARD_ACTIVATION"

    const/16 v14, 0xcc

    const-string v15, "Seller Flow: Gift Card Activation"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_GIFT_CARD_ACTIVATION:Lcom/squareup/analytics/RegisterViewName;

    .line 221
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_ITEM_LIST"

    const/16 v14, 0xcd

    const-string v15, "Seller Flow: Item List"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_ITEM_LIST:Lcom/squareup/analytics/RegisterViewName;

    .line 222
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_KEYPAD_PANEL"

    const/16 v14, 0xce

    const-string v15, "Seller Flow: Keypad Panel"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_KEYPAD_PANEL:Lcom/squareup/analytics/RegisterViewName;

    .line 223
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_LIBRARY_LIST"

    const/16 v14, 0xcf

    const-string v15, "Seller Flow: Library List"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_LIBRARY_LIST:Lcom/squareup/analytics/RegisterViewName;

    .line 224
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_PERMISSION_PASSCODE_POPUP"

    const/16 v14, 0xd0

    const-string v15, "Seller Flow: Permission Passcode Popup"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_PERMISSION_PASSCODE_POPUP:Lcom/squareup/analytics/RegisterViewName;

    .line 225
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_PRICE_ENTRY"

    const/16 v14, 0xd1

    const-string v15, "Seller Flow: Price Entry"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_PRICE_ENTRY:Lcom/squareup/analytics/RegisterViewName;

    .line 226
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_TICKET_COMP"

    const/16 v14, 0xd2

    const-string v15, "Seller Flow: Ticket Comp"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_TICKET_COMP:Lcom/squareup/analytics/RegisterViewName;

    .line 227
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SELLER_FLOW_TICKET_VOID"

    const/16 v14, 0xd3

    const-string v15, "Seller Flow: Ticket Void"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_TICKET_VOID:Lcom/squareup/analytics/RegisterViewName;

    .line 228
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SERVICES_TAX"

    const/16 v14, 0xd4

    const-string v15, "Tax: View New Tax Page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SERVICES_TAX:Lcom/squareup/analytics/RegisterViewName;

    .line 229
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_BANK_ACCOUNT"

    const/16 v14, 0xd5

    const-string v15, "Settings Bank Account"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_BANK_ACCOUNT:Lcom/squareup/analytics/RegisterViewName;

    .line 230
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_BARCODE_SCANNERS"

    const/16 v14, 0xd6

    const-string v15, "Settings Barcode Scanners"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_BARCODE_SCANNERS:Lcom/squareup/analytics/RegisterViewName;

    .line 231
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_CASH"

    const/16 v14, 0xd7

    const-string v15, "Settings Cash Management"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_CASH:Lcom/squareup/analytics/RegisterViewName;

    .line 232
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_CASH_DRAWERS"

    const/16 v14, 0xd8

    const-string v15, "Settings Cash Drawers"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_CASH_DRAWERS:Lcom/squareup/analytics/RegisterViewName;

    .line 233
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_CUSTOMER_MANAGEMENT"

    const/16 v14, 0xd9

    const-string v15, "Settings Customer Management"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_CUSTOMER_MANAGEMENT:Lcom/squareup/analytics/RegisterViewName;

    .line 234
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_CUSTOMER_DISPLAY"

    const/16 v14, 0xda

    const-string v15, "Settings Customer Display"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_CUSTOMER_DISPLAY:Lcom/squareup/analytics/RegisterViewName;

    .line 235
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_CURRENT_DRAWER"

    const/16 v14, 0xdb

    const-string v15, "Settings Current Drawer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_CURRENT_DRAWER:Lcom/squareup/analytics/RegisterViewName;

    .line 236
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_DRAWER_HISTORY"

    const/16 v14, 0xdc

    const-string v15, "Settings Drawer History"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_DRAWER_HISTORY:Lcom/squareup/analytics/RegisterViewName;

    .line 237
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_DRAWER_REPORT"

    const/16 v14, 0xdd

    const-string v15, "Settings Drawer Report"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_DRAWER_REPORT:Lcom/squareup/analytics/RegisterViewName;

    .line 238
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_EDIT_TICKET_GROUP"

    const/16 v14, 0xde

    const-string v15, "Settings Edit Ticket Group"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_EDIT_TICKET_GROUP:Lcom/squareup/analytics/RegisterViewName;

    .line 239
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_EMAIL_COLLECTION"

    const/16 v14, 0xdf

    const-string v15, "Settings Email Collection"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_EMAIL_COLLECTION:Lcom/squareup/analytics/RegisterViewName;

    .line 240
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_EMPLOYEE_MANAGEMENT"

    const/16 v14, 0xe0

    const-string v15, "Settings Employee Management"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_EMPLOYEE_MANAGEMENT:Lcom/squareup/analytics/RegisterViewName;

    .line 241
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_GIFTCARDS"

    const/16 v14, 0xe1

    const-string v15, "Settings Gift Cards"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_GIFTCARDS:Lcom/squareup/analytics/RegisterViewName;

    .line 242
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_INSTANT_DEPOSIT"

    const/16 v14, 0xe2

    const-string v15, "Settings Instant Deposit"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_INSTANT_DEPOSIT:Lcom/squareup/analytics/RegisterViewName;

    .line 243
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_INSTANT_DEPOSIT_LINK_CARD"

    const/16 v14, 0xe3

    const-string v15, "Instant Deposit: Link Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_INSTANT_DEPOSIT_LINK_CARD:Lcom/squareup/analytics/RegisterViewName;

    .line 244
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_INSTANT_DEPOSIT_LINK_CARD_RESULT"

    const/16 v14, 0xe4

    const-string v15, "Instant Deposit: Link Card Result"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_INSTANT_DEPOSIT_LINK_CARD_RESULT:Lcom/squareup/analytics/RegisterViewName;

    .line 245
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_LIST"

    const/16 v14, 0xe5

    const-string v15, "Settings Settings"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_LIST:Lcom/squareup/analytics/RegisterViewName;

    .line 246
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_LOYALTY"

    const/16 v14, 0xe6

    const-string v15, "Settings Loyalty"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_LOYALTY:Lcom/squareup/analytics/RegisterViewName;

    .line 247
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_OFFLINE"

    const/16 v14, 0xe7

    const-string v15, "Settings Offline Mode"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_OFFLINE:Lcom/squareup/analytics/RegisterViewName;

    .line 248
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_ONLINE_CHECKOUT"

    const/16 v14, 0xe8

    const-string v15, "Settings Online Checkout"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_ONLINE_CHECKOUT:Lcom/squareup/analytics/RegisterViewName;

    .line 249
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_OPEN_TICKET"

    const/16 v14, 0xe9

    const-string v15, "Settings Open Tickets"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_OPEN_TICKET:Lcom/squareup/analytics/RegisterViewName;

    .line 250
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_PAID"

    const/16 v14, 0xea

    const-string v15, "Settings Paid In/Out"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_PAID:Lcom/squareup/analytics/RegisterViewName;

    .line 251
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_PRINTER_EDIT"

    const/16 v14, 0xeb

    const-string v15, "Settings Edit Printer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_PRINTER_EDIT:Lcom/squareup/analytics/RegisterViewName;

    .line 252
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_PRINTER_LIST"

    const/16 v14, 0xec

    const-string v15, "Settings Printers"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_PRINTER_LIST:Lcom/squareup/analytics/RegisterViewName;

    .line 253
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_PUBLIC_PROFILE"

    const/16 v14, 0xed

    const-string v15, "Settings Public Profile"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_PUBLIC_PROFILE:Lcom/squareup/analytics/RegisterViewName;

    .line 254
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_PUBLIC_PROFILE_ADDRESS"

    const/16 v14, 0xee

    const-string v15, "Settings Business Information: Edit Business Address Page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_PUBLIC_PROFILE_ADDRESS:Lcom/squareup/analytics/RegisterViewName;

    .line 255
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_SALES_SUMMARY"

    const/16 v14, 0xef

    const-string v15, "Settings Sales Summary"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SALES_SUMMARY:Lcom/squareup/analytics/RegisterViewName;

    .line 256
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_SHARED_SETTINGS"

    const/16 v14, 0xf0

    const-string v15, "Settings Shared Settings"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SHARED_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

    .line 257
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_SIGNATURE"

    const/16 v14, 0xf1

    const-string v15, "Settings Signature"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SIGNATURE:Lcom/squareup/analytics/RegisterViewName;

    .line 258
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_STAND"

    const/16 v14, 0xf2

    const-string v15, "Settings Square Stand"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_STAND:Lcom/squareup/analytics/RegisterViewName;

    .line 259
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_SUPPORT"

    const/16 v14, 0xf3

    const-string v15, "Settings Support"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SUPPORT:Lcom/squareup/analytics/RegisterViewName;

    .line 260
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_SURCHARGING"

    const/16 v14, 0xf4

    const-string v15, "Settings Surcharging"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SURCHARGING:Lcom/squareup/analytics/RegisterViewName;

    .line 261
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_SWIPE_CHIP_CARDS"

    const/16 v14, 0xf5

    const-string v15, "Settings Swipe Chip Cards"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SWIPE_CHIP_CARDS:Lcom/squareup/analytics/RegisterViewName;

    .line 262
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_SWIPE_CHIP_CARDS_ENABLE_SCREEN"

    const/16 v14, 0xf6

    const-string v15, "Settings Swipe Chip Cards: Confirmation"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SWIPE_CHIP_CARDS_ENABLE_SCREEN:Lcom/squareup/analytics/RegisterViewName;

    .line 263
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_TAX_EDIT"

    const/16 v14, 0xf7

    const-string v15, "Settings Tax"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_TAX_EDIT:Lcom/squareup/analytics/RegisterViewName;

    .line 264
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_TAX_LIST"

    const/16 v14, 0xf8

    const-string v15, "Settings Taxes"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_TAX_LIST:Lcom/squareup/analytics/RegisterViewName;

    .line 265
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_TILE_APPEARANCE"

    const/16 v14, 0xf9

    const-string v15, "Settings Tile Appearance"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_TILE_APPEARANCE:Lcom/squareup/analytics/RegisterViewName;

    .line 266
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_TIPPING"

    const/16 v14, 0xfa

    const-string v15, "Settings Tipping"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_TIPPING:Lcom/squareup/analytics/RegisterViewName;

    .line 267
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SETTINGS_USE_PREDEFINED_TICKETS"

    const/16 v14, 0xfb

    const-string v15, "Use Predefined Tickets Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_USE_PREDEFINED_TICKETS:Lcom/squareup/analytics/RegisterViewName;

    .line 268
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SPLIT_TENDER_WARNING_DIALOG"

    const/16 v14, 0xfc

    const-string v15, "Alert: Confirmation To Cancel Bill With Completed Payment Sources"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SPLIT_TENDER_WARNING_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    .line 269
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "START_SHIFT"

    const/16 v14, 0xfd

    const-string v15, "Start Shift Drawer Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->START_SHIFT:Lcom/squareup/analytics/RegisterViewName;

    .line 270
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SUPPORT_ABOUT"

    const/16 v14, 0xfe

    const-string v15, "Support About"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_ABOUT:Lcom/squareup/analytics/RegisterViewName;

    .line 271
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SUPPORT_ANNOUNCEMENTS"

    const/16 v14, 0xff

    const-string v15, "Support Announcements"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_ANNOUNCEMENTS:Lcom/squareup/analytics/RegisterViewName;

    .line 272
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SUPPORT_ANNOUNCEMENT_DETAILS"

    const/16 v14, 0x100

    const-string v15, "Support Announcement Details"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_ANNOUNCEMENT_DETAILS:Lcom/squareup/analytics/RegisterViewName;

    .line 273
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SUPPORT_CONTACT"

    const/16 v14, 0x101

    const-string v15, "Support Contact"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_CONTACT:Lcom/squareup/analytics/RegisterViewName;

    .line 274
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SUPPORT_HARDWARE_DISCLAIMER"

    const/16 v14, 0x102

    const-string v15, "Support Hardware Disclaimer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_HARDWARE_DISCLAIMER:Lcom/squareup/analytics/RegisterViewName;

    .line 275
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SUPPORT_HELP"

    const/16 v14, 0x103

    const-string v15, "Support Static Help"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_HELP:Lcom/squareup/analytics/RegisterViewName;

    .line 276
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SUPPORT_HELP_JEDI"

    const/16 v14, 0x104

    const-string v15, "Support Jedi Help"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_HELP_JEDI:Lcom/squareup/analytics/RegisterViewName;

    .line 277
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SUPPORT_LEGAL"

    const/16 v14, 0x105

    const-string v15, "Support Legal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_LEGAL:Lcom/squareup/analytics/RegisterViewName;

    .line 278
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SUPPORT_LIBRARIES"

    const/16 v14, 0x106

    const-string v15, "Support Libraries"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_LIBRARIES:Lcom/squareup/analytics/RegisterViewName;

    .line 279
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SUPPORT_LICENSES"

    const/16 v14, 0x107

    const-string v15, "Support Licenses"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_LICENSES:Lcom/squareup/analytics/RegisterViewName;

    .line 280
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SUPPORT_MESSAGES"

    const/16 v14, 0x108

    const-string v15, "Support Messages"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_MESSAGES:Lcom/squareup/analytics/RegisterViewName;

    .line 281
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SUPPORT_TROUBLESHOOTING"

    const/16 v14, 0x109

    const-string v15, "Support Troubleshooting"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_TROUBLESHOOTING:Lcom/squareup/analytics/RegisterViewName;

    .line 282
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SUPPORT_TUTORIALS"

    const/16 v14, 0x10a

    const-string v15, "Support Tutorials"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_TUTORIALS:Lcom/squareup/analytics/RegisterViewName;

    .line 283
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SYSTEM_PERMISSIONS_ABOUT_SETTINGS"

    const/16 v14, 0x10b

    const-string v15, "System Permissions: About Device Settings"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SYSTEM_PERMISSIONS_ABOUT_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

    .line 284
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SYSTEM_PERMISSIONS_ENABLE_SETTINGS"

    const/16 v14, 0x10c

    const-string v15, "System Permissions: Enable Device Settings"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SYSTEM_PERMISSIONS_ENABLE_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

    .line 285
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "SYSTEM_PERMISSIONS_PERMISSION_DENIED"

    const/16 v14, 0x10d

    const-string v15, "System Permissions: Permission Denied"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->SYSTEM_PERMISSIONS_PERMISSION_DENIED:Lcom/squareup/analytics/RegisterViewName;

    .line 286
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TAXES_CREATE"

    const/16 v14, 0x10e

    const-string v15, "Tax: View New Tax Page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TAXES_CREATE:Lcom/squareup/analytics/RegisterViewName;

    .line 287
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_ERROR_CLOCK_IN"

    const/16 v14, 0x10f

    const-string v15, "Timecards: Error: Clock In"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

    .line 288
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_ERROR_CLOCK_OUT"

    const/16 v14, 0x110

    const-string v15, "Timecards: Error: Clock Out"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

    .line 289
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_ERROR_DEFAULT"

    const/16 v14, 0x111

    const-string v15, "Timecards: Error"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_DEFAULT:Lcom/squareup/analytics/RegisterViewName;

    .line 290
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_ERROR_END_BREAK"

    const/16 v14, 0x112

    const-string v15, "Timecards: Error: End Break"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    .line 291
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_ERROR_NO_INTERNET"

    const/16 v14, 0x113

    const-string v15, "Timecards: Error: No Internet"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_NO_INTERNET:Lcom/squareup/analytics/RegisterViewName;

    .line 292
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_ERROR_START_BREAK"

    const/16 v14, 0x114

    const-string v15, "Timecards: Error: Start Break"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_START_BREAK:Lcom/squareup/analytics/RegisterViewName;

    .line 293
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_LOADING"

    const/16 v14, 0x115

    const-string v15, "Timecards: Loading"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_LOADING:Lcom/squareup/analytics/RegisterViewName;

    .line 294
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_PASSCODE"

    const/16 v14, 0x116

    const-string v15, "Timecards: Passcode"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_PASSCODE:Lcom/squareup/analytics/RegisterViewName;

    .line 295
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_REMINDER_SELECT_ACTION_CLOCK_IN"

    const/16 v14, 0x117

    const-string v15, "Timecards Reminder: Select Action: Clock In"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_REMINDER_SELECT_ACTION_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

    .line 296
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_REMINDER_SELECT_ACTION_END_BREAK"

    const/16 v14, 0x118

    const-string v15, "Timecards Reminder: Select Action: End Break"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_REMINDER_SELECT_ACTION_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    .line 297
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_REMINDER_SUCCESS_CLOCK_IN"

    const/16 v14, 0x119

    const-string v15, "Timecards Reminder: Success: Clock In"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_REMINDER_SUCCESS_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

    .line 298
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_REMINDER_SUCCESS_END_BREAK"

    const/16 v14, 0x11a

    const-string v15, "Timecards Reminder: Success: End Break"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_REMINDER_SUCCESS_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    .line 299
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_SELECT_ACTION_CLOCK_IN"

    const/16 v14, 0x11b

    const-string v15, "Timecards: Select Action: Clock In"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

    .line 300
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_SELECT_ACTION_CLOCK_OUT"

    const/16 v14, 0x11c

    const-string v15, "Timecards: Select Action: Clock Out"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

    .line 301
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_SELECT_ACTION_END_BREAK"

    const/16 v14, 0x11d

    const-string v15, "Timecards: Select Action: End Break"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    .line 302
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_SELECT_ACTION_JOBS_LIST"

    const/16 v14, 0x11e

    const-string v15, "Timecards: Select Action: Jobs List"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_JOBS_LIST:Lcom/squareup/analytics/RegisterViewName;

    .line 303
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_SELECT_ACTION_LIST_BREAKS"

    const/16 v14, 0x11f

    const-string v15, "Timecards: Select Action: List Breaks"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_LIST_BREAKS:Lcom/squareup/analytics/RegisterViewName;

    .line 304
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_SELECT_ACTION_START_BREAK_OR_CLOCK_OUT"

    const/16 v14, 0x120

    const-string v15, "Timecards: Select Action: Start Break Or Clock Out"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_START_BREAK_OR_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

    .line 306
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_SUCCESS_CLOCK_IN"

    const/16 v14, 0x121

    const-string v15, "Timecards: Success: Clock In"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

    .line 307
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_SUCCESS_CLOCK_OUT"

    const/16 v14, 0x122

    const-string v15, "Timecards: Success: Clock Out"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

    .line 308
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_SUCCESS_CLOCK_OUT_SUMMARY"

    const/16 v14, 0x123

    const-string v15, "Timecards: Success: Clock Out Summary"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_CLOCK_OUT_SUMMARY:Lcom/squareup/analytics/RegisterViewName;

    .line 309
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_SUCCESS_END_BREAK"

    const/16 v14, 0x124

    const-string v15, "Timecards: Success: End Break"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    .line 310
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TIMECARDS_SUCCESS_START_BREAK"

    const/16 v14, 0x125

    const-string v15, "Timecards: Success: Start Break"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_START_BREAK:Lcom/squareup/analytics/RegisterViewName;

    .line 311
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "TRANSACTION_DETAIL"

    const/16 v14, 0x126

    const-string v15, "Transaction detail"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->TRANSACTION_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    .line 312
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_COUNTRY"

    const/16 v14, 0x127

    const-string v15, "Welcome Flow Country Selector"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_COUNTRY:Lcom/squareup/analytics/RegisterViewName;

    .line 313
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_CREATE"

    const/16 v14, 0x128

    const-string v15, "Welcome Flow Create Account"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_CREATE:Lcom/squareup/analytics/RegisterViewName;

    .line 314
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_CREATE_PASSWORD"

    const/16 v14, 0x129

    const-string v15, "Welcome Flow Create Account Password"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_CREATE_PASSWORD:Lcom/squareup/analytics/RegisterViewName;

    .line 315
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_DEVICE_CODE_SIGN_IN"

    const/16 v14, 0x12a

    const-string v15, "Welcome Flow Device Code Sign In"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_DEVICE_CODE_SIGN_IN:Lcom/squareup/analytics/RegisterViewName;

    .line 316
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_LANDING"

    const/16 v14, 0x12b

    const-string v15, "Welcome Flow Landing Page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_LANDING:Lcom/squareup/analytics/RegisterViewName;

    .line 317
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_RESET"

    const/16 v14, 0x12c

    const-string v15, "Welcome Flow Reset Password"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_RESET:Lcom/squareup/analytics/RegisterViewName;

    .line 318
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_SIGN_IN"

    const/16 v14, 0x12d

    const-string v15, "Welcome Flow Sign In"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SIGN_IN:Lcom/squareup/analytics/RegisterViewName;

    .line 319
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_SIGN_IN_APP_LANDING"

    const/16 v14, 0x12e

    const-string v15, "Sign In App Welcome Flow Landing View"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SIGN_IN_APP_LANDING:Lcom/squareup/analytics/RegisterViewName;

    .line 320
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_SPLASH"

    const/16 v14, 0x12f

    const-string v15, "Welcome Flow Splash Page"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH:Lcom/squareup/analytics/RegisterViewName;

    .line 321
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_SPLASH_PAGE_1"

    const/16 v14, 0x130

    const-string v15, "Welcome Flow Splash Page 1"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH_PAGE_1:Lcom/squareup/analytics/RegisterViewName;

    .line 322
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_SPLASH_PAGE_2"

    const/16 v14, 0x131

    const-string v15, "Welcome Flow Splash Page 2"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH_PAGE_2:Lcom/squareup/analytics/RegisterViewName;

    .line 323
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_SPLASH_PAGE_3"

    const/16 v14, 0x132

    const-string v15, "Welcome Flow Splash Page 3"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH_PAGE_3:Lcom/squareup/analytics/RegisterViewName;

    .line 324
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_SPLASH_PAGE_4"

    const/16 v14, 0x133

    const-string v15, "Welcome Flow Splash Page 4"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH_PAGE_4:Lcom/squareup/analytics/RegisterViewName;

    .line 325
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_WORLD_LANDING"

    const/16 v14, 0x134

    const-string v15, "World Welcome Flow Landing View"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_WORLD_LANDING:Lcom/squareup/analytics/RegisterViewName;

    .line 326
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "WELCOME_X2_LANDING"

    const/16 v14, 0x135

    const-string v15, "X2 Welcome Flow Landing View"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_X2_LANDING:Lcom/squareup/analytics/RegisterViewName;

    .line 331
    new-instance v0, Lcom/squareup/analytics/RegisterViewName;

    const-string v13, "UNKNOWN"

    const/16 v14, 0x136

    const-string v15, "<Unknown>"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterViewName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->UNKNOWN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v0, 0x137

    new-array v0, v0, [Lcom/squareup/analytics/RegisterViewName;

    .line 8
    sget-object v13, Lcom/squareup/analytics/RegisterViewName;->ACCOUNT_HOME:Lcom/squareup/analytics/RegisterViewName;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVITY:Lcom/squareup/analytics/RegisterViewName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_ACTIVATE_VIA_WEB:Lcom/squareup/analytics/RegisterViewName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_ACTIVATE_YOUR_ACCOUNT:Lcom/squareup/analytics/RegisterViewName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_BUSINESS:Lcom/squareup/analytics/RegisterViewName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_BUSINESS_ADDRESS:Lcom/squareup/analytics/RegisterViewName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CATEGORY:Lcom/squareup/analytics/RegisterViewName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_EDIT_SHIPPING:Lcom/squareup/analytics/RegisterViewName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_END:Lcom/squareup/analytics/RegisterViewName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_GET_READER:Lcom/squareup/analytics/RegisterViewName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_INITIAL_SHIPPING:Lcom/squareup/analytics/RegisterViewName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_INTENT_HOW:Lcom/squareup/analytics/RegisterViewName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_INTENT_WHERE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_LINK_BANK:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_LINK_BANK_COMPLETE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_NEW_ACTIVATE_YOUR_ACCOUNT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_PERSONAL:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_QUIZ:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CONTACTLESS_ORDER_COMFIRMATION_PAID:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CONTACTLESS_PAYMENT_INPUT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CONTACTLESS_SHIPPING_CONFIRMATION:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CONTACTLESS_SHIPPING_PAID:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CONTACTLESS_SHIPPING_PAID_R12_ONLY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CONTACTLESS_UPSELL_PAID:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_CONTACTLESS_PAYMENT_PAID:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_RETRY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_SEND_READER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_SUBCATEGORY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_VERIFY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->BIZBANK_BALANCE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CARD_ON_FILE_LINK_CARD_REQUEST:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CARD_ON_FILE_LINK_CARD_REQUEST_FAILURE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CARD_ON_FILE_LINK_CARD_REQUEST_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CASH_MANAGEMENT_END_DRAWER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CASH_PAYMENT_TUTORIAL_DECLINE_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CASH_PAYMENT_TUTORIAL_FINISH_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CHECKOUT_KEYPAD_VIEW:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CHECKOUT_LIBRARY_VIEW:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->COLLECT_CASH_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->COUPONS_REDEEM_MULTIPLE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->COUPONS_REDEEM_NONE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->COUPONS_REDEEM_ONE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->COUPONS_SEARCH:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_ADD_ITEM_TO_GRID:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_FIRST_ITEM_CREATED:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_ITEM_SETUP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_SHOW_MODAL_DONE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_SHOW_MODAL_VIA_ONBOARDING:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_SHOW_MODAL_VIA_SUPPORT_APPLET:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_SHOW_SKIP_MODAL:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_ADD_CUSTOMERS_TO_GROUP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_ADDING_CUSTOMERS_TO_GROUP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_ALL_NOTES:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_ALL_FREQUENT_ITEMS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_FILTER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_FILTERS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_GROUPS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_CONVERSATION:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_CREATE_GROUP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_CREATE_NOTE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_CUSTOMER_ACTIVITY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_DELETING_CUSTOMERS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_EDIT_FILTER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_RESOLVE_DUPLICATES:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_REVIEW_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_UPDATE_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_CUSTOMER_SAVE_CARD:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_CUSTOMER_DIP_PROCCESSING:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_CUSTOMER_CUSTOMER_EMAIL:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_CUSTOMER_VERIFY_ZIPCODE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_MERGE_CUSTOMERS_CONFIRMATION:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_MERGING_CUSTOMERS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_MERGING_DUPLICATES:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_SELECT_LOYALTY_PHONE_NUMBER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_SEND_MESSAGE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_VIEW_NOTE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_REMINDER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_BILL_HISTORY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_MESSAGING_CREATE_CONVERSATION:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_MESSAGING_SHOW_CONVERSATION:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_MESSAGING_SHOW_CONVERSATION_LIST:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_BULK_DELETE_CONFIRM:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_CONTACTS_LIST:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_CREATE_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_CUSTOMER_PROFILE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_CUSTOMER_PROFILE_CARD:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_EDIT_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_GROUPS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_GROUPS_MANUAL_GROUP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_GROUPS_SMART_GROUP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_GROUPS_UPDATE_GROUP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CUSTOMER_CHECKOUT_BUYER_CART:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CUSTOMER_CHECKOUT_PAYMENT_PROMPT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->DELEGATED_LOCKOUT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->DEMO:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->DEPOSITS_REPORT_SUMMARY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->DEPOSITS_REPORT_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->EMPLOYEES_APPLET_MASTER_SCREEN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->EMPLOYEES_APPLET_CREATE_EMPLOYEE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->EMPLOYEES_APPLET_UPDATE_EMPLOYEE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->FEATURE_TOUR:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_DIP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_DIP_JCB:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_INTERAC:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_MANUAL:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_SWIPE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->FEE_TUTORIAL_TAP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->GAP_TIME_EDUCATION:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_HISTORY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_ORDER_READER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_ORDER_READER_MAGSTRIPE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_ORDER_READER_MAGSTRIPE_CONFIRM:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_REFERRAL:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_RETAIL_MAP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_DEPOSITS_REPORT_DEPOSITING_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_DEPOSITS_REPORT_DISPLAYED_AVAILABLE_BALANCE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_DEPOSITS_REPORT_DISPLAYED_ERROR:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_HEADER_DEPOSITING_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_HEADER_DISPLAYED_SHOWING_AVAILABLE_BALANCE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_HEADER_DISPLAYED_SHOWING_ERROR:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_LINK_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_RESEND_EMAIL_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INVOICES_APPLET_INVOICE_DETAILS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INVOICES_BILL_HISTORY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INVOICES_CREATION_FORM:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INVOICES_LIST:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INVOICES_PROMPT_IDV:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ISSUE_CONTACTLESS_CARD_PRESENT_REFUND:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ISSUE_REFUND:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_EDIT_CATEGORY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_EDIT_DISCOUNT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_EDIT_ITEM:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_EDIT_MODIFIER_SET:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_MASTER_SCREEN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->LEARN_ABOURT_SQUARE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->LOYALTY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_ENROLL_BUYER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_ENROLL_MERCHANT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_REPORT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_STATUS_PROGRESS_REWARD_BUYER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_STATUS_PROGRESS_REWARD_MERCHANT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->MESSAGE_CENTER_LIST:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->MESSAGE_CENTER_MESSAGE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->O1_READER_DEPRECATION_NOTICE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ORDER_ENTRY_ITEM_SUGGESTIONS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ORDERS_BILL_HISTORY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->ONBOARDING_VERTICAL_SELECTION:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_AUTHORIZING:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_CARD:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_CASH:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_CHOOSE_COF_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_COUPON:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_EMAIL_COLLECTION:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_EMV_AUTHORIZING:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_EMV_BEGIN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_EMV_CONFIRM:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_EMV_ENTER_PIN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_EMV_SELECT_ACCOUNT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_ENTER_TICKET_AFTER_AUTH:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_ENTER_TICKET_BEFORE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_INVOICE_PAYMENT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_MAIN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_OTHER_TENDER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_PROCESSING:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_RECEIPT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_SIGNATURE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_SPLIT_TENDER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_SPLIT_TENDER_CASH:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_SPLIT_TENDER_OTHER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_SPLIT_TENDER_THIRD_PARTY_CARD:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_THIRD_PARTY_CARD:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_TIP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_TUTORIAL_BANNER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_TUTORIAL_DECLINE_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_TUTORIAL_FINISH_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYROLL_UPSELL:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PREFILL_EMAIL_ADDRESS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PRINT_ERROR_POPUP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->R12_MEET_THE_READER_MODAL:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xb3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->R12_MULTIPAGE_WALKTHROUGH_PAGE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xb4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->R12_NATIVE_ORDER_ADDRESS_PAGE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xb5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->R12_NATIVE_ORDER_CHARGE_PAGE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xb6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->R12_NATIVE_ORDER_DETAILS_PAGE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xb7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->R12_NATIVE_ORDER_STATUS_PAGE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xb8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->REISSUE_RECEIPT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xb9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->REPORTS_CURRENT_DRAWER_CASH_MANAGEMENT_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xba

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->REPORTS_CUSTOM_SALES_REPORT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xbb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->REPORTS_DEFAULT_SALES_REPORT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xbc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SALES_REPORT_VIEW_SALES_REPORT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xbd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SCALE_ERROR:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xbe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELECT_CATEGORY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xbf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xc0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xc1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_CONTAINER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xc2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_DISCOUNTS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xc3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_TAXES:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xc4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_COMP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xc5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xc6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_PRICE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xc7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_VOID:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xc8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_DISCOUNT_ENTRY_MONEY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xc9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_DISCOUNT_ENTRY_PERCENT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xca

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_FAVORITE_PAGE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xcb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_GIFT_CARD_ACTIVATION:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xcc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_ITEM_LIST:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xcd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_KEYPAD_PANEL:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xce

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_LIBRARY_LIST:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xcf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_PERMISSION_PASSCODE_POPUP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xd0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_PRICE_ENTRY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xd1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_TICKET_COMP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xd2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_TICKET_VOID:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xd3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SERVICES_TAX:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xd4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_BANK_ACCOUNT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xd5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_BARCODE_SCANNERS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xd6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_CASH:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xd7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_CASH_DRAWERS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xd8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_CUSTOMER_MANAGEMENT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xd9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_CUSTOMER_DISPLAY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xda

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_CURRENT_DRAWER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xdb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_DRAWER_HISTORY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xdc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_DRAWER_REPORT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xdd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_EDIT_TICKET_GROUP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xde

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_EMAIL_COLLECTION:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xdf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_EMPLOYEE_MANAGEMENT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xe0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_GIFTCARDS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xe1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_INSTANT_DEPOSIT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xe2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_INSTANT_DEPOSIT_LINK_CARD:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xe3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_INSTANT_DEPOSIT_LINK_CARD_RESULT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xe4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_LIST:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xe5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_LOYALTY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xe6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_OFFLINE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xe7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_ONLINE_CHECKOUT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xe8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_OPEN_TICKET:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xe9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_PAID:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xea

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_PRINTER_EDIT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xeb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_PRINTER_LIST:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xec

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_PUBLIC_PROFILE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xed

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_PUBLIC_PROFILE_ADDRESS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xee

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SALES_SUMMARY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xef

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SHARED_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xf0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SIGNATURE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xf1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_STAND:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xf2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SUPPORT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xf3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SURCHARGING:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xf4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SWIPE_CHIP_CARDS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xf5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SWIPE_CHIP_CARDS_ENABLE_SCREEN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xf6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_TAX_EDIT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xf7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_TAX_LIST:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xf8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_TILE_APPEARANCE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xf9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_TIPPING:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xfa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_USE_PREDEFINED_TICKETS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xfb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SPLIT_TENDER_WARNING_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xfc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->START_SHIFT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xfd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_ABOUT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xfe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_ANNOUNCEMENTS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0xff

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_ANNOUNCEMENT_DETAILS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x100

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_CONTACT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x101

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_HARDWARE_DISCLAIMER:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x102

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_HELP:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x103

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_HELP_JEDI:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x104

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_LEGAL:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x105

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_LIBRARIES:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x106

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_LICENSES:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x107

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_MESSAGES:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x108

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_TROUBLESHOOTING:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x109

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_TUTORIALS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x10a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SYSTEM_PERMISSIONS_ABOUT_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x10b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SYSTEM_PERMISSIONS_ENABLE_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x10c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SYSTEM_PERMISSIONS_PERMISSION_DENIED:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x10d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TAXES_CREATE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x10e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x10f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x110

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_DEFAULT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x111

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x112

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_NO_INTERNET:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x113

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_START_BREAK:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x114

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_LOADING:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x115

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_PASSCODE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x116

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_REMINDER_SELECT_ACTION_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x117

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_REMINDER_SELECT_ACTION_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x118

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_REMINDER_SUCCESS_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x119

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_REMINDER_SUCCESS_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x11a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x11b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x11c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x11d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_JOBS_LIST:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x11e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_LIST_BREAKS:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x11f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_START_BREAK_OR_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x120

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x121

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x122

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_CLOCK_OUT_SUMMARY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x123

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x124

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_START_BREAK:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x125

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TRANSACTION_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x126

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_COUNTRY:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x127

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_CREATE:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x128

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_CREATE_PASSWORD:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x129

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_DEVICE_CODE_SIGN_IN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x12a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_LANDING:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x12b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_RESET:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x12c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SIGN_IN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x12d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SIGN_IN_APP_LANDING:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x12e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x12f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH_PAGE_1:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x130

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH_PAGE_2:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x131

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH_PAGE_3:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x132

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH_PAGE_4:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x133

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_WORLD_LANDING:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x134

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->WELCOME_X2_LANDING:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x135

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->UNKNOWN:Lcom/squareup/analytics/RegisterViewName;

    const/16 v2, 0x136

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/analytics/RegisterViewName;->$VALUES:[Lcom/squareup/analytics/RegisterViewName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 335
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 336
    iput-object p3, p0, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 8
    const-class v0, Lcom/squareup/analytics/RegisterViewName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/analytics/RegisterViewName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 8
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->$VALUES:[Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0}, [Lcom/squareup/analytics/RegisterViewName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 340
    iget-object v0, p0, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    return-object v0
.end method
