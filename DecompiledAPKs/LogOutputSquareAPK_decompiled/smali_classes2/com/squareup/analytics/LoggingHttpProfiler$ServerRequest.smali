.class public Lcom/squareup/analytics/LoggingHttpProfiler$ServerRequest;
.super Lcom/squareup/analytics/event/v1/TimingEvent;
.source "LoggingHttpProfiler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/analytics/LoggingHttpProfiler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ServerRequest"
.end annotation


# instance fields
.field final base_url:Ljava/lang/String;

.field final content_type:Ljava/lang/String;

.field final duration_ms:J

.field final http_method:Ljava/lang/String;

.field final request_size:J

.field final request_uri:Ljava/lang/String;

.field final response_size:J

.field final response_status:I


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJIJ)V
    .locals 1

    .line 80
    sget-object v0, Lcom/squareup/analytics/RegisterTimingName;->SERVER_CALL:Lcom/squareup/analytics/RegisterTimingName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/TimingEvent;-><init>(Lcom/squareup/analytics/RegisterTimingName;)V

    .line 81
    iput-object p1, p0, Lcom/squareup/analytics/LoggingHttpProfiler$ServerRequest;->http_method:Ljava/lang/String;

    .line 82
    iput-object p2, p0, Lcom/squareup/analytics/LoggingHttpProfiler$ServerRequest;->request_uri:Ljava/lang/String;

    .line 83
    iput-object p3, p0, Lcom/squareup/analytics/LoggingHttpProfiler$ServerRequest;->content_type:Ljava/lang/String;

    .line 84
    iput-wide p5, p0, Lcom/squareup/analytics/LoggingHttpProfiler$ServerRequest;->request_size:J

    .line 85
    iput-object p4, p0, Lcom/squareup/analytics/LoggingHttpProfiler$ServerRequest;->base_url:Ljava/lang/String;

    .line 86
    iput-wide p7, p0, Lcom/squareup/analytics/LoggingHttpProfiler$ServerRequest;->response_size:J

    .line 87
    iput p9, p0, Lcom/squareup/analytics/LoggingHttpProfiler$ServerRequest;->response_status:I

    .line 88
    iput-wide p10, p0, Lcom/squareup/analytics/LoggingHttpProfiler$ServerRequest;->duration_ms:J

    return-void
.end method
