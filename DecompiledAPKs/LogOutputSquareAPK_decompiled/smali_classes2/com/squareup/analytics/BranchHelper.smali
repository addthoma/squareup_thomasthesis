.class public Lcom/squareup/analytics/BranchHelper;
.super Ljava/lang/Object;
.source "BranchHelper.java"

# interfaces
.implements Lcom/squareup/analytics/DeepLinkHelper;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/analytics/BranchHelper$Module;
    }
.end annotation


# static fields
.field private static final ANONYMOUS_VISITOR_TOKEN:Ljava/lang/String; = "$anonymous_visitor_token"

.field private static final DEEPLINK_PATH:Ljava/lang/String; = "$android_deeplink_path"

.field private static final ENCRYPTED_EMAIL:Ljava/lang/String; = "$encrypted_email"


# instance fields
.field private application:Landroid/app/Application;

.field private final branch:Lio/branch/referral/Branch;

.field private latestConfig:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field

.field private final listener:Lio/branch/referral/Branch$BranchReferralInitListener;


# direct methods
.method constructor <init>(Landroid/app/Application;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/analytics/BranchHelper;->latestConfig:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 34
    new-instance v0, Lcom/squareup/analytics/-$$Lambda$BranchHelper$sIZSQ8-LC0majejoNj90brLHHQk;

    invoke-direct {v0, p0}, Lcom/squareup/analytics/-$$Lambda$BranchHelper$sIZSQ8-LC0majejoNj90brLHHQk;-><init>(Lcom/squareup/analytics/BranchHelper;)V

    iput-object v0, p0, Lcom/squareup/analytics/BranchHelper;->listener:Lio/branch/referral/Branch$BranchReferralInitListener;

    .line 44
    iput-object p1, p0, Lcom/squareup/analytics/BranchHelper;->application:Landroid/app/Application;

    .line 45
    invoke-static {p1}, Lio/branch/referral/Branch;->getInstance(Landroid/content/Context;)Lio/branch/referral/Branch;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/analytics/BranchHelper;->branch:Lio/branch/referral/Branch;

    return-void
.end method

.method static synthetic lambda$nonEmptyParameter$1(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/Boolean;
    .locals 0

    if-eqz p1, :cond_0

    .line 58
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$nonEmptyParameter$2(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 3

    .line 61
    :try_start_0
    invoke-virtual {p1, p0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p1

    .line 64
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to get known key "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method static synthetic lambda$nonEmptyParameter$3(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 0

    .line 67
    invoke-static {p0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private nonEmptyParameter(Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/analytics/BranchHelper;->latestConfig:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/analytics/-$$Lambda$BranchHelper$rx-WZ4NQ2VTayAgnZV94vU29kyo;

    invoke-direct {v1, p1}, Lcom/squareup/analytics/-$$Lambda$BranchHelper$rx-WZ4NQ2VTayAgnZV94vU29kyo;-><init>(Ljava/lang/String;)V

    .line 58
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/analytics/-$$Lambda$BranchHelper$Phg_c2r56ldop7ef-O_X64juZnU;

    invoke-direct {v1, p1}, Lcom/squareup/analytics/-$$Lambda$BranchHelper$Phg_c2r56ldop7ef-O_X64juZnU;-><init>(Ljava/lang/String;)V

    .line 59
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/analytics/-$$Lambda$BranchHelper$qGyoZMNx8dvBPZwkaTZjIClz3ss;->INSTANCE:Lcom/squareup/analytics/-$$Lambda$BranchHelper$qGyoZMNx8dvBPZwkaTZjIClz3ss;

    .line 67
    invoke-virtual {p1, v0}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public anonymousVisitorToken()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "$anonymous_visitor_token"

    .line 75
    invoke-direct {p0, v0}, Lcom/squareup/analytics/BranchHelper;->nonEmptyParameter(Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public deepLinkPath()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "$android_deeplink_path"

    .line 80
    invoke-direct {p0, v0}, Lcom/squareup/analytics/BranchHelper;->nonEmptyParameter(Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/-$$Lambda$_dGH6AjOMswyVOYh_OjC-sYtnFI;->INSTANCE:Lcom/squareup/analytics/-$$Lambda$_dGH6AjOMswyVOYh_OjC-sYtnFI;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public encryptedEmail()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "$encrypted_email"

    .line 71
    invoke-direct {p0, v0}, Lcom/squareup/analytics/BranchHelper;->nonEmptyParameter(Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public init()V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/analytics/BranchHelper;->application:Landroid/app/Application;

    invoke-static {v0}, Lio/branch/referral/Branch;->getAutoInstance(Landroid/content/Context;)Lio/branch/referral/Branch;

    return-void
.end method

.method public synthetic lambda$new$0$BranchHelper(Lorg/json/JSONObject;Lio/branch/referral/BranchError;)V
    .locals 1

    if-nez p2, :cond_0

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p1, p2, v0

    const-string v0, "got branch info\n%s"

    .line 36
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    iget-object p2, p0, Lcom/squareup/analytics/BranchHelper;->latestConfig:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onActivityCreate(Landroid/app/Activity;)V
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/analytics/BranchHelper;->branch:Lio/branch/referral/Branch;

    iget-object v1, p0, Lcom/squareup/analytics/BranchHelper;->listener:Lio/branch/referral/Branch$BranchReferralInitListener;

    invoke-virtual {v0, v1, p1}, Lio/branch/referral/Branch;->initSession(Lio/branch/referral/Branch$BranchReferralInitListener;Landroid/app/Activity;)Z

    return-void
.end method
