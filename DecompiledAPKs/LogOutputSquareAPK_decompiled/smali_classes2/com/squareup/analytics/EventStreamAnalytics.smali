.class public Lcom/squareup/analytics/EventStreamAnalytics;
.super Ljava/lang/Object;
.source "EventStreamAnalytics.java"

# interfaces
.implements Lcom/squareup/analytics/Analytics;


# static fields
.field private static final ES2_NO_COUNTRY:Ljava/lang/String; = null

.field private static final LOGGED_OUT_TOKEN:Ljava/lang/String; = "ANONYMOUS"


# instance fields
.field private final adIdCache:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final eventStreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

.field private final eventstream:Lcom/squareup/eventstream/v1/EventStream;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardRedirectSettings:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final subjectBuilder:Lcom/squareup/protos/eventstream/v1/Subject$Builder;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream;Lcom/squareup/eventstream/v2/EventstreamV2;Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/eventstream/v1/EventStream;",
            "Lcom/squareup/eventstream/v2/EventstreamV2;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventstream:Lcom/squareup/eventstream/v1/EventStream;

    .line 49
    iput-object p2, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventStreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    .line 50
    iput-object p3, p0, Lcom/squareup/analytics/EventStreamAnalytics;->localeProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p4, p0, Lcom/squareup/analytics/EventStreamAnalytics;->onboardRedirectSettings:Lcom/squareup/settings/LocalSetting;

    .line 52
    iput-object p5, p0, Lcom/squareup/analytics/EventStreamAnalytics;->adIdCache:Lcom/squareup/settings/LocalSetting;

    const-string p1, ""

    .line 54
    invoke-interface {p4, p1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/analytics/EventStreamAnalytics;->setSubjectAnonymousToken(Ljava/lang/String;)V

    .line 56
    sget-object p2, Lcom/squareup/analytics/EventStreamAnalytics;->ES2_NO_COUNTRY:Ljava/lang/String;

    const-string p3, "ANONYMOUS"

    invoke-direct {p0, p3, p3, p2}, Lcom/squareup/analytics/EventStreamAnalytics;->setEs2Subject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-interface {p5}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/squareup/analytics/EventStreamAnalytics;->setAdvertisingId(Ljava/lang/String;)V

    .line 59
    new-instance p2, Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;-><init>()V

    .line 60
    invoke-virtual {p2, p3}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->user_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    move-result-object p2

    .line 61
    invoke-virtual {p2, p1}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->anonymized_user_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/analytics/EventStreamAnalytics;->subjectBuilder:Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    .line 62
    iget-object p1, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventstream:Lcom/squareup/eventstream/v1/EventStream;

    invoke-virtual {p1}, Lcom/squareup/eventstream/v1/EventStream;->state()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    iget-object p2, p0, Lcom/squareup/analytics/EventStreamAnalytics;->subjectBuilder:Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    invoke-virtual {p2}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->build()Lcom/squareup/protos/eventstream/v1/Subject;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setSubject(Lcom/squareup/protos/eventstream/v1/Subject;)V

    return-void
.end method

.method private logEvent(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventstream:Lcom/squareup/eventstream/v1/EventStream;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/eventstream/v1/EventStream;->log(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method private setEs2Subject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 188
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventStreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v2/EventstreamV2;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    const-string v1, "subject_user_token"

    .line 189
    invoke-virtual {v0, v1, p1}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "subject_merchant_token"

    .line 190
    invoke-virtual {v0, p1, p2}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "subject_registered_country_code"

    .line 191
    invoke-virtual {v0, p1, p3}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private setSubjectAnonymousToken(Ljava/lang/String;)V
    .locals 2

    .line 183
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventStreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v2/EventstreamV2;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    const-string v1, "subject_anonymous_token"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public clearCommonProperty(Ljava/lang/String;)V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventstream:Lcom/squareup/eventstream/v1/EventStream;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v1/EventStream;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    invoke-virtual {v0, p1}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->clearCommonProperty(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventStreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v2/EventstreamV2;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    invoke-virtual {v0, p1}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->clearCommonProperty(Ljava/lang/String;)V

    return-void
.end method

.method public clearUser()V
    .locals 4

    .line 99
    sget-object v0, Lcom/squareup/analytics/EventStreamAnalytics;->ES2_NO_COUNTRY:Ljava/lang/String;

    const-string v1, "ANONYMOUS"

    invoke-direct {p0, v1, v1, v0}, Lcom/squareup/analytics/EventStreamAnalytics;->setEs2Subject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventstream:Lcom/squareup/eventstream/v1/EventStream;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v1/EventStream;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    .line 102
    iget-object v2, p0, Lcom/squareup/analytics/EventStreamAnalytics;->subjectBuilder:Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    const-string v3, ""

    .line 103
    invoke-virtual {v2, v3}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->account_country_code(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    move-result-object v2

    .line 104
    invoke-virtual {v2, v1}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->user_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    move-result-object v2

    .line 105
    invoke-virtual {v2, v1}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    .line 106
    iget-object v1, p0, Lcom/squareup/analytics/EventStreamAnalytics;->subjectBuilder:Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->build()Lcom/squareup/protos/eventstream/v1/Subject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setSubject(Lcom/squareup/protos/eventstream/v1/Subject;)V

    return-void
.end method

.method public varargs log(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 127
    iget-object p2, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventstream:Lcom/squareup/eventstream/v1/EventStream;

    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "legacy:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, v0, p1}, Lcom/squareup/eventstream/v1/EventStream;->log(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method public logAction(Lcom/squareup/analytics/RegisterActionName;)V
    .locals 1

    .line 163
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/squareup/analytics/EventStreamAnalytics;->logEvent(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method public logCrashSync(Lcom/squareup/analytics/event/v1/CrashEvent;)V
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventstream:Lcom/squareup/eventstream/v1/EventStream;

    invoke-virtual {v0, p1}, Lcom/squareup/eventstream/v1/EventStream;->logSync(Ljava/lang/Object;)V

    return-void
.end method

.method public logError(Lcom/squareup/analytics/RegisterErrorName;)V
    .locals 1

    .line 167
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ERROR:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterErrorName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/squareup/analytics/EventStreamAnalytics;->logEvent(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method public logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventstream:Lcom/squareup/eventstream/v1/EventStream;

    invoke-virtual {v0, p1}, Lcom/squareup/eventstream/v1/EventStream;->log(Ljava/lang/Object;)V

    return-void
.end method

.method public logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventStreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    invoke-virtual {v0, p1}, Lcom/squareup/eventstream/v2/EventstreamV2;->log(Ljava/lang/Object;)V

    return-void
.end method

.method public logPaymentFinished(Ljava/lang/String;)V
    .locals 1

    .line 175
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->FINISHED_PAYMENT:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-direct {p0, v0, p1}, Lcom/squareup/analytics/EventStreamAnalytics;->logEvent(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method public logReaderEvent(Lcom/squareup/analytics/RegisterReaderName;)V
    .locals 1

    .line 171
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->READER:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterReaderName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/squareup/analytics/EventStreamAnalytics;->logEvent(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method public logSelect(Lcom/squareup/analytics/RegisterSelectName;)V
    .locals 1

    .line 159
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->SELECT:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterSelectName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/squareup/analytics/EventStreamAnalytics;->logEvent(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method public logTap(Lcom/squareup/analytics/RegisterTapName;)V
    .locals 1

    .line 155
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TAP:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterTapName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/squareup/analytics/EventStreamAnalytics;->logEvent(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method public logView(Lcom/squareup/analytics/RegisterViewName;)V
    .locals 2

    .line 144
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->UNKNOWN:Lcom/squareup/analytics/RegisterViewName;

    if-eq p1, v0, :cond_0

    .line 148
    new-instance v0, Lcom/squareup/analytics/event/ViewEvent;

    invoke-direct {v0, p1}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Lcom/squareup/analytics/RegisterViewName;)V

    invoke-virtual {p0, v0}, Lcom/squareup/analytics/EventStreamAnalytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 151
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->VIEW:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/squareup/analytics/EventStreamAnalytics;->logEvent(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void

    .line 145
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Do not call logView with "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->UNKNOWN:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onActivityStart()V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventstream:Lcom/squareup/eventstream/v1/EventStream;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v1/EventStream;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    iget-object v1, p0, Lcom/squareup/analytics/EventStreamAnalytics;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-virtual {v0, v1}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setLocale(Ljava/util/Locale;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventStreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v2/EventstreamV2;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    iget-object v1, p0, Lcom/squareup/analytics/EventStreamAnalytics;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-virtual {v0, v1}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->setLocale(Ljava/util/Locale;)V

    return-void
.end method

.method public setAdvertisingId(Ljava/lang/String;)V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->adIdCache:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 117
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventstream:Lcom/squareup/eventstream/v1/EventStream;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v1/EventStream;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    invoke-virtual {v0, p1}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setAdvertisingId(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventStreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v2/EventstreamV2;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    invoke-virtual {v0, p1}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->setAdvertisingId(Ljava/lang/String;)V

    return-void
.end method

.method public setAnonymizedUserToken(Ljava/lang/String;)V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->onboardRedirectSettings:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 92
    invoke-direct {p0, p1}, Lcom/squareup/analytics/EventStreamAnalytics;->setSubjectAnonymousToken(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->subjectBuilder:Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->anonymized_user_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    .line 95
    iget-object p1, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventstream:Lcom/squareup/eventstream/v1/EventStream;

    invoke-virtual {p1}, Lcom/squareup/eventstream/v1/EventStream;->state()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->subjectBuilder:Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    invoke-virtual {v0}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->build()Lcom/squareup/protos/eventstream/v1/Subject;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setSubject(Lcom/squareup/protos/eventstream/v1/Subject;)V

    return-void
.end method

.method public setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventstream:Lcom/squareup/eventstream/v1/EventStream;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v1/EventStream;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventStreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v2/EventstreamV2;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setLocation(Landroid/location/Location;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Location set to %s"

    .line 110
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventstream:Lcom/squareup/eventstream/v1/EventStream;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v1/EventStream;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    invoke-virtual {v0, p1}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setLocation(Landroid/location/Location;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventStreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    invoke-virtual {v0}, Lcom/squareup/eventstream/v2/EventstreamV2;->state()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    invoke-virtual {v0, p1}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->setLocation(Landroid/location/Location;)V

    return-void
.end method

.method public setUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 81
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/analytics/EventStreamAnalytics;->setEs2Subject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics;->subjectBuilder:Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    .line 84
    invoke-virtual {v0, p3}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->account_country_code(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    move-result-object p3

    .line 85
    invoke-virtual {p3, p1}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->user_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    move-result-object p1

    .line 86
    invoke-virtual {p1, p2}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    .line 87
    iget-object p1, p0, Lcom/squareup/analytics/EventStreamAnalytics;->eventstream:Lcom/squareup/eventstream/v1/EventStream;

    invoke-virtual {p1}, Lcom/squareup/eventstream/v1/EventStream;->state()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    iget-object p2, p0, Lcom/squareup/analytics/EventStreamAnalytics;->subjectBuilder:Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    invoke-virtual {p2}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->build()Lcom/squareup/protos/eventstream/v1/Subject;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setSubject(Lcom/squareup/protos/eventstream/v1/Subject;)V

    return-void
.end method
