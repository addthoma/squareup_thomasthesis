.class public final Lcom/squareup/analytics/LoggingHttpProfiler_Factory;
.super Ljava/lang/Object;
.source "LoggingHttpProfiler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/analytics/LoggingHttpProfiler;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/analytics/LoggingHttpProfiler_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/analytics/LoggingHttpProfiler_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/analytics/LoggingHttpProfiler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/analytics/LoggingHttpProfiler_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/analytics/LoggingHttpProfiler_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/analytics/LoggingHttpProfiler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ldagger/Lazy;Ldagger/Lazy;)Lcom/squareup/analytics/LoggingHttpProfiler;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/analytics/LoggingHttpProfiler;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/analytics/LoggingHttpProfiler;

    invoke-direct {v0, p0, p1}, Lcom/squareup/analytics/LoggingHttpProfiler;-><init>(Ldagger/Lazy;Ldagger/Lazy;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/analytics/LoggingHttpProfiler;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/analytics/LoggingHttpProfiler_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/analytics/LoggingHttpProfiler_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/analytics/LoggingHttpProfiler_Factory;->newInstance(Ldagger/Lazy;Ldagger/Lazy;)Lcom/squareup/analytics/LoggingHttpProfiler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/analytics/LoggingHttpProfiler_Factory;->get()Lcom/squareup/analytics/LoggingHttpProfiler;

    move-result-object v0

    return-object v0
.end method
