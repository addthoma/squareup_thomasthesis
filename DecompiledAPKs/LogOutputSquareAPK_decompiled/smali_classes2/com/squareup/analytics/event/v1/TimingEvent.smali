.class public abstract Lcom/squareup/analytics/event/v1/TimingEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "TimingEvent.java"


# instance fields
.field public final sampling_rate:D


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterTimingName;)V
    .locals 2

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 33
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/analytics/event/v1/TimingEvent;-><init>(Lcom/squareup/analytics/RegisterTimingName;D)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/RegisterTimingName;D)V
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TIMING:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterTimingName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 38
    iput-wide p2, p0, Lcom/squareup/analytics/event/v1/TimingEvent;->sampling_rate:D

    return-void
.end method
