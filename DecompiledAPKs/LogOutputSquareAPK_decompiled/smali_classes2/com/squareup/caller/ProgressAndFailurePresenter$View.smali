.class public interface abstract Lcom/squareup/caller/ProgressAndFailurePresenter$View;
.super Ljava/lang/Object;
.source "ProgressAndFailurePresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/caller/ProgressAndFailurePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "View"
.end annotation


# virtual methods
.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract getFailurePopup()Lcom/squareup/mortar/Popup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mortar/Popup<",
            "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getProgressPopup()Lcom/squareup/mortar/Popup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mortar/Popup<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method
