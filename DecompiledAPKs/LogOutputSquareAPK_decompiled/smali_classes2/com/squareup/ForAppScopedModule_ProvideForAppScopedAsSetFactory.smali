.class public final Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;
.super Ljava/lang/Object;
.source "ForAppScopedModule_ProvideForAppScopedAsSetFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/Set<",
        "Lmortar/Scoped;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final backgroundJobLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/log/BackgroundJobLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadBlockedLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/MainThreadBlockedLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final queueJobCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueJobCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardJobCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardJobCreator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/log/BackgroundJobLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/MainThreadBlockedLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueJobCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardJobCreator;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;->backgroundJobLoggerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;->mainThreadBlockedLoggerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;->queueJobCreatorProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;->storeAndForwardJobCreatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/log/BackgroundJobLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/MainThreadBlockedLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueJobCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardJobCreator;",
            ">;)",
            "Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;"
        }
    .end annotation

    .line 52
    new-instance v0, Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideForAppScopedAsSet(Lcom/squareup/backgroundjob/log/BackgroundJobLogger;Lcom/squareup/log/MainThreadBlockedLogger;Lcom/squareup/queue/QueueJobCreator;Lcom/squareup/payment/offline/StoreAndForwardJobCreator;)Ljava/util/Set;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/backgroundjob/log/BackgroundJobLogger;",
            "Lcom/squareup/log/MainThreadBlockedLogger;",
            "Lcom/squareup/queue/QueueJobCreator;",
            "Lcom/squareup/payment/offline/StoreAndForwardJobCreator;",
            ")",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation

    .line 58
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/ForAppScopedModule;->provideForAppScopedAsSet(Lcom/squareup/backgroundjob/log/BackgroundJobLogger;Lcom/squareup/log/MainThreadBlockedLogger;Lcom/squareup/queue/QueueJobCreator;Lcom/squareup/payment/offline/StoreAndForwardJobCreator;)Ljava/util/Set;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;->get()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;->backgroundJobLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/backgroundjob/log/BackgroundJobLogger;

    iget-object v1, p0, Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;->mainThreadBlockedLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/log/MainThreadBlockedLogger;

    iget-object v2, p0, Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;->queueJobCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/queue/QueueJobCreator;

    iget-object v3, p0, Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;->storeAndForwardJobCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/payment/offline/StoreAndForwardJobCreator;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ForAppScopedModule_ProvideForAppScopedAsSetFactory;->provideForAppScopedAsSet(Lcom/squareup/backgroundjob/log/BackgroundJobLogger;Lcom/squareup/log/MainThreadBlockedLogger;Lcom/squareup/queue/QueueJobCreator;Lcom/squareup/payment/offline/StoreAndForwardJobCreator;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
