.class public Lcom/squareup/cashmanagement/CashReportPayloadFactory;
.super Ljava/lang/Object;
.source "CashReportPayloadFactory.java"


# instance fields
.field private final dateFormatter:Ljava/text/DateFormat;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final timeFormatter:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/permissions/EmployeeManagement;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->res:Lcom/squareup/util/Res;

    .line 37
    iput-object p2, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->dateFormatter:Ljava/text/DateFormat;

    .line 38
    iput-object p3, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->timeFormatter:Ljava/text/DateFormat;

    .line 39
    iput-object p4, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 40
    iput-object p5, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-void
.end method

.method private appendFullDateFormatted(Ljava/lang/StringBuilder;Ljava/util/Date;)V
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->dateFormatter:Ljava/text/DateFormat;

    invoke-virtual {v0, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 156
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    invoke-direct {p0, p1, p2}, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->appendTime(Ljava/lang/StringBuilder;Ljava/util/Date;)V

    return-void
.end method

.method private appendTime(Ljava/lang/StringBuilder;Ljava/util/Date;)V
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->timeFormatter:Ljava/text/DateFormat;

    invoke-virtual {v0, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    .line 162
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private canViewExpectedInDrawer(Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Z
    .locals 2

    .line 167
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    sget-object v1, Lcom/squareup/permissions/Permission;->VIEW_EXPECTED_IN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 168
    invoke-interface {p1}, Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;->getHasViewAmountInCashDrawerPermission()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private reportDateRange(Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 143
    invoke-direct {p0, v0, p1}, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->appendFullDateFormatted(Ljava/lang/StringBuilder;Ljava/util/Date;)V

    const-string v1, " \u2014 "

    .line 144
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    invoke-static {p1, p2}, Lcom/squareup/util/Times;->onDifferentDay(Ljava/util/Date;Ljava/util/Date;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 146
    invoke-direct {p0, v0, p2}, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->appendFullDateFormatted(Ljava/lang/StringBuilder;Ljava/util/Date;)V

    goto :goto_0

    .line 148
    :cond_0
    invoke-direct {p0, v0, p2}, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->appendTime(Ljava/lang/StringBuilder;Ljava/util/Date;)V

    .line 150
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public buildPayload(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Ljava/util/List;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Lcom/squareup/cashmanagement/CashReportPayload;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ")",
            "Lcom/squareup/cashmanagement/CashReportPayload;"
        }
    .end annotation

    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->toHeaderSection(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;

    move-result-object v0

    .line 47
    new-instance v1, Lcom/squareup/cashmanagement/DrawerSummarySection;

    iget-object v2, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/cashmanagement/R$string;->uppercase_drawer_report_summary:I

    .line 48
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 49
    invoke-virtual {p0, p1, p3}, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->regularSummaryRows(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Ljava/util/List;

    move-result-object v3

    .line 50
    invoke-virtual {p0, p1, p3}, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->mediumSummaryRows(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/cashmanagement/DrawerSummarySection;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    .line 51
    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    invoke-static {v2, p1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 52
    new-instance v2, Lcom/squareup/cashmanagement/PaidInOutSection;

    iget-object v3, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/cashmanagement/R$string;->uppercase_paid_in_out:I

    .line 53
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 54
    invoke-virtual {p0, p2, p3}, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->paidInOutRows(Ljava/util/List;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Ljava/util/List;

    move-result-object p2

    new-instance p3, Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v4, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/cashmanagement/R$string;->paid_in_out_total:I

    .line 55
    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 56
    invoke-interface {v5, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, v4, p1}, Lcom/squareup/print/payload/LabelAmountPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v3, p2, p3}, Lcom/squareup/cashmanagement/PaidInOutSection;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;)V

    .line 57
    new-instance p1, Lcom/squareup/cashmanagement/CashReportPayload;

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/cashmanagement/CashReportPayload;-><init>(Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;Lcom/squareup/cashmanagement/DrawerSummarySection;Lcom/squareup/cashmanagement/PaidInOutSection;)V

    return-object p1
.end method

.method mediumSummaryRows(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 81
    invoke-direct {p0, p2}, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->canViewExpectedInDrawer(Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    new-instance v1, Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v2, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/cashmanagement/R$string;->cash_drawer_expected_in_drawer:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v4, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 83
    invoke-interface {v3, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/print/payload/LabelAmountPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_0
    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    sget-object v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->CLOSED:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "________"

    if-eqz v1, :cond_1

    .line 88
    iget-object v1, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_cash_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 89
    iget-object v1, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_cash_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 90
    invoke-static {v3, p1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 89
    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 90
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    move-object p1, v2

    .line 92
    :goto_0
    new-instance v1, Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v3, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/cashmanagement/R$string;->cash_drawer_actual_in_drawer:I

    .line 93
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Lcom/squareup/print/payload/LabelAmountPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    invoke-direct {p0, p2}, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->canViewExpectedInDrawer(Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 95
    new-instance p2, Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v1, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cashmanagement/R$string;->cash_drawer_difference:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p2, v1, p1}, Lcom/squareup/print/payload/LabelAmountPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v0
.end method

.method paidInOutRows(Ljava/util/List;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/cashmanagement/PaidInOutSection$PaidInOutEvent;",
            ">;"
        }
    .end annotation

    .line 103
    invoke-direct {p0, p2}, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->canViewExpectedInDrawer(Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 104
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 107
    :cond_0
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 108
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    .line 111
    iget-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v1, v1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 112
    iget-object v2, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v3, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_IN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "time"

    if-eqz v2, :cond_1

    .line 113
    iget-object v2, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/cashmanagement/R$string;->cash_drawer_report_paid_in:I

    invoke-interface {v2, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v4, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->timeFormatter:Ljava/text/DateFormat;

    .line 114
    invoke-virtual {v4, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 115
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 116
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 117
    iget-object v2, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 119
    :cond_1
    iget-object v2, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/cashmanagement/R$string;->cash_drawer_report_paid_out:I

    invoke-interface {v2, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v4, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->timeFormatter:Ljava/text/DateFormat;

    .line 120
    invoke-virtual {v4, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 121
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 122
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 123
    iget-object v2, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    invoke-static {v3}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 125
    :goto_1
    new-instance v3, Lcom/squareup/cashmanagement/PaidInOutSection$PaidInOutEvent;

    iget-object v0, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->description:Ljava/lang/String;

    invoke-direct {v3, v1, v2, v0}, Lcom/squareup/cashmanagement/PaidInOutSection$PaidInOutEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object p2
.end method

.method regularSummaryRows(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 63
    new-instance v1, Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v2, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/cashmanagement/R$string;->cash_drawer_starting_cash:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v4, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->starting_cash_money:Lcom/squareup/protos/common/Money;

    .line 64
    invoke-interface {v3, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/print/payload/LabelAmountPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    invoke-direct {p0, p2}, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->canViewExpectedInDrawer(Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 66
    new-instance p2, Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v1, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cashmanagement/R$string;->cash_drawer_cash_sales:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_payment_money:Lcom/squareup/protos/common/Money;

    .line 67
    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p2, v1, v2}, Lcom/squareup/print/payload/LabelAmountPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    new-instance p2, Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v1, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cashmanagement/R$string;->cash_drawer_cash_refunds:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    .line 69
    invoke-static {v3}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p2, v1, v2}, Lcom/squareup/print/payload/LabelAmountPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    .line 71
    invoke-static {p2, p1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 72
    new-instance p2, Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v1, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cashmanagement/R$string;->cash_drawer_paid_in_out:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 73
    invoke-interface {v2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v1, p1}, Lcom/squareup/print/payload/LabelAmountPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method toHeaderSection(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;
    .locals 3

    .line 132
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cashmanagement/R$string;->cash_drawer_report_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    const-string v2, ""

    .line 133
    invoke-static {v1, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "description"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 135
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 136
    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v1, v1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p1, p1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    .line 137
    invoke-static {p1}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    .line 136
    invoke-direct {p0, v1, p1}, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->reportDateRange(Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    .line 138
    new-instance v1, Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;

    invoke-direct {v1, v0, p1}, Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method
