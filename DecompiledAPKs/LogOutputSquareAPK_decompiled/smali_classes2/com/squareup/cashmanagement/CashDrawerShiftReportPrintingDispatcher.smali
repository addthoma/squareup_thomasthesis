.class public Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;
.super Ljava/lang/Object;
.source "CashDrawerShiftReportPrintingDispatcher.java"


# static fields
.field private static final PRINT_JOB_SOURCE:Ljava/lang/String; = "CrashDrawer"


# instance fields
.field private final cashReportPayloadFactory:Lcom/squareup/cashmanagement/CashReportPayloadFactory;

.field private final printSpooler:Lcom/squareup/print/PrintSpooler;

.field private final printerStations:Lcom/squareup/print/PrinterStations;


# direct methods
.method public constructor <init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrinterStations;Lcom/squareup/cashmanagement/CashReportPayloadFactory;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    .line 22
    iput-object p2, p0, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 23
    iput-object p3, p0, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;->cashReportPayloadFactory:Lcom/squareup/cashmanagement/CashReportPayloadFactory;

    return-void
.end method


# virtual methods
.method public print(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Ljava/util/List;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ")V"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    .line 29
    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->getEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object v0

    .line 30
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 31
    iget-object v1, p0, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;->cashReportPayloadFactory:Lcom/squareup/cashmanagement/CashReportPayloadFactory;

    .line 32
    invoke-virtual {v1, p1, p2, p3}, Lcom/squareup/cashmanagement/CashReportPayloadFactory;->buildPayload(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Ljava/util/List;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Lcom/squareup/cashmanagement/CashReportPayload;

    move-result-object p1

    .line 34
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/print/PrinterStation;

    .line 35
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    const-string v1, "Cash Drawer Report"

    const-string v2, "CrashDrawer"

    invoke-virtual {v0, p3, p1, v1, v2}, Lcom/squareup/print/PrintSpooler;->enqueueForPrint(Lcom/squareup/print/PrintTarget;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method
