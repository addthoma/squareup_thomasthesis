.class Lcom/squareup/cashmanagement/CashDrawerShiftCursor$1;
.super Ljava/lang/Object;
.source "CashDrawerShiftCursor.java"

# interfaces
.implements Lcom/squareup/cashmanagement/CashDrawerShiftRow;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->getCurrent()Lcom/squareup/cashmanagement/CashDrawerShiftRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;


# direct methods
.method constructor <init>(Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/cashmanagement/CashDrawerShiftCursor$1;->this$0:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDate()Ljava/util/Date;
    .locals 3

    .line 31
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/squareup/cashmanagement/CashDrawerShiftCursor$1;->this$0:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-static {v1}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->access$100(Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->getLong(I)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftCursor$1;->this$0:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-static {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->access$200(Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftCursor$1;->this$0:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-static {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->access$000(Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
