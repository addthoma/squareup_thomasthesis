.class public final Lcom/squareup/cashmanagement/NoExtraCashManagementPermissionsHolder;
.super Ljava/lang/Object;
.source "NoExtraCashManagementPermissionsHolder.kt"

# interfaces
.implements Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\n\u001a\u00020\u000bH\u0016R$\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00048V@VX\u0096\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\t\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/cashmanagement/NoExtraCashManagementPermissionsHolder;",
        "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
        "()V",
        "<anonymous parameter 0>",
        "",
        "hasViewAmountInCashDrawerPermission",
        "getHasViewAmountInCashDrawerPermission",
        "()Z",
        "setHasViewAmountInCashDrawerPermission",
        "(Z)V",
        "revokeHasViewAmountInCashDrawerPermission",
        "",
        "cashmanagement_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHasViewAmountInCashDrawerPermission()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public revokeHasViewAmountInCashDrawerPermission()V
    .locals 2

    .line 9
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This object should be readonly"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public setHasViewAmountInCashDrawerPermission(Z)V
    .locals 1

    .line 13
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "This object should be readonly"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
