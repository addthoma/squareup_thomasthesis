.class public interface abstract Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$TaskQueuer;
.super Ljava/lang/Object;
.source "RealCashDrawerShiftManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TaskQueuer"
.end annotation


# virtual methods
.method public abstract cashDrawerShiftClose(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
.end method

.method public abstract cashDrawerShiftCreate(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
.end method

.method public abstract cashDrawerShiftEmail(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract cashDrawerShiftEnd(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
.end method

.method public abstract cashDrawerShiftUpdate(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
.end method
