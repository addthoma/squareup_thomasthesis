.class Lcom/squareup/cogs/ItemsSyncEvent;
.super Lcom/squareup/analytics/event/v1/TimingEvent;
.source "ItemsSyncEvent.java"


# instance fields
.field final database_duration_ms:J

.field final database_objects_per_second:I

.field final initial_sync:Z

.field final number_of_objects:I

.field final number_of_objects_deleted:I

.field final number_of_objects_updated:I

.field final total_duration_ms:J

.field final total_object_per_second:I


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;)V
    .locals 2

    .line 40
    sget-object v0, Lcom/squareup/analytics/RegisterTimingName;->ITEMS_SYNC:Lcom/squareup/analytics/RegisterTimingName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/TimingEvent;-><init>(Lcom/squareup/analytics/RegisterTimingName;)V

    .line 41
    iget-boolean v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->initialSync:Z

    iput-boolean v0, p0, Lcom/squareup/cogs/ItemsSyncEvent;->initial_sync:Z

    .line 42
    iget v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->numberOfObjects:I

    iput v0, p0, Lcom/squareup/cogs/ItemsSyncEvent;->number_of_objects:I

    .line 43
    iget v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->numberOfObjectsUpdated:I

    iput v0, p0, Lcom/squareup/cogs/ItemsSyncEvent;->number_of_objects_updated:I

    .line 44
    iget v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->numberOfObjectsDeleted:I

    iput v0, p0, Lcom/squareup/cogs/ItemsSyncEvent;->number_of_objects_deleted:I

    .line 45
    iget-wide v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->totalDurationMs:J

    iput-wide v0, p0, Lcom/squareup/cogs/ItemsSyncEvent;->total_duration_ms:J

    .line 46
    iget v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->totalObjectsPerSecond:I

    iput v0, p0, Lcom/squareup/cogs/ItemsSyncEvent;->total_object_per_second:I

    .line 47
    iget-wide v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->databaseDurationMs:J

    iput-wide v0, p0, Lcom/squareup/cogs/ItemsSyncEvent;->database_duration_ms:J

    .line 48
    iget p1, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->databaseObjectsPerSecond:I

    iput p1, p0, Lcom/squareup/cogs/ItemsSyncEvent;->database_objects_per_second:I

    return-void
.end method
