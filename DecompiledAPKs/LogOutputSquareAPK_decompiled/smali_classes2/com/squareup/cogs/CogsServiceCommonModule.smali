.class public final Lcom/squareup/cogs/CogsServiceCommonModule;
.super Ljava/lang/Object;
.source "CogsServiceCommonModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCogsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/cogs/CogsService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitCogs;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 17
    const-class v0, Lcom/squareup/cogs/CogsService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cogs/CogsService;

    return-object p0
.end method
