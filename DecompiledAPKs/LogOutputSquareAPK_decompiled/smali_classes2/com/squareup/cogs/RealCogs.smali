.class public final Lcom/squareup/cogs/RealCogs;
.super Lcom/squareup/shared/catalog/RealCatalog;
.source "RealCogs.java"

# interfaces
.implements Lcom/squareup/cogs/Cogs;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cogs/RealCogs$RxProgressNotifier;,
        Lcom/squareup/cogs/RealCogs$RealCatalogThreadEnforcer;
    }
.end annotation


# instance fields
.field private final mainScheduler:Lrx/Scheduler;

.field private final supportedConnectV2ObjectTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field

.field private final syncProgressRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/cogs/SqliteCatalogStoreFactory;Lcom/squareup/shared/catalog/CatalogFile;Ljava/util/List;Lrx/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/FileThreadEnforcer;Ljava/io/File;Lcom/squareup/shared/catalog/CatalogEndpoint;Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/logging/CatalogAnalytics;Lcom/squareup/shared/catalog/logging/Clock;Lcom/jakewharton/rxrelay/BehaviorRelay;Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;Ljava/util/List;Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;Ljava/util/concurrent/Executor;)V
    .locals 18
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/SqliteCatalogStoreFactory;",
            "Lcom/squareup/shared/catalog/CatalogFile<",
            "Lcom/squareup/shared/catalog/StorageMetadata;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;",
            ">;",
            "Lrx/Scheduler;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/FileThreadEnforcer;",
            "Ljava/io/File;",
            "Lcom/squareup/shared/catalog/CatalogEndpoint;",
            "Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/shared/catalog/logging/CatalogAnalytics;",
            "Lcom/squareup/shared/catalog/logging/Clock;",
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;",
            "Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    move-object/from16 v10, p0

    move-object/from16 v8, p13

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v13, p3

    move-object/from16 v5, p5

    move-object/from16 v15, p7

    move-object/from16 v3, p8

    move-object/from16 v9, p9

    move-object/from16 v6, p10

    move-object/from16 v11, p11

    move-object/from16 v14, p12

    move-object/from16 v12, p14

    move-object/from16 v16, p15

    move-object/from16 v4, p16

    move-object/from16 v7, p17

    .line 87
    new-instance v10, Lcom/squareup/cogs/RealCogs$RealCatalogThreadEnforcer;

    move-object/from16 v17, v0

    move-object v0, v8

    move-object v8, v10

    const/4 v1, 0x0

    move-object/from16 v2, p6

    invoke-direct {v10, v2, v1}, Lcom/squareup/cogs/RealCogs$RealCatalogThreadEnforcer;-><init>(Lcom/squareup/FileThreadEnforcer;Lcom/squareup/cogs/RealCogs$1;)V

    new-instance v2, Lcom/squareup/cogs/RealCogs$RxProgressNotifier;

    move-object v10, v2

    invoke-direct {v2, v0, v1}, Lcom/squareup/cogs/RealCogs$RxProgressNotifier;-><init>(Lcom/jakewharton/rxrelay/BehaviorRelay;Lcom/squareup/cogs/RealCogs$1;)V

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/shared/catalog/RealCatalog;-><init>(Lcom/squareup/shared/catalog/CatalogStore$Factory;Lcom/squareup/shared/catalog/CatalogFile;Lcom/squareup/shared/catalog/CatalogEndpoint;Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;Lcom/squareup/shared/catalog/logging/CatalogAnalytics;Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;Ljava/util/List;Lcom/squareup/shared/catalog/logging/Clock;Ljava/io/File;Ljava/util/List;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p13

    .line 91
    iput-object v1, v0, Lcom/squareup/cogs/RealCogs;->syncProgressRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-object/from16 v1, p4

    .line 92
    iput-object v1, v0, Lcom/squareup/cogs/RealCogs;->mainScheduler:Lrx/Scheduler;

    move-object/from16 v1, p15

    .line 93
    iput-object v1, v0, Lcom/squareup/cogs/RealCogs;->supportedConnectV2ObjectTypes:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Lcom/squareup/cogs/SqliteCatalogStoreFactory;Lcom/squareup/shared/catalog/CatalogFile;Ljava/util/List;Lrx/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/FileThreadEnforcer;Ljava/io/File;Lcom/squareup/shared/catalog/CatalogEndpoint;Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/logging/CatalogAnalytics;Lcom/squareup/shared/catalog/logging/Clock;Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;Ljava/util/List;Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;Ljava/util/concurrent/Executor;)V
    .locals 18
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/SqliteCatalogStoreFactory;",
            "Lcom/squareup/shared/catalog/CatalogFile<",
            "Lcom/squareup/shared/catalog/StorageMetadata;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;",
            ">;",
            "Lrx/Scheduler;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/FileThreadEnforcer;",
            "Ljava/io/File;",
            "Lcom/squareup/shared/catalog/CatalogEndpoint;",
            "Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/shared/catalog/logging/CatalogAnalytics;",
            "Lcom/squareup/shared/catalog/logging/Clock;",
            "Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;",
            "Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 65
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v13

    .line 63
    invoke-direct/range {v0 .. v17}, Lcom/squareup/cogs/RealCogs;-><init>(Lcom/squareup/cogs/SqliteCatalogStoreFactory;Lcom/squareup/shared/catalog/CatalogFile;Ljava/util/List;Lrx/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/FileThreadEnforcer;Ljava/io/File;Lcom/squareup/shared/catalog/CatalogEndpoint;Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/logging/CatalogAnalytics;Lcom/squareup/shared/catalog/logging/Clock;Lcom/jakewharton/rxrelay/BehaviorRelay;Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;Ljava/util/List;Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "TT;>;)",
            "Lrx/Single<",
            "TT;>;"
        }
    .end annotation

    .line 97
    invoke-static {p0, p1}, Lcom/squareup/cogs/CogsTasks;->asSingle(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public asSingleOnline(Lcom/squareup/shared/catalog/CatalogOnlineTask;)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogOnlineTask<",
            "TT;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TT;>;>;"
        }
    .end annotation

    .line 103
    invoke-static {p0, p1}, Lcom/squareup/cogs/CogsTasks;->asSingleOnline(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogOnlineTask;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public asSingleOnlineThenLocal(Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask<",
            "TT;TS;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TS;>;>;"
        }
    .end annotation

    .line 108
    invoke-static {p0, p1}, Lcom/squareup/cogs/CogsTasks;->asSingleOnlineThenLocal(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public cogsSyncProgress()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 129
    iget-object v0, p0, Lcom/squareup/cogs/RealCogs;->syncProgressRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/cogs/RealCogs;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public foregroundSyncAsSingle(Lcom/squareup/shared/catalog/utils/ElapsedTime;J)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            "J)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation

    .line 119
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/cogs/CogsTasks;->foregroundSyncAsSingle(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/utils/ElapsedTime;J)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public shouldForegroundSyncAsSingle(Lcom/squareup/shared/catalog/utils/ElapsedTime;)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .line 125
    invoke-static {p0, p1}, Lcom/squareup/cogs/CogsTasks;->shouldForegroundSyncAsSingle(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/utils/ElapsedTime;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public syncAsSingle()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation

    .line 112
    invoke-static {p0}, Lcom/squareup/cogs/CogsTasks;->syncAsSingle(Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method
