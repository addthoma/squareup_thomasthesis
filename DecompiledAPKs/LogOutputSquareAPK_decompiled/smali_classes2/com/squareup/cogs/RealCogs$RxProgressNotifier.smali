.class Lcom/squareup/cogs/RealCogs$RxProgressNotifier;
.super Ljava/lang/Object;
.source "RealCogs.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cogs/RealCogs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RxProgressNotifier"
.end annotation


# instance fields
.field private final relay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/jakewharton/rxrelay/BehaviorRelay;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput-object p1, p0, Lcom/squareup/cogs/RealCogs$RxProgressNotifier;->relay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method

.method synthetic constructor <init>(Lcom/jakewharton/rxrelay/BehaviorRelay;Lcom/squareup/cogs/RealCogs$1;)V
    .locals 0

    .line 148
    invoke-direct {p0, p1}, Lcom/squareup/cogs/RealCogs$RxProgressNotifier;-><init>(Lcom/jakewharton/rxrelay/BehaviorRelay;)V

    return-void
.end method


# virtual methods
.method public onNext(I)V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/cogs/RealCogs$RxProgressNotifier;->relay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
