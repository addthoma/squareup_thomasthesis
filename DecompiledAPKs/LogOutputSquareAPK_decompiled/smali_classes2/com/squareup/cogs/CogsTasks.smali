.class public final Lcom/squareup/cogs/CogsTasks;
.super Ljava/lang/Object;
.source "CogsTasks.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static asSingle(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "TT;>;)",
            "Lrx/Single<",
            "TT;>;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$cKpcmtpATYElHfedrpNUeyMEPX8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cogs/-$$Lambda$CogsTasks$cKpcmtpATYElHfedrpNUeyMEPX8;-><init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogTask;)V

    sget-object p0, Lrx/Emitter$BackpressureMode;->LATEST:Lrx/Emitter$BackpressureMode;

    invoke-static {v0, p0}, Lrx/Observable;->create(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object p0

    .line 57
    invoke-virtual {p0}, Lrx/Observable;->toSingle()Lrx/Single;

    move-result-object p0

    return-object p0
.end method

.method public static asSingleOnline(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogOnlineTask;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/shared/catalog/CatalogOnlineTask<",
            "TT;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TT;>;>;"
        }
    .end annotation

    .line 70
    new-instance v0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$OSqIJ44Bk2ebrmB4oTgGAo2wulo;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cogs/-$$Lambda$CogsTasks$OSqIJ44Bk2ebrmB4oTgGAo2wulo;-><init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogOnlineTask;)V

    sget-object p0, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, p0}, Lio/reactivex/Flowable;->create(Lio/reactivex/FlowableOnSubscribe;Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p0

    .line 76
    invoke-virtual {p0}, Lio/reactivex/Flowable;->singleOrError()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static asSingleOnlineThenLocal(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask<",
            "TT;TS;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TS;>;>;"
        }
    .end annotation

    .line 89
    new-instance v0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$fArCnmjWRtQj34qUvuJM-Dh9sck;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cogs/-$$Lambda$CogsTasks$fArCnmjWRtQj34qUvuJM-Dh9sck;-><init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;)V

    sget-object p0, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, p0}, Lio/reactivex/Flowable;->create(Lio/reactivex/FlowableOnSubscribe;Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p0

    .line 95
    invoke-virtual {p0}, Lio/reactivex/Flowable;->singleOrError()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/CatalogTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "TT;>;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$pm8NFb-xcvHx2Jmovi0YZJqcybk;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cogs/-$$Lambda$CogsTasks$pm8NFb-xcvHx2Jmovi0YZJqcybk;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-object v0
.end method

.method public static foregroundSyncAsSingle(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/utils/ElapsedTime;J)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            "J)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation

    .line 115
    new-instance v0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$tzGkS7_75n92fSHO9GikTV-WLoE;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/cogs/-$$Lambda$CogsTasks$tzGkS7_75n92fSHO9GikTV-WLoE;-><init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/utils/ElapsedTime;J)V

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object p0

    .line 120
    invoke-virtual {p0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$asSingle$2(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogTask;Lrx/Emitter;)V
    .locals 1

    .line 48
    new-instance v0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$v9p5KAGjYPkisPWU4CnhigsPlaQ;

    invoke-direct {v0, p2}, Lcom/squareup/cogs/-$$Lambda$CogsTasks$v9p5KAGjYPkisPWU4CnhigsPlaQ;-><init>(Lrx/Emitter;)V

    invoke-interface {p0, p1, v0}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method static synthetic lambda$asSingleOnline$4(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogOnlineTask;Lio/reactivex/FlowableEmitter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 72
    new-instance v0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$Cd-mUPUZeAyddtJD4hu-x4HGZRc;

    invoke-direct {v0, p2}, Lcom/squareup/cogs/-$$Lambda$CogsTasks$Cd-mUPUZeAyddtJD4hu-x4HGZRc;-><init>(Lio/reactivex/FlowableEmitter;)V

    invoke-interface {p0, p1, v0}, Lcom/squareup/cogs/Cogs;->executeOnline(Lcom/squareup/shared/catalog/CatalogOnlineTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method static synthetic lambda$asSingleOnlineThenLocal$6(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;Lio/reactivex/FlowableEmitter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 91
    new-instance v0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$E01pe5cVzn6lkpcnIsR4BVW4vKI;

    invoke-direct {v0, p2}, Lcom/squareup/cogs/-$$Lambda$CogsTasks$E01pe5cVzn6lkpcnIsR4BVW4vKI;-><init>(Lio/reactivex/FlowableEmitter;)V

    invoke-interface {p0, p1, v0}, Lcom/squareup/cogs/Cogs;->executeOnlineThenLocal(Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method static synthetic lambda$findById$0(Ljava/lang/Class;Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 0

    .line 27
    invoke-interface {p2, p0, p1}, Lcom/squareup/shared/catalog/Catalog$Local;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$foregroundSyncAsSingle$10(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/utils/ElapsedTime;JLio/reactivex/ObservableEmitter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 116
    new-instance v0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$1r2pCB16RL_9VzKmt-WfaFKX4Js;

    invoke-direct {v0, p4}, Lcom/squareup/cogs/-$$Lambda$CogsTasks$1r2pCB16RL_9VzKmt-WfaFKX4Js;-><init>(Lio/reactivex/ObservableEmitter;)V

    invoke-interface {p0, p1, p2, p3, v0}, Lcom/squareup/cogs/Cogs;->foregroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;JLcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method static synthetic lambda$null$1(Lrx/Emitter;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 50
    :try_start_0
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p0, p1}, Lrx/Emitter;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    invoke-interface {p0}, Lrx/Emitter;->onCompleted()V

    return-void

    :catch_0
    move-exception p1

    .line 52
    invoke-interface {p0, p1}, Lrx/Emitter;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic lambda$null$11(Lio/reactivex/ObservableEmitter;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 129
    invoke-interface {p0, p1}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    .line 130
    invoke-interface {p0}, Lio/reactivex/ObservableEmitter;->onComplete()V

    return-void
.end method

.method static synthetic lambda$null$3(Lio/reactivex/FlowableEmitter;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 0

    .line 73
    invoke-interface {p0, p1}, Lio/reactivex/FlowableEmitter;->onNext(Ljava/lang/Object;)V

    .line 74
    invoke-interface {p0}, Lio/reactivex/FlowableEmitter;->onComplete()V

    return-void
.end method

.method static synthetic lambda$null$5(Lio/reactivex/FlowableEmitter;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 0

    .line 92
    invoke-interface {p0, p1}, Lio/reactivex/FlowableEmitter;->onNext(Ljava/lang/Object;)V

    .line 93
    invoke-interface {p0}, Lio/reactivex/FlowableEmitter;->onComplete()V

    return-void
.end method

.method static synthetic lambda$null$7(Lio/reactivex/ObservableEmitter;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 0

    .line 104
    invoke-interface {p0, p1}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    .line 105
    invoke-interface {p0}, Lio/reactivex/ObservableEmitter;->onComplete()V

    return-void
.end method

.method static synthetic lambda$null$9(Lio/reactivex/ObservableEmitter;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 0

    .line 117
    invoke-interface {p0, p1}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    .line 118
    invoke-interface {p0}, Lio/reactivex/ObservableEmitter;->onComplete()V

    return-void
.end method

.method static synthetic lambda$shouldForegroundSyncAsSingle$12(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/utils/ElapsedTime;Lio/reactivex/ObservableEmitter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 128
    new-instance v0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$VIGrLySOoeJbwAsdFSnLXK96qjc;

    invoke-direct {v0, p2}, Lcom/squareup/cogs/-$$Lambda$CogsTasks$VIGrLySOoeJbwAsdFSnLXK96qjc;-><init>(Lio/reactivex/ObservableEmitter;)V

    invoke-interface {p0, p1, v0}, Lcom/squareup/cogs/Cogs;->shouldForegroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method static synthetic lambda$syncAsSingle$8(Lcom/squareup/cogs/Cogs;Lio/reactivex/ObservableEmitter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 103
    new-instance v0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$dYI6HdfeMNPxtZALnKxu3ZsBh2c;

    invoke-direct {v0, p1}, Lcom/squareup/cogs/-$$Lambda$CogsTasks$dYI6HdfeMNPxtZALnKxu3ZsBh2c;-><init>(Lio/reactivex/ObservableEmitter;)V

    const/4 p1, 0x0

    invoke-interface {p0, v0, p1}, Lcom/squareup/cogs/Cogs;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    return-void
.end method

.method public static shouldForegroundSyncAsSingle(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/utils/ElapsedTime;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .line 127
    new-instance v0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$7roqzL9AR-DFAD83nF4Bq3aCkUc;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cogs/-$$Lambda$CogsTasks$7roqzL9AR-DFAD83nF4Bq3aCkUc;-><init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/utils/ElapsedTime;)V

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object p0

    .line 132
    invoke-virtual {p0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static syncAsSingle(Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation

    .line 102
    new-instance v0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$du_zN73uj3FsRk6Ibe299npSVOM;

    invoke-direct {v0, p0}, Lcom/squareup/cogs/-$$Lambda$CogsTasks$du_zN73uj3FsRk6Ibe299npSVOM;-><init>(Lcom/squareup/cogs/Cogs;)V

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object p0

    .line 107
    invoke-virtual {p0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static write()Lcom/squareup/cogs/WriteBuilder;
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/cogs/WriteBuilder;

    invoke-direct {v0}, Lcom/squareup/cogs/WriteBuilder;-><init>()V

    return-object v0
.end method
