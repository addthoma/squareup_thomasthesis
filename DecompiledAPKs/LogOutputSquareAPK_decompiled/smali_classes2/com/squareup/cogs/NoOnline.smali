.class public Lcom/squareup/cogs/NoOnline;
.super Ljava/lang/Object;
.source "NoOnline.java"

# interfaces
.implements Lcom/squareup/shared/catalog/Catalog$Online;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public batchRetrieveCatalogConnectV2Objects(Ljava/util/List;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;>;"
        }
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public batchUpsertCatalogConnectV2Objects(Ljava/util/List;Ljava/lang/String;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;",
            ">;"
        }
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public countVariationsWithAssignedUnit(Ljava/lang/String;I)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public deleteCatalogConnectV2Object(Ljava/lang/String;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public retrieveCatalogConnectV2Object(Ljava/lang/String;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;"
        }
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method
