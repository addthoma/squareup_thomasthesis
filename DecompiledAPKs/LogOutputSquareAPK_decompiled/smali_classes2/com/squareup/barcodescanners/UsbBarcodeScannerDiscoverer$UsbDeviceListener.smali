.class Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbDeviceListener;
.super Ljava/lang/Object;
.source "UsbBarcodeScannerDiscoverer.java"

# interfaces
.implements Lcom/squareup/usb/UsbDiscoverer$DeviceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UsbDeviceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;


# direct methods
.method private constructor <init>(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)V
    .locals 0

    .line 302
    iput-object p1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbDeviceListener;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$1;)V
    .locals 0

    .line 302
    invoke-direct {p0, p1}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbDeviceListener;-><init>(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)V

    return-void
.end method


# virtual methods
.method public deviceAvailable(Landroid/hardware/usb/UsbDevice;)V
    .locals 4

    .line 304
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbDeviceListener;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    invoke-static {v0}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->access$400(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 308
    new-instance v0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;

    iget-object v1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbDeviceListener;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    .line 309
    invoke-static {v1}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->access$500(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Lcom/squareup/hardware/usb/UsbManager;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbDeviceListener;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    invoke-static {v2}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->access$600(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbDeviceListener;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    invoke-static {v3}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->access$700(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Lcom/squareup/util/Res;

    move-result-object v3

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;-><init>(Lcom/squareup/hardware/usb/UsbManager;Landroid/hardware/usb/UsbDevice;Ljava/util/concurrent/Executor;Lcom/squareup/util/Res;)V

    .line 310
    new-instance v1, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScannerListener;

    iget-object v2, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbDeviceListener;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    invoke-direct {v1, v2, v0}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScannerListener;-><init>(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;Lcom/squareup/barcodescanners/BarcodeScanner;)V

    invoke-virtual {v0, v1}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->setListener(Lcom/squareup/barcodescanners/BarcodeScanner$Listener;)V

    .line 311
    invoke-virtual {v0}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->start()V

    .line 312
    iget-object v1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbDeviceListener;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    invoke-static {v1}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->access$400(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    .line 305
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Barcode scanner already available."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public deviceUnavailable(Landroid/hardware/usb/UsbDevice;)V
    .locals 1

    .line 316
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbDeviceListener;->this$0:Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    invoke-static {v0}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;->access$400(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;

    if-eqz p1, :cond_0

    .line 318
    invoke-virtual {p1}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->stop()V

    :cond_0
    return-void
.end method
