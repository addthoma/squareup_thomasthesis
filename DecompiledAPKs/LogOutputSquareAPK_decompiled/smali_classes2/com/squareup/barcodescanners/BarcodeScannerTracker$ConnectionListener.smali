.class public interface abstract Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;
.super Ljava/lang/Object;
.source "BarcodeScannerTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/barcodescanners/BarcodeScannerTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ConnectionListener"
.end annotation


# virtual methods
.method public abstract barcodeScannerConnected(Lcom/squareup/barcodescanners/BarcodeScanner;)V
.end method

.method public abstract barcodeScannerDisconnected(Lcom/squareup/barcodescanners/BarcodeScanner;)V
.end method
