.class public final Lcom/squareup/accessibility/pin/impl/TutorialPageResources;
.super Ljava/lang/Object;
.source "TutorialPageResources.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB#\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/accessibility/pin/impl/TutorialPageResources;",
        "",
        "titleId",
        "",
        "messageId",
        "screenImageDrawableId",
        "(III)V",
        "getMessageId",
        "()I",
        "getScreenImageDrawableId",
        "getTitleId",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;

.field private static final tutorialPages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/accessibility/pin/impl/TutorialPageResources;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final messageId:I

.field private final screenImageDrawableId:I

.field private final titleId:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->Companion:Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    .line 21
    new-instance v1, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    .line 22
    sget v2, Lcom/squareup/accessibility/pin/impl/R$string;->tutorial_p0_title:I

    .line 23
    sget v3, Lcom/squareup/accessibility/pin/impl/R$string;->tutorial_p0_message:I

    .line 24
    sget v4, Lcom/squareup/accessibility/pin/impl/R$drawable;->tutorial_screen_0:I

    .line 21
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;-><init>(III)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 27
    new-instance v1, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    .line 28
    sget v2, Lcom/squareup/accessibility/pin/impl/R$string;->tutorial_p1_title:I

    .line 29
    sget v3, Lcom/squareup/accessibility/pin/impl/R$string;->tutorial_p1_message:I

    .line 30
    sget v4, Lcom/squareup/accessibility/pin/impl/R$drawable;->tutorial_screen_1:I

    .line 27
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;-><init>(III)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 33
    new-instance v1, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    .line 34
    sget v2, Lcom/squareup/accessibility/pin/impl/R$string;->tutorial_p2_title:I

    .line 35
    sget v3, Lcom/squareup/accessibility/pin/impl/R$string;->tutorial_p2_message:I

    .line 36
    sget v4, Lcom/squareup/accessibility/pin/impl/R$drawable;->tutorial_screen_2:I

    .line 33
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;-><init>(III)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 39
    new-instance v1, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    .line 40
    sget v2, Lcom/squareup/accessibility/pin/impl/R$string;->tutorial_p3_title:I

    .line 41
    sget v3, Lcom/squareup/accessibility/pin/impl/R$string;->tutorial_p3_message:I

    .line 42
    sget v4, Lcom/squareup/accessibility/pin/impl/R$drawable;->tutorial_screen_3:I

    .line 39
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;-><init>(III)V

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 45
    new-instance v1, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    .line 46
    sget v2, Lcom/squareup/accessibility/pin/impl/R$string;->tutorial_p4_title:I

    .line 47
    sget v3, Lcom/squareup/accessibility/pin/impl/R$string;->tutorial_p4_message:I

    .line 48
    sget v4, Lcom/squareup/accessibility/pin/impl/R$drawable;->tutorial_screen_4:I

    .line 45
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;-><init>(III)V

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 51
    new-instance v1, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    .line 52
    sget v2, Lcom/squareup/accessibility/pin/impl/R$string;->tutorial_p5_title:I

    .line 53
    sget v3, Lcom/squareup/accessibility/pin/impl/R$string;->tutorial_p5_message:I

    .line 54
    sget v4, Lcom/squareup/accessibility/pin/impl/R$drawable;->tutorial_screen_5:I

    .line 51
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;-><init>(III)V

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 57
    new-instance v1, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    .line 58
    sget v2, Lcom/squareup/accessibility/pin/impl/R$string;->tutorial_p6_title:I

    .line 59
    sget v3, Lcom/squareup/accessibility/pin/impl/R$string;->tutorial_p6_message:I

    .line 60
    sget v4, Lcom/squareup/accessibility/pin/impl/R$drawable;->tutorial_screen_6:I

    .line 57
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;-><init>(III)V

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 64
    new-instance v1, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;

    .line 65
    sget v2, Lcom/squareup/accessibility/pin/impl/R$string;->empty_string:I

    .line 66
    sget v3, Lcom/squareup/accessibility/pin/impl/R$string;->empty_string:I

    .line 68
    sget v4, Lcom/squareup/accessibility/pin/impl/R$drawable;->tutorial_screen_6:I

    .line 64
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;-><init>(III)V

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 20
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->tutorialPages:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->titleId:I

    iput p2, p0, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->messageId:I

    iput p3, p0, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->screenImageDrawableId:I

    return-void
.end method

.method public static final synthetic access$getTutorialPages$cp()Ljava/util/List;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->tutorialPages:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final getMessageId()I
    .locals 1

    .line 16
    iget v0, p0, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->messageId:I

    return v0
.end method

.method public final getScreenImageDrawableId()I
    .locals 1

    .line 17
    iget v0, p0, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->screenImageDrawableId:I

    return v0
.end method

.method public final getTitleId()I
    .locals 1

    .line 15
    iget v0, p0, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->titleId:I

    return v0
.end method
