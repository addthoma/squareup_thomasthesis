.class public final Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;
.super Landroidx/viewpager/widget/ViewPager;
.source "AccessiblePinTutorialViewPager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager$Component;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAccessiblePinTutorialViewPager.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AccessiblePinTutorialViewPager.kt\ncom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,35:1\n52#2:36\n*E\n*S KotlinDebug\n*F\n+ 1 AccessiblePinTutorialViewPager.kt\ncom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager\n*L\n21#1:36\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\rB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;",
        "Landroidx/viewpager/widget/ViewPager;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "dispatchPopulateAccessibilityEvent",
        "",
        "event",
        "Landroid/view/accessibility/AccessibilityEvent;",
        "Component",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0, p1, p2}, Landroidx/viewpager/widget/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const-class p2, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager$Component;

    .line 22
    invoke-interface {p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager$Component;->buyerLocaleOverride()Lcom/squareup/buyer/language/BuyerLocaleOverride;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 29
    sget-object p1, Lcom/squareup/accessibility/pin/impl/TutorialPageResources;->Companion:Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;

    invoke-virtual {p0}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;->getCurrentItem()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/accessibility/pin/impl/TutorialPageResources$Companion;->textForAccessibility(ILcom/squareup/buyer/language/BuyerLocaleOverride;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager;->announceForAccessibility(Ljava/lang/CharSequence;)V

    return v0

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
