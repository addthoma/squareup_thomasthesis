.class public final Lcom/squareup/accessibility/AccessibilitySettings;
.super Ljava/lang/Object;
.source "AccessibilitySettings.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accessibility/AccessibilitySettings$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u0011\u001a\u00020\u0008J\u0006\u0010\u0012\u001a\u00020\u0008J\u000e\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0007\u001a\u00020\u0008J\u000e\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0007\u001a\u00020\u0008J\u0008\u0010\u0016\u001a\u00020\u0014H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\t\u001a\u00020\u00082\u0006\u0010\u0007\u001a\u00020\u00088B@BX\u0082\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR\u0017\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/accessibility/AccessibilitySettings;",
        "",
        "application",
        "Landroid/app/Application;",
        "accessibilityManager",
        "Landroid/view/accessibility/AccessibilityManager;",
        "(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;)V",
        "enabled",
        "",
        "isAccessibilityEnabled",
        "()Z",
        "setAccessibilityEnabled",
        "(Z)V",
        "talkBackState",
        "Lio/reactivex/Observable;",
        "getTalkBackState",
        "()Lio/reactivex/Observable;",
        "isMagnificationEnabled",
        "isTalkBackEnabled",
        "setMagnificationEnabled",
        "",
        "setTalkBackEnabled",
        "updateAccessibilityState",
        "Companion",
        "accessibility_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ACCESSIBILITY_DISPLAY_MAGNIFICATION_ENABLED:Ljava/lang/String; = "accessibility_display_magnification_enabled"

.field public static final Companion:Lcom/squareup/accessibility/AccessibilitySettings$Companion;

.field private static final GOOGLE_TALKBACK_SERVICE:Ljava/lang/String; = "com.android.talkback/com.google.android.marvin.talkback.TalkBackService"


# instance fields
.field private final accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private final application:Landroid/app/Application;

.field private final talkBackState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/accessibility/AccessibilitySettings$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/accessibility/AccessibilitySettings$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/accessibility/AccessibilitySettings;->Companion:Lcom/squareup/accessibility/AccessibilitySettings$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accessibilityManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/accessibility/AccessibilitySettings;->application:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/accessibility/AccessibilitySettings;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 21
    new-instance p1, Lcom/squareup/accessibility/AccessibilitySettings$talkBackState$1;

    invoke-direct {p1, p0}, Lcom/squareup/accessibility/AccessibilitySettings$talkBackState$1;-><init>(Lcom/squareup/accessibility/AccessibilitySettings;)V

    check-cast p1, Lio/reactivex/ObservableOnSubscribe;

    invoke-static {p1}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.create<Boolea\u2026eListener(listener) }\n  }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance p2, Lcom/squareup/accessibility/AccessibilitySettings$talkBackState$2;

    invoke-direct {p2, p0}, Lcom/squareup/accessibility/AccessibilitySettings$talkBackState$2;-><init>(Lcom/squareup/accessibility/AccessibilitySettings;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Kt;->startWithLazy(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function0;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/accessibility/AccessibilitySettings;->talkBackState:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getAccessibilityManager$p(Lcom/squareup/accessibility/AccessibilitySettings;)Landroid/view/accessibility/AccessibilityManager;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/accessibility/AccessibilitySettings;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    return-object p0
.end method

.method private final isAccessibilityEnabled()Z
    .locals 3

    .line 63
    iget-object v0, p0, Lcom/squareup/accessibility/AccessibilitySettings;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "accessibility_enabled"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private final setAccessibilityEnabled(Z)V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/accessibility/AccessibilitySettings;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accessibility_enabled"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private final updateAccessibilityState()V
    .locals 1

    .line 69
    invoke-virtual {p0}, Lcom/squareup/accessibility/AccessibilitySettings;->isTalkBackEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/accessibility/AccessibilitySettings;->isMagnificationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/squareup/accessibility/AccessibilitySettings;->setAccessibilityEnabled(Z)V

    return-void
.end method


# virtual methods
.method public final getTalkBackState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/accessibility/AccessibilitySettings;->talkBackState:Lio/reactivex/Observable;

    return-object v0
.end method

.method public final isMagnificationEnabled()Z
    .locals 3

    .line 46
    iget-object v0, p0, Lcom/squareup/accessibility/AccessibilitySettings;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "accessibility_display_magnification_enabled"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public final isTalkBackEnabled()Z
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/accessibility/AccessibilitySettings;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "enabled_accessibility_services"

    .line 30
    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.talkback/com.google.android.marvin.talkback.TalkBackService"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final setMagnificationEnabled(Z)V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/accessibility/AccessibilitySettings;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accessibility_display_magnification_enabled"

    .line 50
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 56
    invoke-direct {p0}, Lcom/squareup/accessibility/AccessibilitySettings;->updateAccessibilityState()V

    return-void
.end method

.method public final setTalkBackEnabled(Z)V
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/accessibility/AccessibilitySettings;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz p1, :cond_0

    const-string p1, "com.android.talkback/com.google.android.marvin.talkback.TalkBackService"

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const-string v1, "enabled_accessibility_services"

    .line 36
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 42
    invoke-direct {p0}, Lcom/squareup/accessibility/AccessibilitySettings;->updateAccessibilityState()V

    return-void
.end method
