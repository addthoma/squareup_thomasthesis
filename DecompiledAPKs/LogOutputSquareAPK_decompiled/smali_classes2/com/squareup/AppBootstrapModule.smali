.class public Lcom/squareup/AppBootstrapModule;
.super Ljava/lang/Object;
.source "AppBootstrapModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# instance fields
.field private final activityListener:Lcom/squareup/ActivityListener;

.field private final appDelegate:Lcom/squareup/AppDelegate;

.field private final application:Landroid/app/Application;

.field private final crashReporter:Lcom/squareup/log/CrashReporter;

.field private final emailSupportLedgerEnabled:Z

.field private final startUptime:J


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/AppDelegate;JLcom/squareup/log/CrashReporter;Lcom/squareup/ActivityListener;Z)V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/AppBootstrapModule;->application:Landroid/app/Application;

    .line 32
    iput-object p2, p0, Lcom/squareup/AppBootstrapModule;->appDelegate:Lcom/squareup/AppDelegate;

    .line 33
    iput-wide p3, p0, Lcom/squareup/AppBootstrapModule;->startUptime:J

    .line 34
    iput-object p5, p0, Lcom/squareup/AppBootstrapModule;->crashReporter:Lcom/squareup/log/CrashReporter;

    .line 35
    iput-object p6, p0, Lcom/squareup/AppBootstrapModule;->activityListener:Lcom/squareup/ActivityListener;

    .line 36
    iput-boolean p7, p0, Lcom/squareup/AppBootstrapModule;->emailSupportLedgerEnabled:Z

    return-void
.end method


# virtual methods
.method provideActivityListener()Lcom/squareup/ActivityListener;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/AppBootstrapModule;->activityListener:Lcom/squareup/ActivityListener;

    return-object v0
.end method

.method provideAppDelegate()Lcom/squareup/AppDelegate;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/AppBootstrapModule;->appDelegate:Lcom/squareup/AppDelegate;

    return-object v0
.end method

.method provideApplication()Landroid/app/Application;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/AppBootstrapModule;->application:Landroid/app/Application;

    return-object v0
.end method

.method provideCrashReporter()Lcom/squareup/log/CrashReporter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/AppBootstrapModule;->crashReporter:Lcom/squareup/log/CrashReporter;

    return-object v0
.end method

.method provideEmailSupportLedgerEnabled()Z
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 60
    iget-boolean v0, p0, Lcom/squareup/AppBootstrapModule;->emailSupportLedgerEnabled:Z

    return v0
.end method

.method provideStartUptime()J
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 48
    iget-wide v0, p0, Lcom/squareup/AppBootstrapModule;->startUptime:J

    return-wide v0
.end method
