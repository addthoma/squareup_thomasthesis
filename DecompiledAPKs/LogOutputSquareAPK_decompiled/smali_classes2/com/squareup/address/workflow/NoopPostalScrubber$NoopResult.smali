.class final Lcom/squareup/address/workflow/NoopPostalScrubber$NoopResult;
.super Lcom/squareup/text/PostalScrubber$Result;
.source "NoopPostalScrubber.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/address/workflow/NoopPostalScrubber;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "NoopResult"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNoopPostalScrubber.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NoopPostalScrubber.kt\ncom/squareup/address/workflow/NoopPostalScrubber$NoopResult\n*L\n1#1,11:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/address/workflow/NoopPostalScrubber$NoopResult;",
        "Lcom/squareup/text/PostalScrubber$Result;",
        "proposed",
        "",
        "(Ljava/lang/String;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    const/4 v0, 0x1

    .line 9
    invoke-direct {p0, v0, p1}, Lcom/squareup/text/PostalScrubber$Result;-><init>(ZLjava/lang/String;)V

    return-void
.end method
