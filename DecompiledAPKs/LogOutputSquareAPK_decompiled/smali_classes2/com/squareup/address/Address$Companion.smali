.class public final Lcom/squareup/address/Address$Companion;
.super Ljava/lang/Object;
.source "Address.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/address/Address;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAddress.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Address.kt\ncom/squareup/address/Address$Companion\n*L\n1#1,139:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0007J\u0010\u0010\u0008\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH\u0007J\u0010\u0010\u000b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u000cH\u0007R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/address/Address$Companion;",
        "",
        "()V",
        "EMPTY",
        "Lcom/squareup/address/Address;",
        "from",
        "entity",
        "Lcom/squareup/server/account/MerchantProfileResponse$Entity;",
        "fromConnectV2Address",
        "address",
        "Lcom/squareup/protos/connect/v2/resources/Address;",
        "fromGlobalAddress",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "address_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 45
    invoke-direct {p0}, Lcom/squareup/address/Address$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final from(Lcom/squareup/server/account/MerchantProfileResponse$Entity;)Lcom/squareup/address/Address;
    .locals 8
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "entity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    new-instance v0, Lcom/squareup/address/Address;

    .line 79
    iget-object v2, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->street1:Ljava/lang/String;

    .line 80
    iget-object v3, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->street2:Ljava/lang/String;

    .line 81
    iget-object v4, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->city:Ljava/lang/String;

    .line 82
    iget-object v5, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->state:Ljava/lang/String;

    .line 83
    iget-object v6, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->postal_code:Ljava/lang/String;

    .line 84
    iget-object v1, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->country_code:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 p1, 0x0

    :goto_0
    move-object v7, p1

    goto :goto_1

    .line 85
    :cond_0
    iget-object p1, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->country_code:Ljava/lang/String;

    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-static {p1}, Lcom/squareup/CountryCode;->parseCountryCode(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object p1

    goto :goto_0

    :goto_1
    move-object v1, v0

    .line 78
    invoke-direct/range {v1 .. v7}, Lcom/squareup/address/Address;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V

    return-object v0
.end method

.method public final fromConnectV2Address(Lcom/squareup/protos/connect/v2/resources/Address;)Lcom/squareup/address/Address;
    .locals 8
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "address"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_1:Ljava/lang/String;

    .line 69
    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Address;->address_line_2:Ljava/lang/String;

    .line 70
    iget-object v4, p1, Lcom/squareup/protos/connect/v2/resources/Address;->locality:Ljava/lang/String;

    .line 71
    iget-object v5, p1, Lcom/squareup/protos/connect/v2/resources/Address;->administrative_district_level_1:Ljava/lang/String;

    .line 72
    iget-object v6, p1, Lcom/squareup/protos/connect/v2/resources/Address;->postal_code:Ljava/lang/String;

    .line 73
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/resources/Address;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Country;->name()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/squareup/CountryCode;->valueOf(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move-object v7, p1

    .line 67
    new-instance p1, Lcom/squareup/address/Address;

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/address/Address;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V

    return-object p1
.end method

.method public final fromGlobalAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;
    .locals 8
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "address"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    new-instance v0, Lcom/squareup/address/Address;

    .line 57
    iget-object v2, p1, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    .line 58
    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    .line 59
    iget-object v4, p1, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    .line 60
    iget-object v5, p1, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    .line 61
    iget-object v6, p1, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    .line 62
    iget-object p1, p1, Lcom/squareup/protos/common/location/GlobalAddress;->country_code:Lcom/squareup/protos/common/countries/Country;

    invoke-virtual {p1}, Lcom/squareup/protos/common/countries/Country;->name()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/CountryCode;->valueOf(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object v7

    move-object v1, v0

    .line 56
    invoke-direct/range {v1 .. v7}, Lcom/squareup/address/Address;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V

    return-object v0
.end method
