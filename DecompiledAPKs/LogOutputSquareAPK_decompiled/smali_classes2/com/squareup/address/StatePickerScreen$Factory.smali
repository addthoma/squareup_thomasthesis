.class public Lcom/squareup/address/StatePickerScreen$Factory;
.super Ljava/lang/Object;
.source "StatePickerScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/address/StatePickerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 39
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/address/StatePickerScreen;

    .line 40
    new-instance v1, Lcom/squareup/address/StatePickerScreen$DialogBuilder;

    invoke-static {v0}, Lcom/squareup/address/StatePickerScreen;->access$000(Lcom/squareup/address/StatePickerScreen;)Lcom/squareup/CountryCode;

    move-result-object v0

    invoke-direct {v1, p1, v0}, Lcom/squareup/address/StatePickerScreen$DialogBuilder;-><init>(Landroid/content/Context;Lcom/squareup/CountryCode;)V

    invoke-virtual {v1}, Lcom/squareup/address/StatePickerScreen$DialogBuilder;->build()Landroid/app/Dialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
