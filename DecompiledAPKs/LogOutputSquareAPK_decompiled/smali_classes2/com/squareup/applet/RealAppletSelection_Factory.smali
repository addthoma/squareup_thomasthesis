.class public final Lcom/squareup/applet/RealAppletSelection_Factory;
.super Ljava/lang/Object;
.source "RealAppletSelection_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/applet/RealAppletSelection;",
        ">;"
    }
.end annotation


# instance fields
.field private final appletsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/applet/RealAppletSelection_Factory;->appletsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/applet/RealAppletSelection_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;)",
            "Lcom/squareup/applet/RealAppletSelection_Factory;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/squareup/applet/RealAppletSelection_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/applet/RealAppletSelection_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/applet/Applets;)Lcom/squareup/applet/RealAppletSelection;
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/applet/RealAppletSelection;

    invoke-direct {v0, p0}, Lcom/squareup/applet/RealAppletSelection;-><init>(Lcom/squareup/applet/Applets;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/applet/RealAppletSelection;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/applet/RealAppletSelection_Factory;->appletsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/Applets;

    invoke-static {v0}, Lcom/squareup/applet/RealAppletSelection_Factory;->newInstance(Lcom/squareup/applet/Applets;)Lcom/squareup/applet/RealAppletSelection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/applet/RealAppletSelection_Factory;->get()Lcom/squareup/applet/RealAppletSelection;

    move-result-object v0

    return-object v0
.end method
