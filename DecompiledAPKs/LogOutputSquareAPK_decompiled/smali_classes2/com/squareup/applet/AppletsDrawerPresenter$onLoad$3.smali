.class final Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$3;
.super Lkotlin/jvm/internal/Lambda;
.source "AppletsDrawerPresenter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/applet/AppletsDrawerPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $applet:Lcom/squareup/applet/Applet;

.field final synthetic $item:Lcom/squareup/applet/AppletsDrawerItem;


# direct methods
.method constructor <init>(Lcom/squareup/applet/Applet;Lcom/squareup/applet/AppletsDrawerItem;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$3;->$applet:Lcom/squareup/applet/Applet;

    iput-object p2, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$3;->$item:Lcom/squareup/applet/AppletsDrawerItem;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$3;->$applet:Lcom/squareup/applet/Applet;

    invoke-virtual {v0}, Lcom/squareup/applet/Applet;->visibility()Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x0

    .line 126
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 127
    new-instance v1, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$3$1;

    invoke-direct {v1, p0}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$3$1;-><init>(Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$3;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "applet.visibility()\n    \u2026ne(visible)\n            }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 44
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerPresenter$onLoad$3;->invoke()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
