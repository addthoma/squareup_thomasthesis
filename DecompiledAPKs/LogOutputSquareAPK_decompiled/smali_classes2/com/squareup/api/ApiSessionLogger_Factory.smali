.class public final Lcom/squareup/api/ApiSessionLogger_Factory;
.super Ljava/lang/Object;
.source "ApiSessionLogger_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/api/ApiSessionLogger;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventStreamProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v1/EventStream;",
            ">;"
        }
    .end annotation
.end field

.field private final eventstreamV2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v2/EventstreamV2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v1/EventStream;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v2/EventstreamV2;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/api/ApiSessionLogger_Factory;->eventStreamProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/api/ApiSessionLogger_Factory;->eventstreamV2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/api/ApiSessionLogger_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v1/EventStream;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v2/EventstreamV2;",
            ">;)",
            "Lcom/squareup/api/ApiSessionLogger_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/api/ApiSessionLogger_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/api/ApiSessionLogger_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/eventstream/v1/EventStream;Lcom/squareup/eventstream/v2/EventstreamV2;)Lcom/squareup/api/ApiSessionLogger;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/api/ApiSessionLogger;

    invoke-direct {v0, p0, p1}, Lcom/squareup/api/ApiSessionLogger;-><init>(Lcom/squareup/eventstream/v1/EventStream;Lcom/squareup/eventstream/v2/EventstreamV2;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/api/ApiSessionLogger;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/api/ApiSessionLogger_Factory;->eventStreamProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v1/EventStream;

    iget-object v1, p0, Lcom/squareup/api/ApiSessionLogger_Factory;->eventstreamV2Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v2/EventstreamV2;

    invoke-static {v0, v1}, Lcom/squareup/api/ApiSessionLogger_Factory;->newInstance(Lcom/squareup/eventstream/v1/EventStream;Lcom/squareup/eventstream/v2/EventstreamV2;)Lcom/squareup/api/ApiSessionLogger;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/api/ApiSessionLogger_Factory;->get()Lcom/squareup/api/ApiSessionLogger;

    move-result-object v0

    return-object v0
.end method
