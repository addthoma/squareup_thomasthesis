.class final enum Lcom/squareup/api/ApiVersion$8;
.super Lcom/squareup/api/ApiVersion;
.source "ApiVersion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/ApiVersion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 113
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/api/ApiVersion;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/api/ApiVersion$1;)V

    return-void
.end method


# virtual methods
.method upgrade(Landroid/content/Intent;)V
    .locals 7

    const-string v0, "com.android.browser.application_id"

    .line 115
    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-wide/16 v1, 0x0

    const-string v3, "com.squareup.pos.AUTO_RETURN_TIMEOUT_MS"

    .line 118
    invoke-virtual {p1, v3, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    if-eqz v0, :cond_0

    const-string v5, "auto_return"

    goto :goto_0

    :cond_0
    const-string v5, "com.squareup.pos.AUTO_RETURN"

    :goto_0
    cmp-long v6, v3, v1

    if-eqz v6, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 121
    :goto_1
    invoke-virtual {p1, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 123
    sget-object v1, Lcom/squareup/api/ApiVersion$8;->V2_0:Lcom/squareup/api/ApiVersion;

    sget-object v2, Lcom/squareup/api/ApiVersion$8;->V3_0:Lcom/squareup/api/ApiVersion;

    invoke-static {p1, v1, v2, v0}, Lcom/squareup/api/ApiVersion;->access$100(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;Lcom/squareup/api/ApiVersion;Z)Landroid/content/Intent;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    return-void
.end method
