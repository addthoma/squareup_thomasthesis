.class public Lcom/squareup/api/ApiTransactionParams;
.super Ljava/lang/Object;
.source "ApiTransactionParams.java"


# instance fields
.field public final allowSplitTender:Z

.field public final apiVersion:Lcom/squareup/api/ApiVersion;

.field public final delayCapture:Z

.field public final tenderTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/api/ApiTenderType;",
            ">;"
        }
    .end annotation
.end field

.field public final timeout:J


# direct methods
.method public constructor <init>(Ljava/util/Set;Lcom/squareup/api/ApiVersion;JZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/api/ApiTenderType;",
            ">;",
            "Lcom/squareup/api/ApiVersion;",
            "JZZ)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/api/ApiTransactionParams;->tenderTypes:Ljava/util/Set;

    .line 20
    iput-object p2, p0, Lcom/squareup/api/ApiTransactionParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    .line 21
    iput-wide p3, p0, Lcom/squareup/api/ApiTransactionParams;->timeout:J

    .line 22
    iput-boolean p5, p0, Lcom/squareup/api/ApiTransactionParams;->allowSplitTender:Z

    .line 23
    iput-boolean p6, p0, Lcom/squareup/api/ApiTransactionParams;->delayCapture:Z

    return-void
.end method
