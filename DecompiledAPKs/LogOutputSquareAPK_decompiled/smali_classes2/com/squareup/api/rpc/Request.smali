.class public final Lcom/squareup/api/rpc/Request;
.super Lcom/squareup/wire/Message;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/rpc/Request$ProtoAdapter_Request;,
        Lcom/squareup/api/rpc/Request$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/rpc/Request;",
        "Lcom/squareup/api/rpc/Request$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/rpc/Request;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_API_VERSION:Lcom/squareup/api/sync/ApiVersion;

.field public static final DEFAULT_ID:Ljava/lang/Long;

.field public static final DEFAULT_METHOD_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_REQUEST_ENQUEUED_AT_MILLIS:Ljava/lang/Long;

.field public static final DEFAULT_SERVICE_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_SKIP_WRITABLE_SESSION_STATE_VALIDATION:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final api_version:Lcom/squareup/api/sync/ApiVersion;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ApiVersion#ADAPTER"
        tag = 0x3e9
    .end annotation
.end field

.field public final create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.CreateSessionRequest#ADAPTER"
        tag = 0x41a
    .end annotation
.end field

.field public final get_request:Lcom/squareup/api/sync/GetRequest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.GetRequest#ADAPTER"
        tag = 0x41b
    .end annotation
.end field

.field public final id:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.inventory.InventoryAdjustment#ADAPTER"
        tag = 0x578
    .end annotation
.end field

.field public final method_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final put_request:Lcom/squareup/api/sync/PutRequest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.PutRequest#ADAPTER"
        tag = 0x41c
    .end annotation
.end field

.field public final request_enqueued_at_millis:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x641
    .end annotation
.end field

.field public final scope:Lcom/squareup/api/sync/RequestScope;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.RequestScope#ADAPTER"
        tag = 0x3e8
    .end annotation
.end field

.field public final service_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final skip_writable_session_state_validation:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5dc
    .end annotation
.end field

.field public final writable_session_state:Lcom/squareup/api/sync/WritableSessionState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.WritableSessionState#ADAPTER"
        tag = 0x3ea
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 29
    new-instance v0, Lcom/squareup/api/rpc/Request$ProtoAdapter_Request;

    invoke-direct {v0}, Lcom/squareup/api/rpc/Request$ProtoAdapter_Request;-><init>()V

    sput-object v0, Lcom/squareup/api/rpc/Request;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 33
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/rpc/Request;->DEFAULT_ID:Ljava/lang/Long;

    .line 39
    sget-object v1, Lcom/squareup/api/sync/ApiVersion;->VERSION_0:Lcom/squareup/api/sync/ApiVersion;

    sput-object v1, Lcom/squareup/api/rpc/Request;->DEFAULT_API_VERSION:Lcom/squareup/api/sync/ApiVersion;

    const/4 v1, 0x0

    .line 41
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/api/rpc/Request;->DEFAULT_SKIP_WRITABLE_SESSION_STATE_VALIDATION:Ljava/lang/Boolean;

    .line 43
    sput-object v0, Lcom/squareup/api/rpc/Request;->DEFAULT_REQUEST_ENQUEUED_AT_MILLIS:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/sync/RequestScope;Lcom/squareup/api/sync/ApiVersion;Lcom/squareup/api/sync/WritableSessionState;Lcom/squareup/api/sync/CreateSessionRequest;Lcom/squareup/api/sync/GetRequest;Lcom/squareup/api/sync/PutRequest;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/squareup/protos/inventory/InventoryAdjustment;)V
    .locals 14

    .line 186
    sget-object v13, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/api/rpc/Request;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/sync/RequestScope;Lcom/squareup/api/sync/ApiVersion;Lcom/squareup/api/sync/WritableSessionState;Lcom/squareup/api/sync/CreateSessionRequest;Lcom/squareup/api/sync/GetRequest;Lcom/squareup/api/sync/PutRequest;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/squareup/protos/inventory/InventoryAdjustment;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/sync/RequestScope;Lcom/squareup/api/sync/ApiVersion;Lcom/squareup/api/sync/WritableSessionState;Lcom/squareup/api/sync/CreateSessionRequest;Lcom/squareup/api/sync/GetRequest;Lcom/squareup/api/sync/PutRequest;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/squareup/protos/inventory/InventoryAdjustment;Lokio/ByteString;)V
    .locals 1

    .line 194
    sget-object v0, Lcom/squareup/api/rpc/Request;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p13}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 195
    iput-object p1, p0, Lcom/squareup/api/rpc/Request;->id:Ljava/lang/Long;

    .line 196
    iput-object p2, p0, Lcom/squareup/api/rpc/Request;->service_name:Ljava/lang/String;

    .line 197
    iput-object p3, p0, Lcom/squareup/api/rpc/Request;->method_name:Ljava/lang/String;

    .line 198
    iput-object p4, p0, Lcom/squareup/api/rpc/Request;->scope:Lcom/squareup/api/sync/RequestScope;

    .line 199
    iput-object p5, p0, Lcom/squareup/api/rpc/Request;->api_version:Lcom/squareup/api/sync/ApiVersion;

    .line 200
    iput-object p6, p0, Lcom/squareup/api/rpc/Request;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    .line 201
    iput-object p7, p0, Lcom/squareup/api/rpc/Request;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    .line 202
    iput-object p8, p0, Lcom/squareup/api/rpc/Request;->get_request:Lcom/squareup/api/sync/GetRequest;

    .line 203
    iput-object p9, p0, Lcom/squareup/api/rpc/Request;->put_request:Lcom/squareup/api/sync/PutRequest;

    .line 204
    iput-object p10, p0, Lcom/squareup/api/rpc/Request;->skip_writable_session_state_validation:Ljava/lang/Boolean;

    .line 205
    iput-object p11, p0, Lcom/squareup/api/rpc/Request;->request_enqueued_at_millis:Ljava/lang/Long;

    .line 206
    iput-object p12, p0, Lcom/squareup/api/rpc/Request;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 231
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/rpc/Request;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 232
    :cond_1
    check-cast p1, Lcom/squareup/api/rpc/Request;

    .line 233
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Request;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/rpc/Request;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->id:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/api/rpc/Request;->id:Ljava/lang/Long;

    .line 234
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->service_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/rpc/Request;->service_name:Ljava/lang/String;

    .line 235
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->method_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/rpc/Request;->method_name:Ljava/lang/String;

    .line 236
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->scope:Lcom/squareup/api/sync/RequestScope;

    iget-object v3, p1, Lcom/squareup/api/rpc/Request;->scope:Lcom/squareup/api/sync/RequestScope;

    .line 237
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->api_version:Lcom/squareup/api/sync/ApiVersion;

    iget-object v3, p1, Lcom/squareup/api/rpc/Request;->api_version:Lcom/squareup/api/sync/ApiVersion;

    .line 238
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    iget-object v3, p1, Lcom/squareup/api/rpc/Request;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    .line 239
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    iget-object v3, p1, Lcom/squareup/api/rpc/Request;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    .line 240
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->get_request:Lcom/squareup/api/sync/GetRequest;

    iget-object v3, p1, Lcom/squareup/api/rpc/Request;->get_request:Lcom/squareup/api/sync/GetRequest;

    .line 241
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->put_request:Lcom/squareup/api/sync/PutRequest;

    iget-object v3, p1, Lcom/squareup/api/rpc/Request;->put_request:Lcom/squareup/api/sync/PutRequest;

    .line 242
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->skip_writable_session_state_validation:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/rpc/Request;->skip_writable_session_state_validation:Ljava/lang/Boolean;

    .line 243
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->request_enqueued_at_millis:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/api/rpc/Request;->request_enqueued_at_millis:Ljava/lang/Long;

    .line 244
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    iget-object p1, p1, Lcom/squareup/api/rpc/Request;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    .line 245
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 250
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_c

    .line 252
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Request;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 253
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->id:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 254
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->service_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 255
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->method_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 256
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->scope:Lcom/squareup/api/sync/RequestScope;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/api/sync/RequestScope;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 257
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->api_version:Lcom/squareup/api/sync/ApiVersion;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/api/sync/ApiVersion;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 258
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/api/sync/WritableSessionState;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 259
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/api/sync/CreateSessionRequest;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 260
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->get_request:Lcom/squareup/api/sync/GetRequest;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/api/sync/GetRequest;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 261
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->put_request:Lcom/squareup/api/sync/PutRequest;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/api/sync/PutRequest;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 262
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->skip_writable_session_state_validation:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 263
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->request_enqueued_at_millis:Ljava/lang/Long;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 264
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/inventory/InventoryAdjustment;->hashCode()I

    move-result v2

    :cond_b
    add-int/2addr v0, v2

    .line 265
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_c
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/rpc/Request$Builder;
    .locals 2

    .line 211
    new-instance v0, Lcom/squareup/api/rpc/Request$Builder;

    invoke-direct {v0}, Lcom/squareup/api/rpc/Request$Builder;-><init>()V

    .line 212
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->id:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/rpc/Request$Builder;->id:Ljava/lang/Long;

    .line 213
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->service_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/rpc/Request$Builder;->service_name:Ljava/lang/String;

    .line 214
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->method_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/rpc/Request$Builder;->method_name:Ljava/lang/String;

    .line 215
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->scope:Lcom/squareup/api/sync/RequestScope;

    iput-object v1, v0, Lcom/squareup/api/rpc/Request$Builder;->scope:Lcom/squareup/api/sync/RequestScope;

    .line 216
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->api_version:Lcom/squareup/api/sync/ApiVersion;

    iput-object v1, v0, Lcom/squareup/api/rpc/Request$Builder;->api_version:Lcom/squareup/api/sync/ApiVersion;

    .line 217
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    iput-object v1, v0, Lcom/squareup/api/rpc/Request$Builder;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    .line 218
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    iput-object v1, v0, Lcom/squareup/api/rpc/Request$Builder;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    .line 219
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->get_request:Lcom/squareup/api/sync/GetRequest;

    iput-object v1, v0, Lcom/squareup/api/rpc/Request$Builder;->get_request:Lcom/squareup/api/sync/GetRequest;

    .line 220
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->put_request:Lcom/squareup/api/sync/PutRequest;

    iput-object v1, v0, Lcom/squareup/api/rpc/Request$Builder;->put_request:Lcom/squareup/api/sync/PutRequest;

    .line 221
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->skip_writable_session_state_validation:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/rpc/Request$Builder;->skip_writable_session_state_validation:Ljava/lang/Boolean;

    .line 222
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->request_enqueued_at_millis:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/rpc/Request$Builder;->request_enqueued_at_millis:Ljava/lang/Long;

    .line 223
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    iput-object v1, v0, Lcom/squareup/api/rpc/Request$Builder;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    .line 224
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Request;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/rpc/Request$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Request;->newBuilder()Lcom/squareup/api/rpc/Request$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 273
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->id:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->id:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 274
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->service_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", service_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->service_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->method_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", method_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->method_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->scope:Lcom/squareup/api/sync/RequestScope;

    if-eqz v1, :cond_3

    const-string v1, ", scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->scope:Lcom/squareup/api/sync/RequestScope;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 277
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->api_version:Lcom/squareup/api/sync/ApiVersion;

    if-eqz v1, :cond_4

    const-string v1, ", api_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->api_version:Lcom/squareup/api/sync/ApiVersion;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 278
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    if-eqz v1, :cond_5

    const-string v1, ", writable_session_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 279
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    if-eqz v1, :cond_6

    const-string v1, ", create_session_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 280
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->get_request:Lcom/squareup/api/sync/GetRequest;

    if-eqz v1, :cond_7

    const-string v1, ", get_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->get_request:Lcom/squareup/api/sync/GetRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 281
    :cond_7
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->put_request:Lcom/squareup/api/sync/PutRequest;

    if-eqz v1, :cond_8

    const-string v1, ", put_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->put_request:Lcom/squareup/api/sync/PutRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 282
    :cond_8
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->skip_writable_session_state_validation:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", skip_writable_session_state_validation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->skip_writable_session_state_validation:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 283
    :cond_9
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->request_enqueued_at_millis:Ljava/lang/Long;

    if-eqz v1, :cond_a

    const-string v1, ", request_enqueued_at_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->request_enqueued_at_millis:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 284
    :cond_a
    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    if-eqz v1, :cond_b

    const-string v1, ", inventory_adjust_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Request;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_b
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Request{"

    .line 285
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
