.class final Lcom/squareup/api/rpc/Response$ProtoAdapter_Response;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Response.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/rpc/Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Response"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/rpc/Response;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 220
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/rpc/Response;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/rpc/Response;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 247
    new-instance v0, Lcom/squareup/api/rpc/Response$Builder;

    invoke-direct {v0}, Lcom/squareup/api/rpc/Response$Builder;-><init>()V

    .line 248
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 249
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/16 v4, 0x578

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 258
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 255
    :pswitch_0
    sget-object v3, Lcom/squareup/api/sync/PutResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/PutResponse;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Response$Builder;->put_response(Lcom/squareup/api/sync/PutResponse;)Lcom/squareup/api/rpc/Response$Builder;

    goto :goto_0

    .line 254
    :pswitch_1
    sget-object v3, Lcom/squareup/api/sync/GetResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/GetResponse;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Response$Builder;->get_response(Lcom/squareup/api/sync/GetResponse;)Lcom/squareup/api/rpc/Response$Builder;

    goto :goto_0

    .line 253
    :pswitch_2
    sget-object v3, Lcom/squareup/api/sync/CreateSessionResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/CreateSessionResponse;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Response$Builder;->create_session_response(Lcom/squareup/api/sync/CreateSessionResponse;)Lcom/squareup/api/rpc/Response$Builder;

    goto :goto_0

    .line 256
    :cond_0
    sget-object v3, Lcom/squareup/api/items/InventoryAdjustResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/InventoryAdjustResponse;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Response$Builder;->inventory_adjust_response(Lcom/squareup/api/items/InventoryAdjustResponse;)Lcom/squareup/api/rpc/Response$Builder;

    goto :goto_0

    .line 252
    :cond_1
    sget-object v3, Lcom/squareup/api/rpc/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/rpc/Error;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Response$Builder;->error(Lcom/squareup/api/rpc/Error;)Lcom/squareup/api/rpc/Response$Builder;

    goto :goto_0

    .line 251
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Response$Builder;->id(Ljava/lang/Long;)Lcom/squareup/api/rpc/Response$Builder;

    goto :goto_0

    .line 262
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/rpc/Response$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 263
    invoke-virtual {v0}, Lcom/squareup/api/rpc/Response$Builder;->build()Lcom/squareup/api/rpc/Response;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x41a
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 218
    invoke-virtual {p0, p1}, Lcom/squareup/api/rpc/Response$ProtoAdapter_Response;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/rpc/Response;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/rpc/Response;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 236
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Response;->id:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 237
    sget-object v0, Lcom/squareup/api/rpc/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 238
    sget-object v0, Lcom/squareup/api/sync/CreateSessionResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Response;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    const/16 v2, 0x41a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 239
    sget-object v0, Lcom/squareup/api/sync/GetResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Response;->get_response:Lcom/squareup/api/sync/GetResponse;

    const/16 v2, 0x41b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 240
    sget-object v0, Lcom/squareup/api/sync/PutResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Response;->put_response:Lcom/squareup/api/sync/PutResponse;

    const/16 v2, 0x41c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 241
    sget-object v0, Lcom/squareup/api/items/InventoryAdjustResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Response;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    const/16 v2, 0x578

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 242
    invoke-virtual {p2}, Lcom/squareup/api/rpc/Response;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 218
    check-cast p2, Lcom/squareup/api/rpc/Response;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/rpc/Response$ProtoAdapter_Response;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/rpc/Response;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/rpc/Response;)I
    .locals 4

    .line 225
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Response;->id:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/rpc/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    const/4 v3, 0x3

    .line 226
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/sync/CreateSessionResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Response;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    const/16 v3, 0x41a

    .line 227
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/sync/GetResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Response;->get_response:Lcom/squareup/api/sync/GetResponse;

    const/16 v3, 0x41b

    .line 228
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/sync/PutResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Response;->put_response:Lcom/squareup/api/sync/PutResponse;

    const/16 v3, 0x41c

    .line 229
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/InventoryAdjustResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Response;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    const/16 v3, 0x578

    .line 230
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 231
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Response;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 218
    check-cast p1, Lcom/squareup/api/rpc/Response;

    invoke-virtual {p0, p1}, Lcom/squareup/api/rpc/Response$ProtoAdapter_Response;->encodedSize(Lcom/squareup/api/rpc/Response;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/rpc/Response;)Lcom/squareup/api/rpc/Response;
    .locals 2

    .line 268
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Response;->newBuilder()Lcom/squareup/api/rpc/Response$Builder;

    move-result-object p1

    .line 269
    iget-object v0, p1, Lcom/squareup/api/rpc/Response$Builder;->error:Lcom/squareup/api/rpc/Error;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/rpc/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Response$Builder;->error:Lcom/squareup/api/rpc/Error;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/rpc/Error;

    iput-object v0, p1, Lcom/squareup/api/rpc/Response$Builder;->error:Lcom/squareup/api/rpc/Error;

    .line 270
    :cond_0
    iget-object v0, p1, Lcom/squareup/api/rpc/Response$Builder;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/api/sync/CreateSessionResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Response$Builder;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/CreateSessionResponse;

    iput-object v0, p1, Lcom/squareup/api/rpc/Response$Builder;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    .line 271
    :cond_1
    iget-object v0, p1, Lcom/squareup/api/rpc/Response$Builder;->get_response:Lcom/squareup/api/sync/GetResponse;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/api/sync/GetResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Response$Builder;->get_response:Lcom/squareup/api/sync/GetResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/GetResponse;

    iput-object v0, p1, Lcom/squareup/api/rpc/Response$Builder;->get_response:Lcom/squareup/api/sync/GetResponse;

    .line 272
    :cond_2
    iget-object v0, p1, Lcom/squareup/api/rpc/Response$Builder;->put_response:Lcom/squareup/api/sync/PutResponse;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/api/sync/PutResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Response$Builder;->put_response:Lcom/squareup/api/sync/PutResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/PutResponse;

    iput-object v0, p1, Lcom/squareup/api/rpc/Response$Builder;->put_response:Lcom/squareup/api/sync/PutResponse;

    .line 273
    :cond_3
    iget-object v0, p1, Lcom/squareup/api/rpc/Response$Builder;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/api/items/InventoryAdjustResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Response$Builder;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/InventoryAdjustResponse;

    iput-object v0, p1, Lcom/squareup/api/rpc/Response$Builder;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    .line 274
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Response$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 275
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Response$Builder;->build()Lcom/squareup/api/rpc/Response;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 218
    check-cast p1, Lcom/squareup/api/rpc/Response;

    invoke-virtual {p0, p1}, Lcom/squareup/api/rpc/Response$ProtoAdapter_Response;->redact(Lcom/squareup/api/rpc/Response;)Lcom/squareup/api/rpc/Response;

    move-result-object p1

    return-object p1
.end method
