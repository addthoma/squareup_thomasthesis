.class Lcom/squareup/api/ApiRequestController$1;
.super Lcom/squareup/mortar/BundlerAdapter;
.source "ApiRequestController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/api/ApiRequestController;->getBundler()Lmortar/bundler/Bundler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/api/ApiRequestController;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiRequestController;Ljava/lang/String;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-direct {p0, p2}, Lcom/squareup/mortar/BundlerAdapter;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-static {v0}, Lcom/squareup/api/ApiRequestController;->access$700(Lcom/squareup/api/ApiRequestController;)Lcom/squareup/pauses/PauseAndResumeRegistrar;

    move-result-object v0

    new-instance v1, Lcom/squareup/api/ApiRequestController$1$1;

    invoke-direct {v1, p0}, Lcom/squareup/api/ApiRequestController$1$1;-><init>(Lcom/squareup/api/ApiRequestController$1;)V

    invoke-interface {v0, p1, v1}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->register(Lmortar/MortarScope;Lcom/squareup/pauses/PausesAndResumes;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 80
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-static {v0}, Lcom/squareup/api/ApiRequestController;->access$000(Lcom/squareup/api/ApiRequestController;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/api/ApiRequestController;->access$002(Lcom/squareup/api/ApiRequestController;Z)Z

    if-eqz p1, :cond_2

    .line 85
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    const-string v1, "registerApiClientId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/api/ApiRequestController;->access$102(Lcom/squareup/api/ApiRequestController;Ljava/lang/String;)Ljava/lang/String;

    .line 87
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    const-string v1, "SEQUENCE_UUID"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/api/ApiRequestController;->access$202(Lcom/squareup/api/ApiRequestController;Ljava/lang/String;)Ljava/lang/String;

    .line 88
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-static {v0}, Lcom/squareup/api/ApiRequestController;->access$200(Lcom/squareup/api/ApiRequestController;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 89
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-static {v0}, Lcom/squareup/api/ApiRequestController;->access$300(Lcom/squareup/api/ApiRequestController;)Lcom/squareup/api/ApiSessionLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-static {v1}, Lcom/squareup/api/ApiRequestController;->access$200(Lcom/squareup/api/ApiRequestController;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/ApiSessionLogger;->setApiLogProperty(Ljava/lang/String;)V

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    const-string v1, "REQUEST_START_TIME_KEY"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/squareup/api/ApiRequestController;->access$402(Lcom/squareup/api/ApiRequestController;J)J

    :cond_2
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 3

    .line 96
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-static {v0}, Lcom/squareup/api/ApiRequestController;->access$100(Lcom/squareup/api/ApiRequestController;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerApiClientId"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-static {v0}, Lcom/squareup/api/ApiRequestController;->access$200(Lcom/squareup/api/ApiRequestController;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SEQUENCE_UUID"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-static {v0}, Lcom/squareup/api/ApiRequestController;->access$400(Lcom/squareup/api/ApiRequestController;)J

    move-result-wide v0

    const-string v2, "REQUEST_START_TIME_KEY"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void
.end method
