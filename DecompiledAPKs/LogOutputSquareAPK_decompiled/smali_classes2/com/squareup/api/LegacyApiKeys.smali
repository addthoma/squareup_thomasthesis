.class public final Lcom/squareup/api/LegacyApiKeys;
.super Ljava/lang/Object;
.source "LegacyApiKeys.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/LegacyApiKeys$V2;,
        Lcom/squareup/api/LegacyApiKeys$V1;
    }
.end annotation


# static fields
.field public static final AUTO_RETURN_NO_TIMEOUT:J = 0x0L

.field public static final AUTO_RETURN_TIMEOUT_MAX_MILLIS:J = 0x2710L

.field public static final AUTO_RETURN_TIMEOUT_MIN_MILLIS:J = 0xc80L

.field public static final NATIVE_REQUEST_KEYS_V2_TO_V3:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 110
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const-string v1, "com.squareup.pos.AUTO_RETURN_TIMEOUT_MS"

    const-string v2, "V3_AUTO_RETURN"

    .line 112
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.squareup.pos.REQUEST_METADATA"

    const-string v2, "com.squareup.pos.STATE"

    .line 114
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.squareup.pos.TENDER_CARD"

    const-string v2, "V3_TENDER_CARD"

    .line 115
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/LegacyApiKeys;->NATIVE_REQUEST_KEYS_V2_TO_V3:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method
