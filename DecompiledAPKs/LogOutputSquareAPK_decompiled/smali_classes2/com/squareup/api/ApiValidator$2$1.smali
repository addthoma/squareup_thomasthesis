.class Lcom/squareup/api/ApiValidator$2$1;
.super Ljava/lang/Object;
.source "ApiValidator.java"

# interfaces
.implements Lcom/squareup/receiving/ReceivedMapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/api/ApiValidator$2;->lambda$call$0(Lcom/squareup/api/RequestParams;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lkotlin/Pair;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/receiving/ReceivedMapper<",
        "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
        "Lcom/squareup/api/ApiErrorResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/api/ApiValidator$2;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiValidator$2;)V
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/api/ApiValidator$2$1;->this$1:Lcom/squareup/api/ApiValidator$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private fromStatus(I)Lcom/squareup/api/ApiErrorResult;
    .locals 1

    const/16 v0, 0x190

    if-eq p1, v0, :cond_0

    const/16 v0, 0x194

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1f4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1f7

    if-eq p1, v0, :cond_0

    .line 287
    sget-object p1, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_UNKNOWN_ERROR:Lcom/squareup/api/ApiErrorResult;

    return-object p1

    .line 285
    :cond_0
    sget-object p1, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_NO_SERVER:Lcom/squareup/api/ApiErrorResult;

    return-object p1
.end method


# virtual methods
.method public isAccepted(Lcom/squareup/protos/client/rolodex/GetContactResponse;)Lcom/squareup/api/ApiErrorResult;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic isAccepted(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 250
    check-cast p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/api/ApiValidator$2$1;->isAccepted(Lcom/squareup/protos/client/rolodex/GetContactResponse;)Lcom/squareup/api/ApiErrorResult;

    move-result-object p1

    return-object p1
.end method

.method public isClientError(Lcom/squareup/protos/client/rolodex/GetContactResponse;I)Lcom/squareup/api/ApiErrorResult;
    .locals 0

    .line 268
    invoke-direct {p0, p2}, Lcom/squareup/api/ApiValidator$2$1;->fromStatus(I)Lcom/squareup/api/ApiErrorResult;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic isClientError(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 0

    .line 250
    check-cast p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/ApiValidator$2$1;->isClientError(Lcom/squareup/protos/client/rolodex/GetContactResponse;I)Lcom/squareup/api/ApiErrorResult;

    move-result-object p1

    return-object p1
.end method

.method public isNetworkError()Lcom/squareup/api/ApiErrorResult;
    .locals 1

    .line 263
    sget-object v0, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_NO_NETWORK:Lcom/squareup/api/ApiErrorResult;

    return-object v0
.end method

.method public bridge synthetic isNetworkError()Ljava/lang/Object;
    .locals 1

    .line 250
    invoke-virtual {p0}, Lcom/squareup/api/ApiValidator$2$1;->isNetworkError()Lcom/squareup/api/ApiErrorResult;

    move-result-object v0

    return-object v0
.end method

.method public isRejected(Lcom/squareup/protos/client/rolodex/GetContactResponse;)Lcom/squareup/api/ApiErrorResult;
    .locals 0

    .line 259
    sget-object p1, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_INVALID_ID:Lcom/squareup/api/ApiErrorResult;

    return-object p1
.end method

.method public bridge synthetic isRejected(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 250
    check-cast p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/api/ApiValidator$2$1;->isRejected(Lcom/squareup/protos/client/rolodex/GetContactResponse;)Lcom/squareup/api/ApiErrorResult;

    move-result-object p1

    return-object p1
.end method

.method public isServerError(I)Lcom/squareup/api/ApiErrorResult;
    .locals 0

    .line 272
    invoke-direct {p0, p1}, Lcom/squareup/api/ApiValidator$2$1;->fromStatus(I)Lcom/squareup/api/ApiErrorResult;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic isServerError(I)Ljava/lang/Object;
    .locals 0

    .line 250
    invoke-virtual {p0, p1}, Lcom/squareup/api/ApiValidator$2$1;->isServerError(I)Lcom/squareup/api/ApiErrorResult;

    move-result-object p1

    return-object p1
.end method

.method public isSessionExpired()Lcom/squareup/api/ApiErrorResult;
    .locals 1

    .line 276
    sget-object v0, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_EXCEPTION_NOT_A_NETWORK_ERROR:Lcom/squareup/api/ApiErrorResult;

    return-object v0
.end method

.method public bridge synthetic isSessionExpired()Ljava/lang/Object;
    .locals 1

    .line 250
    invoke-virtual {p0}, Lcom/squareup/api/ApiValidator$2$1;->isSessionExpired()Lcom/squareup/api/ApiErrorResult;

    move-result-object v0

    return-object v0
.end method
