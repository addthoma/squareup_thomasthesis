.class public final enum Lcom/squareup/api/items/PageLayout;
.super Ljava/lang/Enum;
.source "PageLayout.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/PageLayout$ProtoAdapter_PageLayout;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/PageLayout;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/PageLayout;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/PageLayout;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FLEXIBLE:Lcom/squareup/api/items/PageLayout;

.field public static final enum IMAGE_TEXT_TILES_12X11:Lcom/squareup/api/items/PageLayout;

.field public static final enum IMAGE_TEXT_TILES_12X8:Lcom/squareup/api/items/PageLayout;

.field public static final enum IMAGE_TEXT_TILES_16X11:Lcom/squareup/api/items/PageLayout;

.field public static final enum IMAGE_TEXT_TILES_8X8:Lcom/squareup/api/items/PageLayout;

.field public static final enum IMAGE_TILES_2X10:Lcom/squareup/api/items/PageLayout;

.field public static final enum IMAGE_TILES_5X5:Lcom/squareup/api/items/PageLayout;

.field public static final enum TEXT_TILES_3X9:Lcom/squareup/api/items/PageLayout;

.field public static final enum UNKNOWN_PAGE_LAYOUT:Lcom/squareup/api/items/PageLayout;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 14
    new-instance v0, Lcom/squareup/api/items/PageLayout;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_PAGE_LAYOUT"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/api/items/PageLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PageLayout;->UNKNOWN_PAGE_LAYOUT:Lcom/squareup/api/items/PageLayout;

    .line 24
    new-instance v0, Lcom/squareup/api/items/PageLayout;

    const/4 v2, 0x1

    const-string v3, "FLEXIBLE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/api/items/PageLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PageLayout;->FLEXIBLE:Lcom/squareup/api/items/PageLayout;

    .line 33
    new-instance v0, Lcom/squareup/api/items/PageLayout;

    const/4 v3, 0x2

    const-string v4, "IMAGE_TILES_5X5"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/api/items/PageLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PageLayout;->IMAGE_TILES_5X5:Lcom/squareup/api/items/PageLayout;

    .line 41
    new-instance v0, Lcom/squareup/api/items/PageLayout;

    const/4 v4, 0x3

    const-string v5, "TEXT_TILES_3X9"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/api/items/PageLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PageLayout;->TEXT_TILES_3X9:Lcom/squareup/api/items/PageLayout;

    .line 48
    new-instance v0, Lcom/squareup/api/items/PageLayout;

    const/4 v5, 0x4

    const-string v6, "IMAGE_TEXT_TILES_8X8"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/api/items/PageLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PageLayout;->IMAGE_TEXT_TILES_8X8:Lcom/squareup/api/items/PageLayout;

    .line 50
    new-instance v0, Lcom/squareup/api/items/PageLayout;

    const/4 v6, 0x5

    const-string v7, "IMAGE_TEXT_TILES_12X11"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/api/items/PageLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PageLayout;->IMAGE_TEXT_TILES_12X11:Lcom/squareup/api/items/PageLayout;

    .line 52
    new-instance v0, Lcom/squareup/api/items/PageLayout;

    const/4 v7, 0x6

    const-string v8, "IMAGE_TEXT_TILES_12X8"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/api/items/PageLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PageLayout;->IMAGE_TEXT_TILES_12X8:Lcom/squareup/api/items/PageLayout;

    .line 54
    new-instance v0, Lcom/squareup/api/items/PageLayout;

    const/4 v8, 0x7

    const-string v9, "IMAGE_TEXT_TILES_16X11"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/api/items/PageLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PageLayout;->IMAGE_TEXT_TILES_16X11:Lcom/squareup/api/items/PageLayout;

    .line 61
    new-instance v0, Lcom/squareup/api/items/PageLayout;

    const/16 v9, 0x8

    const-string v10, "IMAGE_TILES_2X10"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/api/items/PageLayout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PageLayout;->IMAGE_TILES_2X10:Lcom/squareup/api/items/PageLayout;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/api/items/PageLayout;

    .line 13
    sget-object v10, Lcom/squareup/api/items/PageLayout;->UNKNOWN_PAGE_LAYOUT:Lcom/squareup/api/items/PageLayout;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/api/items/PageLayout;->FLEXIBLE:Lcom/squareup/api/items/PageLayout;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/PageLayout;->IMAGE_TILES_5X5:Lcom/squareup/api/items/PageLayout;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/api/items/PageLayout;->TEXT_TILES_3X9:Lcom/squareup/api/items/PageLayout;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/api/items/PageLayout;->IMAGE_TEXT_TILES_8X8:Lcom/squareup/api/items/PageLayout;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/api/items/PageLayout;->IMAGE_TEXT_TILES_12X11:Lcom/squareup/api/items/PageLayout;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/api/items/PageLayout;->IMAGE_TEXT_TILES_12X8:Lcom/squareup/api/items/PageLayout;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/api/items/PageLayout;->IMAGE_TEXT_TILES_16X11:Lcom/squareup/api/items/PageLayout;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/api/items/PageLayout;->IMAGE_TILES_2X10:Lcom/squareup/api/items/PageLayout;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/api/items/PageLayout;->$VALUES:[Lcom/squareup/api/items/PageLayout;

    .line 63
    new-instance v0, Lcom/squareup/api/items/PageLayout$ProtoAdapter_PageLayout;

    invoke-direct {v0}, Lcom/squareup/api/items/PageLayout$ProtoAdapter_PageLayout;-><init>()V

    sput-object v0, Lcom/squareup/api/items/PageLayout;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 68
    iput p3, p0, Lcom/squareup/api/items/PageLayout;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/PageLayout;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 84
    :pswitch_0
    sget-object p0, Lcom/squareup/api/items/PageLayout;->IMAGE_TILES_2X10:Lcom/squareup/api/items/PageLayout;

    return-object p0

    .line 83
    :pswitch_1
    sget-object p0, Lcom/squareup/api/items/PageLayout;->IMAGE_TEXT_TILES_16X11:Lcom/squareup/api/items/PageLayout;

    return-object p0

    .line 82
    :pswitch_2
    sget-object p0, Lcom/squareup/api/items/PageLayout;->IMAGE_TEXT_TILES_12X8:Lcom/squareup/api/items/PageLayout;

    return-object p0

    .line 81
    :pswitch_3
    sget-object p0, Lcom/squareup/api/items/PageLayout;->IMAGE_TEXT_TILES_12X11:Lcom/squareup/api/items/PageLayout;

    return-object p0

    .line 80
    :pswitch_4
    sget-object p0, Lcom/squareup/api/items/PageLayout;->IMAGE_TEXT_TILES_8X8:Lcom/squareup/api/items/PageLayout;

    return-object p0

    .line 79
    :pswitch_5
    sget-object p0, Lcom/squareup/api/items/PageLayout;->TEXT_TILES_3X9:Lcom/squareup/api/items/PageLayout;

    return-object p0

    .line 78
    :pswitch_6
    sget-object p0, Lcom/squareup/api/items/PageLayout;->IMAGE_TILES_5X5:Lcom/squareup/api/items/PageLayout;

    return-object p0

    .line 77
    :pswitch_7
    sget-object p0, Lcom/squareup/api/items/PageLayout;->FLEXIBLE:Lcom/squareup/api/items/PageLayout;

    return-object p0

    .line 76
    :pswitch_8
    sget-object p0, Lcom/squareup/api/items/PageLayout;->UNKNOWN_PAGE_LAYOUT:Lcom/squareup/api/items/PageLayout;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/PageLayout;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/api/items/PageLayout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/PageLayout;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/PageLayout;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/api/items/PageLayout;->$VALUES:[Lcom/squareup/api/items/PageLayout;

    invoke-virtual {v0}, [Lcom/squareup/api/items/PageLayout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/PageLayout;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 91
    iget v0, p0, Lcom/squareup/api/items/PageLayout;->value:I

    return v0
.end method
