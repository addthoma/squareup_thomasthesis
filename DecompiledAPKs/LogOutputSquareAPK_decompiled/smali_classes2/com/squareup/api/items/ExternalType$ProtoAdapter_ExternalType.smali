.class final Lcom/squareup/api/items/ExternalType$ProtoAdapter_ExternalType;
.super Lcom/squareup/wire/EnumAdapter;
.source "ExternalType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ExternalType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ExternalType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/api/items/ExternalType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 41
    const-class v0, Lcom/squareup/api/items/ExternalType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/api/items/ExternalType;
    .locals 0

    .line 46
    invoke-static {p1}, Lcom/squareup/api/items/ExternalType;->fromValue(I)Lcom/squareup/api/items/ExternalType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ExternalType$ProtoAdapter_ExternalType;->fromValue(I)Lcom/squareup/api/items/ExternalType;

    move-result-object p1

    return-object p1
.end method
