.class public final Lcom/squareup/api/items/Point$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Point.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Point;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/Point;",
        "Lcom/squareup/api/items/Point$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public x:Ljava/lang/Integer;

.field public y:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/Point;
    .locals 4

    .line 112
    iget-object v0, p0, Lcom/squareup/api/items/Point$Builder;->x:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/api/items/Point$Builder;->y:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 117
    new-instance v2, Lcom/squareup/api/items/Point;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/api/items/Point;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v2

    :cond_0
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 114
    iget-object v2, p0, Lcom/squareup/api/items/Point$Builder;->x:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "x"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/squareup/api/items/Point$Builder;->y:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "y"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/api/items/Point$Builder;->build()Lcom/squareup/api/items/Point;

    move-result-object v0

    return-object v0
.end method

.method public x(Ljava/lang/Integer;)Lcom/squareup/api/items/Point$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/api/items/Point$Builder;->x:Ljava/lang/Integer;

    return-object p0
.end method

.method public y(Ljava/lang/Integer;)Lcom/squareup/api/items/Point$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/api/items/Point$Builder;->y:Ljava/lang/Integer;

    return-object p0
.end method
