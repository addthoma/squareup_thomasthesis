.class public final Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;
.super Lcom/squareup/wire/Message;
.source "Surcharge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Surcharge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AutoGratuitySettings"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$ProtoAdapter_AutoGratuitySettings;,
        Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;,
        Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;",
        "Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AUTO_ENABLE_TYPE:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

.field public static final DEFAULT_MINIMUM_SEAT_COUNT:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final auto_enable_type:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Surcharge$AutoGratuitySettings$AutoEnableType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final minimum_seat_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 332
    new-instance v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$ProtoAdapter_AutoGratuitySettings;

    invoke-direct {v0}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$ProtoAdapter_AutoGratuitySettings;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 336
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->DEFAULT_MINIMUM_SEAT_COUNT:Ljava/lang/Integer;

    .line 338
    sget-object v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->UNKNOWN:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    sput-object v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->DEFAULT_AUTO_ENABLE_TYPE:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;)V
    .locals 1

    .line 357
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;-><init>(Ljava/lang/Integer;Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;Lokio/ByteString;)V
    .locals 1

    .line 362
    sget-object v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 363
    iput-object p1, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->minimum_seat_count:Ljava/lang/Integer;

    .line 364
    iput-object p2, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->auto_enable_type:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 379
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 380
    :cond_1
    check-cast p1, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    .line 381
    invoke-virtual {p0}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->minimum_seat_count:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->minimum_seat_count:Ljava/lang/Integer;

    .line 382
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->auto_enable_type:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    iget-object p1, p1, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->auto_enable_type:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    .line 383
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 388
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 390
    invoke-virtual {p0}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 391
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->minimum_seat_count:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 392
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->auto_enable_type:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 393
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;
    .locals 2

    .line 369
    new-instance v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;-><init>()V

    .line 370
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->minimum_seat_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->minimum_seat_count:Ljava/lang/Integer;

    .line 371
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->auto_enable_type:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    iput-object v1, v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->auto_enable_type:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    .line 372
    invoke-virtual {p0}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 331
    invoke-virtual {p0}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->newBuilder()Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 400
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 401
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->minimum_seat_count:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", minimum_seat_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->minimum_seat_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 402
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->auto_enable_type:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    if-eqz v1, :cond_1

    const-string v1, ", auto_enable_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->auto_enable_type:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AutoGratuitySettings{"

    .line 403
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
