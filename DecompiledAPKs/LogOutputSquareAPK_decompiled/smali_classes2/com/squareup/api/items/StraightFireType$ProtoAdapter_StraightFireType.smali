.class final Lcom/squareup/api/items/StraightFireType$ProtoAdapter_StraightFireType;
.super Lcom/squareup/wire/EnumAdapter;
.source "StraightFireType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/StraightFireType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_StraightFireType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/api/items/StraightFireType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 57
    const-class v0, Lcom/squareup/api/items/StraightFireType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/api/items/StraightFireType;
    .locals 0

    .line 62
    invoke-static {p1}, Lcom/squareup/api/items/StraightFireType;->fromValue(I)Lcom/squareup/api/items/StraightFireType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 55
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/StraightFireType$ProtoAdapter_StraightFireType;->fromValue(I)Lcom/squareup/api/items/StraightFireType;

    move-result-object p1

    return-object p1
.end method
