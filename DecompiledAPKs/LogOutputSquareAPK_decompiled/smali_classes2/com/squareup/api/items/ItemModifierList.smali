.class public final Lcom/squareup/api/items/ItemModifierList;
.super Lcom/squareup/wire/Message;
.source "ItemModifierList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/ItemModifierList$ProtoAdapter_ItemModifierList;,
        Lcom/squareup/api/items/ItemModifierList$SelectionType;,
        Lcom/squareup/api/items/ItemModifierList$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/ItemModifierList;",
        "Lcom/squareup/api/items/ItemModifierList$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/ItemModifierList;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_CONVERSATIONAL:Ljava/lang/Boolean;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ORDINAL:Ljava/lang/Integer;

.field public static final DEFAULT_SELECTION_TYPE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

.field public static final DEFAULT_V2_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.MerchantCatalogObjectReference#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final is_conversational:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final ordinal:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemModifierList$SelectionType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final v2_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 24
    new-instance v0, Lcom/squareup/api/items/ItemModifierList$ProtoAdapter_ItemModifierList;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemModifierList$ProtoAdapter_ItemModifierList;-><init>()V

    sput-object v0, Lcom/squareup/api/items/ItemModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 32
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/api/items/ItemModifierList;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    .line 34
    sget-object v1, Lcom/squareup/api/items/ItemModifierList$SelectionType;->SINGLE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    sput-object v1, Lcom/squareup/api/items/ItemModifierList;->DEFAULT_SELECTION_TYPE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    .line 36
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/ItemModifierList;->DEFAULT_IS_CONVERSATIONAL:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/items/ItemModifierList$SelectionType;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 9

    .line 99
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/api/items/ItemModifierList;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/items/ItemModifierList$SelectionType;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/items/ItemModifierList$SelectionType;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 105
    sget-object v0, Lcom/squareup/api/items/ItemModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 106
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierList;->id:Ljava/lang/String;

    .line 107
    iput-object p2, p0, Lcom/squareup/api/items/ItemModifierList;->name:Ljava/lang/String;

    .line 108
    iput-object p3, p0, Lcom/squareup/api/items/ItemModifierList;->ordinal:Ljava/lang/Integer;

    .line 109
    iput-object p4, p0, Lcom/squareup/api/items/ItemModifierList;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    .line 110
    iput-object p5, p0, Lcom/squareup/api/items/ItemModifierList;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 111
    iput-object p6, p0, Lcom/squareup/api/items/ItemModifierList;->is_conversational:Ljava/lang/Boolean;

    .line 112
    iput-object p7, p0, Lcom/squareup/api/items/ItemModifierList;->v2_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 132
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/ItemModifierList;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 133
    :cond_1
    check-cast p1, Lcom/squareup/api/items/ItemModifierList;

    .line 134
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierList;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/ItemModifierList;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ItemModifierList;->id:Ljava/lang/String;

    .line 135
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ItemModifierList;->name:Ljava/lang/String;

    .line 136
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->ordinal:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/ItemModifierList;->ordinal:Ljava/lang/Integer;

    .line 137
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    iget-object v3, p1, Lcom/squareup/api/items/ItemModifierList;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    .line 138
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v3, p1, Lcom/squareup/api/items/ItemModifierList;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->is_conversational:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/ItemModifierList;->is_conversational:Ljava/lang/Boolean;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->v2_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/api/items/ItemModifierList;->v2_id:Ljava/lang/String;

    .line 141
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 146
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 148
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierList;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 149
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 150
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemModifierList$SelectionType;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/api/items/MerchantCatalogObjectReference;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->is_conversational:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->v2_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 156
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/ItemModifierList$Builder;
    .locals 2

    .line 117
    new-instance v0, Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemModifierList$Builder;-><init>()V

    .line 118
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierList$Builder;->id:Ljava/lang/String;

    .line 119
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierList$Builder;->name:Ljava/lang/String;

    .line 120
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->ordinal:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierList$Builder;->ordinal:Ljava/lang/Integer;

    .line 121
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierList$Builder;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    .line 122
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierList$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 123
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->is_conversational:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierList$Builder;->is_conversational:Ljava/lang/Boolean;

    .line 124
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->v2_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierList$Builder;->v2_id:Ljava/lang/String;

    .line 125
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierList;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemModifierList$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierList;->newBuilder()Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 164
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", ordinal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->ordinal:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 167
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    if-eqz v1, :cond_3

    const-string v1, ", selection_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 168
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_4

    const-string v1, ", catalog_object_reference="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 169
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->is_conversational:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", is_conversational="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->is_conversational:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 170
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->v2_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", v2_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList;->v2_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ItemModifierList{"

    .line 171
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
