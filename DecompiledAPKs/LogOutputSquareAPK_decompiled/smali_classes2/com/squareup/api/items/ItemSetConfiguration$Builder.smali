.class public final Lcom/squareup/api/items/ItemSetConfiguration$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ItemSetConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ItemSetConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/ItemSetConfiguration;",
        "Lcom/squareup/api/items/ItemSetConfiguration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public applies_to_custom_amounts:Ljava/lang/Boolean;

.field public filter:Lcom/squareup/api/items/ObjectFilter;

.field public item:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 124
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 125
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/ItemSetConfiguration$Builder;->item:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public applies_to_custom_amounts(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemSetConfiguration$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/api/items/ItemSetConfiguration$Builder;->applies_to_custom_amounts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/api/items/ItemSetConfiguration;
    .locals 5

    .line 154
    new-instance v0, Lcom/squareup/api/items/ItemSetConfiguration;

    iget-object v1, p0, Lcom/squareup/api/items/ItemSetConfiguration$Builder;->filter:Lcom/squareup/api/items/ObjectFilter;

    iget-object v2, p0, Lcom/squareup/api/items/ItemSetConfiguration$Builder;->item:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/api/items/ItemSetConfiguration$Builder;->applies_to_custom_amounts:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/api/items/ItemSetConfiguration;-><init>(Lcom/squareup/api/items/ObjectFilter;Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemSetConfiguration$Builder;->build()Lcom/squareup/api/items/ItemSetConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public filter(Lcom/squareup/api/items/ObjectFilter;)Lcom/squareup/api/items/ItemSetConfiguration$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/api/items/ItemSetConfiguration$Builder;->filter:Lcom/squareup/api/items/ObjectFilter;

    return-object p0
.end method

.method public item(Ljava/util/List;)Lcom/squareup/api/items/ItemSetConfiguration$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)",
            "Lcom/squareup/api/items/ItemSetConfiguration$Builder;"
        }
    .end annotation

    .line 137
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 138
    iput-object p1, p0, Lcom/squareup/api/items/ItemSetConfiguration$Builder;->item:Ljava/util/List;

    return-object p0
.end method
