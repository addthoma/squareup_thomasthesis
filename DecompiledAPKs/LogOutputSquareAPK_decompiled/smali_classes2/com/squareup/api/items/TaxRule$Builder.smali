.class public final Lcom/squareup/api/items/TaxRule$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TaxRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/TaxRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/TaxRule;",
        "Lcom/squareup/api/items/TaxRule$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public action_type:Lcom/squareup/api/items/EnablingActionType;

.field public condition:Lcom/squareup/api/items/TaxRule$Condition;

.field public exempt_product_set:Lcom/squareup/api/sync/ObjectId;

.field public id:Ljava/lang/String;

.field public item_config:Lcom/squareup/api/items/ItemSetConfiguration;

.field public name:Ljava/lang/String;

.field public tax_config:Lcom/squareup/api/items/TaxSetConfiguration;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 196
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public action_type(Lcom/squareup/api/items/EnablingActionType;)Lcom/squareup/api/items/TaxRule$Builder;
    .locals 0

    .line 233
    iput-object p1, p0, Lcom/squareup/api/items/TaxRule$Builder;->action_type:Lcom/squareup/api/items/EnablingActionType;

    return-object p0
.end method

.method public build()Lcom/squareup/api/items/TaxRule;
    .locals 10

    .line 259
    new-instance v9, Lcom/squareup/api/items/TaxRule;

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/TaxRule$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/TaxRule$Builder;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    iget-object v4, p0, Lcom/squareup/api/items/TaxRule$Builder;->tax_config:Lcom/squareup/api/items/TaxSetConfiguration;

    iget-object v5, p0, Lcom/squareup/api/items/TaxRule$Builder;->action_type:Lcom/squareup/api/items/EnablingActionType;

    iget-object v6, p0, Lcom/squareup/api/items/TaxRule$Builder;->item_config:Lcom/squareup/api/items/ItemSetConfiguration;

    iget-object v7, p0, Lcom/squareup/api/items/TaxRule$Builder;->exempt_product_set:Lcom/squareup/api/sync/ObjectId;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/api/items/TaxRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TaxRule$Condition;Lcom/squareup/api/items/TaxSetConfiguration;Lcom/squareup/api/items/EnablingActionType;Lcom/squareup/api/items/ItemSetConfiguration;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 181
    invoke-virtual {p0}, Lcom/squareup/api/items/TaxRule$Builder;->build()Lcom/squareup/api/items/TaxRule;

    move-result-object v0

    return-object v0
.end method

.method public condition(Lcom/squareup/api/items/TaxRule$Condition;)Lcom/squareup/api/items/TaxRule$Builder;
    .locals 0

    .line 213
    iput-object p1, p0, Lcom/squareup/api/items/TaxRule$Builder;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    return-object p0
.end method

.method public exempt_product_set(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/TaxRule$Builder;
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/squareup/api/items/TaxRule$Builder;->exempt_product_set:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/TaxRule$Builder;
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/squareup/api/items/TaxRule$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public item_config(Lcom/squareup/api/items/ItemSetConfiguration;)Lcom/squareup/api/items/TaxRule$Builder;
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/squareup/api/items/TaxRule$Builder;->item_config:Lcom/squareup/api/items/ItemSetConfiguration;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/TaxRule$Builder;
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/api/items/TaxRule$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public tax_config(Lcom/squareup/api/items/TaxSetConfiguration;)Lcom/squareup/api/items/TaxRule$Builder;
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/api/items/TaxRule$Builder;->tax_config:Lcom/squareup/api/items/TaxSetConfiguration;

    return-object p0
.end method
