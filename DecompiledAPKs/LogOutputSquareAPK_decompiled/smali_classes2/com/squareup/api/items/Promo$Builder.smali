.class public final Lcom/squareup/api/items/Promo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Promo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Promo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/Promo;",
        "Lcom/squareup/api/items/Promo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public active:Ljava/lang/Boolean;

.field public discount:Lcom/squareup/api/sync/ObjectId;

.field public ends_at:Ljava/lang/Long;

.field public id:Ljava/lang/String;

.field public limited_use:Ljava/lang/Boolean;

.field public num_times_allowed:Ljava/lang/Integer;

.field public num_times_used:Ljava/lang/Integer;

.field public promo_code:Ljava/lang/String;

.field public starts_at:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 213
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public active(Ljava/lang/Boolean;)Lcom/squareup/api/items/Promo$Builder;
    .locals 0

    .line 261
    iput-object p1, p0, Lcom/squareup/api/items/Promo$Builder;->active:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/api/items/Promo;
    .locals 12

    .line 272
    new-instance v11, Lcom/squareup/api/items/Promo;

    iget-object v1, p0, Lcom/squareup/api/items/Promo$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/Promo$Builder;->discount:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p0, Lcom/squareup/api/items/Promo$Builder;->promo_code:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/api/items/Promo$Builder;->num_times_allowed:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/api/items/Promo$Builder;->num_times_used:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/api/items/Promo$Builder;->starts_at:Ljava/lang/Long;

    iget-object v7, p0, Lcom/squareup/api/items/Promo$Builder;->ends_at:Ljava/lang/Long;

    iget-object v8, p0, Lcom/squareup/api/items/Promo$Builder;->active:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/api/items/Promo$Builder;->limited_use:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/api/items/Promo;-><init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 194
    invoke-virtual {p0}, Lcom/squareup/api/items/Promo$Builder;->build()Lcom/squareup/api/items/Promo;

    move-result-object v0

    return-object v0
.end method

.method public discount(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Promo$Builder;
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/squareup/api/items/Promo$Builder;->discount:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public ends_at(Ljava/lang/Long;)Lcom/squareup/api/items/Promo$Builder;
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/squareup/api/items/Promo$Builder;->ends_at:Ljava/lang/Long;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/Promo$Builder;
    .locals 0

    .line 217
    iput-object p1, p0, Lcom/squareup/api/items/Promo$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public limited_use(Ljava/lang/Boolean;)Lcom/squareup/api/items/Promo$Builder;
    .locals 0

    .line 266
    iput-object p1, p0, Lcom/squareup/api/items/Promo$Builder;->limited_use:Ljava/lang/Boolean;

    return-object p0
.end method

.method public num_times_allowed(Ljava/lang/Integer;)Lcom/squareup/api/items/Promo$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/api/items/Promo$Builder;->num_times_allowed:Ljava/lang/Integer;

    return-object p0
.end method

.method public num_times_used(Ljava/lang/Integer;)Lcom/squareup/api/items/Promo$Builder;
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/api/items/Promo$Builder;->num_times_used:Ljava/lang/Integer;

    return-object p0
.end method

.method public promo_code(Ljava/lang/String;)Lcom/squareup/api/items/Promo$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/api/items/Promo$Builder;->promo_code:Ljava/lang/String;

    return-object p0
.end method

.method public starts_at(Ljava/lang/Long;)Lcom/squareup/api/items/Promo$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/api/items/Promo$Builder;->starts_at:Ljava/lang/Long;

    return-object p0
.end method
