.class public final enum Lcom/squareup/api/items/PromoConsumptionResult;
.super Ljava/lang/Enum;
.source "PromoConsumptionResult.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/PromoConsumptionResult$ProtoAdapter_PromoConsumptionResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/PromoConsumptionResult;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/PromoConsumptionResult;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/PromoConsumptionResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PERSIST_FAILURE:Lcom/squareup/api/items/PromoConsumptionResult;

.field public static final enum PROMO_NOT_FOUND:Lcom/squareup/api/items/PromoConsumptionResult;

.field public static final enum PROMO_OK:Lcom/squareup/api/items/PromoConsumptionResult;

.field public static final enum REACHED_LIMIT:Lcom/squareup/api/items/PromoConsumptionResult;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 11
    new-instance v0, Lcom/squareup/api/items/PromoConsumptionResult;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "REACHED_LIMIT"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/api/items/PromoConsumptionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PromoConsumptionResult;->REACHED_LIMIT:Lcom/squareup/api/items/PromoConsumptionResult;

    .line 16
    new-instance v0, Lcom/squareup/api/items/PromoConsumptionResult;

    const/4 v3, 0x2

    const-string v4, "PERSIST_FAILURE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/api/items/PromoConsumptionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PromoConsumptionResult;->PERSIST_FAILURE:Lcom/squareup/api/items/PromoConsumptionResult;

    .line 21
    new-instance v0, Lcom/squareup/api/items/PromoConsumptionResult;

    const/4 v4, 0x3

    const-string v5, "PROMO_OK"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/api/items/PromoConsumptionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PromoConsumptionResult;->PROMO_OK:Lcom/squareup/api/items/PromoConsumptionResult;

    .line 26
    new-instance v0, Lcom/squareup/api/items/PromoConsumptionResult;

    const/4 v5, 0x4

    const-string v6, "PROMO_NOT_FOUND"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/api/items/PromoConsumptionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PromoConsumptionResult;->PROMO_NOT_FOUND:Lcom/squareup/api/items/PromoConsumptionResult;

    new-array v0, v5, [Lcom/squareup/api/items/PromoConsumptionResult;

    .line 10
    sget-object v5, Lcom/squareup/api/items/PromoConsumptionResult;->REACHED_LIMIT:Lcom/squareup/api/items/PromoConsumptionResult;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/api/items/PromoConsumptionResult;->PERSIST_FAILURE:Lcom/squareup/api/items/PromoConsumptionResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/PromoConsumptionResult;->PROMO_OK:Lcom/squareup/api/items/PromoConsumptionResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/api/items/PromoConsumptionResult;->PROMO_NOT_FOUND:Lcom/squareup/api/items/PromoConsumptionResult;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/api/items/PromoConsumptionResult;->$VALUES:[Lcom/squareup/api/items/PromoConsumptionResult;

    .line 28
    new-instance v0, Lcom/squareup/api/items/PromoConsumptionResult$ProtoAdapter_PromoConsumptionResult;

    invoke-direct {v0}, Lcom/squareup/api/items/PromoConsumptionResult$ProtoAdapter_PromoConsumptionResult;-><init>()V

    sput-object v0, Lcom/squareup/api/items/PromoConsumptionResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput p3, p0, Lcom/squareup/api/items/PromoConsumptionResult;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/PromoConsumptionResult;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 44
    :cond_0
    sget-object p0, Lcom/squareup/api/items/PromoConsumptionResult;->PROMO_NOT_FOUND:Lcom/squareup/api/items/PromoConsumptionResult;

    return-object p0

    .line 43
    :cond_1
    sget-object p0, Lcom/squareup/api/items/PromoConsumptionResult;->PROMO_OK:Lcom/squareup/api/items/PromoConsumptionResult;

    return-object p0

    .line 42
    :cond_2
    sget-object p0, Lcom/squareup/api/items/PromoConsumptionResult;->PERSIST_FAILURE:Lcom/squareup/api/items/PromoConsumptionResult;

    return-object p0

    .line 41
    :cond_3
    sget-object p0, Lcom/squareup/api/items/PromoConsumptionResult;->REACHED_LIMIT:Lcom/squareup/api/items/PromoConsumptionResult;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/PromoConsumptionResult;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/api/items/PromoConsumptionResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/PromoConsumptionResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/PromoConsumptionResult;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/api/items/PromoConsumptionResult;->$VALUES:[Lcom/squareup/api/items/PromoConsumptionResult;

    invoke-virtual {v0}, [Lcom/squareup/api/items/PromoConsumptionResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/PromoConsumptionResult;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 51
    iget v0, p0, Lcom/squareup/api/items/PromoConsumptionResult;->value:I

    return v0
.end method
