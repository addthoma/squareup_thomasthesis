.class public final Lcom/squareup/api/items/FavoritesListPosition$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FavoritesListPosition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/FavoritesListPosition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/FavoritesListPosition;",
        "Lcom/squareup/api/items/FavoritesListPosition$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public favorite_object:Lcom/squareup/api/sync/ObjectId;

.field public id:Ljava/lang/String;

.field public ordinal:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 118
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/FavoritesListPosition;
    .locals 5

    .line 146
    new-instance v0, Lcom/squareup/api/items/FavoritesListPosition;

    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/FavoritesListPosition$Builder;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p0, Lcom/squareup/api/items/FavoritesListPosition$Builder;->ordinal:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/api/items/FavoritesListPosition;-><init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 111
    invoke-virtual {p0}, Lcom/squareup/api/items/FavoritesListPosition$Builder;->build()Lcom/squareup/api/items/FavoritesListPosition;

    move-result-object v0

    return-object v0
.end method

.method public favorite_object(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/FavoritesListPosition$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/api/items/FavoritesListPosition$Builder;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/FavoritesListPosition$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/api/items/FavoritesListPosition$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/FavoritesListPosition$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/api/items/FavoritesListPosition$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method
