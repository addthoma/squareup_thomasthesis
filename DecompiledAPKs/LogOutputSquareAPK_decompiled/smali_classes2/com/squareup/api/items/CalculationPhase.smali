.class public final enum Lcom/squareup/api/items/CalculationPhase;
.super Ljava/lang/Enum;
.source "CalculationPhase.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/CalculationPhase$ProtoAdapter_CalculationPhase;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/CalculationPhase;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/CalculationPhase;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/CalculationPhase;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DISCOUNT_AMOUNT_PHASE:Lcom/squareup/api/items/CalculationPhase;

.field public static final enum DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

.field public static final enum FEE_SUBTOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

.field public static final enum FEE_TOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

.field public static final enum SURCHARGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

.field public static final enum SURCHARGE_TOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

.field public static final enum SWEDISH_ROUNDING_PHASE:Lcom/squareup/api/items/CalculationPhase;

.field public static final enum TIP_PHASE:Lcom/squareup/api/items/CalculationPhase;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 11
    new-instance v0, Lcom/squareup/api/items/CalculationPhase;

    const/4 v1, 0x0

    const-string v2, "DISCOUNT_PERCENTAGE_PHASE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/api/items/CalculationPhase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    .line 13
    new-instance v0, Lcom/squareup/api/items/CalculationPhase;

    const/4 v2, 0x1

    const-string v3, "DISCOUNT_AMOUNT_PHASE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/api/items/CalculationPhase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_AMOUNT_PHASE:Lcom/squareup/api/items/CalculationPhase;

    .line 15
    new-instance v0, Lcom/squareup/api/items/CalculationPhase;

    const/4 v3, 0x2

    const-string v4, "FEE_SUBTOTAL_PHASE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/api/items/CalculationPhase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/CalculationPhase;->FEE_SUBTOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

    .line 17
    new-instance v0, Lcom/squareup/api/items/CalculationPhase;

    const/4 v4, 0x3

    const-string v5, "FEE_TOTAL_PHASE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/api/items/CalculationPhase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/CalculationPhase;->FEE_TOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

    .line 19
    new-instance v0, Lcom/squareup/api/items/CalculationPhase;

    const/4 v5, 0x4

    const-string v6, "TIP_PHASE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/api/items/CalculationPhase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/CalculationPhase;->TIP_PHASE:Lcom/squareup/api/items/CalculationPhase;

    .line 21
    new-instance v0, Lcom/squareup/api/items/CalculationPhase;

    const/4 v6, 0x5

    const-string v7, "SWEDISH_ROUNDING_PHASE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/api/items/CalculationPhase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/CalculationPhase;->SWEDISH_ROUNDING_PHASE:Lcom/squareup/api/items/CalculationPhase;

    .line 23
    new-instance v0, Lcom/squareup/api/items/CalculationPhase;

    const/4 v7, 0x6

    const-string v8, "SURCHARGE_PHASE"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/api/items/CalculationPhase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/CalculationPhase;->SURCHARGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    .line 25
    new-instance v0, Lcom/squareup/api/items/CalculationPhase;

    const/4 v8, 0x7

    const-string v9, "SURCHARGE_TOTAL_PHASE"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/api/items/CalculationPhase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/CalculationPhase;->SURCHARGE_TOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/api/items/CalculationPhase;

    .line 10
    sget-object v9, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_AMOUNT_PHASE:Lcom/squareup/api/items/CalculationPhase;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/CalculationPhase;->FEE_SUBTOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/api/items/CalculationPhase;->FEE_TOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/api/items/CalculationPhase;->TIP_PHASE:Lcom/squareup/api/items/CalculationPhase;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/api/items/CalculationPhase;->SWEDISH_ROUNDING_PHASE:Lcom/squareup/api/items/CalculationPhase;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/api/items/CalculationPhase;->SURCHARGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/api/items/CalculationPhase;->SURCHARGE_TOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/api/items/CalculationPhase;->$VALUES:[Lcom/squareup/api/items/CalculationPhase;

    .line 27
    new-instance v0, Lcom/squareup/api/items/CalculationPhase$ProtoAdapter_CalculationPhase;

    invoke-direct {v0}, Lcom/squareup/api/items/CalculationPhase$ProtoAdapter_CalculationPhase;-><init>()V

    sput-object v0, Lcom/squareup/api/items/CalculationPhase;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput p3, p0, Lcom/squareup/api/items/CalculationPhase;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/CalculationPhase;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 47
    :pswitch_0
    sget-object p0, Lcom/squareup/api/items/CalculationPhase;->SURCHARGE_TOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

    return-object p0

    .line 46
    :pswitch_1
    sget-object p0, Lcom/squareup/api/items/CalculationPhase;->SURCHARGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    return-object p0

    .line 45
    :pswitch_2
    sget-object p0, Lcom/squareup/api/items/CalculationPhase;->SWEDISH_ROUNDING_PHASE:Lcom/squareup/api/items/CalculationPhase;

    return-object p0

    .line 44
    :pswitch_3
    sget-object p0, Lcom/squareup/api/items/CalculationPhase;->TIP_PHASE:Lcom/squareup/api/items/CalculationPhase;

    return-object p0

    .line 43
    :pswitch_4
    sget-object p0, Lcom/squareup/api/items/CalculationPhase;->FEE_TOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

    return-object p0

    .line 42
    :pswitch_5
    sget-object p0, Lcom/squareup/api/items/CalculationPhase;->FEE_SUBTOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

    return-object p0

    .line 41
    :pswitch_6
    sget-object p0, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_AMOUNT_PHASE:Lcom/squareup/api/items/CalculationPhase;

    return-object p0

    .line 40
    :pswitch_7
    sget-object p0, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/CalculationPhase;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/api/items/CalculationPhase;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/CalculationPhase;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/CalculationPhase;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/api/items/CalculationPhase;->$VALUES:[Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v0}, [Lcom/squareup/api/items/CalculationPhase;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/CalculationPhase;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 54
    iget v0, p0, Lcom/squareup/api/items/CalculationPhase;->value:I

    return v0
.end method
