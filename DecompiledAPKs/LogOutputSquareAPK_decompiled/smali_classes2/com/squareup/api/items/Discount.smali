.class public final Lcom/squareup/api/items/Discount;
.super Lcom/squareup/wire/Message;
.source "Discount.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Discount$ProtoAdapter_Discount;,
        Lcom/squareup/api/items/Discount$ModifyTaxBasis;,
        Lcom/squareup/api/items/Discount$ApplicationMethod;,
        Lcom/squareup/api/items/Discount$DiscountType;,
        Lcom/squareup/api/items/Discount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/Discount;",
        "Lcom/squareup/api/items/Discount$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Discount;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APPLICATION_METHOD:Lcom/squareup/api/items/Discount$ApplicationMethod;

.field public static final DEFAULT_COLOR:Ljava/lang/String; = ""

.field public static final DEFAULT_COMP_ORDINAL:Ljava/lang/Integer;

.field public static final DEFAULT_DISCOUNT_TYPE:Lcom/squareup/api/items/Discount$DiscountType;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_PIN_REQUIRED:Ljava/lang/Boolean;

.field public static final DEFAULT_V2_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final amount:Lcom/squareup/protos/common/dinero/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.dinero.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Discount$ApplicationMethod#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.MerchantCatalogObjectReference#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final color:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final comp_ordinal:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xa
    .end annotation
.end field

.field public final discount_type:Lcom/squareup/api/items/Discount$DiscountType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Discount$DiscountType#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final maximum_amount:Lcom/squareup/protos/common/dinero/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.dinero.Money#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final modify_tax_basis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Discount$ModifyTaxBasis#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final pin_required:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final v2_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 36
    new-instance v0, Lcom/squareup/api/items/Discount$ProtoAdapter_Discount;

    invoke-direct {v0}, Lcom/squareup/api/items/Discount$ProtoAdapter_Discount;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 48
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/api/items/Discount;->DEFAULT_PIN_REQUIRED:Ljava/lang/Boolean;

    .line 50
    sget-object v1, Lcom/squareup/api/items/Discount$DiscountType;->FIXED:Lcom/squareup/api/items/Discount$DiscountType;

    sput-object v1, Lcom/squareup/api/items/Discount;->DEFAULT_DISCOUNT_TYPE:Lcom/squareup/api/items/Discount$DiscountType;

    .line 52
    sget-object v1, Lcom/squareup/api/items/Discount$ApplicationMethod;->MANUALLY_APPLIED:Lcom/squareup/api/items/Discount$ApplicationMethod;

    sput-object v1, Lcom/squareup/api/items/Discount;->DEFAULT_APPLICATION_METHOD:Lcom/squareup/api/items/Discount$ApplicationMethod;

    .line 54
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_COMP_ORDINAL:Ljava/lang/Integer;

    .line 58
    sget-object v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    sput-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Ljava/lang/Boolean;Lcom/squareup/api/items/Discount$DiscountType;Lcom/squareup/api/items/Discount$ApplicationMethod;Ljava/lang/Integer;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/String;Lcom/squareup/api/items/Discount$ModifyTaxBasis;Lcom/squareup/protos/common/dinero/Money;)V
    .locals 15

    .line 182
    sget-object v14, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/api/items/Discount;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Ljava/lang/Boolean;Lcom/squareup/api/items/Discount$DiscountType;Lcom/squareup/api/items/Discount$ApplicationMethod;Ljava/lang/Integer;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/String;Lcom/squareup/api/items/Discount$ModifyTaxBasis;Lcom/squareup/protos/common/dinero/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Ljava/lang/Boolean;Lcom/squareup/api/items/Discount$DiscountType;Lcom/squareup/api/items/Discount$ApplicationMethod;Ljava/lang/Integer;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/String;Lcom/squareup/api/items/Discount$ModifyTaxBasis;Lcom/squareup/protos/common/dinero/Money;Lokio/ByteString;)V
    .locals 1

    .line 189
    sget-object v0, Lcom/squareup/api/items/Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p14}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 190
    iput-object p1, p0, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    .line 191
    iput-object p2, p0, Lcom/squareup/api/items/Discount;->color:Ljava/lang/String;

    .line 192
    iput-object p3, p0, Lcom/squareup/api/items/Discount;->name:Ljava/lang/String;

    .line 193
    iput-object p4, p0, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    .line 194
    iput-object p5, p0, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    .line 195
    iput-object p6, p0, Lcom/squareup/api/items/Discount;->pin_required:Ljava/lang/Boolean;

    .line 196
    iput-object p7, p0, Lcom/squareup/api/items/Discount;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    .line 197
    iput-object p8, p0, Lcom/squareup/api/items/Discount;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    .line 198
    iput-object p9, p0, Lcom/squareup/api/items/Discount;->comp_ordinal:Ljava/lang/Integer;

    .line 199
    iput-object p10, p0, Lcom/squareup/api/items/Discount;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 200
    iput-object p11, p0, Lcom/squareup/api/items/Discount;->v2_id:Ljava/lang/String;

    .line 201
    iput-object p12, p0, Lcom/squareup/api/items/Discount;->modify_tax_basis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    .line 202
    iput-object p13, p0, Lcom/squareup/api/items/Discount;->maximum_amount:Lcom/squareup/protos/common/dinero/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 228
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/Discount;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 229
    :cond_1
    check-cast p1, Lcom/squareup/api/items/Discount;

    .line 230
    invoke-virtual {p0}, Lcom/squareup/api/items/Discount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/Discount;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    .line 231
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->color:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Discount;->color:Ljava/lang/String;

    .line 232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Discount;->name:Ljava/lang/String;

    .line 233
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    .line 234
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    iget-object v3, p1, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    .line 235
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->pin_required:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/Discount;->pin_required:Ljava/lang/Boolean;

    .line 236
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    iget-object v3, p1, Lcom/squareup/api/items/Discount;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    .line 237
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    iget-object v3, p1, Lcom/squareup/api/items/Discount;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    .line 238
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->comp_ordinal:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/Discount;->comp_ordinal:Ljava/lang/Integer;

    .line 239
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v3, p1, Lcom/squareup/api/items/Discount;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 240
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->v2_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Discount;->v2_id:Ljava/lang/String;

    .line 241
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->modify_tax_basis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    iget-object v3, p1, Lcom/squareup/api/items/Discount;->modify_tax_basis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    .line 242
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->maximum_amount:Lcom/squareup/protos/common/dinero/Money;

    iget-object p1, p1, Lcom/squareup/api/items/Discount;->maximum_amount:Lcom/squareup/protos/common/dinero/Money;

    .line 243
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 248
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_d

    .line 250
    invoke-virtual {p0}, Lcom/squareup/api/items/Discount;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 251
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 252
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->color:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 253
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 254
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 255
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/dinero/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 256
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->pin_required:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 257
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/api/items/Discount$DiscountType;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 258
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/api/items/Discount$ApplicationMethod;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 259
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->comp_ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 260
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/api/items/MerchantCatalogObjectReference;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 261
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->v2_id:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 262
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->modify_tax_basis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 263
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->maximum_amount:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/common/dinero/Money;->hashCode()I

    move-result v2

    :cond_c
    add-int/2addr v0, v2

    .line 264
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_d
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/Discount$Builder;
    .locals 2

    .line 207
    new-instance v0, Lcom/squareup/api/items/Discount$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Discount$Builder;-><init>()V

    .line 208
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Discount$Builder;->id:Ljava/lang/String;

    .line 209
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->color:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Discount$Builder;->color:Ljava/lang/String;

    .line 210
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Discount$Builder;->name:Ljava/lang/String;

    .line 211
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Discount$Builder;->percentage:Ljava/lang/String;

    .line 212
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    iput-object v1, v0, Lcom/squareup/api/items/Discount$Builder;->amount:Lcom/squareup/protos/common/dinero/Money;

    .line 213
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->pin_required:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/Discount$Builder;->pin_required:Ljava/lang/Boolean;

    .line 214
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    iput-object v1, v0, Lcom/squareup/api/items/Discount$Builder;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    .line 215
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    iput-object v1, v0, Lcom/squareup/api/items/Discount$Builder;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    .line 216
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->comp_ordinal:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/Discount$Builder;->comp_ordinal:Ljava/lang/Integer;

    .line 217
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iput-object v1, v0, Lcom/squareup/api/items/Discount$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 218
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->v2_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Discount$Builder;->v2_id:Ljava/lang/String;

    .line 219
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->modify_tax_basis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    iput-object v1, v0, Lcom/squareup/api/items/Discount$Builder;->modify_tax_basis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    .line 220
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->maximum_amount:Lcom/squareup/protos/common/dinero/Money;

    iput-object v1, v0, Lcom/squareup/api/items/Discount$Builder;->maximum_amount:Lcom/squareup/protos/common/dinero/Money;

    .line 221
    invoke-virtual {p0}, Lcom/squareup/api/items/Discount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Discount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/api/items/Discount;->newBuilder()Lcom/squareup/api/items/Discount$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 272
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->color:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->color:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_4

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 277
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->pin_required:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", pin_required="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->pin_required:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 278
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    if-eqz v1, :cond_6

    const-string v1, ", discount_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 279
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    if-eqz v1, :cond_7

    const-string v1, ", application_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 280
    :cond_7
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->comp_ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    const-string v1, ", comp_ordinal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->comp_ordinal:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 281
    :cond_8
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_9

    const-string v1, ", catalog_object_reference="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 282
    :cond_9
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->v2_id:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", v2_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->v2_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    :cond_a
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->modify_tax_basis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    if-eqz v1, :cond_b

    const-string v1, ", modify_tax_basis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->modify_tax_basis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 284
    :cond_b
    iget-object v1, p0, Lcom/squareup/api/items/Discount;->maximum_amount:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_c

    const-string v1, ", maximum_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Discount;->maximum_amount:Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Discount{"

    .line 285
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
