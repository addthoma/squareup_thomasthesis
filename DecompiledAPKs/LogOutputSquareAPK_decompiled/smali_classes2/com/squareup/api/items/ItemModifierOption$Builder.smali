.class public final Lcom/squareup/api/items/ItemModifierOption$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ItemModifierOption.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ItemModifierOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/ItemModifierOption;",
        "Lcom/squareup/api/items/ItemModifierOption$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

.field public id:Ljava/lang/String;

.field public modifier_list:Lcom/squareup/api/sync/ObjectId;

.field public name:Ljava/lang/String;

.field public on_by_default:Ljava/lang/Boolean;

.field public ordinal:Ljava/lang/Integer;

.field public price:Lcom/squareup/protos/common/dinero/Money;

.field public v2_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 198
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/ItemModifierOption;
    .locals 11

    .line 255
    new-instance v10, Lcom/squareup/api/items/ItemModifierOption;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->price:Lcom/squareup/protos/common/dinero/Money;

    iget-object v3, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->on_by_default:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->ordinal:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    iget-object v7, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v8, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->v2_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/api/items/ItemModifierOption;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/String;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 181
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierOption$Builder;->build()Lcom/squareup/api/items/ItemModifierOption;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_reference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/api/items/ItemModifierOption$Builder;
    .locals 0

    .line 239
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public modifier_list(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemModifierOption$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public on_by_default(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemModifierOption$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->on_by_default:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemModifierOption$Builder;
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemModifierOption$Builder;
    .locals 0

    .line 207
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->price:Lcom/squareup/protos/common/dinero/Money;

    return-object p0
.end method

.method public v2_id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierOption$Builder;->v2_id:Ljava/lang/String;

    return-object p0
.end method
