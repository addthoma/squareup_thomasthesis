.class final Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater$onEnterScope$2;
.super Ljava/lang/Object;
.source "RealMultipassDeviceIdUpdater.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/SimpleResponse;",
        "it",
        "Lcom/squareup/connectivity/InternetState;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;


# direct methods
.method constructor <init>(Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater$onEnterScope$2;->this$0:Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/connectivity/InternetState;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/connectivity/InternetState;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object p1, p0, Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater$onEnterScope$2;->this$0:Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;

    invoke-static {p1}, Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;->access$updateDeviceIdIfNeeded(Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/connectivity/InternetState;

    invoke-virtual {p0, p1}, Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater$onEnterScope$2;->apply(Lcom/squareup/connectivity/InternetState;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
