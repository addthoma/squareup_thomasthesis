.class public final Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper_Factory;
.super Ljava/lang/Object;
.source "RealMultipassOtkHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/multipassauth/MultipassService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/SessionIdPIIProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/multipassauth/MultipassService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/SessionIdPIIProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/multipassauth/MultipassService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/SessionIdPIIProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;)",
            "Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/api/multipassauth/MultipassService;Lcom/squareup/account/SessionIdPIIProvider;Lcom/squareup/http/Server;)Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;-><init>(Lcom/squareup/api/multipassauth/MultipassService;Lcom/squareup/account/SessionIdPIIProvider;Lcom/squareup/http/Server;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/multipassauth/MultipassService;

    iget-object v1, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/account/SessionIdPIIProvider;

    iget-object v2, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/http/Server;

    invoke-static {v0, v1, v2}, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper_Factory;->newInstance(Lcom/squareup/api/multipassauth/MultipassService;Lcom/squareup/account/SessionIdPIIProvider;Lcom/squareup/http/Server;)Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper_Factory;->get()Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;

    move-result-object v0

    return-object v0
.end method
