.class public abstract Lcom/squareup/api/ServicesCommonModule;
.super Ljava/lang/Object;
.source "ServicesCommonModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideImageService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/ImageService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 21
    const-class v0, Lcom/squareup/server/ImageService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/ImageService;

    return-object p0
.end method

.method static provideSafetyNetService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/SafetyNetService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 32
    const-class v0, Lcom/squareup/server/SafetyNetService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/SafetyNetService;

    return-object p0
.end method

.method static provideStandardReceiver(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;)Lcom/squareup/receiving/StandardReceiver;
    .locals 1
    .param p0    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/receiving/StandardReceiver;

    invoke-direct {v0, p0, p1}, Lcom/squareup/receiving/StandardReceiver;-><init>(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;)V

    return-object v0
.end method

.method static provideTransactionLedgerService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/transaction_ledger/TransactionLedgerService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 27
    const-class v0, Lcom/squareup/server/transaction_ledger/TransactionLedgerService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/transaction_ledger/TransactionLedgerService;

    return-object p0
.end method
