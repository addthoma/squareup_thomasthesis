.class public final enum Lcom/squareup/api/sync/CatalogFeature;
.super Ljava/lang/Enum;
.source "CatalogFeature.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/sync/CatalogFeature$ProtoAdapter_CatalogFeature;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/sync/CatalogFeature;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/sync/CatalogFeature;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ADVANCED_MODIFIERS:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum ADVANCED_TAXES:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum APPOINTMENTS_SERVICE:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum COMP_DISCOUNTS:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum CONDITIONAL_TAXES:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum CONFIGURATION:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum COUPONS:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum CUSTOM_ATTRIBUTES:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum DINING_OPTIONS:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum FAVORITES_LIST:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum FLEXIBLE_PAGE_LAYOUTS:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum FLOOR_PLANS:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum FRACTIONAL_QUANTITIES:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum FUNCTION_PLACEHOLDER_TYPES:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum GIFT_CARDS:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum ITEM_OPTIONS:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum ITEM_VARIATION_LIMIT:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum LESSER_EQUAL_VALUE:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum MAX_APPLICATIONS_PER_ATTACHMENT:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum MENU_GROUPS:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum MENU_GROUP_MEMBERSHIPS:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum MENU_GROUP_PAGES:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum MOBILE_PAGE_LAYOUT:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum NON_AUTOMATIC_APPLICATION_MODE:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum PLACEHOLDER_TYPE_CUSTOM_AMOUNT:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum PREDEFINED_TICKETS:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum PRICING_RULES:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum PRODUCTSETS_ALL_PRODUCTS:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum PRODUCT_SET_CUSTOM_AMOUNTS:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum SHARED_IMAGES:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum SURCHARGES:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum SURCHARGE_TAXABILITY:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum VOID_REASONS:Lcom/squareup/api/sync/CatalogFeature;

.field public static final enum WHOLE_PURCHASE:Lcom/squareup/api/sync/CatalogFeature;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 23
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/4 v1, 0x1

    const-string v2, "GIFT_CARDS"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->GIFT_CARDS:Lcom/squareup/api/sync/CatalogFeature;

    .line 28
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/4 v2, 0x2

    const-string v3, "COUPONS"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->COUPONS:Lcom/squareup/api/sync/CatalogFeature;

    .line 33
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/4 v3, 0x3

    const-string v4, "APPOINTMENTS_SERVICE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->APPOINTMENTS_SERVICE:Lcom/squareup/api/sync/CatalogFeature;

    .line 38
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/4 v4, 0x4

    const-string v5, "FLEXIBLE_PAGE_LAYOUTS"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->FLEXIBLE_PAGE_LAYOUTS:Lcom/squareup/api/sync/CatalogFeature;

    .line 45
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/4 v5, 0x5

    const-string v6, "DINING_OPTIONS"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->DINING_OPTIONS:Lcom/squareup/api/sync/CatalogFeature;

    .line 51
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/4 v6, 0x6

    const-string v7, "CONDITIONAL_TAXES"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->CONDITIONAL_TAXES:Lcom/squareup/api/sync/CatalogFeature;

    .line 56
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/4 v7, 0x7

    const-string v8, "VOID_REASONS"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->VOID_REASONS:Lcom/squareup/api/sync/CatalogFeature;

    .line 62
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/16 v8, 0x8

    const-string v9, "ADVANCED_MODIFIERS"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->ADVANCED_MODIFIERS:Lcom/squareup/api/sync/CatalogFeature;

    .line 69
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/16 v9, 0x9

    const-string v10, "CONFIGURATION"

    invoke-direct {v0, v10, v8, v9}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->CONFIGURATION:Lcom/squareup/api/sync/CatalogFeature;

    .line 74
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/16 v10, 0xa

    const-string v11, "COMP_DISCOUNTS"

    invoke-direct {v0, v11, v9, v10}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->COMP_DISCOUNTS:Lcom/squareup/api/sync/CatalogFeature;

    .line 80
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/16 v11, 0xb

    const-string v12, "PREDEFINED_TICKETS"

    invoke-direct {v0, v12, v10, v11}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->PREDEFINED_TICKETS:Lcom/squareup/api/sync/CatalogFeature;

    .line 87
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/16 v12, 0xe

    const-string v13, "PLACEHOLDER_TYPE_CUSTOM_AMOUNT"

    invoke-direct {v0, v13, v11, v12}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->PLACEHOLDER_TYPE_CUSTOM_AMOUNT:Lcom/squareup/api/sync/CatalogFeature;

    .line 92
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/16 v13, 0xf

    const-string v14, "MENU_GROUPS"

    const/16 v15, 0xc

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->MENU_GROUPS:Lcom/squareup/api/sync/CatalogFeature;

    .line 98
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/16 v14, 0x10

    const-string v15, "FUNCTION_PLACEHOLDER_TYPES"

    const/16 v11, 0xd

    invoke-direct {v0, v15, v11, v14}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->FUNCTION_PLACEHOLDER_TYPES:Lcom/squareup/api/sync/CatalogFeature;

    .line 103
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const/16 v11, 0x11

    const-string v15, "FLOOR_PLANS"

    invoke-direct {v0, v15, v12, v11}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->FLOOR_PLANS:Lcom/squareup/api/sync/CatalogFeature;

    .line 108
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v15, "MENU_GROUP_PAGES"

    const/16 v12, 0x12

    invoke-direct {v0, v15, v13, v12}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->MENU_GROUP_PAGES:Lcom/squareup/api/sync/CatalogFeature;

    .line 113
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v12, "FAVORITES_LIST"

    const/16 v15, 0x13

    invoke-direct {v0, v12, v14, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->FAVORITES_LIST:Lcom/squareup/api/sync/CatalogFeature;

    .line 120
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v12, "MENU_GROUP_MEMBERSHIPS"

    const/16 v15, 0x14

    invoke-direct {v0, v12, v11, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->MENU_GROUP_MEMBERSHIPS:Lcom/squareup/api/sync/CatalogFeature;

    .line 125
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v12, "SURCHARGES"

    const/16 v15, 0x12

    const/16 v11, 0x15

    invoke-direct {v0, v12, v15, v11}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->SURCHARGES:Lcom/squareup/api/sync/CatalogFeature;

    .line 130
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "PRICING_RULES"

    const/16 v12, 0x13

    const/16 v15, 0x16

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->PRICING_RULES:Lcom/squareup/api/sync/CatalogFeature;

    .line 136
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "SURCHARGE_TAXABILITY"

    const/16 v12, 0x14

    const/16 v15, 0x17

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->SURCHARGE_TAXABILITY:Lcom/squareup/api/sync/CatalogFeature;

    .line 141
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "FRACTIONAL_QUANTITIES"

    const/16 v12, 0x15

    const/16 v15, 0x18

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->FRACTIONAL_QUANTITIES:Lcom/squareup/api/sync/CatalogFeature;

    .line 147
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "SHARED_IMAGES"

    const/16 v12, 0x16

    const/16 v15, 0x19

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->SHARED_IMAGES:Lcom/squareup/api/sync/CatalogFeature;

    .line 153
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "ITEM_VARIATION_LIMIT"

    const/16 v12, 0x17

    const/16 v15, 0x1a

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->ITEM_VARIATION_LIMIT:Lcom/squareup/api/sync/CatalogFeature;

    .line 159
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "ITEM_OPTIONS"

    const/16 v12, 0x18

    const/16 v15, 0x1b

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->ITEM_OPTIONS:Lcom/squareup/api/sync/CatalogFeature;

    .line 164
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "PRODUCTSETS_ALL_PRODUCTS"

    const/16 v12, 0x19

    const/16 v15, 0x1c

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->PRODUCTSETS_ALL_PRODUCTS:Lcom/squareup/api/sync/CatalogFeature;

    .line 170
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "LESSER_EQUAL_VALUE"

    const/16 v12, 0x1a

    const/16 v15, 0x1d

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->LESSER_EQUAL_VALUE:Lcom/squareup/api/sync/CatalogFeature;

    .line 175
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "CUSTOM_ATTRIBUTES"

    const/16 v12, 0x1b

    const/16 v15, 0x1e

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->CUSTOM_ATTRIBUTES:Lcom/squareup/api/sync/CatalogFeature;

    .line 180
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "WHOLE_PURCHASE"

    const/16 v12, 0x1c

    const/16 v15, 0x1f

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->WHOLE_PURCHASE:Lcom/squareup/api/sync/CatalogFeature;

    .line 185
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "NON_AUTOMATIC_APPLICATION_MODE"

    const/16 v12, 0x1d

    const/16 v15, 0x20

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->NON_AUTOMATIC_APPLICATION_MODE:Lcom/squareup/api/sync/CatalogFeature;

    .line 190
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "MAX_APPLICATIONS_PER_ATTACHMENT"

    const/16 v12, 0x1e

    const/16 v15, 0x21

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->MAX_APPLICATIONS_PER_ATTACHMENT:Lcom/squareup/api/sync/CatalogFeature;

    .line 195
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "PRODUCT_SET_CUSTOM_AMOUNTS"

    const/16 v12, 0x1f

    const/16 v15, 0x22

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->PRODUCT_SET_CUSTOM_AMOUNTS:Lcom/squareup/api/sync/CatalogFeature;

    .line 200
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "ADVANCED_TAXES"

    const/16 v12, 0x20

    const/16 v15, 0x23

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->ADVANCED_TAXES:Lcom/squareup/api/sync/CatalogFeature;

    .line 206
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature;

    const-string v11, "MOBILE_PAGE_LAYOUT"

    const/16 v12, 0x21

    const/16 v15, 0x24

    invoke-direct {v0, v11, v12, v15}, Lcom/squareup/api/sync/CatalogFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->MOBILE_PAGE_LAYOUT:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v0, 0x22

    new-array v0, v0, [Lcom/squareup/api/sync/CatalogFeature;

    .line 18
    sget-object v11, Lcom/squareup/api/sync/CatalogFeature;->GIFT_CARDS:Lcom/squareup/api/sync/CatalogFeature;

    const/4 v12, 0x0

    aput-object v11, v0, v12

    sget-object v11, Lcom/squareup/api/sync/CatalogFeature;->COUPONS:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->APPOINTMENTS_SERVICE:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->FLEXIBLE_PAGE_LAYOUTS:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->DINING_OPTIONS:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->CONDITIONAL_TAXES:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->VOID_REASONS:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->ADVANCED_MODIFIERS:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->CONFIGURATION:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->COMP_DISCOUNTS:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->PREDEFINED_TICKETS:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->PLACEHOLDER_TYPE_CUSTOM_AMOUNT:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->MENU_GROUPS:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->FUNCTION_PLACEHOLDER_TYPES:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->FLOOR_PLANS:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->MENU_GROUP_PAGES:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->FAVORITES_LIST:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v14

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->MENU_GROUP_MEMBERSHIPS:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->SURCHARGES:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->PRICING_RULES:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->SURCHARGE_TAXABILITY:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->FRACTIONAL_QUANTITIES:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->SHARED_IMAGES:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->ITEM_VARIATION_LIMIT:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->ITEM_OPTIONS:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->PRODUCTSETS_ALL_PRODUCTS:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->LESSER_EQUAL_VALUE:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->CUSTOM_ATTRIBUTES:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->WHOLE_PURCHASE:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->NON_AUTOMATIC_APPLICATION_MODE:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->MAX_APPLICATIONS_PER_ATTACHMENT:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->PRODUCT_SET_CUSTOM_AMOUNTS:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->ADVANCED_TAXES:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->MOBILE_PAGE_LAYOUT:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->$VALUES:[Lcom/squareup/api/sync/CatalogFeature;

    .line 208
    new-instance v0, Lcom/squareup/api/sync/CatalogFeature$ProtoAdapter_CatalogFeature;

    invoke-direct {v0}, Lcom/squareup/api/sync/CatalogFeature$ProtoAdapter_CatalogFeature;-><init>()V

    sput-object v0, Lcom/squareup/api/sync/CatalogFeature;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 212
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 213
    iput p3, p0, Lcom/squareup/api/sync/CatalogFeature;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/sync/CatalogFeature;
    .locals 0

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 p0, 0x0

    return-object p0

    .line 254
    :pswitch_1
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->MOBILE_PAGE_LAYOUT:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 253
    :pswitch_2
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->ADVANCED_TAXES:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 252
    :pswitch_3
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->PRODUCT_SET_CUSTOM_AMOUNTS:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 251
    :pswitch_4
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->MAX_APPLICATIONS_PER_ATTACHMENT:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 250
    :pswitch_5
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->NON_AUTOMATIC_APPLICATION_MODE:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 249
    :pswitch_6
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->WHOLE_PURCHASE:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 248
    :pswitch_7
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->CUSTOM_ATTRIBUTES:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 247
    :pswitch_8
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->LESSER_EQUAL_VALUE:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 246
    :pswitch_9
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->PRODUCTSETS_ALL_PRODUCTS:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 245
    :pswitch_a
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->ITEM_OPTIONS:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 244
    :pswitch_b
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->ITEM_VARIATION_LIMIT:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 243
    :pswitch_c
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->SHARED_IMAGES:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 242
    :pswitch_d
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->FRACTIONAL_QUANTITIES:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 241
    :pswitch_e
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->SURCHARGE_TAXABILITY:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 240
    :pswitch_f
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->PRICING_RULES:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 239
    :pswitch_10
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->SURCHARGES:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 238
    :pswitch_11
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->MENU_GROUP_MEMBERSHIPS:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 237
    :pswitch_12
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->FAVORITES_LIST:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 236
    :pswitch_13
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->MENU_GROUP_PAGES:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 235
    :pswitch_14
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->FLOOR_PLANS:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 234
    :pswitch_15
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->FUNCTION_PLACEHOLDER_TYPES:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 233
    :pswitch_16
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->MENU_GROUPS:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 232
    :pswitch_17
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->PLACEHOLDER_TYPE_CUSTOM_AMOUNT:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 231
    :pswitch_18
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->PREDEFINED_TICKETS:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 230
    :pswitch_19
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->COMP_DISCOUNTS:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 229
    :pswitch_1a
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->CONFIGURATION:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 228
    :pswitch_1b
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->ADVANCED_MODIFIERS:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 227
    :pswitch_1c
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->VOID_REASONS:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 226
    :pswitch_1d
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->CONDITIONAL_TAXES:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 225
    :pswitch_1e
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->DINING_OPTIONS:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 224
    :pswitch_1f
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->FLEXIBLE_PAGE_LAYOUTS:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 223
    :pswitch_20
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->APPOINTMENTS_SERVICE:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 222
    :pswitch_21
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->COUPONS:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    .line 221
    :pswitch_22
    sget-object p0, Lcom/squareup/api/sync/CatalogFeature;->GIFT_CARDS:Lcom/squareup/api/sync/CatalogFeature;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_0
        :pswitch_0
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/sync/CatalogFeature;
    .locals 1

    .line 18
    const-class v0, Lcom/squareup/api/sync/CatalogFeature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/sync/CatalogFeature;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/sync/CatalogFeature;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/api/sync/CatalogFeature;->$VALUES:[Lcom/squareup/api/sync/CatalogFeature;

    invoke-virtual {v0}, [Lcom/squareup/api/sync/CatalogFeature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/sync/CatalogFeature;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 261
    iget v0, p0, Lcom/squareup/api/sync/CatalogFeature;->value:I

    return v0
.end method
