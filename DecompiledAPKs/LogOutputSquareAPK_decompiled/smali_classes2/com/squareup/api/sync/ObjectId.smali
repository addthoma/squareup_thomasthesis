.class public final Lcom/squareup/api/sync/ObjectId;
.super Lcom/squareup/wire/Message;
.source "ObjectId.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/sync/ObjectId$ProtoAdapter_ObjectId;,
        Lcom/squareup/api/sync/ObjectId$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/sync/ObjectId;",
        "Lcom/squareup/api/sync/ObjectId$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CUSTOMER_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_REF_TYPE:Lcom/squareup/api/items/Type;

.field private static final serialVersionUID:J


# instance fields
.field public final customer_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final ref_type:Lcom/squareup/api/items/Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Type#ADAPTER"
        tag = 0x65
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final type:Lcom/squareup/api/sync/ObjectType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectType#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/api/sync/ObjectId$ProtoAdapter_ObjectId;

    invoke-direct {v0}, Lcom/squareup/api/sync/ObjectId$ProtoAdapter_ObjectId;-><init>()V

    sput-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 35
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    sput-object v0, Lcom/squareup/api/sync/ObjectId;->DEFAULT_REF_TYPE:Lcom/squareup/api/items/Type;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Type;)V
    .locals 7

    .line 83
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/api/sync/ObjectId;-><init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Type;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Type;Lokio/ByteString;)V
    .locals 1

    .line 88
    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 89
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    .line 90
    iput-object p2, p0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    .line 91
    iput-object p3, p0, Lcom/squareup/api/sync/ObjectId;->merchant_token:Ljava/lang/String;

    .line 92
    iput-object p4, p0, Lcom/squareup/api/sync/ObjectId;->customer_token:Ljava/lang/String;

    .line 93
    iput-object p5, p0, Lcom/squareup/api/sync/ObjectId;->ref_type:Lcom/squareup/api/items/Type;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 111
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/sync/ObjectId;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 112
    :cond_1
    check-cast p1, Lcom/squareup/api/sync/ObjectId;

    .line 113
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectId;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectId;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectId;->merchant_token:Ljava/lang/String;

    .line 116
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->customer_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/sync/ObjectId;->customer_token:Ljava/lang/String;

    .line 117
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->ref_type:Lcom/squareup/api/items/Type;

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectId;->ref_type:Lcom/squareup/api/items/Type;

    .line 118
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 123
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 125
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectId;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->customer_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->ref_type:Lcom/squareup/api/items/Type;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/api/items/Type;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 131
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/sync/ObjectId$Builder;
    .locals 2

    .line 98
    new-instance v0, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    .line 99
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectId$Builder;->id:Ljava/lang/String;

    .line 100
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectId$Builder;->type:Lcom/squareup/api/sync/ObjectType;

    .line 101
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectId$Builder;->merchant_token:Ljava/lang/String;

    .line 102
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->customer_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectId$Builder;->customer_token:Ljava/lang/String;

    .line 103
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->ref_type:Lcom/squareup/api/items/Type;

    iput-object v1, v0, Lcom/squareup/api/sync/ObjectId$Builder;->ref_type:Lcom/squareup/api/items/Type;

    .line 104
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectId;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectId$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectId;->newBuilder()Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    if-eqz v1, :cond_1

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->customer_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", customer_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->customer_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->ref_type:Lcom/squareup/api/items/Type;

    if-eqz v1, :cond_4

    const-string v1, ", ref_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId;->ref_type:Lcom/squareup/api/items/Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ObjectId{"

    .line 144
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
