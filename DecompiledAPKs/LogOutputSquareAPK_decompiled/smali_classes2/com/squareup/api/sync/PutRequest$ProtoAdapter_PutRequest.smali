.class final Lcom/squareup/api/sync/PutRequest$ProtoAdapter_PutRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "PutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/PutRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PutRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/sync/PutRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 181
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/sync/PutRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/sync/PutRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 202
    new-instance v0, Lcom/squareup/api/sync/PutRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/PutRequest$Builder;-><init>()V

    .line 203
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 204
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/16 v4, 0x3e8

    if-eq v3, v4, :cond_0

    .line 210
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 208
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/PutRequest$Builder;->OBSOLETE_skip_client_state_validation(Ljava/lang/Boolean;)Lcom/squareup/api/sync/PutRequest$Builder;

    goto :goto_0

    .line 207
    :cond_1
    iget-object v3, v0, Lcom/squareup/api/sync/PutRequest$Builder;->objects:Ljava/util/List;

    sget-object v4, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 206
    :cond_2
    sget-object v3, Lcom/squareup/api/sync/WritableSessionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/WritableSessionState;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/PutRequest$Builder;->client_state(Lcom/squareup/api/sync/WritableSessionState;)Lcom/squareup/api/sync/PutRequest$Builder;

    goto :goto_0

    .line 214
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/PutRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 215
    invoke-virtual {v0}, Lcom/squareup/api/sync/PutRequest$Builder;->build()Lcom/squareup/api/sync/PutRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 179
    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/PutRequest$ProtoAdapter_PutRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/sync/PutRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/sync/PutRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 194
    sget-object v0, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/api/sync/PutRequest;->objects:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 195
    sget-object v0, Lcom/squareup/api/sync/WritableSessionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/PutRequest;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 196
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/PutRequest;->OBSOLETE_skip_client_state_validation:Ljava/lang/Boolean;

    const/16 v2, 0x3e8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 197
    invoke-virtual {p2}, Lcom/squareup/api/sync/PutRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 179
    check-cast p2, Lcom/squareup/api/sync/PutRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/sync/PutRequest$ProtoAdapter_PutRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/sync/PutRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/sync/PutRequest;)I
    .locals 4

    .line 186
    sget-object v0, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/api/sync/PutRequest;->objects:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/sync/WritableSessionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/PutRequest;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    const/4 v3, 0x1

    .line 187
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/PutRequest;->OBSOLETE_skip_client_state_validation:Ljava/lang/Boolean;

    const/16 v3, 0x3e8

    .line 188
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    invoke-virtual {p1}, Lcom/squareup/api/sync/PutRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 179
    check-cast p1, Lcom/squareup/api/sync/PutRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/PutRequest$ProtoAdapter_PutRequest;->encodedSize(Lcom/squareup/api/sync/PutRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/sync/PutRequest;)Lcom/squareup/api/sync/PutRequest;
    .locals 2

    .line 220
    invoke-virtual {p1}, Lcom/squareup/api/sync/PutRequest;->newBuilder()Lcom/squareup/api/sync/PutRequest$Builder;

    move-result-object p1

    .line 221
    iget-object v0, p1, Lcom/squareup/api/sync/PutRequest$Builder;->objects:Ljava/util/List;

    sget-object v1, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 222
    iget-object v0, p1, Lcom/squareup/api/sync/PutRequest$Builder;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/sync/WritableSessionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/PutRequest$Builder;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/WritableSessionState;

    iput-object v0, p1, Lcom/squareup/api/sync/PutRequest$Builder;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    .line 223
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/api/sync/PutRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 224
    invoke-virtual {p1}, Lcom/squareup/api/sync/PutRequest$Builder;->build()Lcom/squareup/api/sync/PutRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 179
    check-cast p1, Lcom/squareup/api/sync/PutRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/PutRequest$ProtoAdapter_PutRequest;->redact(Lcom/squareup/api/sync/PutRequest;)Lcom/squareup/api/sync/PutRequest;

    move-result-object p1

    return-object p1
.end method
