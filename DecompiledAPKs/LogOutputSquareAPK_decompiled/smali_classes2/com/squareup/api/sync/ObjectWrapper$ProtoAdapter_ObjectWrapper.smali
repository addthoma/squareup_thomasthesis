.class final Lcom/squareup/api/sync/ObjectWrapper$ProtoAdapter_ObjectWrapper;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ObjectWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/ObjectWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ObjectWrapper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/sync/ObjectWrapper;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 993
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/sync/ObjectWrapper;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/sync/ObjectWrapper;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1090
    new-instance v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;-><init>()V

    .line 1091
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1092
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_6

    const/4 v4, 0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x2

    if-eq v3, v4, :cond_4

    const/4 v4, 0x3

    if-eq v3, v4, :cond_3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_2

    const/16 v4, 0x65

    if-eq v3, v4, :cond_1

    const/16 v4, 0x771

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    packed-switch v3, :pswitch_data_1

    .line 1143
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1140
    :pswitch_0
    sget-object v3, Lcom/squareup/api/items/SurchargeFeeMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/SurchargeFeeMembership;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->surcharge_fee_membership(Lcom/squareup/api/items/SurchargeFeeMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto :goto_0

    .line 1139
    :pswitch_1
    sget-object v3, Lcom/squareup/api/items/TimePeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/TimePeriod;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->time_period(Lcom/squareup/api/items/TimePeriod;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto :goto_0

    .line 1138
    :pswitch_2
    sget-object v3, Lcom/squareup/api/items/ProductSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ProductSet;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->product_set(Lcom/squareup/api/items/ProductSet;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto :goto_0

    .line 1137
    :pswitch_3
    sget-object v3, Lcom/squareup/api/items/PricingRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/PricingRule;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->pricing_rule(Lcom/squareup/api/items/PricingRule;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto :goto_0

    .line 1136
    :pswitch_4
    sget-object v3, Lcom/squareup/api/items/Surcharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Surcharge;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->surcharge(Lcom/squareup/api/items/Surcharge;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto :goto_0

    .line 1135
    :pswitch_5
    sget-object v3, Lcom/squareup/api/items/MenuGroupMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/MenuGroupMembership;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_group_membership(Lcom/squareup/api/items/MenuGroupMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto :goto_0

    .line 1134
    :pswitch_6
    sget-object v3, Lcom/squareup/api/items/FavoritesListPosition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/FavoritesListPosition;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->favorites_list_position(Lcom/squareup/api/items/FavoritesListPosition;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto :goto_0

    .line 1133
    :pswitch_7
    sget-object v3, Lcom/squareup/api/items/FloorPlanTile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/FloorPlanTile;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->floor_plan_tile(Lcom/squareup/api/items/FloorPlanTile;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1132
    :pswitch_8
    sget-object v3, Lcom/squareup/api/items/FloorPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/FloorPlan;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->floor_plan(Lcom/squareup/api/items/FloorPlan;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1131
    :pswitch_9
    sget-object v3, Lcom/squareup/api/items/Tag;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Tag;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tag(Lcom/squareup/api/items/Tag;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1130
    :pswitch_a
    sget-object v3, Lcom/squareup/api/items/Menu;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Menu;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu(Lcom/squareup/api/items/Menu;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1129
    :pswitch_b
    sget-object v3, Lcom/squareup/api/items/VoidReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/VoidReason;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->void_reason(Lcom/squareup/api/items/VoidReason;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1128
    :pswitch_c
    sget-object v3, Lcom/squareup/api/items/TicketTemplate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/TicketTemplate;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_template(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1127
    :pswitch_d
    sget-object v3, Lcom/squareup/api/items/TicketGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/TicketGroup;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_group(Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1126
    :pswitch_e
    sget-object v3, Lcom/squareup/api/items/Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Configuration;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->configuration(Lcom/squareup/api/items/Configuration;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1125
    :pswitch_f
    sget-object v3, Lcom/squareup/api/items/TaxRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/TaxRule;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tax_rule(Lcom/squareup/api/items/TaxRule;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1124
    :pswitch_10
    sget-object v3, Lcom/squareup/api/items/DiningOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/DiningOption;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->dining_option(Lcom/squareup/api/items/DiningOption;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1123
    :pswitch_11
    sget-object v3, Lcom/squareup/api/items/OBSOLETE_TenderFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/OBSOLETE_TenderFee;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->obsolete_TenderFee(Lcom/squareup/api/items/OBSOLETE_TenderFee;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1122
    :pswitch_12
    sget-object v3, Lcom/squareup/api/items/InventoryInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/InventoryInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->inventory_info(Lcom/squareup/api/items/InventoryInfo;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1121
    :pswitch_13
    sget-object v3, Lcom/squareup/api/items/Promo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Promo;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->promo(Lcom/squareup/api/items/Promo;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1120
    :pswitch_14
    sget-object v3, Lcom/squareup/api/items/AdditionalItemImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/AdditionalItemImage;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->additional_item_image(Lcom/squareup/api/items/AdditionalItemImage;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1119
    :pswitch_15
    sget-object v3, Lcom/squareup/api/items/MarketItemSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/MarketItemSettings;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->market_item_settings(Lcom/squareup/api/items/MarketItemSettings;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1118
    :pswitch_16
    sget-object v3, Lcom/squareup/api/items/ItemModifierOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemModifierOption;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_option(Lcom/squareup/api/items/ItemModifierOption;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1117
    :pswitch_17
    sget-object v3, Lcom/squareup/api/items/ItemItemModifierListMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemItemModifierListMembership;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_item_modifier_list_membership(Lcom/squareup/api/items/ItemItemModifierListMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1116
    :pswitch_18
    sget-object v3, Lcom/squareup/api/items/ItemModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1115
    :pswitch_19
    sget-object v3, Lcom/squareup/api/items/ItemFeeMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemFeeMembership;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_fee_membership(Lcom/squareup/api/items/ItemFeeMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1114
    :pswitch_1a
    sget-object v3, Lcom/squareup/api/items/Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Discount;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->discount(Lcom/squareup/api/items/Discount;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1113
    :pswitch_1b
    sget-object v3, Lcom/squareup/api/items/Placeholder;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Placeholder;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->placeholder(Lcom/squareup/api/items/Placeholder;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1112
    :pswitch_1c
    sget-object v3, Lcom/squareup/api/items/Fee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Fee;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->fee(Lcom/squareup/api/items/Fee;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1111
    :pswitch_1d
    sget-object v3, Lcom/squareup/api/items/ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemVariation;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_variation(Lcom/squareup/api/items/ItemVariation;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1110
    :pswitch_1e
    sget-object v3, Lcom/squareup/api/items/MenuCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/MenuCategory;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1109
    :pswitch_1f
    sget-object v3, Lcom/squareup/api/items/ItemPageMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemPageMembership;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_page_membership(Lcom/squareup/api/items/ItemPageMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1108
    :pswitch_20
    sget-object v3, Lcom/squareup/api/items/Page;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Page;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->page(Lcom/squareup/api/items/Page;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1107
    :pswitch_21
    sget-object v3, Lcom/squareup/api/items/ItemImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemImage;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_image(Lcom/squareup/api/items/ItemImage;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1106
    :pswitch_22
    sget-object v3, Lcom/squareup/api/items/Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Item;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item(Lcom/squareup/api/items/Item;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1141
    :cond_0
    sget-object v3, Lcom/squareup/protos/agenda/Business;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/agenda/Business;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->business(Lcom/squareup/protos/agenda/Business;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1100
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/api/items/Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/Type;

    invoke-virtual {v0, v4}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_type(Lcom/squareup/api/items/Type;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    .line 1102
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 1097
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->deleted(Ljava/lang/Boolean;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1096
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->timestamp(Ljava/lang/Long;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1095
    :cond_4
    sget-object v3, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1094
    :cond_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    goto/16 :goto_0

    .line 1147
    :cond_6
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1148
    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3ef
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 991
    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ObjectWrapper$ProtoAdapter_ObjectWrapper;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1044
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1045
    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1046
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->timestamp:Ljava/lang/Long;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1047
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->deleted:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1048
    sget-object v0, Lcom/squareup/protos/agenda/Business;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->business:Lcom/squareup/protos/agenda/Business;

    const/16 v2, 0x771

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1049
    sget-object v0, Lcom/squareup/api/items/Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->object_type:Lcom/squareup/api/items/Type;

    const/16 v2, 0x65

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1050
    sget-object v0, Lcom/squareup/api/items/Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->item:Lcom/squareup/api/items/Item;

    const/16 v2, 0x3e9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1051
    sget-object v0, Lcom/squareup/api/items/ItemImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->item_image:Lcom/squareup/api/items/ItemImage;

    const/16 v2, 0x3ea

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1052
    sget-object v0, Lcom/squareup/api/items/Page;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->page:Lcom/squareup/api/items/Page;

    const/16 v2, 0x3eb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1053
    sget-object v0, Lcom/squareup/api/items/ItemPageMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    const/16 v2, 0x3ec

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1054
    sget-object v0, Lcom/squareup/api/items/MenuCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->menu_category:Lcom/squareup/api/items/MenuCategory;

    const/16 v2, 0x3ed

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1055
    sget-object v0, Lcom/squareup/api/items/ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->item_variation:Lcom/squareup/api/items/ItemVariation;

    const/16 v2, 0x3ef

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1056
    sget-object v0, Lcom/squareup/api/items/Fee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->fee:Lcom/squareup/api/items/Fee;

    const/16 v2, 0x3f0

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1057
    sget-object v0, Lcom/squareup/api/items/Placeholder;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->placeholder:Lcom/squareup/api/items/Placeholder;

    const/16 v2, 0x3f1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1058
    sget-object v0, Lcom/squareup/api/items/Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->discount:Lcom/squareup/api/items/Discount;

    const/16 v2, 0x3f2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1059
    sget-object v0, Lcom/squareup/api/items/ItemFeeMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    const/16 v2, 0x3f3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1060
    sget-object v0, Lcom/squareup/api/items/ItemModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    const/16 v2, 0x3f4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1061
    sget-object v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    const/16 v2, 0x3f5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1062
    sget-object v0, Lcom/squareup/api/items/ItemModifierOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    const/16 v2, 0x3f6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1063
    sget-object v0, Lcom/squareup/api/items/MarketItemSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    const/16 v2, 0x3f7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1064
    sget-object v0, Lcom/squareup/api/items/AdditionalItemImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    const/16 v2, 0x3f8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1065
    sget-object v0, Lcom/squareup/api/items/Promo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->promo:Lcom/squareup/api/items/Promo;

    const/16 v2, 0x3f9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1066
    sget-object v0, Lcom/squareup/api/items/InventoryInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    const/16 v2, 0x3fa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1067
    sget-object v0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    const/16 v2, 0x3fb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1068
    sget-object v0, Lcom/squareup/api/items/DiningOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->dining_option:Lcom/squareup/api/items/DiningOption;

    const/16 v2, 0x3fc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1069
    sget-object v0, Lcom/squareup/api/items/TaxRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->tax_rule:Lcom/squareup/api/items/TaxRule;

    const/16 v2, 0x3fd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1070
    sget-object v0, Lcom/squareup/api/items/Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->configuration:Lcom/squareup/api/items/Configuration;

    const/16 v2, 0x3fe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1071
    sget-object v0, Lcom/squareup/api/items/TicketGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    const/16 v2, 0x3ff

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1072
    sget-object v0, Lcom/squareup/api/items/TicketTemplate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    const/16 v2, 0x400

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1073
    sget-object v0, Lcom/squareup/api/items/VoidReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->void_reason:Lcom/squareup/api/items/VoidReason;

    const/16 v2, 0x401

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1074
    sget-object v0, Lcom/squareup/api/items/Menu;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->menu:Lcom/squareup/api/items/Menu;

    const/16 v2, 0x402

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1075
    sget-object v0, Lcom/squareup/api/items/Tag;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->tag:Lcom/squareup/api/items/Tag;

    const/16 v2, 0x403

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1076
    sget-object v0, Lcom/squareup/api/items/FloorPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    const/16 v2, 0x404

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1077
    sget-object v0, Lcom/squareup/api/items/FloorPlanTile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    const/16 v2, 0x405

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1078
    sget-object v0, Lcom/squareup/api/items/FavoritesListPosition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    const/16 v2, 0x406

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1079
    sget-object v0, Lcom/squareup/api/items/MenuGroupMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    const/16 v2, 0x407

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1080
    sget-object v0, Lcom/squareup/api/items/Surcharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->surcharge:Lcom/squareup/api/items/Surcharge;

    const/16 v2, 0x408

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1081
    sget-object v0, Lcom/squareup/api/items/PricingRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    const/16 v2, 0x409

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1082
    sget-object v0, Lcom/squareup/api/items/ProductSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->product_set:Lcom/squareup/api/items/ProductSet;

    const/16 v2, 0x40a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1083
    sget-object v0, Lcom/squareup/api/items/TimePeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->time_period:Lcom/squareup/api/items/TimePeriod;

    const/16 v2, 0x40b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1084
    sget-object v0, Lcom/squareup/api/items/SurchargeFeeMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectWrapper;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    const/16 v2, 0x40c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1085
    invoke-virtual {p2}, Lcom/squareup/api/sync/ObjectWrapper;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 991
    check-cast p2, Lcom/squareup/api/sync/ObjectWrapper;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/sync/ObjectWrapper$ProtoAdapter_ObjectWrapper;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/sync/ObjectWrapper;)I
    .locals 4

    .line 998
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    const/4 v3, 0x2

    .line 999
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->timestamp:Ljava/lang/Long;

    const/4 v3, 0x3

    .line 1000
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->deleted:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 1001
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/agenda/Business;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->business:Lcom/squareup/protos/agenda/Business;

    const/16 v3, 0x771

    .line 1002
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->object_type:Lcom/squareup/api/items/Type;

    const/16 v3, 0x65

    .line 1003
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->item:Lcom/squareup/api/items/Item;

    const/16 v3, 0x3e9

    .line 1004
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/ItemImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->item_image:Lcom/squareup/api/items/ItemImage;

    const/16 v3, 0x3ea

    .line 1005
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Page;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->page:Lcom/squareup/api/items/Page;

    const/16 v3, 0x3eb

    .line 1006
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/ItemPageMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    const/16 v3, 0x3ec

    .line 1007
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/MenuCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->menu_category:Lcom/squareup/api/items/MenuCategory;

    const/16 v3, 0x3ed

    .line 1008
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->item_variation:Lcom/squareup/api/items/ItemVariation;

    const/16 v3, 0x3ef

    .line 1009
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Fee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->fee:Lcom/squareup/api/items/Fee;

    const/16 v3, 0x3f0

    .line 1010
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Placeholder;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->placeholder:Lcom/squareup/api/items/Placeholder;

    const/16 v3, 0x3f1

    .line 1011
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->discount:Lcom/squareup/api/items/Discount;

    const/16 v3, 0x3f2

    .line 1012
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/ItemFeeMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    const/16 v3, 0x3f3

    .line 1013
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/ItemModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    const/16 v3, 0x3f4

    .line 1014
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/ItemItemModifierListMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    const/16 v3, 0x3f5

    .line 1015
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/ItemModifierOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    const/16 v3, 0x3f6

    .line 1016
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/MarketItemSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    const/16 v3, 0x3f7

    .line 1017
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/AdditionalItemImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    const/16 v3, 0x3f8

    .line 1018
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Promo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->promo:Lcom/squareup/api/items/Promo;

    const/16 v3, 0x3f9

    .line 1019
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/InventoryInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    const/16 v3, 0x3fa

    .line 1020
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/OBSOLETE_TenderFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    const/16 v3, 0x3fb

    .line 1021
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/DiningOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->dining_option:Lcom/squareup/api/items/DiningOption;

    const/16 v3, 0x3fc

    .line 1022
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/TaxRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->tax_rule:Lcom/squareup/api/items/TaxRule;

    const/16 v3, 0x3fd

    .line 1023
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->configuration:Lcom/squareup/api/items/Configuration;

    const/16 v3, 0x3fe

    .line 1024
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/TicketGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    const/16 v3, 0x3ff

    .line 1025
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/TicketTemplate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    const/16 v3, 0x400

    .line 1026
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/VoidReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->void_reason:Lcom/squareup/api/items/VoidReason;

    const/16 v3, 0x401

    .line 1027
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Menu;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->menu:Lcom/squareup/api/items/Menu;

    const/16 v3, 0x402

    .line 1028
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Tag;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->tag:Lcom/squareup/api/items/Tag;

    const/16 v3, 0x403

    .line 1029
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/FloorPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    const/16 v3, 0x404

    .line 1030
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/FloorPlanTile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    const/16 v3, 0x405

    .line 1031
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/FavoritesListPosition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    const/16 v3, 0x406

    .line 1032
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/MenuGroupMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    const/16 v3, 0x407

    .line 1033
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Surcharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->surcharge:Lcom/squareup/api/items/Surcharge;

    const/16 v3, 0x408

    .line 1034
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/PricingRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    const/16 v3, 0x409

    .line 1035
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/ProductSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->product_set:Lcom/squareup/api/items/ProductSet;

    const/16 v3, 0x40a

    .line 1036
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/TimePeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->time_period:Lcom/squareup/api/items/TimePeriod;

    const/16 v3, 0x40b

    .line 1037
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/SurchargeFeeMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectWrapper;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    const/16 v3, 0x40c

    .line 1038
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1039
    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 991
    check-cast p1, Lcom/squareup/api/sync/ObjectWrapper;

    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ObjectWrapper$ProtoAdapter_ObjectWrapper;->encodedSize(Lcom/squareup/api/sync/ObjectWrapper;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/api/sync/ObjectWrapper;
    .locals 2

    .line 1153
    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    .line 1154
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    .line 1155
    :cond_0
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->business:Lcom/squareup/protos/agenda/Business;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/agenda/Business;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->business:Lcom/squareup/protos/agenda/Business;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/agenda/Business;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->business:Lcom/squareup/protos/agenda/Business;

    .line 1156
    :cond_1
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item:Lcom/squareup/api/items/Item;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/api/items/Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item:Lcom/squareup/api/items/Item;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item:Lcom/squareup/api/items/Item;

    .line 1157
    :cond_2
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_image:Lcom/squareup/api/items/ItemImage;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/api/items/ItemImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_image:Lcom/squareup/api/items/ItemImage;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemImage;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_image:Lcom/squareup/api/items/ItemImage;

    .line 1158
    :cond_3
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->page:Lcom/squareup/api/items/Page;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/api/items/Page;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->page:Lcom/squareup/api/items/Page;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Page;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->page:Lcom/squareup/api/items/Page;

    .line 1159
    :cond_4
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/api/items/ItemPageMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemPageMembership;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_page_membership:Lcom/squareup/api/items/ItemPageMembership;

    .line 1160
    :cond_5
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_category:Lcom/squareup/api/items/MenuCategory;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/api/items/MenuCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_category:Lcom/squareup/api/items/MenuCategory;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MenuCategory;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_category:Lcom/squareup/api/items/MenuCategory;

    .line 1161
    :cond_6
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_variation:Lcom/squareup/api/items/ItemVariation;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/api/items/ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_variation:Lcom/squareup/api/items/ItemVariation;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_variation:Lcom/squareup/api/items/ItemVariation;

    .line 1162
    :cond_7
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->fee:Lcom/squareup/api/items/Fee;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/api/items/Fee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->fee:Lcom/squareup/api/items/Fee;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->fee:Lcom/squareup/api/items/Fee;

    .line 1163
    :cond_8
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->placeholder:Lcom/squareup/api/items/Placeholder;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/api/items/Placeholder;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->placeholder:Lcom/squareup/api/items/Placeholder;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Placeholder;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->placeholder:Lcom/squareup/api/items/Placeholder;

    .line 1164
    :cond_9
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->discount:Lcom/squareup/api/items/Discount;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/api/items/Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->discount:Lcom/squareup/api/items/Discount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->discount:Lcom/squareup/api/items/Discount;

    .line 1165
    :cond_a
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/api/items/ItemFeeMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemFeeMembership;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_fee_membership:Lcom/squareup/api/items/ItemFeeMembership;

    .line 1166
    :cond_b
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/api/items/ItemModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierList;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_list:Lcom/squareup/api/items/ItemModifierList;

    .line 1167
    :cond_c
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemItemModifierListMembership;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_item_modifier_list_membership:Lcom/squareup/api/items/ItemItemModifierListMembership;

    .line 1168
    :cond_d
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/api/items/ItemModifierOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierOption;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    .line 1169
    :cond_e
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/squareup/api/items/MarketItemSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MarketItemSettings;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->market_item_settings:Lcom/squareup/api/items/MarketItemSettings;

    .line 1170
    :cond_f
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    if-eqz v0, :cond_10

    sget-object v0, Lcom/squareup/api/items/AdditionalItemImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/AdditionalItemImage;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->additional_item_image:Lcom/squareup/api/items/AdditionalItemImage;

    .line 1171
    :cond_10
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->promo:Lcom/squareup/api/items/Promo;

    if-eqz v0, :cond_11

    sget-object v0, Lcom/squareup/api/items/Promo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->promo:Lcom/squareup/api/items/Promo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Promo;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->promo:Lcom/squareup/api/items/Promo;

    .line 1172
    :cond_11
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    if-eqz v0, :cond_12

    sget-object v0, Lcom/squareup/api/items/InventoryInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/InventoryInfo;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->inventory_info:Lcom/squareup/api/items/InventoryInfo;

    .line 1173
    :cond_12
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    if-eqz v0, :cond_13

    sget-object v0, Lcom/squareup/api/items/OBSOLETE_TenderFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/OBSOLETE_TenderFee;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->obsolete_TenderFee:Lcom/squareup/api/items/OBSOLETE_TenderFee;

    .line 1174
    :cond_13
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->dining_option:Lcom/squareup/api/items/DiningOption;

    if-eqz v0, :cond_14

    sget-object v0, Lcom/squareup/api/items/DiningOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->dining_option:Lcom/squareup/api/items/DiningOption;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/DiningOption;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->dining_option:Lcom/squareup/api/items/DiningOption;

    .line 1175
    :cond_14
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tax_rule:Lcom/squareup/api/items/TaxRule;

    if-eqz v0, :cond_15

    sget-object v0, Lcom/squareup/api/items/TaxRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tax_rule:Lcom/squareup/api/items/TaxRule;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TaxRule;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tax_rule:Lcom/squareup/api/items/TaxRule;

    .line 1176
    :cond_15
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->configuration:Lcom/squareup/api/items/Configuration;

    if-eqz v0, :cond_16

    sget-object v0, Lcom/squareup/api/items/Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->configuration:Lcom/squareup/api/items/Configuration;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Configuration;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->configuration:Lcom/squareup/api/items/Configuration;

    .line 1177
    :cond_16
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    if-eqz v0, :cond_17

    sget-object v0, Lcom/squareup/api/items/TicketGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketGroup;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    .line 1178
    :cond_17
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    if-eqz v0, :cond_18

    sget-object v0, Lcom/squareup/api/items/TicketTemplate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketTemplate;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    .line 1179
    :cond_18
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->void_reason:Lcom/squareup/api/items/VoidReason;

    if-eqz v0, :cond_19

    sget-object v0, Lcom/squareup/api/items/VoidReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->void_reason:Lcom/squareup/api/items/VoidReason;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/VoidReason;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->void_reason:Lcom/squareup/api/items/VoidReason;

    .line 1180
    :cond_19
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu:Lcom/squareup/api/items/Menu;

    if-eqz v0, :cond_1a

    sget-object v0, Lcom/squareup/api/items/Menu;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu:Lcom/squareup/api/items/Menu;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Menu;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu:Lcom/squareup/api/items/Menu;

    .line 1181
    :cond_1a
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tag:Lcom/squareup/api/items/Tag;

    if-eqz v0, :cond_1b

    sget-object v0, Lcom/squareup/api/items/Tag;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tag:Lcom/squareup/api/items/Tag;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Tag;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tag:Lcom/squareup/api/items/Tag;

    .line 1182
    :cond_1b
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    if-eqz v0, :cond_1c

    sget-object v0, Lcom/squareup/api/items/FloorPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FloorPlan;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->floor_plan:Lcom/squareup/api/items/FloorPlan;

    .line 1183
    :cond_1c
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    if-eqz v0, :cond_1d

    sget-object v0, Lcom/squareup/api/items/FloorPlanTile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FloorPlanTile;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->floor_plan_tile:Lcom/squareup/api/items/FloorPlanTile;

    .line 1184
    :cond_1d
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    if-eqz v0, :cond_1e

    sget-object v0, Lcom/squareup/api/items/FavoritesListPosition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/FavoritesListPosition;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->favorites_list_position:Lcom/squareup/api/items/FavoritesListPosition;

    .line 1185
    :cond_1e
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    if-eqz v0, :cond_1f

    sget-object v0, Lcom/squareup/api/items/MenuGroupMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MenuGroupMembership;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_group_membership:Lcom/squareup/api/items/MenuGroupMembership;

    .line 1186
    :cond_1f
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->surcharge:Lcom/squareup/api/items/Surcharge;

    if-eqz v0, :cond_20

    sget-object v0, Lcom/squareup/api/items/Surcharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->surcharge:Lcom/squareup/api/items/Surcharge;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Surcharge;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->surcharge:Lcom/squareup/api/items/Surcharge;

    .line 1187
    :cond_20
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    if-eqz v0, :cond_21

    sget-object v0, Lcom/squareup/api/items/PricingRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PricingRule;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->pricing_rule:Lcom/squareup/api/items/PricingRule;

    .line 1188
    :cond_21
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->product_set:Lcom/squareup/api/items/ProductSet;

    if-eqz v0, :cond_22

    sget-object v0, Lcom/squareup/api/items/ProductSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->product_set:Lcom/squareup/api/items/ProductSet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ProductSet;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->product_set:Lcom/squareup/api/items/ProductSet;

    .line 1189
    :cond_22
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->time_period:Lcom/squareup/api/items/TimePeriod;

    if-eqz v0, :cond_23

    sget-object v0, Lcom/squareup/api/items/TimePeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->time_period:Lcom/squareup/api/items/TimePeriod;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TimePeriod;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->time_period:Lcom/squareup/api/items/TimePeriod;

    .line 1190
    :cond_23
    iget-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    if-eqz v0, :cond_24

    sget-object v0, Lcom/squareup/api/items/SurchargeFeeMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/SurchargeFeeMembership;

    iput-object v0, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->surcharge_fee_membership:Lcom/squareup/api/items/SurchargeFeeMembership;

    .line 1191
    :cond_24
    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1192
    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 991
    check-cast p1, Lcom/squareup/api/sync/ObjectWrapper;

    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ObjectWrapper$ProtoAdapter_ObjectWrapper;->redact(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    return-object p1
.end method
