.class public final enum Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;
.super Ljava/lang/Enum;
.source "ReferenceDescriptor.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/ReferenceDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OnDeleteType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType$ProtoAdapter_OnDeleteType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BLOCK:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

.field public static final enum CASCADE:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

.field public static final enum EXTERNAL:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

.field public static final enum SET_NULL:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 146
    new-instance v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "CASCADE"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->CASCADE:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    .line 148
    new-instance v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    const/4 v3, 0x2

    const-string v4, "SET_NULL"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->SET_NULL:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    .line 150
    new-instance v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    const/4 v4, 0x3

    const-string v5, "BLOCK"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->BLOCK:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    .line 155
    new-instance v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    const/4 v5, 0x4

    const-string v6, "EXTERNAL"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->EXTERNAL:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    new-array v0, v5, [Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    .line 145
    sget-object v5, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->CASCADE:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->SET_NULL:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->BLOCK:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->EXTERNAL:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->$VALUES:[Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    .line 157
    new-instance v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType$ProtoAdapter_OnDeleteType;

    invoke-direct {v0}, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType$ProtoAdapter_OnDeleteType;-><init>()V

    sput-object v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 161
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 162
    iput p3, p0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 173
    :cond_0
    sget-object p0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->EXTERNAL:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    return-object p0

    .line 172
    :cond_1
    sget-object p0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->BLOCK:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    return-object p0

    .line 171
    :cond_2
    sget-object p0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->SET_NULL:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    return-object p0

    .line 170
    :cond_3
    sget-object p0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->CASCADE:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;
    .locals 1

    .line 145
    const-class v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;
    .locals 1

    .line 145
    sget-object v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->$VALUES:[Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    invoke-virtual {v0}, [Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 180
    iget v0, p0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->value:I

    return v0
.end method
