.class public final Lcom/squareup/api/sync/VisibleTypeSet;
.super Lcom/squareup/wire/Message;
.source "VisibleTypeSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/sync/VisibleTypeSet$ProtoAdapter_VisibleTypeSet;,
        Lcom/squareup/api/sync/VisibleTypeSet$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/sync/VisibleTypeSet;",
        "Lcom/squareup/api/sync/VisibleTypeSet$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/sync/VisibleTypeSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALL_TYPES:Ljava/lang/Boolean;

.field public static final DEFAULT_ITEMS_VISIBLE_TYPE_SET:Lcom/squareup/api/items/ItemsVisibleTypeSet;

.field private static final serialVersionUID:J


# instance fields
.field public final all_types:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final items_visible_type_set:Lcom/squareup/api/items/ItemsVisibleTypeSet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemsVisibleTypeSet#ADAPTER"
        tag = 0xa
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/api/sync/VisibleTypeSet$ProtoAdapter_VisibleTypeSet;

    invoke-direct {v0}, Lcom/squareup/api/sync/VisibleTypeSet$ProtoAdapter_VisibleTypeSet;-><init>()V

    sput-object v0, Lcom/squareup/api/sync/VisibleTypeSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 30
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/sync/VisibleTypeSet;->DEFAULT_ALL_TYPES:Ljava/lang/Boolean;

    .line 32
    sget-object v0, Lcom/squareup/api/items/ItemsVisibleTypeSet;->LEGACY:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    sput-object v0, Lcom/squareup/api/sync/VisibleTypeSet;->DEFAULT_ITEMS_VISIBLE_TYPE_SET:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/api/items/ItemsVisibleTypeSet;)V
    .locals 1

    .line 53
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/api/sync/VisibleTypeSet;-><init>(Ljava/lang/Boolean;Lcom/squareup/api/items/ItemsVisibleTypeSet;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/api/items/ItemsVisibleTypeSet;Lokio/ByteString;)V
    .locals 1

    .line 58
    sget-object v0, Lcom/squareup/api/sync/VisibleTypeSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 59
    iput-object p1, p0, Lcom/squareup/api/sync/VisibleTypeSet;->all_types:Ljava/lang/Boolean;

    .line 60
    iput-object p2, p0, Lcom/squareup/api/sync/VisibleTypeSet;->items_visible_type_set:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 75
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/sync/VisibleTypeSet;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 76
    :cond_1
    check-cast p1, Lcom/squareup/api/sync/VisibleTypeSet;

    .line 77
    invoke-virtual {p0}, Lcom/squareup/api/sync/VisibleTypeSet;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/sync/VisibleTypeSet;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/VisibleTypeSet;->all_types:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/sync/VisibleTypeSet;->all_types:Ljava/lang/Boolean;

    .line 78
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/VisibleTypeSet;->items_visible_type_set:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    iget-object p1, p1, Lcom/squareup/api/sync/VisibleTypeSet;->items_visible_type_set:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    .line 79
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 84
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 86
    invoke-virtual {p0}, Lcom/squareup/api/sync/VisibleTypeSet;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 87
    iget-object v1, p0, Lcom/squareup/api/sync/VisibleTypeSet;->all_types:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 88
    iget-object v1, p0, Lcom/squareup/api/sync/VisibleTypeSet;->items_visible_type_set:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemsVisibleTypeSet;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 89
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/sync/VisibleTypeSet$Builder;
    .locals 2

    .line 65
    new-instance v0, Lcom/squareup/api/sync/VisibleTypeSet$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/VisibleTypeSet$Builder;-><init>()V

    .line 66
    iget-object v1, p0, Lcom/squareup/api/sync/VisibleTypeSet;->all_types:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->all_types:Ljava/lang/Boolean;

    .line 67
    iget-object v1, p0, Lcom/squareup/api/sync/VisibleTypeSet;->items_visible_type_set:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    iput-object v1, v0, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->items_visible_type_set:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    .line 68
    invoke-virtual {p0}, Lcom/squareup/api/sync/VisibleTypeSet;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/api/sync/VisibleTypeSet;->newBuilder()Lcom/squareup/api/sync/VisibleTypeSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    iget-object v1, p0, Lcom/squareup/api/sync/VisibleTypeSet;->all_types:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", all_types="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/VisibleTypeSet;->all_types:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 98
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/sync/VisibleTypeSet;->items_visible_type_set:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    if-eqz v1, :cond_1

    const-string v1, ", items_visible_type_set="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/VisibleTypeSet;->items_visible_type_set:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "VisibleTypeSet{"

    .line 99
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
