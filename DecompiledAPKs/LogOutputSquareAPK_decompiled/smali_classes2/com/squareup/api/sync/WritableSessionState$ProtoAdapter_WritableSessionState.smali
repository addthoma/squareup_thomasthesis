.class final Lcom/squareup/api/sync/WritableSessionState$ProtoAdapter_WritableSessionState;
.super Lcom/squareup/wire/ProtoAdapter;
.source "WritableSessionState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/WritableSessionState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_WritableSessionState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/sync/WritableSessionState;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 132
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/sync/WritableSessionState;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/sync/WritableSessionState;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 151
    new-instance v0, Lcom/squareup/api/sync/WritableSessionState$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/WritableSessionState$Builder;-><init>()V

    .line 152
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 153
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 158
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 156
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/WritableSessionState$Builder;->seq(Ljava/lang/Long;)Lcom/squareup/api/sync/WritableSessionState$Builder;

    goto :goto_0

    .line 155
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/WritableSessionState$Builder;->session_id(Ljava/lang/Long;)Lcom/squareup/api/sync/WritableSessionState$Builder;

    goto :goto_0

    .line 162
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/WritableSessionState$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 163
    invoke-virtual {v0}, Lcom/squareup/api/sync/WritableSessionState$Builder;->build()Lcom/squareup/api/sync/WritableSessionState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 130
    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/WritableSessionState$ProtoAdapter_WritableSessionState;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/sync/WritableSessionState;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/sync/WritableSessionState;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 144
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/WritableSessionState;->session_id:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 145
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/WritableSessionState;->seq:Ljava/lang/Long;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 146
    invoke-virtual {p2}, Lcom/squareup/api/sync/WritableSessionState;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 130
    check-cast p2, Lcom/squareup/api/sync/WritableSessionState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/sync/WritableSessionState$ProtoAdapter_WritableSessionState;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/sync/WritableSessionState;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/sync/WritableSessionState;)I
    .locals 4

    .line 137
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/WritableSessionState;->session_id:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/WritableSessionState;->seq:Ljava/lang/Long;

    const/4 v3, 0x2

    .line 138
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    invoke-virtual {p1}, Lcom/squareup/api/sync/WritableSessionState;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 130
    check-cast p1, Lcom/squareup/api/sync/WritableSessionState;

    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/WritableSessionState$ProtoAdapter_WritableSessionState;->encodedSize(Lcom/squareup/api/sync/WritableSessionState;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/sync/WritableSessionState;)Lcom/squareup/api/sync/WritableSessionState;
    .locals 0

    .line 168
    invoke-virtual {p1}, Lcom/squareup/api/sync/WritableSessionState;->newBuilder()Lcom/squareup/api/sync/WritableSessionState$Builder;

    move-result-object p1

    .line 169
    invoke-virtual {p1}, Lcom/squareup/api/sync/WritableSessionState$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 170
    invoke-virtual {p1}, Lcom/squareup/api/sync/WritableSessionState$Builder;->build()Lcom/squareup/api/sync/WritableSessionState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 130
    check-cast p1, Lcom/squareup/api/sync/WritableSessionState;

    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/WritableSessionState$ProtoAdapter_WritableSessionState;->redact(Lcom/squareup/api/sync/WritableSessionState;)Lcom/squareup/api/sync/WritableSessionState;

    move-result-object p1

    return-object p1
.end method
