.class final Lcom/squareup/api/sync/ReferenceDescriptor$ProtoAdapter_ReferenceDescriptor;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReferenceDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/ReferenceDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReferenceDescriptor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/sync/ReferenceDescriptor;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 197
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/sync/ReferenceDescriptor;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/sync/ReferenceDescriptor;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 218
    new-instance v0, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;-><init>()V

    .line 219
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 220
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x7

    if-eq v3, v4, :cond_1

    const/16 v4, 0x8

    if-eq v3, v4, :cond_0

    .line 233
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 231
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->external(Ljava/lang/Boolean;)Lcom/squareup/api/sync/ReferenceDescriptor$Builder;

    goto :goto_0

    .line 225
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    invoke-virtual {v0, v4}, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->on_delete(Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;)Lcom/squareup/api/sync/ReferenceDescriptor$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 227
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 222
    :cond_2
    iget-object v3, v0, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->type:Ljava/util/List;

    sget-object v4, Lcom/squareup/api/sync/ObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 237
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 238
    invoke-virtual {v0}, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->build()Lcom/squareup/api/sync/ReferenceDescriptor;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 195
    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ReferenceDescriptor$ProtoAdapter_ReferenceDescriptor;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/sync/ReferenceDescriptor;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/sync/ReferenceDescriptor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 210
    sget-object v0, Lcom/squareup/api/sync/ObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/api/sync/ReferenceDescriptor;->type:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 211
    sget-object v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ReferenceDescriptor;->on_delete:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 212
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ReferenceDescriptor;->external:Ljava/lang/Boolean;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 213
    invoke-virtual {p2}, Lcom/squareup/api/sync/ReferenceDescriptor;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 195
    check-cast p2, Lcom/squareup/api/sync/ReferenceDescriptor;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/sync/ReferenceDescriptor$ProtoAdapter_ReferenceDescriptor;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/sync/ReferenceDescriptor;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/sync/ReferenceDescriptor;)I
    .locals 4

    .line 202
    sget-object v0, Lcom/squareup/api/sync/ObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/api/sync/ReferenceDescriptor;->type:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ReferenceDescriptor;->on_delete:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    const/4 v3, 0x7

    .line 203
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ReferenceDescriptor;->external:Ljava/lang/Boolean;

    const/16 v3, 0x8

    .line 204
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    invoke-virtual {p1}, Lcom/squareup/api/sync/ReferenceDescriptor;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 195
    check-cast p1, Lcom/squareup/api/sync/ReferenceDescriptor;

    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ReferenceDescriptor$ProtoAdapter_ReferenceDescriptor;->encodedSize(Lcom/squareup/api/sync/ReferenceDescriptor;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/sync/ReferenceDescriptor;)Lcom/squareup/api/sync/ReferenceDescriptor;
    .locals 2

    .line 243
    invoke-virtual {p1}, Lcom/squareup/api/sync/ReferenceDescriptor;->newBuilder()Lcom/squareup/api/sync/ReferenceDescriptor$Builder;

    move-result-object p1

    .line 244
    iget-object v0, p1, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->type:Ljava/util/List;

    sget-object v1, Lcom/squareup/api/sync/ObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 245
    invoke-virtual {p1}, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 246
    invoke-virtual {p1}, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->build()Lcom/squareup/api/sync/ReferenceDescriptor;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 195
    check-cast p1, Lcom/squareup/api/sync/ReferenceDescriptor;

    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ReferenceDescriptor$ProtoAdapter_ReferenceDescriptor;->redact(Lcom/squareup/api/sync/ReferenceDescriptor;)Lcom/squareup/api/sync/ReferenceDescriptor;

    move-result-object p1

    return-object p1
.end method
