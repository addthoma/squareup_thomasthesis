.class public Lcom/squareup/api/NewTransactionEvent;
.super Lcom/squareup/api/RegisterApiEvent;
.source "NewTransactionEvent.java"


# instance fields
.field public final action:Ljava/lang/String;

.field public final amount:Ljava/lang/Long;

.field public final api_version:Ljava/lang/String;

.field public final browser_application_id:Ljava/lang/String;

.field public final card_from_reader_enabled:Z

.field public final card_on_file_enabled:Z

.field public final cash_enabled:Z

.field public final cold_start:Z

.field public final currency_code:Ljava/lang/String;

.field public final customer_id:Ljava/lang/String;

.field public final delay_capture_enabled:Z

.field public final keyed_in_card_enabled:Z

.field public final location_id:Ljava/lang/String;

.field public final note:Ljava/lang/String;

.field public final other_enabled:Z

.field public final package_name:Ljava/lang/String;

.field public final request_metadata:Ljava/lang/String;

.field public final sdk_version:Ljava/lang/String;

.field public final sequenceUuid:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "api_sequence_uuid"
    .end annotation
.end field

.field public final show_custom_tip_field:Ljava/lang/Boolean;

.field public final show_separate_tip_screen:Ljava/lang/Boolean;

.field public final skip_receipt:Z

.field public final source:Ljava/lang/String;

.field public final startup_duration_millis:J

.field public final timeout:J

.field public final tip_enabled:Ljava/lang/Boolean;

.field public final used_deprecated_params:Ljava/lang/String;

.field public final web_callback_uri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Landroid/content/Intent;Ljava/lang/String;ZJJLcom/squareup/api/RequestParams;)V
    .locals 6

    .line 60
    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->API_NEW_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    iget-object v0, p9, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    iget-object v3, v0, Lcom/squareup/api/ClientInfo;->clientId:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p7

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/RegisterApiEvent;-><init>(Lcom/squareup/util/Clock;Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;J)V

    .line 62
    iput-object p3, p0, Lcom/squareup/api/NewTransactionEvent;->sequenceUuid:Ljava/lang/String;

    .line 63
    iput-boolean p4, p0, Lcom/squareup/api/NewTransactionEvent;->cold_start:Z

    .line 64
    iput-wide p5, p0, Lcom/squareup/api/NewTransactionEvent;->startup_duration_millis:J

    .line 65
    iget-object p1, p9, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    if-eqz p1, :cond_0

    iget-object p1, p9, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    iget-object p1, p1, Lcom/squareup/api/ApiVersion;->versionString:Ljava/lang/String;

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Unknown: "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "com.squareup.pos.API_VERSION"

    .line 67
    invoke-virtual {p2, p3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/api/NewTransactionEvent;->api_version:Ljava/lang/String;

    .line 68
    iget-object p1, p9, Lcom/squareup/api/RequestParams;->locationId:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/api/NewTransactionEvent;->location_id:Ljava/lang/String;

    .line 69
    iget-object p1, p9, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    iget-object p1, p1, Lcom/squareup/api/ClientInfo;->packageName:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/api/NewTransactionEvent;->package_name:Ljava/lang/String;

    .line 70
    iget-object p1, p9, Lcom/squareup/api/RequestParams;->state:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/api/NewTransactionEvent;->request_metadata:Ljava/lang/String;

    .line 71
    iget-object p1, p9, Lcom/squareup/api/RequestParams;->sdkVersion:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/api/NewTransactionEvent;->sdk_version:Ljava/lang/String;

    .line 72
    invoke-virtual {p9}, Lcom/squareup/api/RequestParams;->isWebRequest()Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "browser"

    goto :goto_1

    :cond_1
    const-string p1, "native"

    :goto_1
    iput-object p1, p0, Lcom/squareup/api/NewTransactionEvent;->source:Ljava/lang/String;

    .line 73
    iget-object p1, p9, Lcom/squareup/api/RequestParams;->transactionParams:Lcom/squareup/api/TransactionParams;

    .line 74
    iget-object p3, p1, Lcom/squareup/api/TransactionParams;->note:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/api/NewTransactionEvent;->note:Ljava/lang/String;

    .line 75
    iget-object p3, p9, Lcom/squareup/api/RequestParams;->action:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/api/NewTransactionEvent;->action:Ljava/lang/String;

    .line 76
    iget-boolean p3, p1, Lcom/squareup/api/TransactionParams;->skipReceipt:Z

    iput-boolean p3, p0, Lcom/squareup/api/NewTransactionEvent;->skip_receipt:Z

    .line 77
    iget-object p3, p1, Lcom/squareup/api/TransactionParams;->amount:Ljava/lang/Long;

    iput-object p3, p0, Lcom/squareup/api/NewTransactionEvent;->amount:Ljava/lang/Long;

    .line 78
    iget-object p3, p9, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    iget-object p3, p3, Lcom/squareup/api/ClientInfo;->browserApplicationId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/api/NewTransactionEvent;->browser_application_id:Ljava/lang/String;

    .line 79
    iget-object p3, p1, Lcom/squareup/api/TransactionParams;->tenderTypes:Ljava/util/Set;

    sget-object p4, Lcom/squareup/api/ApiTenderType;->POS_API_CARD_PRESENT:Lcom/squareup/api/ApiTenderType;

    invoke-interface {p3, p4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p3

    iput-boolean p3, p0, Lcom/squareup/api/NewTransactionEvent;->card_from_reader_enabled:Z

    .line 80
    iget-object p3, p1, Lcom/squareup/api/TransactionParams;->tenderTypes:Ljava/util/Set;

    sget-object p4, Lcom/squareup/api/ApiTenderType;->CARD_ON_FILE:Lcom/squareup/api/ApiTenderType;

    invoke-interface {p3, p4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p3

    iput-boolean p3, p0, Lcom/squareup/api/NewTransactionEvent;->card_on_file_enabled:Z

    .line 81
    iget-object p3, p1, Lcom/squareup/api/TransactionParams;->tenderTypes:Ljava/util/Set;

    sget-object p4, Lcom/squareup/api/ApiTenderType;->CASH:Lcom/squareup/api/ApiTenderType;

    invoke-interface {p3, p4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p3

    iput-boolean p3, p0, Lcom/squareup/api/NewTransactionEvent;->cash_enabled:Z

    .line 82
    iget-object p3, p1, Lcom/squareup/api/TransactionParams;->currencyCode:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/api/NewTransactionEvent;->currency_code:Ljava/lang/String;

    .line 83
    iget-object p3, p1, Lcom/squareup/api/TransactionParams;->customerId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/api/NewTransactionEvent;->customer_id:Ljava/lang/String;

    .line 84
    iget-boolean p3, p1, Lcom/squareup/api/TransactionParams;->delayCapture:Z

    iput-boolean p3, p0, Lcom/squareup/api/NewTransactionEvent;->delay_capture_enabled:Z

    .line 85
    iget-object p3, p1, Lcom/squareup/api/TransactionParams;->tenderTypes:Ljava/util/Set;

    sget-object p4, Lcom/squareup/api/ApiTenderType;->KEYED_IN_CARD:Lcom/squareup/api/ApiTenderType;

    invoke-interface {p3, p4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p3

    iput-boolean p3, p0, Lcom/squareup/api/NewTransactionEvent;->keyed_in_card_enabled:Z

    .line 86
    iget-object p3, p1, Lcom/squareup/api/TransactionParams;->tenderTypes:Ljava/util/Set;

    sget-object p4, Lcom/squareup/api/ApiTenderType;->OTHER:Lcom/squareup/api/ApiTenderType;

    invoke-interface {p3, p4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p3

    iput-boolean p3, p0, Lcom/squareup/api/NewTransactionEvent;->other_enabled:Z

    .line 87
    iget-boolean p3, p1, Lcom/squareup/api/TransactionParams;->showCustomTip:Z

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    iput-object p3, p0, Lcom/squareup/api/NewTransactionEvent;->show_custom_tip_field:Ljava/lang/Boolean;

    .line 88
    iget-boolean p3, p1, Lcom/squareup/api/TransactionParams;->showSeparateTipScreen:Z

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    iput-object p3, p0, Lcom/squareup/api/NewTransactionEvent;->show_separate_tip_screen:Ljava/lang/Boolean;

    .line 89
    iget-wide p3, p1, Lcom/squareup/api/TransactionParams;->timeout:J

    iput-wide p3, p0, Lcom/squareup/api/NewTransactionEvent;->timeout:J

    .line 90
    iget-boolean p1, p1, Lcom/squareup/api/TransactionParams;->tippingEnabled:Z

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/NewTransactionEvent;->tip_enabled:Ljava/lang/Boolean;

    const-string p1, "com.squareup.pos.USED_DEPRECATED_PARAMETERS"

    .line 91
    invoke-virtual {p2, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/NewTransactionEvent;->used_deprecated_params:Ljava/lang/String;

    .line 92
    iget-object p1, p9, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    iget-object p1, p1, Lcom/squareup/api/ClientInfo;->webCallbackUri:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/api/NewTransactionEvent;->web_callback_uri:Ljava/lang/String;

    return-void
.end method
