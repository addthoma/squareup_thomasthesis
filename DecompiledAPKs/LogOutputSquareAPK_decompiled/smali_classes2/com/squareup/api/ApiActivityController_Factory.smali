.class public final Lcom/squareup/api/ApiActivityController_Factory;
.super Ljava/lang/Object;
.source "ApiActivityController_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/api/ApiActivityController;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ActivityListener;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final apiValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final appDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/AppDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final isReaderSdkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final uuidGeneratorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/UUIDGenerator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/UUIDGenerator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/AppDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ActivityListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/api/ApiActivityController_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p2, p0, Lcom/squareup/api/ApiActivityController_Factory;->uuidGeneratorProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p3, p0, Lcom/squareup/api/ApiActivityController_Factory;->appDelegateProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p4, p0, Lcom/squareup/api/ApiActivityController_Factory;->apiValidatorProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p5, p0, Lcom/squareup/api/ApiActivityController_Factory;->activityListenerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p6, p0, Lcom/squareup/api/ApiActivityController_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p7, p0, Lcom/squareup/api/ApiActivityController_Factory;->isReaderSdkProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p8, p0, Lcom/squareup/api/ApiActivityController_Factory;->busProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/api/ApiActivityController_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/UUIDGenerator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/AppDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ActivityListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;)",
            "Lcom/squareup/api/ApiActivityController_Factory;"
        }
    .end annotation

    .line 63
    new-instance v9, Lcom/squareup/api/ApiActivityController_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/api/ApiActivityController_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/log/UUIDGenerator;Lcom/squareup/AppDelegate;Lcom/squareup/api/ApiValidator;Lcom/squareup/ActivityListener;Lcom/squareup/util/Clock;ZLcom/squareup/badbus/BadBus;)Lcom/squareup/api/ApiActivityController;
    .locals 10

    .line 69
    new-instance v9, Lcom/squareup/api/ApiActivityController;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/api/ApiActivityController;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/log/UUIDGenerator;Lcom/squareup/AppDelegate;Lcom/squareup/api/ApiValidator;Lcom/squareup/ActivityListener;Lcom/squareup/util/Clock;ZLcom/squareup/badbus/BadBus;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/api/ApiActivityController;
    .locals 9

    .line 55
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/api/ApiActivityController_Factory;->uuidGeneratorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/log/UUIDGenerator;

    iget-object v0, p0, Lcom/squareup/api/ApiActivityController_Factory;->appDelegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/AppDelegate;

    iget-object v0, p0, Lcom/squareup/api/ApiActivityController_Factory;->apiValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/api/ApiValidator;

    iget-object v0, p0, Lcom/squareup/api/ApiActivityController_Factory;->activityListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ActivityListener;

    iget-object v0, p0, Lcom/squareup/api/ApiActivityController_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/api/ApiActivityController_Factory;->isReaderSdkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    iget-object v0, p0, Lcom/squareup/api/ApiActivityController_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/badbus/BadBus;

    invoke-static/range {v1 .. v8}, Lcom/squareup/api/ApiActivityController_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/log/UUIDGenerator;Lcom/squareup/AppDelegate;Lcom/squareup/api/ApiValidator;Lcom/squareup/ActivityListener;Lcom/squareup/util/Clock;ZLcom/squareup/badbus/BadBus;)Lcom/squareup/api/ApiActivityController;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/api/ApiActivityController_Factory;->get()Lcom/squareup/api/ApiActivityController;

    move-result-object v0

    return-object v0
.end method
