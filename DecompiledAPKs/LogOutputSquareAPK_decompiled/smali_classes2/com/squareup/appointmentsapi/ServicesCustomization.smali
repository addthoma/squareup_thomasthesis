.class public interface abstract Lcom/squareup/appointmentsapi/ServicesCustomization;
.super Ljava/lang/Object;
.source "ServicesCustomization.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003H&J\u000e\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0003H&J\u0018\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH&\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/appointmentsapi/ServicesCustomization;",
        "",
        "allStaffInfo",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/appointmentsapi/StaffInfo;",
        "businessInfo",
        "Lcom/squareup/appointmentsapi/BusinessInfo;",
        "maybeShowGapTimeEducation",
        "",
        "scope",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "useCardOverSheet",
        "",
        "appointments_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract allStaffInfo()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/appointmentsapi/StaffInfo;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract businessInfo()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/appointmentsapi/BusinessInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract maybeShowGapTimeEducation(Lcom/squareup/ui/main/RegisterTreeKey;Z)V
.end method
