.class public final Lcom/squareup/salesreport/SalesReportCoordinatorKt;
.super Ljava/lang/Object;
.source "SalesReportCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028CX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0018\u0010\u0005\u001a\u00020\u0001*\u00020\u00068CX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\"\u0018\u0010\t\u001a\u00020\u0001*\u00020\n8CX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000c\"\u0018\u0010\r\u001a\u00020\u000e*\u00020\u000f8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011\"\u0018\u0010\u0012\u001a\u00020\u0001*\u00020\u00068CX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0008\"\u001a\u0010\u0014\u001a\u0004\u0018\u00010\u0015*\u00020\u000f8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006\u0018"
    }
    d2 = {
        "actionDescription",
        "",
        "Lcom/squareup/salesreport/SalesReportState$ViewCount;",
        "getActionDescription",
        "(Lcom/squareup/salesreport/SalesReportState$ViewCount;)I",
        "buttonRes",
        "Lcom/squareup/salesreport/SalesReportState$TopSectionState;",
        "getButtonRes",
        "(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)I",
        "colorResId",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;",
        "getColorResId",
        "(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;)I",
        "shouldShowComparisonOption",
        "",
        "Lcom/squareup/salesreport/SalesReportState;",
        "getShouldShowComparisonOption",
        "(Lcom/squareup/salesreport/SalesReportState;)Z",
        "titleRes",
        "getTitleRes",
        "withSalesReport",
        "Lcom/squareup/customreport/data/WithSalesReport;",
        "getWithSalesReport",
        "(Lcom/squareup/salesreport/SalesReportState;)Lcom/squareup/customreport/data/WithSalesReport;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getActionDescription$p(Lcom/squareup/salesreport/SalesReportState$ViewCount;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->getActionDescription(Lcom/squareup/salesreport/SalesReportState$ViewCount;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getButtonRes$p(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->getButtonRes(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getColorResId$p(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->getColorResId(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getShouldShowComparisonOption$p(Lcom/squareup/salesreport/SalesReportState;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->getShouldShowComparisonOption(Lcom/squareup/salesreport/SalesReportState;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getTitleRes$p(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->getTitleRes(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getWithSalesReport$p(Lcom/squareup/salesreport/SalesReportState;)Lcom/squareup/customreport/data/WithSalesReport;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->getWithSalesReport(Lcom/squareup/salesreport/SalesReportState;)Lcom/squareup/customreport/data/WithSalesReport;

    move-result-object p0

    return-object p0
.end method

.method private static final getActionDescription(Lcom/squareup/salesreport/SalesReportState$ViewCount;)I
    .locals 1

    .line 1204
    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    .line 1207
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_view_count_all:I

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 1206
    :cond_1
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_view_count_10:I

    goto :goto_0

    .line 1205
    :cond_2
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_view_count_5:I

    :goto_0
    return p0
.end method

.method private static final getButtonRes(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)I
    .locals 1

    .line 1197
    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    .line 1199
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_show_overview:I

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 1198
    :cond_1
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_overview_show_details:I

    :goto_0
    return p0
.end method

.method private static final getColorResId(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;)I
    .locals 1

    .line 1230
    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 1240
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    sget p0, Lcom/squareup/salesreport/impl/R$color;->sales_report_payment_method_emoney_bar_color:I

    goto :goto_0

    .line 1239
    :pswitch_1
    sget p0, Lcom/squareup/salesreport/impl/R$color;->sales_report_payment_method_installment_bar_color:I

    goto :goto_0

    .line 1238
    :pswitch_2
    sget p0, Lcom/squareup/salesreport/impl/R$color;->sales_report_payment_method_external_bar_color:I

    goto :goto_0

    .line 1237
    :pswitch_3
    sget p0, Lcom/squareup/salesreport/impl/R$color;->sales_report_payment_method_zero_amount_bar_color:I

    goto :goto_0

    .line 1236
    :pswitch_4
    sget p0, Lcom/squareup/salesreport/impl/R$color;->sales_report_payment_method_gift_card_bar_color:I

    goto :goto_0

    .line 1235
    :pswitch_5
    sget p0, Lcom/squareup/salesreport/impl/R$color;->sales_report_payment_method_third_party_card_bar_color:I

    goto :goto_0

    .line 1234
    :pswitch_6
    sget p0, Lcom/squareup/salesreport/impl/R$color;->sales_report_payment_method_split_tender_bar_color:I

    goto :goto_0

    .line 1233
    :pswitch_7
    sget p0, Lcom/squareup/salesreport/impl/R$color;->sales_report_payment_method_other_bar_color:I

    goto :goto_0

    .line 1232
    :pswitch_8
    sget p0, Lcom/squareup/salesreport/impl/R$color;->sales_report_payment_method_card_bar_color:I

    goto :goto_0

    .line 1231
    :pswitch_9
    sget p0, Lcom/squareup/salesreport/impl/R$color;->sales_report_payment_method_cash_bar_color:I

    :goto_0
    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static final getShouldShowComparisonOption(Lcom/squareup/salesreport/SalesReportState;)Z
    .locals 2

    .line 1214
    instance-of v0, p0, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/squareup/salesreport/SalesReportState$ExportingReport;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/salesreport/util/RangeSelectionsKt;->getPossibleComparisonRanges(Lcom/squareup/customreport/data/RangeSelection;)Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    if-le p0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private static final getTitleRes(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)I
    .locals 1

    .line 1190
    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    .line 1192
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_label_uppercase:I

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 1191
    :cond_1
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_overview_header_uppercase:I

    :goto_0
    return p0
.end method

.method private static final getWithSalesReport(Lcom/squareup/salesreport/SalesReportState;)Lcom/squareup/customreport/data/WithSalesReport;
    .locals 1

    .line 1222
    instance-of v0, p0, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->getSalesReport()Lcom/squareup/customreport/data/WithSalesReport;

    move-result-object p0

    goto :goto_0

    .line 1223
    :cond_0
    instance-of v0, p0, Lcom/squareup/salesreport/SalesReportState$ExportingReport;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/squareup/salesreport/SalesReportState$ExportingReport;

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$ExportingReport;->getSalesReport()Lcom/squareup/customreport/data/WithSalesReport;

    move-result-object p0

    goto :goto_0

    .line 1224
    :cond_1
    instance-of v0, p0, Lcom/squareup/salesreport/SalesReportState$CustomizingReport;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/squareup/salesreport/SalesReportState$CustomizingReport;

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$CustomizingReport;->getPreviousState()Lcom/squareup/salesreport/SalesReportState;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->getWithSalesReport(Lcom/squareup/salesreport/SalesReportState;)Lcom/squareup/customreport/data/WithSalesReport;

    move-result-object p0

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method
