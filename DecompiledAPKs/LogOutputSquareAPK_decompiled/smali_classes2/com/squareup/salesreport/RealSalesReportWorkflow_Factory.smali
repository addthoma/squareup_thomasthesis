.class public final Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealSalesReportWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/RealSalesReportWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/InitialSalesReportStateProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final arg10Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/customize/CustomizeReportWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/export/ExportReportWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/SalesReportRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/analytics/SalesReportAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/InitialSalesReportStateProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/customize/CustomizeReportWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/export/ExportReportWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/SalesReportRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/analytics/SalesReportAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 52
    iput-object p2, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 53
    iput-object p3, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 54
    iput-object p4, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 55
    iput-object p5, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 56
    iput-object p6, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 57
    iput-object p7, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 58
    iput-object p8, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 59
    iput-object p9, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg8Provider:Ljavax/inject/Provider;

    .line 60
    iput-object p10, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg9Provider:Ljavax/inject/Provider;

    .line 61
    iput-object p11, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg10Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/InitialSalesReportStateProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/customize/CustomizeReportWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/export/ExportReportWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/SalesReportRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/analytics/SalesReportAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;)",
            "Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;"
        }
    .end annotation

    .line 77
    new-instance v12, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/salesreport/InitialSalesReportStateProvider;Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/customreport/data/SalesReportRepository;Lcom/squareup/salesreport/analytics/SalesReportAnalytics;Lcom/squareup/time/CurrentTime;Lcom/squareup/settings/server/Features;Lcom/squareup/feetutorial/FeeTutorial;)Lcom/squareup/salesreport/RealSalesReportWorkflow;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/InitialSalesReportStateProvider;",
            "Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;",
            "Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/customize/CustomizeReportWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/export/ExportReportWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/customreport/data/SalesReportRepository;",
            "Lcom/squareup/salesreport/analytics/SalesReportAnalytics;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ")",
            "Lcom/squareup/salesreport/RealSalesReportWorkflow;"
        }
    .end annotation

    .line 85
    new-instance v12, Lcom/squareup/salesreport/RealSalesReportWorkflow;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/salesreport/RealSalesReportWorkflow;-><init>(Lcom/squareup/salesreport/InitialSalesReportStateProvider;Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/customreport/data/SalesReportRepository;Lcom/squareup/salesreport/analytics/SalesReportAnalytics;Lcom/squareup/time/CurrentTime;Lcom/squareup/settings/server/Features;Lcom/squareup/feetutorial/FeeTutorial;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/RealSalesReportWorkflow;
    .locals 12

    .line 66
    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/salesreport/InitialSalesReportStateProvider;

    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;

    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;

    iget-object v4, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/customreport/data/SalesReportRepository;

    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/time/CurrentTime;

    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg9Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->arg10Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/feetutorial/FeeTutorial;

    invoke-static/range {v1 .. v11}, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->newInstance(Lcom/squareup/salesreport/InitialSalesReportStateProvider;Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/customreport/data/SalesReportRepository;Lcom/squareup/salesreport/analytics/SalesReportAnalytics;Lcom/squareup/time/CurrentTime;Lcom/squareup/settings/server/Features;Lcom/squareup/feetutorial/FeeTutorial;)Lcom/squareup/salesreport/RealSalesReportWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/salesreport/RealSalesReportWorkflow_Factory;->get()Lcom/squareup/salesreport/RealSalesReportWorkflow;

    move-result-object v0

    return-object v0
.end method
