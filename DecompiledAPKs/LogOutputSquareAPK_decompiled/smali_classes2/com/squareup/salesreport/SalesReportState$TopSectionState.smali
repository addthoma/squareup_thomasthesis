.class public final enum Lcom/squareup/salesreport/SalesReportState$TopSectionState;
.super Ljava/lang/Enum;
.source "SalesReportState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/SalesReportState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TopSectionState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/SalesReportState$TopSectionState$Creator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/salesreport/SalesReportState$TopSectionState;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0087\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\t\u0010\u0004\u001a\u00020\u0005H\u00d6\u0001J\u0019\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0005H\u00d6\u0001j\u0002\u0008\u000bj\u0002\u0008\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/salesreport/SalesReportState$TopSectionState;",
        "",
        "Landroid/os/Parcelable;",
        "(Ljava/lang/String;I)V",
        "describeContents",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "SALES_OVERVIEW",
        "SALES_DETAILS",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/salesreport/SalesReportState$TopSectionState;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final enum SALES_DETAILS:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

.field public static final enum SALES_OVERVIEW:Lcom/squareup/salesreport/SalesReportState$TopSectionState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    new-instance v1, Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    const/4 v2, 0x0

    const-string v3, "SALES_OVERVIEW"

    invoke-direct {v1, v3, v2}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->SALES_OVERVIEW:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    const/4 v2, 0x1

    const-string v3, "SALES_DETAILS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->SALES_DETAILS:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->$VALUES:[Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    new-instance v0, Lcom/squareup/salesreport/SalesReportState$TopSectionState$Creator;

    invoke-direct {v0}, Lcom/squareup/salesreport/SalesReportState$TopSectionState$Creator;-><init>()V

    sput-object v0, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/salesreport/SalesReportState$TopSectionState;
    .locals 1

    const-class v0, Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/salesreport/SalesReportState$TopSectionState;
    .locals 1

    sget-object v0, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->$VALUES:[Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    invoke-virtual {v0}, [Lcom/squareup/salesreport/SalesReportState$TopSectionState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
