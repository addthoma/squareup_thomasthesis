.class public final Lcom/squareup/accountfreeze/ShowNotificationJob;
.super Lcom/squareup/backgroundjob/BackgroundJob;
.source "ShowNotificationJob.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/ShowNotificationJob;",
        "Lcom/squareup/backgroundjob/BackgroundJob;",
        "accountFreezeNotifications",
        "Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;",
        "jobNotificationManager",
        "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
        "(Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)V",
        "runJob",
        "Lcom/evernote/android/job/Job$Result;",
        "params",
        "Lcom/squareup/backgroundjob/JobParams;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountFreezeNotifications:Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;


# direct methods
.method public constructor <init>(Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)V
    .locals 1

    const-string v0, "accountFreezeNotifications"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jobNotificationManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0, p2}, Lcom/squareup/backgroundjob/BackgroundJob;-><init>(Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)V

    iput-object p1, p0, Lcom/squareup/accountfreeze/ShowNotificationJob;->accountFreezeNotifications:Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;

    return-void
.end method


# virtual methods
.method public runJob(Lcom/squareup/backgroundjob/JobParams;)Lcom/evernote/android/job/Job$Result;
    .locals 1

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iget-object p1, p0, Lcom/squareup/accountfreeze/ShowNotificationJob;->accountFreezeNotifications:Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;

    invoke-virtual {p1}, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->showNotification()V

    .line 22
    sget-object p1, Lcom/evernote/android/job/Job$Result;->SUCCESS:Lcom/evernote/android/job/Job$Result;

    return-object p1
.end method
