.class public final Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;
.super Lcom/squareup/charts/piechart/renderers/PieChartRenderer;
.source "SingleSlicePieChartRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSingleSlicePieChartRenderer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SingleSlicePieChartRenderer.kt\ncom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,147:1\n1651#2,3:148\n*E\n*S KotlinDebug\n*F\n+ 1 SingleSlicePieChartRenderer.kt\ncom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer\n*L\n66#1,3:148\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J \u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u000e\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017J.\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u00102\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u001d2\u0006\u0010\u001e\u001a\u00020\u0013H\u0016J\u000e\u0010\u001f\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017J\u001e\u0010 \u001a\u00020\u000c2\u0006\u0010!\u001a\u00020\u00062\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u001dH\u0002J\u001e\u0010\"\u001a\u00020\u000c2\u0006\u0010!\u001a\u00020\u00062\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u001dH\u0002J\u0010\u0010#\u001a\u00020\u000c2\u0006\u0010!\u001a\u00020\u0006H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0006@BX\u0086\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u000e\u0010\n\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;",
        "Lcom/squareup/charts/piechart/renderers/PieChartRenderer;",
        "()V",
        "argbEvaluator",
        "Landroid/animation/ArgbEvaluator;",
        "<set-?>",
        "",
        "currentSliceIndex",
        "getCurrentSliceIndex",
        "()I",
        "previousSliceIndex",
        "rotateClockwise",
        "",
        "calculatePaintForTwoSlices",
        "Landroid/graphics/Paint;",
        "slice",
        "Lcom/squareup/charts/piechart/Slice;",
        "previousSlice",
        "ratio",
        "",
        "next",
        "",
        "pieChart",
        "Lcom/squareup/charts/piechart/PieChart;",
        "onDrawPieChart",
        "canvas",
        "Landroid/graphics/Canvas;",
        "backgroundSlice",
        "slices",
        "",
        "chartProgress",
        "previous",
        "shouldDrawNextSlice",
        "index",
        "shouldDrawPreviousSlice",
        "shouldDrawSlice",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final argbEvaluator:Landroid/animation/ArgbEvaluator;

.field private currentSliceIndex:I

.field private previousSliceIndex:I

.field private rotateClockwise:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 14
    invoke-direct {p0}, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;-><init>()V

    const/4 v0, 0x1

    .line 19
    iput-boolean v0, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->rotateClockwise:Z

    const/4 v0, -0x1

    .line 20
    iput v0, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->previousSliceIndex:I

    .line 21
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->argbEvaluator:Landroid/animation/ArgbEvaluator;

    return-void
.end method

.method private final calculatePaintForTwoSlices(Lcom/squareup/charts/piechart/Slice;Lcom/squareup/charts/piechart/Slice;F)Landroid/graphics/Paint;
    .locals 2

    .line 115
    invoke-virtual {p1}, Lcom/squareup/charts/piechart/Slice;->getColor()Lcom/squareup/charts/piechart/SliceColor;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/charts/piechart/SliceColor$SolidColor;

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/squareup/charts/piechart/Slice;->getColor()Lcom/squareup/charts/piechart/SliceColor;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/charts/piechart/SliceColor$SolidColor;

    if-eqz v0, :cond_1

    .line 116
    invoke-virtual {p1}, Lcom/squareup/charts/piechart/Slice;->getColor()Lcom/squareup/charts/piechart/SliceColor;

    move-result-object v0

    check-cast v0, Lcom/squareup/charts/piechart/SliceColor$SolidColor;

    invoke-virtual {v0}, Lcom/squareup/charts/piechart/SliceColor$SolidColor;->getColor()I

    move-result v0

    .line 117
    invoke-virtual {p2}, Lcom/squareup/charts/piechart/Slice;->getColor()Lcom/squareup/charts/piechart/SliceColor;

    move-result-object p2

    check-cast p2, Lcom/squareup/charts/piechart/SliceColor$SolidColor;

    invoke-virtual {p2}, Lcom/squareup/charts/piechart/SliceColor$SolidColor;->getColor()I

    move-result p2

    .line 119
    invoke-virtual {p0, p1}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->slicePaint(Lcom/squareup/charts/piechart/Slice;)Landroid/graphics/Paint;

    move-result-object p1

    .line 122
    iget-object v1, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->argbEvaluator:Landroid/animation/ArgbEvaluator;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p3, p2, v0}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_0

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type kotlin.Int"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 126
    :cond_1
    invoke-virtual {p0, p1}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->slicePaint(Lcom/squareup/charts/piechart/Slice;)Landroid/graphics/Paint;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final shouldDrawNextSlice(ILjava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;)Z"
        }
    .end annotation

    .line 144
    iget v0, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->previousSliceIndex:I

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    add-int/lit8 v2, p1, -0x1

    if-eq v0, v2, :cond_1

    if-nez p1, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p1, v1

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method private final shouldDrawPreviousSlice(ILjava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;)Z"
        }
    .end annotation

    .line 136
    iget v0, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->previousSliceIndex:I

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    add-int/lit8 v2, p1, 0x1

    if-eq v0, v2, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v1

    if-ne p1, p2, :cond_0

    iget p1, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->previousSliceIndex:I

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method private final shouldDrawSlice(I)Z
    .locals 1

    .line 130
    iget v0, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->currentSliceIndex:I

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public final getCurrentSliceIndex()I
    .locals 1

    .line 16
    iget v0, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->currentSliceIndex:I

    return v0
.end method

.method public final next(Lcom/squareup/charts/piechart/PieChart;)V
    .locals 3

    const-string v0, "pieChart"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 28
    iput-boolean v0, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->rotateClockwise:Z

    .line 29
    iget v1, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->currentSliceIndex:I

    iput v1, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->previousSliceIndex:I

    .line 30
    invoke-virtual {p1}, Lcom/squareup/charts/piechart/PieChart;->getSlices()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v0

    if-ge v1, v2, :cond_0

    .line 31
    iget v1, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->currentSliceIndex:I

    add-int/2addr v1, v0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 30
    :goto_0
    iput v1, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->currentSliceIndex:I

    .line 35
    invoke-virtual {p1}, Lcom/squareup/charts/piechart/PieChart;->startAnimation()V

    return-void
.end method

.method public onDrawPieChart(Landroid/graphics/Canvas;Lcom/squareup/charts/piechart/Slice;Ljava/util/List;F)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Lcom/squareup/charts/piechart/Slice;",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;F)V"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v0, p2

    move-object/from16 v7, p3

    move/from16 v8, p4

    const-string v1, "canvas"

    move-object/from16 v9, p1

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "backgroundSlice"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "slices"

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->getDrawableArea()Landroid/graphics/RectF;

    move-result-object v10

    .line 63
    invoke-virtual {v6, v0}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->slicePaint(Lcom/squareup/charts/piechart/Slice;)Landroid/graphics/Paint;

    move-result-object v5

    const/4 v3, 0x0

    const/high16 v4, 0x43b40000    # 360.0f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v2, v10

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->drawSlice(Landroid/graphics/Canvas;Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 65
    move-object v0, v7

    check-cast v0, Ljava/lang/Iterable;

    .line 149
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    const/high16 v0, -0x3d4c0000    # -90.0f

    const/4 v1, 0x0

    const/high16 v12, -0x3d4c0000    # -90.0f

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v13, v1, 0x1

    if-gez v1, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v0, Lcom/squareup/charts/piechart/Slice;

    .line 67
    invoke-direct {v6, v1}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->shouldDrawSlice(I)Z

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v14, 0x0

    if-eqz v2, :cond_1

    .line 69
    invoke-static {v0, v8, v14, v5, v4}, Lcom/squareup/charts/piechart/renderers/RendererUtilsKt;->calculateAngle$default(Lcom/squareup/charts/piechart/Slice;FFILjava/lang/Object;)F

    move-result v15

    goto :goto_1

    .line 71
    :cond_1
    invoke-static {v0, v3, v14, v5, v4}, Lcom/squareup/charts/piechart/renderers/RendererUtilsKt;->calculateAngle$default(Lcom/squareup/charts/piechart/Slice;FFILjava/lang/Object;)F

    move-result v15

    :goto_1
    if-eqz v2, :cond_5

    .line 75
    iget-boolean v2, v6, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->rotateClockwise:Z

    if-eqz v2, :cond_3

    .line 76
    invoke-direct {v6, v1, v7}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->shouldDrawNextSlice(ILjava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 77
    iget v1, v6, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->previousSliceIndex:I

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/charts/piechart/Slice;

    sub-float/2addr v3, v8

    .line 78
    invoke-static {v1, v3, v14, v5, v4}, Lcom/squareup/charts/piechart/renderers/RendererUtilsKt;->calculateAngle$default(Lcom/squareup/charts/piechart/Slice;FFILjava/lang/Object;)F

    move-result v2

    sub-float v3, v12, v2

    add-float v4, v15, v2

    .line 81
    invoke-direct {v6, v0, v1, v8}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->calculatePaintForTwoSlices(Lcom/squareup/charts/piechart/Slice;Lcom/squareup/charts/piechart/Slice;F)Landroid/graphics/Paint;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v2, v10

    .line 83
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->drawSlice(Landroid/graphics/Canvas;Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 85
    :cond_2
    invoke-virtual {v6, v0}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->slicePaint(Lcom/squareup/charts/piechart/Slice;)Landroid/graphics/Paint;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v2, v10

    move v3, v12

    move v4, v15

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->drawSlice(Landroid/graphics/Canvas;Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_2

    :cond_3
    const/high16 v2, 0x43b40000    # 360.0f

    .line 88
    invoke-virtual {v0}, Lcom/squareup/charts/piechart/Slice;->getLength()F

    move-result v16

    mul-float v16, v16, v2

    const/high16 v2, 0x42c80000    # 100.0f

    div-float v16, v16, v2

    .line 90
    invoke-direct {v6, v1, v7}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->shouldDrawPreviousSlice(ILjava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 91
    iget v1, v6, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->previousSliceIndex:I

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/charts/piechart/Slice;

    sub-float/2addr v3, v8

    .line 92
    invoke-static {v1, v3, v14, v5, v4}, Lcom/squareup/charts/piechart/renderers/RendererUtilsKt;->calculateAngle$default(Lcom/squareup/charts/piechart/Slice;FFILjava/lang/Object;)F

    move-result v2

    add-float v16, v12, v16

    add-float v3, v16, v2

    add-float/2addr v2, v15

    neg-float v4, v2

    .line 95
    invoke-direct {v6, v0, v1, v8}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->calculatePaintForTwoSlices(Lcom/squareup/charts/piechart/Slice;Lcom/squareup/charts/piechart/Slice;F)Landroid/graphics/Paint;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v2, v10

    .line 97
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->drawSlice(Landroid/graphics/Canvas;Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_2

    :cond_4
    add-float v3, v12, v16

    neg-float v4, v15

    .line 99
    invoke-virtual {v6, v0}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->slicePaint(Lcom/squareup/charts/piechart/Slice;)Landroid/graphics/Paint;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v2, v10

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->drawSlice(Landroid/graphics/Canvas;Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    :cond_5
    :goto_2
    add-float/2addr v12, v15

    move v1, v13

    goto/16 :goto_0

    :cond_6
    return-void
.end method

.method public final previous(Lcom/squareup/charts/piechart/PieChart;)V
    .locals 1

    const-string v0, "pieChart"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 43
    iput-boolean v0, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->rotateClockwise:Z

    .line 44
    iget v0, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->currentSliceIndex:I

    iput v0, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->previousSliceIndex:I

    if-nez v0, :cond_0

    .line 46
    invoke-virtual {p1}, Lcom/squareup/charts/piechart/PieChart;->getSlices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 45
    iput v0, p0, Lcom/squareup/charts/piechart/renderers/SingleSlicePieChartRenderer;->currentSliceIndex:I

    .line 50
    invoke-virtual {p1}, Lcom/squareup/charts/piechart/PieChart;->startAnimation()V

    return-void
.end method
