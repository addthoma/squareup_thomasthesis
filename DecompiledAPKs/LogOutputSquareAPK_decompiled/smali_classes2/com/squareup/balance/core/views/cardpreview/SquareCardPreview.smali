.class public final Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;
.super Landroid/widget/RelativeLayout;
.source "SquareCardPreview.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;,
        Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;,
        Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText;,
        Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardPreview.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardPreview.kt\ncom/squareup/balance/core/views/cardpreview/SquareCardPreview\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,448:1\n33#2,10:449\n1261#3:459\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardPreview.kt\ncom/squareup/balance/core/views/cardpreview/SquareCardPreview\n*L\n287#1,10:449\n58#1:459\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0093\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0005*\u0001\u0010\u0018\u0000 [2\u00020\u0001:\u0004YZ[\\B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010/\u001a\u0002002\u0006\u00101\u001a\u00020\u0007H\u0002J\u0008\u00102\u001a\u000200H\u0002J\u0008\u00103\u001a\u000200H\u0002J\r\u00104\u001a\u00020\u0010H\u0002\u00a2\u0006\u0002\u00105J\u0006\u00106\u001a\u000200J\u0008\u00107\u001a\u000200H\u0014J\u0018\u00108\u001a\u0002002\u0006\u00109\u001a\u00020\u00072\u0006\u0010:\u001a\u00020\u0007H\u0014J\u0012\u0010;\u001a\u0002002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0002J\u0010\u0010<\u001a\u0002002\u0006\u0010=\u001a\u00020!H\u0016J\u0016\u0010<\u001a\u0002002\u0006\u0010=\u001a\u00020!2\u0006\u0010>\u001a\u00020!J\u001e\u0010?\u001a\u0002002\u0006\u0010@\u001a\u00020A2\u0006\u0010B\u001a\u00020A2\u0006\u0010C\u001a\u00020AJ\u0010\u0010D\u001a\u0002002\u0008\u0010E\u001a\u0004\u0018\u00010AJ\u000e\u0010F\u001a\u0002002\u0006\u0010G\u001a\u00020\u0017J\u000e\u0010H\u001a\u0002002\u0006\u0010I\u001a\u00020JJ\u0012\u0010K\u001a\u0002002\u0008\u0010I\u001a\u0004\u0018\u00010AH\u0007J\u000e\u0010L\u001a\u0002002\u0006\u0010M\u001a\u00020NJ\u0010\u0010L\u001a\u0002002\u0008\u0008\u0001\u0010M\u001a\u00020\u0007J\u0008\u0010O\u001a\u000200H\u0002J\u0008\u0010P\u001a\u000200H\u0002J\u0006\u0010Q\u001a\u000200J\u0006\u0010R\u001a\u000200J\u001a\u0010S\u001a\u0002002\u0006\u0010=\u001a\u00020!2\u0008\u0008\u0002\u0010>\u001a\u00020!H\u0002J\u000c\u0010T\u001a\u00020\u0007*\u00020\u0007H\u0002J\u000c\u0010U\u001a\u00020\u0007*\u00020\u0007H\u0002J\u000c\u0010V\u001a\u000200*\u00020#H\u0002J\u000c\u0010W\u001a\u00020!*\u00020XH\u0002R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000b\u0010\u000cR\u0010\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0011R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020#X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020&X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020#X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020#X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006]"
    }
    d2 = {
        "Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;",
        "Landroid/widget/RelativeLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "animationHandler",
        "Landroid/os/Handler;",
        "getAnimationHandler",
        "()Landroid/os/Handler;",
        "animationHandler$delegate",
        "Lkotlin/Lazy;",
        "animationRunnable",
        "com/squareup/balance/core/views/cardpreview/SquareCardPreview$createAnimationRunnable$1",
        "Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$createAnimationRunnable$1;",
        "cardCornerRadius",
        "Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;",
        "cardDetailsContainer",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "cardOrientation",
        "Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;",
        "cardPreviewMagstripe",
        "Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;",
        "chip",
        "Landroid/widget/ImageView;",
        "constraintContainer",
        "defaultElevation",
        "",
        "defaultElevationDisabled",
        "enabledStateForAnimation",
        "",
        "expirationAndCvc",
        "Landroid/widget/TextView;",
        "frontNameOnCard",
        "masterCardTypeLogo",
        "Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;",
        "maxWidthInPixels",
        "nameOnCard",
        "panOnCard",
        "signatureOnCard",
        "squareLogo",
        "textScaleIncrement",
        "textScaleMax",
        "textScaleMin",
        "adjustConstraintSet",
        "",
        "topGuide",
        "bindViews",
        "configureNameAndPanViews",
        "createAnimationRunnable",
        "()Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$createAnimationRunnable$1;",
        "hideExpirationAndCvc",
        "onDetachedFromWindow",
        "onMeasure",
        "widthMeasureSpec",
        "heightMeasureSpec",
        "readAttributes",
        "setEnabled",
        "enabled",
        "animate",
        "setExpirationAndCvc",
        "expMonth",
        "",
        "expYear",
        "cvcNumber",
        "setName",
        "name",
        "setOrientation",
        "orientation",
        "setPan",
        "pan",
        "Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText;",
        "setPanLast4",
        "setSignature",
        "signature",
        "Landroid/graphics/Bitmap;",
        "setupCornerRadius",
        "setupOrientation",
        "startAnimatingIndefinitely",
        "stopAnimating",
        "updateViewState",
        "adjustHeight",
        "adjustWidth",
        "autoSize",
        "isAnimating",
        "Landroid/view/View;",
        "CardCornerRadius",
        "CardOrientation",
        "Companion",
        "PanText",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIDTH_HEIGHT_RATIO:F = 0.6208955f


# instance fields
.field private final animationHandler$delegate:Lkotlin/Lazy;

.field private final animationRunnable:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$createAnimationRunnable$1;

.field private cardCornerRadius:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;

.field private cardDetailsContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private cardOrientation:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

.field private cardPreviewMagstripe:Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;

.field private chip:Landroid/widget/ImageView;

.field private constraintContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private final defaultElevation:F

.field private final defaultElevationDisabled:F

.field private enabledStateForAnimation:Z

.field private expirationAndCvc:Landroid/widget/TextView;

.field private frontNameOnCard:Landroid/widget/TextView;

.field private masterCardTypeLogo:Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;

.field private maxWidthInPixels:I

.field private nameOnCard:Landroid/widget/TextView;

.field private panOnCard:Landroid/widget/TextView;

.field private signatureOnCard:Landroid/widget/ImageView;

.field private squareLogo:Landroid/widget/ImageView;

.field private final textScaleIncrement:I

.field private final textScaleMax:I

.field private final textScaleMin:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->Companion:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 459
    sget-object v0, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v0, p1}, Lcom/squareup/util/VectorFriendly;->ensureContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 58
    invoke-direct {p0, v0, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->createAnimationRunnable()Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$createAnimationRunnable$1;

    move-result-object p3

    iput-object p3, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->animationRunnable:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$createAnimationRunnable$1;

    .line 72
    sget-object p3, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$animationHandler$2;->INSTANCE:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$animationHandler$2;

    check-cast p3, Lkotlin/jvm/functions/Function0;

    invoke-static {p3}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p3

    iput-object p3, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->animationHandler$delegate:Lkotlin/Lazy;

    const/4 p3, -0x1

    .line 88
    iput p3, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->maxWidthInPixels:I

    .line 89
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/squareup/balance/core/R$dimen;->square_card_preview_elevation:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p3

    iput p3, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->defaultElevation:F

    .line 91
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/squareup/balance/core/R$dimen;->square_card_preview_elevation_disabled:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p3

    iput p3, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->defaultElevationDisabled:F

    .line 92
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/squareup/balance/core/R$integer;->square_card_preview_min_text_size:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p3

    iput p3, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->textScaleMin:I

    .line 93
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/squareup/balance/core/R$integer;->square_card_preview_max_text_size:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p3

    iput p3, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->textScaleMax:I

    .line 95
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/squareup/balance/core/R$integer;->square_card_preview_scale_increment:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p3

    iput p3, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->textScaleIncrement:I

    .line 98
    sget p3, Lcom/squareup/balance/core/R$layout;->square_card_preview_view:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1, p3, v0}, Landroid/widget/RelativeLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 100
    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->bindViews()V

    .line 101
    invoke-direct {p0, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->readAttributes(Landroid/util/AttributeSet;)V

    .line 102
    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->configureNameAndPanViews()V

    .line 104
    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setupOrientation()V

    .line 105
    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setupCornerRadius()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 56
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 57
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getAnimationHandler$p(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;)Landroid/os/Handler;
    .locals 0

    .line 53
    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->getAnimationHandler()Landroid/os/Handler;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCardCornerRadius$p(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;)Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;
    .locals 1

    .line 53
    iget-object p0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardCornerRadius:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;

    if-nez p0, :cond_0

    const-string v0, "cardCornerRadius"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getCardOrientation$p(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;)Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;
    .locals 1

    .line 53
    iget-object p0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardOrientation:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    if-nez p0, :cond_0

    const-string v0, "cardOrientation"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getConstraintContainer$p(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;)Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 1

    .line 53
    iget-object p0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->constraintContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez p0, :cond_0

    const-string v0, "constraintContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getEnabledStateForAnimation$p(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;)Z
    .locals 0

    .line 53
    iget-boolean p0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->enabledStateForAnimation:Z

    return p0
.end method

.method public static final synthetic access$getMaxWidthInPixels$p(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;)I
    .locals 0

    .line 53
    iget p0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->maxWidthInPixels:I

    return p0
.end method

.method public static final synthetic access$setCardCornerRadius$p(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardCornerRadius:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;

    return-void
.end method

.method public static final synthetic access$setCardOrientation$p(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardOrientation:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    return-void
.end method

.method public static final synthetic access$setConstraintContainer$p(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;Landroidx/constraintlayout/widget/ConstraintLayout;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->constraintContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

    return-void
.end method

.method public static final synthetic access$setEnabledStateForAnimation$p(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;Z)V
    .locals 0

    .line 53
    iput-boolean p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->enabledStateForAnimation:Z

    return-void
.end method

.method public static final synthetic access$setMaxWidthInPixels$p(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;I)V
    .locals 0

    .line 53
    iput p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->maxWidthInPixels:I

    return-void
.end method

.method public static final synthetic access$updateViewState(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;ZZ)V
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->updateViewState(ZZ)V

    return-void
.end method

.method private final adjustConstraintSet(I)V
    .locals 5

    .line 381
    new-instance v0, Landroidx/constraintlayout/widget/ConstraintSet;

    invoke-direct {v0}, Landroidx/constraintlayout/widget/ConstraintSet;-><init>()V

    .line 382
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->constraintContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

    const-string v2, "constraintContainer"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v1}, Landroidx/constraintlayout/widget/ConstraintSet;->clone(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 383
    sget v1, Lcom/squareup/balance/core/R$id;->card_details_container:I

    const/4 v3, 0x3

    const/4 v4, 0x4

    invoke-virtual {v0, v1, v3, p1, v4}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 384
    iget-object p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->constraintContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez p1, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, p1}, Landroidx/constraintlayout/widget/ConstraintSet;->applyTo(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    return-void
.end method

.method private final adjustHeight(I)I
    .locals 1

    int-to-float p1, p1

    const v0, 0x3f1ef302

    mul-float p1, p1, v0

    .line 362
    invoke-static {p1}, Lkotlin/math/MathKt;->roundToInt(F)I

    move-result p1

    return p1
.end method

.method private final adjustWidth(I)I
    .locals 1

    int-to-float p1, p1

    const v0, 0x3f1ef302

    div-float/2addr p1, v0

    .line 363
    invoke-static {p1}, Lkotlin/math/MathKt;->roundToInt(F)I

    move-result p1

    return p1
.end method

.method private final autoSize(Landroid/widget/TextView;)V
    .locals 4

    .line 367
    iget v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->textScaleMin:I

    iget v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->textScaleMax:I

    iget v2, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->textScaleIncrement:I

    const/4 v3, 0x1

    .line 366
    invoke-static {p1, v0, v1, v2, v3}, Landroidx/core/widget/TextViewCompat;->setAutoSizeTextTypeUniformWithConfiguration(Landroid/widget/TextView;IIII)V

    return-void
.end method

.method private final bindViews()V
    .locals 1

    .line 388
    sget v0, Lcom/squareup/balance/core/R$id;->card_preview_magstripe:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;

    iput-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardPreviewMagstripe:Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;

    .line 389
    sget v0, Lcom/squareup/balance/core/R$id;->name_on_card:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->nameOnCard:Landroid/widget/TextView;

    .line 390
    sget v0, Lcom/squareup/balance/core/R$id;->expiration_and_cvc:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->expirationAndCvc:Landroid/widget/TextView;

    .line 391
    sget v0, Lcom/squareup/balance/core/R$id;->pan:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->panOnCard:Landroid/widget/TextView;

    .line 392
    sget v0, Lcom/squareup/balance/core/R$id;->card_preview_constraint_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    iput-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->constraintContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 393
    sget v0, Lcom/squareup/balance/core/R$id;->card_preview_logo:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;

    iput-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->masterCardTypeLogo:Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;

    .line 394
    sget v0, Lcom/squareup/balance/core/R$id;->card_details_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    iput-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardDetailsContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 396
    sget v0, Lcom/squareup/balance/core/R$id;->signature_on_card:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->signatureOnCard:Landroid/widget/ImageView;

    .line 397
    sget v0, Lcom/squareup/balance/core/R$id;->card_preview_square_logo:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->squareLogo:Landroid/widget/ImageView;

    .line 398
    sget v0, Lcom/squareup/balance/core/R$id;->card_preview_chip:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->chip:Landroid/widget/ImageView;

    .line 399
    sget v0, Lcom/squareup/balance/core/R$id;->front_business_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->frontNameOnCard:Landroid/widget/TextView;

    return-void
.end method

.method private final configureNameAndPanViews()V
    .locals 2

    .line 280
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->nameOnCard:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "nameOnCard"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->autoSize(Landroid/widget/TextView;)V

    .line 281
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->panOnCard:Landroid/widget/TextView;

    if-nez v0, :cond_1

    const-string v1, "panOnCard"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->autoSize(Landroid/widget/TextView;)V

    .line 282
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->frontNameOnCard:Landroid/widget/TextView;

    if-nez v0, :cond_2

    const-string v1, "frontNameOnCard"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->autoSize(Landroid/widget/TextView;)V

    .line 283
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->expirationAndCvc:Landroid/widget/TextView;

    if-nez v0, :cond_3

    const-string v1, "expirationAndCvc"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-direct {p0, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->autoSize(Landroid/widget/TextView;)V

    return-void
.end method

.method private final createAnimationRunnable()Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$createAnimationRunnable$1;
    .locals 1

    .line 267
    new-instance v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$createAnimationRunnable$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$createAnimationRunnable$1;-><init>(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;)V

    return-object v0
.end method

.method private final getAnimationHandler()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->animationHandler$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    return-object v0
.end method

.method private final isAnimating(Landroid/view/View;)Z
    .locals 1

    .line 403
    invoke-virtual {p1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object p1

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    xor-int/2addr p1, v0

    return p1
.end method

.method private final readAttributes(Landroid/util/AttributeSet;)V
    .locals 3

    .line 287
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/squareup/balance/core/R$styleable;->SquareCardPreview:[I

    const-string v2, "R.styleable.SquareCardPreview"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 453
    invoke-virtual {v0, p1, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string v0, "a"

    .line 455
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 290
    sget v0, Lcom/squareup/balance/core/R$styleable;->SquareCardPreview_sqCardOrientation:I

    invoke-static {}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;->values()[Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    move-result-object v1

    check-cast v1, [Ljava/lang/Enum;

    sget-object v2, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;->BACK:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    check-cast v2, Ljava/lang/Enum;

    .line 289
    invoke-static {p1, v0, v1, v2}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;I[Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    invoke-static {p0, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->access$setCardOrientation$p(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;)V

    .line 294
    sget v0, Lcom/squareup/balance/core/R$styleable;->SquareCardPreview_sqCardCornerRadius:I

    invoke-static {}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;->values()[Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;

    move-result-object v1

    check-cast v1, [Ljava/lang/Enum;

    sget-object v2, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;->LARGE:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;

    check-cast v2, Ljava/lang/Enum;

    .line 293
    invoke-static {p1, v0, v1, v2}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;I[Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;

    invoke-static {p0, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->access$setCardCornerRadius$p(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;)V

    .line 299
    sget v0, Lcom/squareup/balance/core/R$styleable;->SquareCardPreview_sqMaxCardWidth:I

    const/4 v1, -0x1

    .line 298
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->access$setMaxWidthInPixels$p(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;I)V

    .line 302
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 457
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private final setupCornerRadius()V
    .locals 2

    .line 307
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardCornerRadius:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;

    if-nez v0, :cond_0

    const-string v1, "cardCornerRadius"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;->SMALL:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardCornerRadius;

    if-ne v0, v1, :cond_2

    .line 308
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->constraintContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v0, :cond_1

    const-string v1, "constraintContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 309
    :cond_1
    sget v1, Lcom/squareup/balance/core/R$drawable;->white_square_card_preview_base_small_radius:I

    .line 308
    invoke-virtual {v0, v1}, Landroidx/constraintlayout/widget/ConstraintLayout;->setBackgroundResource(I)V

    :cond_2
    return-void
.end method

.method private final setupOrientation()V
    .locals 3

    .line 315
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardOrientation:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    if-nez v0, :cond_0

    const-string v1, "cardOrientation"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;->FRONT:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 318
    :goto_0
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->nameOnCard:Landroid/widget/TextView;

    if-nez v1, :cond_2

    const-string v2, "nameOnCard"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v1, Landroid/view/View;

    xor-int/lit8 v2, v0, 0x1

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 319
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardPreviewMagstripe:Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;

    if-nez v1, :cond_3

    const-string v2, "cardPreviewMagstripe"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v1, Landroid/view/View;

    xor-int/lit8 v2, v0, 0x1

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 320
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->panOnCard:Landroid/widget/TextView;

    if-nez v1, :cond_4

    const-string v2, "panOnCard"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v1, Landroid/view/View;

    xor-int/lit8 v2, v0, 0x1

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 321
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->masterCardTypeLogo:Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;

    if-nez v1, :cond_5

    const-string v2, "masterCardTypeLogo"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v1, Landroid/view/View;

    xor-int/lit8 v2, v0, 0x1

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 324
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardDetailsContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v1, :cond_6

    const-string v2, "cardDetailsContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    if-nez v0, :cond_7

    new-instance v2, Landroid/animation/LayoutTransition;

    invoke-direct {v2}, Landroid/animation/LayoutTransition;-><init>()V

    goto :goto_1

    :cond_7
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, v2}, Landroidx/constraintlayout/widget/ConstraintLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 327
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->signatureOnCard:Landroid/widget/ImageView;

    if-nez v1, :cond_8

    const-string v2, "signatureOnCard"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 328
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->squareLogo:Landroid/widget/ImageView;

    if-nez v1, :cond_9

    const-string v2, "squareLogo"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 329
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->chip:Landroid/widget/ImageView;

    if-nez v1, :cond_a

    const-string v2, "chip"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 330
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->frontNameOnCard:Landroid/widget/TextView;

    if-nez v1, :cond_b

    const-string v2, "frontNameOnCard"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final updateViewState(ZZ)V
    .locals 4

    .line 338
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->masterCardTypeLogo:Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;

    if-nez v0, :cond_0

    const-string v1, "masterCardTypeLogo"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareMasterCardLogo;->setEnabled(ZZ)V

    .line 339
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardPreviewMagstripe:Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;

    if-nez v0, :cond_1

    const-string v1, "cardPreviewMagstripe"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, p1, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardMagStripe;->setEnabled(ZZ)V

    if-eqz p1, :cond_2

    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_2
    const v0, 0x3ecccccd    # 0.4f

    :goto_0
    if-eqz p1, :cond_3

    .line 342
    iget p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->defaultElevation:F

    goto :goto_1

    :cond_3
    iget p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->defaultElevationDisabled:F

    .line 344
    :goto_1
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->nameOnCard:Landroid/widget/TextView;

    if-nez v1, :cond_4

    const-string v2, "nameOnCard"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardViewsCommonKt;->fadeCardPreview(Landroid/view/View;FZ)V

    .line 345
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->frontNameOnCard:Landroid/widget/TextView;

    if-nez v1, :cond_5

    const-string v2, "frontNameOnCard"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardViewsCommonKt;->fadeCardPreview(Landroid/view/View;FZ)V

    .line 346
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->panOnCard:Landroid/widget/TextView;

    if-nez v1, :cond_6

    const-string v2, "panOnCard"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardViewsCommonKt;->fadeCardPreview(Landroid/view/View;FZ)V

    .line 347
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->expirationAndCvc:Landroid/widget/TextView;

    if-nez v1, :cond_7

    const-string v2, "expirationAndCvc"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardViewsCommonKt;->fadeCardPreview(Landroid/view/View;FZ)V

    .line 348
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->chip:Landroid/widget/ImageView;

    if-nez v1, :cond_8

    const-string v2, "chip"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardViewsCommonKt;->fadeCardPreview(Landroid/view/View;FZ)V

    .line 349
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->squareLogo:Landroid/widget/ImageView;

    if-nez v1, :cond_9

    const-string v2, "squareLogo"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardViewsCommonKt;->fadeCardPreview(Landroid/view/View;FZ)V

    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    .line 352
    iget-object v2, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->constraintContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v2, :cond_a

    const-string v3, "constraintContainer"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {v2}, Landroidx/constraintlayout/widget/ConstraintLayout;->getElevation()F

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    aput p1, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p1

    .line 354
    new-instance v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$updateViewState$$inlined$apply$lambda$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$updateViewState$$inlined$apply$lambda$1;-><init>(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;Z)V

    check-cast v0, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    if-eqz p2, :cond_b

    .line 357
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardViewsCommonKt;->getCardPreviewAnimDuration(Landroid/content/Context;)J

    move-result-wide v0

    goto :goto_2

    :cond_b
    const-wide/16 v0, 0x0

    :goto_2
    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 358
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method static synthetic updateViewState$default(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;ZZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 336
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->updateViewState(ZZ)V

    return-void
.end method


# virtual methods
.method public final hideExpirationAndCvc()V
    .locals 2

    .line 197
    sget v0, Lcom/squareup/balance/core/R$id;->top_text_guide_smaller:I

    invoke-direct {p0, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->adjustConstraintSet(I)V

    .line 199
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->expirationAndCvc:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "expirationAndCvc"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .line 252
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->stopAnimating()V

    .line 253
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    .line 206
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 207
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 209
    iget v2, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->maxWidthInPixels:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    if-le v1, v2, :cond_0

    move v1, v2

    :cond_0
    if-gtz v0, :cond_1

    .line 214
    invoke-direct {p0, v1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->adjustHeight(I)I

    move-result v0

    .line 218
    :cond_1
    iget-object v2, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->constraintContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

    const-string v3, "constraintContainer"

    if-nez v2, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v2, Landroid/view/View;

    invoke-static {v2}, Lcom/squareup/util/Views;->getHorizontalMargins(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v1, v2

    .line 219
    iget-object v2, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->constraintContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v2, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v2, Landroid/view/View;

    invoke-static {v2}, Lcom/squareup/util/Views;->getVerticalMargins(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v0, v2

    .line 222
    invoke-direct {p0, v1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->adjustHeight(I)I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 223
    invoke-direct {p0, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->adjustWidth(I)I

    move-result v1

    goto :goto_0

    .line 226
    :cond_4
    invoke-direct {p0, v1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->adjustHeight(I)I

    move-result v0

    .line 229
    :goto_0
    iget-object v2, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->constraintContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v2, :cond_5

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v2}, Landroidx/constraintlayout/widget/ConstraintLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 230
    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->constraintContainer:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v1, :cond_6

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v1}, Landroidx/constraintlayout/widget/ConstraintLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 232
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 236
    invoke-virtual {p0, p1, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setEnabled(ZZ)V

    return-void
.end method

.method public final setEnabled(ZZ)V
    .locals 1

    .line 243
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->isEnabled()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->stopAnimating()V

    .line 246
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->updateViewState(ZZ)V

    .line 248
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    return-void
.end method

.method public final setExpirationAndCvc(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const-string v0, "expMonth"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "expYear"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cvcNumber"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardOrientation:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    if-nez v0, :cond_0

    const-string v1, "cardOrientation"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;->BACK:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_a

    .line 177
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_9

    .line 178
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v1, :cond_3

    const/4 v2, 0x1

    :cond_3
    if-eqz v2, :cond_8

    .line 180
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/balance/core/R$string;->square_card_expiration_and_cvc_phrase:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 181
    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "exp_month"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 182
    check-cast p2, Ljava/lang/CharSequence;

    const-string v0, "exp_year"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 183
    check-cast p3, Ljava/lang/CharSequence;

    const-string p2, "cvc_number"

    invoke-virtual {p1, p2, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 184
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 187
    iget-object p2, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->expirationAndCvc:Landroid/widget/TextView;

    const-string p3, "expirationAndCvc"

    if-nez p2, :cond_4

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    xor-int/2addr p2, v3

    if-eqz p2, :cond_6

    .line 189
    iget-object p2, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->expirationAndCvc:Landroid/widget/TextView;

    if-nez p2, :cond_5

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    :cond_6
    sget p1, Lcom/squareup/balance/core/R$id;->top_text_guide:I

    invoke-direct {p0, p1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->adjustConstraintSet(I)V

    .line 193
    iget-object p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->expirationAndCvc:Landroid/widget/TextView;

    if-nez p1, :cond_7

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void

    .line 178
    :cond_8
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Year has to have 2 characters"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 177
    :cond_9
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Month has to have 2 characters"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 176
    :cond_a
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Pan can only be set for \'back\' of the card"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final setName(Ljava/lang/String;)V
    .locals 3

    .line 118
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->nameOnCard:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "nameOnCard"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const-string v1, ""

    if-eqz p1, :cond_1

    move-object v2, p1

    goto :goto_0

    :cond_1
    move-object v2, v1

    :goto_0
    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->frontNameOnCard:Landroid/widget/TextView;

    if-nez v0, :cond_2

    const-string v2, "frontNameOnCard"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    if-eqz p1, :cond_3

    goto :goto_1

    :cond_3
    move-object p1, v1

    :goto_1
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->invalidate()V

    return-void
.end method

.method public final setOrientation(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;)V
    .locals 1

    const-string v0, "orientation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iput-object p1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardOrientation:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    .line 112
    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setupOrientation()V

    .line 113
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->invalidate()V

    return-void
.end method

.method public final setPan(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText;)V
    .locals 5

    const-string v0, "pan"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardOrientation:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    if-nez v0, :cond_0

    const-string v1, "cardOrientation"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;->BACK:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_d

    .line 153
    invoke-virtual {p1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText;->evaluatePan$public_release()Ljava/lang/String;

    move-result-object p1

    .line 156
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->panOnCard:Landroid/widget/TextView;

    const-string v1, "panOnCard"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v2

    const/high16 v3, 0x3f800000    # 1.0f

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->panOnCard:Landroid/widget/TextView;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->isAnimating(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 158
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->panOnCard:Landroid/widget/TextView;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 159
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->panOnCard:Landroid/widget/TextView;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 160
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_1

    .line 162
    :cond_6
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->panOnCard:Landroid/widget/TextView;

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->isAnimating(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 163
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->panOnCard:Landroid/widget/TextView;

    if-nez v0, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 166
    :cond_9
    :goto_1
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->panOnCard:Landroid/widget/TextView;

    if-nez v0, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v2

    if-eqz v0, :cond_c

    .line 167
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->panOnCard:Landroid/widget/TextView;

    if-nez v0, :cond_b

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_c
    return-void

    .line 151
    :cond_d
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Pan can only be set for \'back\' of the card"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final setPanLast4(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lkotlin/Deprecated;
        message = "use `setPan(Last4(pan))` instead"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "setPan(Last4(pan))"
            imports = {
                "com.squareup.balance.core.views.cardpreview.SquareCardPreview.PanText.Last4"
            }
        .end subannotation
    .end annotation

    .line 146
    new-instance v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;

    invoke-direct {v0, p1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText$Last4;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText;

    invoke-virtual {p0, v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setPan(Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$PanText;)V

    return-void
.end method

.method public final setSignature(I)V
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardOrientation:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    if-nez v0, :cond_0

    const-string v1, "cardOrientation"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;->FRONT:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    .line 133
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->signatureOnCard:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    const-string v1, "signatureOnCard"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 134
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->invalidate()V

    return-void

    .line 132
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Signature can only be set for the \'front\' of the card"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final setSignature(Landroid/graphics/Bitmap;)V
    .locals 2

    const-string v0, "signature"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->cardOrientation:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    if-nez v0, :cond_0

    const-string v1, "cardOrientation"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;->FRONT:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$CardOrientation;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    .line 126
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->signatureOnCard:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    const-string v1, "signatureOnCard"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 127
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->invalidate()V

    return-void

    .line 125
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Signature can only be set for the \'front\' of the card"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final startAnimatingIndefinitely()V
    .locals 1

    .line 257
    invoke-static {}, Lcom/squareup/util/Views;->noAnimationsForInstrumentation()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 259
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->isEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->enabledStateForAnimation:Z

    .line 260
    iget-object v0, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->animationRunnable:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$createAnimationRunnable$1;

    invoke-virtual {v0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$createAnimationRunnable$1;->run()V

    return-void
.end method

.method public final stopAnimating()V
    .locals 2

    .line 264
    invoke-direct {p0}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->getAnimationHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->animationRunnable:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview$createAnimationRunnable$1;

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method
