.class public final Lcom/squareup/balance/core/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/core/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final responsive_square_card_signature_button_margin:I = 0x7f070482

.field public static final square_card_preview_back_name_spacing:I = 0x7f0704eb

.field public static final square_card_preview_background_corner_radius:I = 0x7f0704ec

.field public static final square_card_preview_background_corner_radius_small:I = 0x7f0704ed

.field public static final square_card_preview_elevation:I = 0x7f0704ee

.field public static final square_card_preview_elevation_disabled:I = 0x7f0704ef

.field public static final square_card_preview_elevation_offset_horizontal:I = 0x7f0704f0

.field public static final square_card_preview_elevation_offset_vertical:I = 0x7f0704f1

.field public static final square_card_preview_small_spacing:I = 0x7f0704f2

.field public static final square_card_signature_indicator_top_spacing:I = 0x7f0704f3

.field public static final square_card_signature_spacing:I = 0x7f0704f4

.field public static final square_card_stamp_preview_padding:I = 0x7f0704f5

.field public static final square_card_upsell_elevation:I = 0x7f0704f7

.field public static final square_card_upsell_max_width:I = 0x7f0704f8

.field public static final square_card_upsell_preview_height:I = 0x7f0704f9

.field public static final square_card_upsell_spacing:I = 0x7f0704fa

.field public static final square_card_upsell_spacing_small:I = 0x7f0704fb


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
