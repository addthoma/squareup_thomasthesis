.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter$DefaultImpls;
.super Ljava/lang/Object;
.source "SquareCardOrderedScreenWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static adapter(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;)Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;",
            ")",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    .line 26
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter$adapter$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter$adapter$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 27
    new-instance v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter$adapter$2;

    invoke-direct {v2, p0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter$adapter$2;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 25
    invoke-direct {v0, v1, v2}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method
