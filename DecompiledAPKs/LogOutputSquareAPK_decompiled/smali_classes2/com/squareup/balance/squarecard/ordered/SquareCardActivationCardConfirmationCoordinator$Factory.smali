.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;
.super Ljava/lang/Object;
.source "SquareCardActivationCardConfirmationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J$\u0010\u0007\u001a\u00020\u00082\u001c\u0010\t\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000bj\u0002`\u000e0\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;",
        "",
        "hudToaster",
        "Lcom/squareup/hudtoaster/HudToaster;",
        "swipeBusWhenVisible",
        "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
        "(Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;)V",
        "create",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationScreen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final swipeBusWhenVisible:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;


# direct methods
.method public constructor <init>(Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "hudToaster"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "swipeBusWhenVisible"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;->swipeBusWhenVisible:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
            ">;>;)",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;

    .line 54
    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$Factory;->swipeBusWhenVisible:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    const/4 v3, 0x0

    .line 53
    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
