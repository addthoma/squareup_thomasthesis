.class final Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$start$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSquareCardOrderedWorkflowStarter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->start(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/workflow/rx1/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
        "Lcom/squareup/workflow/ScreenState<",
        "+",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a*\u0012&\u0012$\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u00050\u0002j\u0008\u0012\u0004\u0012\u00020\u0003`\u00060\u00012\u0006\u0010\u0007\u001a\u00020\u0008H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "it",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/rx1/Workflow;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;Lcom/squareup/workflow/rx1/Workflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$start$1;->this$0:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$start$1;->$workflow:Lcom/squareup/workflow/rx1/Workflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)Lcom/squareup/workflow/ScreenState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
            ")",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$start$1;->this$0:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;

    new-instance v1, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$start$1;->$workflow:Lcom/squareup/workflow/rx1/Workflow;

    check-cast v2, Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {v1, v2}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    invoke-static {v0, p1, v1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;->access$toWorkflowState(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/ScreenState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 96
    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$start$1;->invoke(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)Lcom/squareup/workflow/ScreenState;

    move-result-object p1

    return-object p1
.end method
