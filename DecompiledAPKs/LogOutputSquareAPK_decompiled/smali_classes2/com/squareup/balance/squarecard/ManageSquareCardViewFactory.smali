.class public final Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "ManageSquareCardViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 17
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 17
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 18
    sget-object v2, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;->Companion:Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Companion;->getDETAIL_KEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 19
    sget v3, Lcom/squareup/balance/core/R$layout;->bizbank_progress_layout:I

    .line 20
    new-instance v16, Lcom/squareup/workflow/ScreenHint;

    const-class v8, Lcom/squareup/balance/squarecard/ManageSquareCardSection;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x1f7

    const/4 v15, 0x0

    move-object/from16 v4, v16

    invoke-direct/range {v4 .. v15}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 21
    sget-object v4, Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory$1;->INSTANCE:Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory$1;

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object/from16 v4, v16

    .line 17
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 22
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 23
    sget-object v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;->Companion:Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Companion;->getSHEET_KEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 24
    sget v5, Lcom/squareup/balance/core/R$layout;->bizbank_progress_layout:I

    .line 25
    sget-object v1, Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory$2;->INSTANCE:Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory$2;

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0xc

    .line 22
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 26
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 27
    sget-object v1, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;->Companion:Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 28
    sget v5, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_code_confirmation_view:I

    .line 29
    sget-object v1, Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory$3;->INSTANCE:Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory$3;

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 26
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 32
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 33
    sget-object v1, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen;->Companion:Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 34
    sget v5, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_activation_create_pin_view:I

    .line 35
    sget-object v1, Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory$4;->INSTANCE:Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory$4;

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 32
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    move-object/from16 v1, p0

    .line 16
    invoke-direct {v1, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
