.class final Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$6;
.super Lkotlin/jvm/internal/Lambda;
.source "RealManageSquareCardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->render(Lkotlin/Unit;Lcom/squareup/balance/squarecard/ManageSquareCardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
        "+",
        "Lkotlin/Unit;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
        "",
        "workflowResult",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/squarecard/ManageSquareCardState;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;Lcom/squareup/balance/squarecard/ManageSquareCardState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$6;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$6;->$state:Lcom/squareup/balance/squarecard/ManageSquareCardState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "workflowResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult$Back;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult$Back;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$6;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->access$finishOrRefetchCardStatus(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 191
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult$ReportProblem;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult$ReportProblem;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 192
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$6;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->access$getStateFactory$p(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$6;->$state:Lcom/squareup/balance/squarecard/ManageSquareCardState;

    check-cast v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->startCancelingOrderedCard(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingOrderedCard;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 193
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult$Error;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult$Error;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$6;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->access$finishOrRefetchCardStatus(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 194
    :cond_2
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult$CardActivated;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult$CardActivated;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 195
    iget-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$6;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->access$refreshAccountStatusResponse(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)V

    .line 196
    iget-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$6;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->access$finishOrRefetchCardStatus(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 104
    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$6;->invoke(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
