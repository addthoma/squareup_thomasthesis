.class final Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SubmitFeedbackWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;->render(Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;",
        "+",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;",
        "output",
        "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackOutput$ExitWithoutFeedback;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackOutput$ExitWithoutFeedback;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$NoFeedbackSubmitted;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$NoFeedbackSubmitted;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 98
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackOutput$Feedback;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$SubmitFeedback;

    check-cast p1, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackOutput$Feedback;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackOutput$Feedback;->getFeedback()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action$SubmitFeedback;-><init>(Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 73
    check-cast p1, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$1;->invoke(Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
