.class public final Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "SubmitFeedbackWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSubmitFeedbackWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SubmitFeedbackWorkflow.kt\ncom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,155:1\n32#2,12:156\n*E\n*S KotlinDebug\n*F\n+ 1 SubmitFeedbackWorkflow.kt\ncom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow\n*L\n87#1,12:156\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u000020\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00070\u0006j\u0006\u0012\u0002\u0008\u0003`\u00080\u0001:\u0001\u001cB?\u0008\u0007\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\n\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\n\u0012\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\n\u00a2\u0006\u0002\u0010\u0012J\u001e\u0010\u0013\u001a\u00020\u00042\n\u0010\u0014\u001a\u00060\u0002j\u0002`\u00032\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016JB\u0010\u0017\u001a\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00070\u0006j\u0006\u0012\u0002\u0008\u0003`\u00082\n\u0010\u0014\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0018\u001a\u00020\u00042\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u0004H\u0016R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackProps;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "promptForCardFeedbackWorkflow",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow;",
        "submittingCardFeedbackWorkflow",
        "Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;",
        "submitFeedbackSuccessWorkflow",
        "Lcom/squareup/balance/squarecard/cancel/feedback/success/SubmitFeedbackSuccessWorkflow;",
        "submitFeedbackFailureWorkflow",
        "Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;",
        "(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final promptForCardFeedbackWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final submitFeedbackFailureWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final submitFeedbackSuccessWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/success/SubmitFeedbackSuccessWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final submittingCardFeedbackWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/success/SubmitFeedbackSuccessWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "promptForCardFeedbackWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "submittingCardFeedbackWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "submitFeedbackSuccessWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "submitFeedbackFailureWorkflow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;->promptForCardFeedbackWorkflow:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;->submittingCardFeedbackWorkflow:Ljavax/inject/Provider;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;->submitFeedbackSuccessWorkflow:Ljavax/inject/Provider;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;->submitFeedbackFailureWorkflow:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 156
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 161
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 163
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 164
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 165
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 167
    :cond_3
    check-cast v1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 87
    :cond_4
    sget-object p1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$PromptingForFeedback;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$PromptingForFeedback;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 73
    check-cast p1, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;->initialState(Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;",
            "-",
            "Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackOutput;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$PromptingForFeedback;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$PromptingForFeedback;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;->promptForCardFeedbackWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string p2, "promptForCardFeedbackWorkflow.get()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v2, 0x0

    sget-object p1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$1;

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    goto/16 :goto_0

    .line 101
    :cond_0
    instance-of v0, p2, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$SubmittingFeedback;

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;->submittingCardFeedbackWorkflow:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "submittingCardFeedbackWorkflow.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v0

    check-cast v3, Lcom/squareup/workflow/Workflow;

    .line 103
    new-instance v4, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackProps;

    .line 104
    check-cast p2, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$SubmittingFeedback;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$SubmittingFeedback;->getFeedback()Ljava/lang/String;

    move-result-object p2

    .line 103
    invoke-direct {v4, p2, p1}, Lcom/squareup/balance/squarecard/cancel/feedback/submitting/SubmittingCardFeedbackProps;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;)V

    const/4 v5, 0x0

    .line 107
    sget-object p1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$2;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$2;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    .line 101
    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    goto :goto_0

    .line 114
    :cond_1
    sget-object p1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$ShowingSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$ShowingSuccess;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;->submitFeedbackSuccessWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string p2, "submitFeedbackSuccessWorkflow.get()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v2, 0x0

    sget-object p1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$3;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$3;

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    goto :goto_0

    .line 117
    :cond_2
    sget-object p1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$ShowingFailure;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState$ShowingFailure;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;->submitFeedbackFailureWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string p2, "submitFeedbackFailureWorkflow.get()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v2, 0x0

    sget-object p1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$4;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow$render$4;

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 73
    check-cast p1, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    check-cast p2, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;->render(Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 73
    check-cast p1, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackWorkflow;->snapshotState(Lcom/squareup/balance/squarecard/cancel/feedback/SubmitFeedbackState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
