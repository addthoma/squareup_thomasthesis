.class public final Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "PromptForCardFeedbackWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPromptForCardFeedbackWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PromptForCardFeedbackWorkflow.kt\ncom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,39:1\n149#2,5:40\n*E\n*S KotlinDebug\n*F\n+ 1 PromptForCardFeedbackWorkflow.kt\ncom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow\n*L\n36#1,5:40\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002*\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u00070\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0008J?\u0010\t\u001a\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u00072\n\u0010\n\u001a\u00060\u0002j\u0002`\u00032\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00040\u000cH\u0016\u00a2\u0006\u0002\u0010\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "",
        "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackProps;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "()V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    sget-object p1, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow$render$sink$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow$render$sink$1;

    check-cast p1, Lkotlin/jvm/functions/Function2;

    invoke-static {p2, p1}, Lcom/squareup/workflow/RenderContextKt;->makeEventSink(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Sink;

    move-result-object p1

    .line 31
    new-instance p2, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackRendering;

    .line 32
    new-instance v0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow$render$1;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 35
    new-instance v1, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow$render$2;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 31
    invoke-direct {p2, v0, v1}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackRendering;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 41
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 42
    const-class v0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackRendering;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 43
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 41
    invoke-direct {p1, v0, p2, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
