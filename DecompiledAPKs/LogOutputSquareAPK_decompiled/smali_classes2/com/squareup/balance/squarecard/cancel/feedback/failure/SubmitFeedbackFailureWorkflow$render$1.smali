.class final Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SubmitFeedbackFailureWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;Lcom/squareup/workflow/RenderContext;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow$render$1;->this$0:Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow$render$1;->$context:Lcom/squareup/workflow/RenderContext;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow$render$1;->invoke(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow$render$1;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {p1}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow$render$1;->this$0:Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;->access$getFinish$p(Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
