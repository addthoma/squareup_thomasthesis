.class public final Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "CancelSquareCardReasonsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsOutput;",
        "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\u0018\u00002\u0018\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0006J-\u0010\u0007\u001a\u00020\u00052\n\u0010\u0008\u001a\u00060\u0002j\u0002`\u00032\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00040\nH\u0016\u00a2\u0006\u0002\u0010\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "",
        "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsProps;",
        "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsOutput;",
        "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;",
        "()V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    sget-object p1, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow$render$sink$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow$render$sink$1;

    check-cast p1, Lkotlin/jvm/functions/Function2;

    invoke-static {p2, p1}, Lcom/squareup/workflow/RenderContextKt;->makeEventSink(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Sink;

    move-result-object p1

    .line 30
    new-instance p2, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;

    .line 31
    new-instance v0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow$render$1;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 32
    new-instance v1, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow$render$2;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 30
    invoke-direct {p2, v0, v1}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    return-object p2
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;

    move-result-object p1

    return-object p1
.end method
