.class public final Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;
.super Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;
.source "CancelSquareCardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowingSuccessMaybeDisableBizbank"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000b\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\u000c\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\u0013\u0010\u000f\u001a\u00020\u00052\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\u0019\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u000eH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\t\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
        "deactivationReason",
        "Lcom/squareup/balance/squarecard/cancel/DeactivationReason;",
        "isBizBankActive",
        "",
        "(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Z)V",
        "getDeactivationReason",
        "()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;",
        "()Z",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final deactivationReason:Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

.field private final isBizBankActive:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank$Creator;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank$Creator;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Z)V
    .locals 1

    const-string v0, "deactivationReason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->deactivationReason:Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    iput-boolean p2, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->isBizBankActive:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;Lcom/squareup/balance/squarecard/cancel/DeactivationReason;ZILjava/lang/Object;)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->deactivationReason:Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->isBizBankActive:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->copy(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Z)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->deactivationReason:Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->isBizBankActive:Z

    return v0
.end method

.method public final copy(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Z)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;
    .locals 1

    const-string v0, "deactivationReason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;

    invoke-direct {v0, p1, p2}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;-><init>(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Z)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->deactivationReason:Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->deactivationReason:Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->isBizBankActive:Z

    iget-boolean p1, p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->isBizBankActive:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDeactivationReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->deactivationReason:Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->deactivationReason:Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->isBizBankActive:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final isBizBankActive()Z
    .locals 1

    .line 30
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->isBizBankActive:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShowingSuccessMaybeDisableBizbank(deactivationReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->deactivationReason:Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isBizBankActive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->isBizBankActive:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->deactivationReason:Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-boolean p2, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState$ShowingSuccessMaybeDisableBizbank;->isBizBankActive:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
