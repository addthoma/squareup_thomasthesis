.class public final Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealViewBillingAddressWorkflow.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressInput;",
        "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealViewBillingAddressWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealViewBillingAddressWorkflow.kt\ncom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 6 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,138:1\n32#2,12:139\n149#3,5:151\n85#4:156\n240#5:157\n276#6:158\n*E\n*S KotlinDebug\n*F\n+ 1 RealViewBillingAddressWorkflow.kt\ncom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow\n*L\n46#1,12:139\n89#1,5:151\n133#1:156\n133#1:157\n133#1:158\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u000128\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\"\u0012 \u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n0\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJB\u0010\u000e\u001a \u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u0006\u0010\u000f\u001a\u00020\u00102\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00050\u0012H\u0002J:\u0010\u0014\u001a \u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00050\u0012H\u0002J\u000e\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0017H\u0002J:\u0010\u0018\u001a \u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00050\u0012H\u0002J\u001a\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u00032\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016JR\u0010\u001d\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\u001e2\u0006\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u00042\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050 H\u0016J\u0010\u0010!\u001a\u00020\u001c2\u0006\u0010\u000f\u001a\u00020\u0004H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;",
        "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressInput;",
        "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;",
        "",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressScreen;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V",
        "displayAddressScreen",
        "state",
        "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$DisplayAddress;",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen$Event;",
        "errorFetchingAddressScreen",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
        "fetchBillingAddress",
        "Lcom/squareup/workflow/Worker;",
        "fetchingAddressScreen",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "Lcom/squareup/workflow/LayeredScreen;",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizbankService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    return-void
.end method

.method private final displayAddressScreen(Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$DisplayAddress;Lkotlin/jvm/functions/Function1;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$DisplayAddress;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 85
    new-instance v0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;

    .line 86
    sget-object v1, Lcom/squareup/address/Address;->Companion:Lcom/squareup/address/Address$Companion;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$DisplayAddress;->getAddress()Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/address/Address$Companion;->fromGlobalAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;

    move-result-object p1

    .line 85
    invoke-direct {v0, p1, p2}, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;-><init>(Lcom/squareup/address/Address;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 152
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 153
    const-class p2, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 154
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 152
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 90
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final errorFetchingAddressScreen(Lkotlin/jvm/functions/Function1;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 96
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 97
    new-instance v8, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;

    .line 98
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->view_billing_address_error_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v2, v1

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 99
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->view_billing_address_error_message:I

    invoke-direct {v1, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/util/ViewString;

    .line 100
    sget v4, Lcom/squareup/common/strings/R$string;->okay:I

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, v8

    .line 97
    invoke-direct/range {v1 .. v7}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v8, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 96
    invoke-direct {v0, v8, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 103
    invoke-static {v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asDetailScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 104
    sget-object v0, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final fetchBillingAddress()Lcom/squareup/workflow/Worker;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;",
            ">;"
        }
    .end annotation

    .line 121
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest$Builder;-><init>()V

    .line 122
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest;

    move-result-object v0

    .line 124
    iget-object v1, p0, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v2, "request"

    .line 125
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->getCardBillingAddress(Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 127
    sget-object v1, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$fetchBillingAddress$1;->INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$fetchBillingAddress$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "bizbankService\n        .\u2026ess\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    sget-object v1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$fetchBillingAddress$$inlined$asWorker$1;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$fetchBillingAddress$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 157
    invoke-static {v1}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 158
    const-class v1, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v2, Lcom/squareup/workflow/Worker;

    return-object v2
.end method

.method private final fetchingAddressScreen(Lkotlin/jvm/functions/Function1;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 110
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 111
    new-instance v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;

    .line 112
    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->view_billing_address_loading:I

    .line 111
    invoke-direct {v1, v2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;-><init>(I)V

    check-cast v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 110
    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 115
    invoke-static {v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asDetailScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 116
    sget-object v0, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 139
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 144
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 146
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 147
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 148
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 150
    :cond_3
    check-cast v1, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 46
    :cond_4
    sget-object p1, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$FetchingAddress;->INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$FetchingAddress;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;->initialState(Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressInput;

    check-cast p2, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;->render(Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressInput;Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressInput;Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressInput;",
            "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    sget-object p1, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$FetchingAddress;->INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$FetchingAddress;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;->fetchBillingAddress()Lcom/squareup/workflow/Worker;

    move-result-object v1

    const/4 v2, 0x0

    sget-object p1, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$render$1;->INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$render$1;

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->onWorkerOutput$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 59
    new-instance p1, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$render$2;

    invoke-direct {p1, p2}, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$render$2;-><init>(Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;->fetchingAddressScreen(Lkotlin/jvm/functions/Function1;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 66
    :cond_0
    sget-object p1, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$ErrorFetchingAddress;->INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$ErrorFetchingAddress;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$render$3;

    invoke-direct {p1, p2}, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$render$3;-><init>(Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;->errorFetchingAddressScreen(Lkotlin/jvm/functions/Function1;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 73
    :cond_1
    instance-of p1, p2, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$DisplayAddress;

    if-eqz p1, :cond_2

    check-cast p2, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$DisplayAddress;

    sget-object p1, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$render$4;->INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$render$4;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;->displayAddressScreen(Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$DisplayAddress;Lkotlin/jvm/functions/Function1;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;->snapshotState(Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
