.class final Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$fetchBillingAddress$1;
.super Ljava/lang/Object;
.source "RealViewBillingAddressWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;->fetchBillingAddress()Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;",
        "result",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$fetchBillingAddress$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$fetchBillingAddress$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$fetchBillingAddress$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$fetchBillingAddress$1;->INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$fetchBillingAddress$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;",
            ">;)",
            "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$DisplayAddress;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;->billing_address:Lcom/squareup/protos/common/location/GlobalAddress;

    const-string v1, "result.response.billing_address"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$DisplayAddress;-><init>(Lcom/squareup/protos/common/location/GlobalAddress;)V

    check-cast v0, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;

    goto :goto_0

    .line 130
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$ErrorFetchingAddress;->INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState$ErrorFetchingAddress;

    move-object v0, p1

    check-cast v0, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow$fetchBillingAddress$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressState;

    move-result-object p1

    return-object p1
.end method
