.class public final Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;
.super Ljava/lang/Object;
.source "NotificationDisplayPreferencesLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationDisplayPreferencesLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationDisplayPreferencesLayoutRunner.kt\ncom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner\n*L\n1#1,169:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 $2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001$B\u000f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0016\u001a\u00020\u0013H\u0002J\u0008\u0010\u000e\u001a\u00020\u000fH\u0002J\u0010\u0010\u0017\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0002H\u0002J\u0008\u0010\u0019\u001a\u00020\u001aH\u0002J\u0018\u0010\u001b\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0010\u0010\u001e\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0002H\u0002J\u0010\u0010\u001f\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0002H\u0002J\u0010\u0010 \u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0002H\u0002J\u0010\u0010!\u001a\u00020\u00132\u0006\u0010\"\u001a\u00020#H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000c\u001a\n \n*\u0004\u0018\u00010\r0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0011\u001a\u0010\u0012\u000c\u0012\n \n*\u0004\u0018\u00010\u00130\u00130\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "cardDeclines",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "kotlin.jvm.PlatformType",
        "emailChecked",
        "emailField",
        "Lcom/squareup/noho/NohoRow;",
        "errorSnackbar",
        "Lcom/google/android/material/snackbar/Snackbar;",
        "latestRendering",
        "onPrefsUpdatedRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "showBackButton",
        "",
        "dismissError",
        "maybeShowError",
        "rendering",
        "notificationPreferences",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "updateEmailField",
        "updatePreferences",
        "updateReceiverType",
        "receiverType",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final cardDeclines:Lcom/squareup/noho/NohoCheckableRow;

.field private final emailChecked:Lcom/squareup/noho/NohoCheckableRow;

.field private final emailField:Lcom/squareup/noho/NohoRow;

.field private errorSnackbar:Lcom/google/android/material/snackbar/Snackbar;

.field private latestRendering:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;

.field private final onPrefsUpdatedRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final showBackButton:Z

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->Companion:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->view:Landroid/view/View;

    .line 35
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 36
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->notification_preferences_email_value_row:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->emailField:Lcom/squareup/noho/NohoRow;

    .line 38
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->notification_preferences_email_row:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->emailChecked:Lcom/squareup/noho/NohoCheckableRow;

    .line 40
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->notification_preferences_pref_card_declines:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->cardDeclines:Lcom/squareup/noho/NohoCheckableRow;

    .line 41
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string v0, "PublishRelay.create<Unit>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->onPrefsUpdatedRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 45
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->view:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->showBackButton:Z

    .line 50
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 48
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 49
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->notification_preferences_action_bar_title:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->onPrefsUpdatedRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 56
    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$1;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    const-wide/16 v0, 0x1

    .line 57
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Lio/reactivex/Observable;->debounce(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "onPrefsUpdatedRelay\n    \u2026dSchedulers.mainThread())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->view:Landroid/view/View;

    new-instance v1, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$2;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$2;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 67
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$3;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$3;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->onDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static final synthetic access$dismissError(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->dismissError()V

    return-void
.end method

.method public static final synthetic access$getCardDeclines$p(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;)Lcom/squareup/noho/NohoCheckableRow;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->cardDeclines:Lcom/squareup/noho/NohoCheckableRow;

    return-object p0
.end method

.method public static final synthetic access$getLatestRendering$p(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;)Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->latestRendering:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;

    return-object p0
.end method

.method public static final synthetic access$getOnPrefsUpdatedRelay$p(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->onPrefsUpdatedRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$notificationPreferences(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;)Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->notificationPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setLatestRendering$p(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->latestRendering:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;

    return-void
.end method

.method public static final synthetic access$updateReceiverType(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->updateReceiverType(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;)V

    return-void
.end method

.method private final dismissError()V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->errorSnackbar:Lcom/google/android/material/snackbar/Snackbar;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/material/snackbar/Snackbar;->dismiss()V

    :cond_0
    return-void
.end method

.method private final errorSnackbar()Lcom/google/android/material/snackbar/Snackbar;
    .locals 3

    .line 101
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->errorSnackbar:Lcom/google/android/material/snackbar/Snackbar;

    if-nez v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->view:Landroid/view/View;

    .line 105
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->notification_preferences_inscreen_error_updating_notifications:I

    const/4 v2, 0x0

    .line 103
    invoke-static {v0, v1, v2}, Lcom/google/android/material/snackbar/Snackbar;->make(Landroid/view/View;II)Lcom/google/android/material/snackbar/Snackbar;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/balance/squarecard/impl/R$color;->notification_preferences_snackbar_color:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 108
    invoke-virtual {v0, v1}, Lcom/google/android/material/snackbar/Snackbar;->setTextColor(I)Lcom/google/android/material/snackbar/Snackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->errorSnackbar:Lcom/google/android/material/snackbar/Snackbar;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->errorSnackbar:Lcom/google/android/material/snackbar/Snackbar;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    return-object v0
.end method

.method private final maybeShowError(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;)V
    .locals 1

    .line 93
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;->getShowError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->errorSnackbar()Lcom/google/android/material/snackbar/Snackbar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/material/snackbar/Snackbar;->show()V

    .line 96
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;->getOnErrorShown()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private final notificationPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;
    .locals 4

    .line 133
    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    .line 134
    new-instance v1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreference;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->cardDeclines:Lcom/squareup/noho/NohoCheckableRow;

    const-string v3, "cardDeclines"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/noho/NohoCheckableRow;->isChecked()Z

    move-result v2

    invoke-direct {v1, v2}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreference;-><init>(Z)V

    .line 133
    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreference;)V

    return-object v0
.end method

.method private final updateActionBar(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;)V
    .locals 4

    .line 125
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 117
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 119
    iget-boolean v2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->showBackButton:Z

    if-eqz v2, :cond_0

    .line 120
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$updateActionBar$$inlined$apply$lambda$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$updateActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 122
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 125
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateEmailField(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;)V
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->emailField:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;->getEmail()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updatePreferences(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;)V
    .locals 2

    .line 139
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;->getPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;->getCardDeclines()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreference;

    move-result-object p1

    .line 140
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->cardDeclines:Lcom/squareup/noho/NohoCheckableRow;

    sget-object v1, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$updatePreferences$1$1;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$updatePreferences$1$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->cardDeclines:Lcom/squareup/noho/NohoCheckableRow;

    const-string v1, "cardDeclines"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreference;->isEnabled()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 142
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->cardDeclines:Lcom/squareup/noho/NohoCheckableRow;

    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$updatePreferences$$inlined$run$lambda$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$updatePreferences$$inlined$run$lambda$1;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method private final updateReceiverType(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;)V
    .locals 2

    .line 153
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->emailChecked:Lcom/squareup/noho/NohoCheckableRow;

    sget-object v1, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$updateReceiverType$1;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$updateReceiverType$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->emailChecked:Lcom/squareup/noho/NohoCheckableRow;

    const-string v1, "emailChecked"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;->EMAIL:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 155
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->emailChecked:Lcom/squareup/noho/NohoCheckableRow;

    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$updateReceiverType$2;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$updateReceiverType$2;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->latestRendering:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->updateActionBar(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;)V

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->updateEmailField(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;)V

    .line 81
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->updatePreferences(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;)V

    .line 82
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;->getReceiverType()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->updateReceiverType(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;)V

    .line 83
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->maybeShowError(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;)V

    .line 85
    iget-object p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$showRendering$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$showRendering$1;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->showRendering(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
