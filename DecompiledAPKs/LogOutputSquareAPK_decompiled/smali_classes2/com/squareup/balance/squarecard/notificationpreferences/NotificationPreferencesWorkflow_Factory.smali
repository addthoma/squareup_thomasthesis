.class public final Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;
.super Ljava/lang/Object;
.source "NotificationPreferencesWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow;",
            ">;)",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow;",
            ">;)",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;
    .locals 3

    .line 32
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;->newInstance(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow_Factory;->get()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;

    move-result-object v0

    return-object v0
.end method
