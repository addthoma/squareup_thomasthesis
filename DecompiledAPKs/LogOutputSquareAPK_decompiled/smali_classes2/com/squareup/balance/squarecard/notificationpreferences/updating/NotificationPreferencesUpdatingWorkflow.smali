.class public final Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "NotificationPreferencesUpdatingWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationPreferencesUpdatingWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationPreferencesUpdatingWorkflow.kt\ncom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,127:1\n32#2,12:128\n85#3:140\n240#4:141\n276#5:142\n149#6,5:143\n*E\n*S KotlinDebug\n*F\n+ 1 NotificationPreferencesUpdatingWorkflow.kt\ncom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow\n*L\n65#1,12:128\n75#1:140\n75#1:141\n75#1:142\n86#1,5:143\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0001\u001aB\u001d\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0002\u0010\u000fJ\u001e\u0010\u0010\u001a\u00060\u0003j\u0002`\u00112\u0006\u0010\u0012\u001a\u00020\u00022\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016JV\u0010\u0015\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0012\u001a\u00020\u00022\n\u0010\u0016\u001a\u00060\u0003j\u0002`\u00112\u0016\u0010\u0017\u001a\u0012\u0012\u0008\u0012\u00060\u0003j\u0002`\u0011\u0012\u0004\u0012\u00020\u00040\u0018H\u0016J\u0014\u0010\u0019\u001a\u00020\u00142\n\u0010\u0016\u001a\u00060\u0003j\u0002`\u0011H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "dataStore",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;",
        "errorWorkflow",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow;",
        "(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;Ljavax/inject/Provider;)V",
        "initialState",
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/State;",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dataStore:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;

.field private final errorWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "dataStore"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow;->dataStore:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow;->errorWorkflow:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 128
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 133
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 135
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 136
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 137
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 139
    :cond_3
    check-cast v1, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 65
    :cond_4
    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState$UpdatingPreferences;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState$UpdatingPreferences;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow;->initialState(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    check-cast p2, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow;->render(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;",
            "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;",
            "-",
            "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    sget-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState$UpdatingPreferences;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState$UpdatingPreferences;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow;->dataStore:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;

    invoke-interface {p2, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;->updatePreferences(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;)Lio/reactivex/Single;

    move-result-object p1

    .line 140
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$render$$inlined$asWorker$1;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 141
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 142
    const-class p2, Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Worker;

    const/4 v3, 0x0

    .line 75
    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$render$1;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$render$1;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p3

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 83
    new-instance p1, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingScreen;

    .line 85
    new-instance p2, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$render$2;

    invoke-direct {p2, p3}, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$render$2;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 83
    invoke-direct {p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 144
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 145
    const-class p3, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    const-string v0, ""

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 146
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 144
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 87
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 89
    :cond_0
    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState$ErrorUpdatingPreferences;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState$ErrorUpdatingPreferences;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 91
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow;->errorWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string p2, "errorWorkflow.get()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 92
    new-instance v2, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;

    .line 93
    new-instance p1, Lcom/squareup/resources/ResourceString;

    .line 94
    sget p2, Lcom/squareup/balance/squarecard/impl/R$string;->notification_preferences_error_updating_notifications_title:I

    .line 93
    invoke-direct {p1, p2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast p1, Lcom/squareup/resources/TextModel;

    const/4 p2, 0x1

    .line 92
    invoke-direct {v2, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;-><init>(Lcom/squareup/resources/TextModel;Z)V

    const/4 v3, 0x0

    .line 98
    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$render$3;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$render$3;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 90
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow;->snapshotState(Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
