.class public final Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealSquareCardActivatedWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/secure/SecureScopeManager;",
            ">;"
        }
    .end annotation
.end field

.field private final arg10Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/secure/SecureScopeManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;",
            ">;)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 53
    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 54
    iput-object p3, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 55
    iput-object p4, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 56
    iput-object p5, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 57
    iput-object p6, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 58
    iput-object p7, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 59
    iput-object p8, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 60
    iput-object p9, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg8Provider:Ljavax/inject/Provider;

    .line 61
    iput-object p10, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg9Provider:Ljavax/inject/Provider;

    .line 62
    iput-object p11, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg10Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/secure/SecureScopeManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;",
            ">;)",
            "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;"
        }
    .end annotation

    .line 80
    new-instance v12, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/secure/SecureScopeManager;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;)Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/secure/SecureScopeManager;",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow;",
            ">;",
            "Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;",
            ")",
            "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;"
        }
    .end annotation

    .line 90
    new-instance v12, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;-><init>(Lcom/squareup/secure/SecureScopeManager;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;
    .locals 12

    .line 67
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/secure/SecureScopeManager;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    iget-object v7, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    iget-object v8, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg7Provider:Ljavax/inject/Provider;

    iget-object v9, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg8Provider:Ljavax/inject/Provider;

    iget-object v10, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg9Provider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->arg10Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;

    invoke-static/range {v1 .. v11}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->newInstance(Lcom/squareup/secure/SecureScopeManager;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;)Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow_Factory;->get()Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    move-result-object v0

    return-object v0
.end method
