.class final Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$4;
.super Ljava/lang/Object;
.source "SquareCardAddToGooglePayHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->addCardToGooglePay(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardAddToGooglePayHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardAddToGooglePayHelper.kt\ncom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$4\n*L\n1#1,173:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\"\u0010\u0003\u001a\u001e\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00070\u00070\u0004H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/googlepay/GooglePayResponse;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/googlepay/GooglePayAddress;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$4;->this$0:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$4;->$card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Lcom/squareup/googlepay/GooglePayAddress;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/googlepay/GooglePayResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/googlepay/GooglePayAddress;

    .line 60
    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$4;->this$0:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->access$getGooglePay$p(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;)Lcom/squareup/googlepay/GooglePay;

    move-result-object v1

    const-string v2, "opaquePaymentCard"

    .line 61
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lkotlin/text/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    const-string v2, "(this as java.lang.String).getBytes(charset)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$4;->$card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iget-object v2, v2, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->pan_last_four:Ljava/lang/String;

    const-string v3, "card.pan_last_four"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    iget-object v3, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$4;->this$0:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    invoke-static {v3}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->access$getContext$p(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;)Landroid/app/Application;

    move-result-object v3

    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->square_card:I

    invoke-virtual {v3, v4}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "context.getString(R.string.square_card)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "googlePayAddress"

    .line 64
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-interface {v1, v0, v2, v3, p1}, Lcom/squareup/googlepay/GooglePay;->pushTokenize([BLjava/lang/String;Ljava/lang/String;Lcom/squareup/googlepay/GooglePayAddress;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 61
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$4;->apply(Lkotlin/Pair;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
