.class public final Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;
.super Ljava/lang/Object;
.source "RealSquareCardActivatedAnalytics.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0002J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0002J\u0008\u0010\n\u001a\u00020\u0006H\u0016J\u0008\u0010\u000b\u001a\u00020\u0006H\u0016J\u0008\u0010\u000c\u001a\u00020\u0006H\u0016J\u0008\u0010\r\u001a\u00020\u0006H\u0016J\u0008\u0010\u000e\u001a\u00020\u0006H\u0016J\u0008\u0010\u000f\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;",
        "squareCardAnalyticsLogger",
        "Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;",
        "(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V",
        "logClick",
        "",
        "description",
        "",
        "logImpression",
        "logSquareCardActivatedScreen",
        "logSquareCardAddToGooglePayCancelClick",
        "logSquareCardAddToGooglePayClick",
        "logSquareCardAddToGooglePayErrorScreen",
        "logSquareCardAddToGooglePaySuccessScreen",
        "logSquareCardSuspendedScreen",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "squareCardAnalyticsLogger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    return-void
.end method

.method private final logClick(Ljava/lang/String;)V
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method private final logImpression(Ljava/lang/String;)V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;->logImpression(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public logSquareCardActivatedScreen()V
    .locals 1

    const-string v0, "Square Card: Active Page"

    .line 20
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logSquareCardAddToGooglePayCancelClick()V
    .locals 1

    const-string v0, "Add To Digital Wallet: Cancel"

    .line 32
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logSquareCardAddToGooglePayClick()V
    .locals 1

    const-string v0, "Square Card: Add To Digital Wallet"

    .line 28
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logSquareCardAddToGooglePayErrorScreen()V
    .locals 1

    const-string v0, "Add To Digital Wallet: Error"

    .line 40
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logSquareCardAddToGooglePaySuccessScreen()V
    .locals 1

    const-string v0, "Add To Digital Wallet: Success"

    .line 36
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logSquareCardSuspendedScreen()V
    .locals 1

    const-string v0, "Square Card: Suspended"

    .line 24
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method
