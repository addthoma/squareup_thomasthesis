.class final Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;
.super Lkotlin/jvm/internal/Lambda;
.source "SquareCardActivatedWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->render(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
        "+",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardActivatedWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardActivatedWorkflow.kt\ncom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5\n*L\n1#1,509:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult;",
        "event",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screenState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

.field final synthetic $state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->$screenState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$GoBack;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult$Back;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult$Back;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 209
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$GetHelpWithCard;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$GetHelpWithCard;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult$ReportProblem;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult$ReportProblem;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 210
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$AddToGooglePay;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$AddToGooglePay;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 211
    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->access$getAnalytics$p(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;->logSquareCardAddToGooglePayClick()V

    .line 212
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    sget-object v3, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLoadingGooglePay;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLoadingGooglePay;

    check-cast v3, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    invoke-static {v0, v3}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflowKt;->access$plus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object v0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 214
    :cond_2
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$ResetCardPin;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$ResetCardPin;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    sget-object v3, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$StartResetPin;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$StartResetPin;

    check-cast v3, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    invoke-static {v0, v3}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflowKt;->access$plus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object v0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 215
    :cond_3
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$LearnMoreAboutSuspendedCard;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$LearnMoreAboutSuspendedCard;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 216
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    sget-object v3, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLearnMoreAboutSuspendedCardDialog;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLearnMoreAboutSuspendedCardDialog;

    check-cast v3, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    invoke-static {v0, v3}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflowKt;->access$plus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object v0

    .line 215
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 218
    :cond_4
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$ToggleCard;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$ToggleCard;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 219
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 220
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    iget-object v3, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    invoke-static {v0, v3}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->access$maybeResetPrivateCardDataState(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object v0

    .line 221
    iget-object v3, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->$screenState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    move-object v4, v3

    check-cast v4, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x5

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;->copy$default(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;ZZZILjava/lang/Object;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    move-result-object v3

    check-cast v3, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    .line 220
    invoke-static {v0, v3}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflowKt;->access$plus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object v0

    .line 219
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    .line 223
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->access$getSquareCardDataRequester$p(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/balance/squarecard/SquareCardDataRequester;->toggleCard(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    goto :goto_0

    .line 226
    :cond_5
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$ShowBillingAddress;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$ShowBillingAddress;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    sget-object v3, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayBillingAddress;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayBillingAddress;

    check-cast v3, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    invoke-static {v0, v3}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflowKt;->access$plus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object v0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 227
    :cond_6
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$NotificationPreferencesClicked;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$NotificationPreferencesClicked;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    sget-object v3, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayNotificationPreferences;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$DisplayNotificationPreferences;

    check-cast v3, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    invoke-static {v0, v3}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflowKt;->access$plus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object v0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 228
    :cond_7
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$TogglePrivateCardDetails;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event$TogglePrivateCardDetails;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->access$handleCardDetailsToggle(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_8
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 107
    check-cast p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$5;->invoke(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
