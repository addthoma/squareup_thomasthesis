.class public abstract Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch;
.super Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;
.source "SquareCardPrivateDataState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "RequiresDataFetch"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch$RefreshAfterToggle;,
        Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch$RefreshAfterTwoFactor;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0007\u0008B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\t\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;",
        "()V",
        "key",
        "",
        "getKey",
        "()Ljava/lang/String;",
        "RefreshAfterToggle",
        "RefreshAfterTwoFactor",
        "Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch$RefreshAfterToggle;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch$RefreshAfterTwoFactor;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final key:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    .line 64
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 66
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UUID.randomUUID()\n        .toString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch;->key:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch;-><init>()V

    return-void
.end method


# virtual methods
.method public final getKey()Ljava/lang/String;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardPrivateDataState$RequiresDataFetch;->key:Ljava/lang/String;

    return-object v0
.end method
