.class public interface abstract Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;
.super Ljava/lang/Object;
.source "SquareCardActivatedAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0003H&J\u0008\u0010\u0006\u001a\u00020\u0003H&J\u0008\u0010\u0007\u001a\u00020\u0003H&J\u0008\u0010\u0008\u001a\u00020\u0003H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;",
        "",
        "logSquareCardActivatedScreen",
        "",
        "logSquareCardAddToGooglePayCancelClick",
        "logSquareCardAddToGooglePayClick",
        "logSquareCardAddToGooglePayErrorScreen",
        "logSquareCardAddToGooglePaySuccessScreen",
        "logSquareCardSuspendedScreen",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract logSquareCardActivatedScreen()V
.end method

.method public abstract logSquareCardAddToGooglePayCancelClick()V
.end method

.method public abstract logSquareCardAddToGooglePayClick()V
.end method

.method public abstract logSquareCardAddToGooglePayErrorScreen()V
.end method

.method public abstract logSquareCardAddToGooglePaySuccessScreen()V
.end method

.method public abstract logSquareCardSuspendedScreen()V
.end method
