.class public final Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalyticsKt;
.super Ljava/lang/Object;
.source "RealSquareCardActivatedAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "SQUARE_CARD_ACTIVATED_SCREEN",
        "",
        "SQUARE_CARD_ADD_TO_GOOGLE_PAY_CANCEL_CLICK",
        "SQUARE_CARD_ADD_TO_GOOGLE_PAY_CLICK",
        "SQUARE_CARD_ADD_TO_GOOGLE_PAY_ERROR_SCREEN",
        "SQUARE_CARD_ADD_TO_GOOGLE_PAY_SUCCESS_SCREEN",
        "SQUARE_CARD_SUSPENDED_SCREEN",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final SQUARE_CARD_ACTIVATED_SCREEN:Ljava/lang/String; = "Square Card: Active Page"

.field private static final SQUARE_CARD_ADD_TO_GOOGLE_PAY_CANCEL_CLICK:Ljava/lang/String; = "Add To Digital Wallet: Cancel"

.field private static final SQUARE_CARD_ADD_TO_GOOGLE_PAY_CLICK:Ljava/lang/String; = "Square Card: Add To Digital Wallet"

.field private static final SQUARE_CARD_ADD_TO_GOOGLE_PAY_ERROR_SCREEN:Ljava/lang/String; = "Add To Digital Wallet: Error"

.field private static final SQUARE_CARD_ADD_TO_GOOGLE_PAY_SUCCESS_SCREEN:Ljava/lang/String; = "Add To Digital Wallet: Success"

.field private static final SQUARE_CARD_SUSPENDED_SCREEN:Ljava/lang/String; = "Square Card: Suspended"
