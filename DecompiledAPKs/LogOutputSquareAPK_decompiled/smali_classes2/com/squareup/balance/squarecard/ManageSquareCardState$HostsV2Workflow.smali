.class public abstract Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow;
.super Lcom/squareup/balance/squarecard/ManageSquareCardState;
.source "ManageSquareCardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ManageSquareCardState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "HostsV2Workflow"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$AddPhoneNumber;,
        Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;,
        Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingActivatedCard;,
        Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingOrderedCard;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\u000c\r\u000e\u000fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002RF\u0010\u0003\u001a6\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\t0\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000b\u0082\u0001\u0004\u0010\u0011\u0012\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
        "()V",
        "subWorkflow",
        "Lcom/squareup/workflow/Workflow;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "getSubWorkflow",
        "()Lcom/squareup/workflow/Workflow;",
        "AddPhoneNumber",
        "CancelingActivatedCard",
        "CancelingOrderedCard",
        "CardActivated",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$AddPhoneNumber;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingActivatedCard;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingOrderedCard;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ManageSquareCardState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getSubWorkflow()Lcom/squareup/workflow/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Workflow<",
            "**",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation
.end method
