.class final Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "CardOrderingAuthWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;->render(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;",
        "+",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;",
        "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthOutput;",
        "it",
        "Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screenState:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;

.field final synthetic $state:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$3;->this$0:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$3;->$screenState:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$3;->$state:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState;",
            "Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$3;->$screenState:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;

    check-cast p1, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$NotifyingMissingField;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$NotifyingMissingField;->getMissingField()Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    move-result-object p1

    sget-object v0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 116
    sget-object p1, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingSsnInfo;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingSsnInfo;

    check-cast p1, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 115
    :cond_1
    sget-object p1, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingPersonalInfo;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState$ShowingPersonalInfo;

    check-cast p1, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;

    .line 119
    :goto_0
    iget-object v1, p0, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$3;->this$0:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow;

    new-instance v2, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$3$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$3$1;-><init>(Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$3;Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthState$CardOrderingAuthScreenState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    invoke-static {v1, p1, v2, v0, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthWorkflow$render$3;->invoke(Lcom/squareup/balance/squarecard/authv2/missingfield/CardOrderingAuthMissingFieldOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
