.class public final Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "CardOrderingAuthSsnInfoWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
        "Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardOrderingAuthSsnInfoWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardOrderingAuthSsnInfoWorkflow.kt\ncom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,51:1\n149#2,5:52\n*E\n*S KotlinDebug\n*F\n+ 1 CardOrderingAuthSsnInfoWorkflow.kt\ncom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow\n*L\n48#1,5:52\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000*\u0001\u000c\u0018\u00002*\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u00070\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ:\u0010\u000e\u001a\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u00072\n\u0010\u000f\u001a\u00060\u0002j\u0002`\u00032\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00040\u0011H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\r\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
        "Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoProps;",
        "Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "analytics",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;",
        "(Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)V",
        "logAnalytics",
        "com/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow$logAnalytics$1",
        "Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow$logAnalytics$1;",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

.field private final logAnalytics:Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow$logAnalytics$1;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow;->analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    .line 30
    new-instance p1, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow$logAnalytics$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow$logAnalytics$1;-><init>(Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow;->logAnalytics:Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow$logAnalytics$1;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow;->analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    return-object p0
.end method


# virtual methods
.method public render(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow;->logAnalytics:Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow$logAnalytics$1;

    check-cast v0, Lcom/squareup/workflow/Worker;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {p2, v0, v1, v2, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 42
    sget-object v0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow$render$outputSink$1;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow$render$outputSink$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p2, v0}, Lcom/squareup/workflow/RenderContextKt;->makeEventSink(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Sink;

    move-result-object p2

    .line 44
    new-instance v0, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoRendering;

    .line 45
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerSsn()Ljava/lang/String;

    move-result-object p1

    .line 46
    new-instance v1, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow$render$1;

    invoke-direct {v1, p2}, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 47
    new-instance v2, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow$render$2;

    invoke-direct {v2, p2}, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 44
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoRendering;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 53
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 54
    const-class p2, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoRendering;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 55
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 53
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/authv2/ssninfo/CardOrderingAuthSsnInfoWorkflow;->render(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
