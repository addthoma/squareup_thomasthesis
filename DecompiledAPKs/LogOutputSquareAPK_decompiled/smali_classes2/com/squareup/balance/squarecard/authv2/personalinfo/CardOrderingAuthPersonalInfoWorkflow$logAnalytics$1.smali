.class public final Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$logAnalytics$1;
.super Lcom/squareup/workflow/LifecycleWorker;
.source "CardOrderingAuthPersonalInfoWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"
    }
    d2 = {
        "com/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$logAnalytics$1",
        "Lcom/squareup/workflow/LifecycleWorker;",
        "onStarted",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 36
    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$logAnalytics$1;->this$0:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;

    invoke-direct {p0}, Lcom/squareup/workflow/LifecycleWorker;-><init>()V

    return-void
.end method


# virtual methods
.method public onStarted()V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow$logAnalytics$1;->this$0:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;->access$getAnalytics$p(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthPersonalInfoWorkflow;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;->logPersonalInfoScreen()V

    return-void
.end method
