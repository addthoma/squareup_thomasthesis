.class public final Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;
.super Ljava/lang/Object;
.source "RealAuthSquareCardAnalytics.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u000b\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0006H\u0016J\u0010\u0010\u0008\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\nH\u0002J\u0008\u0010\u000b\u001a\u00020\u0006H\u0016J\u0008\u0010\u000c\u001a\u00020\u0006H\u0016J\u0008\u0010\r\u001a\u00020\u0006H\u0016J\u0008\u0010\u000e\u001a\u00020\u0006H\u0016J\u0008\u0010\u000f\u001a\u00020\u0006H\u0016J\u0010\u0010\u0010\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\nH\u0002J\u0008\u0010\u0011\u001a\u00020\u0006H\u0016J\u0008\u0010\u0012\u001a\u00020\u0006H\u0016J\u0008\u0010\u0013\u001a\u00020\u0006H\u0016J\u0008\u0010\u0014\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;",
        "squareCardAnalyticsLogger",
        "Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;",
        "(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V",
        "logCancelIdvCloseClicked",
        "",
        "logCancelIdvTryAgainClicked",
        "logClick",
        "description",
        "",
        "logIdvFailedCloseClicked",
        "logIdvFailedScreen",
        "logIdvFailedTryAgainClicked",
        "logIdvMissingFieldDialogScreen",
        "logIdvRejectedScreen",
        "logImpression",
        "logPersonalInfoContinueClicked",
        "logPersonalInfoScreen",
        "logSsnInfoContinueClicked",
        "logSsnInfoScreen",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "squareCardAnalyticsLogger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    return-void
.end method

.method private final logClick(Ljava/lang/String;)V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method private final logImpression(Ljava/lang/String;)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;->logImpression(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public logCancelIdvCloseClicked()V
    .locals 1

    const-string v0, "IDV: Cancel Verification Cancel Ordering"

    .line 56
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logCancelIdvTryAgainClicked()V
    .locals 1

    const-string v0, "IDV: Cancel Verification Try Again"

    .line 60
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logIdvFailedCloseClicked()V
    .locals 1

    const-string v0, "IDV: Unable To Verify Close"

    .line 52
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logIdvFailedScreen()V
    .locals 1

    const-string v0, "IDV: Unable To Verify Personal Information"

    .line 44
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logIdvFailedTryAgainClicked()V
    .locals 1

    const-string v0, "IDV: Unable To Verify Try Again"

    .line 48
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logIdvMissingFieldDialogScreen()V
    .locals 1

    const-string v0, "IDV: Information Invalid"

    .line 40
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logIdvRejectedScreen()V
    .locals 1

    const-string v0, "IDV: Not Eligible For Square Card"

    .line 64
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logPersonalInfoContinueClicked()V
    .locals 1

    const-string v0, "IDV: Personal Information Continue"

    .line 28
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logPersonalInfoScreen()V
    .locals 1

    const-string v0, "IDV: Personal Information Page"

    .line 24
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logSsnInfoContinueClicked()V
    .locals 1

    const-string v0, "IDV: SSN Continue"

    .line 36
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logSsnInfoScreen()V
    .locals 1

    const-string v0, "IDV: SSN Page"

    .line 32
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method
