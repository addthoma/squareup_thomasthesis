.class public final Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;
.super Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;
.source "AuthSquareCardPersonalInfoScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SubmitPersonalInfo"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;",
        "ownerName",
        "",
        "address",
        "Lcom/squareup/address/Address;",
        "birthDate",
        "Lorg/threeten/bp/LocalDate;",
        "(Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;)V",
        "getAddress",
        "()Lcom/squareup/address/Address;",
        "getBirthDate",
        "()Lorg/threeten/bp/LocalDate;",
        "getOwnerName",
        "()Ljava/lang/String;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final address:Lcom/squareup/address/Address;

.field private final birthDate:Lorg/threeten/bp/LocalDate;

.field private final ownerName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;)V
    .locals 1

    const-string v0, "ownerName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "address"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;->ownerName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;->address:Lcom/squareup/address/Address;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;->birthDate:Lorg/threeten/bp/LocalDate;

    return-void
.end method


# virtual methods
.method public final getAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;->address:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public final getBirthDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;->birthDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getOwnerName()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;->ownerName:Ljava/lang/String;

    return-object v0
.end method
