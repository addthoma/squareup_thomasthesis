.class public final Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$update$$inlined$onClickDebounced$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 AuthSquareCardPersonalInfoCoordinator.kt\ncom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator\n*L\n1#1,1322:1\n52#2,8:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen$inlined:Lcom/squareup/workflow/legacy/Screen;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$update$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$update$$inlined$onClickDebounced$1;->$screen$inlined:Lcom/squareup/workflow/legacy/Screen;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1323
    iget-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$update$$inlined$onClickDebounced$1;->$screen$inlined:Lcom/squareup/workflow/legacy/Screen;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 1324
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;

    .line 1325
    iget-object v1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$update$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->access$getOwnerName$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;)Landroid/widget/EditText;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    .line 1326
    iget-object v2, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$update$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;

    invoke-static {v2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->access$getOwnerAddress$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;)Lcom/squareup/address/AddressLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/address/AddressLayout;->getAddress()Lcom/squareup/address/Address;

    move-result-object v2

    const-string v3, "ownerAddress.address"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1327
    iget-object v3, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$update$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;

    invoke-static {v3}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->access$getOwnerBirthDate$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;)Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->getDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    .line 1324
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;-><init>(Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;)V

    .line 1323
    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
