.class public final Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;
.super Ljava/lang/Object;
.source "RealSquareCardUpseller_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/SquareCardUpsellExperiment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/SquareCardUpsellExperiment;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/SquareCardUpsellExperiment;",
            ">;)",
            "Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;"
        }
    .end annotation

    .line 55
    new-instance v7, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Ldagger/Lazy;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/experiments/SquareCardUpsellExperiment;)Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/balance/BalanceAppletGateway;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/squareup/experiments/SquareCardUpsellExperiment;",
            ")",
            "Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;"
        }
    .end annotation

    .line 61
    new-instance v7, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Ldagger/Lazy;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/experiments/SquareCardUpsellExperiment;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/balance/squarecard/ManageSquareCardSection;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/experiments/SquareCardUpsellExperiment;

    invoke-static/range {v1 .. v6}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Ldagger/Lazy;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/experiments/SquareCardUpsellExperiment;)Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller_Factory;->get()Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;

    move-result-object v0

    return-object v0
.end method
