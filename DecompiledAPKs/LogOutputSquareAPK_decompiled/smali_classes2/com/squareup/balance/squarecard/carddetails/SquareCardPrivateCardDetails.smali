.class public final Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;
.super Ljava/lang/Object;
.source "SquareCardPrivateCardDetails.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0006H\u00c6\u0003J1\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0006H\u00c6\u0001J\t\u0010\u0014\u001a\u00020\u0006H\u00d6\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u0006H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0003H\u00d6\u0001J\u0019\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0006H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\r\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;",
        "Landroid/os/Parcelable;",
        "fullPan",
        "",
        "cvc",
        "month",
        "",
        "year",
        "(Ljava/lang/String;Ljava/lang/String;II)V",
        "getCvc",
        "()Ljava/lang/String;",
        "getFullPan",
        "getMonth",
        "()I",
        "getYear",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final cvc:Ljava/lang/String;

.field private final fullPan:Ljava/lang/String;

.field private final month:I

.field private final year:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails$Creator;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails$Creator;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1

    const-string v0, "fullPan"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cvc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->fullPan:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->cvc:Ljava/lang/String;

    iput p3, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->month:I

    iput p4, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->year:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/Object;)Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->fullPan:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->cvc:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->month:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget p4, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->year:I

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->copy(Ljava/lang/String;Ljava/lang/String;II)Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->fullPan:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->cvc:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->month:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->year:I

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;II)Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;
    .locals 1

    const-string v0, "fullPan"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cvc"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->fullPan:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->fullPan:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->cvc:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->cvc:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->month:I

    iget v1, p1, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->month:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->year:I

    iget p1, p1, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->year:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCvc()Ljava/lang/String;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->cvc:Ljava/lang/String;

    return-object v0
.end method

.method public final getFullPan()Ljava/lang/String;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->fullPan:Ljava/lang/String;

    return-object v0
.end method

.method public final getMonth()I
    .locals 1

    .line 10
    iget v0, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->month:I

    return v0
.end method

.method public final getYear()I
    .locals 1

    .line 11
    iget v0, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->year:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->fullPan:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->cvc:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->month:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->year:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SquareCardPrivateCardDetails(fullPan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->fullPan:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cvc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->cvc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", month="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->month:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", year="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->year:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->fullPan:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->cvc:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget p2, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->month:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget p2, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;->year:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
