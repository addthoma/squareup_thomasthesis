.class public final Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "CancelingBizbankWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankOutput;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCancelingBizbankWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CancelingBizbankWorkflow.kt\ncom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,78:1\n85#2:79\n240#3:80\n276#4:81\n*E\n*S KotlinDebug\n*F\n+ 1 CancelingBizbankWorkflow.kt\ncom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow\n*L\n75#1:79\n75#1:80\n75#1:81\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\u0018\u00002\u0018\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00040\nH\u0002J-\u0010\u000b\u001a\u00020\u00052\n\u0010\u000c\u001a\u00060\u0002j\u0002`\u00032\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00040\u000eH\u0016\u00a2\u0006\u0002\u0010\u0010R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "",
        "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankProps;",
        "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankOutput;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V",
        "cancelBizbank",
        "Lcom/squareup/workflow/Worker;",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizbankService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    return-void
.end method

.method private final cancelBizbank()Lcom/squareup/workflow/Worker;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankOutput;",
            ">;"
        }
    .end annotation

    .line 63
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;-><init>()V

    const/4 v1, 0x0

    .line 64
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;->bizbank_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v2, "setBalanceStatusRequest"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->setBalanceStatus(Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 69
    sget-object v1, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow$cancelBizbank$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow$cancelBizbank$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "bizbankService.setBalanc\u2026ure\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    sget-object v1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow$cancelBizbank$$inlined$asWorker$1;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow$cancelBizbank$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 80
    invoke-static {v1}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 81
    const-class v1, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankOutput;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v2, Lcom/squareup/workflow/Worker;

    return-object v2
.end method


# virtual methods
.method public render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;
    .locals 6

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    sget-object p1, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow$render$sink$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow$render$sink$1;

    check-cast p1, Lkotlin/jvm/functions/Function2;

    invoke-static {p2, p1}, Lcom/squareup/workflow/RenderContextKt;->makeEventSink(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Sink;

    move-result-object p1

    .line 44
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;->cancelBizbank()Lcom/squareup/workflow/Worker;

    move-result-object v1

    new-instance v0, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow$render$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow$render$1;-><init>(Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p2

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 48
    new-instance p2, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 49
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;

    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->canceling_bizbank_spinner_message:I

    invoke-direct {v0, v1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;-><init>(I)V

    check-cast v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 50
    new-instance v1, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow$render$2;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 48
    invoke-direct {p2, v0, v1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    return-object p2
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancelbizbank/canceling/CancelingBizbankWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    move-result-object p1

    return-object p1
.end method
