.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory_Factory;
.super Ljava/lang/Object;
.source "OrderSquareCardViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;",
            ">;)",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;)Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;
    .locals 3

    .line 29
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;

    invoke-static {v0, v1, v2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory_Factory;->newInstance(Lcom/squareup/balance/squarecard/order/OrderSquareCardBusinessNameCoordinator$Factory;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$Factory;)Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory_Factory;->get()Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;

    move-result-object v0

    return-object v0
.end method
