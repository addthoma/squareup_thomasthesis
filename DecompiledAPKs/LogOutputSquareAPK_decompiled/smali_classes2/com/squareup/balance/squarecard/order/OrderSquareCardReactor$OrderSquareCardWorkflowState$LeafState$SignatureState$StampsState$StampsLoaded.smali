.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;
.super Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState;
.source "OrderSquareCardReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StampsLoaded"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0019\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001BE\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\t\u0010 \u001a\u00020\u0003H\u00c6\u0003J\t\u0010!\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0007H\u00c6\u0003J\t\u0010#\u001a\u00020\tH\u00c6\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003J\u000f\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003J\t\u0010&\u001a\u00020\u0010H\u00c6\u0003JW\u0010\'\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\u000e\u0008\u0002\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u0010H\u00c6\u0001J\u0013\u0010(\u001a\u00020\u00032\u0008\u0010)\u001a\u0004\u0018\u00010*H\u00d6\u0003J\t\u0010+\u001a\u00020,H\u00d6\u0001J\t\u0010-\u001a\u00020\tH\u00d6\u0001R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0014\u0010\u0008\u001a\u00020\tX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0016\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u001a\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001f\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState;",
        "skippedInitialScreens",
        "",
        "allowStampsCustomization",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;",
        "squareCardCustomizationSettings",
        "Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
        "signatureAsJson",
        "",
        "stamp",
        "Lcom/squareup/protos/client/bizbank/Stamp;",
        "stampsToRestore",
        "",
        "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
        "status",
        "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;",
        "(ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;)V",
        "getAllowStampsCustomization",
        "()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;",
        "getSignatureAsJson",
        "()Ljava/lang/String;",
        "getSkippedInitialScreens",
        "()Z",
        "getSquareCardCustomizationSettings",
        "()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
        "getStamp",
        "()Lcom/squareup/protos/client/bizbank/Stamp;",
        "getStampsToRestore",
        "()Ljava/util/List;",
        "getStatus",
        "()Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allowStampsCustomization:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

.field private final signatureAsJson:Ljava/lang/String;

.field private final skippedInitialScreens:Z

.field private final squareCardCustomizationSettings:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

.field private final stamp:Lcom/squareup/protos/client/bizbank/Stamp;

.field private final stampsToRestore:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation
.end field

.field private final status:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;


# direct methods
.method public constructor <init>(ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;",
            "Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bizbank/Stamp;",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;",
            "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;",
            ")V"
        }
    .end annotation

    const-string v0, "allowStampsCustomization"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squareCardCustomizationSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureAsJson"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampsToRestore"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "status"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 194
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->skippedInitialScreens:Z

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->allowStampsCustomization:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->squareCardCustomizationSettings:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->signatureAsJson:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->stamp:Lcom/squareup/protos/client/bizbank/Stamp;

    iput-object p6, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->stampsToRestore:Ljava/util/List;

    iput-object p7, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->status:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSkippedInitialScreens()Z

    move-result p1

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object p2

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object p3

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSignatureAsJson()Ljava/lang/String;

    move-result-object p4

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStamp()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object p5

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStampsToRestore()Ljava/util/List;

    move-result-object p6

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->status:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move p3, p1

    move-object p4, p9

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->copy(ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSkippedInitialScreens()Z

    move-result v0

    return v0
.end method

.method public final component2()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v0

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/client/bizbank/Stamp;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStamp()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v0

    return-object v0
.end method

.method public final component6()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStampsToRestore()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final component7()Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->status:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    return-object v0
.end method

.method public final copy(ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;",
            "Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bizbank/Stamp;",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;",
            "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;",
            ")",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;"
        }
    .end annotation

    const-string v0, "allowStampsCustomization"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squareCardCustomizationSettings"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureAsJson"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampsToRestore"

    move-object v7, p6

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "status"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    move-object v1, v0

    move v2, p1

    move-object v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;-><init>(ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSkippedInitialScreens()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSkippedInitialScreens()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStamp()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStamp()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStampsToRestore()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStampsToRestore()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->status:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    iget-object p1, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->status:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->allowStampsCustomization:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    return-object v0
.end method

.method public getSignatureAsJson()Ljava/lang/String;
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->signatureAsJson:Ljava/lang/String;

    return-object v0
.end method

.method public getSkippedInitialScreens()Z
    .locals 1

    .line 187
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->skippedInitialScreens:Z

    return v0
.end method

.method public getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->squareCardCustomizationSettings:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    return-object v0
.end method

.method public getStamp()Lcom/squareup/protos/client/bizbank/Stamp;
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->stamp:Lcom/squareup/protos/client/bizbank/Stamp;

    return-object v0
.end method

.method public getStampsToRestore()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation

    .line 192
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->stampsToRestore:Ljava/util/List;

    return-object v0
.end method

.method public final getStatus()Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->status:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSkippedInitialScreens()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStamp()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStampsToRestore()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_5
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->status:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StampsLoaded(skippedInitialScreens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSkippedInitialScreens()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", allowStampsCustomization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", squareCardCustomizationSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", signatureAsJson="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", stamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStamp()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stampsToRestore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStampsToRestore()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->status:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
