.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics$DefaultImpls;
.super Ljava/lang/Object;
.source "OrderSquareCardAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static logShippingDetailsContinueClick(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsContinueClick(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingDetailsCorrectedAddressAlertScreen(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsCorrectedAddressAlertScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingDetailsCorrectedAddressOriginalSubmitted(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsCorrectedAddressOriginalSubmitted(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingDetailsCorrectedAddressRecommendedSubmitted(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsCorrectedAddressRecommendedSubmitted(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingDetailsCorrectedAddressScreen(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsCorrectedAddressScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingDetailsMissingInfoErrorScreen(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsMissingInfoErrorScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingDetailsScreen(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingDetailsServerErrorScreen(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsServerErrorScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingDetailsUnverifiedAddressErrorScreen(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsUnverifiedAddressErrorScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingDetailsUnverifiedAddressProceedClick(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsUnverifiedAddressProceedClick(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingDetailsUnverifiedAddressReenterClick(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsUnverifiedAddressReenterClick(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingDetailsUnverifiedAddressScreen(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsUnverifiedAddressScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingOrderConfirmedScreen(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingOrderConfirmedScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingOrderConfirmedWithSignatureOnly(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingOrderConfirmedWithSignatureOnly(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingOrderConfirmedWithStampsAndSignature(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingOrderConfirmedWithStampsAndSignature(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingOrderConfirmedWithStampsOnly(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingOrderConfirmedWithStampsOnly(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public static logShippingOrderErrorScreen(Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;)V
    .locals 0

    check-cast p0, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingOrderErrorScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method
