.class public final Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;
.super Ljava/lang/Object;
.source "SquareCardCustomizationSettings.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings$Creator;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardCustomizationSettings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardCustomizationSettings.kt\ncom/squareup/balance/squarecard/order/SquareCardIdvSettings\n*L\n1#1,107:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0087\u0008\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003JA\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u0005H\u00c6\u0001J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u00d6\u0003J\t\u0010!\u001a\u00020\u001cH\u00d6\u0001J\u0006\u0010\"\u001a\u00020\u001eJ\u0006\u0010#\u001a\u00020\u001eJ\u0006\u0010$\u001a\u00020\u001eJ\t\u0010%\u001a\u00020\u0005H\u00d6\u0001J\u0019\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020\u001cH\u00d6\u0001J\u000c\u0010+\u001a\u00020\u001e*\u00020\u0007H\u0002R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0013\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
        "Landroid/os/Parcelable;",
        "idv",
        "Lcom/squareup/balance/squarecard/order/ApprovalState;",
        "ownerName",
        "",
        "ownerAddress",
        "Lcom/squareup/address/Address;",
        "ownerBirthDate",
        "Lorg/threeten/bp/LocalDate;",
        "ownerSsn",
        "(Lcom/squareup/balance/squarecard/order/ApprovalState;Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Ljava/lang/String;)V",
        "getIdv",
        "()Lcom/squareup/balance/squarecard/order/ApprovalState;",
        "getOwnerAddress",
        "()Lcom/squareup/address/Address;",
        "getOwnerBirthDate",
        "()Lorg/threeten/bp/LocalDate;",
        "getOwnerName",
        "()Ljava/lang/String;",
        "getOwnerSsn",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "isAddressValid",
        "isBirthDateValid",
        "isSsnValid",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "isValid",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final idv:Lcom/squareup/balance/squarecard/order/ApprovalState;

.field private final ownerAddress:Lcom/squareup/address/Address;

.field private final ownerBirthDate:Lorg/threeten/bp/LocalDate;

.field private final ownerName:Ljava/lang/String;

.field private final ownerSsn:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings$Creator;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings$Creator;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/balance/squarecard/order/ApprovalState;Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Ljava/lang/String;)V
    .locals 1

    const-string v0, "idv"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ownerName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->idv:Lcom/squareup/balance/squarecard/order/ApprovalState;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerAddress:Lcom/squareup/address/Address;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerBirthDate:Lorg/threeten/bp/LocalDate;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerSsn:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/balance/squarecard/order/ApprovalState;Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->idv:Lcom/squareup/balance/squarecard/order/ApprovalState;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerName:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerAddress:Lcom/squareup/address/Address;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerBirthDate:Lorg/threeten/bp/LocalDate;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerSsn:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->copy(Lcom/squareup/balance/squarecard/order/ApprovalState;Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Ljava/lang/String;)Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object p0

    return-object p0
.end method

.method private final isValid(Lcom/squareup/address/Address;)Z
    .locals 2

    .line 49
    iget-object v0, p1, Lcom/squareup/address/Address;->street:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/address/Address;->city:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/address/Address;->state:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 50
    iget-object p1, p1, Lcom/squareup/address/Address;->postalCode:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/text/PostalCodes;->isUsZipCode(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method


# virtual methods
.method public final component1()Lcom/squareup/balance/squarecard/order/ApprovalState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->idv:Lcom/squareup/balance/squarecard/order/ApprovalState;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerName:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/address/Address;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerAddress:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public final component4()Lorg/threeten/bp/LocalDate;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerBirthDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerSsn:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/squareup/balance/squarecard/order/ApprovalState;Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Ljava/lang/String;)Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;
    .locals 7

    const-string v0, "idv"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ownerName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;-><init>(Lcom/squareup/balance/squarecard/order/ApprovalState;Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Ljava/lang/String;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->idv:Lcom/squareup/balance/squarecard/order/ApprovalState;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->idv:Lcom/squareup/balance/squarecard/order/ApprovalState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerAddress:Lcom/squareup/address/Address;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerAddress:Lcom/squareup/address/Address;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerBirthDate:Lorg/threeten/bp/LocalDate;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerBirthDate:Lorg/threeten/bp/LocalDate;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerSsn:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerSsn:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getIdv()Lcom/squareup/balance/squarecard/order/ApprovalState;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->idv:Lcom/squareup/balance/squarecard/order/ApprovalState;

    return-object v0
.end method

.method public final getOwnerAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerAddress:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public final getOwnerBirthDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerBirthDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getOwnerName()Ljava/lang/String;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerName:Ljava/lang/String;

    return-object v0
.end method

.method public final getOwnerSsn()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerSsn:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->idv:Lcom/squareup/balance/squarecard/order/ApprovalState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerAddress:Lcom/squareup/address/Address;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerBirthDate:Lorg/threeten/bp/LocalDate;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerSsn:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public final isAddressValid()Z
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerAddress:Lcom/squareup/address/Address;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->isValid(Lcom/squareup/address/Address;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final isBirthDateValid()Z
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerBirthDate:Lorg/threeten/bp/LocalDate;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isSsnValid()Z
    .locals 4

    .line 44
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerSsn:Ljava/lang/String;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/CharSequence;

    new-instance v2, Lkotlin/text/Regex;

    const-string v3, "[0-9]{9}"

    invoke-direct {v2, v3}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lkotlin/text/Regex;->matches(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SquareCardIdvSettings(idv="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->idv:Lcom/squareup/balance/squarecard/order/ApprovalState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", ownerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", ownerAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerAddress:Lcom/squareup/address/Address;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", ownerBirthDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerBirthDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", ownerSsn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerSsn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->idv:Lcom/squareup/balance/squarecard/order/ApprovalState;

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerAddress:Lcom/squareup/address/Address;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object p2, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerBirthDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->ownerSsn:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
