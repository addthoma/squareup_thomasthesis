.class public final Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;
.super Ljava/lang/Object;
.source "RealOrderSquareCardSerializer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderSquareCardSerializer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderSquareCardSerializer.kt\ncom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,306:1\n180#2:307\n148#2:308\n148#2:309\n148#2:310\n148#2:311\n148#2:313\n148#2:315\n61#3:312\n61#3:314\n61#3:316\n*E\n*S KotlinDebug\n*F\n+ 1 RealOrderSquareCardSerializer.kt\ncom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer\n*L\n173#1:307\n173#1:308\n173#1:309\n173#1:310\n173#1:311\n173#1:313\n173#1:315\n173#1:312\n173#1:314\n173#1:316\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nJ\u0016\u0010\u000b\u001a\u00020\u00082\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fJ8\u0010\u0010\u001a\u00020\u00082\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u00122\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;",
        "",
        "authWorkflowStarter",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;",
        "orderWorkflowStarter",
        "Lcom/squareup/mailorder/OrderScreenWorkflowStarter;",
        "(Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;Lcom/squareup/mailorder/OrderScreenWorkflowStarter;)V",
        "deserializeState",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "startAuthSquareCard",
        "skippedInitialScreens",
        "",
        "squareCardCustomizationInfo",
        "Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
        "startOrderingSquareCard",
        "signatureJson",
        "",
        "customizationToken",
        "stampsToRestore",
        "",
        "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion;


# instance fields
.field private final authWorkflowStarter:Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;

.field private final orderWorkflowStarter:Lcom/squareup/mailorder/OrderScreenWorkflowStarter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;->Companion:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;Lcom/squareup/mailorder/OrderScreenWorkflowStarter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "authWorkflowStarter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderWorkflowStarter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;->authWorkflowStarter:Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;->orderWorkflowStarter:Lcom/squareup/mailorder/OrderScreenWorkflowStarter;

    return-void
.end method

.method public static synthetic startOrderingSquareCard$default(Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;
    .locals 7

    and-int/lit8 p7, p6, 0x4

    const-string v0, ""

    if-eqz p7, :cond_0

    move-object v4, v0

    goto :goto_0

    :cond_0
    move-object v4, p3

    :goto_0
    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_1

    move-object v5, v0

    goto :goto_1

    :cond_1
    move-object v5, p4

    :goto_1
    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v6, p5

    .line 149
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;->startOrderingSquareCard(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;
    .locals 13

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    .line 307
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 174
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Class.forName(className)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/JvmClassMappingKt;->getKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    .line 177
    const-class v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingSplash;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingSplash;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingSplash;

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    goto/16 :goto_0

    .line 178
    :cond_0
    const-class v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;

    .line 179
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v1

    .line 180
    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->readSquareCardCustomizationSettings(Lokio/BufferedSource;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v2

    .line 181
    iget-object v3, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;->authWorkflowStarter:Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;

    sget-object v4, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-interface {p1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    invoke-interface {v3, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;->start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    .line 178
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;-><init>(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Lcom/squareup/workflow/rx1/Workflow;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    goto/16 :goto_0

    .line 183
    :cond_1
    const-class v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "T::class.java.enumConstants[readInt()]"

    if-eqz v2, :cond_2

    .line 184
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    .line 308
    const-class v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError$ErrorType;

    invoke-virtual {v1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Enum;

    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result p1

    aget-object p1, v1, p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError$ErrorType;

    .line 183
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;

    invoke-direct {v1, v0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;-><init>(ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError$ErrorType;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    goto/16 :goto_0

    .line 187
    :cond_2
    const-class v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;

    .line 188
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v1

    .line 189
    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->readSquareCardCustomizationSettings(Lokio/BufferedSource;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object p1

    .line 187
    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;-><init>(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    goto/16 :goto_0

    .line 191
    :cond_3
    const-class v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 192
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v5

    .line 309
    const-class v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, v0

    check-cast v6, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    .line 194
    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->readSquareCardCustomizationSettings(Lokio/BufferedSource;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v7

    .line 195
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    .line 198
    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->access$readStampsToRestore(Lokio/BufferedSource;)Ljava/util/List;

    move-result-object v10

    .line 191
    new-instance p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    move-object v4, p1

    invoke-direct/range {v4 .. v10}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;-><init>(ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;)V

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    goto/16 :goto_0

    .line 200
    :cond_4
    const-class v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const/4 v4, 0x0

    if-eqz v2, :cond_6

    .line 310
    const-class v0, Lcom/squareup/cardcustomizations/signature/InkLevel;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, v0

    check-cast v6, Lcom/squareup/cardcustomizations/signature/InkLevel;

    .line 202
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v7

    .line 311
    const-class v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v8, v0

    check-cast v8, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    .line 204
    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->readSquareCardCustomizationSettings(Lokio/BufferedSource;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v9

    .line 205
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v10

    .line 312
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/bizbank/Stamp;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v4

    :cond_5
    move-object v11, v4

    check-cast v11, Lcom/squareup/protos/client/bizbank/Stamp;

    .line 207
    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->access$readStampsToRestore(Lokio/BufferedSource;)Ljava/util/List;

    move-result-object v12

    .line 200
    new-instance p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    move-object v5, p1

    invoke-direct/range {v5 .. v12}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;-><init>(Lcom/squareup/cardcustomizations/signature/InkLevel;ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;)V

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    goto/16 :goto_0

    .line 209
    :cond_6
    const-class v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    .line 210
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v6

    .line 211
    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->readSquareCardCustomizationSettings(Lokio/BufferedSource;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v7

    .line 212
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v8

    .line 213
    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->access$readStampsToRestore(Lokio/BufferedSource;)Ljava/util/List;

    move-result-object v9

    .line 214
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;->orderWorkflowStarter:Lcom/squareup/mailorder/OrderScreenWorkflowStarter;

    sget-object v2, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-interface {p1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/mailorder/OrderScreenWorkflowStarter;->start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object v10

    move-object v5, v0

    .line 209
    invoke-direct/range {v5 .. v10}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;-><init>(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Ljava/util/List;Lcom/squareup/workflow/rx1/Workflow;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    goto/16 :goto_0

    .line 216
    :cond_7
    const-class v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 217
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v6

    .line 313
    const-class v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v7, v0

    check-cast v7, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    .line 219
    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->readSquareCardCustomizationSettings(Lokio/BufferedSource;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v8

    .line 220
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v9

    .line 314
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/bizbank/Stamp;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v4

    :cond_8
    move-object v10, v4

    check-cast v10, Lcom/squareup/protos/client/bizbank/Stamp;

    .line 222
    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->access$readStampsToRestore(Lokio/BufferedSource;)Ljava/util/List;

    move-result-object v11

    .line 216
    new-instance p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    move-object v5, p1

    invoke-direct/range {v5 .. v11}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;-><init>(ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;)V

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    goto :goto_0

    .line 224
    :cond_9
    const-class v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 225
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v6

    .line 315
    const-class v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v7, v0

    check-cast v7, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    .line 227
    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->readSquareCardCustomizationSettings(Lokio/BufferedSource;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v8

    .line 228
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v9

    .line 316
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/bizbank/Stamp;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v4

    :cond_a
    move-object v10, v4

    check-cast v10, Lcom/squareup/protos/client/bizbank/Stamp;

    .line 230
    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->access$readStampsToRestore(Lokio/BufferedSource;)Ljava/util/List;

    move-result-object v11

    .line 231
    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->access$readStampsStatus(Lokio/BufferedSource;)Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    move-result-object v12

    .line 224
    new-instance p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    move-object v5, p1

    invoke-direct/range {v5 .. v12}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;-><init>(ZLcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Lcom/squareup/protos/client/bizbank/Stamp;Ljava/util/List;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;)V

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    :goto_0
    return-object p1

    .line 233
    :cond_b
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final startAuthSquareCard(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;
    .locals 2

    const-string v0, "squareCardCustomizationInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;->authWorkflowStarter:Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;->start(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object v0

    .line 142
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;

    invoke-direct {v1, p1, p2, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;-><init>(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Lcom/squareup/workflow/rx1/Workflow;)V

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    return-object v1
.end method

.method public final startOrderingSquareCard(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;)",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;"
        }
    .end annotation

    move-object/from16 v3, p3

    move-object/from16 v4, p5

    const-string v0, "squareCardCustomizationInfo"

    move-object v2, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureJson"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customizationToken"

    move-object/from16 v1, p4

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampsToRestore"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    move-object v0, v4

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v5, 0x1

    xor-int/2addr v0, v5

    .line 153
    move-object v6, v3

    check-cast v6, Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-lez v6, :cond_0

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    if-eqz v0, :cond_1

    if-eqz v5, :cond_1

    .line 155
    sget-object v0, Lcom/squareup/mailorder/CardCustomizationOption;->STAMPS_AND_SIGNATURE:Lcom/squareup/mailorder/CardCustomizationOption;

    goto :goto_1

    :cond_1
    if-eqz v0, :cond_2

    .line 157
    sget-object v0, Lcom/squareup/mailorder/CardCustomizationOption;->STAMPS:Lcom/squareup/mailorder/CardCustomizationOption;

    goto :goto_1

    .line 159
    :cond_2
    sget-object v0, Lcom/squareup/mailorder/CardCustomizationOption;->SIGNATURE:Lcom/squareup/mailorder/CardCustomizationOption;

    :goto_1
    move-object v11, p0

    move-object v8, v0

    .line 162
    iget-object v0, v11, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;->orderWorkflowStarter:Lcom/squareup/mailorder/OrderScreenWorkflowStarter;

    .line 163
    new-instance v12, Lcom/squareup/mailorder/OrderWorkflowStartArgument;

    const/4 v6, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object v5, v12

    move-object/from16 v7, p4

    invoke-direct/range {v5 .. v10}, Lcom/squareup/mailorder/OrderWorkflowStartArgument;-><init>(Lcom/squareup/mailorder/ContactInfo;Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 162
    invoke-interface {v0, v12}, Lcom/squareup/mailorder/OrderScreenWorkflowStarter;->start(Lcom/squareup/mailorder/OrderWorkflowStartArgument;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object v5

    .line 168
    new-instance v6, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    move-object v0, v6

    move v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;-><init>(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Ljava/util/List;Lcom/squareup/workflow/rx1/Workflow;)V

    check-cast v6, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    return-object v6
.end method
