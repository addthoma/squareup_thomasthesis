.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;
.super Ljava/lang/Object;
.source "OrderSquareCardSignatureCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B=\u0008\u0001\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ$\u0010\u000e\u001a\u00020\u000f2\u001c\u0010\u0010\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0012j\u0002`\u00150\u0011R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;",
        "",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "mainThreadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "renderSignatureScheduler",
        "Lcom/squareup/balance/core/RenderSignatureScheduler;",
        "stampsScheduler",
        "gson",
        "Lcom/google/gson/Gson;",
        "businessNameFormatter",
        "Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;",
        "(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/balance/core/RenderSignatureScheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;)V",
        "create",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureScreen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final businessNameFormatter:Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;

.field private final gson:Lcom/google/gson/Gson;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final renderSignatureScheduler:Lcom/squareup/balance/core/RenderSignatureScheduler;

.field private final stampsScheduler:Lio/reactivex/Scheduler;


# direct methods
.method public constructor <init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/balance/core/RenderSignatureScheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;)V
    .locals 1
    .param p1    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p2    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "mainScheduler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThreadEnforcer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renderSignatureScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampsScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "businessNameFormatter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;->renderSignatureScheduler:Lcom/squareup/balance/core/RenderSignatureScheduler;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;->stampsScheduler:Lio/reactivex/Scheduler;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;->gson:Lcom/google/gson/Gson;

    iput-object p6, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;->businessNameFormatter:Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;>;)",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    .line 140
    iget-object v3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    iget-object v4, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    .line 141
    iget-object v5, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;->renderSignatureScheduler:Lcom/squareup/balance/core/RenderSignatureScheduler;

    iget-object v6, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;->stampsScheduler:Lio/reactivex/Scheduler;

    iget-object v7, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;->gson:Lcom/google/gson/Gson;

    iget-object v8, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;->businessNameFormatter:Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;

    const/4 v9, 0x0

    move-object v1, v0

    move-object v2, p1

    .line 139
    invoke-direct/range {v1 .. v9}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;-><init>(Lio/reactivex/Observable;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/balance/core/RenderSignatureScheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
