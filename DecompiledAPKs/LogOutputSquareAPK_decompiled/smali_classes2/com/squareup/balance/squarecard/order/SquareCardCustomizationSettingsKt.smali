.class public final Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;
.super Ljava/lang/Object;
.source "SquareCardCustomizationSettings.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardCustomizationSettings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardCustomizationSettings.kt\ncom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,107:1\n140#2:108\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardCustomizationSettings.kt\ncom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt\n*L\n80#1:108\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0002\u001a\u0012\u0010\u0005\u001a\u00020\u0006*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0001\u001a\u0012\u0010\u0008\u001a\u00020\u0006*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0004\u00a8\u0006\t"
    }
    d2 = {
        "readSquareCardCustomizationSettings",
        "Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
        "Lokio/BufferedSource;",
        "readSquareCardIdvSettings",
        "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
        "writeSquareCardCustomizationSettings",
        "Lokio/BufferedSink;",
        "settings",
        "writeSquareCardIdvSettings",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final readSquareCardCustomizationSettings(Lokio/BufferedSource;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;
    .locals 7

    const-string v0, "$this$readSquareCardCustomizationSettings"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    new-instance v0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    .line 100
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v2

    .line 101
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v3

    .line 102
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readFloat(Lokio/BufferedSource;)F

    move-result v4

    .line 103
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readFloat(Lokio/BufferedSource;)F

    move-result v5

    .line 104
    invoke-static {p0}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->readSquareCardIdvSettings(Lokio/BufferedSource;)Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v6

    move-object v1, v0

    .line 99
    invoke-direct/range {v1 .. v6}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;-><init>(Ljava/lang/String;Ljava/lang/String;FFLcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    return-object v0
.end method

.method public static final readSquareCardIdvSettings(Lokio/BufferedSource;)Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;
    .locals 7

    const-string v0, "$this$readSquareCardIdvSettings"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    sget-object v0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$readSquareCardIdvSettings$$inlined$readOptionalEnumByOrdinal$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$readSquareCardIdvSettings$$inlined$readOptionalEnumByOrdinal$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v2, v0

    check-cast v2, Lcom/squareup/balance/squarecard/order/ApprovalState;

    .line 81
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v3

    .line 82
    sget-object v0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$readSquareCardIdvSettings$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$readSquareCardIdvSettings$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/address/Address;

    .line 83
    sget-object v0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$readSquareCardIdvSettings$2;->INSTANCE:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$readSquareCardIdvSettings$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lorg/threeten/bp/LocalDate;

    .line 84
    sget-object v0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$readSquareCardIdvSettings$3;->INSTANCE:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$readSquareCardIdvSettings$3;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object p0

    move-object v6, p0

    check-cast v6, Ljava/lang/String;

    .line 79
    new-instance p0, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;-><init>(Lcom/squareup/balance/squarecard/order/ApprovalState;Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final writeSquareCardCustomizationSettings(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;)Lokio/BufferedSink;
    .locals 1

    const-string v0, "$this$writeSquareCardCustomizationSettings"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->getOwnerName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 92
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->getBusinessName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 93
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->getMinInkLevel()F

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeFloat(Lokio/BufferedSink;F)Lokio/BufferedSink;

    .line 94
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->getMaxInkLevel()F

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeFloat(Lokio/BufferedSink;F)Lokio/BufferedSink;

    .line 95
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->writeSquareCardIdvSettings(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lokio/BufferedSink;

    return-object p0
.end method

.method public static final writeSquareCardIdvSettings(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lokio/BufferedSink;
    .locals 2

    const-string v0, "$this$writeSquareCardIdvSettings"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getIdv()Lcom/squareup/balance/squarecard/order/ApprovalState;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeOptionalEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    .line 72
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 73
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerAddress()Lcom/squareup/address/Address;

    move-result-object v0

    sget-object v1, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$writeSquareCardIdvSettings$1$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$writeSquareCardIdvSettings$1$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {p0, v0, v1}, Lcom/squareup/workflow/SnapshotKt;->writeNullable(Lokio/BufferedSink;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Lokio/BufferedSink;

    .line 74
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerBirthDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    sget-object v1, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$writeSquareCardIdvSettings$1$2;->INSTANCE:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$writeSquareCardIdvSettings$1$2;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {p0, v0, v1}, Lcom/squareup/workflow/SnapshotKt;->writeNullable(Lokio/BufferedSink;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Lokio/BufferedSink;

    .line 75
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerSsn()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$writeSquareCardIdvSettings$1$3;->INSTANCE:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt$writeSquareCardIdvSettings$1$3;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p0, p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeNullable(Lokio/BufferedSink;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Lokio/BufferedSink;

    return-object p0
.end method
