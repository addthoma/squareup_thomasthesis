.class public final synthetic Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->values()[Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->SUCCESS:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->VERIFICATION_NOT_AVAILABLE:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->VERIFICATION_FAILED:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->UNKNOWN_ERROR:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    return-void
.end method
