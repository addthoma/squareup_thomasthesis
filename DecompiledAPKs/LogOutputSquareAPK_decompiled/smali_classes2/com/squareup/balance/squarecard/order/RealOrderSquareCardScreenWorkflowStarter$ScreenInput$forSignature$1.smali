.class final Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forSignature$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderSquareCardScreenWorkflowStarter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;",
        "it",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forSignature$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forSignature$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forSignature$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forSignature$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forSignature$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$GoBack;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnBackFromSignature;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnBackFromSignature;

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;

    goto :goto_0

    .line 127
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;

    if-eqz v0, :cond_1

    .line 128
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnSignatureComplete;

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;->getSignaturePayload()Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;->getStampsToRestore()Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnSignatureComplete;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;Ljava/lang/String;Ljava/util/List;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;

    goto :goto_0

    .line 129
    :cond_1
    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$InkLevelError;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnInkLevelError;

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$InkLevelError;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$InkLevelError;->getInkLevel()Lcom/squareup/cardcustomizations/signature/InkLevel;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$InkLevelError;->getSignatureAsJson()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnInkLevelError;-><init>(Lcom/squareup/cardcustomizations/signature/InkLevel;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;

    goto :goto_0

    .line 130
    :cond_2
    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$LaunchStampsGallery;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$LaunchStampsGallery;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnLaunchStampsGallery;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnLaunchStampsGallery;

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;

    goto :goto_0

    .line 131
    :cond_3
    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$StampPopulated;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$StampPopulated;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    sget-object p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnStampPopulated;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnStampPopulated;

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 100
    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput$forSignature$1;->invoke(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;)Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent;

    move-result-object p1

    return-object p1
.end method
