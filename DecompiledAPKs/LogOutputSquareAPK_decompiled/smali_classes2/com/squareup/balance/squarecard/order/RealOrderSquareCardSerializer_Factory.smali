.class public final Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer_Factory;
.super Ljava/lang/Object;
.source "RealOrderSquareCardSerializer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderScreenWorkflowStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderScreenWorkflowStarter;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderScreenWorkflowStarter;",
            ">;)",
            "Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer_Factory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;Lcom/squareup/mailorder/OrderScreenWorkflowStarter;)Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;Lcom/squareup/mailorder/OrderScreenWorkflowStarter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/mailorder/OrderScreenWorkflowStarter;

    invoke-static {v0, v1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer_Factory;->newInstance(Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;Lcom/squareup/mailorder/OrderScreenWorkflowStarter;)Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer_Factory;->get()Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;

    move-result-object v0

    return-object v0
.end method
