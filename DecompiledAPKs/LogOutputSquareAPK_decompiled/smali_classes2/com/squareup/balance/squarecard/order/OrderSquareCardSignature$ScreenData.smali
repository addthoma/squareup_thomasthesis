.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;
.super Ljava/lang/Object;
.source "OrderSquareCardSignatureScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderSquareCardSignatureScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderSquareCardSignatureScreen.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData\n*L\n1#1,55:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u000e\u0018\u00002\u00020\u0001BS\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u0012\u000e\u0008\u0002\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0002\u0010\u000fR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0015R\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0013R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0017\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001b\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
        "",
        "businessName",
        "",
        "minInkLevel",
        "",
        "maxInkLevel",
        "stamp",
        "Lcom/squareup/protos/client/bizbank/Stamp;",
        "signatureAsJson",
        "allowStampsCustomization",
        "",
        "stampsToRestore",
        "",
        "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
        "(Ljava/lang/String;FFLcom/squareup/protos/client/bizbank/Stamp;Ljava/lang/String;ZLjava/util/List;)V",
        "getAllowStampsCustomization",
        "()Z",
        "getBusinessName",
        "()Ljava/lang/String;",
        "getMaxInkLevel",
        "()F",
        "getMinInkLevel",
        "getSignatureAsJson",
        "getStamp",
        "()Lcom/squareup/protos/client/bizbank/Stamp;",
        "getStampsToRestore",
        "()Ljava/util/List;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allowStampsCustomization:Z

.field private final businessName:Ljava/lang/String;

.field private final maxInkLevel:F

.field private final minInkLevel:F

.field private final signatureAsJson:Ljava/lang/String;

.field private final stamp:Lcom/squareup/protos/client/bizbank/Stamp;

.field private final stampsToRestore:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7f

    const/4 v9, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;-><init>(Ljava/lang/String;FFLcom/squareup/protos/client/bizbank/Stamp;Ljava/lang/String;ZLjava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;FFLcom/squareup/protos/client/bizbank/Stamp;Ljava/lang/String;ZLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "FF",
            "Lcom/squareup/protos/client/bizbank/Stamp;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;)V"
        }
    .end annotation

    const-string v0, "businessName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureAsJson"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampsToRestore"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->businessName:Ljava/lang/String;

    iput p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->minInkLevel:F

    iput p3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->maxInkLevel:F

    iput-object p4, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->stamp:Lcom/squareup/protos/client/bizbank/Stamp;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->signatureAsJson:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->allowStampsCustomization:Z

    iput-object p7, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->stampsToRestore:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;FFLcom/squareup/protos/client/bizbank/Stamp;Ljava/lang/String;ZLjava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 5

    and-int/lit8 p9, p8, 0x1

    const-string v0, ""

    if-eqz p9, :cond_0

    move-object p9, v0

    goto :goto_0

    :cond_0
    move-object p9, p1

    :goto_0
    and-int/lit8 p1, p8, 0x2

    if-eqz p1, :cond_1

    const/4 p2, 0x0

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    move v1, p2

    :goto_1
    and-int/lit8 p1, p8, 0x4

    if-eqz p1, :cond_2

    const/high16 p3, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_2
    move v2, p3

    :goto_2
    and-int/lit8 p1, p8, 0x8

    if-eqz p1, :cond_3

    const/4 p1, 0x0

    .line 43
    move-object p4, p1

    check-cast p4, Lcom/squareup/protos/client/bizbank/Stamp;

    :cond_3
    move-object v3, p4

    and-int/lit8 p1, p8, 0x10

    if-eqz p1, :cond_4

    goto :goto_3

    :cond_4
    move-object v0, p5

    :goto_3
    and-int/lit8 p1, p8, 0x20

    if-eqz p1, :cond_5

    const/4 p6, 0x0

    const/4 v4, 0x0

    goto :goto_4

    :cond_5
    move v4, p6

    :goto_4
    and-int/lit8 p1, p8, 0x40

    if-eqz p1, :cond_6

    .line 46
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p7

    :cond_6
    move-object p8, p7

    move-object p1, p0

    move-object p2, p9

    move p3, v1

    move p4, v2

    move-object p5, v3

    move-object p6, v0

    move p7, v4

    invoke-direct/range {p1 .. p8}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;-><init>(Ljava/lang/String;FFLcom/squareup/protos/client/bizbank/Stamp;Ljava/lang/String;ZLjava/util/List;)V

    return-void
.end method


# virtual methods
.method public final getAllowStampsCustomization()Z
    .locals 1

    .line 45
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->allowStampsCustomization:Z

    return v0
.end method

.method public final getBusinessName()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->businessName:Ljava/lang/String;

    return-object v0
.end method

.method public final getMaxInkLevel()F
    .locals 1

    .line 42
    iget v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->maxInkLevel:F

    return v0
.end method

.method public final getMinInkLevel()F
    .locals 1

    .line 41
    iget v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->minInkLevel:F

    return v0
.end method

.method public final getSignatureAsJson()Ljava/lang/String;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->signatureAsJson:Ljava/lang/String;

    return-object v0
.end method

.method public final getStamp()Lcom/squareup/protos/client/bizbank/Stamp;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->stamp:Lcom/squareup/protos/client/bizbank/Stamp;

    return-object v0
.end method

.method public final getStampsToRestore()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->stampsToRestore:Ljava/util/List;

    return-object v0
.end method
