.class final Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$start$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderSquareCardScreenWorkflowStarter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "Lrx/Observable<",
        "+",
        "Lcom/squareup/workflow/ScreenState<",
        "+",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a2\u0012.\u0008\u0001\u0012*\u0012&\u0012$\u0012\u0004\u0012\u00020\u0004\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0005j\u0002`\u00060\u0003j\u0008\u0012\u0004\u0012\u00020\u0004`\u00070\u00020\u00012\u0006\u0010\u0008\u001a\u00020\tH\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Observable;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "it",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/rx1/Workflow;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;Lcom/squareup/workflow/rx1/Workflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$start$2;->this$0:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$start$2;->$workflow:Lcom/squareup/workflow/rx1/Workflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 75
    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$start$2;->invoke(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$start$2;->this$0:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;

    new-instance v1, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$start$2;->$workflow:Lcom/squareup/workflow/rx1/Workflow;

    check-cast v2, Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {v1, v2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    invoke-static {v0, p1, v1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;->access$toWorkflowState(Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter$ScreenInput;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
