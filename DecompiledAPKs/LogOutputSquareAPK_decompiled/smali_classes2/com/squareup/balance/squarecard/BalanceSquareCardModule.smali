.class public abstract Lcom/squareup/balance/squarecard/BalanceSquareCardModule;
.super Ljava/lang/Object;
.source "BalanceSquareCardModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00d4\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH!J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H!J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H!J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0011\u001a\u00020\u0019H!J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0011\u001a\u00020\u001cH!J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0011\u001a\u00020\u001fH!J\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H!J\u0010\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'H!J\u0010\u0010(\u001a\u00020)2\u0006\u0010\u0011\u001a\u00020\u001cH!J\u0010\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-H!J\u0010\u0010.\u001a\u00020/2\u0006\u00100\u001a\u000201H!J\u0014\u00102\u001a\u0004\u0018\u0001032\u0008\u0010\u0011\u001a\u0004\u0018\u000104H!J\u0010\u00105\u001a\u0002062\u0006\u0010\u0011\u001a\u000207H!J\u0010\u00108\u001a\u0002092\u0006\u0010:\u001a\u00020;H!J\u0010\u0010<\u001a\u00020=2\u0006\u0010>\u001a\u00020?H!J\u0010\u0010@\u001a\u00020A2\u0006\u0010B\u001a\u00020CH!\u00a8\u0006D"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/BalanceSquareCardModule;",
        "",
        "()V",
        "bindManageSquareCardWorkflow",
        "Lcom/squareup/balance/squarecard/ManageSquareCardWorkflow;",
        "realManageSquareCardWorkflow",
        "Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;",
        "bindManageSquareCardWorkflowViewFactory",
        "Lcom/squareup/balance/squarecard/ManageSquareCardWorkflowViewFactory;",
        "realManageSquareCardWorkflowViewFactory",
        "Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;",
        "bindSquareCardActivatedWorkflow",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;",
        "realSquareCardActivatedWorkflow",
        "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;",
        "bindsAuthSquareCardAnalytics",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;",
        "analytics",
        "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;",
        "bindsAuthSquareCardWorkflow",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;",
        "realAuthSquareCardScreenWorkflowStarter",
        "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;",
        "bindsCancelSquareCardAnalytics",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
        "Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;",
        "bindsMailOrderAnalytics",
        "Lcom/squareup/mailorder/MailOrderAnalytics;",
        "Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;",
        "bindsManageSquareCardAnalytics",
        "Lcom/squareup/balance/squarecard/ManageSquareCardAnalytics;",
        "Lcom/squareup/balance/squarecard/RealManageSquareCardAnalytics;",
        "bindsNotificationPreferencesRemoteDataStore",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;",
        "notificationPreferencesRemoteDataStore",
        "Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore;",
        "bindsOrderServiceHelperEndpoints",
        "Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;",
        "orderSquareCardServiceHelper",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;",
        "bindsOrderSquareCardAnalytics",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;",
        "bindsOrderSquareCardStampRequester",
        "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;",
        "realCardDataRequester",
        "Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;",
        "bindsOrderSquareCardWorkflow",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;",
        "realOrderSquareCardScreenWorkflowStarter",
        "Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;",
        "bindsSquareCardActivatedAnalytics",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;",
        "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;",
        "bindsSquareCardOrderedAnalytics",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;",
        "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;",
        "bindsSquareCardOrderedWorkflowStarter",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;",
        "realSquareCardOrderedWorkflowStarter",
        "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;",
        "bindsSquareCardResetPinWorkflow",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinWorkflow;",
        "realSquareCardResetPinWorkflow",
        "Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;",
        "bindsViewBillingAddressWorkflow",
        "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressWorkflow;",
        "realViewBillingAddressWorkflow",
        "Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindManageSquareCardWorkflow(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/balance/squarecard/ManageSquareCardWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindManageSquareCardWorkflowViewFactory(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;)Lcom/squareup/balance/squarecard/ManageSquareCardWorkflowViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindSquareCardActivatedWorkflow(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsAuthSquareCardAnalytics(Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardAnalytics;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsAuthSquareCardWorkflow(Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsCancelSquareCardAnalytics(Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsMailOrderAnalytics(Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;)Lcom/squareup/mailorder/MailOrderAnalytics;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsManageSquareCardAnalytics(Lcom/squareup/balance/squarecard/RealManageSquareCardAnalytics;)Lcom/squareup/balance/squarecard/ManageSquareCardAnalytics;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsNotificationPreferencesRemoteDataStore(Lcom/squareup/balance/squarecard/notificationpreferences/RealNotificationPreferencesRemoteDataStore;)Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsOrderServiceHelperEndpoints(Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper;)Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsOrderSquareCardAnalytics(Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalytics;)Lcom/squareup/balance/squarecard/order/OrderSquareCardAnalytics;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsOrderSquareCardStampRequester(Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;)Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsOrderSquareCardWorkflow(Lcom/squareup/balance/squarecard/order/RealOrderSquareCardScreenWorkflowStarter;)Lcom/squareup/balance/squarecard/order/OrderSquareCardScreenWorkflowStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsSquareCardActivatedAnalytics(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedAnalytics;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsSquareCardOrderedAnalytics(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsSquareCardOrderedWorkflowStarter(Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsSquareCardResetPinWorkflow(Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;)Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsViewBillingAddressWorkflow(Lcom/squareup/balance/squarecard/billingaddress/RealViewBillingAddressWorkflow;)Lcom/squareup/balance/squarecard/billingaddress/ViewBillingAddressWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
