.class final Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$updatePin$1;
.super Ljava/lang/Object;
.source "RealSquareCardResetPinWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->updatePin(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;",
        "kotlin.jvm.PlatformType",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bizbank/SetPasscodeResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$updatePin$1;->this$0:Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bizbank/SetPasscodeResponse;",
            ">;)",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinSuccess;

    check-cast p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;

    goto :goto_0

    .line 218
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinError;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$updatePin$1;->this$0:Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {v1, p1}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->access$isWeakPasscode(Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Z

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinError;-><init>(Z)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$updatePin$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;

    move-result-object p1

    return-object p1
.end method
