.class final Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSquareCardResetPinWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow;->render(Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinInput;Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;",
        "+",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinResult;",
        "event",
        "Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$2;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$2;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$2;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;",
            "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    sget-object v0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event$GoBack;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinResult$Back;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinResult$Back;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 92
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event$SubmitPin;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPin;

    check-cast p1, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event$SubmitPin;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event$SubmitPin;->getPin()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPin;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/resetpin/RealSquareCardResetPinWorkflow$render$2;->invoke(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
