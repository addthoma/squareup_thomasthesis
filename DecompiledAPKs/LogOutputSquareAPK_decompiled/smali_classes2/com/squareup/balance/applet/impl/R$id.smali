.class public final Lcom/squareup/balance/applet/impl/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/applet/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final add_amount_button:I = 0x7f0a0167

.field public static final add_amount_help_text:I = 0x7f0a0168

.field public static final add_amount_input_field:I = 0x7f0a0169

.field public static final add_amount_source:I = 0x7f0a016a

.field public static final add_money_button:I = 0x7f0a016d

.field public static final amount:I = 0x7f0a01b7

.field public static final amount_hint:I = 0x7f0a01be

.field public static final amount_value:I = 0x7f0a01bf

.field public static final applet_sections_list:I = 0x7f0a01c7

.field public static final available_balance:I = 0x7f0a01e7

.field public static final available_balance_title:I = 0x7f0a01e8

.field public static final balance_card:I = 0x7f0a020f

.field public static final balance_empty_card_activity:I = 0x7f0a0211

.field public static final balance_error_card_activity:I = 0x7f0a0212

.field public static final balance_spinner:I = 0x7f0a0213

.field public static final balance_transactions:I = 0x7f0a0215

.field public static final confirm_transfer_view:I = 0x7f0a038d

.field public static final date:I = 0x7f0a0546

.field public static final deposit_account:I = 0x7f0a0568

.field public static final deposit_hint:I = 0x7f0a0569

.field public static final desired_deposit:I = 0x7f0a0590

.field public static final error_message:I = 0x7f0a071e

.field public static final expense_type_business:I = 0x7f0a0735

.field public static final expense_type_personal:I = 0x7f0a0736

.field public static final instant_deposit_hint:I = 0x7f0a084c

.field public static final instant_transfer_fee:I = 0x7f0a0857

.field public static final instant_transfer_fee_section:I = 0x7f0a0858

.field public static final load_more_spinner:I = 0x7f0a094e

.field public static final recent_activity_container:I = 0x7f0a0d14

.field public static final recent_activity_empty:I = 0x7f0a0d15

.field public static final recent_activty_title:I = 0x7f0a0d16

.field public static final remaining_balance:I = 0x7f0a0d54

.field public static final square_card_instant_deposit_confirm:I = 0x7f0a0f09

.field public static final square_card_instant_deposit_help:I = 0x7f0a0f0a

.field public static final square_card_instant_deposit_result:I = 0x7f0a0f0b

.field public static final square_card_instant_deposit_result_container:I = 0x7f0a0f0c

.field public static final square_card_instant_deposit_spinner_holder:I = 0x7f0a0f0d

.field public static final square_card_instant_deposit_upsell:I = 0x7f0a0f0e

.field public static final square_card_progress_spinner_message:I = 0x7f0a0f14

.field public static final starting_balance:I = 0x7f0a0f30

.field public static final transaction_amount_detail:I = 0x7f0a1064

.field public static final transaction_date:I = 0x7f0a1066

.field public static final transaction_description:I = 0x7f0a1068

.field public static final transaction_detail_view:I = 0x7f0a1069

.field public static final transaction_expense_type:I = 0x7f0a106a

.field public static final transaction_load_more_retry:I = 0x7f0a106e

.field public static final transaction_time:I = 0x7f0a106f

.field public static final transaction_total:I = 0x7f0a1070

.field public static final transactions:I = 0x7f0a1072

.field public static final transfer_arrives_speed:I = 0x7f0a1076

.field public static final transfer_result_message:I = 0x7f0a1077

.field public static final transfer_result_spinner:I = 0x7f0a1078

.field public static final transfer_result_view:I = 0x7f0a1079

.field public static final transfer_speed_instant:I = 0x7f0a107a

.field public static final transfer_speed_instant_fee_rate:I = 0x7f0a107b

.field public static final transfer_speed_instant_group:I = 0x7f0a107c

.field public static final transfer_speed_instant_text:I = 0x7f0a107d

.field public static final transfer_speed_standard:I = 0x7f0a107e

.field public static final transfer_speed_standard_fee_rate:I = 0x7f0a107f

.field public static final transfer_to:I = 0x7f0a1080

.field public static final transfer_to_bank_button:I = 0x7f0a1081

.field public static final transfer_to_bank_view:I = 0x7f0a1082

.field public static final transferring_to_bank:I = 0x7f0a1083


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
