.class public final Lcom/squareup/balance/transferout/RealTransferOutWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealTransferOutWorkflow.kt"

# interfaces
.implements Lcom/squareup/balance/transferout/TransferOutWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/balance/transferout/TransferOutWorkflowProps;",
        "Lcom/squareup/balance/transferout/TransferOutState;",
        "Lcom/squareup/balance/transferout/TransferOutWorkflowResult;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/balance/transferout/TransferOutWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealTransferOutWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealTransferOutWorkflow.kt\ncom/squareup/balance/transferout/RealTransferOutWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,46:1\n32#2,12:47\n*E\n*S KotlinDebug\n*F\n+ 1 RealTransferOutWorkflow.kt\ncom/squareup/balance/transferout/RealTransferOutWorkflow\n*L\n30#1,12:47\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u000128\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\"\u0012 \u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n0\u0002B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u000bJ\u001a\u0010\u000c\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u00032\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016JJ\u0010\u0010\u001a \u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u0006\u0010\r\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u00042\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0004H\u0016\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/balance/transferout/RealTransferOutWorkflow;",
        "Lcom/squareup/balance/transferout/TransferOutWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/balance/transferout/TransferOutWorkflowProps;",
        "Lcom/squareup/balance/transferout/TransferOutState;",
        "Lcom/squareup/balance/transferout/TransferOutWorkflowResult;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/balance/transferout/TransferOutScreen;",
        "()V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/balance/transferout/TransferOutWorkflowProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/transferout/TransferOutState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 47
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 52
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 54
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 55
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 56
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 58
    :cond_3
    check-cast v1, Lcom/squareup/balance/transferout/TransferOutState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 30
    :cond_4
    sget-object p1, Lcom/squareup/balance/transferout/TransferOutState$EnterAmount;->INSTANCE:Lcom/squareup/balance/transferout/TransferOutState$EnterAmount;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/transferout/TransferOutState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/balance/transferout/TransferOutWorkflowProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/transferout/RealTransferOutWorkflow;->initialState(Lcom/squareup/balance/transferout/TransferOutWorkflowProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/transferout/TransferOutState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/balance/transferout/TransferOutWorkflowProps;

    check-cast p2, Lcom/squareup/balance/transferout/TransferOutState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/transferout/RealTransferOutWorkflow;->render(Lcom/squareup/balance/transferout/TransferOutWorkflowProps;Lcom/squareup/balance/transferout/TransferOutState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/balance/transferout/TransferOutWorkflowProps;Lcom/squareup/balance/transferout/TransferOutState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/transferout/TransferOutWorkflowProps;",
            "Lcom/squareup/balance/transferout/TransferOutState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/transferout/TransferOutState;",
            "-",
            "Lcom/squareup/balance/transferout/TransferOutWorkflowResult;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object p1, Lcom/squareup/balance/transferout/TransferOutState$EnterAmount;->INSTANCE:Lcom/squareup/balance/transferout/TransferOutState$EnterAmount;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const/4 p3, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_4

    .line 38
    sget-object p1, Lcom/squareup/balance/transferout/TransferOutState$SubmitInstantDeposit;->INSTANCE:Lcom/squareup/balance/transferout/TransferOutState$SubmitInstantDeposit;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 39
    sget-object p1, Lcom/squareup/balance/transferout/TransferOutState$SubmitNormalDeposit;->INSTANCE:Lcom/squareup/balance/transferout/TransferOutState$SubmitNormalDeposit;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 40
    sget-object p1, Lcom/squareup/balance/transferout/TransferOutState$TransferSucceeded;->INSTANCE:Lcom/squareup/balance/transferout/TransferOutState$TransferSucceeded;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 41
    sget-object p1, Lcom/squareup/balance/transferout/TransferOutState$TransferFailed;->INSTANCE:Lcom/squareup/balance/transferout/TransferOutState$TransferFailed;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lkotlin/NotImplementedError;

    invoke-direct {p1, v0, p3, v0}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 40
    :cond_1
    new-instance p1, Lkotlin/NotImplementedError;

    invoke-direct {p1, v0, p3, v0}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 39
    :cond_2
    new-instance p1, Lkotlin/NotImplementedError;

    invoke-direct {p1, v0, p3, v0}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 38
    :cond_3
    new-instance p1, Lkotlin/NotImplementedError;

    invoke-direct {p1, v0, p3, v0}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 37
    :cond_4
    new-instance p1, Lkotlin/NotImplementedError;

    invoke-direct {p1, v0, p3, v0}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public snapshotState(Lcom/squareup/balance/transferout/TransferOutState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/balance/transferout/TransferOutState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/transferout/RealTransferOutWorkflow;->snapshotState(Lcom/squareup/balance/transferout/TransferOutState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
