.class public abstract Lcom/squareup/balance/transferout/TransferOutState;
.super Ljava/lang/Object;
.source "TransferOutState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/transferout/TransferOutState$EnterAmount;,
        Lcom/squareup/balance/transferout/TransferOutState$SubmitInstantDeposit;,
        Lcom/squareup/balance/transferout/TransferOutState$SubmitNormalDeposit;,
        Lcom/squareup/balance/transferout/TransferOutState$TransferSucceeded;,
        Lcom/squareup/balance/transferout/TransferOutState$TransferFailed;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0005\u0003\u0004\u0005\u0006\u0007B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0005\u0008\t\n\u000b\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/balance/transferout/TransferOutState;",
        "Landroid/os/Parcelable;",
        "()V",
        "EnterAmount",
        "SubmitInstantDeposit",
        "SubmitNormalDeposit",
        "TransferFailed",
        "TransferSucceeded",
        "Lcom/squareup/balance/transferout/TransferOutState$EnterAmount;",
        "Lcom/squareup/balance/transferout/TransferOutState$SubmitInstantDeposit;",
        "Lcom/squareup/balance/transferout/TransferOutState$SubmitNormalDeposit;",
        "Lcom/squareup/balance/transferout/TransferOutState$TransferSucceeded;",
        "Lcom/squareup/balance/transferout/TransferOutState$TransferFailed;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 7
    invoke-direct {p0}, Lcom/squareup/balance/transferout/TransferOutState;-><init>()V

    return-void
.end method
