.class final Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$loadMoreData$1;
.super Ljava/lang/Object;
.source "RealBalanceActivityRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->loadMoreData(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/balance/activity/data/BalanceActivity;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/activity/data/BalanceActivity;",
        "batchResult",
        "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $balanceActivity:Lcom/squareup/balance/activity/data/BalanceActivity;

.field final synthetic $isPending:Z

.field final synthetic $result:Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;ZLcom/squareup/balance/activity/data/BalanceActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$loadMoreData$1;->$result:Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    iput-boolean p2, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$loadMoreData$1;->$isPending:Z

    iput-object p3, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$loadMoreData$1;->$balanceActivity:Lcom/squareup/balance/activity/data/BalanceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/balance/activity/data/UnifiedActivityResult;)Lcom/squareup/balance/activity/data/BalanceActivity;
    .locals 3

    const-string v0, "batchResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    instance-of v0, p1, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$Success;

    if-eqz v0, :cond_0

    .line 129
    new-instance v0, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$Success;

    .line 131
    iget-object v1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$loadMoreData$1;->$result:Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    invoke-virtual {v1}, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;->getActivities()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    check-cast p1, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$Success;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$Success;->getActivities()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    .line 133
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$Success;->getBatchToken()Ljava/lang/String;

    move-result-object p1

    .line 129
    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$Success;-><init>(Ljava/util/List;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    goto :goto_0

    .line 138
    :cond_0
    new-instance p1, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$ErrorFetchingMore;

    .line 139
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$loadMoreData$1;->$result:Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    invoke-virtual {v0}, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;->getActivities()Ljava/util/List;

    move-result-object v0

    .line 140
    iget-object v1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$loadMoreData$1;->$result:Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    invoke-virtual {v1}, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;->getBatchToken()Ljava/lang/String;

    move-result-object v1

    .line 138
    invoke-direct {p1, v0, v1}, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$ErrorFetchingMore;-><init>(Ljava/util/List;Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    .line 144
    :goto_0
    iget-boolean p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$loadMoreData$1;->$isPending:Z

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    .line 145
    iget-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$loadMoreData$1;->$balanceActivity:Lcom/squareup/balance/activity/data/BalanceActivity;

    check-cast v0, Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    const/4 v2, 0x2

    invoke-static {p1, v0, v1, v2, v1}, Lcom/squareup/balance/activity/data/BalanceActivity;->copy$default(Lcom/squareup/balance/activity/data/BalanceActivity;Lcom/squareup/balance/activity/data/UnifiedActivityResult;Lcom/squareup/balance/activity/data/UnifiedActivityResult;ILjava/lang/Object;)Lcom/squareup/balance/activity/data/BalanceActivity;

    move-result-object p1

    goto :goto_1

    .line 147
    :cond_1
    iget-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$loadMoreData$1;->$balanceActivity:Lcom/squareup/balance/activity/data/BalanceActivity;

    check-cast v0, Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    const/4 v2, 0x1

    invoke-static {p1, v1, v0, v2, v1}, Lcom/squareup/balance/activity/data/BalanceActivity;->copy$default(Lcom/squareup/balance/activity/data/BalanceActivity;Lcom/squareup/balance/activity/data/UnifiedActivityResult;Lcom/squareup/balance/activity/data/UnifiedActivityResult;ILjava/lang/Object;)Lcom/squareup/balance/activity/data/BalanceActivity;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$loadMoreData$1;->apply(Lcom/squareup/balance/activity/data/UnifiedActivityResult;)Lcom/squareup/balance/activity/data/BalanceActivity;

    move-result-object p1

    return-object p1
.end method
