.class public final Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore;
.super Ljava/lang/Object;
.source "RealRemoteBalanceActivityDetailsDataStore.kt"

# interfaces
.implements Lcom/squareup/balance/activity/data/RemoteBalanceActivityDetailsDataStore;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0018\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore;",
        "Lcom/squareup/balance/activity/data/RemoteBalanceActivityDetailsDataStore;",
        "remoteService",
        "Lcom/squareup/balance/activity/data/service/BalanceActivityService;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/balance/activity/data/service/BalanceActivityService;Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "fetchDetails",
        "Lio/reactivex/Single;",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails;",
        "activityToken",
        "",
        "updateCategory",
        "Lio/reactivex/Completable;",
        "transactionToken",
        "isPersonalExpense",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final remoteService:Lcom/squareup/balance/activity/data/service/BalanceActivityService;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/activity/data/service/BalanceActivityService;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "remoteService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore;->remoteService:Lcom/squareup/balance/activity/data/service/BalanceActivityService;

    iput-object p2, p0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method


# virtual methods
.method public fetchDetails(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails;",
            ">;"
        }
    .end annotation

    const-string v0, "activityToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;-><init>()V

    .line 22
    invoke-virtual {v0, p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;->activity_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;

    move-result-object p1

    .line 23
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    const-string v1, "settings.userSettings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;

    move-result-object p1

    .line 24
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest$Builder;->build()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest;

    move-result-object p1

    .line 26
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore;->remoteService:Lcom/squareup/balance/activity/data/service/BalanceActivityService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/balance/activity/data/service/BalanceActivityService;->getUnifiedActivityDetails(Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsRequest;)Lcom/squareup/balance/activity/data/service/BalanceActivityResponse;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/service/BalanceActivityResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 28
    sget-object v0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$fetchDetails$1;->INSTANCE:Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$fetchDetails$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "remoteService.getUnified\u2026ils\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public updateCategory(Ljava/lang/String;Z)Lio/reactivex/Completable;
    .locals 1

    const-string v0, "transactionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest$Builder;-><init>()V

    .line 44
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest$Builder;->transaction_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest$Builder;

    move-result-object p1

    .line 45
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest$Builder;->is_personal_expense(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest$Builder;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest;

    move-result-object p1

    .line 48
    iget-object p2, p0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore;->remoteService:Lcom/squareup/balance/activity/data/service/BalanceActivityService;

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/balance/activity/data/service/BalanceActivityService;->setTransactionCategory(Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 50
    sget-object p2, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$updateCategory$1;->INSTANCE:Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$updateCategory$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Lio/reactivex/Single;->ignoreElement()Lio/reactivex/Completable;

    move-result-object p1

    const-string p2, "remoteService.setTransac\u2026\n        .ignoreElement()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
