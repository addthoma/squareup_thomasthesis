.class final Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1$1;
.super Ljava/lang/Object;
.source "RealBalanceActivityRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;->apply(Lcom/squareup/balance/activity/data/BalanceActivity;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/balance/activity/data/BalanceActivity;",
        "balanceActivity",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1$1;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/balance/activity/data/BalanceActivity;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;"
        }
    .end annotation

    const-string v0, "balanceActivity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1$1;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;

    iget-object v0, v0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;

    iget-object v1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1$1;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;

    iget-object v1, v1, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;->$type:Lcom/squareup/balance/activity/data/BalanceActivityType;

    invoke-static {v0, v1, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->access$loadMoreData(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/balance/activity/data/BalanceActivity;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/balance/activity/data/BalanceActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1$1;->apply(Lcom/squareup/balance/activity/data/BalanceActivity;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
