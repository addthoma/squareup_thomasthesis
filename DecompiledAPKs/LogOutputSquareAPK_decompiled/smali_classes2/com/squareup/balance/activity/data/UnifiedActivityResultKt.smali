.class public final Lcom/squareup/balance/activity/data/UnifiedActivityResultKt;
.super Ljava/lang/Object;
.source "UnifiedActivityResult.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUnifiedActivityResult.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UnifiedActivityResult.kt\ncom/squareup/balance/activity/data/UnifiedActivityResultKt\n*L\n1#1,29:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "hasBatchToken",
        "",
        "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final hasBatchToken(Lcom/squareup/balance/activity/data/UnifiedActivityResult;)Z
    .locals 3

    const-string v0, "$this$hasBatchToken"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    instance-of v0, p0, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    check-cast p0, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    invoke-virtual {p0}, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;->getBatchToken()Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    if-nez p0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    return v1
.end method
