.class public interface abstract Lcom/squareup/balance/activity/data/BalanceActivityRepository;
.super Ljava/lang/Object;
.source "BalanceActivityRepository.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0016\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0016\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00082\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0008\u0010\t\u001a\u00020\nH&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/balance/activity/data/BalanceActivityRepository;",
        "",
        "balanceActivity",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/balance/activity/data/BalanceActivity;",
        "type",
        "Lcom/squareup/balance/activity/data/BalanceActivityType;",
        "fetchMoreBalanceActivity",
        "Lio/reactivex/Single;",
        "reset",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract balanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;)Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;"
        }
    .end annotation
.end method

.method public abstract fetchMoreBalanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;"
        }
    .end annotation
.end method

.method public abstract reset()V
.end method
