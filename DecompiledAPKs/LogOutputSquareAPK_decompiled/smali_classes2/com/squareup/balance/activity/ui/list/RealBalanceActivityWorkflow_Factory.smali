.class public final Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealBalanceActivityWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsWorkflow;",
            ">;)",
            "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsWorkflow;",
            ">;)",
            "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow_Factory;->newInstance(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow_Factory;->get()Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;

    move-result-object v0

    return-object v0
.end method
