.class public final synthetic Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->values()[Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->CARD_PAYMENT:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->PAYROLL_CANCELLATION:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->DEBIT_CARD_TRANSFER_IN:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->STANDARD_TRANSFER_OUT:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->INSTANT_TRANSFER_OUT:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->CARD_PROCESSING_SALES:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->CARD_PROCESSING_ADJUSTMENTS:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    return-void
.end method
