.class public final Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealBalanceActivityDetailsWorkflow.kt"

# interfaces
.implements Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;",
        "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBalanceActivityDetailsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBalanceActivityDetailsWorkflow.kt\ncom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,138:1\n32#2,12:139\n*E\n*S KotlinDebug\n*F\n+ 1 RealBalanceActivityDetailsWorkflow.kt\ncom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow\n*L\n69#1,12:139\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u000126\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n0\u0002:\u0001\u001bB\u001f\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u001a\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00032\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016JH\u0010\u0016\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u0006\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00042\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0004H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;",
        "Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;",
        "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;",
        "",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityScreen;",
        "loadingWorkflow",
        "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;",
        "errorWorkflow",
        "Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;",
        "successWorkflow",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;",
        "(Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final errorWorkflow:Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;

.field private final loadingWorkflow:Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;

.field private final successWorkflow:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loadingWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "successWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;->loadingWorkflow:Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;->errorWorkflow:Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;

    iput-object p3, p0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;->successWorkflow:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 139
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 144
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 146
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 147
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 148
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 150
    :cond_3
    check-cast v1, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 69
    :cond_4
    sget-object p1, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$FetchingBalanceActivityDetails;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$FetchingBalanceActivityDetails;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;->initialState(Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;

    check-cast p2, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;->render(Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;",
            "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$FetchingBalanceActivityDetails;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$FetchingBalanceActivityDetails;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object p2, p0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;->loadingWorkflow:Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/Workflow;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;->getActivity()Lcom/squareup/protos/bizbank/UnifiedActivity;

    move-result-object v2

    const/4 v3, 0x0

    sget-object p1, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$render$1;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$render$1;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 92
    :cond_0
    instance-of v0, p2, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$DisplayingBalanceActivityDetails;

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;->successWorkflow:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 94
    new-instance v3, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    .line 95
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;->getActivity()Lcom/squareup/protos/bizbank/UnifiedActivity;

    move-result-object p1

    .line 96
    check-cast p2, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$DisplayingBalanceActivityDetails;

    invoke-virtual {p2}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$DisplayingBalanceActivityDetails;->getDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object p2

    .line 94
    invoke-direct {v3, p1, p2}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)V

    const/4 v4, 0x0

    .line 98
    sget-object p1, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$render$2;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$render$2;

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 92
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 101
    :cond_1
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$DisplayingBalanceActivityFetchingError;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState$DisplayingBalanceActivityFetchingError;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 102
    iget-object p2, p0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;->errorWorkflow:Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/Workflow;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;->getActivity()Lcom/squareup/protos/bizbank/UnifiedActivity;

    move-result-object v2

    const/4 v3, 0x0

    sget-object p1, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$render$3;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$render$3;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;->snapshotState(Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
