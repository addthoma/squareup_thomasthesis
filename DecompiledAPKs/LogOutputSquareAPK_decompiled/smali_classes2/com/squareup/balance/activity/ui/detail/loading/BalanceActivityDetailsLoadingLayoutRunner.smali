.class public final Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner;
.super Ljava/lang/Object;
.source "BalanceActivityDetailsLoadingLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBalanceActivityDetailsLoadingLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BalanceActivityDetailsLoadingLayoutRunner.kt\ncom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner\n*L\n1#1,48:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000fB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "showBackButton",
        "",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Binding",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final showBackButton:Z

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner;->view:Landroid/view/View;

    .line 17
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 18
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner;->view:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner;->showBackButton:Z

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 3

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object p2, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 24
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 25
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;->getTitle()Lcom/squareup/util/ViewString;

    move-result-object v1

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 27
    iget-boolean v1, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner;->showBackButton:Z

    if-eqz v1, :cond_0

    .line 28
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner$showRendering$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner$showRendering$$inlined$apply$lambda$1;-><init>(Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner;Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 30
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 33
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 34
    iget-object p2, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner$showRendering$2;

    invoke-direct {v0, p1}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner$showRendering$2;-><init>(Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner;->showRendering(Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
