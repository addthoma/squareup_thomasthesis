.class public final Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow_Factory;
.super Ljava/lang/Object;
.source "BalanceActivityDetailsLoadingWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;",
            ">;)",
            "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow_Factory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;)Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;-><init>(Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;

    invoke-static {v0, v1}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow_Factory;->newInstance(Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;)Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow_Factory;->get()Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;

    move-result-object v0

    return-object v0
.end method
