.class public interface abstract Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;
.super Ljava/lang/Object;
.source "BalanceActivityMapper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J \u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u000c2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u0003H&J\u0010\u0010\u000e\u001a\u00020\t2\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;",
        "",
        "isPositiveAmount",
        "",
        "activity",
        "Lcom/squareup/protos/bizbank/UnifiedActivity;",
        "label",
        "",
        "runningBalanceAmount",
        "",
        "shouldCrossOutAmount",
        "subLabel",
        "Lcom/squareup/resources/TextModel;",
        "isPending",
        "transactionAmount",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract isPositiveAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Z
.end method

.method public abstract label(Lcom/squareup/protos/bizbank/UnifiedActivity;)Ljava/lang/String;
.end method

.method public abstract runningBalanceAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Ljava/lang/CharSequence;
.end method

.method public abstract shouldCrossOutAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Z
.end method

.method public abstract subLabel(Lcom/squareup/protos/bizbank/UnifiedActivity;Z)Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/bizbank/UnifiedActivity;",
            "Z)",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end method

.method public abstract transactionAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Ljava/lang/CharSequence;
.end method
