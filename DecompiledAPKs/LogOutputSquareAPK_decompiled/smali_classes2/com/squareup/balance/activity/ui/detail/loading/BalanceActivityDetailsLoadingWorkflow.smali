.class public final Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "BalanceActivityDetailsLoadingWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/protos/bizbank/UnifiedActivity;",
        "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBalanceActivityDetailsLoadingWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BalanceActivityDetailsLoadingWorkflow.kt\ncom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,52:1\n149#2,5:53\n85#3:58\n240#4:59\n276#5:60\n*E\n*S KotlinDebug\n*F\n+ 1 BalanceActivityDetailsLoadingWorkflow.kt\ncom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow\n*L\n43#1,5:53\n49#1:58\n49#1:59\n49#1:60\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0018\u000020\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0002`\u00080\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0016\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0011\u001a\u00020\u0002H\u0002J@\u0010\u0012\u001a\u001e\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0002`\u00082\u0006\u0010\u0013\u001a\u00020\u00022\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00030\u0015H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/protos/bizbank/UnifiedActivity;",
        "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityScreen;",
        "detailsRepository",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;",
        "mapper",
        "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;",
        "(Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;)V",
        "fetchDetails",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails;",
        "activity",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final detailsRepository:Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;

.field private final mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "detailsRepository"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mapper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;->detailsRepository:Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;->mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;

    return-void
.end method

.method private final fetchDetails(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/bizbank/UnifiedActivity;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;->detailsRepository:Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_token:Ljava/lang/String;

    const-string v1, "activity.activity_token"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;->balanceActivityDetails(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 58
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow$fetchDetails$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow$fetchDetails$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 59
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 60
    const-class v0, Lcom/squareup/balance/activity/data/BalanceActivityDetails;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/protos/bizbank/UnifiedActivity;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;->render(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/bizbank/UnifiedActivity;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow$render$sink$1;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow$render$sink$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p2, v0}, Lcom/squareup/workflow/RenderContextKt;->makeEventSink(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;->fetchDetails(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/workflow/Worker;

    move-result-object v2

    new-instance v1, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow$render$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow$render$1;-><init>(Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;)V

    move-object v4, v1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p2

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 40
    new-instance p2, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;

    .line 41
    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow;->mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;

    invoke-virtual {v1, p1}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->actionBarTitle(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/util/ViewString;

    move-result-object p1

    .line 42
    new-instance v1, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow$render$2;

    invoke-direct {v1, v0}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 40
    invoke-direct {p2, p1, v1}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 54
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 55
    const-class v0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 56
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 54
    invoke-direct {p1, v0, p2, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 44
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
