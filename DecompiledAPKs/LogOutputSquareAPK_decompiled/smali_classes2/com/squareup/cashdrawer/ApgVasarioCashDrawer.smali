.class public final Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;
.super Ljava/lang/Object;
.source "ApgVasarioCashDrawer.java"

# interfaces
.implements Lcom/squareup/cashdrawer/CashDrawer;
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$DiscovererListener;,
        Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;
    }
.end annotation


# static fields
.field static final APG_VENDOR_ID:I = 0x7c5

.field private static final GET_STATUS_BYTES:[B

.field private static final NAME:Ljava/lang/String; = "APG VB554"

.field private static final OPEN_DRAWER_BYTES:[B


# instance fields
.field private device:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;

.field private listener:Lcom/squareup/cashdrawer/CashDrawer$Listener;

.field private mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation
.end field

.field private final manager:Lcom/squareup/hardware/usb/UsbManager;

.field private final usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x40

    aput-byte v2, v0, v1

    .line 29
    sput-object v0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->GET_STATUS_BYTES:[B

    const/4 v0, 0x2

    new-array v0, v0, [B

    .line 30
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->OPEN_DRAWER_BYTES:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x1t
        0x1t
    .end array-data
.end method

.method public constructor <init>(Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    .line 43
    iput-object p2, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->manager:Lcom/squareup/hardware/usb/UsbManager;

    .line 44
    iput-object p3, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method

.method static synthetic access$100()[B
    .locals 1

    .line 24
    sget-object v0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->OPEN_DRAWER_BYTES:[B

    return-object v0
.end method

.method static synthetic access$200()[B
    .locals 1

    .line 24
    sget-object v0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->GET_STATUS_BYTES:[B

    return-object v0
.end method

.method static synthetic access$300(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)Lcom/squareup/hardware/usb/UsbManager;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->manager:Lcom/squareup/hardware/usb/UsbManager;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)Lcom/squareup/cashdrawer/CashDrawer$Listener;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->listener:Lcom/squareup/cashdrawer/CashDrawer$Listener;

    return-object p0
.end method

.method static synthetic access$502(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;)Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->device:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;

    return-object p1
.end method

.method static synthetic access$600(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-object p0
.end method

.method private ensureConnected()V
    .locals 2

    .line 82
    invoke-virtual {p0}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 83
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Drawer is not connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "APG VB554"

    return-object v0
.end method

.method public isAvailable()Z
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->device:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isDrawerOpen()Z
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->forbid()V

    .line 71
    invoke-direct {p0}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->ensureConnected()V

    .line 72
    iget-object v0, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->device:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;

    invoke-static {v0}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;->access$000(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;)Z

    move-result v0

    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 49
    iget-object p1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    new-instance v0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$DiscovererListener;

    invoke-direct {v0, p0}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$DiscovererListener;-><init>(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)V

    const/16 v1, 0x7c5

    invoke-virtual {p1, v1, v0}, Lcom/squareup/usb/UsbDiscoverer;->setDeviceListenerForVendorId(ILcom/squareup/usb/UsbDiscoverer$DeviceListener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    const/16 v1, 0x7c5

    invoke-virtual {v0, v1}, Lcom/squareup/usb/UsbDiscoverer;->removeDeviceListenerForVendorId(I)V

    return-void
.end method

.method public openDrawer()V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->forbid()V

    .line 77
    invoke-direct {p0}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->ensureConnected()V

    .line 78
    iget-object v0, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->device:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;->openCashDrawer()V

    return-void
.end method

.method public setListener(Lcom/squareup/cashdrawer/CashDrawer$Listener;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->listener:Lcom/squareup/cashdrawer/CashDrawer$Listener;

    return-void
.end method
