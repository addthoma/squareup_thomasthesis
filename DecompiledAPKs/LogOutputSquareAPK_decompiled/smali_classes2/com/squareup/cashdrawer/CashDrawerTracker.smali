.class public Lcom/squareup/cashdrawer/CashDrawerTracker;
.super Ljava/lang/Object;
.source "CashDrawerTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;
    }
.end annotation


# instance fields
.field private final apgVasarioCashDrawer:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

.field private final apgVasarioCashDrawerListener:Lcom/squareup/cashdrawer/CashDrawer$Listener;

.field private final backgroundThreadExecutor:Ljava/util/concurrent/Executor;

.field private final cashDrawerSpooler:Lcom/squareup/print/PrintSpooler;

.field private final hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final printerStations:Lcom/squareup/print/PrinterStations;


# direct methods
.method public constructor <init>(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterStations;Ljava/util/concurrent/Executor;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/thread/executor/MainThread;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/cashdrawer/CashDrawerExecutor;
        .end annotation
    .end param
    .param p6    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->apgVasarioCashDrawer:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    .line 56
    iput-object p3, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    .line 57
    iput-object p2, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->cashDrawerSpooler:Lcom/squareup/print/PrintSpooler;

    .line 58
    iput-object p4, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 59
    iput-object p5, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->backgroundThreadExecutor:Ljava/util/concurrent/Executor;

    .line 60
    iput-object p6, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    .line 61
    iput-object p7, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 63
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->listeners:Ljava/util/List;

    .line 65
    new-instance p2, Lcom/squareup/cashdrawer/CashDrawerTracker$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/cashdrawer/CashDrawerTracker$1;-><init>(Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)V

    iput-object p2, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->apgVasarioCashDrawerListener:Lcom/squareup/cashdrawer/CashDrawer$Listener;

    .line 75
    iget-object p2, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->apgVasarioCashDrawerListener:Lcom/squareup/cashdrawer/CashDrawer$Listener;

    invoke-virtual {p1, p2}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->setListener(Lcom/squareup/cashdrawer/CashDrawer$Listener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/cashdrawer/CashDrawer;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/cashdrawer/CashDrawerTracker;->notifyListenersForCashDrawerConnected(Lcom/squareup/cashdrawer/CashDrawer;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/cashdrawer/CashDrawer;Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/squareup/cashdrawer/CashDrawerTracker;->notifyListenersForCashDrawerDisconnected(Lcom/squareup/cashdrawer/CashDrawer;Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V

    return-void
.end method

.method private addHardwarePrinterIfConnected(Ljava/util/Collection;Lcom/squareup/print/PrinterStation;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;",
            "Lcom/squareup/print/PrinterStation;",
            ")V"
        }
    .end annotation

    .line 167
    invoke-virtual {p2}, Lcom/squareup/print/PrinterStation;->hasHardwarePrinterSelected()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 170
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/print/PrinterStation;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object p2

    .line 171
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v0, p2}, Lcom/squareup/print/HardwarePrinterTracker;->getHardwarePrinter(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 174
    invoke-interface {p1, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method private getHardwarePrintersWithCashDrawers()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation

    .line 155
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 158
    iget-object v1, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->printerStations:Lcom/squareup/print/PrinterStations;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {v1, v2}, Lcom/squareup/print/PrinterStations;->getEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object v1

    .line 159
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/PrinterStation;

    .line 160
    invoke-direct {p0, v0, v2}, Lcom/squareup/cashdrawer/CashDrawerTracker;->addHardwarePrinterIfConnected(Ljava/util/Collection;Lcom/squareup/print/PrinterStation;)V

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private notifyListenersForCashDrawerConnected(Lcom/squareup/cashdrawer/CashDrawer;)V
    .locals 2

    .line 179
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cashdrawer/-$$Lambda$CashDrawerTracker$LheIyxBjmSbRQUgnCl7pB5RRmdM;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cashdrawer/-$$Lambda$CashDrawerTracker$LheIyxBjmSbRQUgnCl7pB5RRmdM;-><init>(Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/cashdrawer/CashDrawer;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private notifyListenersForCashDrawerDisconnected(Lcom/squareup/cashdrawer/CashDrawer;Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V
    .locals 2

    .line 188
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cashdrawer/-$$Lambda$CashDrawerTracker$9qmLZRgF4IuZfhAPANXG6hHsyck;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/cashdrawer/-$$Lambda$CashDrawerTracker$9qmLZRgF4IuZfhAPANXG6hHsyck;-><init>(Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/cashdrawer/CashDrawer;Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private notifyListenersForCashDrawersOpened()V
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cashdrawer/-$$Lambda$CashDrawerTracker$-dNpL-tgf6bWdTZSC5wbEl8Lq5w;

    invoke-direct {v1, p0}, Lcom/squareup/cashdrawer/-$$Lambda$CashDrawerTracker$-dNpL-tgf6bWdTZSC5wbEl8Lq5w;-><init>(Lcom/squareup/cashdrawer/CashDrawerTracker;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public addListener(Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getAvailableCashDrawers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cashdrawer/CashDrawer;",
            ">;"
        }
    .end annotation

    .line 87
    invoke-virtual {p0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->hasAvailableCashDrawers()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 89
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 91
    iget-object v1, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->apgVasarioCashDrawer:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-virtual {v1}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 92
    iget-object v1, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->apgVasarioCashDrawer:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object v0
.end method

.method public getAvailablePrinterCashDrawerCount()I
    .locals 1

    .line 105
    invoke-direct {p0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->getHardwarePrintersWithCashDrawers()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    return v0
.end method

.method public getAvailableUsbCashDrawerCount()I
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->apgVasarioCashDrawer:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->isAvailable()Z

    move-result v0

    return v0
.end method

.method public hasAvailableCashDrawers()Z
    .locals 1

    .line 115
    invoke-virtual {p0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->getAvailableUsbCashDrawerCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$notifyListenersForCashDrawerConnected$1$CashDrawerTracker(Lcom/squareup/cashdrawer/CashDrawer;)V
    .locals 2

    .line 180
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;

    .line 181
    invoke-interface {v1, p1}, Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;->cashDrawerConnected(Lcom/squareup/cashdrawer/CashDrawer;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic lambda$notifyListenersForCashDrawerDisconnected$2$CashDrawerTracker(Lcom/squareup/cashdrawer/CashDrawer;Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V
    .locals 2

    .line 189
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;

    .line 190
    invoke-interface {v1, p1, p2}, Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;->cashDrawerDisconnected(Lcom/squareup/cashdrawer/CashDrawer;Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic lambda$notifyListenersForCashDrawersOpened$3$CashDrawerTracker()V
    .locals 2

    .line 197
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;

    .line 198
    invoke-interface {v1}, Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;->cashDrawersOpened()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic lambda$openAllCashDrawers$0$CashDrawerTracker(Ljava/util/Collection;)V
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->apgVasarioCashDrawer:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->apgVasarioCashDrawer:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->openDrawer()V

    .line 142
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->cashDrawerSpooler:Lcom/squareup/print/PrintSpooler;

    invoke-virtual {v0, p1}, Lcom/squareup/print/PrintSpooler;->enqueuePriorityOpenCashDrawersFor(Ljava/lang/Iterable;)V

    .line 146
    :cond_1
    invoke-direct {p0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->notifyListenersForCashDrawersOpened()V

    return-void
.end method

.method public mayHaveAvailableCashDrawers()Z
    .locals 1

    .line 127
    invoke-virtual {p0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->hasAvailableCashDrawers()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->getAvailablePrinterCashDrawerCount()I

    move-result v0

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public openAllCashDrawers()V
    .locals 3

    .line 135
    invoke-direct {p0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->getHardwarePrintersWithCashDrawers()Ljava/util/Collection;

    move-result-object v0

    .line 137
    iget-object v1, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->backgroundThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/cashdrawer/-$$Lambda$CashDrawerTracker$RL2GzHPJOAmXFbX3rH0gygytxdk;

    invoke-direct {v2, p0, v0}, Lcom/squareup/cashdrawer/-$$Lambda$CashDrawerTracker$RL2GzHPJOAmXFbX3rH0gygytxdk;-><init>(Lcom/squareup/cashdrawer/CashDrawerTracker;Ljava/util/Collection;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public removeListener(Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;)V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method
