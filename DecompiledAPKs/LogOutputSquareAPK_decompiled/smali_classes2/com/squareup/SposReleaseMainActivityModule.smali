.class public Lcom/squareup/SposReleaseMainActivityModule;
.super Ljava/lang/Object;
.source "SposReleaseMainActivityModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/googlepay/GooglePayModule;,
        Lcom/squareup/ui/settings/ReleaseSettingsAppletModule;,
        Lcom/squareup/SposMainActivityModule;,
        Lcom/squareup/ui/root/SposReleaseAppletModule;,
        Lcom/squareup/pos/help/SposReleaseHelpAppletSettingsModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
