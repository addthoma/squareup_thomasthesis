.class public final Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;
.super Ljava/lang/Object;
.source "SposMainActivityModule_ProvideDeepLinkHandlersFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/List<",
        "Lcom/squareup/deeplinks/DeepLinkHandler;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final accountFreezeDeepLinkHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final activityDeepLinkHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityDeepLinkHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final balanceAppletDeepLinkHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletDeepLinkHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final createItemTutorialDeepLinkHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final customersDeepLinkHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final firstPaymentTutorialDeepLinksHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final helpDeepLinksHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpDeepLinksHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesDeepLinkHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final itemsDeepLinkHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsDeepLinkHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingDeepLinkHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryDeepLinkHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryDeepLinkHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubDeepLinkHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/linking/OrderHubDeepLinkHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final readerTutorialDeepLinkHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderTutorialDeepLinkHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final reportsDeepLinkHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsDeepLinkHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsAppletDeepLinkHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpDeepLinksHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderTutorialDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/linking/OrderHubDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityDeepLinkHandler;",
            ">;)V"
        }
    .end annotation

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->helpDeepLinksHandlerProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p2, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->reportsDeepLinkHandlerProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p3, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->customersDeepLinkHandlerProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p4, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->invoicesDeepLinkHandlerProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p5, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->itemsDeepLinkHandlerProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p6, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->readerTutorialDeepLinkHandlerProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p7, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->onboardingDeepLinkHandlerProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p8, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->settingsAppletDeepLinkHandlerProvider:Ljavax/inject/Provider;

    .line 84
    iput-object p9, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->balanceAppletDeepLinkHandlerProvider:Ljavax/inject/Provider;

    .line 85
    iput-object p10, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->accountFreezeDeepLinkHandlerProvider:Ljavax/inject/Provider;

    .line 86
    iput-object p11, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->firstPaymentTutorialDeepLinksHandlerProvider:Ljavax/inject/Provider;

    .line 87
    iput-object p12, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->createItemTutorialDeepLinkHandlerProvider:Ljavax/inject/Provider;

    .line 88
    iput-object p13, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->orderEntryDeepLinkHandlerProvider:Ljavax/inject/Provider;

    .line 89
    iput-object p14, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->orderHubDeepLinkHandlerProvider:Ljavax/inject/Provider;

    .line 90
    iput-object p15, p0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->activityDeepLinkHandlerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpDeepLinksHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderTutorialDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceAppletDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/linking/OrderHubDeepLinkHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityDeepLinkHandler;",
            ">;)",
            "Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;"
        }
    .end annotation

    .line 114
    new-instance v16, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v16
.end method

.method public static provideDeepLinkHandlers(Lcom/squareup/ui/help/HelpDeepLinksHandler;Lcom/squareup/reports/applet/ReportsDeepLinkHandler;Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;Lcom/squareup/ui/items/ItemsDeepLinkHandler;Lcom/squareup/ui/main/ReaderTutorialDeepLinkHandler;Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;Lcom/squareup/ui/balance/BalanceAppletDeepLinkHandler;Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;Lcom/squareup/orderentry/OrderEntryDeepLinkHandler;Lcom/squareup/ui/orderhub/linking/OrderHubDeepLinkHandler;Lcom/squareup/ui/activity/ActivityDeepLinkHandler;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/help/HelpDeepLinksHandler;",
            "Lcom/squareup/reports/applet/ReportsDeepLinkHandler;",
            "Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;",
            "Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;",
            "Lcom/squareup/ui/items/ItemsDeepLinkHandler;",
            "Lcom/squareup/ui/main/ReaderTutorialDeepLinkHandler;",
            "Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;",
            "Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;",
            "Lcom/squareup/ui/balance/BalanceAppletDeepLinkHandler;",
            "Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;",
            "Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;",
            "Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;",
            "Lcom/squareup/orderentry/OrderEntryDeepLinkHandler;",
            "Lcom/squareup/ui/orderhub/linking/OrderHubDeepLinkHandler;",
            "Lcom/squareup/ui/activity/ActivityDeepLinkHandler;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/deeplinks/DeepLinkHandler;",
            ">;"
        }
    .end annotation

    .line 131
    invoke-static/range {p0 .. p14}, Lcom/squareup/SposMainActivityModule;->provideDeepLinkHandlers(Lcom/squareup/ui/help/HelpDeepLinksHandler;Lcom/squareup/reports/applet/ReportsDeepLinkHandler;Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;Lcom/squareup/ui/items/ItemsDeepLinkHandler;Lcom/squareup/ui/main/ReaderTutorialDeepLinkHandler;Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;Lcom/squareup/ui/balance/BalanceAppletDeepLinkHandler;Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;Lcom/squareup/orderentry/OrderEntryDeepLinkHandler;Lcom/squareup/ui/orderhub/linking/OrderHubDeepLinkHandler;Lcom/squareup/ui/activity/ActivityDeepLinkHandler;)Ljava/util/List;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->get()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/deeplinks/DeepLinkHandler;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 95
    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->helpDeepLinksHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/help/HelpDeepLinksHandler;

    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->reportsDeepLinkHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/reports/applet/ReportsDeepLinkHandler;

    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->customersDeepLinkHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;

    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->invoicesDeepLinkHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;

    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->itemsDeepLinkHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/ui/items/ItemsDeepLinkHandler;

    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->readerTutorialDeepLinkHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/main/ReaderTutorialDeepLinkHandler;

    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->onboardingDeepLinkHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;

    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->settingsAppletDeepLinkHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;

    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->balanceAppletDeepLinkHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/balance/BalanceAppletDeepLinkHandler;

    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->accountFreezeDeepLinkHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;

    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->firstPaymentTutorialDeepLinksHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;

    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->createItemTutorialDeepLinkHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;

    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->orderEntryDeepLinkHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/orderentry/OrderEntryDeepLinkHandler;

    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->orderHubDeepLinkHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/ui/orderhub/linking/OrderHubDeepLinkHandler;

    iget-object v1, v0, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->activityDeepLinkHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/activity/ActivityDeepLinkHandler;

    invoke-static/range {v2 .. v16}, Lcom/squareup/SposMainActivityModule_ProvideDeepLinkHandlersFactory;->provideDeepLinkHandlers(Lcom/squareup/ui/help/HelpDeepLinksHandler;Lcom/squareup/reports/applet/ReportsDeepLinkHandler;Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;Lcom/squareup/ui/items/ItemsDeepLinkHandler;Lcom/squareup/ui/main/ReaderTutorialDeepLinkHandler;Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;Lcom/squareup/ui/balance/BalanceAppletDeepLinkHandler;Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;Lcom/squareup/orderentry/OrderEntryDeepLinkHandler;Lcom/squareup/ui/orderhub/linking/OrderHubDeepLinkHandler;Lcom/squareup/ui/activity/ActivityDeepLinkHandler;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method
