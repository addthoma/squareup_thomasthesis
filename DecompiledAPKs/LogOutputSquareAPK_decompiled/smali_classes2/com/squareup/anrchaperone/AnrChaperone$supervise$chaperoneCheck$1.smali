.class public final Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;
.super Ljava/lang/Object;
.source "AnrChaperone.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/anrchaperone/AnrChaperone;->supervise(Lkotlin/jvm/functions/Function0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"
    }
    d2 = {
        "com/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1",
        "Ljava/lang/Runnable;",
        "run",
        "",
        "anr-chaperone_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $callback:Lkotlin/jvm/functions/Function0;

.field final synthetic $chaperoneLastUpdate:Lkotlin/jvm/internal/Ref$LongRef;

.field final synthetic $lastANR:Lkotlin/jvm/internal/Ref$LongRef;

.field final synthetic $mainThreadTick:Lcom/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1;

.field final synthetic this$0:Lcom/squareup/anrchaperone/AnrChaperone;


# direct methods
.method constructor <init>(Lcom/squareup/anrchaperone/AnrChaperone;Lkotlin/jvm/internal/Ref$LongRef;Lkotlin/jvm/internal/Ref$LongRef;Lkotlin/jvm/functions/Function0;Lcom/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/internal/Ref$LongRef;",
            "Lkotlin/jvm/internal/Ref$LongRef;",
            "Lkotlin/jvm/functions/Function0;",
            "Lcom/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1;",
            ")V"
        }
    .end annotation

    .line 65
    iput-object p1, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    iput-object p2, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->$chaperoneLastUpdate:Lkotlin/jvm/internal/Ref$LongRef;

    iput-object p3, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->$lastANR:Lkotlin/jvm/internal/Ref$LongRef;

    iput-object p4, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->$callback:Lkotlin/jvm/functions/Function0;

    iput-object p5, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->$mainThreadTick:Lcom/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 67
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-static {v0}, Lcom/squareup/anrchaperone/AnrChaperone;->access$getClock$p(Lcom/squareup/anrchaperone/AnrChaperone;)Lcom/squareup/util/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v0

    .line 68
    iget-object v2, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-virtual {v2}, Lcom/squareup/anrchaperone/AnrChaperone;->getMainThreadLastUpdate()J

    move-result-wide v2

    sub-long v2, v0, v2

    .line 69
    iget-object v4, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->$chaperoneLastUpdate:Lkotlin/jvm/internal/Ref$LongRef;

    iget-wide v4, v4, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    sub-long v4, v0, v4

    .line 70
    iget-object v6, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->$chaperoneLastUpdate:Lkotlin/jvm/internal/Ref$LongRef;

    iput-wide v0, v6, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    .line 71
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-static {v0}, Lcom/squareup/anrchaperone/AnrChaperone;->access$getStopping$p(Lcom/squareup/anrchaperone/AnrChaperone;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 74
    :cond_0
    invoke-static {}, Lcom/squareup/anrchaperone/AnrChaperone;->access$Companion()Lcom/squareup/anrchaperone/AnrChaperone$Companion;

    const-wide/16 v0, 0x1388

    const-wide/16 v6, 0x3e8

    cmp-long v8, v2, v0

    if-ltz v8, :cond_3

    .line 75
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->$lastANR:Lkotlin/jvm/internal/Ref$LongRef;

    iget-wide v0, v0, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    iget-object v2, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-virtual {v2}, Lcom/squareup/anrchaperone/AnrChaperone;->getMainThreadLastUpdate()J

    move-result-wide v2

    cmp-long v8, v0, v2

    if-eqz v8, :cond_2

    const-wide/16 v0, 0x5dc

    cmp-long v2, v4, v0

    if-lez v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    .line 78
    invoke-static {}, Landroid/os/Debug;->isDebuggerConnected()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-static {v0}, Lcom/squareup/anrchaperone/AnrChaperone;->access$isVisible(Lcom/squareup/anrchaperone/AnrChaperone;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->$lastANR:Lkotlin/jvm/internal/Ref$LongRef;

    iget-object v1, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-virtual {v1}, Lcom/squareup/anrchaperone/AnrChaperone;->getMainThreadLastUpdate()J

    move-result-wide v1

    iput-wide v1, v0, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    .line 81
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-virtual {v0}, Lcom/squareup/anrchaperone/AnrChaperone;->stop()V

    .line 82
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->$callback:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 85
    :cond_2
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-static {v0}, Lcom/squareup/anrchaperone/AnrChaperone;->access$getChaperoneThread$p(Lcom/squareup/anrchaperone/AnrChaperone;)Lcom/squareup/thread/executor/SerialExecutor;

    move-result-object v0

    move-object v1, p0

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {}, Lcom/squareup/anrchaperone/AnrChaperone;->access$Companion()Lcom/squareup/anrchaperone/AnrChaperone$Companion;

    invoke-interface {v0, v1, v6, v7}, Lcom/squareup/thread/executor/SerialExecutor;->executeDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_3

    .line 87
    :cond_3
    invoke-static {}, Lcom/squareup/anrchaperone/AnrChaperone;->access$Companion()Lcom/squareup/anrchaperone/AnrChaperone$Companion;

    sub-long/2addr v0, v2

    .line 89
    invoke-static {}, Lcom/squareup/anrchaperone/AnrChaperone;->access$Companion()Lcom/squareup/anrchaperone/AnrChaperone$Companion;

    const-wide/16 v4, 0x1

    cmp-long v8, v4, v0

    if-lez v8, :cond_4

    goto :goto_1

    :cond_4
    cmp-long v4, v6, v0

    if-ltz v4, :cond_5

    goto :goto_2

    :cond_5
    :goto_1
    invoke-static {}, Lcom/squareup/anrchaperone/AnrChaperone;->access$Companion()Lcom/squareup/anrchaperone/AnrChaperone$Companion;

    move-wide v0, v6

    .line 90
    :goto_2
    invoke-static {}, Lcom/squareup/anrchaperone/AnrChaperone;->access$Companion()Lcom/squareup/anrchaperone/AnrChaperone$Companion;

    const-wide/16 v4, 0xbb8

    cmp-long v6, v2, v4

    if-lez v6, :cond_6

    .line 91
    iget-object v2, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-static {v2}, Lcom/squareup/anrchaperone/AnrChaperone;->access$getMainThread$p(Lcom/squareup/anrchaperone/AnrChaperone;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->$mainThreadTick:Lcom/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1;

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 93
    :cond_6
    iget-object v2, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$chaperoneCheck$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-static {v2}, Lcom/squareup/anrchaperone/AnrChaperone;->access$getChaperoneThread$p(Lcom/squareup/anrchaperone/AnrChaperone;)Lcom/squareup/thread/executor/SerialExecutor;

    move-result-object v2

    move-object v3, p0

    check-cast v3, Ljava/lang/Runnable;

    invoke-interface {v2, v3, v0, v1}, Lcom/squareup/thread/executor/SerialExecutor;->executeDelayed(Ljava/lang/Runnable;J)Z

    :goto_3
    return-void
.end method
