.class synthetic Lcom/squareup/calc/TaxAndDiscountAdjuster$1;
.super Ljava/lang/Object;
.source "TaxAndDiscountAdjuster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/calc/TaxAndDiscountAdjuster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$calc$constants$RoundingType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 450
    invoke-static {}, Lcom/squareup/calc/constants/RoundingType;->values()[Lcom/squareup/calc/constants/RoundingType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/calc/TaxAndDiscountAdjuster$1;->$SwitchMap$com$squareup$calc$constants$RoundingType:[I

    :try_start_0
    sget-object v0, Lcom/squareup/calc/TaxAndDiscountAdjuster$1;->$SwitchMap$com$squareup$calc$constants$RoundingType:[I

    sget-object v1, Lcom/squareup/calc/constants/RoundingType;->BANKERS:Lcom/squareup/calc/constants/RoundingType;

    invoke-virtual {v1}, Lcom/squareup/calc/constants/RoundingType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/calc/TaxAndDiscountAdjuster$1;->$SwitchMap$com$squareup$calc$constants$RoundingType:[I

    sget-object v1, Lcom/squareup/calc/constants/RoundingType;->TRUNCATE:Lcom/squareup/calc/constants/RoundingType;

    invoke-virtual {v1}, Lcom/squareup/calc/constants/RoundingType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method
