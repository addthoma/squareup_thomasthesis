.class public interface abstract Lcom/squareup/feedback/AppFeedbackScreen$Runner;
.super Ljava/lang/Object;
.source "AppFeedbackScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/feedback/AppFeedbackScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0006H&J\u0018\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH&\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/feedback/AppFeedbackScreen$Runner;",
        "",
        "appFeedbackScreenData",
        "Lrx/Observable;",
        "Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;",
        "goBackFromFeedbackScreen",
        "",
        "submitFeedbackAndRating",
        "numStars",
        "",
        "feedback",
        "",
        "feedback_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract appFeedbackScreenData()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract goBackFromFeedbackScreen()V
.end method

.method public abstract submitFeedbackAndRating(ILjava/lang/String;)V
.end method
