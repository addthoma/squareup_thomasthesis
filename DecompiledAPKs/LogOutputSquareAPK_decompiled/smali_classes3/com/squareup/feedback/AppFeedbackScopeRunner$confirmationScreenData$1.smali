.class final Lcom/squareup/feedback/AppFeedbackScopeRunner$confirmationScreenData$1;
.super Ljava/lang/Object;
.source "AppFeedbackScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/feedback/AppFeedbackScopeRunner;->confirmationScreenData()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;",
        "it",
        "",
        "apply",
        "(Ljava/lang/Boolean;)Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/feedback/AppFeedbackScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/feedback/AppFeedbackScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner$confirmationScreenData$1;->this$0:Lcom/squareup/feedback/AppFeedbackScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Boolean;)Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner$confirmationScreenData$1;->this$0:Lcom/squareup/feedback/AppFeedbackScopeRunner;

    invoke-static {v0}, Lcom/squareup/feedback/AppFeedbackScopeRunner;->access$getConfirmationScreenDataFactory$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;

    move-result-object v0

    .line 88
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    .line 89
    iget-object v1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner$confirmationScreenData$1;->this$0:Lcom/squareup/feedback/AppFeedbackScopeRunner;

    invoke-static {v1}, Lcom/squareup/feedback/AppFeedbackScopeRunner;->access$getIntentAvailabilityManager$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lcom/squareup/util/IntentAvailabilityManager;

    move-result-object v1

    .line 90
    iget-object v2, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner$confirmationScreenData$1;->this$0:Lcom/squareup/feedback/AppFeedbackScopeRunner;

    invoke-static {v2}, Lcom/squareup/feedback/AppFeedbackScopeRunner;->access$getPlayStoreIntentCreator$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lcom/squareup/feedback/PlayStoreIntentCreator;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/feedback/PlayStoreIntentCreator;->create()Landroid/content/Intent;

    move-result-object v2

    .line 89
    invoke-interface {v1, v2}, Lcom/squareup/util/IntentAvailabilityManager;->isAvailable(Landroid/content/Intent;)Z

    move-result v1

    .line 87
    invoke-virtual {v0, p1, v1}, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;->create(ZZ)Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/feedback/AppFeedbackScopeRunner$confirmationScreenData$1;->apply(Ljava/lang/Boolean;)Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method
