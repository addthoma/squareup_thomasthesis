.class public final Lcom/squareup/feedback/AppFeedbackScreen;
.super Lcom/squareup/feedback/InAppFeedbackScope;
.source "AppFeedbackScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/feedback/AppFeedbackScreen$Runner;,
        Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;,
        Lcom/squareup/feedback/AppFeedbackScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAppFeedbackScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AppFeedbackScreen.kt\ncom/squareup/feedback/AppFeedbackScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,47:1\n52#2:48\n24#3,4:49\n*E\n*S KotlinDebug\n*F\n+ 1 AppFeedbackScreen.kt\ncom/squareup/feedback/AppFeedbackScreen\n*L\n37#1:48\n42#1,4:49\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 \u00122\u00020\u00012\u00020\u00022\u00020\u0003:\u0003\u0012\u0013\u0014B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0014J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u000cH\u0016\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/feedback/AppFeedbackScreen;",
        "Lcom/squareup/feedback/InAppFeedbackScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "appFeedbackScope",
        "Lcom/squareup/feedback/AppFeedbackScope;",
        "(Lcom/squareup/feedback/AppFeedbackScope;)V",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "Companion",
        "Runner",
        "ScreenData",
        "feedback_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/feedback/AppFeedbackScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/feedback/AppFeedbackScreen$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/feedback/AppFeedbackScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/feedback/AppFeedbackScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/feedback/AppFeedbackScreen;->Companion:Lcom/squareup/feedback/AppFeedbackScreen$Companion;

    .line 49
    new-instance v0, Lcom/squareup/feedback/AppFeedbackScreen$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/feedback/AppFeedbackScreen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 52
    sput-object v0, Lcom/squareup/feedback/AppFeedbackScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/feedback/AppFeedbackScope;)V
    .locals 1

    const-string v0, "appFeedbackScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0, p1}, Lcom/squareup/feedback/InAppFeedbackScope;-><init>(Lcom/squareup/feedback/AppFeedbackScope;)V

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p0}, Lcom/squareup/feedback/AppFeedbackScreen;->getParent()Lcom/squareup/feedback/AppFeedbackScope;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v0, "view.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    const-class v0, Lcom/squareup/feedback/AppFeedbackScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 37
    check-cast p1, Lcom/squareup/feedback/AppFeedbackScope$Component;

    .line 38
    invoke-interface {p1}, Lcom/squareup/feedback/AppFeedbackScope$Component;->appFeedbackCoordinator()Lcom/squareup/feedback/AppFeedbackCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 17
    sget v0, Lcom/squareup/feedback/R$layout;->app_feedback_view:I

    return v0
.end method
