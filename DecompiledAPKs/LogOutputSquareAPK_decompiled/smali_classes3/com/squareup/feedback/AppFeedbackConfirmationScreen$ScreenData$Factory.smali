.class public final Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;
.super Ljava/lang/Object;
.source "AppFeedbackConfirmationScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;",
        "",
        "appNameFormatter",
        "Lcom/squareup/util/AppNameFormatter;",
        "(Lcom/squareup/util/AppNameFormatter;)V",
        "create",
        "Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;",
        "isMaxRating",
        "",
        "playStoreIntentAvailable",
        "feedback_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final appNameFormatter:Lcom/squareup/util/AppNameFormatter;


# direct methods
.method public constructor <init>(Lcom/squareup/util/AppNameFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "appNameFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    return-void
.end method


# virtual methods
.method public final create(ZZ)Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;
    .locals 2

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 57
    new-instance p1, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;

    .line 58
    new-instance p2, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$DisplayPlayStoreSection$Show;

    .line 59
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v1, Lcom/squareup/feedback/R$string;->rate_on_play_store_prompt:I

    invoke-interface {v0, v1}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 58
    invoke-direct {p2, v0}, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$DisplayPlayStoreSection$Show;-><init>(Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$DisplayPlayStoreSection;

    .line 57
    invoke-direct {p1, p2}, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;-><init>(Lcom/squareup/feedback/AppFeedbackConfirmationScreen$DisplayPlayStoreSection;)V

    goto :goto_0

    .line 63
    :cond_0
    new-instance p1, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;

    sget-object p2, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$DisplayPlayStoreSection$Hide;->INSTANCE:Lcom/squareup/feedback/AppFeedbackConfirmationScreen$DisplayPlayStoreSection$Hide;

    check-cast p2, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$DisplayPlayStoreSection;

    invoke-direct {p1, p2}, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;-><init>(Lcom/squareup/feedback/AppFeedbackConfirmationScreen$DisplayPlayStoreSection;)V

    :goto_0
    return-object p1
.end method
