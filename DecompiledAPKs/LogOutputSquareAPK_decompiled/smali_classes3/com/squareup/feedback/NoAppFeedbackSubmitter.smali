.class public final Lcom/squareup/feedback/NoAppFeedbackSubmitter;
.super Ljava/lang/Object;
.source "AppFeedbackSubmitter.kt"

# interfaces
.implements Lcom/squareup/feedback/AppFeedbackSubmitter;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAppFeedbackSubmitter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AppFeedbackSubmitter.kt\ncom/squareup/feedback/NoAppFeedbackSubmitter\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,34:1\n151#2,2:35\n*E\n*S KotlinDebug\n*F\n+ 1 AppFeedbackSubmitter.kt\ncom/squareup/feedback/NoAppFeedbackSubmitter\n*L\n33#1,2:35\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u001f\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0096\u0001\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/feedback/NoAppFeedbackSubmitter;",
        "Lcom/squareup/feedback/AppFeedbackSubmitter;",
        "()V",
        "submit",
        "Lrx/Observable;",
        "Lcom/squareup/feedback/AppFeedbackResponse;",
        "rating",
        "",
        "feedback",
        "",
        "feedback_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/feedback/AppFeedbackSubmitter;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 35
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 36
    const-class v2, Lcom/squareup/feedback/AppFeedbackSubmitter;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/feedback/AppFeedbackSubmitter;

    iput-object v0, p0, Lcom/squareup/feedback/NoAppFeedbackSubmitter;->$$delegate_0:Lcom/squareup/feedback/AppFeedbackSubmitter;

    return-void
.end method


# virtual methods
.method public submit(ILjava/lang/String;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/feedback/AppFeedbackResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "feedback"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/feedback/NoAppFeedbackSubmitter;->$$delegate_0:Lcom/squareup/feedback/AppFeedbackSubmitter;

    invoke-interface {v0, p1, p2}, Lcom/squareup/feedback/AppFeedbackSubmitter;->submit(ILjava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
