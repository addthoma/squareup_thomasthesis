.class Lcom/squareup/feetutorial/RatesTourScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "RatesTourScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/feetutorial/RatesTourScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/feetutorial/RatesTourView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private currentCategory:Lcom/squareup/feetutorial/RateCategory;

.field private final flow:Lflow/Flow;

.field private final rateTours:Lcom/squareup/feetutorial/RateTours;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/feetutorial/RateTours;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/util/BrowserLauncher;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 69
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->flow:Lflow/Flow;

    .line 71
    iput-object p2, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 72
    iput-object p3, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->rateTours:Lcom/squareup/feetutorial/RateTours;

    .line 73
    iput-object p4, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    .line 74
    iput-object p5, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Leaving RatesTourScreen"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->currentCategory:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v1}, Lcom/squareup/feetutorial/RateCategory;->getCloseEvent()Lcom/squareup/analytics/RegisterActionName;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public help()V
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->HELP_FEE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_help_url:I

    invoke-interface {v0, v1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(I)V

    return-void
.end method

.method public synthetic lambda$null$0$RatesTourScreen$Presenter(Lcom/squareup/feetutorial/RatesTourView;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->currentCategory:Lcom/squareup/feetutorial/RateCategory;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 84
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/feetutorial/RatePage;

    iget-object v0, v0, Lcom/squareup/feetutorial/RatePage;->category:Lcom/squareup/feetutorial/RateCategory;

    iput-object v0, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->currentCategory:Lcom/squareup/feetutorial/RateCategory;

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->currentCategory:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/feetutorial/RatesTourView;->setItems(Ljava/util/List;Lcom/squareup/feetutorial/RateCategory;)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$RatesTourScreen$Presenter(Lcom/squareup/feetutorial/RatesTourView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->rateTours:Lcom/squareup/feetutorial/RateTours;

    invoke-virtual {v0}, Lcom/squareup/feetutorial/RateTours;->pages()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/feetutorial/-$$Lambda$RatesTourScreen$Presenter$lG83KETdImVt0j-tmV44KtqTS-o;

    invoke-direct {v1, p0, p1}, Lcom/squareup/feetutorial/-$$Lambda$RatesTourScreen$Presenter$lG83KETdImVt0j-tmV44KtqTS-o;-><init>(Lcom/squareup/feetutorial/RatesTourScreen$Presenter;Lcom/squareup/feetutorial/RatesTourView;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 78
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Shown RatesTourScreen"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/feetutorial/RatesTourView;

    .line 81
    new-instance v0, Lcom/squareup/feetutorial/-$$Lambda$RatesTourScreen$Presenter$yEYQ-4NCYdJFQ1-Z6OxxTlI-wdk;

    invoke-direct {v0, p0, p1}, Lcom/squareup/feetutorial/-$$Lambda$RatesTourScreen$Presenter$yEYQ-4NCYdJFQ1-Z6OxxTlI-wdk;-><init>(Lcom/squareup/feetutorial/RatesTourScreen$Presenter;Lcom/squareup/feetutorial/RatesTourView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public setCurrentCategory(Lcom/squareup/feetutorial/RateCategory;)V
    .locals 1

    .line 102
    iput-object p1, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->currentCategory:Lcom/squareup/feetutorial/RateCategory;

    .line 103
    iget-object p1, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->currentCategory:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v0}, Lcom/squareup/feetutorial/RateCategory;->getViewEvent()Lcom/squareup/analytics/RegisterViewName;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    return-void
.end method
