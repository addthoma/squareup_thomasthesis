.class Lcom/squareup/connectivity/RealConnectivityMonitor$1;
.super Landroid/content/BroadcastReceiver;
.source "RealConnectivityMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/connectivity/RealConnectivityMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/connectivity/RealConnectivityMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/connectivity/RealConnectivityMonitor;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/squareup/connectivity/RealConnectivityMonitor$1;->this$0:Lcom/squareup/connectivity/RealConnectivityMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .line 37
    iget-object p1, p0, Lcom/squareup/connectivity/RealConnectivityMonitor$1;->this$0:Lcom/squareup/connectivity/RealConnectivityMonitor;

    invoke-static {p1}, Lcom/squareup/connectivity/RealConnectivityMonitor;->access$100(Lcom/squareup/connectivity/RealConnectivityMonitor;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/connectivity/RealConnectivityMonitor$1;->this$0:Lcom/squareup/connectivity/RealConnectivityMonitor;

    invoke-static {p2}, Lcom/squareup/connectivity/RealConnectivityMonitor;->access$000(Lcom/squareup/connectivity/RealConnectivityMonitor;)Lcom/squareup/connectivity/InternetState;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 38
    iget-object p1, p0, Lcom/squareup/connectivity/RealConnectivityMonitor$1;->this$0:Lcom/squareup/connectivity/RealConnectivityMonitor;

    invoke-static {p1}, Lcom/squareup/connectivity/RealConnectivityMonitor;->access$200(Lcom/squareup/connectivity/RealConnectivityMonitor;)Lcom/squareup/internet/InternetStatusMonitor;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/internet/InternetStatusMonitor;->logNetworkConnectionStatus()V

    return-void
.end method
