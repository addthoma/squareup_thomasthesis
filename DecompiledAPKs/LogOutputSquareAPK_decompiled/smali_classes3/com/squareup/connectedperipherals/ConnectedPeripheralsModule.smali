.class public abstract Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule;
.super Ljava/lang/Object;
.source "ConnectedPeripheralsModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\'\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule;",
        "",
        "()V",
        "Companion",
        "connected-peripherals_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule;->Companion:Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideConnectedPeripheralsLoggingProviders(Lcom/squareup/connectedperipherals/ConnectedCardReadersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;)Ljava/util/Set;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/connectedperipherals/ConnectedCardReadersLoggingProvider;",
            "Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;",
            "Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;",
            "Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule;->Companion:Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule$Companion;->provideConnectedPeripheralsLoggingProviders(Lcom/squareup/connectedperipherals/ConnectedCardReadersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method
