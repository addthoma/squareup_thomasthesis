.class public final Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;
.super Ljava/lang/Object;
.source "ConnectedCashDrawersLoggingProvider.kt"

# interfaces
.implements Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nConnectedCashDrawersLoggingProvider.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ConnectedCashDrawersLoggingProvider.kt\ncom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,20:1\n1360#2:21\n1429#2,3:22\n*E\n*S KotlinDebug\n*F\n+ 1 ConnectedCashDrawersLoggingProvider.kt\ncom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider\n*L\n13#1:21\n13#1,3:22\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
        "cashDrawerTracker",
        "Lcom/squareup/cashdrawer/CashDrawerTracker;",
        "(Lcom/squareup/cashdrawer/CashDrawerTracker;)V",
        "getCashDrawerTracker",
        "()Lcom/squareup/cashdrawer/CashDrawerTracker;",
        "getConnectedPeripherals",
        "",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralData;",
        "connected-peripherals_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;


# direct methods
.method public constructor <init>(Lcom/squareup/cashdrawer/CashDrawerTracker;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cashDrawerTracker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    return-void
.end method


# virtual methods
.method public final getCashDrawerTracker()Lcom/squareup/cashdrawer/CashDrawerTracker;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    return-object v0
.end method

.method public getConnectedPeripherals()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralData;",
            ">;"
        }
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->getAvailableCashDrawers()Ljava/util/List;

    move-result-object v0

    const-string v1, "cashDrawerTracker.availableCashDrawers"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 21
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 22
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 23
    check-cast v2, Lcom/squareup/cashdrawer/CashDrawer;

    .line 14
    new-instance v11, Lcom/squareup/connectedperipherals/ConnectedPeripheralData;

    const/4 v4, 0x0

    .line 15
    sget-object v3, Lcom/squareup/connectedperipherals/DeviceType;->CashDrawer:Lcom/squareup/connectedperipherals/DeviceType;

    invoke-virtual {v3}, Lcom/squareup/connectedperipherals/DeviceType;->name()Ljava/lang/String;

    move-result-object v5

    const-string v3, "it"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2}, Lcom/squareup/cashdrawer/CashDrawer;->getName()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/squareup/connectedperipherals/ConnectedPeripheralType;->USB:Lcom/squareup/connectedperipherals/ConnectedPeripheralType;

    const/4 v8, 0x0

    const/16 v9, 0x11

    const/4 v10, 0x0

    move-object v3, v11

    .line 14
    invoke-direct/range {v3 .. v10}, Lcom/squareup/connectedperipherals/ConnectedPeripheralData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/connectedperipherals/ConnectedPeripheralType;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 16
    invoke-interface {v1, v11}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 24
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method
