.class public final Lcom/squareup/hardware/UsbDetachedReceiver_MembersInjector;
.super Ljava/lang/Object;
.source "UsbDetachedReceiver_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/hardware/UsbDetachedReceiver;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final usbDiscovererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/hardware/UsbDetachedReceiver_MembersInjector;->usbDiscovererProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/hardware/UsbDetachedReceiver_MembersInjector;->eventSinkProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/hardware/UsbDetachedReceiver;",
            ">;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/hardware/UsbDetachedReceiver_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/hardware/UsbDetachedReceiver_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectEventSink(Lcom/squareup/hardware/UsbDetachedReceiver;Lcom/squareup/badbus/BadEventSink;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/hardware/UsbDetachedReceiver;->eventSink:Lcom/squareup/badbus/BadEventSink;

    return-void
.end method

.method public static injectUsbDiscoverer(Lcom/squareup/hardware/UsbDetachedReceiver;Lcom/squareup/usb/UsbDiscoverer;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/hardware/UsbDetachedReceiver;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/hardware/UsbDetachedReceiver;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/hardware/UsbDetachedReceiver_MembersInjector;->usbDiscovererProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/usb/UsbDiscoverer;

    invoke-static {p1, v0}, Lcom/squareup/hardware/UsbDetachedReceiver_MembersInjector;->injectUsbDiscoverer(Lcom/squareup/hardware/UsbDetachedReceiver;Lcom/squareup/usb/UsbDiscoverer;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/hardware/UsbDetachedReceiver_MembersInjector;->eventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/badbus/BadEventSink;

    invoke-static {p1, v0}, Lcom/squareup/hardware/UsbDetachedReceiver_MembersInjector;->injectEventSink(Lcom/squareup/hardware/UsbDetachedReceiver;Lcom/squareup/badbus/BadEventSink;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/hardware/UsbDetachedReceiver;

    invoke-virtual {p0, p1}, Lcom/squareup/hardware/UsbDetachedReceiver_MembersInjector;->injectMembers(Lcom/squareup/hardware/UsbDetachedReceiver;)V

    return-void
.end method
