.class final Lcom/squareup/hardware/usb/UsbManager$1;
.super Ljava/lang/Object;
.source "UsbManager.java"

# interfaces
.implements Lcom/squareup/hardware/usb/UsbManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/hardware/usb/UsbManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDeviceList()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Landroid/hardware/usb/UsbDevice;",
            ">;"
        }
    .end annotation

    .line 89
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    return-object v0
.end method

.method public hasPermission(Landroid/hardware/usb/UsbDevice;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public requestPermission(Landroid/hardware/usb/UsbDevice;Landroid/app/PendingIntent;)V
    .locals 0

    return-void
.end method
