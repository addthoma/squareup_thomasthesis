.class public final Lcom/squareup/ms/NoopMinesweeperModule_ProvideMinesweeperFactory;
.super Ljava/lang/Object;
.source "NoopMinesweeperModule_ProvideMinesweeperFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ms/NoopMinesweeperModule_ProvideMinesweeperFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ms/Minesweeper;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ms/NoopMinesweeperModule_ProvideMinesweeperFactory;
    .locals 1

    .line 22
    invoke-static {}, Lcom/squareup/ms/NoopMinesweeperModule_ProvideMinesweeperFactory$InstanceHolder;->access$000()Lcom/squareup/ms/NoopMinesweeperModule_ProvideMinesweeperFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideMinesweeper()Lcom/squareup/ms/Minesweeper;
    .locals 2

    .line 26
    invoke-static {}, Lcom/squareup/ms/NoopMinesweeperModule;->provideMinesweeper()Lcom/squareup/ms/Minesweeper;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ms/Minesweeper;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ms/Minesweeper;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/ms/NoopMinesweeperModule_ProvideMinesweeperFactory;->provideMinesweeper()Lcom/squareup/ms/Minesweeper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ms/NoopMinesweeperModule_ProvideMinesweeperFactory;->get()Lcom/squareup/ms/Minesweeper;

    move-result-object v0

    return-object v0
.end method
