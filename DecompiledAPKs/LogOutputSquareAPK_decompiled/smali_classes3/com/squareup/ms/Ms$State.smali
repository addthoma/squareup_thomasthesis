.class final enum Lcom/squareup/ms/Ms$State;
.super Ljava/lang/Enum;
.source "Ms.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ms/Ms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ms/Ms$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ms/Ms$State;

.field public static final enum CALLED_BACK:Lcom/squareup/ms/Ms$State;

.field public static final enum NONE:Lcom/squareup/ms/Ms$State;

.field public static final enum STARTING:Lcom/squareup/ms/Ms$State;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 32
    new-instance v0, Lcom/squareup/ms/Ms$State;

    const/4 v1, 0x0

    const-string v2, "NONE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ms/Ms$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ms/Ms$State;->NONE:Lcom/squareup/ms/Ms$State;

    new-instance v0, Lcom/squareup/ms/Ms$State;

    const/4 v2, 0x1

    const-string v3, "STARTING"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ms/Ms$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ms/Ms$State;->STARTING:Lcom/squareup/ms/Ms$State;

    new-instance v0, Lcom/squareup/ms/Ms$State;

    const/4 v3, 0x2

    const-string v4, "CALLED_BACK"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ms/Ms$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ms/Ms$State;->CALLED_BACK:Lcom/squareup/ms/Ms$State;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ms/Ms$State;

    .line 31
    sget-object v4, Lcom/squareup/ms/Ms$State;->NONE:Lcom/squareup/ms/Ms$State;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ms/Ms$State;->STARTING:Lcom/squareup/ms/Ms$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ms/Ms$State;->CALLED_BACK:Lcom/squareup/ms/Ms$State;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ms/Ms$State;->$VALUES:[Lcom/squareup/ms/Ms$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ms/Ms$State;
    .locals 1

    .line 31
    const-class v0, Lcom/squareup/ms/Ms$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ms/Ms$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ms/Ms$State;
    .locals 1

    .line 31
    sget-object v0, Lcom/squareup/ms/Ms$State;->$VALUES:[Lcom/squareup/ms/Ms$State;

    invoke-virtual {v0}, [Lcom/squareup/ms/Ms$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ms/Ms$State;

    return-object v0
.end method
