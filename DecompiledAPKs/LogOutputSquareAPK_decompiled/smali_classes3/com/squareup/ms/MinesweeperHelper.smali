.class public Lcom/squareup/ms/MinesweeperHelper;
.super Ljava/lang/Object;
.source "MinesweeperHelper.java"

# interfaces
.implements Lcom/squareup/ms/Minesweeper$DataListener;
.implements Lcom/squareup/ms/Minesweeper$MinesweeperLogger;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final clock:Lcom/squareup/util/Clock;

.field private final minesweeperService:Lcom/squareup/ms/MinesweeperService;

.field private final minesweeperTicket:Lcom/squareup/ms/MinesweeperTicket;

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/ms/MinesweeperService;Lcom/squareup/ms/MinesweeperTicket;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ms/MinesweeperHelper;->minesweeperService:Lcom/squareup/ms/MinesweeperService;

    .line 33
    iput-object p2, p0, Lcom/squareup/ms/MinesweeperHelper;->minesweeperTicket:Lcom/squareup/ms/MinesweeperTicket;

    .line 34
    iput-object p3, p0, Lcom/squareup/ms/MinesweeperHelper;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 35
    iput-object p4, p0, Lcom/squareup/ms/MinesweeperHelper;->analytics:Lcom/squareup/analytics/Analytics;

    .line 36
    iput-object p5, p0, Lcom/squareup/ms/MinesweeperHelper;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method static synthetic lambda$null$1(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "PassData"

    .line 81
    invoke-static {p0, v0}, Lcom/squareup/receiving/SuccessOrFailureLogger;->logFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$0$MinesweeperHelper(Lcom/squareup/ms/Minesweeper;Lcom/squareup/protos/client/flipper/GetTicketResponse;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 61
    iget-object v0, p2, Lcom/squareup/protos/client/flipper/GetTicketResponse;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p2, Lcom/squareup/protos/client/flipper/GetTicketResponse;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    iget-object v0, v0, Lcom/squareup/protos/client/flipper/SealedTicket;->expiration:Ljava/lang/Long;

    .line 66
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p2, Lcom/squareup/protos/client/flipper/GetTicketResponse;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    iget-object v2, v2, Lcom/squareup/protos/client/flipper/SealedTicket;->creation:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 67
    iget-object v2, p0, Lcom/squareup/ms/MinesweeperHelper;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v2}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v2

    .line 68
    new-instance v4, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->creation(Ljava/lang/Long;)Lcom/squareup/protos/client/flipper/SealedTicket$Builder;

    move-result-object v4

    add-long/2addr v2, v0

    .line 69
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->expiration(Ljava/lang/Long;)Lcom/squareup/protos/client/flipper/SealedTicket$Builder;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/flipper/GetTicketResponse;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    iget-object v1, v1, Lcom/squareup/protos/client/flipper/SealedTicket;->ciphertext:Lokio/ByteString;

    .line 70
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->ciphertext(Lokio/ByteString;)Lcom/squareup/protos/client/flipper/SealedTicket$Builder;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/flipper/GetTicketResponse;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    iget-object v1, v1, Lcom/squareup/protos/client/flipper/SealedTicket;->key_index:Ljava/lang/Long;

    .line 71
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->key_index(Ljava/lang/Long;)Lcom/squareup/protos/client/flipper/SealedTicket$Builder;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->build()Lcom/squareup/protos/client/flipper/SealedTicket;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/squareup/ms/MinesweeperHelper;->minesweeperTicket:Lcom/squareup/ms/MinesweeperTicket;

    invoke-interface {v1, v0}, Lcom/squareup/ms/MinesweeperTicket;->setTicket(Lcom/squareup/protos/client/flipper/SealedTicket;)V

    .line 77
    :cond_0
    iget-object v0, p2, Lcom/squareup/protos/client/flipper/GetTicketResponse;->ms_frame:Lokio/ByteString;

    if-eqz v0, :cond_1

    .line 78
    iget-object p2, p2, Lcom/squareup/protos/client/flipper/GetTicketResponse;->ms_frame:Lokio/ByteString;

    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/ms/Minesweeper;->passDataToMinesweeper([B)V

    :cond_1
    return-void
.end method

.method public synthetic lambda$passDataToServer$2$MinesweeperHelper(Lcom/squareup/ms/Minesweeper;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 59
    new-instance v0, Lcom/squareup/ms/-$$Lambda$MinesweeperHelper$VUwbxUV5M-LDjgMfQG19FpnZGj8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ms/-$$Lambda$MinesweeperHelper$VUwbxUV5M-LDjgMfQG19FpnZGj8;-><init>(Lcom/squareup/ms/MinesweeperHelper;Lcom/squareup/ms/Minesweeper;)V

    sget-object p1, Lcom/squareup/ms/-$$Lambda$MinesweeperHelper$QJFTQnwri5OZoBcKcJ29kyjAg2o;->INSTANCE:Lcom/squareup/ms/-$$Lambda$MinesweeperHelper$QJFTQnwri5OZoBcKcJ29kyjAg2o;

    invoke-virtual {p2, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public logMinesweeperEvent(Ljava/lang/String;)V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/ms/MinesweeperHelper;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->MINESWEEPER:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void
.end method

.method public onMinesweeperFailureToInitialize(Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;Ljava/lang/Throwable;)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 89
    invoke-static {p2}, Lcom/squareup/util/Logs;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const-string p1, "Minesweeper error: %s\n%s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 90
    iget-object v0, p0, Lcom/squareup/ms/MinesweeperHelper;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->MINESWEEPER:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 91
    invoke-static {p2, p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-void
.end method

.method public passDataToServer([BLcom/squareup/ms/Minesweeper;)V
    .locals 2

    .line 40
    new-instance v0, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;-><init>()V

    .line 42
    invoke-static {p1}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->ms_frame(Lokio/ByteString;)Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;

    .line 43
    iget-object p1, p0, Lcom/squareup/ms/MinesweeperHelper;->minesweeperTicket:Lcom/squareup/ms/MinesweeperTicket;

    invoke-interface {p1}, Lcom/squareup/ms/MinesweeperTicket;->getTicket()Lcom/squareup/protos/client/flipper/SealedTicket;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 45
    iget-object v1, p1, Lcom/squareup/protos/client/flipper/SealedTicket;->ciphertext:Lokio/ByteString;

    if-nez v1, :cond_0

    .line 50
    iget-object p1, p0, Lcom/squareup/ms/MinesweeperHelper;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->FLIPPER_INVALID_SAVED_TICKET:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {p1, v1}, Lcom/squareup/analytics/Analytics;->logError(Lcom/squareup/analytics/RegisterErrorName;)V

    goto :goto_0

    .line 52
    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->current_ticket(Lcom/squareup/protos/client/flipper/SealedTicket;)Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;

    .line 57
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/ms/MinesweeperHelper;->minesweeperService:Lcom/squareup/ms/MinesweeperService;

    invoke-virtual {v0}, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->build()Lcom/squareup/protos/client/flipper/GetTicketRequest;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ms/MinesweeperService;->sendMinesweeperFrame(Lcom/squareup/protos/client/flipper/GetTicketRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 58
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ms/-$$Lambda$MinesweeperHelper$ieKU5cJEAcDTH_JX28iEoGVxTF8;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ms/-$$Lambda$MinesweeperHelper$ieKU5cJEAcDTH_JX28iEoGVxTF8;-><init>(Lcom/squareup/ms/MinesweeperHelper;Lcom/squareup/ms/Minesweeper;)V

    .line 59
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
