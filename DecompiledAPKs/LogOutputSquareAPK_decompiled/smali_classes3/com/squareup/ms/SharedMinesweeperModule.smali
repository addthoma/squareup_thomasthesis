.class public final Lcom/squareup/ms/SharedMinesweeperModule;
.super Ljava/lang/Object;
.source "SharedMinesweeperModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0008\u0010\u0007\u001a\u00020\u0008H\u0007J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u001e\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010H\u0007J \u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00082\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\nH\u0007J\u0008\u0010\u0018\u001a\u00020\u0011H\u0007\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/ms/SharedMinesweeperModule;",
        "",
        "()V",
        "provideDataListener",
        "Lcom/squareup/ms/Minesweeper$DataListener;",
        "minesweeperHelper",
        "Lcom/squareup/ms/MinesweeperHelper;",
        "provideMinesweeperExecutorService",
        "Lcom/squareup/ms/MinesweeperExecutorService;",
        "provideMinesweeperLogger",
        "Lcom/squareup/ms/Minesweeper$MinesweeperLogger;",
        "provideMinesweeperTicket",
        "Lcom/squareup/ms/MinesweeperTicket;",
        "context",
        "Landroid/app/Application;",
        "minesweeperProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/ms/Minesweeper;",
        "provideMs",
        "Lcom/squareup/ms/Ms;",
        "executorService",
        "crashnado",
        "Lcom/squareup/crashnado/Crashnado;",
        "minesweeperLogger",
        "provideNoopMinesweeper",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ms/SharedMinesweeperModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/ms/SharedMinesweeperModule;

    invoke-direct {v0}, Lcom/squareup/ms/SharedMinesweeperModule;-><init>()V

    sput-object v0, Lcom/squareup/ms/SharedMinesweeperModule;->INSTANCE:Lcom/squareup/ms/SharedMinesweeperModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideDataListener(Lcom/squareup/ms/MinesweeperHelper;)Lcom/squareup/ms/Minesweeper$DataListener;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "minesweeperHelper"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    check-cast p0, Lcom/squareup/ms/Minesweeper$DataListener;

    return-object p0
.end method

.method public static final provideMinesweeperExecutorService()Lcom/squareup/ms/MinesweeperExecutorService;
    .locals 3
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ms/MinesweeperExecutorService;

    .line 33
    sget-object v1, Lcom/squareup/ms/SharedMinesweeperModule$provideMinesweeperExecutorService$1;->INSTANCE:Lcom/squareup/ms/SharedMinesweeperModule$provideMinesweeperExecutorService$1;

    check-cast v1, Ljava/util/concurrent/ThreadFactory;

    invoke-static {v1}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    const-string v2, "Executors.newSingleThrea\u2026er\"\n          )\n        }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {v0, v1}, Lcom/squareup/ms/MinesweeperExecutorService;-><init>(Ljava/util/concurrent/ExecutorService;)V

    return-object v0
.end method

.method public static final provideMinesweeperLogger(Lcom/squareup/ms/MinesweeperHelper;)Lcom/squareup/ms/Minesweeper$MinesweeperLogger;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "minesweeperHelper"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    check-cast p0, Lcom/squareup/ms/Minesweeper$MinesweeperLogger;

    return-object p0
.end method

.method public static final provideMinesweeperTicket(Landroid/app/Application;Ljavax/inject/Provider;)Lcom/squareup/ms/MinesweeperTicket;
    .locals 1
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;)",
            "Lcom/squareup/ms/MinesweeperTicket;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "minesweeperProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    new-instance v0, Lcom/squareup/ms/RealMinesweeperTicket;

    invoke-virtual {p0}, Landroid/app/Application;->getFilesDir()Ljava/io/File;

    move-result-object p0

    invoke-direct {v0, p0, p1}, Lcom/squareup/ms/RealMinesweeperTicket;-><init>(Ljava/io/File;Ljavax/inject/Provider;)V

    check-cast v0, Lcom/squareup/ms/MinesweeperTicket;

    return-object v0
.end method

.method public static final provideMs(Lcom/squareup/ms/MinesweeperExecutorService;Lcom/squareup/crashnado/Crashnado;Lcom/squareup/ms/Minesweeper$MinesweeperLogger;)Lcom/squareup/ms/Ms;
    .locals 7
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "executorService"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "crashnado"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "minesweeperLogger"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/squareup/ms/Ms;

    .line 49
    move-object v3, p0

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p0

    invoke-direct {v4, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 50
    new-instance v6, Lcom/squareup/ms/Ms$MsNativeMethodDelegate;

    invoke-direct {v6}, Lcom/squareup/ms/Ms$MsNativeMethodDelegate;-><init>()V

    move-object v1, v0

    move-object v2, p1

    move-object v5, p2

    .line 48
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ms/Ms;-><init>(Lcom/squareup/crashnado/Crashnado;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;Lcom/squareup/ms/Minesweeper$MinesweeperLogger;Lcom/squareup/ms/Ms$MsNativeMethodDelegate;)V

    return-object v0
.end method

.method public static final provideNoopMinesweeper()Lcom/squareup/ms/Minesweeper;
    .locals 1
    .annotation runtime Lcom/squareup/ms/Noop;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 68
    sget-object v0, Lcom/squareup/ms/NoopMinesweeper;->INSTANCE:Lcom/squareup/ms/NoopMinesweeper;

    check-cast v0, Lcom/squareup/ms/Minesweeper;

    return-object v0
.end method
