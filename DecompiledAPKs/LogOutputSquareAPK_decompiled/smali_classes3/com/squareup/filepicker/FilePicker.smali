.class public interface abstract Lcom/squareup/filepicker/FilePicker;
.super Ljava/lang/Object;
.source "FilePicker.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J!\u0010\u0004\u001a\u00020\u00052\u0012\u0010\u0006\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00080\u0007\"\u00020\u0008H&\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH&\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/filepicker/FilePicker;",
        "",
        "isAvailable",
        "",
        "launch",
        "",
        "mimeTypes",
        "",
        "",
        "([Ljava/lang/String;)V",
        "results",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/filepicker/FilePickerResult;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract isAvailable()Z
.end method

.method public varargs abstract launch([Ljava/lang/String;)V
.end method

.method public abstract results()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/filepicker/FilePickerResult;",
            ">;"
        }
    .end annotation
.end method
