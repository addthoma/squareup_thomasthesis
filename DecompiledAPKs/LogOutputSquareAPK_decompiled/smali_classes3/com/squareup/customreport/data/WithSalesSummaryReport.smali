.class public final Lcom/squareup/customreport/data/WithSalesSummaryReport;
.super Lcom/squareup/customreport/data/SalesSummaryReport;
.source "SalesSummaryReport.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/customreport/data/WithSalesSummaryReport$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\n\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u001f\u0010\u000b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\t\u0010\u000c\u001a\u00020\rH\u00d6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\rH\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\u0019\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\rH\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/customreport/data/WithSalesSummaryReport;",
        "Lcom/squareup/customreport/data/SalesSummaryReport;",
        "salesSummary",
        "Lcom/squareup/customreport/data/SalesSummary;",
        "comparisonSalesSummary",
        "(Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/customreport/data/SalesSummary;)V",
        "getComparisonSalesSummary",
        "()Lcom/squareup/customreport/data/SalesSummary;",
        "getSalesSummary",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final comparisonSalesSummary:Lcom/squareup/customreport/data/SalesSummary;

.field private final salesSummary:Lcom/squareup/customreport/data/SalesSummary;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/customreport/data/WithSalesSummaryReport$Creator;

    invoke-direct {v0}, Lcom/squareup/customreport/data/WithSalesSummaryReport$Creator;-><init>()V

    sput-object v0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/customreport/data/SalesSummary;)V
    .locals 1

    const-string v0, "salesSummary"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, v0}, Lcom/squareup/customreport/data/SalesSummaryReport;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->salesSummary:Lcom/squareup/customreport/data/SalesSummary;

    iput-object p2, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->comparisonSalesSummary:Lcom/squareup/customreport/data/SalesSummary;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/customreport/data/SalesSummary;ILjava/lang/Object;)Lcom/squareup/customreport/data/WithSalesSummaryReport;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->salesSummary:Lcom/squareup/customreport/data/SalesSummary;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->comparisonSalesSummary:Lcom/squareup/customreport/data/SalesSummary;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/customreport/data/WithSalesSummaryReport;->copy(Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/customreport/data/SalesSummary;)Lcom/squareup/customreport/data/WithSalesSummaryReport;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/customreport/data/SalesSummary;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->salesSummary:Lcom/squareup/customreport/data/SalesSummary;

    return-object v0
.end method

.method public final component2()Lcom/squareup/customreport/data/SalesSummary;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->comparisonSalesSummary:Lcom/squareup/customreport/data/SalesSummary;

    return-object v0
.end method

.method public final copy(Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/customreport/data/SalesSummary;)Lcom/squareup/customreport/data/WithSalesSummaryReport;
    .locals 1

    const-string v0, "salesSummary"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/customreport/data/WithSalesSummaryReport;

    invoke-direct {v0, p1, p2}, Lcom/squareup/customreport/data/WithSalesSummaryReport;-><init>(Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/customreport/data/SalesSummary;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/customreport/data/WithSalesSummaryReport;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/customreport/data/WithSalesSummaryReport;

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->salesSummary:Lcom/squareup/customreport/data/SalesSummary;

    iget-object v1, p1, Lcom/squareup/customreport/data/WithSalesSummaryReport;->salesSummary:Lcom/squareup/customreport/data/SalesSummary;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->comparisonSalesSummary:Lcom/squareup/customreport/data/SalesSummary;

    iget-object p1, p1, Lcom/squareup/customreport/data/WithSalesSummaryReport;->comparisonSalesSummary:Lcom/squareup/customreport/data/SalesSummary;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getComparisonSalesSummary()Lcom/squareup/customreport/data/SalesSummary;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->comparisonSalesSummary:Lcom/squareup/customreport/data/SalesSummary;

    return-object v0
.end method

.method public final getSalesSummary()Lcom/squareup/customreport/data/SalesSummary;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->salesSummary:Lcom/squareup/customreport/data/SalesSummary;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->salesSummary:Lcom/squareup/customreport/data/SalesSummary;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->comparisonSalesSummary:Lcom/squareup/customreport/data/SalesSummary;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WithSalesSummaryReport(salesSummary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->salesSummary:Lcom/squareup/customreport/data/SalesSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", comparisonSalesSummary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->comparisonSalesSummary:Lcom/squareup/customreport/data/SalesSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->salesSummary:Lcom/squareup/customreport/data/SalesSummary;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object p2, p0, Lcom/squareup/customreport/data/WithSalesSummaryReport;->comparisonSalesSummary:Lcom/squareup/customreport/data/SalesSummary;

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    :goto_0
    return-void
.end method
