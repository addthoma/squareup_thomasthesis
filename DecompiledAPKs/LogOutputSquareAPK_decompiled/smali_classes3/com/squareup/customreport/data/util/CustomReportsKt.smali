.class public final Lcom/squareup/customreport/data/util/CustomReportsKt;
.super Ljava/lang/Object;
.source "CustomReports.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomReports.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomReports.kt\ncom/squareup/customreport/data/util/CustomReportsKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,143:1\n1360#2:144\n1429#2,3:145\n959#2:148\n1360#2:149\n1429#2,3:150\n959#2:153\n*E\n*S KotlinDebug\n*F\n+ 1 CustomReports.kt\ncom/squareup/customreport/data/util/CustomReportsKt\n*L\n69#1:144\n69#1,3:145\n70#1:148\n82#1:149\n82#1,3:150\n83#1:153\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\n\u0010\u0005\u001a\u00020\u0006*\u00020\u0002\u001a\n\u0010\u0007\u001a\u00020\u0008*\u00020\u0002\u001a\n\u0010\t\u001a\u00020\u0006*\u00020\u0002\u001a\u000c\u0010\n\u001a\u00020\u0006*\u00020\u0002H\u0000\u001a\u0012\u0010\u000b\u001a\u00020\u000c*\u00020\u00022\u0006\u0010\r\u001a\u00020\u000e\u001a\u000c\u0010\u000f\u001a\u00020\u0010*\u00020\u0011H\u0002\u001a\u0018\u0010\u000f\u001a\u00020\u0012*\u0004\u0018\u00010\u00022\n\u0008\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0002\u00a8\u0006\u0014"
    }
    d2 = {
        "toChartedSale",
        "Lcom/squareup/customreport/data/ChartedSale;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
        "zoneId",
        "Lorg/threeten/bp/ZoneId;",
        "toSalesCategory",
        "Lcom/squareup/customreport/data/SalesItem;",
        "toSalesDiscount",
        "Lcom/squareup/customreport/data/SalesDiscount;",
        "toSalesItem",
        "toSalesItemVariation",
        "toSalesPaymentMethod",
        "Lcom/squareup/customreport/data/SalesPaymentMethod;",
        "totalCollectedMoney",
        "Lcom/squareup/protos/common/Money;",
        "toSalesSummary",
        "Lcom/squareup/customreport/data/SalesSummary;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;",
        "Lcom/squareup/customreport/data/SalesSummaryReport;",
        "comparisonReport",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toChartedSale(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;Lorg/threeten/bp/ZoneId;)Lcom/squareup/customreport/data/ChartedSale;
    .locals 4

    const-string v0, "$this$toChartedSale"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "zoneId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    if-eqz v0, :cond_1

    sget-object v1, Lcom/squareup/customreport/data/util/CustomReportsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    .line 130
    :cond_0
    new-instance v0, Lcom/squareup/customreport/data/ChartedSale;

    .line 133
    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_completed_at_ms:Ljava/lang/Long;

    const-string v3, "group_by_value.transaction_completed_at_ms"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lorg/threeten/bp/Instant;->ofEpochMilli(J)Lorg/threeten/bp/Instant;

    move-result-object v2

    .line 134
    invoke-virtual {v2, p1}, Lorg/threeten/bp/Instant;->atZone(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object p1

    .line 135
    invoke-virtual {p1}, Lorg/threeten/bp/ZonedDateTime;->toLocalDateTime()Lorg/threeten/bp/LocalDateTime;

    move-result-object p1

    const-string v2, "Instant\n            // [\u2026       .toLocalDateTime()"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 136
    invoke-static {p0, v2, v1, v2}, Lcom/squareup/customreport/data/util/CustomReportsKt;->toSalesSummary$default(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;ILjava/lang/Object;)Lcom/squareup/customreport/data/SalesSummaryReport;

    move-result-object p0

    .line 130
    invoke-direct {v0, p1, p0}, Lcom/squareup/customreport/data/ChartedSale;-><init>(Lorg/threeten/bp/LocalDateTime;Lcom/squareup/customreport/data/SalesSummaryReport;)V

    return-object v0

    .line 138
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot create ChartedSale for given GroupByType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 138
    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final toSalesCategory(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Lcom/squareup/customreport/data/SalesItem;
    .locals 7

    const-string v0, "$this$toSalesCategory"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    iget-object v2, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const-string v0, "group_by_value.item_cate\u2026.name_or_translation_type"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    const-string v0, "aggregate.sales.item_gross_sales_money"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_count:Ljava/lang/String;

    const-string v1, "aggregate.sales.item_count"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 70
    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    if-eqz p0, :cond_1

    check-cast p0, Ljava/lang/Iterable;

    .line 144
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 145
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 146
    check-cast v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    const-string v6, "it"

    .line 69
    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/customreport/data/util/CustomReportsKt;->toSalesItem(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Lcom/squareup/customreport/data/SalesItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 147
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 148
    new-instance p0, Lcom/squareup/customreport/data/util/CustomReportsKt$toSalesCategory$$inlined$sortedByDescending$1;

    invoke-direct {p0}, Lcom/squareup/customreport/data/util/CustomReportsKt$toSalesCategory$$inlined$sortedByDescending$1;-><init>()V

    check-cast p0, Ljava/util/Comparator;

    invoke-static {v0, p0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p0

    if-eqz p0, :cond_1

    goto :goto_1

    .line 71
    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p0

    :goto_1
    move-object v6, p0

    .line 64
    new-instance p0, Lcom/squareup/customreport/data/SalesItem;

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/customreport/data/SalesItem;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/common/Money;JLjava/util/List;)V

    return-object p0
.end method

.method public static final toSalesDiscount(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Lcom/squareup/customreport/data/SalesDiscount;
    .locals 5

    const-string v0, "$this$toSalesDiscount"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    new-instance v0, Lcom/squareup/customreport/data/SalesDiscount;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const-string v2, "group_by_value.discount.name_or_translation_type"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_money:Lcom/squareup/protos/common/Money;

    const-string v3, "aggregate.sales.discount_money"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_count:Ljava/lang/Long;

    const-string v3, "aggregate.sales.discount_count"

    invoke-static {p0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 100
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/customreport/data/SalesDiscount;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/common/Money;J)V

    return-object v0
.end method

.method public static final toSalesItem(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Lcom/squareup/customreport/data/SalesItem;
    .locals 7

    const-string v0, "$this$toSalesItem"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    iget-object v2, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const-string v0, "group_by_value.item.name_or_translation_type"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    const-string v0, "aggregate.sales.item_gross_sales_money"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_count:Ljava/lang/String;

    const-string v1, "aggregate.sales.item_count"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 83
    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    if-eqz p0, :cond_1

    check-cast p0, Ljava/lang/Iterable;

    .line 149
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 150
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 151
    check-cast v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    const-string v6, "it"

    .line 82
    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/customreport/data/util/CustomReportsKt;->toSalesItemVariation(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Lcom/squareup/customreport/data/SalesItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 152
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 153
    new-instance p0, Lcom/squareup/customreport/data/util/CustomReportsKt$toSalesItem$$inlined$sortedByDescending$1;

    invoke-direct {p0}, Lcom/squareup/customreport/data/util/CustomReportsKt$toSalesItem$$inlined$sortedByDescending$1;-><init>()V

    check-cast p0, Ljava/util/Comparator;

    invoke-static {v0, p0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p0

    if-eqz p0, :cond_1

    goto :goto_1

    .line 84
    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p0

    :goto_1
    move-object v6, p0

    .line 77
    new-instance p0, Lcom/squareup/customreport/data/SalesItem;

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/customreport/data/SalesItem;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/common/Money;JLjava/util/List;)V

    return-object p0
.end method

.method public static final toSalesItemVariation(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Lcom/squareup/customreport/data/SalesItem;
    .locals 7

    const-string v0, "$this$toSalesItemVariation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    iget-object v2, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const-string v0, "group_by_value.item_vari\u2026.name_or_translation_type"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    const-string v0, "aggregate.sales.item_gross_sales_money"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_count:Ljava/lang/String;

    const-string v0, "aggregate.sales.item_count"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 94
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 90
    new-instance p0, Lcom/squareup/customreport/data/SalesItem;

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/customreport/data/SalesItem;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/common/Money;JLjava/util/List;)V

    return-object p0
.end method

.method public static final toSalesPaymentMethod(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;Lcom/squareup/protos/common/Money;)Lcom/squareup/customreport/data/SalesPaymentMethod;
    .locals 8

    const-string v0, "$this$toSalesPaymentMethod"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "totalCollectedMoney"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    new-instance v0, Lcom/squareup/customreport/data/SalesPaymentMethod;

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    const-string v2, "group_by_value.payment_method"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_collected_money:Lcom/squareup/protos/common/Money;

    const-string v3, "aggregate.net.total_collected_money"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    iget-object v3, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-eqz v7, :cond_1

    .line 114
    :goto_0
    sget-object v3, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    .line 115
    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_collected_money:Lcom/squareup/protos/common/Money;

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-double v4, v4

    iget-object p0, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string/jumbo p1, "totalCollectedMoney.amount"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide p0

    long-to-double p0, p0

    div-double/2addr v4, p0

    .line 114
    invoke-virtual {v3, v4, v5}, Lcom/squareup/util/Percentage$Companion;->fromRate(D)Lcom/squareup/util/Percentage;

    move-result-object p0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    .line 110
    :goto_1
    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/customreport/data/SalesPaymentMethod;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    return-object v0
.end method

.method private static final toSalesSummary(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Lcom/squareup/customreport/data/SalesSummary;
    .locals 15

    .line 45
    new-instance v14, Lcom/squareup/customreport/data/SalesSummary;

    .line 46
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    move-object v2, v0

    goto :goto_0

    :cond_0
    move-object v2, v1

    .line 47
    :goto_0
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->transaction_count:Ljava/lang/Long;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    goto :goto_1

    :cond_1
    const-wide/16 v3, 0x0

    .line 48
    :goto_1
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    move-object v5, v0

    goto :goto_2

    :cond_2
    move-object v5, v1

    .line 49
    :goto_2
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_sales_money:Lcom/squareup/protos/common/Money;

    move-object v6, v0

    goto :goto_3

    :cond_3
    move-object v6, v1

    .line 50
    :goto_3
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    move-object v7, v0

    goto :goto_4

    :cond_4
    move-object v7, v1

    .line 51
    :goto_4
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v0, :cond_5

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_money:Lcom/squareup/protos/common/Money;

    move-object v8, v0

    goto :goto_5

    :cond_5
    move-object v8, v1

    .line 52
    :goto_5
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v0, :cond_6

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tax_money:Lcom/squareup/protos/common/Money;

    move-object v9, v0

    goto :goto_6

    :cond_6
    move-object v9, v1

    .line 53
    :goto_6
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v0, :cond_7

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tip_money:Lcom/squareup/protos/common/Money;

    move-object v10, v0

    goto :goto_7

    :cond_7
    move-object v10, v1

    .line 54
    :goto_7
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v0, :cond_8

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    move-object v11, v0

    goto :goto_8

    :cond_8
    move-object v11, v1

    .line 55
    :goto_8
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v0, :cond_9

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    move-object v12, v0

    goto :goto_9

    :cond_9
    move-object v12, v1

    .line 56
    :goto_9
    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v0, :cond_a

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->credit_money:Lcom/squareup/protos/common/Money;

    move-object v13, v0

    goto :goto_a

    :cond_a
    move-object v13, v1

    .line 57
    :goto_a
    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz p0, :cond_b

    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_sales_money:Lcom/squareup/protos/common/Money;

    goto :goto_b

    :cond_b
    move-object p0, v1

    :goto_b
    move-object v0, v14

    move-object v1, v2

    move-wide v2, v3

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, p0

    .line 45
    invoke-direct/range {v0 .. v13}, Lcom/squareup/customreport/data/SalesSummary;-><init>(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-object v14
.end method

.method public static final toSalesSummary(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Lcom/squareup/customreport/data/SalesSummaryReport;
    .locals 1

    if-eqz p0, :cond_1

    .line 33
    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    if-eqz p0, :cond_1

    .line 34
    new-instance v0, Lcom/squareup/customreport/data/WithSalesSummaryReport;

    .line 35
    invoke-static {p0}, Lcom/squareup/customreport/data/util/CustomReportsKt;->toSalesSummary(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Lcom/squareup/customreport/data/SalesSummary;

    move-result-object p0

    if-eqz p1, :cond_0

    .line 36
    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/squareup/customreport/data/util/CustomReportsKt;->toSalesSummary(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Lcom/squareup/customreport/data/SalesSummary;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 34
    :goto_0
    invoke-direct {v0, p0, p1}, Lcom/squareup/customreport/data/WithSalesSummaryReport;-><init>(Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/customreport/data/SalesSummary;)V

    .line 33
    check-cast v0, Lcom/squareup/customreport/data/SalesSummaryReport;

    goto :goto_1

    .line 38
    :cond_1
    sget-object p0, Lcom/squareup/customreport/data/NoSalesSummaryReport;->INSTANCE:Lcom/squareup/customreport/data/NoSalesSummaryReport;

    move-object v0, p0

    check-cast v0, Lcom/squareup/customreport/data/SalesSummaryReport;

    :goto_1
    return-object v0
.end method

.method public static synthetic toSalesSummary$default(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;ILjava/lang/Object;)Lcom/squareup/customreport/data/SalesSummaryReport;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 32
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/customreport/data/util/CustomReportsKt;->toSalesSummary(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Lcom/squareup/customreport/data/SalesSummaryReport;

    move-result-object p0

    return-object p0
.end method
