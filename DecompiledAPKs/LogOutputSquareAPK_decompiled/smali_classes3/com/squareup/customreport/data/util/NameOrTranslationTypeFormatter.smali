.class public Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;
.super Ljava/lang/Object;
.source "NameOrTranslationTypeFormatter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0000\u0008\u0016\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0018\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u00082\u0006\u0010\u000b\u001a\u00020\u0008H\u0016J\u000c\u0010\u000c\u001a\u00020\r*\u00020\u000eH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "formatName",
        "",
        "nameOrTranslationType",
        "Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;",
        "formatNameAndVariation",
        "itemNameOrTranslationType",
        "variationNameOrTranslationType",
        "resId",
        "",
        "Lcom/squareup/protos/beemo/translation_types/TranslationType;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private final resId(Lcom/squareup/protos/beemo/translation_types/TranslationType;)I
    .locals 1

    .line 114
    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 188
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_unknown_vendor:I

    goto/16 :goto_0

    .line 187
    :pswitch_1
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_menu:I

    goto/16 :goto_0

    .line 186
    :pswitch_2
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_surcharge:I

    goto/16 :goto_0

    .line 185
    :pswitch_3
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_default_measurement_unit:I

    goto/16 :goto_0

    .line 184
    :pswitch_4
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_register_hardware:I

    goto/16 :goto_0

    .line 183
    :pswitch_5
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_cash:I

    goto/16 :goto_0

    .line 182
    :pswitch_6
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_egifting:I

    goto/16 :goto_0

    .line 181
    :pswitch_7
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_terminal:I

    goto/16 :goto_0

    .line 180
    :pswitch_8
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_capital:I

    goto/16 :goto_0

    .line 179
    :pswitch_9
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_payroll:I

    goto/16 :goto_0

    .line 178
    :pswitch_a
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_white_label:I

    goto/16 :goto_0

    .line 177
    :pswitch_b
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_starbucks:I

    goto/16 :goto_0

    .line 176
    :pswitch_c
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_online_store:I

    goto/16 :goto_0

    .line 175
    :pswitch_d
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_store:I

    goto/16 :goto_0

    .line 174
    :pswitch_e
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_billing:I

    goto/16 :goto_0

    .line 173
    :pswitch_f
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_invoices:I

    goto/16 :goto_0

    .line 172
    :pswitch_10
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_appointments:I

    goto/16 :goto_0

    .line 171
    :pswitch_11
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_external_api:I

    goto/16 :goto_0

    .line 170
    :pswitch_12
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_order:I

    goto/16 :goto_0

    .line 169
    :pswitch_13
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_market:I

    goto/16 :goto_0

    .line 168
    :pswitch_14
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_order_source_register:I

    goto/16 :goto_0

    .line 167
    :pswitch_15
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_default_order_source_name:I

    goto/16 :goto_0

    .line 166
    :pswitch_16
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_default_surcharge:I

    goto/16 :goto_0

    .line 165
    :pswitch_17
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_auto_gratuity:I

    goto/16 :goto_0

    .line 164
    :pswitch_18
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_surcharge:I

    goto/16 :goto_0

    .line 163
    :pswitch_19
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_reporting_group:I

    goto/16 :goto_0

    .line 162
    :pswitch_1a
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_unknown_payment_source_name:I

    goto/16 :goto_0

    .line 161
    :pswitch_1b
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_contact_token_name:I

    goto/16 :goto_0

    .line 160
    :pswitch_1c
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_payment_source_name:I

    goto/16 :goto_0

    .line 159
    :pswitch_1d
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_ticket_template_custom_check:I

    goto/16 :goto_0

    .line 158
    :pswitch_1e
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_ticket_template_new_sales:I

    goto/16 :goto_0

    .line 157
    :pswitch_1f
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_conversational_mode:I

    goto/16 :goto_0

    .line 156
    :pswitch_20
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_employee_role_mobile_staff_role:I

    goto/16 :goto_0

    .line 155
    :pswitch_21
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_employee_role_owner_role:I

    goto/16 :goto_0

    .line 154
    :pswitch_22
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_employee_role_owner_role:I

    goto/16 :goto_0

    .line 153
    :pswitch_23
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_employee_role_account_owner_role:I

    goto/16 :goto_0

    .line 152
    :pswitch_24
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_employee_role:I

    goto/16 :goto_0

    .line 151
    :pswitch_25
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_device_credential:I

    goto/16 :goto_0

    .line 150
    :pswitch_26
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_default_device:I

    goto/16 :goto_0

    .line 149
    :pswitch_27
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_ticket_name:I

    goto/16 :goto_0

    .line 148
    :pswitch_28
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_ticket_group_name:I

    goto/16 :goto_0

    .line 147
    :pswitch_29
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_ticket_group:I

    goto/16 :goto_0

    .line 146
    :pswitch_2a
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_comp:I

    goto/16 :goto_0

    .line 145
    :pswitch_2b
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_void_reason:I

    goto/16 :goto_0

    .line 144
    :pswitch_2c
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_vendor:I

    goto/16 :goto_0

    .line 143
    :pswitch_2d
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_gift_card:I

    goto/16 :goto_0

    .line 142
    :pswitch_2e
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_dining_option:I

    goto/16 :goto_0

    .line 141
    :pswitch_2f
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_tax:I

    goto/16 :goto_0

    .line 140
    :pswitch_30
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_default_modifier_set:I

    goto :goto_0

    .line 139
    :pswitch_31
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_modifier_set:I

    goto :goto_0

    .line 138
    :pswitch_32
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_default_item_variation:I

    goto :goto_0

    .line 137
    :pswitch_33
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_custom_amount_item_variation:I

    goto :goto_0

    .line 136
    :pswitch_34
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_employee:I

    goto :goto_0

    .line 135
    :pswitch_35
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_mobile_staff:I

    goto :goto_0

    .line 134
    :pswitch_36
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_subunit:I

    goto :goto_0

    .line 133
    :pswitch_37
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_modifier:I

    goto :goto_0

    .line 132
    :pswitch_38
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_discount:I

    goto :goto_0

    .line 131
    :pswitch_39
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_comp_reason:I

    goto :goto_0

    .line 130
    :pswitch_3a
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_no_category:I

    goto :goto_0

    .line 129
    :pswitch_3b
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_default_device:I

    goto :goto_0

    .line 128
    :pswitch_3c
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_swedish_rounding:I

    goto :goto_0

    .line 127
    :pswitch_3d
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_credit:I

    goto :goto_0

    .line 126
    :pswitch_3e
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_default_item_modifier:I

    goto :goto_0

    .line 125
    :pswitch_3f
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_inclusive_tax:I

    goto :goto_0

    .line 124
    :pswitch_40
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_cash_refund:I

    goto :goto_0

    .line 123
    :pswitch_41
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_refund:I

    goto :goto_0

    .line 122
    :pswitch_42
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_change:I

    goto :goto_0

    .line 121
    :pswitch_43
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_default_tip:I

    goto :goto_0

    .line 120
    :pswitch_44
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_default_sales_tax:I

    goto :goto_0

    .line 119
    :pswitch_45
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_default_item:I

    goto :goto_0

    .line 118
    :pswitch_46
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_default_discount:I

    goto :goto_0

    .line 117
    :pswitch_47
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_default_comp_reason:I

    goto :goto_0

    .line 116
    :pswitch_48
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_unitemized_tax:I

    goto :goto_0

    .line 115
    :pswitch_49
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_custom_amount_item:I

    :goto_0
    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_49
        :pswitch_48
        :pswitch_47
        :pswitch_46
        :pswitch_45
        :pswitch_44
        :pswitch_43
        :pswitch_42
        :pswitch_41
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;
    .locals 1

    const-string v0, "nameOrTranslationType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iget-object v0, p1, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->res:Lcom/squareup/util/Res;

    .line 96
    iget-object p1, p1, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    if-eqz p1, :cond_1

    invoke-direct {p0, p1}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->resId(Lcom/squareup/protos/beemo/translation_types/TranslationType;)I

    move-result p1

    goto :goto_0

    :cond_1
    sget p1, Lcom/squareup/customreport/R$string;->translation_type_unknown:I

    .line 95
    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public formatNameAndVariation(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;
    .locals 2

    const-string v0, "itemNameOrTranslationType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "variationNameOrTranslationType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-virtual {p0, p1}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object p1

    .line 104
    invoke-virtual {p0, p2}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object p2

    .line 106
    iget-object v0, p0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/customreport/R$string;->cart_variation:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 107
    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 108
    check-cast p2, Ljava/lang/CharSequence;

    const-string v0, "price_point"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 109
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 110
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
