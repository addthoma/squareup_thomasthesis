.class public final Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;
.super Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;
.source "ContactAttribute.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CustomUnknownAttribute"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute$Creator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0016\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u000f\u0008\u0010\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005B\'\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\t\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0015\u001a\u00020\u0002H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0002H\u00c6\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0002H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0004H\u00c4\u0003J3\u0010\u0019\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00022\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00022\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u00022\u0008\u0008\u0002\u0010\t\u001a\u00020\u0004H\u00c6\u0001J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\u0013\u0010\u001c\u001a\u00020\u001d2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u00d6\u0003J\t\u0010 \u001a\u00020\u001bH\u00d6\u0001J\u0008\u0010!\u001a\u00020\u0004H\u0016J\t\u0010\"\u001a\u00020\u0002H\u00d6\u0001J\u0019\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020\u001bH\u00d6\u0001R\u0016\u0010\u0008\u001a\u0004\u0018\u00010\u0002X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0006\u001a\u00020\u0002X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0014\u0010\u0007\u001a\u00020\u0002X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cR\u0014\u0010\t\u001a\u00020\u0004X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0002X\u0096\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0012\u0010\u0013\u001a\u0004\u0008\u0014\u0010\u000c\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;",
        "",
        "attribute",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        "(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V",
        "key",
        "name",
        "fallbackValue",
        "original",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V",
        "getFallbackValue",
        "()Ljava/lang/String;",
        "getKey",
        "getName",
        "getOriginal",
        "()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        "value",
        "value$annotations",
        "()V",
        "getValue",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toAttribute",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final fallbackValue:Ljava/lang/String;

.field private final key:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final original:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute$Creator;

    invoke-direct {v0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute$Creator;-><init>()V

    sput-object v0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V
    .locals 3

    const-string v0, "attribute"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    const-string v1, "attribute.key"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->name:Ljava/lang/String;

    const-string v2, "attribute.name"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    .line 102
    invoke-direct {p0, v0, v1, v2, p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    .line 108
    sget-object v0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;->Companion:Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->UNKNOWN:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    invoke-static {v0, p1, v1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;->access$requireType(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V
    .locals 1

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "original"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 101
    invoke-direct {p0, v0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->key:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->name:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->fallbackValue:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->original:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    .line 113
    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->value:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;ILjava/lang/Object;)Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getKey()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getName()Ljava/lang/String;

    move-result-object p2

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object p3

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p4

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic value$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final component4()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;
    .locals 1

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "original"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getFallbackValue()Ljava/lang/String;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->fallbackValue:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->key:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->name:Ljava/lang/String;

    return-object v0
.end method

.method protected getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->original:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    return-object v0
.end method

.method public bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->value:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getKey()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 1

    .line 111
    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CustomUnknownAttribute(key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", fallbackValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", original="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->key:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->name:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->fallbackValue:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->original:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
