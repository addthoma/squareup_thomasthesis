.class final Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$showCreateGroupAction$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealChooseGroupsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->showCreateGroupAction(Lcom/squareup/crm/groups/choose/ChooseGroupsState;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
        "-",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/crm/groups/choose/ChooseGroupsState;


# direct methods
.method constructor <init>(Lcom/squareup/crm/groups/choose/ChooseGroupsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$showCreateGroupAction$1;->$state:Lcom/squareup/crm/groups/choose/ChooseGroupsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$showCreateGroupAction$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
            "-",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    new-instance v0, Lcom/squareup/crm/groups/choose/ChooseGroupsState$CreatingGroup;

    .line 94
    iget-object v1, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$showCreateGroupAction$1;->$state:Lcom/squareup/crm/groups/choose/ChooseGroupsState;

    invoke-virtual {v1}, Lcom/squareup/crm/groups/choose/ChooseGroupsState;->getGroups()Ljava/util/List;

    move-result-object v1

    .line 95
    iget-object v2, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$showCreateGroupAction$1;->$state:Lcom/squareup/crm/groups/choose/ChooseGroupsState;

    invoke-virtual {v2}, Lcom/squareup/crm/groups/choose/ChooseGroupsState;->getSelectedGroupsTokens()Ljava/util/Set;

    move-result-object v2

    .line 93
    invoke-direct {v0, v1, v2}, Lcom/squareup/crm/groups/choose/ChooseGroupsState$CreatingGroup;-><init>(Ljava/util/List;Ljava/util/Set;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
