.class public abstract Lcom/squareup/crm/RolodexEventLoader;
.super Lcom/squareup/datafetch/AbstractLoader;
.source "RolodexEventLoader.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/RolodexEventLoader$SharedScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/datafetch/AbstractLoader<",
        "Ljava/lang/String;",
        "Lcom/squareup/protos/client/rolodex/Event;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008&\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0001\u000eB!\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0002H&\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/crm/RolodexEventLoader;",
        "Lcom/squareup/datafetch/AbstractLoader;",
        "",
        "Lcom/squareup/protos/client/rolodex/Event;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "threadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V",
        "setContactToken",
        "",
        "value",
        "SharedScope",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 9
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param

    const-string v0, "connectivityMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "threadEnforcer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v5, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    .line 19
    invoke-direct/range {v1 .. v8}, Lcom/squareup/datafetch/AbstractLoader;-><init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public abstract setContactToken(Ljava/lang/String;)V
.end method
