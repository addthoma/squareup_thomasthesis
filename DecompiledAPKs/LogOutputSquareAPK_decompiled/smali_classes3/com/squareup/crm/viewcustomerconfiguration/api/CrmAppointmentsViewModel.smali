.class public final Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;
.super Ljava/lang/Object;
.source "CrmAppointmentsViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;,
        Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u00102\u00020\u0001:\u0002\u0010\u0011B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\u0006\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u001d\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u000c\u001a\u00020\rH\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;",
        "",
        "pastAppointments",
        "Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;",
        "upcomingAppointments",
        "(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;)V",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "CrmAppointmentsSectionViewModel",
        "crm-view-customer-configuration_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$Companion;

.field private static final EMPTY:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;


# instance fields
.field public final pastAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

.field public final upcomingAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->Companion:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$Companion;

    .line 49
    new-instance v0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;

    .line 50
    sget-object v1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;->Companion:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$Companion;

    invoke-virtual {v1}, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$Companion;->getEMPTY()Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    move-result-object v1

    .line 51
    sget-object v2, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;->Companion:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$Companion;

    invoke-virtual {v2}, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$Companion;->getEMPTY()Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    move-result-object v2

    .line 49
    invoke-direct {v0, v1, v2}, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;-><init>(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;)V

    sput-object v0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->EMPTY:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;)V
    .locals 1

    const-string v0, "pastAppointments"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "upcomingAppointments"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->pastAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    iput-object p2, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->upcomingAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    return-void
.end method

.method public static final synthetic access$getEMPTY$cp()Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->EMPTY:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;ILjava/lang/Object;)Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->pastAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->upcomingAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->copy(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;)Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->pastAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    return-object v0
.end method

.method public final component2()Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->upcomingAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    return-object v0
.end method

.method public final copy(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;)Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;
    .locals 1

    const-string v0, "pastAppointments"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "upcomingAppointments"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;

    invoke-direct {v0, p1, p2}, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;-><init>(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;

    iget-object v0, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->pastAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    iget-object v1, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->pastAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->upcomingAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    iget-object p1, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->upcomingAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->pastAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->upcomingAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CrmAppointmentsViewModel(pastAppointments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->pastAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", upcomingAppointments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel;->upcomingAppointments:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
