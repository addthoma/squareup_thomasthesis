.class public final enum Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;
.super Ljava/lang/Enum;
.source "RolodexContactLoaderHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/RolodexContactLoaderHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VisualState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

.field public static final enum SHOWING_CUSTOMERS_ALL:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

.field public static final enum SHOWING_CUSTOMERS_FOUND:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

.field public static final enum SHOWING_FAILED_TO_LOAD:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

.field public static final enum SHOWING_NO_CUSTOMERS_AT_ALL:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

.field public static final enum SHOWING_NO_CUSTOMERS_FOUND:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

.field public static final enum SHOWING_PROGRESS_SPINNER:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 29
    new-instance v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    const/4 v1, 0x0

    const-string v2, "SHOWING_PROGRESS_SPINNER"

    invoke-direct {v0, v2, v1}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_PROGRESS_SPINNER:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    .line 33
    new-instance v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    const/4 v2, 0x1

    const-string v3, "SHOWING_FAILED_TO_LOAD"

    invoke-direct {v0, v3, v2}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_FAILED_TO_LOAD:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    .line 35
    new-instance v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    const/4 v3, 0x2

    const-string v4, "SHOWING_NO_CUSTOMERS_AT_ALL"

    invoke-direct {v0, v4, v3}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_NO_CUSTOMERS_AT_ALL:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    .line 37
    new-instance v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    const/4 v4, 0x3

    const-string v5, "SHOWING_NO_CUSTOMERS_FOUND"

    invoke-direct {v0, v5, v4}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_NO_CUSTOMERS_FOUND:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    .line 39
    new-instance v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    const/4 v5, 0x4

    const-string v6, "SHOWING_CUSTOMERS_ALL"

    invoke-direct {v0, v6, v5}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_CUSTOMERS_ALL:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    .line 41
    new-instance v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    const/4 v6, 0x5

    const-string v7, "SHOWING_CUSTOMERS_FOUND"

    invoke-direct {v0, v7, v6}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_CUSTOMERS_FOUND:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    .line 26
    sget-object v7, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_PROGRESS_SPINNER:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_FAILED_TO_LOAD:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_NO_CUSTOMERS_AT_ALL:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_NO_CUSTOMERS_FOUND:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_CUSTOMERS_ALL:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_CUSTOMERS_FOUND:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->$VALUES:[Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;
    .locals 1

    .line 26
    const-class v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;
    .locals 1

    .line 26
    sget-object v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->$VALUES:[Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    invoke-virtual {v0}, [Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    return-object v0
.end method
