.class final Lcom/squareup/crm/RealRolodexServiceHelper$runMergeAllJob$1;
.super Ljava/lang/Object;
.source "RealRolodexServiceHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crm/RealRolodexServiceHelper;->runMergeAllJob()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u0002 \u0004*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;",
        "kotlin.jvm.PlatformType",
        "triggerResponse",
        "Lcom/squareup/protos/client/rolodex/TriggerMergeAllResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/crm/RealRolodexServiceHelper;


# direct methods
.method constructor <init>(Lcom/squareup/crm/RealRolodexServiceHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexServiceHelper$runMergeAllJob$1;->this$0:Lcom/squareup/crm/RealRolodexServiceHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/protos/client/rolodex/TriggerMergeAllResponse;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/TriggerMergeAllResponse;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;",
            ">;>;"
        }
    .end annotation

    const-string/jumbo v0, "triggerResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 556
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexServiceHelper$runMergeAllJob$1;->this$0:Lcom/squareup/crm/RealRolodexServiceHelper;

    invoke-static {v0}, Lcom/squareup/crm/RealRolodexServiceHelper;->access$getRolodexService$p(Lcom/squareup/crm/RealRolodexServiceHelper;)Lcom/squareup/server/crm/RolodexService;

    move-result-object v0

    .line 558
    new-instance v1, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;-><init>()V

    .line 559
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/TriggerMergeAllResponse;->job_id:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;->job_id(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;

    move-result-object p1

    .line 560
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;

    move-result-object p1

    const-string v1, "GetMergeAllStatusRequest\u2026                 .build()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 557
    invoke-interface {v0, p1}, Lcom/squareup/server/crm/RolodexService;->getMergeAllStatus(Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 562
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 564
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/squareup/crm/RealRolodexServiceHelper$runMergeAllJob$1;->this$0:Lcom/squareup/crm/RealRolodexServiceHelper;

    invoke-static {v1}, Lcom/squareup/crm/RealRolodexServiceHelper;->access$getRpcScheduler$p(Lcom/squareup/crm/RealRolodexServiceHelper;)Lio/reactivex/Scheduler;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 563
    invoke-virtual {p1, v2, v3, v0, v1}, Lio/reactivex/Single;->delaySubscription(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 565
    sget-object v0, Lcom/squareup/crm/RealRolodexServiceHelper$runMergeAllJob$1$1;->INSTANCE:Lcom/squareup/crm/RealRolodexServiceHelper$runMergeAllJob$1$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->repeatWhen(Lio/reactivex/functions/Function;)Lio/reactivex/Flowable;

    move-result-object p1

    .line 566
    sget-object v0, Lcom/squareup/crm/RealRolodexServiceHelper$runMergeAllJob$1$2;->INSTANCE:Lcom/squareup/crm/RealRolodexServiceHelper$runMergeAllJob$1$2;

    check-cast v0, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, v0}, Lio/reactivex/Flowable;->skipWhile(Lio/reactivex/functions/Predicate;)Lio/reactivex/Flowable;

    move-result-object p1

    .line 567
    invoke-virtual {p1}, Lio/reactivex/Flowable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 120
    check-cast p1, Lcom/squareup/protos/client/rolodex/TriggerMergeAllResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/crm/RealRolodexServiceHelper$runMergeAllJob$1;->apply(Lcom/squareup/protos/client/rolodex/TriggerMergeAllResponse;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
