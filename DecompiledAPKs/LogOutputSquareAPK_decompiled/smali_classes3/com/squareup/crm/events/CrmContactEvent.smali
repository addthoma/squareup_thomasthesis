.class public Lcom/squareup/crm/events/CrmContactEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "CrmContactEvent.java"


# instance fields
.field private final crm_register_contact_token:Ljava/lang/String;

.field private final crm_register_event:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V
    .locals 1

    const-string v0, "crm_register"

    .line 14
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 15
    iget-object p1, p1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/crm/events/CrmContactEvent;->crm_register_event:Ljava/lang/String;

    .line 16
    iput-object p2, p0, Lcom/squareup/crm/events/CrmContactEvent;->crm_register_contact_token:Ljava/lang/String;

    return-void
.end method
