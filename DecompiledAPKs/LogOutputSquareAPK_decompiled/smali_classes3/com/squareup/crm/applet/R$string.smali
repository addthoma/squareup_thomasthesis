.class public final Lcom/squareup/crm/applet/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/applet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final crm_add_customer_to_manual_group:I = 0x7f120601

.field public static final crm_add_filter_label:I = 0x7f120602

.field public static final crm_add_to_manual_group:I = 0x7f120606

.field public static final crm_add_to_manual_group_another:I = 0x7f120607

.field public static final crm_added_customers_to_group_many_format:I = 0x7f120609

.field public static final crm_added_customers_to_group_one_format:I = 0x7f12060a

.field public static final crm_adding_customers_to_group_error:I = 0x7f12060b

.field public static final crm_adding_customers_to_group_many_format:I = 0x7f12060c

.field public static final crm_adding_customers_to_group_one_format:I = 0x7f12060d

.field public static final crm_apply_filters_label:I = 0x7f120617

.field public static final crm_choose_filters_search_hint:I = 0x7f120646

.field public static final crm_choose_filters_title:I = 0x7f120647

.field public static final crm_confirm_customer_deletion_loyalty_title:I = 0x7f12064e

.field public static final crm_confirm_customer_deletion_many_format:I = 0x7f12064f

.field public static final crm_confirm_customer_deletion_one_format:I = 0x7f120652

.field public static final crm_confirm_customer_deletion_title:I = 0x7f120653

.field public static final crm_confirm_customer_loyalty_deletion_many_format:I = 0x7f120654

.field public static final crm_confirm_customer_loyalty_deletion_one_format:I = 0x7f120655

.field public static final crm_contact_list_deselect_all:I = 0x7f12065a

.field public static final crm_contact_list_select_all:I = 0x7f12065b

.field public static final crm_conversation_view_profile:I = 0x7f120662

.field public static final crm_create_first_manual_group:I = 0x7f120675

.field public static final crm_create_manual_group:I = 0x7f120678

.field public static final crm_create_manual_group_first:I = 0x7f120679

.field public static final crm_custom_attribute_email_hint:I = 0x7f120681

.field public static final crm_custom_attribute_phone_hint:I = 0x7f120682

.field public static final crm_customers_deleted_error:I = 0x7f120691

.field public static final crm_customers_deleted_many_format:I = 0x7f120692

.field public static final crm_customers_deleted_one_format:I = 0x7f120693

.field public static final crm_customers_selected_many_pattern:I = 0x7f120694

.field public static final crm_customers_selected_one:I = 0x7f120695

.field public static final crm_customers_selected_zero:I = 0x7f120696

.field public static final crm_customers_will_be_added_to_group_many_pattern:I = 0x7f120697

.field public static final crm_customers_will_be_added_to_group_one:I = 0x7f120698

.field public static final crm_customers_will_be_merged_into_format:I = 0x7f120699

.field public static final crm_delete:I = 0x7f12069a

.field public static final crm_delete_group_confirm_label:I = 0x7f12069d

.field public static final crm_delete_group_label:I = 0x7f12069e

.field public static final crm_deleting_customers:I = 0x7f1206a1

.field public static final crm_duplicates_merged_error:I = 0x7f1206a4

.field public static final crm_duplicates_merged_format:I = 0x7f1206a5

.field public static final crm_edit_group_label:I = 0x7f1206ad

.field public static final crm_empty_directory_subtitle:I = 0x7f1206bf

.field public static final crm_empty_directory_title:I = 0x7f1206c0

.field public static final crm_failed_to_delete_group:I = 0x7f1206c1

.field public static final crm_failed_to_load_filters:I = 0x7f1206c3

.field public static final crm_failed_to_save_group:I = 0x7f1206c4

.field public static final crm_feedback_empty_list_warning:I = 0x7f1206c5

.field public static final crm_feedback_no_message:I = 0x7f1206c6

.field public static final crm_feedback_title:I = 0x7f1206c7

.field public static final crm_filter_bubble_label:I = 0x7f1206c8

.field public static final crm_filter_customers_menu_label:I = 0x7f1206c9

.field public static final crm_filter_customers_title:I = 0x7f1206ca

.field public static final crm_filter_disjunction_separator_pattern:I = 0x7f1206cb

.field public static final crm_filter_search_empty:I = 0x7f1206cc

.field public static final crm_filter_visit_frequency_value_pattern:I = 0x7f1206cd

.field public static final crm_groups_manual_groups_uppercase:I = 0x7f1206e0

.field public static final crm_groups_smart_groups_uppercase:I = 0x7f1206e2

.field public static final crm_groups_title:I = 0x7f1206e3

.field public static final crm_manual_group_create_description:I = 0x7f120713

.field public static final crm_merge_customers_confirmation_title:I = 0x7f120715

.field public static final crm_merging_duplicates:I = 0x7f12071b

.field public static final crm_remove_all_filters_label:I = 0x7f120753

.field public static final crm_remove_filter_label:I = 0x7f12075a

.field public static final crm_remove_from_group:I = 0x7f12075b

.field public static final crm_resolve_duplicates_label:I = 0x7f12075c

.field public static final crm_resolve_duplicates_title:I = 0x7f12075d

.field public static final crm_rightpane_default_many:I = 0x7f12075e

.field public static final crm_rightpane_default_one:I = 0x7f12075f

.field public static final crm_rightpane_default_zero_noresults:I = 0x7f120760

.field public static final crm_rightpane_multiselect_many:I = 0x7f120761

.field public static final crm_rightpane_multiselect_one:I = 0x7f120762

.field public static final crm_rightpane_multiselect_zero:I = 0x7f120763

.field public static final crm_save:I = 0x7f120764

.field public static final crm_save_filters_label:I = 0x7f120765

.field public static final crm_save_filters_title:I = 0x7f120766

.field public static final crm_select_customers_add_to_group_label:I = 0x7f12076a

.field public static final crm_select_customers_delete_label:I = 0x7f12076b

.field public static final crm_select_customers_menu_label:I = 0x7f12076c

.field public static final crm_select_customers_merge_label:I = 0x7f12076d

.field public static final crm_select_group_title:I = 0x7f12076e

.field public static final crm_select_loyalty_phone:I = 0x7f12076f

.field public static final crm_select_loyalty_phone_primary_button_label:I = 0x7f120770

.field public static final crm_select_loyalty_phone_primary_message:I = 0x7f120771

.field public static final crm_select_loyalty_phone_secondary_message:I = 0x7f120772

.field public static final crm_titlecase_customers:I = 0x7f120779

.field public static final crm_view_feedback:I = 0x7f120783

.field public static final crm_view_groups_label:I = 0x7f120784

.field public static final customers_applet_title:I = 0x7f1207ad


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
