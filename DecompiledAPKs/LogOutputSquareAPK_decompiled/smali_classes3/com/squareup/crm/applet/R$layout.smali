.class public final Lcom/squareup/crm/applet/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/applet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final crm_add_customers_to_group_view:I = 0x7f0d0110

.field public static final crm_choose_filter_card_view:I = 0x7f0d011b

.field public static final crm_choose_filters_card_view:I = 0x7f0d011c

.field public static final crm_edit_filter_card_view:I = 0x7f0d013c

.field public static final crm_empty_filter_content:I = 0x7f0d0142

.field public static final crm_merge_customers_confirmation_view:I = 0x7f0d014a

.field public static final crm_merge_proposal_item_view:I = 0x7f0d014b

.field public static final crm_multi_option_filter_content:I = 0x7f0d014c

.field public static final crm_resolve_duplicates_card_view:I = 0x7f0d0159

.field public static final crm_save_filters_card_view:I = 0x7f0d015c

.field public static final crm_select_loyalty_phone_number_card_view:I = 0x7f0d015e

.field public static final crm_single_option_filter_content:I = 0x7f0d0161

.field public static final crm_single_text_filter_content:I = 0x7f0d0163

.field public static final crm_update_group2_view:I = 0x7f0d0166

.field public static final crm_v2_all_customers_list:I = 0x7f0d0169

.field public static final crm_v2_conversation_view:I = 0x7f0d0172

.field public static final crm_v2_create_manual_group_view:I = 0x7f0d0173

.field public static final crm_v2_customers_list_content:I = 0x7f0d0175

.field public static final crm_v2_filter_bubble:I = 0x7f0d0180

.field public static final crm_v2_filter_layout:I = 0x7f0d0181

.field public static final crm_v2_groups_list_row:I = 0x7f0d0182

.field public static final crm_v2_message_list_row:I = 0x7f0d018a

.field public static final crm_v2_message_list_view:I = 0x7f0d018b

.field public static final crm_v2_multiselect_actionbar:I = 0x7f0d018c

.field public static final crm_v2_multiselect_dropdown:I = 0x7f0d018d

.field public static final crm_v2_no_customer_selected_tablet:I = 0x7f0d018e

.field public static final crm_v2_view_group:I = 0x7f0d019e

.field public static final crm_v2_view_groups_list:I = 0x7f0d019f

.field public static final crm_visit_frequency_filter_content:I = 0x7f0d01a3


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
