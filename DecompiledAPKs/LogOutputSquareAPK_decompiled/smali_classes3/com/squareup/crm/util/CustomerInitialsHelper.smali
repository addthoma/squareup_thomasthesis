.class public final Lcom/squareup/crm/util/CustomerInitialsHelper;
.super Ljava/lang/Object;
.source "CustomerInitialsHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomerInitialsHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomerInitialsHelper.kt\ncom/squareup/crm/util/CustomerInitialsHelper\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,135:1\n704#2:136\n777#2,2:137\n*E\n*S KotlinDebug\n*F\n+ 1 CustomerInitialsHelper.kt\ncom/squareup/crm/util/CustomerInitialsHelper\n*L\n58#1:136\n58#1,2:137\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001\u0016B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008J\u0010\u0010\u0005\u001a\u00020\u00062\u0008\u0010\t\u001a\u0004\u0018\u00010\u0006J\u0018\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0006H\u0002J\u0018\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0006H\u0002J\u0016\u0010\u000f\u001a\u00020\u000c2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0011H\u0002J\u0016\u0010\u0012\u001a\u00020\u000c2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0011H\u0002J\u000c\u0010\u0013\u001a\u00020\u0014*\u00020\u000cH\u0002J\u000e\u0010\u0015\u001a\u00020\u0006*\u0004\u0018\u00010\u0006H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/crm/util/CustomerInitialsHelper;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "getInitials",
        "",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "name",
        "getInitialsFirstNameFirstStyle",
        "firstLastName",
        "Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;",
        "defaultInitials",
        "getInitialsLastNameFirstStyle",
        "parseFirstNameFirstStyle",
        "words",
        "",
        "parseLastNameFirstStyle",
        "containsChineseJapaneseOrKoreanCharacter",
        "",
        "firstOrEmpty",
        "FirstLastName",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/crm/util/CustomerInitialsHelper;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private final containsChineseJapaneseOrKoreanCharacter(Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;)Z
    .locals 2

    .line 127
    invoke-virtual {p1}, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;->getFirstName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 126
    invoke-static {v0}, Lcom/squareup/util/LanguagesKt;->containsChineseJapaneseOrKoreanCharacter(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;->getLastName()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 127
    invoke-static {p1}, Lcom/squareup/util/LanguagesKt;->containsChineseJapaneseOrKoreanCharacter(Ljava/lang/String;)Z

    move-result p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_3

    :cond_2
    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method private final firstOrEmpty(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    if-eqz p1, :cond_0

    .line 131
    check-cast p1, Ljava/lang/CharSequence;

    .line 130
    invoke-static {p1}, Lkotlin/text/StringsKt;->firstOrNull(Ljava/lang/CharSequence;)Ljava/lang/Character;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result p1

    .line 131
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const-string p1, ""

    :goto_1
    return-object p1
.end method

.method private final getInitialsFirstNameFirstStyle(Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 95
    invoke-virtual {p1}, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;->component1()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;->component2()Ljava/lang/String;

    move-result-object p1

    .line 96
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_4

    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    const/4 v2, 0x1

    :cond_3
    if-eqz v2, :cond_4

    goto :goto_2

    .line 99
    :cond_4
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, v0}, Lcom/squareup/crm/util/CustomerInitialsHelper;->firstOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, p1}, Lcom/squareup/crm/util/CustomerInitialsHelper;->firstOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 100
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p2

    const-string v0, "Locale.getDefault()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_5

    invoke-virtual {p1, p2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    const-string p1, "(this as java.lang.String).toUpperCase(locale)"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object p2

    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final getInitialsLastNameFirstStyle(Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 75
    invoke-virtual {p1}, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;->component1()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;->component2()Ljava/lang/String;

    move-result-object p1

    .line 76
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_4

    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_4

    goto :goto_5

    .line 79
    :cond_4
    move-object p2, p1

    check-cast p2, Ljava/lang/CharSequence;

    if-eqz p2, :cond_5

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-nez p2, :cond_6

    :cond_5
    const/4 v2, 0x1

    :cond_6
    if-nez v2, :cond_7

    move-object v0, p1

    goto :goto_4

    :cond_7
    if-eqz v0, :cond_8

    goto :goto_4

    :cond_8
    const-string v0, ""

    :goto_4
    const/4 p1, 0x2

    .line 83
    invoke-static {v0, p1}, Lkotlin/text/StringsKt;->take(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    .line 84
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p2

    const-string v0, "Locale.getDefault()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_9

    invoke-virtual {p1, p2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    const-string p1, "(this as java.lang.String).toUpperCase(locale)"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_5
    return-object p2

    :cond_9
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final parseFirstNameFirstStyle(Ljava/util/List;)Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;"
        }
    .end annotation

    .line 113
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    .line 116
    new-instance v0, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :cond_0
    new-instance v0, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 114
    :cond_1
    new-instance v0, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;

    invoke-direct {v0, v1, v1}, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method private final parseLastNameFirstStyle(Ljava/util/List;)Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;"
        }
    .end annotation

    .line 105
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    .line 108
    new-instance v0, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 107
    :cond_0
    new-instance v0, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 106
    :cond_1
    new-instance v0, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;

    invoke-direct {v0, v1, v1}, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method


# virtual methods
.method public final getInitials(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 3

    const-string v0, "contact"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;

    invoke-static {p1}, Lcom/squareup/crm/util/RolodexContactHelper;->getFirstName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/squareup/crm/util/RolodexContactHelper;->getLastName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0, v0}, Lcom/squareup/crm/util/CustomerInitialsHelper;->containsChineseJapaneseOrKoreanCharacter(Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;)Z

    move-result p1

    .line 32
    iget-object v1, p0, Lcom/squareup/crm/util/CustomerInitialsHelper;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crm/R$string;->crm_contact_default_display_initials:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz p1, :cond_0

    .line 34
    invoke-direct {p0, v0, v1}, Lcom/squareup/crm/util/CustomerInitialsHelper;->getInitialsLastNameFirstStyle(Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 36
    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/squareup/crm/util/CustomerInitialsHelper;->getInitialsFirstNameFirstStyle(Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final getInitials(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .line 52
    iget-object v0, p0, Lcom/squareup/crm/util/CustomerInitialsHelper;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/R$string;->crm_contact_default_display_initials:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_4

    .line 54
    invoke-static {p1}, Lcom/squareup/util/LanguagesKt;->containsChineseJapaneseOrKoreanCharacter(Ljava/lang/String;)Z

    move-result v1

    .line 57
    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    const-string p1, " "

    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lkotlin/text/StringsKt;->split$default(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 136
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 137
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Ljava/lang/String;

    .line 58
    check-cast v4, Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_0

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 138
    :cond_2
    check-cast v2, Ljava/util/List;

    if-eqz v1, :cond_3

    .line 60
    invoke-direct {p0, v2}, Lcom/squareup/crm/util/CustomerInitialsHelper;->parseLastNameFirstStyle(Ljava/util/List;)Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;

    move-result-object p1

    .line 61
    invoke-direct {p0, p1, v0}, Lcom/squareup/crm/util/CustomerInitialsHelper;->getInitialsLastNameFirstStyle(Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 63
    :cond_3
    invoke-direct {p0, v2}, Lcom/squareup/crm/util/CustomerInitialsHelper;->parseFirstNameFirstStyle(Ljava/util/List;)Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;

    move-result-object p1

    .line 64
    invoke-direct {p0, p1, v0}, Lcom/squareup/crm/util/CustomerInitialsHelper;->getInitialsFirstNameFirstStyle(Lcom/squareup/crm/util/CustomerInitialsHelper$FirstLastName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :goto_2
    return-object p1

    :cond_4
    return-object v0
.end method
