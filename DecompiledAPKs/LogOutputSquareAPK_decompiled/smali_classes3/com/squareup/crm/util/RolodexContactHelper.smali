.class public final Lcom/squareup/crm/util/RolodexContactHelper;
.super Ljava/lang/Object;
.source "RolodexContactHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRolodexContactHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RolodexContactHelper.kt\ncom/squareup/crm/util/RolodexContactHelper\n+ 2 Strings.kt\nkotlin/text/StringsKt__StringsKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,631:1\n45#2:632\n17#2,22:633\n747#3:655\n769#3,2:656\n747#3:658\n769#3,2:659\n747#3:661\n769#3,2:662\n1550#3,3:664\n1360#3:667\n1429#3,3:668\n704#3:671\n777#3,2:672\n1099#3,2:674\n1127#3,4:676\n250#3,2:680\n1099#3,2:682\n1127#3,4:684\n704#3:688\n777#3,2:689\n1360#3:691\n1429#3,3:692\n1412#3,9:695\n1642#3,2:704\n1421#3:706\n1370#3:707\n1401#3,4:708\n713#3,10:712\n1651#3,2:722\n723#3,2:724\n1653#3:726\n725#3:727\n*E\n*S KotlinDebug\n*F\n+ 1 RolodexContactHelper.kt\ncom/squareup/crm/util/RolodexContactHelper\n*L\n180#1:632\n180#1,22:633\n216#1:655\n216#1,2:656\n216#1:658\n216#1,2:659\n232#1:661\n232#1,2:662\n383#1,3:664\n384#1:667\n384#1,3:668\n394#1:671\n394#1,2:672\n434#1,2:674\n434#1,4:676\n488#1,2:680\n573#1,2:682\n573#1,4:684\n576#1:688\n576#1,2:689\n577#1:691\n577#1,3:692\n587#1,9:695\n587#1,2:704\n587#1:706\n599#1:707\n599#1,4:708\n607#1,10:712\n607#1,2:722\n607#1,2:724\n607#1:726\n607#1:727\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00cc\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\'\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a.\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\n0\u000e\u001a\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\n\u001a\u000e\u0010\u0012\u001a\u00020\n2\u0006\u0010\u0013\u001a\u00020\u0014\u001a\u0006\u0010\u0015\u001a\u00020\u0010\u001a\u000e\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\n\u001a\u0018\u0010\u0019\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\n2\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001b\u001a\u0010\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\n0\u001d*\u00020\u0017\u001a\u000c\u0010\u001e\u001a\u0004\u0018\u00010\n*\u00020\u0017\u001a\u000c\u0010\u001f\u001a\u0004\u0018\u00010 *\u00020\u0017\u001a\u0014\u0010!\u001a\u0004\u0018\u00010\"*\u00020\u00172\u0006\u0010\u0013\u001a\u00020#\u001a\u0016\u0010$\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020&0%*\u00020\u0017\u001a\u000c\u0010\'\u001a\u0004\u0018\u00010(*\u00020\u0017\u001a\u000c\u0010)\u001a\u0004\u0018\u00010\n*\u00020\u0017\u001a\u000c\u0010*\u001a\u0004\u0018\u00010\n*\u00020\u0017\u001a\u000c\u0010+\u001a\u0004\u0018\u00010\n*\u00020\u0017\u001a\u000c\u0010,\u001a\u0004\u0018\u00010\n*\u00020\u0017\u001a\u0012\u0010-\u001a\u00020\n*\u00020\u00172\u0006\u0010.\u001a\u00020/\u001a\u0016\u00100\u001a\u0004\u0018\u00010\n*\u0004\u0018\u00010\u00172\u0006\u0010.\u001a\u00020/\u001a\u000c\u00101\u001a\u0004\u0018\u00010\n*\u00020\u0017\u001a\n\u00102\u001a\u00020\n*\u00020\u0017\u001a\u0016\u00103\u001a\u00020\n*\u00020\u00172\u0008\u0008\u0002\u00104\u001a\u00020\nH\u0007\u001a\u000c\u00105\u001a\u0004\u0018\u00010\n*\u00020\u0017\u001a\u0018\u00106\u001a\u0008\u0012\u0004\u0012\u0002070\u001d*\u00020\u00172\u0006\u00108\u001a\u000209\u001a\u000c\u0010:\u001a\u0004\u0018\u00010\n*\u00020\u0017\u001a\u000c\u0010;\u001a\u0004\u0018\u00010\n*\u00020\u0017\u001a\u000c\u0010<\u001a\u00020=*\u0004\u0018\u00010>\u001a\n\u0010?\u001a\u00020@*\u00020A\u001a\u001e\u0010B\u001a\u0004\u0018\u00010C*\u0004\u0018\u00010\u00172\u000e\u0010D\u001a\n\u0012\u0004\u0012\u00020E\u0018\u00010\u001d\u001a\u000e\u0010F\u001a\u0004\u0018\u00010\u0017*\u0004\u0018\u00010C\u001a\u000c\u0010G\u001a\u00020H*\u00020IH\u0002\u001a\u000c\u0010J\u001a\u00020I*\u00020HH\u0002\u001a\u0012\u0010K\u001a\u00020\u0017*\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u001b\u001a\u0012\u0010L\u001a\u00020\u0017*\u00020\u00172\u0006\u0010M\u001a\u00020N\u001a\u0012\u0010O\u001a\u00020\u0017*\u00020\u00172\u0006\u0010P\u001a\u00020Q\u001a\u0014\u0010R\u001a\u00020\u0017*\u00020\u00172\u0008\u0010S\u001a\u0004\u0018\u00010 \u001a\u0014\u0010T\u001a\u00020\u0017*\u00020\u00172\u0006\u0010U\u001a\u00020&H\u0002\u001a\u0018\u0010V\u001a\u00020\u0017*\u00020\u00172\u000c\u0010W\u001a\u0008\u0012\u0004\u0012\u00020&0\u001d\u001a\u0014\u0010X\u001a\u00020\u0017*\u00020\u00172\u0008\u0010Y\u001a\u0004\u0018\u00010(\u001a\u0014\u0010Z\u001a\u00020\u0017*\u00020\u00172\u0008\u0010[\u001a\u0004\u0018\u00010\n\u001a\u0014\u0010\\\u001a\u00020\u0017*\u00020\u00172\u0008\u0010]\u001a\u0004\u0018\u00010\n\u001a\u0014\u0010^\u001a\u00020\u0017*\u00020\u00172\u0008\u0010_\u001a\u0004\u0018\u00010\n\u001a\u001a\u0010`\u001a\u00020\u0017*\u00020\u00172\u000e\u0010a\u001a\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u001d\u001a\u0014\u0010b\u001a\u00020\u0017*\u00020\u00172\u0008\u0010c\u001a\u0004\u0018\u00010\n\u001a\u0012\u0010d\u001a\u00020\u0017*\u00020\u00172\u0006\u0010e\u001a\u00020\n\u001a\u0014\u0010f\u001a\u00020\u0017*\u00020\u00172\u0008\u0010P\u001a\u0004\u0018\u00010\n\u001a\u0014\u0010g\u001a\u00020\u0017*\u00020\u00172\u0008\u0010h\u001a\u0004\u0018\u00010\n\u001a\u0014\u0010i\u001a\u00020\u0017*\u00020\u00172\u0008\u0010j\u001a\u0004\u0018\u00010\n\u001a\u0014\u0010k\u001a\u00020\u0017*\u00020\u00172\u0006\u0010l\u001a\u00020\u000cH\u0002\u001a\u0012\u0010m\u001a\u00020\u0017*\u00020\u00172\u0006\u0010n\u001a\u00020\n\u001a\u0014\u0010o\u001a\u00020\u0017*\u00020\u00172\u0008\u0010p\u001a\u0004\u0018\u00010\n\u001a$\u0010q\u001a\u00020\u0017*\u00020\u00172\u0006\u0010r\u001a\u00020\n2\u0008\u0010U\u001a\u0004\u0018\u00010&2\u0006\u0010l\u001a\u00020\u000c\u001a\u0012\u0010s\u001a\u00020\u0017*\u00020\u00172\u0006\u0010t\u001a\u00020\n\u001a\u0012\u0010u\u001a\u00020\u0017*\u00020\u00172\u0006\u0010v\u001a\u00020Q\u001a\u0012\u0010w\u001a\u00020\u0017*\u00020\u00172\u0006\u0010x\u001a\u00020y\u001a\u001c\u0010z\u001a\u00020\u0017*\u00020\u00172\u0006\u0010{\u001a\u00020&2\u0006\u0010l\u001a\u00020\u000cH\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006|"
    }
    d2 = {
        "FIRST_NAME_FIRST",
        "Lkotlin/text/Regex;",
        "LAST_NAME_FIRST",
        "SEARCH_TERM_IS_EMAIL_REGEX",
        "SEARCH_TERM_IS_PHONE_REGEX",
        "buildContactSet",
        "Lcom/squareup/protos/client/rolodex/ContactSet;",
        "checkedByDefault",
        "",
        "groupToken",
        "",
        "contactCount",
        "",
        "xoredContactTokens",
        "",
        "builderForNameString",
        "Lcom/squareup/protos/client/rolodex/Contact$Builder;",
        "nameString",
        "getAnalyticsNameForAttributeType",
        "type",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;",
        "newContactBuilder",
        "newContactFromSearchTerm",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "searchTerm",
        "newContactFromSearchTermOrGroup",
        "group",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "contactToList",
        "",
        "formatManualGroups",
        "getAddress",
        "Lcom/squareup/address/Address;",
        "getAppSpecificData",
        "Lcom/squareup/protos/client/rolodex/AppField;",
        "Lcom/squareup/crm/util/AppSpecificDataType;",
        "getAttributeMapping",
        "",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        "getBirthday",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "getCompany",
        "getDisplayNameOrNull",
        "getEmail",
        "getFirstName",
        "getFullName",
        "res",
        "Lcom/squareup/util/Res;",
        "getFullNameOrNull",
        "getLastName",
        "getLoyaltyTokenForContact",
        "getNonEmptyDisplayName",
        "default",
        "getNote",
        "getOrderedAttributes",
        "Lcom/squareup/crm/model/ContactAttribute;",
        "attributeSchema",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema;",
        "getPhone",
        "getReferenceId",
        "rebuild",
        "Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;",
        "Lcom/squareup/protos/client/rolodex/CustomerProfile;",
        "toAttributeBuilder",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
        "toBuyerInfo",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;",
        "customerInstruments",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
        "toContact",
        "toDateTime",
        "Lcom/squareup/protos/common/time/DateTime;",
        "Lcom/squareup/protos/client/ISO8601Date;",
        "toISO8601Date",
        "withAddedGroup",
        "withAddedInstrument",
        "instrument",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "withAddedNote",
        "note",
        "Lcom/squareup/protos/client/rolodex/Note;",
        "withAddress",
        "address",
        "withAppendedAttribute",
        "attribute",
        "withAttributes",
        "attributes",
        "withBirthday",
        "birthday",
        "withCompany",
        "company",
        "withEmail",
        "email",
        "withFirstName",
        "firstName",
        "withGroups",
        "groups",
        "withLastName",
        "lastName",
        "withName",
        "name",
        "withNote",
        "withPhone",
        "phone",
        "withReferenceId",
        "referenceId",
        "withRemovedAttribute",
        "index",
        "withRemovedNote",
        "noteToken",
        "withSchemaVersion",
        "schemaVersion",
        "withUpdatedAttribute",
        "attributeKey",
        "withUpdatedLoyaltyToken",
        "loyaltyToken",
        "withUpdatedNote",
        "newNote",
        "withUpdatedProfileFromState",
        "state",
        "Lcom/squareup/ui/crm/flow/SaveCardSharedState;",
        "withUpsertedAttribute",
        "newAttribute",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final FIRST_NAME_FIRST:Lkotlin/text/Regex;

.field private static final LAST_NAME_FIRST:Lkotlin/text/Regex;

.field private static final SEARCH_TERM_IS_EMAIL_REGEX:Lkotlin/text/Regex;

.field private static final SEARCH_TERM_IS_PHONE_REGEX:Lkotlin/text/Regex;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 49
    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^[\\s\\d]+$"

    invoke-direct {v0, v1}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/crm/util/RolodexContactHelper;->SEARCH_TERM_IS_PHONE_REGEX:Lkotlin/text/Regex;

    .line 50
    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^.*@.*$"

    invoke-direct {v0, v1}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/crm/util/RolodexContactHelper;->SEARCH_TERM_IS_EMAIL_REGEX:Lkotlin/text/Regex;

    .line 51
    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^(.+), (.+)$"

    invoke-direct {v0, v1}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/crm/util/RolodexContactHelper;->LAST_NAME_FIRST:Lkotlin/text/Regex;

    .line 52
    new-instance v0, Lkotlin/text/Regex;

    const-string v1, "^(.+) (.+)$"

    invoke-direct {v0, v1}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/crm/util/RolodexContactHelper;->FIRST_NAME_FIRST:Lkotlin/text/Regex;

    return-void
.end method

.method public static final buildContactSet(ZLjava/lang/String;ILjava/util/Collection;)Lcom/squareup/protos/client/rolodex/ContactSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/ContactSet;"
        }
    .end annotation

    const-string/jumbo v0, "xoredContactTokens"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 462
    new-instance v0, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;-><init>()V

    .line 463
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->contact_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;

    move-result-object v0

    if-eqz p0, :cond_1

    if-nez p1, :cond_0

    .line 467
    sget-object p0, Lcom/squareup/protos/client/rolodex/ContactSetType;->ALL_CONTACTS:Lcom/squareup/protos/client/rolodex/ContactSetType;

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/squareup/protos/client/rolodex/ContactSetType;->GROUP:Lcom/squareup/protos/client/rolodex/ContactSetType;

    :goto_0
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->type(Lcom/squareup/protos/client/rolodex/ContactSetType;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;

    move-result-object p0

    .line 468
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->group_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;

    move-result-object p0

    .line 469
    check-cast p3, Ljava/lang/Iterable;

    invoke-static {p3}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->contact_tokens_excluded(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;

    goto :goto_1

    .line 472
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/rolodex/ContactSetType;->INCLUDED_CONTACTS:Lcom/squareup/protos/client/rolodex/ContactSetType;

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->type(Lcom/squareup/protos/client/rolodex/ContactSetType;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;

    move-result-object p0

    .line 473
    check-cast p3, Ljava/lang/Iterable;

    invoke-static {p3}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->contact_tokens_included(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;

    move-result-object p0

    .line 474
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->contact_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;

    .line 477
    :goto_1
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->build()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object p0

    const-string p1, "builder.build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final builderForNameString(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 10

    const-string v0, "nameString"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    move-object v1, p0

    check-cast v1, Ljava/lang/CharSequence;

    const-string v0, " "

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lkotlin/text/StringsKt;->split$default(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 175
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const-string v2, "newContactBuilder()\n    \u2026       .build()\n        )"

    const/4 v3, 0x2

    if-ne v1, v3, :cond_6

    .line 176
    invoke-static {}, Lcom/squareup/crm/util/RolodexContactHelper;->newContactBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 178
    new-instance v1, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;-><init>()V

    const/4 v3, 0x0

    .line 179
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->given_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v1

    const/4 v4, 0x1

    .line 180
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 632
    check-cast v0, Ljava/lang/CharSequence;

    .line 634
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v5

    sub-int/2addr v5, v4

    move v6, v5

    const/4 v5, 0x0

    const/4 v7, 0x0

    :goto_0
    if-gt v5, v6, :cond_5

    if-nez v7, :cond_0

    move v8, v5

    goto :goto_1

    :cond_0
    move v8, v6

    .line 639
    :goto_1
    invoke-interface {v0, v8}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v8

    const/16 v9, 0x20

    if-gt v8, v9, :cond_1

    const/4 v8, 0x1

    goto :goto_2

    :cond_1
    const/4 v8, 0x0

    :goto_2
    if-nez v7, :cond_3

    if-nez v8, :cond_2

    const/4 v7, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    if-nez v8, :cond_4

    goto :goto_3

    :cond_4
    add-int/lit8 v6, v6, -0x1

    goto :goto_0

    :cond_5
    :goto_3
    add-int/2addr v6, v4

    .line 654
    invoke-interface {v0, v5, v6}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 632
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 180
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->surname(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object v0

    .line 177
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_4

    .line 184
    :cond_6
    invoke-static {}, Lcom/squareup/crm/util/RolodexContactHelper;->newContactBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 186
    new-instance v1, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;-><init>()V

    .line 187
    invoke-virtual {v1, p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->given_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    .line 188
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p0

    .line 185
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_4
    return-object p0
.end method

.method public static final contactToList(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$contactToList"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_8

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    .line 217
    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 655
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 656
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Ljava/lang/String;

    .line 218
    check-cast v6, Ljava/lang/CharSequence;

    if-eqz v6, :cond_2

    invoke-static {v6}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v6, 0x1

    :goto_2
    if-nez v6, :cond_0

    invoke-interface {v2, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 657
    :cond_3
    check-cast v2, Ljava/util/List;

    move-object v5, v2

    check-cast v5, Ljava/lang/Iterable;

    const-string v1, " "

    .line 219
    move-object v6, v1

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x3e

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    aput-object v1, v2, v3

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    aput-object v1, v2, v4

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    aput-object v1, v2, v0

    const/4 v0, 0x3

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->company_name:Ljava/lang/String;

    aput-object p0, v2, v0

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 658
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 659
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_4
    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Ljava/lang/String;

    .line 222
    check-cast v2, Ljava/lang/CharSequence;

    if-eqz v2, :cond_6

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    goto :goto_5

    :cond_6
    :goto_4
    const/4 v2, 0x1

    :goto_5
    if-nez v2, :cond_4

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 660
    :cond_7
    check-cast v0, Ljava/util/List;

    goto :goto_6

    :cond_8
    const/4 v0, 0x0

    :goto_6
    if-eqz v0, :cond_9

    goto :goto_7

    .line 223
    :cond_9
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_7
    return-object v0
.end method

.method public static final formatManualGroups(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 9

    const-string v0, "$this$formatManualGroups"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 336
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->group:Ljava/util/List;

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/protos/client/rolodex/GroupType;

    const/4 v1, 0x0

    .line 337
    sget-object v2, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    aput-object v2, v0, v1

    invoke-static {p0, v0}, Lcom/squareup/crm/util/RolodexGroupHelper;->filterByTypeAndSort(Ljava/util/List;[Lcom/squareup/protos/client/rolodex/GroupType;)Ljava/util/List;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Ljava/lang/Iterable;

    const-string p0, ", "

    .line 338
    move-object v1, p0

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object p0, Lcom/squareup/crm/util/RolodexContactHelper$formatManualGroups$1$1;->INSTANCE:Lcom/squareup/crm/util/RolodexContactHelper$formatManualGroups$1$1;

    move-object v6, p0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getAddress(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/address/Address;
    .locals 1

    const-string v0, "$this$getAddress"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 343
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/squareup/crm/util/RolodexProtoHelper;->toAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getAnalyticsNameForAttributeType(Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 438
    sget-object v0, Lcom/squareup/crm/util/RolodexContactHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 447
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    const-string p0, "Date"

    goto :goto_0

    :pswitch_1
    const-string p0, "Address"

    goto :goto_0

    :pswitch_2
    const-string p0, "Email"

    goto :goto_0

    :pswitch_3
    const-string p0, "Phone"

    goto :goto_0

    :pswitch_4
    const-string p0, "Enum"

    goto :goto_0

    :pswitch_5
    const-string p0, "Text"

    goto :goto_0

    :pswitch_6
    const-string p0, "Boolean"

    goto :goto_0

    :pswitch_7
    const-string p0, "Number"

    goto :goto_0

    :pswitch_8
    const-string p0, "Unknown"

    :goto_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final getAppSpecificData(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/crm/util/AppSpecificDataType;)Lcom/squareup/protos/client/rolodex/AppField;
    .locals 3

    const-string v0, "$this$getAppSpecificData"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 488
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->app_fields:Ljava/util/List;

    const-string v0, "app_fields"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    .line 680
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/client/rolodex/AppField;

    .line 488
    iget-object v2, p1, Lcom/squareup/crm/util/AppSpecificDataType;->asdName:Ljava/lang/String;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/AppField;->name:Ljava/lang/String;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 681
    :goto_0
    check-cast v0, Lcom/squareup/protos/client/rolodex/AppField;

    return-object v0
.end method

.method public static final getAttributeMapping(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$getAttributeMapping"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 434
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    const-string v0, "attributes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    const/16 v0, 0xa

    .line 674
    invoke-static {p0, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-static {v0}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v0

    .line 675
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v1, Ljava/util/Map;

    .line 676
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 677
    move-object v2, v0

    check-cast v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    .line 434
    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static final getBirthday(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    const-string v0, "$this$getBirthday"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 353
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->birthday:Lcom/squareup/protos/common/time/YearMonthDay;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getCompany(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getCompany"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 306
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->company_name:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getDisplayNameOrNull(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 5

    const-string v0, "$this$getDisplayNameOrNull"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v1, v3

    :goto_0
    const/4 v4, 0x1

    aput-object v1, v0, v4

    const/4 v1, 0x2

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_1

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    :cond_1
    aput-object v3, v0, v1

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 661
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 662
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_2
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Ljava/lang/String;

    .line 232
    check-cast v3, Ljava/lang/CharSequence;

    if-eqz v3, :cond_4

    invoke-static {v3}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_3
    const/4 v3, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v3, 0x1

    :goto_3
    if-nez v3, :cond_2

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 663
    :cond_5
    check-cast v0, Ljava/util/List;

    .line 233
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method public static final getEmail(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getEmail"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 278
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getFirstName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getFirstName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getFullName(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getFullName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 537
    invoke-static {p0, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->getFullNameOrNull(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const-string p0, ""

    .line 538
    :goto_0
    check-cast p0, Ljava/lang/CharSequence;

    .line 539
    invoke-static {p0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget p0, Lcom/squareup/crm/R$string;->crm_contact_default_display_name:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :cond_1
    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method public static final getFullNameOrNull(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 4

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-nez p0, :cond_0

    goto/16 :goto_6

    .line 521
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    if-eqz v1, :cond_4

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_5

    goto :goto_6

    .line 522
    :cond_5
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_7

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    goto :goto_5

    :cond_7
    :goto_4
    const/4 v0, 0x1

    :goto_5
    if-eqz v0, :cond_8

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    goto :goto_6

    .line 523
    :cond_8
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_9

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    const/4 v2, 0x1

    :cond_a
    if-eqz v2, :cond_b

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    goto :goto_6

    .line 524
    :cond_b
    sget v0, Lcom/squareup/crm/R$string;->customer_unit_contact_full_name_format:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 525
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "first"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 526
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    check-cast p0, Ljava/lang/CharSequence;

    const-string v0, "last"

    invoke-virtual {p1, v0, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 527
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 528
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_c
    :goto_6
    return-object v0
.end method

.method public static final getLastName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getLastName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getLoyaltyTokenForContact(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getLoyaltyTokenForContact"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 492
    sget-object v0, Lcom/squareup/crm/util/AppSpecificDataType;->LOYALTY_ACCOUNT_TOKEN:Lcom/squareup/crm/util/AppSpecificDataType;

    invoke-static {p0, v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getAppSpecificData(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/crm/util/AppSpecificDataType;)Lcom/squareup/protos/client/rolodex/AppField;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 493
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/AppField;->text_value:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    const-string p0, ""

    :goto_1
    return-object p0
.end method

.method public static final getNonEmptyDisplayName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1, v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getNonEmptyDisplayName$default(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final getNonEmptyDisplayName(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getNonEmptyDisplayName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "default"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->getDisplayNameOrNull(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const-string p0, ""

    :goto_0
    check-cast p0, Ljava/lang/CharSequence;

    invoke-static {p0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object p0, p1

    :cond_1
    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method public static synthetic getNonEmptyDisplayName$default(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const-string p1, " "

    .line 237
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->getNonEmptyDisplayName(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final getNote(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getNote"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 363
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->memo:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getOrderedAttributes(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/crm/model/ContactAttribute;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$getOrderedAttributes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attributeSchema"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 573
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    const-string v1, "attributes"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    const/16 v1, 0xa

    .line 682
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-static {v2}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v2

    .line 683
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v3, Ljava/util/Map;

    .line 684
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 685
    move-object v4, v2

    check-cast v4, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    .line 573
    iget-object v4, v4, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 575
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema;->attribute_definitions:Ljava/util/List;

    const-string v0, "attributeSchema.attribute_definitions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 688
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 689
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    .line 576
    iget-object v4, v4, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    const-string v5, "it.is_visible_in_profile"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 690
    :cond_2
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 691
    new-instance p1, Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 692
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const-string v2, "definition"

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 693
    check-cast v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    .line 578
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/crm/util/RolodexContactHelper;->toAttributeBuilder(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v2

    .line 580
    iget-object v4, v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    if-eqz v4, :cond_3

    .line 581
    iget-object v5, v4, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iput-object v5, v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    .line 582
    iget-object v4, v4, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    iput-object v4, v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->fallback_value:Ljava/lang/String;

    .line 585
    :cond_3
    invoke-virtual {v2}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 694
    :cond_4
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 695
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 704
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_5
    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 703
    check-cast v1, Lkotlin/Pair;

    invoke-virtual {v1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    invoke-virtual {v1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    const-string v4, "attribute"

    .line 587
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v3, v1}, Lcom/squareup/crm/util/RolodexAttributeHelper;->extractContactAttribute(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)Lcom/squareup/crm/model/ContactAttribute;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 703
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 706
    :cond_6
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final getPhone(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getPhone"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getReferenceId(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getReferenceId"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 321
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->merchant_provided_id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final newContactBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 1

    .line 227
    new-instance v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;-><init>()V

    return-object v0
.end method

.method public static final newContactFromSearchTerm(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 3

    const-string v0, "searchTerm"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    move-object v0, p0

    check-cast v0, Ljava/lang/CharSequence;

    sget-object v1, Lcom/squareup/crm/util/RolodexContactHelper;->SEARCH_TERM_IS_PHONE_REGEX:Lkotlin/text/Regex;

    invoke-virtual {v1, v0}, Lkotlin/text/Regex;->matches(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, "newContactBuilder()\n    \u2026     )\n          .build()"

    if-eqz v1, :cond_0

    .line 136
    invoke-static {}, Lcom/squareup/crm/util/RolodexContactHelper;->newContactBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 138
    new-instance v1, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;-><init>()V

    .line 139
    invoke-virtual {v1, p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    .line 140
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p0

    .line 137
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 142
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 143
    :cond_0
    sget-object v1, Lcom/squareup/crm/util/RolodexContactHelper;->SEARCH_TERM_IS_EMAIL_REGEX:Lkotlin/text/Regex;

    invoke-virtual {v1, v0}, Lkotlin/text/Regex;->matches(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    invoke-static {}, Lcom/squareup/crm/util/RolodexContactHelper;->newContactBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 146
    new-instance v1, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;-><init>()V

    .line 147
    invoke-virtual {v1, p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->email_address(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p0

    .line 145
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 150
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 151
    :cond_1
    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->builderForNameString(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string v0, "builderForNameString(searchTerm).build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p0
.end method

.method public static final newContactFromSearchTermOrGroup(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    const-string v0, "searchTerm"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    move-object v0, p0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    invoke-static {}, Lcom/squareup/crm/util/RolodexContactHelper;->newContactBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    goto :goto_0

    .line 162
    :cond_0
    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->newContactFromSearchTerm(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    :goto_0
    const-string v0, "baseContact"

    if-eqz p1, :cond_1

    .line 166
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withAddedGroup(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    .line 169
    :cond_1
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final rebuild(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 0

    if-eqz p0, :cond_0

    .line 629
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile;->newBuilder()Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    invoke-direct {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;-><init>()V

    :goto_0
    return-object p0
.end method

.method public static final toAttributeBuilder(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;
    .locals 2

    const-string v0, "$this$toAttributeBuilder"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;-><init>()V

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->key(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v0

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->is_visible_in_profile(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v0

    .line 199
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->type(Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object p0

    const-string v0, "Attribute.Builder()\n    \u2026rofile)\n      .type(type)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toBuyerInfo(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    return-object v0

    .line 105
    :cond_0
    new-instance v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;-><init>()V

    if-eqz p0, :cond_1

    .line 106
    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v2, v0

    :goto_0
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;->customer_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;

    move-result-object v1

    .line 108
    new-instance v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;-><init>()V

    if-eqz p0, :cond_2

    .line 109
    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object v3, v0

    :goto_1
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    move-result-object v2

    if-eqz p0, :cond_3

    .line 110
    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/Contact;->phone_token:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move-object v3, v0

    :goto_2
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    move-result-object v2

    if-eqz p0, :cond_4

    .line 111
    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/Contact;->buyer_summary:Lcom/squareup/protos/client/rolodex/BuyerSummary;

    if-eqz v3, :cond_4

    iget-object v3, v3, Lcom/squareup/protos/client/rolodex/BuyerSummary;->last_visit:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v3, :cond_4

    invoke-static {v3}, Lcom/squareup/crm/util/RolodexContactHelper;->toISO8601Date(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v3

    goto :goto_3

    :cond_4
    move-object v3, v0

    :goto_3
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->last_visit(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    move-result-object v2

    if-eqz p0, :cond_5

    .line 113
    sget-object v3, Lcom/squareup/crm/util/AppSpecificDataType;->LOYALTY_ACCOUNT_PHONE_TOKEN:Lcom/squareup/crm/util/AppSpecificDataType;

    invoke-static {p0, v3}, Lcom/squareup/crm/util/RolodexContactHelper;->getAppSpecificData(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/crm/util/AppSpecificDataType;)Lcom/squareup/protos/client/rolodex/AppField;

    move-result-object p0

    if-eqz p0, :cond_5

    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/AppField;->text_value:Ljava/lang/String;

    .line 112
    :cond_5
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->loyalty_account_phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    move-result-object p0

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    move-result-object p0

    .line 107
    invoke-virtual {v1, p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;->display_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;

    move-result-object p0

    if-eqz p1, :cond_6

    goto :goto_4

    .line 117
    :cond_6
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_4
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;->available_instrument_details(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;

    move-result-object p0

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object p0

    return-object p0
.end method

.method public static final toContact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 81
    :cond_0
    invoke-static {}, Lcom/squareup/crm/util/RolodexContactHelper;->newContactBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 84
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    if-eqz p0, :cond_1

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->display_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    .line 88
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz p0, :cond_1

    .line 90
    new-instance v1, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;-><init>()V

    .line 91
    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->toDateTime(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->last_visit(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;

    move-result-object p0

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->build()Lcom/squareup/protos/client/rolodex/BuyerSummary;

    move-result-object p0

    .line 89
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->buyer_summary(Lcom/squareup/protos/client/rolodex/BuyerSummary;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    .line 97
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    return-object p0
.end method

.method private static final toDateTime(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/common/time/DateTime;
    .locals 5

    .line 612
    :try_start_0
    new-instance v0, Lcom/squareup/protos/common/time/DateTime$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/DateTime$Builder;-><init>()V

    .line 613
    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Times;->parseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    const-string v1, "Times.parseIso8601Date(date_string)"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    const/16 p0, 0x3e8

    int-to-long v3, p0

    mul-long v1, v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/common/time/DateTime$Builder;->instant_usec(Ljava/lang/Long;)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object p0

    .line 614
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/DateTime$Builder;->build()Lcom/squareup/protos/common/time/DateTime;

    move-result-object p0

    const-string v0, "DateTime.Builder()\n     \u2026 * 1000)\n        .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 616
    new-instance v0, Ljava/lang/RuntimeException;

    check-cast p0, Ljava/lang/Throwable;

    const-string v1, "Error reading timestamp as ISO8601 date."

    invoke-direct {v0, v1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private static final toISO8601Date(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/ISO8601Date;
    .locals 5

    .line 621
    new-instance v0, Ljava/util/Date;

    iget-object p0, p0, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const/16 p0, 0x3e8

    int-to-long v3, p0

    div-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-static {v0}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    .line 623
    new-instance v0, Lcom/squareup/protos/client/ISO8601Date$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ISO8601Date$Builder;-><init>()V

    .line 624
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/ISO8601Date$Builder;->date_string(Ljava/lang/String;)Lcom/squareup/protos/client/ISO8601Date$Builder;

    move-result-object p0

    .line 625
    invoke-virtual {p0}, Lcom/squareup/protos/client/ISO8601Date$Builder;->build()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object p0

    const-string v0, "ISO8601Date.Builder()\n  \u2026ateString)\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withAddedGroup(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    const-string v0, "$this$withAddedGroup"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "group"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 123
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->group:Ljava/util/List;

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p0

    :goto_0
    check-cast p0, Ljava/util/Collection;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p0, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->group(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .grou\u2026Of(group))\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withAddedInstrument(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/instruments/InstrumentSummary;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    const-string v0, "$this$withAddedInstrument"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instrument"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 544
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;->instrument:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_1
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    .line 545
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 547
    new-instance v0, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$Builder;-><init>()V

    .line 548
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$Builder;->instrument(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$Builder;

    move-result-object p1

    .line 549
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile$Builder;->build()Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    move-result-object p1

    .line 546
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->instruments_on_file(Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 551
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .inst\u2026()\n      )\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withAddedNote(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Note;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 2

    const-string v0, "$this$withAddedNote"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "note"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 377
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 378
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->note:Ljava/util/List;

    const-string/jumbo v1, "this.note"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    invoke-static {p1, p0}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->note(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 379
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .note\u2026this.note)\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withAddress(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/address/Address;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    const-string v0, "$this$withAddress"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 347
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 348
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->rebuild(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/squareup/crm/util/RolodexProtoHelper;->toGlobalAddress(Lcom/squareup/address/Address;)Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 349
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .prof\u2026).build())\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final withAppendedAttribute(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 2

    .line 591
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    const-string v1, "attributes"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withAttributes(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    return-object p0
.end method

.method public static final withAttributes(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Contact;"
        }
    .end annotation

    const-string v0, "$this$withAttributes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attributes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 401
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 402
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->attributes(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 403
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .attr\u2026ttributes)\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withBirthday(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    const-string v0, "$this$withBirthday"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 357
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 358
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->rebuild(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->birthday(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 359
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .prof\u2026).build())\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withCompany(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    const-string v0, "$this$withCompany"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 310
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->company_name:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object p0

    .line 314
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 315
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->rebuild(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->company_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 316
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .prof\u2026).build())\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withEmail(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    const-string v0, "$this$withEmail"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 282
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object p0

    .line 286
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 287
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->rebuild(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->email_address(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 288
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .prof\u2026).build())\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withFirstName(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    const-string v0, "$this$withFirstName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object p0

    .line 250
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 251
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->rebuild(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->given_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 252
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .prof\u2026).build())\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withGroups(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Contact;"
        }
    .end annotation

    const-string v0, "$this$withGroups"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 129
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->group(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .group(groups)\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withLastName(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    const-string v0, "$this$withLastName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object p0

    .line 264
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 265
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->rebuild(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->surname(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 266
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .prof\u2026).build())\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withName(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 5

    const-string v0, "$this$withName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    sget-object v0, Lcom/squareup/crm/util/RolodexContactHelper;->LAST_NAME_FIRST:Lkotlin/text/Regex;

    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lkotlin/text/Regex;->matchEntire(Ljava/lang/CharSequence;)Lkotlin/text/MatchResult;

    move-result-object v0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lkotlin/text/MatchResult;->getDestructured()Lkotlin/text/MatchResult$Destructured;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkotlin/text/MatchResult$Destructured;->getMatch()Lkotlin/text/MatchResult;

    move-result-object v4

    invoke-interface {v4}, Lkotlin/text/MatchResult;->getGroupValues()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v0}, Lkotlin/text/MatchResult$Destructured;->getMatch()Lkotlin/text/MatchResult;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/text/MatchResult;->getGroupValues()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 271
    invoke-static {p0, v0}, Lcom/squareup/crm/util/RolodexContactHelper;->withFirstName(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/squareup/crm/util/RolodexContactHelper;->withLastName(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 272
    :cond_0
    sget-object v0, Lcom/squareup/crm/util/RolodexContactHelper;->FIRST_NAME_FIRST:Lkotlin/text/Regex;

    invoke-virtual {v0, v1}, Lkotlin/text/Regex;->matchEntire(Ljava/lang/CharSequence;)Lkotlin/text/MatchResult;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/text/MatchResult;->getDestructured()Lkotlin/text/MatchResult$Destructured;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lkotlin/text/MatchResult$Destructured;->getMatch()Lkotlin/text/MatchResult;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/text/MatchResult;->getGroupValues()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0}, Lkotlin/text/MatchResult$Destructured;->getMatch()Lkotlin/text/MatchResult;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/text/MatchResult;->getGroupValues()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 273
    invoke-static {p0, v1}, Lcom/squareup/crm/util/RolodexContactHelper;->withFirstName(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/squareup/crm/util/RolodexContactHelper;->withLastName(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    goto :goto_1

    .line 274
    :cond_2
    invoke-static {p0, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withLastName(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public static final withNote(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    const-string v0, "$this$withNote"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 367
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->memo:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object p0

    .line 371
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 372
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->rebuild(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->memo(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 373
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .prof\u2026).build())\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withPhone(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    const-string v0, "$this$withPhone"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 296
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object p0

    .line 300
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 301
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->rebuild(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 302
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .prof\u2026).build())\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withReferenceId(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    const-string v0, "$this$withReferenceId"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 325
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->merchant_provided_id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object p0

    .line 329
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 330
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->rebuild(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->merchant_provided_id(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 331
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .prof\u2026).build())\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final withRemovedAttribute(Lcom/squareup/protos/client/rolodex/Contact;I)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 7

    .line 607
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    const-string v1, "attributes"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 712
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 723
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v5, v3, 0x1

    if-gez v3, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    .line 724
    :cond_0
    move-object v6, v4

    check-cast v6, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    if-eq v3, p1, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_2

    .line 607
    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_2
    move v3, v5

    goto :goto_0

    .line 727
    :cond_3
    check-cast v1, Ljava/util/List;

    .line 607
    invoke-static {p0, v1}, Lcom/squareup/crm/util/RolodexContactHelper;->withAttributes(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    return-object p0
.end method

.method public static final withRemovedNote(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 4

    const-string v0, "$this$withRemovedNote"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "noteToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 394
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->note:Ljava/util/List;

    const-string v1, "note"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 671
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 672
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/rolodex/Note;

    .line 394
    iget-object v3, v3, Lcom/squareup/protos/client/rolodex/Note;->note_token:Ljava/lang/String;

    invoke-static {v3, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 673
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 395
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 396
    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->note(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 397
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .note(notes)\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withSchemaVersion(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    const-string v0, "$this$withSchemaVersion"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->schema_version:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 206
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 207
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->schema_version(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 208
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n        .sc\u2026Version)\n        .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p0
.end method

.method public static final withUpdatedAttribute(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;I)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 2

    const-string v0, "$this$withUpdatedAttribute"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attributeKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 415
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    const-string v1, "attributes"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-gez p3, :cond_0

    goto :goto_1

    :cond_0
    if-le v0, p3, :cond_3

    .line 416
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    if-eqz p2, :cond_1

    .line 418
    invoke-static {p0, p2, p3}, Lcom/squareup/crm/util/RolodexContactHelper;->withUpsertedAttribute(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;I)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    goto :goto_0

    .line 420
    :cond_1
    invoke-static {p0, p3}, Lcom/squareup/crm/util/RolodexContactHelper;->withRemovedAttribute(Lcom/squareup/protos/client/rolodex/Contact;I)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_2
    if-eqz p2, :cond_4

    .line 423
    invoke-static {p0, p2, p3}, Lcom/squareup/crm/util/RolodexContactHelper;->withUpsertedAttribute(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;I)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    return-object p0

    :cond_3
    :goto_1
    if-eqz p2, :cond_4

    .line 426
    invoke-static {p0, p2}, Lcom/squareup/crm/util/RolodexContactHelper;->withAppendedAttribute(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    :cond_4
    return-object p0
.end method

.method public static final withUpdatedLoyaltyToken(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 2

    const-string v0, "$this$withUpdatedLoyaltyToken"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 497
    sget-object v0, Lcom/squareup/crm/util/AppSpecificDataType;->LOYALTY_ACCOUNT_TOKEN:Lcom/squareup/crm/util/AppSpecificDataType;

    invoke-static {p0, v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getAppSpecificData(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/crm/util/AppSpecificDataType;)Lcom/squareup/protos/client/rolodex/AppField;

    move-result-object v0

    if-nez v0, :cond_0

    .line 499
    new-instance v0, Lcom/squareup/protos/client/rolodex/AppField$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/AppField$Builder;-><init>()V

    .line 500
    sget-object v1, Lcom/squareup/crm/util/AppSpecificDataType;->LOYALTY_ACCOUNT_TOKEN:Lcom/squareup/crm/util/AppSpecificDataType;

    iget-object v1, v1, Lcom/squareup/crm/util/AppSpecificDataType;->asdName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/AppField$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AppField$Builder;

    move-result-object v0

    .line 501
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/AppField$Builder;->text_value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AppField$Builder;

    move-result-object p1

    .line 502
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AppField$Builder;->build()Lcom/squareup/protos/client/rolodex/AppField;

    move-result-object p1

    goto :goto_0

    .line 504
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AppField;->newBuilder()Lcom/squareup/protos/client/rolodex/AppField$Builder;

    move-result-object v0

    .line 505
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/AppField$Builder;->text_value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AppField$Builder;

    move-result-object p1

    .line 506
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AppField$Builder;->build()Lcom/squareup/protos/client/rolodex/AppField;

    move-result-object p1

    .line 509
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 510
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->app_fields(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 511
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .app_\u2026AppField))\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withUpdatedNote(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Note;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 5

    const-string v0, "$this$withUpdatedNote"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newNote"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 383
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->note:Ljava/util/List;

    const-string v1, "note"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 664
    instance-of v2, v0, Ljava/util/Collection;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 665
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Note;

    .line 383
    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/Note;->note_token:Ljava/lang/String;

    iget-object v4, p1, Lcom/squareup/protos/client/rolodex/Note;->note_token:Ljava/lang/String;

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v3, 0x1

    :cond_2
    :goto_0
    if-eqz v3, :cond_5

    .line 384
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->note:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 667
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 668
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 669
    check-cast v2, Lcom/squareup/protos/client/rolodex/Note;

    .line 384
    iget-object v3, v2, Lcom/squareup/protos/client/rolodex/Note;->note_token:Ljava/lang/String;

    iget-object v4, p1, Lcom/squareup/protos/client/rolodex/Note;->note_token:Ljava/lang/String;

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move-object v2, p1

    :cond_3
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 670
    :cond_4
    check-cast v1, Ljava/util/List;

    goto :goto_2

    .line 386
    :cond_5
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->note:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    .line 388
    :goto_2
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 389
    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->note(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 390
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .note(notes)\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withUpdatedProfileFromState(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/ui/crm/flow/SaveCardSharedState;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 2

    const-string v0, "$this$withUpdatedProfileFromState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 555
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-nez v0, :cond_0

    .line 556
    new-instance v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;-><init>()V

    .line 557
    iget-object v1, p1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->firstName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->given_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v0

    .line 558
    iget-object v1, p1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->lastName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->surname(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v0

    goto :goto_0

    .line 560
    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/CustomerProfile;->newBuilder()Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v0

    .line 563
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 566
    iget-object p1, p1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->email:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->email_address(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p1

    .line 567
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p1

    .line 564
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 569
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    const-string p1, "newBuilder()\n      .prof\u2026()\n      )\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final withUpsertedAttribute(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;I)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 5

    .line 598
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "attributes"

    if-eqz v0, :cond_3

    .line 599
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 707
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    const/4 v2, 0x0

    .line 709
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_0

    .line 710
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v3, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    if-ne v2, p2, :cond_1

    move-object v3, p1

    .line 599
    :cond_1
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v2, v4

    goto :goto_0

    .line 711
    :cond_2
    check-cast v1, Ljava/util/List;

    goto :goto_1

    .line 601
    :cond_3
    iget-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Ljava/util/Collection;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p2, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    .line 603
    :goto_1
    invoke-static {p0, v1}, Lcom/squareup/crm/util/RolodexContactHelper;->withAttributes(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    return-object p0
.end method
