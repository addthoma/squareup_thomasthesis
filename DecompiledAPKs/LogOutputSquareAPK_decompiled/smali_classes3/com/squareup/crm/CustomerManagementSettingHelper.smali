.class Lcom/squareup/crm/CustomerManagementSettingHelper;
.super Ljava/lang/Object;
.source "CustomerManagementSettingHelper.java"


# static fields
.field private static final MCC_DENTISTRY:I = 0x1f55

.field private static final MCC_MEDICAL_PRACTITIONERS:I = 0x1f4b

.field private static final MCC_MEDICAL_SERVICES:I = 0x1fa3

.field private static final MCC_RETAIL_SHOPS:I = 0x1517


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Constructor should not be called."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method static getDefaultForAfterCheckoutSetting(Lcom/squareup/settings/server/AccountStatusSettings;)Z
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->getMcc()I

    move-result p0

    const/16 v0, 0x1f55

    if-eq p0, v0, :cond_0

    const/16 v0, 0x1f4b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x1fa3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static getDefaultForBeforeCheckoutSetting(Lcom/squareup/settings/server/AccountStatusSettings;)Z
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->getMcc()I

    move-result p0

    const/16 v0, 0x1517

    if-eq p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
