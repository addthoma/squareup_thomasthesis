.class public final enum Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;
.super Ljava/lang/Enum;
.source "OnlineCheckoutPayLinkEvents.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;",
        "",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getValue",
        "()Ljava/lang/String;",
        "GET_PAY_LINK",
        "PAY_LINK_NEW_SALE",
        "PAY_LINK_TAP_TO_COPY_LINK",
        "PAY_LINK_TRY_AGAIN",
        "SHARE_PAY",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

.field public static final enum GET_PAY_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

.field public static final enum PAY_LINK_NEW_SALE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

.field public static final enum PAY_LINK_TAP_TO_COPY_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

.field public static final enum PAY_LINK_TRY_AGAIN:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

.field public static final enum SHARE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    const/4 v2, 0x0

    const-string v3, "GET_PAY_LINK"

    const-string v4, "SPOS Checkout: Online Checkout: Get Pay Link"

    .line 7
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;->GET_PAY_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    const/4 v2, 0x1

    const-string v3, "PAY_LINK_NEW_SALE"

    const-string v4, "SPOS Checkout: Online Checkout: Pay Link: New Sale"

    .line 8
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;->PAY_LINK_NEW_SALE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    const/4 v2, 0x2

    const-string v3, "PAY_LINK_TAP_TO_COPY_LINK"

    const-string v4, "SPOS Checkout: Online Checkout: Pay Link: Tap to copy link"

    .line 9
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;->PAY_LINK_TAP_TO_COPY_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    const/4 v2, 0x3

    const-string v3, "PAY_LINK_TRY_AGAIN"

    const-string v4, "SPOS Checkout: Online Checkout: Pay Link: Try Again"

    .line 10
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;->PAY_LINK_TRY_AGAIN:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    const/4 v2, 0x4

    const-string v3, "SHARE_PAY"

    const-string v4, "SPOS Checkout: Online Checkout: Share Pay"

    .line 11
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;->SHARE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;->$VALUES:[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;
    .locals 1

    const-class v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;
    .locals 1

    sget-object v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;->$VALUES:[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    invoke-virtual {v0}, [Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    return-object v0
.end method


# virtual methods
.method public final getValue()Ljava/lang/String;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;->value:Ljava/lang/String;

    return-object v0
.end method
