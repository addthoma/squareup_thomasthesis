.class public interface abstract Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;
.super Ljava/lang/Object;
.source "OnlineStoreAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0007H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
        "",
        "logTap",
        "",
        "event",
        "Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;",
        "logView",
        "Lcom/squareup/onlinestore/analytics/OnlineStoreViewEvent;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V
.end method

.method public abstract logView(Lcom/squareup/onlinestore/analytics/OnlineStoreViewEvent;)V
.end method
