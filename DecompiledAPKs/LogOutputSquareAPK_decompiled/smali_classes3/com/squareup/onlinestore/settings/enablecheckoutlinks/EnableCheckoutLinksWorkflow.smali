.class public final Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "EnableCheckoutLinksWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action;,
        Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEnableCheckoutLinksWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EnableCheckoutLinksWorkflow.kt\ncom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,181:1\n32#2,12:182\n85#3:194\n85#3:197\n240#4:195\n240#4:198\n276#5:196\n276#5:199\n149#6,5:200\n*E\n*S KotlinDebug\n*F\n+ 1 EnableCheckoutLinksWorkflow.kt\ncom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow\n*L\n106#1,12:182\n118#1:194\n139#1:197\n118#1:195\n139#1:198\n118#1:196\n139#1:199\n163#1,5:200\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 \u001d2<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0002\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001:\u0002\u001c\u001dB\u0017\u0008\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u001f\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u00022\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016\u00a2\u0006\u0002\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0015H\u0007JS\u0010\u0016\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u00032\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00020\u0019H\u0016\u00a2\u0006\u0002\u0010\u001aJ\u0010\u0010\u001b\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u0003H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "checkoutLinksRepository",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
        "analytics",
        "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
        "(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;",
        "logCheckoutLinksEnabled",
        "enabled",
        "",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "Action",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CHECK_CHECKOUTLINK_ENABLED_KEY:Ljava/lang/String; = "check-checkoutlink-worker"

.field public static final Companion:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Companion;

.field public static final ENABLE_CHECKOUTLINK_KEY:Ljava/lang/String; = "enable-checkoutlink-worker"


# instance fields
.field private final analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

.field private final checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;->Companion:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "checkoutLinksRepository"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    return-void
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 182
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 187
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 188
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 189
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 190
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 191
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 193
    :cond_3
    check-cast v1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 106
    :cond_4
    sget-object p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$CheckoutLinksLoadingState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$CheckoutLinksLoadingState;

    move-object v1, p1

    check-cast v1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;

    move-result-object p1

    return-object p1
.end method

.method public final logCheckoutLinksEnabled(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 170
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;

    sget-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->CHECKOUT_LINKS_ENABLE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V

    check-cast v0, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {p1, v0}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    goto :goto_0

    .line 172
    :cond_0
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;

    sget-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->CHECKOUT_LINKS_ENABLE_FAILURE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V

    check-cast v0, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {p1, v0}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;->render(Lkotlin/Unit;Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    .line 116
    sget-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$CheckoutLinksLoadingState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$CheckoutLinksLoadingState;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 118
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    invoke-interface {p2}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->checkCheckoutLinksEnabled()Lio/reactivex/Single;

    move-result-object p2

    .line 194
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$$inlined$asWorker$1;

    invoke-direct {v0, p2, v1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 195
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p2

    .line 196
    const-class v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p2}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    .line 120
    sget-object p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$1;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    const-string v0, "check-checkoutlink-worker"

    .line 117
    invoke-interface {p3, v1, v0, p2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 123
    new-instance p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$LoadingScreen;

    new-instance p3, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$2;

    invoke-direct {p3, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    invoke-direct {p2, p3}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$LoadingScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;

    goto/16 :goto_0

    .line 125
    :cond_0
    sget-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$NetworkErrorState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$NetworkErrorState;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    new-instance p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$NetworkErrorScreen;

    .line 127
    new-instance p3, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$3;

    invoke-direct {p3, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$3;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    .line 128
    new-instance v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$4;

    invoke-direct {v0, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$4;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 126
    invoke-direct {p2, p3, v0}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$NetworkErrorScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;

    goto/16 :goto_0

    .line 131
    :cond_1
    sget-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$CheckoutLinksNotEnabledState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$CheckoutLinksNotEnabledState;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 132
    new-instance p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnableScreen;

    .line 133
    new-instance p3, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$5;

    invoke-direct {p3, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$5;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    .line 134
    new-instance v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$6;

    invoke-direct {v0, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$6;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 132
    invoke-direct {p2, p3, v0}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnableScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;

    goto/16 :goto_0

    .line 137
    :cond_2
    sget-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$EnablingCheckoutLinksState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$EnablingCheckoutLinksState;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 139
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    invoke-interface {p2}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->getJwtTokenForSquareSync()Lio/reactivex/Single;

    move-result-object p2

    .line 197
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$$inlined$asWorker$2;

    invoke-direct {v0, p2, v1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$$inlined$asWorker$2;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 198
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p2

    .line 199
    const-class v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p2}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    .line 140
    new-instance p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$7;

    invoke-direct {p2, p0}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$7;-><init>(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    const-string v0, "enable-checkoutlink-worker"

    .line 138
    invoke-interface {p3, v1, v0, p2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 147
    new-instance p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnablingScreen;

    new-instance p3, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$8;

    invoke-direct {p3, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$8;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    invoke-direct {p2, p3}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnablingScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;

    goto :goto_0

    .line 149
    :cond_3
    sget-object p3, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$EnabledCheckoutLinksState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$EnabledCheckoutLinksState;

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_4

    .line 150
    new-instance p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnabledScreen;

    new-instance p3, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$9;

    invoke-direct {p3, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$9;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    invoke-direct {p2, p3}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnabledScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;

    goto :goto_0

    .line 152
    :cond_4
    sget-object p3, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$CouldNotEnableCheckoutLinksState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$CouldNotEnableCheckoutLinksState;

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_5

    .line 153
    new-instance p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$UnableToEnableScreen;

    .line 154
    new-instance p3, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$10;

    invoke-direct {p3, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$10;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    .line 155
    new-instance v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$11;

    invoke-direct {v0, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$11;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 153
    invoke-direct {p2, p3, v0}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$UnableToEnableScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;

    goto :goto_0

    .line 158
    :cond_5
    sget-object p3, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$FeatureNotAvailableState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$FeatureNotAvailableState;

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_6

    new-instance p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$FeatureNotAvailableScreen;

    .line 159
    new-instance p3, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$12;

    invoke-direct {p3, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$12;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    .line 158
    invoke-direct {p2, p3}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$FeatureNotAvailableScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;

    .line 163
    :goto_0
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 201
    new-instance p3, Lcom/squareup/workflow/legacy/Screen;

    .line 202
    const-class v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 203
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 201
    invoke-direct {p3, v0, p2, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 163
    invoke-virtual {p1, p3}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 158
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;->snapshotState(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
