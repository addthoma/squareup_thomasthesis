.class public abstract Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action;
.super Ljava/lang/Object;
.source "EnableCheckoutLinksWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$EnableCheckoutLinks;,
        Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$CheckoutLinksEnabled;,
        Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$EnableCheckoutLinksApiResult;,
        Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$RetryOnNetworkError;,
        Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$Exit;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0005\u0008\t\n\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0019\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u00a2\u0006\u0002\u0010\u0007\u0082\u0001\u0005\r\u000e\u000f\u0010\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;",
        "",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;",
        "CheckoutLinksEnabled",
        "EnableCheckoutLinks",
        "EnableCheckoutLinksApiResult",
        "Exit",
        "RetryOnNetworkError",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$EnableCheckoutLinks;",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$CheckoutLinksEnabled;",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$EnableCheckoutLinksApiResult;",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$RetryOnNetworkError;",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$Exit;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 55
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 55
    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;",
            ">;)",
            "Lkotlin/Unit;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    instance-of v0, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$CheckoutLinksEnabled;

    if-eqz v0, :cond_3

    .line 65
    move-object v0, p0

    check-cast v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$CheckoutLinksEnabled;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$CheckoutLinksEnabled;->getResult()Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;

    move-result-object v1

    .line 66
    instance-of v2, v1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult$EnabledStatus;

    if-eqz v2, :cond_1

    .line 67
    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$CheckoutLinksEnabled;->getResult()Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;

    move-result-object v0

    check-cast v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult$EnabledStatus;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult$EnabledStatus;->getEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    sget-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$EnabledCheckoutLinksState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$EnabledCheckoutLinksState;

    check-cast v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;

    goto :goto_0

    .line 70
    :cond_0
    sget-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$CheckoutLinksNotEnabledState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$CheckoutLinksNotEnabledState;

    check-cast v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;

    goto :goto_0

    .line 73
    :cond_1
    instance-of v0, v1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult$NetworkError;

    if-eqz v0, :cond_2

    .line 74
    sget-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$NetworkErrorState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$NetworkErrorState;

    check-cast v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;

    .line 65
    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_2

    .line 74
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 78
    :cond_3
    instance-of v0, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$EnableCheckoutLinks;

    if-eqz v0, :cond_4

    .line 79
    sget-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$EnablingCheckoutLinksState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$EnablingCheckoutLinksState;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_2

    .line 81
    :cond_4
    instance-of v0, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$Exit;

    if-eqz v0, :cond_5

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1

    .line 82
    :cond_5
    instance-of v0, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$EnableCheckoutLinksApiResult;

    if-eqz v0, :cond_9

    .line 83
    move-object v0, p0

    check-cast v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$EnableCheckoutLinksApiResult;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$EnableCheckoutLinksApiResult;->getResult()Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;

    move-result-object v0

    .line 84
    instance-of v1, v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$Success;

    if-eqz v1, :cond_6

    .line 85
    sget-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$EnabledCheckoutLinksState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$EnabledCheckoutLinksState;

    check-cast v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;

    goto :goto_1

    .line 87
    :cond_6
    sget-object v1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$UnableToEnableSos;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$UnableToEnableSos;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 88
    sget-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$FeatureNotAvailableState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$FeatureNotAvailableState;

    check-cast v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;

    goto :goto_1

    .line 90
    :cond_7
    sget-object v1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$OtherError;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 91
    sget-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$CouldNotEnableCheckoutLinksState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$CouldNotEnableCheckoutLinksState;

    check-cast v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;

    .line 83
    :goto_1
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_2

    .line 91
    :cond_8
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 95
    :cond_9
    instance-of v0, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$RetryOnNetworkError;

    if-eqz v0, :cond_a

    .line 96
    sget-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$CheckoutLinksLoadingState;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState$CheckoutLinksLoadingState;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    :cond_a
    :goto_2
    const/4 p1, 0x0

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
