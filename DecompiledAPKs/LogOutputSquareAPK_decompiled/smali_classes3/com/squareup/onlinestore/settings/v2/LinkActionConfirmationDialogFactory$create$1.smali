.class final Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$create$1;
.super Ljava/lang/Object;
.source "LinkActionConfirmationDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003j\u0002`\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/Dialog;",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData;",
        "",
        "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreen;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$create$1;->this$0:Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/Dialog;
    .locals 2

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component1()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData;

    .line 30
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$create$1;->this$0:Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$create$1;->$context:Landroid/content/Context;

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;

    invoke-static {v0, v1, p1}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;->access$getDeactivateLinkConfirmationDialogScreen(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;Landroid/content/Context;Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;)Landroid/app/Dialog;

    move-result-object p1

    goto :goto_0

    .line 33
    :cond_0
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeleteLinkConfirmationDialogScreenData;

    if-eqz v0, :cond_1

    .line 34
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$create$1;->this$0:Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$create$1;->$context:Landroid/content/Context;

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeleteLinkConfirmationDialogScreenData;

    invoke-static {v0, v1, p1}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;->access$getDeleteLinkConfirmationDialogScreen(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;Landroid/content/Context;Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeleteLinkConfirmationDialogScreenData;)Landroid/app/Dialog;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method
