.class public abstract Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind;
.super Ljava/lang/Object;
.source "OnlineCheckoutSettingsV2State.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ErrorKind"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$LoadCheckoutLinksError;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$InitialLoadError;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$UpdateEnabledError;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$DeleteError;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\u0003\u0004\u0005\u0006B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0004\u0007\u0008\t\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind;",
        "Landroid/os/Parcelable;",
        "()V",
        "DeleteError",
        "InitialLoadError",
        "LoadCheckoutLinksError",
        "UpdateEnabledError",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$LoadCheckoutLinksError;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$InitialLoadError;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$UpdateEnabledError;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$DeleteError;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind;-><init>()V

    return-void
.end method
