.class final Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$1;
.super Ljava/lang/Object;
.source "CreateLinkScreenLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/text/SelectableTextScrubber;Landroid/text/method/DigitsKeyListener;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "kotlin.jvm.PlatformType",
        "checkedId",
        "",
        "<anonymous parameter 2>",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Lcom/squareup/noho/NohoCheckableGroup;II)V
    .locals 0

    .line 79
    sget p1, Lcom/squareup/onlinestore/settings/impl/R$id;->donation_link_radio_btn:I

    if-ne p2, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 80
    :goto_0
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;

    invoke-static {p2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->access$getLinkPrice$p(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)Lcom/squareup/noho/NohoEditRow;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    xor-int/lit8 p3, p1, 0x1

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 81
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;

    invoke-static {p2, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->access$updateHelpText(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;Z)V

    return-void
.end method
