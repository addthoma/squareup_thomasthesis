.class public final Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;
.super Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;
.source "CheckoutLinkListScreenLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CustomAmountLinkData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0010\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001B1\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0006H\u00c6\u0003J\u0015\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008H\u00c6\u0003J=\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0014\u0008\u0002\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008H\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00062\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u00d6\u0003J\t\u0010\u001b\u001a\u00020\tH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\rR\u001d\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;",
        "name",
        "",
        "amount",
        "enabled",
        "",
        "onClickRow",
        "Lkotlin/Function1;",
        "",
        "",
        "(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function1;)V",
        "getAmount",
        "()Ljava/lang/String;",
        "getEnabled",
        "()Z",
        "getName",
        "getOnClickRow",
        "()Lkotlin/jvm/functions/Function1;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:Ljava/lang/String;

.field private final enabled:Z

.field private final name:Ljava/lang/String;

.field private final onClickRow:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickRow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 292
    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->amount:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->enabled:Z

    iput-object p4, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->onClickRow:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->name:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->amount:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-boolean p3, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->enabled:Z

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->onClickRow:Lkotlin/jvm/functions/Function1;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->copy(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function1;)Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->amount:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->enabled:Z

    return v0
.end method

.method public final component4()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->onClickRow:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function1;)Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickRow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->amount:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->amount:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->enabled:Z

    iget-boolean v1, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->enabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->onClickRow:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->onClickRow:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()Ljava/lang/String;
    .locals 1

    .line 289
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->amount:Ljava/lang/String;

    return-object v0
.end method

.method public final getEnabled()Z
    .locals 1

    .line 290
    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->enabled:Z

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .line 288
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getOnClickRow()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 291
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->onClickRow:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->name:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->amount:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->enabled:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->onClickRow:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CustomAmountLinkData(name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->amount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", onClickRow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;->onClickRow:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
