.class public final Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;
.super Ljava/lang/Object;
.source "EditLinkScreenLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;,
        Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditLinkScreenLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditLinkScreenLayoutRunner.kt\ncom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner\n*L\n1#1,293:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002<=B;\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J$\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\u001c0 H\u0002J$\u0010\"\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020#2\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\u001c0 H\u0002J\u0010\u0010$\u001a\u00020\u001c2\u0006\u0010%\u001a\u00020&H\u0002J\u0008\u0010\'\u001a\u00020\u001cH\u0002J\u0010\u0010(\u001a\u00020\u001c2\u0006\u0010%\u001a\u00020)H\u0002J\u0010\u0010*\u001a\u00020\u001c2\u0006\u0010%\u001a\u00020+H\u0002J\u0012\u0010,\u001a\u00020\u001c2\u0008\u0008\u0001\u0010-\u001a\u00020.H\u0002J\u0018\u0010/\u001a\u00020\u001c2\u0006\u00100\u001a\u00020\u00022\u0006\u00101\u001a\u000202H\u0016J\u0010\u00103\u001a\u00020\u001c2\u0006\u0010%\u001a\u000204H\u0002J2\u00105\u001a\u00020\u001c2\u000c\u00106\u001a\u0008\u0012\u0004\u0012\u00020\u001c072\u0006\u00108\u001a\u00020!2\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\u001c0 H\u0002J\u0008\u00109\u001a\u00020\u001cH\u0002J\u0010\u0010:\u001a\u00020\u001c2\u0006\u00108\u001a\u00020!H\u0002J\u0016\u0010;\u001a\u00020\u001c2\u000c\u00106\u001a\u0008\u0012\u0004\u0012\u00020\u001c07H\u0002R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006>"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData;",
        "view",
        "Landroid/view/View;",
        "moneyScrubber",
        "Lcom/squareup/text/SelectableTextScrubber;",
        "moneyDigitsKeyListener",
        "Landroid/text/method/DigitsKeyListener;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "onlineStoreRestrictions",
        "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
        "(Landroid/view/View;Lcom/squareup/text/SelectableTextScrubber;Landroid/text/method/DigitsKeyListener;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "createLinkScreenContainer",
        "detailsLabel",
        "errorMsg",
        "Landroid/widget/TextView;",
        "linkName",
        "Lcom/squareup/noho/NohoEditRow;",
        "linkPrice",
        "saveLinkScreenContainer",
        "onSaveCustomAmountLinkInfo",
        "",
        "linkInfo",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;",
        "onClickSaveButton",
        "Lkotlin/Function1;",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
        "onSaveDonation",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;",
        "showCreateLinkScreenData",
        "screenData",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;",
        "showEmptyPriceError",
        "showLinkAlreadyExistsErrorScreenData",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;",
        "showLinkCreationFailedScreenData",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkCreationFailedErrorScreenData;",
        "showNameError",
        "msg",
        "",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "showSaveLinkScreenData",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$SaveLinkScreenData;",
        "updateActionBar",
        "onClickUpButton",
        "Lkotlin/Function0;",
        "editLinkInfo",
        "updateDonationsFeatureViewsVisibilty",
        "updateInputFields",
        "updateSaveLinkScreenActionBar",
        "Binding",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final createLinkScreenContainer:Landroid/view/View;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final detailsLabel:Landroid/view/View;

.field private final errorMsg:Landroid/widget/TextView;

.field private final linkName:Lcom/squareup/noho/NohoEditRow;

.field private final linkPrice:Lcom/squareup/noho/NohoEditRow;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

.field private final saveLinkScreenContainer:Landroid/view/View;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/text/SelectableTextScrubber;Landroid/text/method/DigitsKeyListener;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/text/SelectableTextScrubber;",
            "Landroid/text/method/DigitsKeyListener;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyScrubber"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyDigitsKeyListener"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onlineStoreRestrictions"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->view:Landroid/view/View;

    iput-object p4, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p5, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p6, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    .line 56
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object p4, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, p4}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 58
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->create_link_screen_container:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->createLinkScreenContainer:Landroid/view/View;

    .line 59
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->save_link_screen_container:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->saveLinkScreenContainer:Landroid/view/View;

    .line 61
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->details_label:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->detailsLabel:Landroid/view/View;

    .line 62
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->link_name:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    .line 63
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->link_price:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    .line 64
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->error_msg:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    .line 67
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    iget-object p4, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p5, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v0, 0x0

    invoke-static {p5, v0, v1}, Lcom/squareup/money/MoneyBuilderKt;->of(Lcom/squareup/protos/common/CurrencyCode;J)Lcom/squareup/protos/common/Money;

    move-result-object p5

    invoke-interface {p4, p5}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p4

    invoke-virtual {p1, p4}, Lcom/squareup/noho/NohoEditRow;->setHint(Ljava/lang/CharSequence;)V

    .line 68
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    new-instance p4, Lcom/squareup/text/ScrubbingTextWatcher;

    move-object p5, p1

    check-cast p5, Lcom/squareup/text/HasSelectableText;

    invoke-direct {p4, p2, p5}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/SelectableTextScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {p1, p4}, Lcom/squareup/noho/NohoEditRow;->setScrubber(Lcom/squareup/text/ScrubbingTextWatcher;)V

    .line 69
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    check-cast p3, Landroid/text/method/KeyListener;

    invoke-virtual {p1, p3}, Lcom/squareup/noho/NohoEditRow;->setKeyListener(Landroid/text/method/KeyListener;)V

    return-void
.end method

.method public static final synthetic access$getLinkName$p(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;)Lcom/squareup/noho/NohoEditRow;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    return-object p0
.end method

.method public static final synthetic access$onSaveCustomAmountLinkInfo(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->onSaveCustomAmountLinkInfo(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final synthetic access$onSaveDonation(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->onSaveDonation(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final onSaveCustomAmountLinkInfo(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;Lkotlin/jvm/functions/Function1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 188
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    :cond_0
    move-object v4, v1

    .line 189
    :goto_0
    move-object v0, v4

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_3

    .line 190
    sget p1, Lcom/squareup/onlinestore/settings/impl/R$string;->name_is_required:I

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->showNameError(I)V

    return-void

    .line 194
    :cond_3
    sget-object v0, Lcom/squareup/onlinestore/common/PayLinkUtil;->INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

    invoke-virtual {v0, v4}, Lcom/squareup/onlinestore/common/PayLinkUtil;->nameExceedsCharCountLimit(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 195
    sget p1, Lcom/squareup/onlinestore/settings/impl/R$string;->name_cannot_exceed_255:I

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->showNameError(I)V

    return-void

    .line 199
    :cond_4
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_5

    check-cast v0, Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyLocaleHelperKt;->extractMoney(Ljava/lang/CharSequence;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    :cond_5
    move-object v5, v1

    if-eqz v5, :cond_7

    .line 200
    sget-object v0, Lcom/squareup/onlinestore/common/PayLinkUtil;->INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

    invoke-virtual {v0, v5}, Lcom/squareup/onlinestore/common/PayLinkUtil;->isAmountInRange(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-nez v0, :cond_6

    goto :goto_3

    .line 205
    :cond_6
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x9

    const/4 v8, 0x0

    move-object v2, p1

    .line 206
    invoke-static/range {v2 .. v8}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;->copy$default(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZILjava/lang/Object;)Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;

    move-result-object p1

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    .line 201
    :cond_7
    :goto_3
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->showEmptyPriceError()V

    return-void
.end method

.method private final onSaveDonation(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;Lkotlin/jvm/functions/Function1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 213
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v3, v0

    .line 214
    move-object v0, v3

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_3

    .line 215
    sget p1, Lcom/squareup/onlinestore/settings/impl/R$string;->name_is_required:I

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->showNameError(I)V

    return-void

    .line 219
    :cond_3
    sget-object v0, Lcom/squareup/onlinestore/common/PayLinkUtil;->INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

    invoke-virtual {v0, v3}, Lcom/squareup/onlinestore/common/PayLinkUtil;->nameExceedsCharCountLimit(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 220
    sget p1, Lcom/squareup/onlinestore/settings/impl/R$string;->name_cannot_exceed_255:I

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->showNameError(I)V

    return-void

    .line 224
    :cond_4
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object v1, p1

    .line 225
    invoke-static/range {v1 .. v6}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;->copy$default(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;

    move-result-object p1

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final showCreateLinkScreenData(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;)V
    .locals 3

    .line 98
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 100
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->getEditLinkInfo()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->getOnClickSaveButton()Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 99
    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->updateActionBar(Lkotlin/jvm/functions/Function0;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;)V

    .line 103
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->updateDonationsFeatureViewsVisibilty()V

    .line 104
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->getEditLinkInfo()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->updateInputFields(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$showCreateLinkScreenData$1;

    invoke-direct {v0, p0}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$showCreateLinkScreenData$1;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->post(Ljava/lang/Runnable;)Z

    .line 107
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    return-void
.end method

.method private final showEmptyPriceError()V
    .locals 5

    .line 247
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 248
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 252
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    .line 249
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    sget v2, Lcom/squareup/onlinestore/settings/impl/R$string;->create_link_price_error_msg:I

    invoke-static {v1, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 250
    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    sget-object v3, Lcom/squareup/onlinestore/common/PayLinkUtil;->INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

    iget-object v4, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v3, v4}, Lcom/squareup/onlinestore/common/PayLinkUtil;->minPrice(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "min"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 251
    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    sget-object v3, Lcom/squareup/onlinestore/common/PayLinkUtil;->INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

    iget-object v4, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v3, v4}, Lcom/squareup/onlinestore/common/PayLinkUtil;->maxPrice(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "max"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 252
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 253
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void
.end method

.method private final showLinkAlreadyExistsErrorScreenData(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;)V
    .locals 3

    .line 111
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 113
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;->getEditLinkInfo()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;->getOnClickSaveButton()Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 112
    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->updateActionBar(Lkotlin/jvm/functions/Function0;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;)V

    .line 116
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->updateDonationsFeatureViewsVisibilty()V

    .line 117
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;->getEditLinkInfo()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->updateInputFields(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)V

    .line 118
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 119
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 121
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$showLinkAlreadyExistsErrorScreenData$1;

    invoke-direct {v0, p0}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$showLinkAlreadyExistsErrorScreenData$1;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->post(Ljava/lang/Runnable;)Z

    .line 123
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    sget v0, Lcom/squareup/onlinestore/settings/impl/R$string;->create_link_already_exists_error_msg:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 124
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void
.end method

.method private final showLinkCreationFailedScreenData(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkCreationFailedErrorScreenData;)V
    .locals 3

    .line 128
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkCreationFailedErrorScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 130
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkCreationFailedErrorScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkCreationFailedErrorScreenData;->getEditLinkInfo()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkCreationFailedErrorScreenData;->getOnClickSaveButton()Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 129
    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->updateActionBar(Lkotlin/jvm/functions/Function0;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;)V

    .line 133
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->updateDonationsFeatureViewsVisibilty()V

    .line 135
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkCreationFailedErrorScreenData;->getEditLinkInfo()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->updateInputFields(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)V

    .line 136
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 137
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 139
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$showLinkCreationFailedScreenData$1;

    invoke-direct {v0, p0}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$showLinkCreationFailedScreenData$1;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->post(Ljava/lang/Runnable;)Z

    .line 141
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    sget v0, Lcom/squareup/onlinestore/settings/impl/R$string;->create_link_error_msg:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 142
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void
.end method

.method private final showNameError(I)V
    .locals 2

    .line 240
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 241
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 242
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 243
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void
.end method

.method private final showSaveLinkScreenData(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$SaveLinkScreenData;)V
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$SaveLinkScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 147
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$SaveLinkScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->updateSaveLinkScreenActionBar(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final updateActionBar(Lkotlin/jvm/functions/Function0;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 181
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 164
    new-instance v8, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v8}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 166
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_edit_link:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v8, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 167
    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    invoke-virtual {v8, v1, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 169
    sget-object v2, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 170
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_action_save:I

    invoke-direct {v1, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/resources/TextModel;

    .line 171
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;Lkotlin/jvm/functions/Function0;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;)V

    move-object v5, v1

    check-cast v5, Lkotlin/jvm/functions/Function0;

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, v8

    .line 168
    invoke-static/range {v1 .. v7}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 181
    invoke-virtual {v8}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateDonationsFeatureViewsVisibilty()V
    .locals 2

    .line 151
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    invoke-interface {v0}, Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;->isOnlineCheckoutSettingsDonationsEnabled()Z

    move-result v0

    .line 152
    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->detailsLabel:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final updateInputFields(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)V
    .locals 2

    .line 258
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;->getName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;)V

    .line 260
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    invoke-static {v0}, Lcom/squareup/onlinestore/ui/util/NohoEditRowUtilKt;->moveCursorPosToEnd(Lcom/squareup/noho/NohoEditRow;)V

    .line 261
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;)V

    .line 262
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 264
    :cond_0
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;

    if-eqz v0, :cond_1

    .line 265
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;->getName()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;)V

    .line 266
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    invoke-static {p1}, Lcom/squareup/onlinestore/ui/util/NohoEditRowUtilKt;->moveCursorPosToEnd(Lcom/squareup/noho/NohoEditRow;)V

    .line 267
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final updateSaveLinkScreenActionBar(Lkotlin/jvm/functions/Function0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 236
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 231
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 233
    new-instance v2, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_edit_link:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 234
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 236
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->createLinkScreenContainer:Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 77
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->saveLinkScreenContainer:Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 80
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;

    if-eqz p2, :cond_0

    .line 81
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->showCreateLinkScreenData(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;)V

    goto :goto_0

    .line 83
    :cond_0
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;

    if-eqz p2, :cond_1

    .line 84
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->showLinkAlreadyExistsErrorScreenData(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;)V

    goto :goto_0

    .line 86
    :cond_1
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkCreationFailedErrorScreenData;

    if-eqz p2, :cond_2

    .line 87
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkCreationFailedErrorScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->showLinkCreationFailedScreenData(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkCreationFailedErrorScreenData;)V

    goto :goto_0

    .line 89
    :cond_2
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$SaveLinkScreenData;

    if-eqz p2, :cond_3

    .line 90
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$SaveLinkScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->showSaveLinkScreenData(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$SaveLinkScreenData;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->createLinkScreenContainer:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 92
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->saveLinkScreenContainer:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->showRendering(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
