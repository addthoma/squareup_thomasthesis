.class public final Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;
.super Ljava/lang/Object;
.source "CheckoutLinkListScreenLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;,
        Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$Factory;,
        Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckoutLinkListScreenLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CheckoutLinkListScreenLayoutRunner.kt\ncom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 7 Views.kt\ncom/squareup/util/Views\n*L\n1#1,323:1\n49#2:324\n50#2,3:330\n53#2:359\n599#3,4:325\n601#3:329\n43#4,2:333\n310#5,3:335\n313#5,3:344\n310#5,6:347\n310#5,6:353\n35#6,6:338\n1103#7,7:360\n*E\n*S KotlinDebug\n*F\n+ 1 CheckoutLinkListScreenLayoutRunner.kt\ncom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner\n*L\n113#1:324\n113#1,3:330\n113#1:359\n113#1,4:325\n113#1:329\n113#1,2:333\n113#1,3:335\n113#1,3:344\n113#1,6:347\n113#1,6:353\n113#1,6:338\n188#1,7:360\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0003456B#\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0016\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0016\u0010\u001e\u001a\u00020\u001f2\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u001f0!H\u0002J\u0008\u0010\"\u001a\u00020#H\u0002J\u0010\u0010$\u001a\u00020\u001f2\u0006\u0010%\u001a\u00020&H\u0002J\u0010\u0010\'\u001a\u00020\u001f2\u0006\u0010%\u001a\u00020(H\u0002J\u0010\u0010)\u001a\u00020\u001f2\u0006\u0010%\u001a\u00020*H\u0002J\u0010\u0010+\u001a\u00020\u001f2\u0006\u0010,\u001a\u00020-H\u0002J\u0010\u0010.\u001a\u00020\u001f2\u0006\u0010%\u001a\u00020/H\u0002J\u0018\u00100\u001a\u00020\u001f2\u0006\u00101\u001a\u00020\u00022\u0006\u00102\u001a\u000203H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;",
        "view",
        "Landroid/view/View;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/text/Formatter;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "checkoutLinkList",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;",
        "checkoutLinksScreenContainer",
        "createLinkButton",
        "createLinksHelpTextContainer",
        "errorContainer",
        "errorMessageView",
        "Lcom/squareup/noho/NohoMessageView;",
        "linkEnabledStatusMessage",
        "Lcom/squareup/noho/NohoLabel;",
        "linksHeader",
        "loadingScreenContainer",
        "progressBar",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "createRecycler",
        "setupActionBar",
        "",
        "onClickUpButton",
        "Lkotlin/Function0;",
        "showBackArrow",
        "",
        "showCheckoutLinksScreen",
        "screen",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;",
        "showErrorScreen",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;",
        "showFeatureNotAvailableScreen",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$FeatureNotAvailableScreenData;",
        "showLinkUpdatedInfo",
        "linkUpdatedInfo",
        "Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo;",
        "showLoadingScreen",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Binding",
        "CheckoutLinkRow",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final checkoutLinkList:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutLinksScreenContainer:Landroid/view/View;

.field private final createLinkButton:Landroid/view/View;

.field private final createLinksHelpTextContainer:Landroid/view/View;

.field private final errorContainer:Landroid/view/View;

.field private final errorMessageView:Lcom/squareup/noho/NohoMessageView;

.field private final linkEnabledStatusMessage:Lcom/squareup/noho/NohoLabel;

.field private final linksHeader:Landroid/view/View;

.field private final loadingScreenContainer:Landroid/view/View;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final progressBar:Landroid/view/View;

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/text/Formatter;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 56
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 58
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->loading_screen_container:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->loadingScreenContainer:Landroid/view/View;

    .line 59
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->progress_bar:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->progressBar:Landroid/view/View;

    .line 61
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->checkout_link_list_screen_container:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->checkoutLinksScreenContainer:Landroid/view/View;

    .line 63
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->create_links_help_text_container:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->createLinksHelpTextContainer:Landroid/view/View;

    .line 65
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->error_container:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->errorContainer:Landroid/view/View;

    .line 67
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->create_link_button:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->createLinkButton:Landroid/view/View;

    .line 68
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->links_header:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->linksHeader:Landroid/view/View;

    .line 70
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->recycler_view:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 71
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->createRecycler(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->checkoutLinkList:Lcom/squareup/cycler/Recycler;

    .line 73
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->error_message_view:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->errorMessageView:Lcom/squareup/noho/NohoMessageView;

    .line 74
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->link_updated_status_message:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->linkEnabledStatusMessage:Lcom/squareup/noho/NohoLabel;

    return-void
.end method

.method public static final synthetic access$getMoneyFormatter$p(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method private final createRecycler(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            ")",
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;",
            ">;"
        }
    .end annotation

    .line 112
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 324
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 325
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 326
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 330
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 331
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 333
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/16 v2, 0x8

    .line 115
    invoke-virtual {v0, v2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 116
    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 333
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 336
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$$special$$inlined$row$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 118
    sget v2, Lcom/squareup/onlinestore/settings/impl/R$layout;->list_loading_indicator:I

    .line 338
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$$special$$inlined$create$1;

    invoke-direct {v3, v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$$special$$inlined$create$1;-><init>(I)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 336
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 335
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 348
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$$special$$inlined$row$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 131
    sget-object v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$createRecycler$1$3$1;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$createRecycler$1$3$1;

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 348
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 347
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 354
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$$special$$inlined$row$3;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$$special$$inlined$row$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 157
    sget-object v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$createRecycler$1$4$1;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$createRecycler$1$4$1;

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 354
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 353
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 328
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    return-object p1

    .line 325
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final setupActionBar(Lkotlin/jvm/functions/Function0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 281
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 274
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 276
    new-instance v2, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_title:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 277
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->showBackArrow()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 278
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 281
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final showBackArrow()Z
    .locals 1

    .line 284
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private final showCheckoutLinksScreen(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;)V
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 186
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->setupActionBar(Lkotlin/jvm/functions/Function0;)V

    .line 188
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->createLinkButton:Landroid/view/View;

    .line 360
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;->getList()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 191
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->checkoutLinkList:Lcom/squareup/cycler/Recycler;

    new-instance v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;-><init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    .line 218
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;->getLinkUpdatedInfo()Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;->getLinkUpdatedInfo()Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->showLinkUpdatedInfo(Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo;)V

    .line 220
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->linkEnabledStatusMessage:Lcom/squareup/noho/NohoLabel;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->linkEnabledStatusMessage:Lcom/squareup/noho/NohoLabel;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 225
    :goto_0
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->linksHeader:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;->getList()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 226
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->createLinksHelpTextContainer:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;->getList()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final showErrorScreen(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;)V
    .locals 2

    .line 262
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 263
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->setupActionBar(Lkotlin/jvm/functions/Function0;)V

    .line 265
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->errorMessageView:Lcom/squareup/noho/NohoMessageView;

    .line 266
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->connection_error_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 267
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->connection_error_msg:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 268
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->connection_error_try_again:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonText(I)V

    .line 269
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showErrorScreen$$inlined$with$lambda$1;

    invoke-direct {v1, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showErrorScreen$$inlined$with$lambda$1;-><init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    const-string v1, "debounce { screen.onClickRetryButton() }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method

.method private final showFeatureNotAvailableScreen(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$FeatureNotAvailableScreenData;)V
    .locals 2

    .line 251
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$FeatureNotAvailableScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 252
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$FeatureNotAvailableScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->setupActionBar(Lkotlin/jvm/functions/Function0;)V

    .line 254
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->errorMessageView:Lcom/squareup/noho/NohoMessageView;

    const/4 v0, 0x0

    .line 255
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 256
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_feature_not_available_msg:I

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 257
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final showLinkUpdatedInfo(Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo;)V
    .locals 4

    .line 231
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo$LinkEnabledStatusUpdated;

    const-string v1, "name"

    if-eqz v0, :cond_1

    .line 232
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo$LinkEnabledStatusUpdated;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo$LinkEnabledStatusUpdated;->getEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->linkEnabledStatusMessage:Lcom/squareup/noho/NohoLabel;

    .line 233
    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    sget v3, Lcom/squareup/onlinestore/settings/impl/R$string;->link_activated_msg:I

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 234
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo$LinkEnabledStatusUpdated;->getName()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 235
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->linkEnabledStatusMessage:Lcom/squareup/noho/NohoLabel;

    .line 237
    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    sget v3, Lcom/squareup/onlinestore/settings/impl/R$string;->link_deactivated_msg:I

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 238
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo$LinkEnabledStatusUpdated;->getName()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 239
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 242
    :cond_1
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo$LinkDeleted;

    if-eqz v0, :cond_2

    .line 245
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->linkEnabledStatusMessage:Lcom/squareup/noho/NohoLabel;

    .line 243
    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    sget v3, Lcom/squareup/onlinestore/settings/impl/R$string;->link_deleted_msg:I

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 244
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo$LinkDeleted;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo$LinkDeleted;->getName()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 245
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private final showLoadingScreen(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;)V
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 106
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->setupActionBar(Lkotlin/jvm/functions/Function0;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->progressBar:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;->getHideProgressBar()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->loadingScreenContainer:Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 81
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->checkoutLinksScreenContainer:Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 82
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->errorContainer:Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 85
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;

    if-eqz p2, :cond_0

    .line 86
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->showLoadingScreen(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;)V

    .line 87
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->loadingScreenContainer:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 89
    :cond_0
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;

    if-eqz p2, :cond_1

    .line 90
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->showCheckoutLinksScreen(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->checkoutLinksScreenContainer:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 93
    :cond_1
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$FeatureNotAvailableScreenData;

    if-eqz p2, :cond_2

    .line 94
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$FeatureNotAvailableScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->showFeatureNotAvailableScreen(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$FeatureNotAvailableScreenData;)V

    .line 95
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->errorContainer:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 97
    :cond_2
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;

    if-eqz p2, :cond_3

    .line 98
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->showErrorScreen(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;)V

    .line 99
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->errorContainer:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 50
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->showRendering(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
