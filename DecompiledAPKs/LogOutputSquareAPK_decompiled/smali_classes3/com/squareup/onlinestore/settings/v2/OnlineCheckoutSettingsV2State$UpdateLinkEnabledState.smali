.class public final Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;
.super Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;
.source "OnlineCheckoutSettingsV2State.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UpdateLinkEnabledState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\u0013\u0010\u0010\u001a\u00020\u00032\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\u0019\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
        "newEnabledState",
        "",
        "checkoutLink",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLink;",
        "(ZLcom/squareup/onlinestore/settings/v2/CheckoutLink;)V",
        "getCheckoutLink",
        "()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;",
        "getNewEnabledState",
        "()Z",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final checkoutLink:Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

.field private final newEnabledState:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState$Creator;

    invoke-direct {v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState$Creator;-><init>()V

    sput-object v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ZLcom/squareup/onlinestore/settings/v2/CheckoutLink;)V
    .locals 1

    const-string v0, "checkoutLink"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->newEnabledState:Z

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->checkoutLink:Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;ZLcom/squareup/onlinestore/settings/v2/CheckoutLink;ILjava/lang/Object;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-boolean p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->newEnabledState:Z

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->checkoutLink:Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->copy(ZLcom/squareup/onlinestore/settings/v2/CheckoutLink;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->newEnabledState:Z

    return v0
.end method

.method public final component2()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;
    .locals 1

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->checkoutLink:Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    return-object v0
.end method

.method public final copy(ZLcom/squareup/onlinestore/settings/v2/CheckoutLink;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;
    .locals 1

    const-string v0, "checkoutLink"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;

    invoke-direct {v0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;-><init>(ZLcom/squareup/onlinestore/settings/v2/CheckoutLink;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;

    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->newEnabledState:Z

    iget-boolean v1, p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->newEnabledState:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->checkoutLink:Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    iget-object p1, p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->checkoutLink:Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->checkoutLink:Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    return-object v0
.end method

.method public final getNewEnabledState()Z
    .locals 1

    .line 48
    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->newEnabledState:Z

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->newEnabledState:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->checkoutLink:Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdateLinkEnabledState(newEnabledState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->newEnabledState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", checkoutLink="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->checkoutLink:Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->newEnabledState:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->checkoutLink:Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
