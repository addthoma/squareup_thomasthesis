.class public final Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;
.super Ljava/lang/Object;
.source "RealOnlineStoreRestrictions.kt"

# interfaces
.implements Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOnlineStoreRestrictions.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOnlineStoreRestrictions.kt\ncom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,51:1\n1577#2,4:52\n*E\n*S KotlinDebug\n*F\n+ 1 RealOnlineStoreRestrictions.kt\ncom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions\n*L\n49#1,4:52\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0008H\u0016J\u0008\u0010\n\u001a\u00020\u0008H\u0016J\u0008\u0010\u000b\u001a\u00020\u0008H\u0016J\u0008\u0010\u000c\u001a\u00020\u0008H\u0016J\u0008\u0010\r\u001a\u00020\u0008H\u0016J\u0008\u0010\u000e\u001a\u00020\u0008H\u0016J\u0008\u0010\u000f\u001a\u00020\u0008H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;",
        "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "isBuyLinksWithSilentAuthEnabled",
        "",
        "isCheckoutLinksBuyButtonEnabled",
        "isOnlineCheckoutPayLinksEnabled",
        "isOnlineCheckoutSettingsDonationsEnabled",
        "isOnlineCheckoutSettingsEntryEnabled",
        "isOnlineCheckoutSettingsV2Enabled",
        "isOnlineCheckoutTenderOptionEnabled",
        "isSingleSellerLocationActive",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method private final isSingleSellerLocationActive()Z
    .locals 5

    .line 49
    iget-object v0, p0, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantUnits()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    check-cast v0, Ljava/lang/Iterable;

    .line 52
    instance-of v2, v0, Ljava/util/Collection;

    if-eqz v2, :cond_0

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_2

    .line 54
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/MerchantUnit;

    if-eqz v3, :cond_2

    .line 49
    iget-object v3, v3, Lcom/squareup/server/account/protos/MerchantUnit;->active:Ljava/lang/Boolean;

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    sget-object v3, Lcom/squareup/server/account/protos/MerchantUnit;->DEFAULT_ACTIVE:Ljava/lang/Boolean;

    const-string v4, "DEFAULT_ACTIVE"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    add-int/lit8 v2, v2, 0x1

    if-gez v2, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_0

    :cond_3
    :goto_2
    const/4 v2, 0x0

    :cond_4
    const/4 v0, 0x1

    if-ne v2, v0, :cond_5

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    :goto_3
    return v0
.end method


# virtual methods
.method public isBuyLinksWithSilentAuthEnabled()Z
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_BUY_LINKS_SILENT_AUTH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->isSingleSellerLocationActive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isCheckoutLinksBuyButtonEnabled()Z
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_BUY_BUTTON:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->isSingleSellerLocationActive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOnlineCheckoutPayLinksEnabled()Z
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_PAY_LINKS_ON_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->isSingleSellerLocationActive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOnlineCheckoutSettingsDonationsEnabled()Z
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_ONLINE_CHECKOUT_SETTINGS_DONATIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    invoke-direct {p0}, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->isSingleSellerLocationActive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOnlineCheckoutSettingsEntryEnabled()Z
    .locals 2

    .line 20
    iget-object v0, p0, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_CHECKOUT_LINKS_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->isSingleSellerLocationActive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOnlineCheckoutSettingsV2Enabled()Z
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_ONLINE_CHECKOUT_SETTINGS_V2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->isSingleSellerLocationActive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOnlineCheckoutTenderOptionEnabled()Z
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_PAY_ONLINE_TENDER_OPTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/onlinestore/restrictions/RealOnlineStoreRestrictions;->isSingleSellerLocationActive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
