.class final Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOnlineCheckoutTenderOptionFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;Lcom/squareup/payment/Transaction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "displayData",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;->invoke(Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;)Ljava/lang/String;
    .locals 4

    const-string v0, "displayData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {v0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$getCatalogItemCountInCart(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)I

    move-result v0

    .line 49
    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {v1}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$hasCustomItemInCart(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Z

    move-result v1

    .line 50
    iget-object v2, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {v2}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$hasEcomAvailableItemInCart(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Z

    move-result v2

    .line 51
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isConnectedToInternet()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isInOfflineMode()Z

    move-result v3

    if-eqz v3, :cond_0

    goto/16 :goto_0

    .line 53
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isSplitTender()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 54
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {p1}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$getRes$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title_no_split_payments:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_1

    :cond_1
    const/4 p1, 0x1

    if-le v0, p1, :cond_2

    if-nez v1, :cond_2

    .line 56
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {p1}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$getRes$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title_single_items_only:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    if-ne v0, p1, :cond_3

    if-nez v1, :cond_3

    if-nez v2, :cond_3

    .line 58
    iget-object v2, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {v2}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$isSilentAuthEnabled(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 60
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {p1}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$getRes$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title_item_unavailable:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_3
    if-lt v0, p1, :cond_4

    if-eqz v1, :cond_4

    .line 62
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {p1}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$getRes$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title_single_items_only:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 63
    :cond_4
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {p1}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$getOnlineStoreRestrictions$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;->isOnlineCheckoutPayLinksEnabled()Z

    move-result p1

    if-nez p1, :cond_5

    if-nez v0, :cond_5

    if-eqz v1, :cond_5

    .line 66
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {p1}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$getRes$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title_items_only:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 68
    :cond_5
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {p1}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$getRes$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 52
    :cond_6
    :goto_0
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {p1}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$getRes$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title_online_only:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1
.end method
