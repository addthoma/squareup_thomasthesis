.class public final Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;
.super Ljava/lang/Object;
.source "PayLinkScreenLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPayLinkScreenLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PayLinkScreenLayoutRunner.kt\ncom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,195:1\n1103#2,7:196\n1103#2,7:203\n*E\n*S KotlinDebug\n*F\n+ 1 PayLinkScreenLayoutRunner.kt\ncom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner\n*L\n126#1,7:196\n138#1,7:203\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 +2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001+B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001bH\u0002J\u0018\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u00022\u0006\u0010 \u001a\u00020!H\u0016J\u0010\u0010\"\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020$H\u0002J\u0010\u0010%\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020&H\u0002J\u0010\u0010\'\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020(H\u0002J\u0010\u0010)\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020*H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0016\u001a\n \u0018*\u0004\u0018\u00010\u00170\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "checkoutLinkScreenContainer",
        "Landroid/view/ViewGroup;",
        "checkoutLinkScreenName",
        "Lcom/squareup/noho/NohoLabel;",
        "checkoutLinkUrl",
        "Lcom/squareup/noho/NohoRow;",
        "errorMessageView",
        "Lcom/squareup/noho/NohoMessageView;",
        "errorScreenContainer",
        "helpText",
        "Lcom/squareup/widgets/MessageView;",
        "loadingScreenContainer",
        "qrCodeView",
        "Landroid/widget/ImageView;",
        "res",
        "Landroid/content/res/Resources;",
        "kotlin.jvm.PlatformType",
        "shareLink",
        "getActionBarTitle",
        "",
        "amount",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateCheckoutLinkScreen",
        "screenData",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;",
        "updateErrorScreen",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$ErrorScreenData;",
        "updateFeatureNotAvailableScreen",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$FeatureNotAvailableScreenData;",
        "updateLoadingScreen",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$LoadingScreenData;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CHECKOUT_LINK_URL_MAX_LINES:I = 0x2

.field private static final CHECKOUT_LINK_URL_WIDTH_PERCENT:F = 0.8f

.field public static final Companion:Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final checkoutLinkScreenContainer:Landroid/view/ViewGroup;

.field private final checkoutLinkScreenName:Lcom/squareup/noho/NohoLabel;

.field private final checkoutLinkUrl:Lcom/squareup/noho/NohoRow;

.field private final errorMessageView:Lcom/squareup/noho/NohoMessageView;

.field private final errorScreenContainer:Landroid/view/ViewGroup;

.field private final helpText:Lcom/squareup/widgets/MessageView;

.field private final loadingScreenContainer:Landroid/view/ViewGroup;

.field private final qrCodeView:Landroid/widget/ImageView;

.field private final res:Landroid/content/res/Resources;

.field private final shareLink:Landroid/view/View;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->Companion:Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    .line 42
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->res:Landroid/content/res/Resources;

    .line 43
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 46
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->checkout_link_screen_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->checkoutLinkScreenContainer:Landroid/view/ViewGroup;

    .line 48
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->loading_screen_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->loadingScreenContainer:Landroid/view/ViewGroup;

    .line 50
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->error_screen_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->errorScreenContainer:Landroid/view/ViewGroup;

    .line 53
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->checkout_link_screen_name_label:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->checkoutLinkScreenName:Lcom/squareup/noho/NohoLabel;

    .line 54
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->online_checkout_checkoutlink_qr_code:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->qrCodeView:Landroid/widget/ImageView;

    .line 55
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->online_checkout_share_checkoutlink:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->shareLink:Landroid/view/View;

    .line 56
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->online_checkout_checkoutlink_url:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->checkoutLinkUrl:Lcom/squareup/noho/NohoRow;

    .line 57
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->online_checkout_checkoutlink_help_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->helpText:Lcom/squareup/widgets/MessageView;

    .line 59
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->online_checkout_error_message_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->errorMessageView:Lcom/squareup/noho/NohoMessageView;

    return-void
.end method

.method public static final synthetic access$getView$p(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;)Landroid/view/View;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    return-object p0
.end method

.method private final getActionBarTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 181
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title_with_money:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 182
    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "money"

    .line 181
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 183
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final updateCheckoutLinkScreen(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;)V
    .locals 9

    .line 102
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget-object v1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$updateCheckoutLinkScreen$1;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$updateCheckoutLinkScreen$1;

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 103
    new-instance v8, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v8}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 105
    new-instance v1, Lcom/squareup/resources/FixedText;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;->getAmount()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->getActionBarTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {v1, v2}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v8, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 107
    sget-object v2, Lcom/squareup/noho/NohoActionButtonStyle;->SECONDARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 108
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_new_sale:I

    invoke-direct {v1, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/resources/TextModel;

    .line 109
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;->getOnClickNewSaleButton()Lkotlin/jvm/functions/Function0;

    move-result-object v5

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, v8

    .line 106
    invoke-static/range {v1 .. v7}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 112
    invoke-virtual {v8}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->checkoutLinkScreenName:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;->getName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 116
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;->getQrCodeBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->qrCodeView:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;->getQrCodeBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->qrCodeView:Landroid/widget/ImageView;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->helpText:Lcom/squareup/widgets/MessageView;

    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_checkout_link_help_text:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->qrCodeView:Landroid/widget/ImageView;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->helpText:Lcom/squareup/widgets/MessageView;

    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_checkout_link_help_text_no_qr_code:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :goto_0
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->checkoutLinkUrl:Lcom/squareup/noho/NohoRow;

    .line 127
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;->getCheckoutLinkUrl()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    const v1, 0x3f4ccccd    # 0.8f

    .line 128
    invoke-static {v0, v1}, Lcom/squareup/noho/NohoRowUtilsKt;->constrainValueSize(Lcom/squareup/noho/NohoRow;F)V

    const/4 v1, 0x2

    .line 129
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v1, v2}, Lcom/squareup/noho/NohoRowUtilsKt;->setMaxLinesForValue(Lcom/squareup/noho/NohoRow;ILandroid/text/TextUtils$TruncateAt;)V

    const v1, 0x800005

    .line 130
    invoke-static {v0, v1}, Lcom/squareup/noho/NohoRowUtilsKt;->setHorizontalGravityForValue(Lcom/squareup/noho/NohoRow;I)V

    .line 131
    check-cast v0, Landroid/view/View;

    .line 196
    new-instance v1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$updateCheckoutLinkScreen$$inlined$with$lambda$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$updateCheckoutLinkScreen$$inlined$with$lambda$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->shareLink:Landroid/view/View;

    .line 203
    new-instance v1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$updateCheckoutLinkScreen$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$updateCheckoutLinkScreen$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final updateErrorScreen(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$ErrorScreenData;)V
    .locals 4

    .line 146
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$ErrorScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 152
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 147
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 149
    new-instance v2, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 150
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$ErrorScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 152
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->errorMessageView:Lcom/squareup/noho/NohoMessageView;

    .line 155
    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_error_screen_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 156
    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_error_screen_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 157
    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_error_screen_retry_text:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonText(I)V

    .line 158
    new-instance v1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$updateErrorScreen$$inlined$with$lambda$1;

    invoke-direct {v1, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner$updateErrorScreen$$inlined$with$lambda$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$ErrorScreenData;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    const-string v1, "debounce {\n        scree\u2026kTryAgainButton()\n      }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method

.method private final updateFeatureNotAvailableScreen(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$FeatureNotAvailableScreenData;)V
    .locals 4

    .line 165
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$FeatureNotAvailableScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 166
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 168
    new-instance v2, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 169
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$FeatureNotAvailableScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 171
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 173
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->errorMessageView:Lcom/squareup/noho/NohoMessageView;

    const/4 v0, 0x0

    .line 174
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 175
    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_feature_not_available_msg:I

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 176
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateLoadingScreen(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$LoadingScreenData;)V
    .locals 4

    .line 92
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$LoadingScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 93
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 95
    new-instance v2, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 96
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$LoadingScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 98
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->checkoutLinkScreenContainer:Landroid/view/ViewGroup;

    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 66
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->loadingScreenContainer:Landroid/view/ViewGroup;

    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 67
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->errorScreenContainer:Landroid/view/ViewGroup;

    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 70
    instance-of p2, p1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$LoadingScreenData;

    if-eqz p2, :cond_0

    .line 71
    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$LoadingScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->updateLoadingScreen(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$LoadingScreenData;)V

    .line 72
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->loadingScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 74
    :cond_0
    instance-of p2, p1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;

    if-eqz p2, :cond_1

    .line 75
    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->updateCheckoutLinkScreen(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->checkoutLinkScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 79
    :cond_1
    instance-of p2, p1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$ErrorScreenData;

    if-eqz p2, :cond_2

    .line 80
    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$ErrorScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->updateErrorScreen(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$ErrorScreenData;)V

    .line 81
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->errorScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 84
    :cond_2
    instance-of p2, p1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$FeatureNotAvailableScreenData;

    if-eqz p2, :cond_3

    .line 85
    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$FeatureNotAvailableScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->updateFeatureNotAvailableScreen(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$FeatureNotAvailableScreenData;)V

    .line 86
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->errorScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenLayoutRunner;->showRendering(Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
