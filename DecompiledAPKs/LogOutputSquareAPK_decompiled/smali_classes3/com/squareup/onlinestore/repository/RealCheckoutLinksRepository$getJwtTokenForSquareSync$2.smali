.class final Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getJwtTokenForSquareSync$2;
.super Ljava/lang/Object;
.source "RealCheckoutLinksRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->getJwtTokenForSquareSync()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/MaybeSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u0002 \u0004*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/onlinestore/api/squaresync/MultiPassAuthResponse;",
        "kotlin.jvm.PlatformType",
        "reqBody",
        "Lcom/squareup/onlinestore/api/squaresync/MultiPassAuthRequestBody;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getJwtTokenForSquareSync$2;->this$0:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/onlinestore/api/squaresync/MultiPassAuthRequestBody;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/api/squaresync/MultiPassAuthRequestBody;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/onlinestore/api/squaresync/MultiPassAuthResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "reqBody"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getJwtTokenForSquareSync$2;->this$0:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;

    invoke-static {v0}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->access$getWeeblySquareSyncService$p(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;)Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;->authenticateMultiPassSession(Lcom/squareup/onlinestore/api/squaresync/MultiPassAuthRequestBody;)Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService$MultiPassSessionAuthResponse;

    move-result-object p1

    .line 89
    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService$MultiPassSessionAuthResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 90
    invoke-virtual {p1}, Lio/reactivex/Single;->toMaybe()Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/onlinestore/api/squaresync/MultiPassAuthRequestBody;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getJwtTokenForSquareSync$2;->apply(Lcom/squareup/onlinestore/api/squaresync/MultiPassAuthRequestBody;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method
