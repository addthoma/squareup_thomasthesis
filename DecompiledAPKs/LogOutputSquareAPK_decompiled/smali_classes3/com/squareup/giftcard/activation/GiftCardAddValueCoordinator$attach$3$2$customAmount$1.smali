.class final Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$customAmount$1;
.super Ljava/lang/Object;
.source "GiftCardAddValueCoordinator.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->call(Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/protos/common/Money;",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$customAmount$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/String;)Lcom/squareup/protos/common/Money;
    .locals 2

    .line 175
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$customAmount$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getMoneyLocaleHelper$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/money/MoneyLocaleHelper;

    move-result-object v0

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/money/MoneyLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$customAmount$1;->call(Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method
