.class public final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;,
        Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\n\u0008\u0000\u0018\u0000 \u000f2\u00020\u0001:\u0002\u000f\u0010B#\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;",
        "",
        "state",
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;",
        "cardBalance",
        "Lcom/squareup/protos/common/Money;",
        "cardName",
        "",
        "(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V",
        "getCardBalance",
        "()Lcom/squareup/protos/common/Money;",
        "getCardName",
        "()Ljava/lang/String;",
        "getState",
        "()Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;",
        "Companion",
        "State",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;


# instance fields
.field private final cardBalance:Lcom/squareup/protos/common/Money;

.field private final cardName:Ljava/lang/String;

.field private final state:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;->Companion:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V
    .locals 0

    .line 607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;->state:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;->cardBalance:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;->cardName:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 607
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getCardBalance()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 609
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;->cardBalance:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getCardName()Ljava/lang/String;
    .locals 1

    .line 610
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;->cardName:Ljava/lang/String;

    return-object v0
.end method

.method public final getState()Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;
    .locals 1

    .line 608
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;->state:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;

    return-object v0
.end method
