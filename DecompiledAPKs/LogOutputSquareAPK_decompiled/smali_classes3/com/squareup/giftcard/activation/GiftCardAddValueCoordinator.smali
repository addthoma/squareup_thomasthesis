.class public final Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "GiftCardAddValueCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001Be\u0008\u0011\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u000e\u0008\u0001\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016Bq\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u000e\u0008\u0001\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u0018\u00a2\u0006\u0002\u0010\u001bJ\u0018\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u00102\u0006\u0010#\u001a\u00020$H\u0002J\u0010\u0010%\u001a\u00020!2\u0006\u0010&\u001a\u00020\'H\u0016R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "controller",
        "Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;",
        "res",
        "Lcom/squareup/util/Res;",
        "accountSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "errorsBar",
        "Lcom/squareup/ui/ErrorsBarPresenter;",
        "moneyLocaleHelper",
        "Lcom/squareup/money/MoneyLocaleHelper;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "shortMoneyFormatter",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "(Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;Lcom/squareup/permissions/EmployeeManagement;)V",
        "marketButtonFactory",
        "Lkotlin/Function1;",
        "Landroid/content/Context;",
        "Lcom/squareup/marketfont/MarketButton;",
        "(Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;Lkotlin/jvm/functions/Function1;)V",
        "cartMaxCents",
        "",
        "maxAmountCents",
        "minAmountCents",
        "addGiftCardToTransaction",
        "",
        "amount",
        "giftCard",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "attach",
        "view",
        "Landroid/view/View;",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cartMaxCents:J

.field private final controller:Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final marketButtonFactory:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/content/Context;",
            "Lcom/squareup/marketfont/MarketButton;",
            ">;"
        }
    .end annotation
.end field

.field private final maxAmountCents:J

.field private final minAmountCents:J

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final shortMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;Lcom/squareup/permissions/EmployeeManagement;)V
    .locals 12
    .param p8    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/Shorter;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "controller"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountSettings"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorsBar"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyLocaleHelper"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shortMoneyFormatter"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    move-object/from16 v1, p10

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    sget-object v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$1;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$1;

    move-object v11, v0

    check-cast v11, Lkotlin/jvm/functions/Function1;

    move-object v1, p0

    .line 80
    invoke-direct/range {v1 .. v11}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;-><init>(Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .param p8    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/Shorter;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/content/Context;",
            "+",
            "Lcom/squareup/marketfont/MarketButton;",
            ">;)V"
        }
    .end annotation

    const-string v0, "controller"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorsBar"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyLocaleHelper"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shortMoneyFormatter"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "marketButtonFactory"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->controller:Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p5, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    iput-object p6, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iput-object p7, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p8, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p9, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p10, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->marketButtonFactory:Lkotlin/jvm/functions/Function1;

    .line 84
    invoke-virtual {p3}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object p1

    const-string p2, "giftCardSettings"

    .line 85
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardActivationMaximum()J

    move-result-wide p2

    iput-wide p2, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->maxAmountCents:J

    .line 86
    invoke-virtual {p1}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardActivationMinimum()J

    move-result-wide p2

    iput-wide p2, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->minAmountCents:J

    .line 87
    invoke-virtual {p1}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardTotalPurchaseMaximum()Ljava/lang/Long;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->cartMaxCents:J

    .line 89
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ErrorsBarPresenter;->setMaxMessages(I)V

    return-void
.end method

.method public static final synthetic access$addGiftCardToTransaction(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/GiftCard;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1, p2}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->addGiftCardToTransaction(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/GiftCard;)V

    return-void
.end method

.method public static final synthetic access$getCartMaxCents$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)J
    .locals 2

    .line 51
    iget-wide v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->cartMaxCents:J

    return-wide v0
.end method

.method public static final synthetic access$getController$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->controller:Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;

    return-object p0
.end method

.method public static final synthetic access$getCurrencyCode$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method public static final synthetic access$getErrorsBar$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    return-object p0
.end method

.method public static final synthetic access$getMarketButtonFactory$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lkotlin/jvm/functions/Function1;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->marketButtonFactory:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method public static final synthetic access$getMaxAmountCents$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)J
    .locals 2

    .line 51
    iget-wide v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->maxAmountCents:J

    return-wide v0
.end method

.method public static final synthetic access$getMinAmountCents$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)J
    .locals 2

    .line 51
    iget-wide v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->minAmountCents:J

    return-wide v0
.end method

.method public static final synthetic access$getMoneyFormatter$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$getMoneyLocaleHelper$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/money/MoneyLocaleHelper;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getShortMoneyFormatter$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$getTransaction$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method private final addGiftCardToTransaction(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/GiftCard;)V
    .locals 3

    .line 239
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->hasGiftCardItemWithServerId(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    .line 241
    iget-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/giftcard/activation/R$string;->duplicate_gift_card_message:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string v0, ""

    .line 240
    invoke-virtual {p1, v0, p2}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->controller:Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/GiftCard;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    const-string v2, "giftCard.state"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinatorKt;->access$getActivityType(Lcom/squareup/protos/client/giftcards/GiftCard$State;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;->addGiftCardToTransaction(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;)V

    .line 247
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->controller:Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;->finishAddValueAndCloseActivationFlow()V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 12

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v3

    .line 94
    sget v0, Lcom/squareup/giftcard/activation/R$id;->gift_card_balance_contents:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/widget/LinearLayout;

    .line 95
    sget v0, Lcom/squareup/giftcard/activation/R$id;->gift_card_preset_amount_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/LinearLayout;

    .line 96
    sget v0, Lcom/squareup/giftcard/activation/R$id;->gift_card_custom_amount_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/FrameLayout;

    .line 98
    sget v0, Lcom/squareup/giftcard/activation/R$id;->gift_card_custom_amount_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 97
    move-object v6, v0

    check-cast v6, Lcom/squareup/widgets/OnScreenRectangleEditText;

    .line 99
    sget v0, Lcom/squareup/giftcard/activation/R$id;->gift_card_custom_amount_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/marketfont/MarketButton;

    .line 100
    sget v0, Lcom/squareup/giftcard/activation/R$id;->gift_card_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 101
    sget v1, Lcom/squareup/giftcard/activation/R$id;->invalid_gift_card:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 103
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->controller:Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;

    invoke-interface {v1}, Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;->giftCardName()Lrx/Observable;

    move-result-object v1

    .line 104
    new-instance v2, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$1;

    invoke-direct {v2, v3}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$1;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;)V

    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    const-string v2, "controller.giftCardName(\u2026AndText(BACK_ARROW, it) }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-static {v1, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 106
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v3, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 109
    new-instance v11, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    move-object v1, v11

    move-object v2, p0

    move-object v5, p1

    invoke-direct/range {v1 .. v10}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;-><init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;Landroid/widget/LinearLayout;Landroid/view/View;Lcom/squareup/widgets/OnScreenRectangleEditText;Lcom/squareup/marketfont/MarketButton;Landroid/widget/FrameLayout;Landroid/widget/LinearLayout;Lcom/squareup/marin/widgets/MarinGlyphMessage;)V

    check-cast v11, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v11}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 226
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$4;

    invoke-direct {v1, p0, v0}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$4;-><init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;Landroid/widget/ProgressBar;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 232
    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$5;

    invoke-direct {v0, p0}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$5;-><init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
