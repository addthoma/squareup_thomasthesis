.class public final Lcom/squareup/giftcard/GiftCardServiceHelper_Factory;
.super Ljava/lang/Object;
.source "GiftCardServiceHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/giftcard/GiftCardServiceHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardConverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/CardConverter;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/GiftCardService;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/GiftCardService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/CardConverter;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/giftcard/GiftCardServiceHelper_Factory;->giftCardServiceProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/giftcard/GiftCardServiceHelper_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/giftcard/GiftCardServiceHelper_Factory;->cardConverterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/giftcard/GiftCardServiceHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/GiftCardService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/CardConverter;",
            ">;)",
            "Lcom/squareup/giftcard/GiftCardServiceHelper_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/giftcard/GiftCardServiceHelper_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/giftcard/GiftCardServiceHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/payment/GiftCardService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/CardConverter;)Lcom/squareup/giftcard/GiftCardServiceHelper;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/giftcard/GiftCardServiceHelper;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/giftcard/GiftCardServiceHelper;-><init>(Lcom/squareup/server/payment/GiftCardService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/CardConverter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/giftcard/GiftCardServiceHelper;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/giftcard/GiftCardServiceHelper_Factory;->giftCardServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/payment/GiftCardService;

    iget-object v1, p0, Lcom/squareup/giftcard/GiftCardServiceHelper_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/giftcard/GiftCardServiceHelper_Factory;->cardConverterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/CardConverter;

    invoke-static {v0, v1, v2}, Lcom/squareup/giftcard/GiftCardServiceHelper_Factory;->newInstance(Lcom/squareup/server/payment/GiftCardService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/CardConverter;)Lcom/squareup/giftcard/GiftCardServiceHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/giftcard/GiftCardServiceHelper_Factory;->get()Lcom/squareup/giftcard/GiftCardServiceHelper;

    move-result-object v0

    return-object v0
.end method
