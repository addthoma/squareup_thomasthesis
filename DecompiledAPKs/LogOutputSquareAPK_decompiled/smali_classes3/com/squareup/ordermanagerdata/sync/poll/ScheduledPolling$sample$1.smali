.class final Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling$sample$1;
.super Ljava/lang/Object;
.source "ScheduledPolling.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;->sample()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "",
        "kotlin.jvm.PlatformType",
        "it",
        "Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;


# direct methods
.method constructor <init>(Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling$sample$1;->this$0:Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;)Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;->getValue()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;->getTimeUnit()Ljava/util/concurrent/TimeUnit;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling$sample$1;->this$0:Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;

    invoke-virtual {v2}, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;->getIntervalScheduler()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-static {v0, v1, p1, v2}, Lio/reactivex/Observable;->interval(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;

    invoke-virtual {p0, p1}, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling$sample$1;->apply(Lcom/squareup/ordermanagerdata/sync/poll/PollingInterval;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
