.class public final Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore$Companion;
.super Ljava/lang/Object;
.source "RealRemoteOrderStore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\u00020\u00048\u0000X\u0081T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0005\u0010\u0002R\u0016\u0010\u0006\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\"\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n8\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u000c\u0010\u0002\u001a\u0004\u0008\r\u0010\u000eR\"\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\n8\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0011\u0010\u0002\u001a\u0004\u0008\u0012\u0010\u000eR\"\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00100\n8\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0014\u0010\u0002\u001a\u0004\u0008\u0015\u0010\u000eR\u0014\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00170\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore$Companion;",
        "",
        "()V",
        "ACTIVE_ORDER_SEARCH_LIMIT",
        "",
        "ACTIVE_ORDER_SEARCH_LIMIT$annotations",
        "CLIENT_SUPPORT_BUILDER",
        "Lcom/squareup/protos/client/orders/ClientSupport$Builder;",
        "kotlin.jvm.PlatformType",
        "SUPPORTED_ACTIONS",
        "",
        "Lcom/squareup/protos/client/orders/Action$Type;",
        "SUPPORTED_ACTIONS$annotations",
        "getSUPPORTED_ACTIONS$impl_release",
        "()Ljava/util/List;",
        "SUPPORTED_ORDER_FEATURES",
        "Lcom/squareup/protos/client/orders/ClientSupport$Feature;",
        "SUPPORTED_ORDER_FEATURES$annotations",
        "getSUPPORTED_ORDER_FEATURES$impl_release",
        "SUPPORTED_ORDER_FEATURES_WITH_ECOM_DELIVERY",
        "SUPPORTED_ORDER_FEATURES_WITH_ECOM_DELIVERY$annotations",
        "getSUPPORTED_ORDER_FEATURES_WITH_ECOM_DELIVERY$impl_release",
        "SUPPORTED_ORDER_GROUPS",
        "Lcom/squareup/protos/client/orders/OrderGroup;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 405
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore$Companion;-><init>()V

    return-void
.end method

.method public static synthetic ACTIVE_ORDER_SEARCH_LIMIT$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic SUPPORTED_ACTIONS$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic SUPPORTED_ORDER_FEATURES$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic SUPPORTED_ORDER_FEATURES_WITH_ECOM_DELIVERY$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final getSUPPORTED_ACTIONS$impl_release()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/Action$Type;",
            ">;"
        }
    .end annotation

    .line 412
    invoke-static {}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->access$getSUPPORTED_ACTIONS$cp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getSUPPORTED_ORDER_FEATURES$impl_release()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/ClientSupport$Feature;",
            ">;"
        }
    .end annotation

    .line 416
    invoke-static {}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->access$getSUPPORTED_ORDER_FEATURES$cp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getSUPPORTED_ORDER_FEATURES_WITH_ECOM_DELIVERY$impl_release()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/ClientSupport$Feature;",
            ">;"
        }
    .end annotation

    .line 427
    invoke-static {}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->access$getSUPPORTED_ORDER_FEATURES_WITH_ECOM_DELIVERY$cp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
