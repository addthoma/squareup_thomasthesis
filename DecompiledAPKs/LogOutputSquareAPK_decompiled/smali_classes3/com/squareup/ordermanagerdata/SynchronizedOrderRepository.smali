.class public final Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;
.super Ljava/lang/Object;
.source "SynchronizedOrderRepository.kt"

# interfaces
.implements Lcom/squareup/ordermanagerdata/OrderRepository;
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;,
        Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSynchronizedOrderRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SynchronizedOrderRepository.kt\ncom/squareup/ordermanagerdata/SynchronizedOrderRepository\n*L\n1#1,603:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00aa\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0010\"\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 J2\u00020\u00012\u00020\u0002:\u0002JKB\'\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u001a\u0010\u0012\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00150\u00140\u0013H\u0016J\u0008\u0010\u0017\u001a\u00020\u0010H\u0016J2\u0010\u0018\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00140\u00192\u0006\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u001b\u001a\u00020\u001c2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u0015H\u0016J\u001a\u0010\u001f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00150\u00140\u0013H\u0016J\u000e\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0013H\u0016J.\u0010!\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00140\u00192\u0006\u0010\u001a\u001a\u00020\u00162\u0006\u0010\"\u001a\u00020#2\u0008\u0010$\u001a\u0004\u0018\u00010%H\u0016J\u001c\u0010&\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00140\u00192\u0006\u0010\'\u001a\u00020\rH\u0016J\u0016\u0010(\u001a\u0008\u0012\u0004\u0012\u00020)0\u00192\u0006\u0010*\u001a\u00020)H\u0002J\u0014\u0010+\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020,0\u00140\u0019H\u0016J\u0016\u0010-\u001a\u0008\u0012\u0004\u0012\u00020)0\u00192\u0006\u0010*\u001a\u00020)H\u0002J\u0014\u0010.\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020,0\u00140\u0019H\u0016J&\u0010/\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00140\u00192\u0006\u0010\u001a\u001a\u00020\u00162\u0008\u00100\u001a\u0004\u0018\u00010\rH\u0016J\"\u00101\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020,0\u00140\u00192\u000c\u00102\u001a\u0008\u0012\u0004\u0012\u00020\r03H\u0016J\u0010\u00104\u001a\u00020,2\u0006\u00105\u001a\u000206H\u0016J\u0008\u00107\u001a\u00020,H\u0016J\u0014\u00108\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020,0\u00140\u0019H\u0016J\u0014\u00109\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020,0\u00140\u0019H\u0016J\u0016\u0010:\u001a\u0008\u0012\u0004\u0012\u00020)0\u00192\u0006\u0010*\u001a\u00020)H\u0002J\"\u0010;\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00150\u00140\u00132\u0006\u0010<\u001a\u00020=H\u0016J4\u0010>\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00140\u00192\u0006\u0010\u001a\u001a\u00020\u00162\u0008\u0010$\u001a\u0004\u0018\u00010%2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u0015H\u0016J\u0016\u0010?\u001a\u0008\u0012\u0004\u0012\u00020)0\u00192\u0006\u0010<\u001a\u00020=H\u0002J\u001c\u0010@\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00150\u00140\u0013H\u0016J$\u0010A\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00140\u00192\u0006\u0010\u001a\u001a\u00020\u00162\u0006\u0010B\u001a\u00020CH\u0016J\u001a\u0010D\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00150\u00140\u0013H\u0016J\u001a\u0010E\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00150\u00140\u0013H\u0016J\u001a\u0010F\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00150\u00140\u0013H\u0016J8\u0010G\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u0002HI \u0011*\n\u0012\u0004\u0012\u0002HI\u0018\u00010H0H0\u0019\"\u0004\u0008\u0000\u0010I*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002HI0H0\u0019H\u0002R\u0010\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000e\u001a\u0010\u0012\u000c\u0012\n \u0011*\u0004\u0018\u00010\u00100\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006L"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;",
        "Lcom/squareup/ordermanagerdata/OrderRepository;",
        "Lmortar/Scoped;",
        "remoteOrderStore",
        "Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;",
        "localOrderStore",
        "Lcom/squareup/ordermanagerdata/local/LocalOrderStore;",
        "syncNotifier",
        "Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;Lcom/squareup/ordermanagerdata/local/LocalOrderStore;Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;Lcom/squareup/settings/server/Features;)V",
        "completedOrdersPaginationToken",
        "",
        "didRemoteOrderSyncSucceedRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "activeOrders",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "canFetchMoreCompletedOrders",
        "cancelOrder",
        "Lio/reactivex/Single;",
        "order",
        "cancelReason",
        "Lcom/squareup/ordermanagerdata/CancellationReason;",
        "lineItemQuantities",
        "Lcom/squareup/protos/client/orders/LineItemQuantity;",
        "completedOrders",
        "didRemoteOrderSyncSucceed",
        "editTracking",
        "fulfillment",
        "Lcom/squareup/orders/model/Order$Fulfillment;",
        "tracking",
        "Lcom/squareup/ordermanagerdata/TrackingInfo;",
        "getOrder",
        "orderId",
        "loadAllPagesOfSearchOrders",
        "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;",
        "searchInFlight",
        "loadNextPageOfCompletedOrders",
        "",
        "loadNextPageOfSearchOrders",
        "markAllOrdersAsKnown",
        "markOrderInProgress",
        "pickupTimeOverride",
        "markOrdersAsPrinted",
        "orderIds",
        "",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "refreshActiveOrders",
        "refreshSyncedOrders",
        "saveSearchOrdersToStore",
        "searchOrders",
        "query",
        "Lcom/squareup/ordermanagerdata/SearchQuery;",
        "shipOrder",
        "startSearch",
        "syncedOrders",
        "transitionOrder",
        "action",
        "Lcom/squareup/protos/client/orders/Action$Type;",
        "unknownOrders",
        "unprintedNewOrders",
        "upcomingOrders",
        "updateRemoteOrderSyncStatus",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "T",
        "Companion",
        "SearchInFlight",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$Companion;

.field public static final MAX_PAGES_FOR_SEARCH:I = 0xa


# instance fields
.field private completedOrdersPaginationToken:Ljava/lang/String;

.field private final didRemoteOrderSyncSucceedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final localOrderStore:Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

.field private final remoteOrderStore:Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

.field private final syncNotifier:Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->Companion:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;Lcom/squareup/ordermanagerdata/local/LocalOrderStore;Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "remoteOrderStore"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localOrderStore"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "syncNotifier"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->remoteOrderStore:Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

    iput-object p2, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->localOrderStore:Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

    iput-object p3, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->syncNotifier:Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;

    iput-object p4, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->features:Lcom/squareup/settings/server/Features;

    const/4 p1, 0x1

    .line 55
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(true)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->didRemoteOrderSyncSucceedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getCompletedOrdersPaginationToken$p(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)Ljava/lang/String;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->completedOrdersPaginationToken:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getDidRemoteOrderSyncSucceedRelay$p(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->didRemoteOrderSyncSucceedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getLocalOrderStore$p(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)Lcom/squareup/ordermanagerdata/local/LocalOrderStore;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->localOrderStore:Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

    return-object p0
.end method

.method public static final synthetic access$loadAllPagesOfSearchOrders(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;)Lio/reactivex/Single;
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->loadAllPagesOfSearchOrders(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$saveSearchOrdersToStore(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;)Lio/reactivex/Single;
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->saveSearchOrdersToStore(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setCompletedOrdersPaginationToken$p(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;Ljava/lang/String;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->completedOrdersPaginationToken:Ljava/lang/String;

    return-void
.end method

.method private final loadAllPagesOfSearchOrders(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;",
            ">;"
        }
    .end annotation

    .line 294
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->getHasMorePages()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    invoke-direct {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->loadNextPageOfSearchOrders(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadAllPagesOfSearchOrders$1;

    invoke-direct {v0, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadAllPagesOfSearchOrders$1;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "loadNextPageOfSearchOrde\u2026PagesOfSearchOrders(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 297
    :cond_0
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(searchInFlight)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method private final loadNextPageOfSearchOrders(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;",
            ">;"
        }
    .end annotation

    .line 305
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->remoteOrderStore:Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

    .line 306
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->getSearchQuery()Lcom/squareup/ordermanagerdata/SearchQuery;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->getPagToken()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 305
    invoke-interface {v0, v1, v2}, Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;->searchOrders(Lcom/squareup/ordermanagerdata/SearchQuery;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 308
    new-instance v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfSearchOrders$1;

    invoke-direct {v1, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfSearchOrders$1;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 320
    new-instance v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfSearchOrders$2;

    invoke-direct {v1, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfSearchOrders$2;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 323
    new-instance v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfSearchOrders$3;

    invoke-direct {v1, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfSearchOrders$3;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "remoteOrderStore.searchO\u2026ht.searchQuery)\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 306
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final saveSearchOrdersToStore(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;",
            ">;"
        }
    .end annotation

    .line 332
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->getLatestResult()Lcom/squareup/ordermanagerdata/ResultState;

    move-result-object v0

    .line 333
    instance-of v1, v0, Lcom/squareup/ordermanagerdata/ResultState$Success;

    if-eqz v1, :cond_1

    .line 334
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->localOrderStore:Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

    .line 336
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->getLatestResult()Lcom/squareup/ordermanagerdata/ResultState;

    move-result-object v1

    check-cast v1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {v1}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->getSearchQuery()Lcom/squareup/ordermanagerdata/SearchQuery;

    move-result-object v2

    .line 337
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->getPagesFetched()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    .line 335
    :goto_0
    invoke-interface {v0, v1, v2, v4}, Lcom/squareup/ordermanagerdata/local/LocalOrderStore;->saveSearchOrders(Ljava/util/List;Lcom/squareup/ordermanagerdata/SearchQuery;Z)Lio/reactivex/Completable;

    move-result-object v0

    .line 339
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    invoke-virtual {v0, p1}, Lio/reactivex/Completable;->andThen(Lio/reactivex/SingleSource;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "localOrderStore\n        \u2026gle.just(searchInFlight))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 341
    :cond_1
    instance-of v0, v0, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    if-eqz v0, :cond_2

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(searchInFlight)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final startSearch(Lcom/squareup/ordermanagerdata/SearchQuery;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/SearchQuery;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;",
            ">;"
        }
    .end annotation

    .line 270
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->remoteOrderStore:Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p1, v1, v2, v1}, Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore$DefaultImpls;->searchOrders$default(Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;Lcom/squareup/ordermanagerdata/SearchQuery;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    .line 271
    new-instance v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$startSearch$1;

    invoke-direct {v1, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$startSearch$1;-><init>(Lcom/squareup/ordermanagerdata/SearchQuery;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 282
    new-instance v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$startSearch$2;

    invoke-direct {v1, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$startSearch$2;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 285
    new-instance v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$startSearch$3;

    invoke-direct {v1, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$startSearch$3;-><init>(Lcom/squareup/ordermanagerdata/SearchQuery;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "remoteOrderStore.searchO\u2026rchError(query)\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final updateRemoteOrderSyncStatus(Lio/reactivex/Single;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;>;"
        }
    .end annotation

    .line 490
    new-instance v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$updateRemoteOrderSyncStatus$1;

    invoke-direct {v0, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$updateRemoteOrderSyncStatus$1;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "map {\n    didRemoteOrder\u2026HandleSuccess)\n    it\n  }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public activeOrders()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;>;"
        }
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->localOrderStore:Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

    .line 139
    invoke-interface {v0}, Lcom/squareup/ordermanagerdata/local/LocalOrderStore;->activeOrders()Lio/reactivex/Observable;

    move-result-object v0

    .line 140
    sget-object v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$activeOrders$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$activeOrders$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->access$mapToResultState(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object v0

    .line 141
    invoke-virtual {v0}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "localOrderStore\n        \u2026cError }\n        .share()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public canFetchMoreCompletedOrders()Z
    .locals 1

    .line 208
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->completedOrdersPaginationToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public cancelOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/List;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/ordermanagerdata/CancellationReason;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/LineItemQuantity;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelReason"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lineItemQuantities"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 454
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->remoteOrderStore:Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

    .line 455
    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;->cancelOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object p1

    .line 456
    invoke-direct {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->updateRemoteOrderSyncStatus(Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object p1

    .line 457
    sget-object p2, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$cancelOrder$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$cancelOrder$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 463
    new-instance p2, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$cancelOrder$2;

    invoke-direct {p2, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$cancelOrder$2;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "remoteOrderStore\n       \u2026lt)\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public completedOrders()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;>;"
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->localOrderStore:Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

    .line 146
    invoke-interface {v0}, Lcom/squareup/ordermanagerdata/local/LocalOrderStore;->completedOrders()Lio/reactivex/Observable;

    move-result-object v0

    .line 147
    sget-object v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$completedOrders$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$completedOrders$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->access$mapToResultState(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "localOrderStore\n        \u2026cError }\n        .share()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public didRemoteOrderSyncSucceed()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 211
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->didRemoteOrderSyncSucceedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public editTracking(Lcom/squareup/orders/model/Order;Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/ordermanagerdata/TrackingInfo;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/orders/model/Order$Fulfillment;",
            "Lcom/squareup/ordermanagerdata/TrackingInfo;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fulfillment"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->remoteOrderStore:Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

    .line 350
    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;->editTracking(Lcom/squareup/orders/model/Order;Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/ordermanagerdata/TrackingInfo;)Lio/reactivex/Single;

    move-result-object p1

    .line 351
    invoke-direct {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->updateRemoteOrderSyncStatus(Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object p1

    .line 352
    sget-object p2, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$editTracking$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$editTracking$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 358
    new-instance p2, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$editTracking$2;

    invoke-direct {p2, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$editTracking$2;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "remoteOrderStore\n       \u2026lt)\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getOrder(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    const-string v0, "orderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 479
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->remoteOrderStore:Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

    .line 480
    invoke-interface {v0, p1}, Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;->getOrder(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 481
    invoke-direct {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->updateRemoteOrderSyncStatus(Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object p1

    .line 482
    sget-object v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$getOrder$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$getOrder$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "remoteOrderStore\n       \u2026tus\n          )\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public loadNextPageOfCompletedOrders()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .line 240
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->remoteOrderStore:Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

    .line 241
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->completedOrdersPaginationToken:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;->completedOrders(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 242
    invoke-direct {p0, v0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->updateRemoteOrderSyncStatus(Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object v0

    .line 243
    new-instance v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfCompletedOrders$1;

    invoke-direct {v1, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfCompletedOrders$1;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 252
    new-instance v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfCompletedOrders$2;

    invoke-direct {v1, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfCompletedOrders$2;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "remoteOrderStore\n       \u2026it)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 241
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public markAllOrdersAsKnown()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .line 166
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->localOrderStore:Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

    .line 167
    invoke-interface {v0}, Lcom/squareup/ordermanagerdata/local/LocalOrderStore;->markAllOrdersAsKnown()Lio/reactivex/Completable;

    move-result-object v0

    .line 168
    sget-object v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$markAllOrdersAsKnown$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$markAllOrdersAsKnown$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->access$toResultStateSingle(Lio/reactivex/Completable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public markOrderInProgress(Lcom/squareup/orders/model/Order;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 374
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->remoteOrderStore:Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

    .line 375
    invoke-interface {v0, p1, p2}, Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;->markOrderInProgress(Lcom/squareup/orders/model/Order;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 376
    invoke-direct {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->updateRemoteOrderSyncStatus(Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object p1

    .line 377
    sget-object p2, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$markOrderInProgress$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$markOrderInProgress$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 383
    new-instance p2, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$markOrderInProgress$2;

    invoke-direct {p2, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$markOrderInProgress$2;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "remoteOrderStore\n       \u2026lt)\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public markOrdersAsPrinted(Ljava/util/Set;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    const-string v0, "orderIds"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->localOrderStore:Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

    .line 173
    invoke-interface {v0, p1}, Lcom/squareup/ordermanagerdata/local/LocalOrderStore;->markOrdersAsPrinted(Ljava/util/Set;)Lio/reactivex/Completable;

    move-result-object p1

    .line 174
    sget-object v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$markOrdersAsPrinted$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$markOrdersAsPrinted$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->access$toResultStateSingle(Lio/reactivex/Completable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->syncNotifier:Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;

    .line 81
    invoke-virtual {v0}, Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;->events()Lio/reactivex/Observable;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_APPLET_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "features.featureEnabled(\u2026.ORDERHUB_APPLET_ROLLOUT)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2TransformersKt;->gateBy(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 83
    new-instance v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$onEnterScope$1;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "syncNotifier\n        .ev\u2026hActiveOrders()\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 86
    invoke-static {v0, p1, v1, v2, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith$default(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public refreshActiveOrders()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .line 214
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->remoteOrderStore:Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

    invoke-interface {v0}, Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;->activeOrders()Lio/reactivex/Single;

    move-result-object v0

    .line 215
    invoke-direct {p0, v0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->updateRemoteOrderSyncStatus(Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object v0

    .line 216
    sget-object v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$refreshActiveOrders$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$refreshActiveOrders$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 224
    new-instance v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$refreshActiveOrders$2;

    invoke-direct {v1, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$refreshActiveOrders$2;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "remoteOrderStore.activeO\u2026it)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public refreshSyncedOrders()Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .line 178
    sget-object v0, Lcom/squareup/util/rx2/Singles;->INSTANCE:Lcom/squareup/util/rx2/Singles;

    .line 179
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->remoteOrderStore:Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

    invoke-interface {v1}, Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;->activeOrders()Lio/reactivex/Single;

    move-result-object v1

    check-cast v1, Lio/reactivex/SingleSource;

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->remoteOrderStore:Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v2, v3, v4, v3}, Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore$DefaultImpls;->completedOrders$default(Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object v2

    check-cast v2, Lio/reactivex/SingleSource;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Singles;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;)Lio/reactivex/Single;

    move-result-object v0

    .line 180
    new-instance v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$refreshSyncedOrders$1;

    invoke-direct {v1, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$refreshSyncedOrders$1;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 191
    new-instance v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$refreshSyncedOrders$2;

    invoke-direct {v1, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$refreshSyncedOrders$2;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "Singles\n        .zip(rem\u2026it)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public searchOrders(Lcom/squareup/ordermanagerdata/SearchQuery;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/SearchQuery;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;>;"
        }
    .end annotation

    const-string v0, "query"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-direct {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->startSearch(Lcom/squareup/ordermanagerdata/SearchQuery;)Lio/reactivex/Single;

    move-result-object p1

    .line 114
    new-instance v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$searchOrders$1;

    invoke-direct {v0, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$searchOrders$1;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 117
    new-instance v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$searchOrders$2;

    invoke-direct {v0, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$searchOrders$2;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMapObservable(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "startSearch(query)\n     \u2026e()\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public shipOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/util/List;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/ordermanagerdata/TrackingInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/LineItemQuantity;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lineItemQuantities"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 425
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->remoteOrderStore:Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

    .line 426
    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;->shipOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object p1

    .line 427
    invoke-direct {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->updateRemoteOrderSyncStatus(Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object p1

    .line 428
    sget-object p2, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$shipOrder$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$shipOrder$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 434
    new-instance p2, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$shipOrder$2;

    invoke-direct {p2, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$shipOrder$2;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "remoteOrderStore\n       \u2026lt)\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public syncedOrders()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;>;"
        }
    .end annotation

    .line 93
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 95
    invoke-virtual {p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->upcomingOrders()Lio/reactivex/Observable;

    move-result-object v1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->activeOrders()Lio/reactivex/Observable;

    move-result-object v2

    .line 97
    invoke-virtual {p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->completedOrders()Lio/reactivex/Observable;

    move-result-object v3

    .line 94
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 99
    sget-object v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$syncedOrders$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$syncedOrders$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables\n        .com\u2026       }\n        .share()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public transitionOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action$Type;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/protos/client/orders/Action$Type;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 399
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->remoteOrderStore:Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

    .line 400
    invoke-interface {v0, p1, p2}, Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;->transitionOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action$Type;)Lio/reactivex/Single;

    move-result-object p1

    .line 401
    invoke-direct {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->updateRemoteOrderSyncStatus(Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object p1

    .line 402
    sget-object p2, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$transitionOrder$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$transitionOrder$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 408
    new-instance p2, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$transitionOrder$2;

    invoke-direct {p2, p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$transitionOrder$2;-><init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "remoteOrderStore\n       \u2026lt)\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public unknownOrders()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;>;"
        }
    .end annotation

    .line 159
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->localOrderStore:Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

    .line 160
    invoke-interface {v0}, Lcom/squareup/ordermanagerdata/local/LocalOrderStore;->unknownOrders()Lio/reactivex/Observable;

    move-result-object v0

    .line 161
    sget-object v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$unknownOrders$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$unknownOrders$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->access$mapToResultState(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object v0

    .line 162
    invoke-virtual {v0}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "localOrderStore\n        \u2026cError }\n        .share()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public unprintedNewOrders()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;>;"
        }
    .end annotation

    .line 152
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->localOrderStore:Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

    .line 153
    invoke-interface {v0}, Lcom/squareup/ordermanagerdata/local/LocalOrderStore;->unprintedNewOrders()Lio/reactivex/Observable;

    move-result-object v0

    .line 154
    sget-object v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$unprintedNewOrders$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$unprintedNewOrders$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->access$mapToResultState(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "localOrderStore\n        \u2026cError }\n        .share()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public upcomingOrders()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;>;"
        }
    .end annotation

    .line 131
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->localOrderStore:Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

    .line 132
    invoke-interface {v0}, Lcom/squareup/ordermanagerdata/local/LocalOrderStore;->upcomingOrders()Lio/reactivex/Observable;

    move-result-object v0

    .line 133
    sget-object v1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$upcomingOrders$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$upcomingOrders$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->access$mapToResultState(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "localOrderStore\n        \u2026cError }\n        .share()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
