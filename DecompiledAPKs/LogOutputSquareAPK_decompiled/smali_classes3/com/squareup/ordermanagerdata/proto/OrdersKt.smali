.class public final Lcom/squareup/ordermanagerdata/proto/OrdersKt;
.super Ljava/lang/Object;
.source "Orders.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrders.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Orders.kt\ncom/squareup/ordermanagerdata/proto/OrdersKt\n*L\n1#1,57:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010 \n\u0002\u0008\u0003\"$\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0005\"\u0015\u0010\u0006\u001a\u00020\u0007*\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\"\u0015\u0010\u000b\u001a\u00020\u000c*\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000e\"\u0017\u0010\u000f\u001a\u0004\u0018\u00010\u0010*\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0012\"\u0015\u0010\u0013\u001a\u00020\u0007*\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\n\"\u0015\u0010\u0015\u001a\u00020\u0002*\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017\"\u001b\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0019*\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u001b\u00a8\u0006\u001c"
    }
    d2 = {
        "FULFILLMENT_COMPARATOR",
        "Ljava/util/Comparator;",
        "Lcom/squareup/orders/model/Order$Fulfillment;",
        "Lkotlin/Comparator;",
        "getFULFILLMENT_COMPARATOR",
        "()Ljava/util/Comparator;",
        "closedAtDate",
        "",
        "Lcom/squareup/orders/model/Order;",
        "getClosedAtDate",
        "(Lcom/squareup/orders/model/Order;)Ljava/lang/String;",
        "displayState",
        "Lcom/squareup/protos/client/orders/OrderDisplayStateData;",
        "getDisplayState",
        "(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;",
        "group",
        "Lcom/squareup/protos/client/orders/OrderGroup;",
        "getGroup",
        "(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;",
        "placedAtDate",
        "getPlacedAtDate",
        "primaryFulfillment",
        "getPrimaryFulfillment",
        "(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;",
        "sortedFulfillments",
        "",
        "getSortedFulfillments",
        "(Lcom/squareup/orders/model/Order;)Ljava/util/List;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final FULFILLMENT_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/orders/model/Order$Fulfillment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 56
    new-instance v0, Lcom/squareup/ordermanagerdata/proto/OrdersKt$$special$$inlined$compareBy$1;

    invoke-direct {v0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt$$special$$inlined$compareBy$1;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    sput-object v0, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->FULFILLMENT_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method public static final getClosedAtDate(Lcom/squareup/orders/model/Order;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$closedAtDate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/orders/model/Order;->closed_at:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/orders/model/Order;->updated_at:Ljava/lang/String;

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPlacedAtDate(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public static final getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;
    .locals 1

    const-string v0, "$this$displayState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    iget-object p0, p0, Lcom/squareup/orders/model/Order;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    iget-object p0, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    const-string v0, "ext_order_client_details.order_display_state_data"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getFULFILLMENT_COMPARATOR()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/squareup/orders/model/Order$Fulfillment;",
            ">;"
        }
    .end annotation

    .line 56
    sget-object v0, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->FULFILLMENT_COMPARATOR:Ljava/util/Comparator;

    return-object v0
.end method

.method public static final getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;
    .locals 1

    const-string v0, "$this$group"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object p0, p0, Lcom/squareup/orders/model/Order;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->groups:Ljava/util/List;

    if-eqz p0, :cond_0

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/orders/OrderGroup;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getPlacedAtDate(Lcom/squareup/orders/model/Order;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$placedAtDate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt;->getPlacedAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/orders/model/Order;->created_at:Ljava/lang/String;

    const-string p0, "created_at"

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public static final getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;
    .locals 1

    const-string v0, "$this$primaryFulfillment"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object p0, p0, Lcom/squareup/orders/model/Order;->fulfillments:Ljava/util/List;

    const-string v0, "fulfillments"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    .line 46
    sget-object v0, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->FULFILLMENT_COMPARATOR:Ljava/util/Comparator;

    .line 45
    invoke-static {p0, v0}, Lkotlin/collections/CollectionsKt;->minWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 44
    check-cast p0, Lcom/squareup/orders/model/Order$Fulfillment;

    return-object p0

    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final getSortedFulfillments(Lcom/squareup/orders/model/Order;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Fulfillment;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$sortedFulfillments"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object p0, p0, Lcom/squareup/orders/model/Order;->fulfillments:Ljava/util/List;

    const-string v0, "fulfillments"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    .line 53
    sget-object v0, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->FULFILLMENT_COMPARATOR:Ljava/util/Comparator;

    .line 52
    invoke-static {p0, v0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method
