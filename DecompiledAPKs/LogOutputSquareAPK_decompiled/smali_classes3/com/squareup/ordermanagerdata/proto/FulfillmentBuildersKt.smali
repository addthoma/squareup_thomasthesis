.class public final Lcom/squareup/ordermanagerdata/proto/FulfillmentBuildersKt;
.super Ljava/lang/Object;
.source "FulfillmentBuilders.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFulfillmentBuilders.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FulfillmentBuilders.kt\ncom/squareup/ordermanagerdata/proto/FulfillmentBuildersKt\n+ 2 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,92:1\n132#2,3:93\n132#2,3:96\n132#2,3:99\n132#2,3:102\n*E\n*S KotlinDebug\n*F\n+ 1 FulfillmentBuilders.kt\ncom/squareup/ordermanagerdata/proto/FulfillmentBuildersKt\n*L\n35#1,3:93\n42#1,3:96\n49#1,3:99\n74#1,3:102\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0008\u0010\u0000\u001a\u0004\u0018\u00010\u0002\u001a\u0014\u0010\u0003\u001a\u00020\u0001*\u00020\u00012\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "note",
        "Lcom/squareup/orders/model/Order$Fulfillment$Builder;",
        "",
        "trackingInfo",
        "Lcom/squareup/ordermanagerdata/TrackingInfo;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final note(Lcom/squareup/orders/model/Order$Fulfillment$Builder;Ljava/lang/String;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 3

    const-string v0, "$this$note"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-eqz v0, :cond_8

    sget-object v1, Lcom/squareup/ordermanagerdata/proto/FulfillmentBuildersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    const-string v2, "null cannot be cast to non-null type B"

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_8

    .line 48
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 49
    :cond_0
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    move-result-object v0

    :goto_0
    check-cast v0, Lcom/squareup/wire/Message;

    .line 100
    invoke-virtual {v0}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v1, v0

    check-cast v1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;

    .line 50
    invoke-virtual {v1, p1}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->note(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;

    .line 101
    invoke-virtual {v0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p1

    .line 48
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    .line 52
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->managed_delivery_details(Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    move-result-object p0

    const-string p1, "managed_delivery_details(managedDeliveryDetails)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    .line 100
    :cond_1
    new-instance p0, Lkotlin/TypeCastException;

    invoke-direct {p0, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 42
    :cond_2
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    move-result-object v0

    :goto_1
    check-cast v0, Lcom/squareup/wire/Message;

    .line 97
    invoke-virtual {v0}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v0

    if-eqz v0, :cond_4

    move-object v1, v0

    check-cast v1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;

    .line 43
    invoke-virtual {v1, p1}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->note(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;

    .line 98
    invoke-virtual {v0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p1

    .line 41
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    .line 45
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->pickup_details(Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    move-result-object p0

    const-string p1, "pickup_details(pickupDetails)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    .line 97
    :cond_4
    new-instance p0, Lkotlin/TypeCastException;

    invoke-direct {p0, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 35
    :cond_5
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz v0, :cond_6

    goto :goto_2

    :cond_6
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    move-result-object v0

    :goto_2
    check-cast v0, Lcom/squareup/wire/Message;

    .line 94
    invoke-virtual {v0}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v0

    if-eqz v0, :cond_7

    move-object v1, v0

    check-cast v1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    .line 36
    invoke-virtual {v1, p1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->shipping_note(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    .line 95
    invoke-virtual {v0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p1

    .line 34
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    .line 38
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->shipment_details(Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    move-result-object p0

    const-string p1, "shipment_details(shipmentDetails)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_3
    return-object p0

    .line 94
    :cond_7
    new-instance p0, Lkotlin/TypeCastException;

    invoke-direct {p0, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 55
    :cond_8
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Fulfillment with type \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, "\" does not support notes."

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final trackingInfo(Lcom/squareup/orders/model/Order$Fulfillment$Builder;Lcom/squareup/ordermanagerdata/TrackingInfo;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 3

    const-string v0, "$this$trackingInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-eqz v0, :cond_3

    sget-object v1, Lcom/squareup/ordermanagerdata/proto/FulfillmentBuildersKt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 74
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    move-result-object v0

    :goto_0
    check-cast v0, Lcom/squareup/wire/Message;

    .line 103
    invoke-virtual {v0}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v0

    if-eqz v0, :cond_2

    move-object v1, v0

    check-cast v1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    if-nez p1, :cond_1

    const/4 p1, 0x0

    .line 76
    invoke-virtual {v1, p1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->carrier(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    .line 77
    invoke-virtual {v1, p1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->tracking_number(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    .line 78
    invoke-virtual {v1, p1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->tracking_url(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto :goto_1

    .line 80
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/TrackingInfo;->getCarrierName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->carrier(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    .line 81
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/TrackingInfo;->getTrackingNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->tracking_number(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    .line 82
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/TrackingInfo;->getTrackingUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->tracking_url(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    .line 104
    :goto_1
    invoke-virtual {v0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p1

    .line 73
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    .line 85
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->shipment_details(Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    move-result-object p0

    const-string p1, "shipment_details(shipmentDetails)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    .line 103
    :cond_2
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type B"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 88
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Fulfillment with type \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, "\" doesn\'t support tracking info."

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
