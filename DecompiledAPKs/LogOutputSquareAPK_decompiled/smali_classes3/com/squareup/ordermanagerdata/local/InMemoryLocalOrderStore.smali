.class public final Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;
.super Ljava/lang/Object;
.source "InMemoryLocalOrderStore.kt"

# interfaces
.implements Lcom/squareup/ordermanagerdata/local/LocalOrderStore;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInMemoryLocalOrderStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InMemoryLocalOrderStore.kt\ncom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,528:1\n1550#2,3:529\n*E\n*S KotlinDebug\n*F\n+ 1 InMemoryLocalOrderStore.kt\ncom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore\n*L\n145#1,3:529\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\"\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u001b\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\u0014\u0010\u0019\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00080\u001aH\u0016J\u0014\u0010\u001b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00080\u001aH\u0016J\u0018\u0010\u001c\u001a\u00020\u001d2\u000e\u0010\u001e\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010 0\u001fH\u0002J\u0008\u0010!\u001a\u00020\u001dH\u0016J\u001a\u0010\"\u001a\u00020\u001d2\u0010\u0010#\u001a\u000c\u0012\u0008\u0012\u00060\u0012j\u0002`\u00130$H\u0016J\u0014\u0010%\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00080\u001aH\u0016J&\u0010&\u001a\u00020\u001d2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010\'\u001a\u00020\u000f2\u0006\u0010(\u001a\u00020\rH\u0016J\u0014\u0010)\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00080\u001aH\u0016J\u0014\u0010*\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00080\u001aH\u0016J\u0014\u0010+\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00080\u001aH\u0016J\u0010\u0010,\u001a\u00020\u001d2\u0006\u0010-\u001a\u00020\tH\u0016J\u001e\u0010.\u001a\u00020\u001d2\u000c\u0010/\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u00100\u001a\u000201H\u0016J\u0010\u00102\u001a\u0002032\u0006\u0010-\u001a\u00020\tH\u0002R(\u0010\u0006\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\t \n*\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u00080\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u000b\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\t \n*\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u00080\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0010\u001a\u0012\u0012\u0008\u0012\u00060\u0012j\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u0015\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\t \n*\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u00080\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u0016\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\t \n*\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u00080\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u0017\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\t \n*\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u00080\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u0018\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\t \n*\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u00080\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;",
        "Lcom/squareup/ordermanagerdata/local/LocalOrderStore;",
        "rpcScheduler",
        "Lio/reactivex/Scheduler;",
        "mainThread",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;)V",
        "activeOrdersRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "kotlin.jvm.PlatformType",
        "completedOrdersRelay",
        "hasSynced",
        "",
        "mostRecentSearchQuery",
        "Lcom/squareup/ordermanagerdata/SearchQuery;",
        "orders",
        "",
        "",
        "Lcom/squareup/ordermanagerdata/local/OrderId;",
        "Lcom/squareup/ordermanagerdata/local/WrappedOrder;",
        "searchResultOrdersRelay",
        "unknownNewOrdersRelay",
        "unprintedNewOrdersRelay",
        "upcomingOrdersRelay",
        "activeOrders",
        "Lio/reactivex/Observable;",
        "completedOrders",
        "createCompletable",
        "Lio/reactivex/Completable;",
        "block",
        "Lkotlin/Function0;",
        "",
        "markAllOrdersAsKnown",
        "markOrdersAsPrinted",
        "orderIds",
        "",
        "ordersFromMostRecentSearch",
        "saveSearchOrders",
        "query",
        "isPaginationResult",
        "unknownOrders",
        "unprintedNewOrders",
        "upcomingOrders",
        "updateOrder",
        "order",
        "updateOrders",
        "updatedOrders",
        "updateMode",
        "Lcom/squareup/ordermanagerdata/local/UpdateMode;",
        "updateSearchResultsWithOrder",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final activeOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation
.end field

.field private final completedOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation
.end field

.field private hasSynced:Z

.field private final mainThread:Lio/reactivex/Scheduler;

.field private mostRecentSearchQuery:Lcom/squareup/ordermanagerdata/SearchQuery;

.field private final orders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ordermanagerdata/local/WrappedOrder;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcScheduler:Lio/reactivex/Scheduler;

.field private final searchResultOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation
.end field

.field private final unknownNewOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation
.end field

.field private final unprintedNewOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation
.end field

.field private final upcomingOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p1    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/ordermanagerdata/local/Single;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "rpcScheduler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->rpcScheduler:Lio/reactivex/Scheduler;

    iput-object p2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->mainThread:Lio/reactivex/Scheduler;

    .line 52
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->orders:Ljava/util/Map;

    .line 53
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026List<Order>>(emptyList())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->upcomingOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 54
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->activeOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 55
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->completedOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 56
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->unknownNewOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 57
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->unprintedNewOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 58
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->searchResultOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getActiveOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->activeOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getCompletedOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->completedOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getHasSynced$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Z
    .locals 0

    .line 42
    iget-boolean p0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->hasSynced:Z

    return p0
.end method

.method public static final synthetic access$getMostRecentSearchQuery$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/squareup/ordermanagerdata/SearchQuery;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->mostRecentSearchQuery:Lcom/squareup/ordermanagerdata/SearchQuery;

    return-object p0
.end method

.method public static final synthetic access$getOrders$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Ljava/util/Map;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->orders:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$getSearchResultOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->searchResultOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getUnknownNewOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->unknownNewOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getUnprintedNewOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->unprintedNewOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getUpcomingOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->upcomingOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$setHasSynced$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Z)V
    .locals 0

    .line 42
    iput-boolean p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->hasSynced:Z

    return-void
.end method

.method public static final synthetic access$setMostRecentSearchQuery$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Lcom/squareup/ordermanagerdata/SearchQuery;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->mostRecentSearchQuery:Lcom/squareup/ordermanagerdata/SearchQuery;

    return-void
.end method

.method public static final synthetic access$updateSearchResultsWithOrder(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Lcom/squareup/orders/model/Order;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->updateSearchResultsWithOrder(Lcom/squareup/orders/model/Order;)V

    return-void
.end method

.method private final createCompletable(Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Ljava/lang/Object;",
            ">;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 331
    new-instance v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt$sam$java_util_concurrent_Callable$0;

    invoke-direct {v0, p1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt$sam$java_util_concurrent_Callable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    move-object p1, v0

    :cond_0
    check-cast p1, Ljava/util/concurrent/Callable;

    invoke-static {p1}, Lio/reactivex/Completable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Completable;

    move-result-object p1

    .line 332
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->rpcScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Completable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object p1

    .line 333
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->mainThread:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Completable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object p1

    const-string v0, "Completable\n        .fro\u2026   .observeOn(mainThread)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final updateSearchResultsWithOrder(Lcom/squareup/orders/model/Order;)V
    .locals 5

    .line 145
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->searchResultOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_4

    check-cast v0, Ljava/lang/Iterable;

    .line 529
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 530
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orders/model/Order;

    .line 145
    iget-object v1, v1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    iget-object v4, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v3, 0x1

    :cond_2
    :goto_0
    if-ne v3, v2, :cond_4

    .line 146
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->mostRecentSearchQuery:Lcom/squareup/ordermanagerdata/SearchQuery;

    if-eqz v0, :cond_4

    .line 147
    invoke-static {p1, v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$inSameGroupAsQuery(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/SearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 148
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->searchResultOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->acceptWithUpdatedOrder$default(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;Ljava/util/Comparator;ILjava/lang/Object;)V

    goto :goto_1

    .line 150
    :cond_3
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->searchResultOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v0, p1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$acceptWithRemovedOrder(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;)V

    :cond_4
    :goto_1
    return-void
.end method


# virtual methods
.method public activeOrders()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->activeOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 77
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->mainThread:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "activeOrdersRelay\n      .observeOn(mainThread)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public completedOrders()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->completedOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 80
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->mainThread:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "completedOrdersRelay\n      .observeOn(mainThread)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public markAllOrdersAsKnown()Lio/reactivex/Completable;
    .locals 1

    .line 299
    new-instance v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markAllOrdersAsKnown$1;

    invoke-direct {v0, p0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markAllOrdersAsKnown$1;-><init>(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p0, v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->createCompletable(Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method

.method public markOrdersAsPrinted(Ljava/util/Set;)Lio/reactivex/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    const-string v0, "orderIds"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 310
    new-instance v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markOrdersAsPrinted$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$markOrdersAsPrinted$1;-><init>(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Ljava/util/Set;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p0, v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->createCompletable(Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public ordersFromMostRecentSearch()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->searchResultOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 94
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->mainThread:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "searchResultOrdersRelay\n\u2026   .observeOn(mainThread)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public saveSearchOrders(Ljava/util/List;Lcom/squareup/ordermanagerdata/SearchQuery;Z)Lio/reactivex/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Lcom/squareup/ordermanagerdata/SearchQuery;",
            "Z)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    const-string v0, "orders"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "query"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 281
    new-instance v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;-><init>(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Lcom/squareup/ordermanagerdata/SearchQuery;ZLjava/util/List;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p0, v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->createCompletable(Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public unknownOrders()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->unknownNewOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 84
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->mainThread:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string/jumbo v1, "unknownNewOrdersRelay\n  \u2026   .observeOn(mainThread)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public unprintedNewOrders()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->unprintedNewOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 89
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->mainThread:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string/jumbo v1, "unprintedNewOrdersRelay\n\u2026   .observeOn(mainThread)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public upcomingOrders()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    .line 73
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->upcomingOrdersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 74
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->mainThread:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string/jumbo v1, "upcomingOrdersRelay\n      .observeOn(mainThread)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public updateOrder(Lcom/squareup/orders/model/Order;)Lio/reactivex/Completable;
    .locals 1

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    new-instance v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrder$1;-><init>(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Lcom/squareup/orders/model/Order;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p0, v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->createCompletable(Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public updateOrders(Ljava/util/List;Lcom/squareup/ordermanagerdata/local/UpdateMode;)Lio/reactivex/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Lcom/squareup/ordermanagerdata/local/UpdateMode;",
            ")",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    const-string/jumbo v0, "updatedOrders"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "updateMode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    new-instance v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;-><init>(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Ljava/util/List;Lcom/squareup/ordermanagerdata/local/UpdateMode;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p0, v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->createCompletable(Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;

    move-result-object p1

    .line 273
    new-instance p2, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$2;

    invoke-direct {p2, p0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$2;-><init>(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)V

    check-cast p2, Lio/reactivex/functions/Action;

    invoke-virtual {p1, p2}, Lio/reactivex/Completable;->doOnComplete(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object p1

    const-string p2, "createCompletable {\n    \u2026lete { hasSynced = true }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
