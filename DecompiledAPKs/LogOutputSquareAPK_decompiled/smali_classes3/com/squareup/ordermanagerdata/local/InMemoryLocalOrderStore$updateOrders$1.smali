.class final Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;
.super Lkotlin/jvm/internal/Lambda;
.source "InMemoryLocalOrderStore.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->updateOrders(Ljava/util/List;Lcom/squareup/ordermanagerdata/local/UpdateMode;)Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInMemoryLocalOrderStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InMemoryLocalOrderStore.kt\ncom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,528:1\n1642#2,2:529\n1642#2,2:531\n1642#2,2:533\n*E\n*S KotlinDebug\n*F\n+ 1 InMemoryLocalOrderStore.kt\ncom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1\n*L\n174#1,2:529\n219#1,2:531\n249#1,2:533\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $updateMode:Lcom/squareup/ordermanagerdata/local/UpdateMode;

.field final synthetic $updatedOrders:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;


# direct methods
.method constructor <init>(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Ljava/util/List;Lcom/squareup/ordermanagerdata/local/UpdateMode;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    iput-object p2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->$updatedOrders:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->$updateMode:Lcom/squareup/ordermanagerdata/local/UpdateMode;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 42
    invoke-virtual {p0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 17

    move-object/from16 v0, p0

    .line 161
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 162
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 163
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 164
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 165
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 171
    new-instance v6, Ljava/util/LinkedHashMap;

    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    .line 172
    iget-object v7, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v7}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getHasSynced$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Z

    move-result v7

    const/4 v8, 0x1

    xor-int/2addr v7, v8

    .line 174
    iget-object v9, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->$updatedOrders:Ljava/util/List;

    check-cast v9, Ljava/lang/Iterable;

    .line 529
    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_b

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/squareup/orders/model/Order;

    .line 175
    sget-object v15, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->Companion:Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;

    .line 176
    iget-object v10, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v10}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getOrders$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Ljava/util/Map;

    move-result-object v10

    iget-object v8, v14, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-interface {v10, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    .line 175
    invoke-virtual {v15, v8, v14, v7}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;->create(Lcom/squareup/ordermanagerdata/local/WrappedOrder;Lcom/squareup/orders/model/Order;Z)Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;

    move-result-object v8

    .line 181
    move-object v10, v6

    check-cast v10, Ljava/util/Map;

    iget-object v15, v14, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    move/from16 v16, v7

    const-string v7, "order.id"

    invoke-static {v15, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->getUpdatedWrappedOrder()Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    move-result-object v7

    invoke-interface {v10, v15, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    invoke-static {v14}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v7

    if-eqz v7, :cond_a

    sget-object v10, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v7}, Lcom/squareup/protos/client/orders/OrderGroup;->ordinal()I

    move-result v7

    aget v7, v10, v7

    const/4 v10, 0x1

    if-eq v7, v10, :cond_5

    const/4 v15, 0x2

    if-eq v7, v15, :cond_2

    const/4 v15, 0x3

    if-ne v7, v15, :cond_a

    .line 193
    invoke-virtual {v3, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-nez v13, :cond_1

    .line 194
    invoke-virtual {v8}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->getOrderUpdated()Z

    move-result v7

    if-eqz v7, :cond_0

    goto :goto_1

    :cond_0
    const/4 v7, 0x0

    goto :goto_2

    :cond_1
    :goto_1
    const/4 v7, 0x1

    :goto_2
    move v13, v7

    goto :goto_7

    .line 189
    :cond_2
    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-nez v12, :cond_4

    .line 190
    invoke-virtual {v8}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->getOrderUpdated()Z

    move-result v7

    if-eqz v7, :cond_3

    goto :goto_3

    :cond_3
    const/4 v7, 0x0

    goto :goto_4

    :cond_4
    :goto_3
    const/4 v7, 0x1

    :goto_4
    move v12, v7

    goto :goto_7

    .line 185
    :cond_5
    invoke-virtual {v1, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-nez v11, :cond_7

    .line 186
    invoke-virtual {v8}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->getOrderUpdated()Z

    move-result v7

    if-eqz v7, :cond_6

    goto :goto_5

    :cond_6
    const/4 v7, 0x0

    goto :goto_6

    :cond_7
    :goto_5
    const/4 v7, 0x1

    :goto_6
    move v11, v7

    .line 199
    :goto_7
    invoke-virtual {v8}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnknownOrder()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 200
    invoke-virtual {v8}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->getUpdatedWrappedOrder()Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/squareup/ordermanagerdata/local/WrappedOrder;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    :cond_8
    invoke-virtual {v8}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnprintedNewOrder()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 203
    invoke-virtual {v8}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->getUpdatedWrappedOrder()Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/squareup/ordermanagerdata/local/WrappedOrder;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    move/from16 v7, v16

    const/4 v8, 0x1

    goto/16 :goto_0

    .line 196
    :cond_a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid order group "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v14}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    .line 207
    :cond_b
    iget-object v7, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->$updateMode:Lcom/squareup/ordermanagerdata/local/UpdateMode;

    .line 208
    sget-object v8, Lcom/squareup/ordermanagerdata/local/UpdateMode$RefreshAllOrders;->INSTANCE:Lcom/squareup/ordermanagerdata/local/UpdateMode$RefreshAllOrders;

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    const/4 v9, 0x0

    if-eqz v8, :cond_d

    .line 210
    iget-object v7, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v7}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getOrders$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Map;->clear()V

    .line 212
    iget-object v7, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v7}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getUpcomingOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v7

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$getACTIVE_ORDER_COMPARATOR$p()Ljava/util/Comparator;

    move-result-object v8

    invoke-static {v1, v8}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 213
    iget-object v1, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getActiveOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$getACTIVE_ORDER_COMPARATOR$p()Ljava/util/Comparator;

    move-result-object v7

    invoke-static {v2, v7}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 214
    iget-object v1, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getCompletedOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$getCOMPLETED_ORDER_COMPARATOR$p()Ljava/util/Comparator;

    move-result-object v2

    invoke-static {v3, v2}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 215
    iget-object v1, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getUnknownNewOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 216
    iget-object v1, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getUnprintedNewOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 218
    iget-object v1, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getMostRecentSearchQuery$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/squareup/ordermanagerdata/SearchQuery;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/ordermanagerdata/SearchQuery;->getOrderGroup()Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v9

    :cond_c
    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    if-eq v9, v1, :cond_18

    .line 219
    iget-object v1, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->$updatedOrders:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 531
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/orders/model/Order;

    .line 220
    iget-object v3, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v3, v2}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$updateSearchResultsWithOrder(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Lcom/squareup/orders/model/Order;)V

    goto :goto_8

    .line 224
    :cond_d
    sget-object v8, Lcom/squareup/ordermanagerdata/local/UpdateMode$RefreshActiveOrders;->INSTANCE:Lcom/squareup/ordermanagerdata/local/UpdateMode$RefreshActiveOrders;

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_14

    .line 225
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 231
    iget-object v3, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v3}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getOrders$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    sget-object v7, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1$4;->INSTANCE:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1$4;

    check-cast v7, Lkotlin/jvm/functions/Function1;

    invoke-static {v3, v7}, Lkotlin/collections/CollectionsKt;->removeAll(Ljava/lang/Iterable;Lkotlin/jvm/functions/Function1;)Z

    if-eqz v12, :cond_e

    .line 234
    iget-object v3, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v3}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getActiveOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v3

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$getACTIVE_ORDER_COMPARATOR$p()Ljava/util/Comparator;

    move-result-object v7

    invoke-static {v2, v7}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_9

    .line 235
    :cond_e
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 236
    iget-object v2, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v2}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getActiveOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v2

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_f
    :goto_9
    if-eqz v11, :cond_10

    .line 240
    iget-object v2, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v2}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getUpcomingOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v2

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$getACTIVE_ORDER_COMPARATOR$p()Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_a

    .line 241
    :cond_10
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 242
    iget-object v1, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getUpcomingOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 245
    :cond_11
    :goto_a
    iget-object v1, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getUnknownNewOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 246
    iget-object v1, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getUnprintedNewOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 248
    iget-object v1, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getMostRecentSearchQuery$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/squareup/ordermanagerdata/SearchQuery;

    move-result-object v1

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/ordermanagerdata/SearchQuery;->getOrderGroup()Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v9

    :cond_12
    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    if-eq v9, v1, :cond_18

    .line 249
    iget-object v1, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->$updatedOrders:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 533
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/orders/model/Order;

    .line 250
    iget-object v3, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v3, v2}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$updateSearchResultsWithOrder(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Lcom/squareup/orders/model/Order;)V

    goto :goto_b

    .line 225
    :cond_13
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Should not receive completed orders when updateMode is RefreshActiveOrders"

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 254
    :cond_14
    sget-object v4, Lcom/squareup/ordermanagerdata/local/UpdateMode$PaginateCompletedOrders;->INSTANCE:Lcom/squareup/ordermanagerdata/local/UpdateMode$PaginateCompletedOrders;

    invoke-static {v7, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 255
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 258
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_16

    if-eqz v13, :cond_18

    .line 266
    iget-object v1, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getCompletedOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-eqz v1, :cond_15

    goto :goto_c

    :cond_15
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    :goto_c
    const-string v2, "(completedOrdersRelay.value ?: emptyList())"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/Collection;

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    .line 267
    iget-object v2, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v2}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getCompletedOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v2

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$getCOMPLETED_ORDER_COMPARATOR$p()Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_d

    .line 258
    :cond_16
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Should not receive upcoming orders when updateMode is PaginateCompletedOrders"

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 255
    :cond_17
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Should not receive active orders when updateMode is PaginateCompletedOrders"

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 271
    :cond_18
    :goto_d
    iget-object v1, v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$updateOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getOrders$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Ljava/util/Map;

    move-result-object v1

    check-cast v6, Ljava/util/Map;

    invoke-interface {v1, v6}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method
