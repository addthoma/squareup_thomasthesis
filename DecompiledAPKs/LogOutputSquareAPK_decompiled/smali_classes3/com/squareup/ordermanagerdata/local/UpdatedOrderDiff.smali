.class final Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;
.super Ljava/lang/Object;
.source "InMemoryLocalOrderStore.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0016\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0082\u0008\u0018\u0000 \u001f2\u00020\u0001:\u0001\u001fB=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\u0008\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00c6\u0003JO\u0010\u0018\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00052\u0008\u0008\u0002\u0010\t\u001a\u00020\u00052\u0008\u0008\u0002\u0010\n\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u00052\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u000cR\u0011\u0010\t\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u000cR\u0011\u0010\n\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000cR\u0013\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\n\n\u0002\u0008\r\u001a\u0004\u0008\u0007\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;",
        "",
        "updatedWrappedOrder",
        "Lcom/squareup/ordermanagerdata/local/WrappedOrder;",
        "orderUpdated",
        "",
        "isNewActiveOrder",
        "isUpcomingOrder",
        "isNewCompletedOrder",
        "isUnknownOrder",
        "isUnprintedNewOrder",
        "(Lcom/squareup/ordermanagerdata/local/WrappedOrder;ZZZZZZ)V",
        "()Z",
        "isUpcomingOrder$1",
        "getOrderUpdated",
        "getUpdatedWrappedOrder",
        "()Lcom/squareup/ordermanagerdata/local/WrappedOrder;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;


# instance fields
.field private final isNewActiveOrder:Z

.field private final isNewCompletedOrder:Z

.field private final isUnknownOrder:Z

.field private final isUnprintedNewOrder:Z

.field private final isUpcomingOrder$1:Z

.field private final orderUpdated:Z

.field private final updatedWrappedOrder:Lcom/squareup/ordermanagerdata/local/WrappedOrder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->Companion:Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ordermanagerdata/local/WrappedOrder;ZZZZZZ)V
    .locals 1

    const-string/jumbo v0, "updatedWrappedOrder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->updatedWrappedOrder:Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    iput-boolean p2, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->orderUpdated:Z

    iput-boolean p3, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewActiveOrder:Z

    iput-boolean p4, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUpcomingOrder$1:Z

    iput-boolean p5, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewCompletedOrder:Z

    iput-boolean p6, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnknownOrder:Z

    iput-boolean p7, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnprintedNewOrder:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;Lcom/squareup/ordermanagerdata/local/WrappedOrder;ZZZZZZILjava/lang/Object;)Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->updatedWrappedOrder:Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-boolean p2, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->orderUpdated:Z

    :cond_1
    move p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewActiveOrder:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUpcomingOrder$1:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewCompletedOrder:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnknownOrder:Z

    :cond_5
    move v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-boolean p7, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnprintedNewOrder:Z

    :cond_6
    move v4, p7

    move-object p2, p0

    move-object p3, p1

    move p4, p9

    move p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    move p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->copy(Lcom/squareup/ordermanagerdata/local/WrappedOrder;ZZZZZZ)Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ordermanagerdata/local/WrappedOrder;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->updatedWrappedOrder:Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->orderUpdated:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewActiveOrder:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUpcomingOrder$1:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewCompletedOrder:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnknownOrder:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnprintedNewOrder:Z

    return v0
.end method

.method public final copy(Lcom/squareup/ordermanagerdata/local/WrappedOrder;ZZZZZZ)Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;
    .locals 9

    const-string/jumbo v0, "updatedWrappedOrder"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;

    move-object v1, v0

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;-><init>(Lcom/squareup/ordermanagerdata/local/WrappedOrder;ZZZZZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->updatedWrappedOrder:Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    iget-object v1, p1, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->updatedWrappedOrder:Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->orderUpdated:Z

    iget-boolean v1, p1, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->orderUpdated:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewActiveOrder:Z

    iget-boolean v1, p1, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewActiveOrder:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUpcomingOrder$1:Z

    iget-boolean v1, p1, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUpcomingOrder$1:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewCompletedOrder:Z

    iget-boolean v1, p1, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewCompletedOrder:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnknownOrder:Z

    iget-boolean v1, p1, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnknownOrder:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnprintedNewOrder:Z

    iget-boolean p1, p1, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnprintedNewOrder:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getOrderUpdated()Z
    .locals 1

    .line 440
    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->orderUpdated:Z

    return v0
.end method

.method public final getUpdatedWrappedOrder()Lcom/squareup/ordermanagerdata/local/WrappedOrder;
    .locals 1

    .line 439
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->updatedWrappedOrder:Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->updatedWrappedOrder:Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->orderUpdated:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewActiveOrder:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUpcomingOrder$1:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewCompletedOrder:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnknownOrder:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnprintedNewOrder:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public final isNewActiveOrder()Z
    .locals 1

    .line 441
    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewActiveOrder:Z

    return v0
.end method

.method public final isNewCompletedOrder()Z
    .locals 1

    .line 443
    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewCompletedOrder:Z

    return v0
.end method

.method public final isUnknownOrder()Z
    .locals 1

    .line 444
    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnknownOrder:Z

    return v0
.end method

.method public final isUnprintedNewOrder()Z
    .locals 1

    .line 445
    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnprintedNewOrder:Z

    return v0
.end method

.method public final isUpcomingOrder()Z
    .locals 1

    .line 442
    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUpcomingOrder$1:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdatedOrderDiff(updatedWrappedOrder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->updatedWrappedOrder:Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", orderUpdated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->orderUpdated:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isNewActiveOrder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewActiveOrder:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isUpcomingOrder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUpcomingOrder$1:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isNewCompletedOrder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isNewCompletedOrder:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isUnknownOrder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnknownOrder:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isUnprintedNewOrder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;->isUnprintedNewOrder:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
