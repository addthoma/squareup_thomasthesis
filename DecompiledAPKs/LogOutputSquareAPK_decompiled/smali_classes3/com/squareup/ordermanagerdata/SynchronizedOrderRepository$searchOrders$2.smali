.class final Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$searchOrders$2;
.super Ljava/lang/Object;
.source "SynchronizedOrderRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->searchOrders(Lcom/squareup/ordermanagerdata/SearchQuery;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001aZ\u0012&\u0008\u0001\u0012\"\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u0003 \u0005*\u0010\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0018\u00010\u00020\u0002 \u0005*,\u0012&\u0008\u0001\u0012\"\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u0003 \u0005*\u0010\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "kotlin.jvm.PlatformType",
        "it",
        "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;


# direct methods
.method constructor <init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$searchOrders$2;->this$0:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;",
            ")",
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->getLatestResult()Lcom/squareup/ordermanagerdata/ResultState;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->getLatestResult()Lcom/squareup/ordermanagerdata/ResultState;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    .line 120
    invoke-virtual {p1}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    .line 124
    :cond_0
    iget-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$searchOrders$2;->this$0:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->access$getLocalOrderStore$p(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ordermanagerdata/local/LocalOrderStore;->ordersFromMostRecentSearch()Lio/reactivex/Observable;

    move-result-object p1

    .line 125
    sget-object v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$searchOrders$2$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$searchOrders$2$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->access$mapToResultState(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p1

    .line 126
    invoke-virtual {p1}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    invoke-virtual {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$searchOrders$2;->apply(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
