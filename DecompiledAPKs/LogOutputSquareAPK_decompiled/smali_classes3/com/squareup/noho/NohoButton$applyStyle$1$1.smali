.class final Lcom/squareup/noho/NohoButton$applyStyle$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "NohoButton.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/NohoButton;->applyStyle(Landroid/util/AttributeSet;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/noho/EdgePainter;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/noho/EdgePainter;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_withStyledAttributes:Landroid/content/res/TypedArray;


# direct methods
.method constructor <init>(Landroid/content/res/TypedArray;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/noho/NohoButton$applyStyle$1$1;->$this_withStyledAttributes:Landroid/content/res/TypedArray;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/noho/EdgePainter;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoButton$applyStyle$1$1;->invoke(Lcom/squareup/noho/EdgePainter;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/noho/EdgePainter;)V
    .locals 3

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/noho/NohoButton$applyStyle$1$1;->$this_withStyledAttributes:Landroid/content/res/TypedArray;

    sget v1, Lcom/squareup/noho/R$styleable;->NohoButton_sqHideBorder:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    not-int v0, v0

    and-int/lit8 v0, v0, 0xf

    invoke-virtual {p1, v0}, Lcom/squareup/noho/EdgePainter;->setEdges(I)V

    .line 156
    iget-object v0, p0, Lcom/squareup/noho/NohoButton$applyStyle$1$1;->$this_withStyledAttributes:Landroid/content/res/TypedArray;

    sget v1, Lcom/squareup/noho/R$styleable;->NohoButton_sqEdgeColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/EdgePainter;->setColor(I)V

    .line 157
    iget-object v0, p0, Lcom/squareup/noho/NohoButton$applyStyle$1$1;->$this_withStyledAttributes:Landroid/content/res/TypedArray;

    sget v1, Lcom/squareup/noho/R$styleable;->NohoButton_sqEdgeWidth:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/EdgePainter;->setEdgeWidth(I)V

    return-void
.end method
