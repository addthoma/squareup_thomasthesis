.class public final enum Lcom/squareup/noho/Badge$Type;
.super Ljava/lang/Enum;
.source "NohoActionBar.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/Badge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/noho/Badge$Type;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u001b\u0008\u0002\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/noho/Badge$Type;",
        "",
        "defStyleAttr",
        "",
        "defStyleRes",
        "(Ljava/lang/String;III)V",
        "getDefStyleAttr",
        "()I",
        "getDefStyleRes",
        "NORMAL",
        "HIGH_PRI",
        "FATAL",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/noho/Badge$Type;

.field public static final enum FATAL:Lcom/squareup/noho/Badge$Type;

.field public static final enum HIGH_PRI:Lcom/squareup/noho/Badge$Type;

.field public static final enum NORMAL:Lcom/squareup/noho/Badge$Type;


# instance fields
.field private final defStyleAttr:I

.field private final defStyleRes:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/noho/Badge$Type;

    new-instance v1, Lcom/squareup/noho/Badge$Type;

    .line 385
    sget v2, Lcom/squareup/noho/R$style;->Widget_Noho_Notification:I

    const/4 v3, 0x0

    const-string v4, "NORMAL"

    invoke-direct {v1, v4, v3, v3, v2}, Lcom/squareup/noho/Badge$Type;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/Badge$Type;->NORMAL:Lcom/squareup/noho/Badge$Type;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/noho/Badge$Type;

    .line 386
    sget v2, Lcom/squareup/noho/R$style;->Widget_Noho_Notification_HighPri:I

    const/4 v4, 0x1

    const-string v5, "HIGH_PRI"

    invoke-direct {v1, v5, v4, v3, v2}, Lcom/squareup/noho/Badge$Type;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/Badge$Type;->HIGH_PRI:Lcom/squareup/noho/Badge$Type;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/Badge$Type;

    .line 387
    sget v2, Lcom/squareup/noho/R$style;->Widget_Noho_Notification_Fatal:I

    const/4 v4, 0x2

    const-string v5, "FATAL"

    invoke-direct {v1, v5, v4, v3, v2}, Lcom/squareup/noho/Badge$Type;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/Badge$Type;->FATAL:Lcom/squareup/noho/Badge$Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/noho/Badge$Type;->$VALUES:[Lcom/squareup/noho/Badge$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 381
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/noho/Badge$Type;->defStyleAttr:I

    iput p4, p0, Lcom/squareup/noho/Badge$Type;->defStyleRes:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/noho/Badge$Type;
    .locals 1

    const-class v0, Lcom/squareup/noho/Badge$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/noho/Badge$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/noho/Badge$Type;
    .locals 1

    sget-object v0, Lcom/squareup/noho/Badge$Type;->$VALUES:[Lcom/squareup/noho/Badge$Type;

    invoke-virtual {v0}, [Lcom/squareup/noho/Badge$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/noho/Badge$Type;

    return-object v0
.end method


# virtual methods
.method public final getDefStyleAttr()I
    .locals 1

    .line 382
    iget v0, p0, Lcom/squareup/noho/Badge$Type;->defStyleAttr:I

    return v0
.end method

.method public final getDefStyleRes()I
    .locals 1

    .line 383
    iget v0, p0, Lcom/squareup/noho/Badge$Type;->defStyleRes:I

    return v0
.end method
