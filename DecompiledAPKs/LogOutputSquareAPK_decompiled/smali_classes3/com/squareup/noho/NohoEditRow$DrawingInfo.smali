.class public final Lcom/squareup/noho/NohoEditRow$DrawingInfo;
.super Ljava/lang/Object;
.source "NohoEditRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoEditRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DrawingInfo"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/noho/NohoEditRow$DrawingInfo;",
        "",
        "totalRect",
        "Landroid/graphics/Rect;",
        "pluginRect",
        "textRect",
        "(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V",
        "getPluginRect",
        "()Landroid/graphics/Rect;",
        "getTextRect",
        "getTotalRect",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final pluginRect:Landroid/graphics/Rect;

.field private final textRect:Landroid/graphics/Rect;

.field private final totalRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 1

    const-string/jumbo v0, "totalRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pluginRect"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "textRect"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->totalRect:Landroid/graphics/Rect;

    iput-object p2, p0, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->pluginRect:Landroid/graphics/Rect;

    iput-object p3, p0, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->textRect:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public final getPluginRect()Landroid/graphics/Rect;
    .locals 1

    .line 435
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->pluginRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final getTextRect()Landroid/graphics/Rect;
    .locals 1

    .line 436
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->textRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final getTotalRect()Landroid/graphics/Rect;
    .locals 1

    .line 434
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->totalRect:Landroid/graphics/Rect;

    return-object v0
.end method
