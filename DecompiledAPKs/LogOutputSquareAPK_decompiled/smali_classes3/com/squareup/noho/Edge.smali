.class public interface abstract annotation Lcom/squareup/noho/Edge;
.super Ljava/lang/Object;
.source "Edge.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->SOURCE:Ljava/lang/annotation/RetentionPolicy;
.end annotation


# static fields
.field public static final ALL:I = 0xf

.field public static final BOTTOM:I = 0x8

.field public static final LEFT:I = 0x1

.field public static final NONE:I = 0x0

.field public static final RIGHT:I = 0x4

.field public static final TOP:I = 0x2
