.class public final Lcom/squareup/noho/NohoEdgeDecoration;
.super Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;
.source "NohoEdgeDecoration.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;,
        Lcom/squareup/noho/NohoEdgeDecoration$DefaultEdgeProvider;,
        Lcom/squareup/noho/NohoEdgeDecoration$AllEdges;,
        Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoEdgeDecoration.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoEdgeDecoration.kt\ncom/squareup/noho/NohoEdgeDecoration\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,290:1\n1163#2:291\n1104#3,2:292\n1360#4:294\n1429#4,3:295\n*E\n*S KotlinDebug\n*F\n+ 1 NohoEdgeDecoration.kt\ncom/squareup/noho/NohoEdgeDecoration\n*L\n123#1:291\n123#1,2:292\n252#1:294\n252#1,3:295\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0004)*+,B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B!\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0006\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0018\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J(\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000c2\u0006\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0018\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u0006H\u0002J\u0018\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u0006H\u0002J \u0010\u001f\u001a\u00020\u00152\u0006\u0010 \u001a\u00020!2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J \u0010\"\u001a\u00020\u00062\u0006\u0010#\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J(\u0010$\u001a\u00020\u00062\u0006\u0010%\u001a\u00020\u00062\u0006\u0010&\u001a\u00020\'2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J \u0010(\u001a\u00020\u00062\u0006\u0010#\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/noho/NohoEdgeDecoration;",
        "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;",
        "resources",
        "Landroid/content/res/Resources;",
        "(Landroid/content/res/Resources;)V",
        "dividerWeightPx",
        "",
        "dividerColor",
        "edgeProvider",
        "Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;",
        "(IILcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;)V",
        "dividerPadding",
        "Landroid/graphics/Rect;",
        "paint",
        "Landroid/graphics/Paint;",
        "getItemEdgesToDraw",
        "itemView",
        "Landroid/view/View;",
        "parent",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "getItemOffsets",
        "",
        "outRect",
        "view",
        "state",
        "Landroidx/recyclerview/widget/RecyclerView$State;",
        "isInFirstColumnOfGrid",
        "",
        "position",
        "columns",
        "isInFirstRowOfGrid",
        "onDraw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "removeOverlappingEdges",
        "edges",
        "removeOverlappingEdgesForGridLayout",
        "originalEdges",
        "layoutManager",
        "Landroidx/recyclerview/widget/GridLayoutManager;",
        "removeOverlappingEdgesForLinearLayoutManager",
        "AllEdges",
        "DefaultEdgeProvider",
        "EdgeProvider",
        "ShowsEdges",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dividerPadding:Landroid/graphics/Rect;

.field private final dividerWeightPx:I

.field private final edgeProvider:Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;

.field private final paint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(IILcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;)V
    .locals 1

    const-string v0, "edgeProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;-><init>()V

    iput p1, p0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerWeightPx:I

    iput-object p3, p0, Lcom/squareup/noho/NohoEdgeDecoration;->edgeProvider:Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;

    .line 42
    new-instance p1, Landroid/graphics/Paint;

    const/4 p3, 0x1

    invoke-direct {p1, p3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lcom/squareup/noho/NohoEdgeDecoration;->paint:Landroid/graphics/Paint;

    .line 43
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerPadding:Landroid/graphics/Rect;

    .line 115
    iget-object p1, p0, Lcom/squareup/noho/NohoEdgeDecoration;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public synthetic constructor <init>(IILcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 40
    sget-object p3, Lcom/squareup/noho/NohoEdgeDecoration$DefaultEdgeProvider;->INSTANCE:Lcom/squareup/noho/NohoEdgeDecoration$DefaultEdgeProvider;

    check-cast p3, Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(IILcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 7

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    sget v0, Lcom/squareup/noho/R$dimen;->noho_divider_hairline:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 111
    sget v0, Lcom/squareup/noho/R$color;->noho_divider_hairline:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    .line 109
    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(IILcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method private final getItemEdgesToDraw(Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;)I
    .locals 3

    .line 197
    invoke-virtual {p2, p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v0

    const-string v1, "parent.getChildViewHolder(itemView)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    iget-object v1, p0, Lcom/squareup/noho/NohoEdgeDecoration;->edgeProvider:Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-interface {v1, v0, v2}, Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;->edges(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)I

    move-result v0

    .line 199
    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/noho/NohoEdgeDecoration;->removeOverlappingEdges(ILandroid/view/View;Landroidx/recyclerview/widget/RecyclerView;)I

    move-result p1

    return p1
.end method

.method private final isInFirstColumnOfGrid(II)Z
    .locals 0

    .line 288
    rem-int/2addr p1, p2

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final isInFirstRowOfGrid(II)Z
    .locals 0

    if-ge p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final removeOverlappingEdges(ILandroid/view/View;Landroidx/recyclerview/widget/RecyclerView;)I
    .locals 2

    .line 207
    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    .line 209
    instance-of v1, v0, Landroidx/recyclerview/widget/GridLayoutManager;

    if-eqz v1, :cond_0

    .line 210
    check-cast v0, Landroidx/recyclerview/widget/GridLayoutManager;

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/squareup/noho/NohoEdgeDecoration;->removeOverlappingEdgesForGridLayout(ILandroidx/recyclerview/widget/GridLayoutManager;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;)I

    move-result p1

    goto :goto_0

    .line 212
    :cond_0
    instance-of v0, v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    if-eqz v0, :cond_1

    .line 213
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoEdgeDecoration;->removeOverlappingEdgesForLinearLayoutManager(ILandroid/view/View;Landroidx/recyclerview/widget/RecyclerView;)I

    move-result p1

    :cond_1
    :goto_0
    return p1
.end method

.method private final removeOverlappingEdgesForGridLayout(ILandroidx/recyclerview/widget/GridLayoutManager;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;)I
    .locals 6

    .line 244
    invoke-virtual {p4, p3}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p3

    const-string v0, "parent.getChildViewHolder(itemView)"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 245
    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p3

    .line 246
    invoke-virtual {p2}, Landroidx/recyclerview/widget/GridLayoutManager;->getSpanCount()I

    move-result v0

    const/4 v1, 0x0

    .line 251
    invoke-static {v1, p3}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 294
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 295
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v4, v2

    check-cast v4, Lkotlin/collections/IntIterator;

    invoke-virtual {v4}, Lkotlin/collections/IntIterator;->nextInt()I

    move-result v4

    .line 252
    invoke-virtual {p2}, Landroidx/recyclerview/widget/GridLayoutManager;->getSpanSizeLookup()Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;->getSpanSize(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 297
    :cond_0
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 253
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->sumOfInt(Ljava/lang/Iterable;)I

    move-result v2

    .line 255
    invoke-direct {p0, v2, v0}, Lcom/squareup/noho/NohoEdgeDecoration;->isInFirstRowOfGrid(II)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, p3

    const/4 v3, 0x0

    :goto_1
    if-lez v1, :cond_1

    if-ge v3, v0, :cond_1

    add-int/lit8 v1, v1, -0x1

    .line 263
    invoke-virtual {p2}, Landroidx/recyclerview/widget/GridLayoutManager;->getSpanSizeLookup()Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;->getSpanSize(I)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_1

    .line 265
    :cond_1
    invoke-virtual {p4, v1}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p2

    .line 266
    iget-object v3, p0, Lcom/squareup/noho/NohoEdgeDecoration;->edgeProvider:Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;

    invoke-interface {v3, p2, v1}, Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;->edges(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)I

    move-result p2

    const/16 v1, 0x8

    invoke-static {p2, v1}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x2

    .line 267
    invoke-static {p1, p2}, Lcom/squareup/noho/Edges;->removeEdge(II)I

    move-result p1

    .line 270
    :cond_2
    invoke-direct {p0, v2, v0}, Lcom/squareup/noho/NohoEdgeDecoration;->isInFirstColumnOfGrid(II)Z

    move-result p2

    if-nez p2, :cond_3

    const/4 p2, 0x1

    sub-int/2addr p3, p2

    .line 272
    invoke-virtual {p4, p3}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p4

    .line 273
    iget-object v0, p0, Lcom/squareup/noho/NohoEdgeDecoration;->edgeProvider:Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;

    invoke-interface {v0, p4, p3}, Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;->edges(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)I

    move-result p3

    const/4 p4, 0x4

    invoke-static {p3, p4}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result p3

    if-eqz p3, :cond_3

    .line 274
    invoke-static {p1, p2}, Lcom/squareup/noho/Edges;->removeEdge(II)I

    move-result p1

    :cond_3
    return p1
.end method

.method private final removeOverlappingEdgesForLinearLayoutManager(ILandroid/view/View;Landroidx/recyclerview/widget/RecyclerView;)I
    .locals 1

    .line 224
    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p2

    const-string v0, "parent.getChildViewHolder(itemView)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p2

    if-lez p2, :cond_0

    add-int/lit8 p2, p2, -0x1

    .line 229
    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p3

    .line 230
    iget-object v0, p0, Lcom/squareup/noho/NohoEdgeDecoration;->edgeProvider:Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;

    invoke-interface {v0, p3, p2}, Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;->edges(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)I

    move-result p2

    const/16 p3, 0x8

    invoke-static {p2, p3}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x2

    .line 231
    invoke-static {p1, p2}, Lcom/squareup/noho/Edges;->removeEdge(II)I

    move-result p1

    :cond_0
    return p1
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V
    .locals 3

    const-string v0, "outRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    invoke-direct {p0, p2, p3}, Lcom/squareup/noho/NohoEdgeDecoration;->getItemEdgesToDraw(Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;)I

    move-result p2

    const/4 p3, 0x1

    .line 186
    invoke-static {p2, p3}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result p3

    const/4 p4, 0x0

    if-eqz p3, :cond_0

    iget p3, p0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerWeightPx:I

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    const/4 v0, 0x2

    .line 187
    invoke-static {p2, v0}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerWeightPx:I

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x4

    .line 188
    invoke-static {p2, v1}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerWeightPx:I

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    const/16 v2, 0x8

    .line 189
    invoke-static {p2, v2}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result p2

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerWeightPx:I

    .line 185
    :cond_3
    invoke-virtual {p1, p3, v0, v1, p4}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    const-string v2, "canvas"

    move-object/from16 v9, p1

    invoke-static {v9, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "parent"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "state"

    move-object/from16 v3, p3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    move-object v2, v1

    check-cast v2, Landroid/view/ViewGroup;

    .line 291
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    const/4 v4, 0x0

    invoke-static {v4, v3}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v3

    new-instance v4, Lcom/squareup/noho/NohoEdgeDecoration$onDraw$$inlined$getChildren$1;

    invoke-direct {v4, v2}, Lcom/squareup/noho/NohoEdgeDecoration$onDraw$$inlined$getChildren$1;-><init>(Landroid/view/ViewGroup;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-static {v3, v4}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v2

    .line 292
    invoke-interface {v2}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v10, v3

    check-cast v10, Landroid/view/View;

    .line 125
    invoke-direct {v0, v10, v1}, Lcom/squareup/noho/NohoEdgeDecoration;->getItemEdgesToDraw(Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;)I

    move-result v11

    .line 126
    invoke-virtual {v1, v10}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v3

    const-string v4, "parent.getChildViewHolder(itemView)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    iget-object v4, v0, Lcom/squareup/noho/NohoEdgeDecoration;->edgeProvider:Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;

    iget-object v5, v0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerPadding:Landroid/graphics/Rect;

    invoke-interface {v4, v3, v5}, Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;->dividerPadding(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroid/graphics/Rect;)V

    .line 128
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    if-eqz v3, :cond_4

    move-object v12, v3

    check-cast v12, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    .line 130
    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v3

    iget-object v4, v0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    iget v4, v0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerWeightPx:I

    sub-int v13, v3, v4

    .line 131
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v3

    iget-object v4, v0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    iget v4, v0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerWeightPx:I

    sub-int v14, v3, v4

    .line 132
    invoke-virtual {v10}, Landroid/view/View;->getRight()I

    move-result v3

    iget-object v4, v0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    iget v4, v0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerWeightPx:I

    add-int v15, v3, v4

    .line 133
    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    move-result v3

    iget-object v4, v0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v4

    iget v4, v0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerWeightPx:I

    add-int v8, v3, v4

    const/4 v3, 0x1

    .line 135
    invoke-static {v11, v3}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 136
    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v3

    iget v4, v12, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;->leftMargin:I

    sub-int/2addr v3, v4

    .line 138
    iget v4, v0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerWeightPx:I

    sub-int v4, v3, v4

    int-to-float v4, v4

    int-to-float v5, v14

    int-to-float v6, v3

    int-to-float v7, v8

    .line 142
    iget-object v3, v0, Lcom/squareup/noho/NohoEdgeDecoration;->paint:Landroid/graphics/Paint;

    move-object/from16 v16, v3

    move-object/from16 v3, p1

    move v1, v8

    move-object/from16 v8, v16

    .line 137
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_0
    move v1, v8

    :goto_1
    const/4 v3, 0x2

    .line 145
    invoke-static {v11, v3}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 146
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v3

    iget v4, v12, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;->topMargin:I

    sub-int/2addr v3, v4

    int-to-float v4, v13

    .line 149
    iget v5, v0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerWeightPx:I

    sub-int v5, v3, v5

    int-to-float v5, v5

    int-to-float v6, v15

    int-to-float v7, v3

    .line 152
    iget-object v8, v0, Lcom/squareup/noho/NohoEdgeDecoration;->paint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    .line 147
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_1
    const/4 v3, 0x4

    .line 155
    invoke-static {v11, v3}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 156
    invoke-virtual {v10}, Landroid/view/View;->getRight()I

    move-result v3

    iget v4, v12, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;->rightMargin:I

    add-int/2addr v3, v4

    int-to-float v4, v3

    int-to-float v5, v14

    .line 160
    iget v6, v0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerWeightPx:I

    add-int/2addr v3, v6

    int-to-float v6, v3

    int-to-float v7, v1

    .line 162
    iget-object v8, v0, Lcom/squareup/noho/NohoEdgeDecoration;->paint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    .line 157
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_2
    const/16 v1, 0x8

    .line 165
    invoke-static {v11, v1}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 166
    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    move-result v1

    iget v3, v12, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int/2addr v1, v3

    int-to-float v4, v13

    int-to-float v5, v1

    int-to-float v6, v15

    .line 171
    iget v3, v0, Lcom/squareup/noho/NohoEdgeDecoration;->dividerWeightPx:I

    add-int/2addr v1, v3

    int-to-float v7, v1

    .line 172
    iget-object v8, v0, Lcom/squareup/noho/NohoEdgeDecoration;->paint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    .line 167
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_3
    move-object/from16 v1, p2

    goto/16 :goto_0

    .line 128
    :cond_4
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView.LayoutParams"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    return-void
.end method
