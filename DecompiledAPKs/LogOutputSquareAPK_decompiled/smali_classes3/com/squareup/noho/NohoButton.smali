.class public final Lcom/squareup/noho/NohoButton;
.super Lcom/squareup/marketfont/MarketButton;
.source "NohoButton.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoButton.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoButton.kt\ncom/squareup/noho/NohoButton\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,219:1\n37#2,6:220\n37#2,6:226\n37#2,6:232\n37#2,6:238\n*E\n*S KotlinDebug\n*F\n+ 1 NohoButton.kt\ncom/squareup/noho/NohoButton\n*L\n113#1,6:220\n135#1,6:226\n82#1,6:232\n89#1,6:238\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B#\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fJ&\u0010 \u001a\u00020\u001d2\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0001\u0010!\u001a\u00020\u00072\u0008\u0008\u0001\u0010\"\u001a\u00020\u0007H\u0002J\u001f\u0010#\u001a\u00020\u001d2\u0017\u0010$\u001a\u0013\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u001d0%\u00a2\u0006\u0002\u0008&J\u0010\u0010\'\u001a\u00020\u001d2\u0006\u0010(\u001a\u00020)H\u0014J\u0008\u0010*\u001a\u00020\u001dH\u0002J\u0008\u0010+\u001a\u00020\u001dH\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\r\u001a\u00020\u000c2\u0006\u0010\u000b\u001a\u00020\u000c@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R(\u0010\u0016\u001a\u0004\u0018\u00010\u00152\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0015@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\"\u0004\u0008\u0019\u0010\u001aR\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/noho/NohoButton;",
        "Lcom/squareup/marketfont/MarketButton;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyle",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "borderPainter",
        "Lcom/squareup/noho/EdgePainter;",
        "value",
        "",
        "isTwoLines",
        "()Z",
        "setTwoLines",
        "(Z)V",
        "mainSpan",
        "Lcom/squareup/noho/TextAppearanceSpanCompat;",
        "minHeightOneLine",
        "minHeightTwoLines",
        "",
        "subText",
        "getSubText",
        "()Ljava/lang/CharSequence;",
        "setSubText",
        "(Ljava/lang/CharSequence;)V",
        "subTextSpan",
        "apply",
        "",
        "type",
        "Lcom/squareup/noho/NohoButtonType;",
        "applyStyle",
        "defStyleAttr",
        "defStyleRes",
        "configureEdges",
        "block",
        "Lkotlin/Function1;",
        "Lkotlin/ExtensionFunctionType;",
        "onDraw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "updateMinHeight",
        "updateTransformation",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final borderPainter:Lcom/squareup/noho/EdgePainter;

.field private isTwoLines:Z

.field private mainSpan:Lcom/squareup/noho/TextAppearanceSpanCompat;

.field private minHeightOneLine:I

.field private minHeightTwoLines:I

.field private subText:Ljava/lang/CharSequence;

.field private subTextSpan:Lcom/squareup/noho/TextAppearanceSpanCompat;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    const-string v0, "a"

    const-string v1, "context"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-static {p1, p2, p3}, Lcom/squareup/noho/NohoButtonKt;->access$defStyleFromButtonType(Landroid/content/Context;Landroid/util/AttributeSet;I)I

    move-result v1

    invoke-direct {p0, p1, p2, v1}, Lcom/squareup/marketfont/MarketButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    new-instance v1, Lcom/squareup/noho/EdgePainter;

    move-object v2, p0

    check-cast v2, Landroid/view/View;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v3}, Lcom/squareup/noho/EdgePainter;-><init>(Landroid/view/View;II)V

    iput-object v1, p0, Lcom/squareup/noho/NohoButton;->borderPainter:Lcom/squareup/noho/EdgePainter;

    .line 82
    sget-object v1, Lcom/squareup/noho/R$styleable;->NohoButton:[I

    const-string v2, "R.styleable.NohoButton"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    invoke-virtual {p1, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p3

    .line 234
    :try_start_0
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    sget v1, Lcom/squareup/noho/R$styleable;->NohoButton_sqButtonType:I

    invoke-static {}, Lcom/squareup/noho/NohoButtonType;->values()[Lcom/squareup/noho/NohoButtonType;

    move-result-object v3

    check-cast v3, [Ljava/lang/Enum;

    sget-object v4, Lcom/squareup/noho/NohoButtonType;->PRIMARY:Lcom/squareup/noho/NohoButtonType;

    check-cast v4, Ljava/lang/Enum;

    invoke-static {p3, v1, v3, v4}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;I[Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v1, Lcom/squareup/noho/NohoButtonType;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 236
    invoke-virtual {p3}, Landroid/content/res/TypedArray;->recycle()V

    .line 86
    invoke-virtual {v1}, Lcom/squareup/noho/NohoButtonType;->getStyleAttr()I

    move-result p3

    invoke-virtual {v1}, Lcom/squareup/noho/NohoButtonType;->getStyleRes()I

    move-result v3

    invoke-direct {p0, p2, p3, v3}, Lcom/squareup/noho/NohoButton;->applyStyle(Landroid/util/AttributeSet;II)V

    .line 91
    sget-object p3, Lcom/squareup/noho/R$styleable;->NohoButton:[I

    invoke-static {p3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-virtual {v1}, Lcom/squareup/noho/NohoButtonType;->getStyleAttr()I

    move-result v2

    .line 93
    invoke-virtual {v1}, Lcom/squareup/noho/NohoButtonType;->getStyleRes()I

    move-result v1

    .line 238
    invoke-virtual {p1, p2, p3, v2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 240
    :try_start_1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    sget p2, Lcom/squareup/noho/R$styleable;->NohoButton_sqSubText:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoButton;->setSubText(Ljava/lang/CharSequence;)V

    .line 96
    sget p2, Lcom/squareup/noho/R$styleable;->NohoButton_sqIsTwoLines:I

    invoke-virtual {p0}, Lcom/squareup/noho/NohoButton;->isTwoLines()Z

    move-result p3

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoButton;->setTwoLines(Z)V

    .line 97
    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 242
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2

    :catchall_1
    move-exception p1

    .line 236
    invoke-virtual {p3}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 31
    sget p3, Lcom/squareup/noho/R$attr;->nohoButtonStyle:I

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$applyStyle(Lcom/squareup/noho/NohoButton;Landroid/util/AttributeSet;II)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoButton;->applyStyle(Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public static final synthetic access$getMainSpan$p(Lcom/squareup/noho/NohoButton;)Lcom/squareup/noho/TextAppearanceSpanCompat;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/noho/NohoButton;->mainSpan:Lcom/squareup/noho/TextAppearanceSpanCompat;

    return-object p0
.end method

.method public static final synthetic access$getMinHeightOneLine$p(Lcom/squareup/noho/NohoButton;)I
    .locals 0

    .line 28
    iget p0, p0, Lcom/squareup/noho/NohoButton;->minHeightOneLine:I

    return p0
.end method

.method public static final synthetic access$getMinHeightTwoLines$p(Lcom/squareup/noho/NohoButton;)I
    .locals 0

    .line 28
    iget p0, p0, Lcom/squareup/noho/NohoButton;->minHeightTwoLines:I

    return p0
.end method

.method public static final synthetic access$getSubTextSpan$p(Lcom/squareup/noho/NohoButton;)Lcom/squareup/noho/TextAppearanceSpanCompat;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/noho/NohoButton;->subTextSpan:Lcom/squareup/noho/TextAppearanceSpanCompat;

    return-object p0
.end method

.method public static final synthetic access$setMainSpan$p(Lcom/squareup/noho/NohoButton;Lcom/squareup/noho/TextAppearanceSpanCompat;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/noho/NohoButton;->mainSpan:Lcom/squareup/noho/TextAppearanceSpanCompat;

    return-void
.end method

.method public static final synthetic access$setMinHeightOneLine$p(Lcom/squareup/noho/NohoButton;I)V
    .locals 0

    .line 28
    iput p1, p0, Lcom/squareup/noho/NohoButton;->minHeightOneLine:I

    return-void
.end method

.method public static final synthetic access$setMinHeightTwoLines$p(Lcom/squareup/noho/NohoButton;I)V
    .locals 0

    .line 28
    iput p1, p0, Lcom/squareup/noho/NohoButton;->minHeightTwoLines:I

    return-void
.end method

.method public static final synthetic access$setSubTextSpan$p(Lcom/squareup/noho/NohoButton;Lcom/squareup/noho/TextAppearanceSpanCompat;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/noho/NohoButton;->subTextSpan:Lcom/squareup/noho/TextAppearanceSpanCompat;

    return-void
.end method

.method public static final synthetic access$updateMinHeight(Lcom/squareup/noho/NohoButton;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/noho/NohoButton;->updateMinHeight()V

    return-void
.end method

.method public static final synthetic access$updateTransformation(Lcom/squareup/noho/NohoButton;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/noho/NohoButton;->updateTransformation()V

    return-void
.end method

.method private final applyStyle(Landroid/util/AttributeSet;II)V
    .locals 4

    .line 135
    invoke-virtual {p0}, Lcom/squareup/noho/NohoButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/squareup/noho/R$styleable;->NohoButton:[I

    const-string v3, "R.styleable.NohoButton"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    invoke-virtual {v0, p1, v2, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string p2, "a"

    .line 228
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    sget p2, Lcom/squareup/noho/R$styleable;->NohoButton_android_minHeight:I

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    invoke-static {p0, p2}, Lcom/squareup/noho/NohoButton;->access$setMinHeightOneLine$p(Lcom/squareup/noho/NohoButton;I)V

    .line 137
    sget p2, Lcom/squareup/noho/R$styleable;->NohoButton_sqMinHeightTwoLines:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    invoke-static {p0, p2}, Lcom/squareup/noho/NohoButton;->access$setMinHeightTwoLines$p(Lcom/squareup/noho/NohoButton;I)V

    .line 139
    invoke-static {p0}, Lcom/squareup/noho/NohoButton;->access$updateMinHeight(Lcom/squareup/noho/NohoButton;)V

    .line 142
    sget p2, Lcom/squareup/noho/R$styleable;->NohoButton_android_textAppearance:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    .line 143
    sget v0, Lcom/squareup/noho/R$styleable;->NohoButton_android_textColor:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 144
    new-instance v2, Lcom/squareup/noho/TextAppearanceSpanCompat;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoButton;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v3, p2, v0}, Lcom/squareup/noho/TextAppearanceSpanCompat;-><init>(Landroid/content/Context;ILandroid/content/res/ColorStateList;)V

    invoke-static {p0, v2}, Lcom/squareup/noho/NohoButton;->access$setMainSpan$p(Lcom/squareup/noho/NohoButton;Lcom/squareup/noho/TextAppearanceSpanCompat;)V

    .line 147
    sget p2, Lcom/squareup/noho/R$styleable;->NohoButton_sqSubTextAppearance:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    .line 148
    sget p3, Lcom/squareup/noho/R$styleable;->NohoButton_sqSubTextColor:I

    invoke-virtual {p1, p3}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p3

    .line 149
    new-instance v0, Lcom/squareup/noho/TextAppearanceSpanCompat;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v2, p2, p3}, Lcom/squareup/noho/TextAppearanceSpanCompat;-><init>(Landroid/content/Context;ILandroid/content/res/ColorStateList;)V

    invoke-static {p0, v0}, Lcom/squareup/noho/NohoButton;->access$setSubTextSpan$p(Lcom/squareup/noho/NohoButton;Lcom/squareup/noho/TextAppearanceSpanCompat;)V

    .line 152
    invoke-static {p0}, Lcom/squareup/noho/NohoButton;->access$updateTransformation(Lcom/squareup/noho/NohoButton;)V

    .line 154
    new-instance p2, Lcom/squareup/noho/NohoButton$applyStyle$1$1;

    invoke-direct {p2, p1}, Lcom/squareup/noho/NohoButton$applyStyle$1$1;-><init>(Landroid/content/res/TypedArray;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoButton;->configureEdges(Lkotlin/jvm/functions/Function1;)V

    .line 159
    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method private final updateMinHeight()V
    .locals 1

    .line 71
    iget-boolean v0, p0, Lcom/squareup/noho/NohoButton;->isTwoLines:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/noho/NohoButton;->minHeightTwoLines:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/squareup/noho/NohoButton;->minHeightOneLine:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoButton;->setMinHeight(I)V

    .line 73
    invoke-virtual {p0}, Lcom/squareup/noho/NohoButton;->getMinHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoButton;->setMinimumHeight(I)V

    return-void
.end method

.method private final updateTransformation()V
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/squareup/noho/NohoButton;->subText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 172
    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    .line 173
    :cond_2
    new-instance v1, Lcom/squareup/noho/NohoButton$updateTransformation$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/noho/NohoButton$updateTransformation$1;-><init>(Lcom/squareup/noho/NohoButton;Ljava/lang/CharSequence;)V

    move-object v0, v1

    check-cast v0, Landroid/text/method/TransformationMethod;

    .line 172
    :goto_2
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoButton;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/noho/NohoButtonType;)V
    .locals 5

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0}, Lcom/squareup/noho/NohoButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/squareup/noho/R$styleable;->NohoButton:[I

    const-string v2, "R.styleable.NohoButton"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/noho/NohoButtonType;->getStyleAttr()I

    move-result v2

    invoke-virtual {p1}, Lcom/squareup/noho/NohoButtonType;->getStyleRes()I

    move-result v3

    const/4 v4, 0x0

    .line 220
    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    :try_start_0
    const-string v1, "a"

    .line 222
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    sget v1, Lcom/squareup/noho/R$styleable;->NohoButton_android_textAppearance:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 115
    move-object v3, v1

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    move-object v1, v4

    :goto_1
    if-eqz v1, :cond_2

    .line 116
    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    move-object v3, p0

    check-cast v3, Landroid/widget/TextView;

    invoke-static {v3, v1}, Landroidx/core/widget/TextViewCompat;->setTextAppearance(Landroid/widget/TextView;I)V

    .line 117
    :cond_2
    sget v1, Lcom/squareup/noho/R$styleable;->NohoButton_android_textColor:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 118
    invoke-virtual {p0, v1}, Lcom/squareup/noho/NohoButton;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 119
    :cond_3
    sget v1, Lcom/squareup/noho/R$styleable;->NohoButton_android_background:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/squareup/noho/NohoButton;->setBackgroundResource(I)V

    .line 121
    invoke-virtual {p1}, Lcom/squareup/noho/NohoButtonType;->getStyleAttr()I

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/noho/NohoButtonType;->getStyleRes()I

    move-result p1

    invoke-static {p0, v4, v1, p1}, Lcom/squareup/noho/NohoButton;->access$applyStyle(Lcom/squareup/noho/NohoButton;Landroid/util/AttributeSet;II)V

    .line 122
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method

.method public final configureEdges(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/noho/EdgePainter;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/noho/NohoButton;->borderPainter:Lcom/squareup/noho/EdgePainter;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    invoke-virtual {p0}, Lcom/squareup/noho/NohoButton;->invalidate()V

    return-void
.end method

.method public final getSubText()Ljava/lang/CharSequence;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/noho/NohoButton;->subText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final isTwoLines()Z
    .locals 1

    .line 39
    iget-boolean v0, p0, Lcom/squareup/noho/NohoButton;->isTwoLines:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/noho/NohoButton;->borderPainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/EdgePainter;->drawEdges(Landroid/graphics/Canvas;)V

    .line 102
    invoke-super {p0, p1}, Lcom/squareup/marketfont/MarketButton;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public final setSubText(Ljava/lang/CharSequence;)V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/noho/NohoButton;->subText:Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 60
    iput-object p1, p0, Lcom/squareup/noho/NohoButton;->subText:Ljava/lang/CharSequence;

    .line 62
    invoke-direct {p0}, Lcom/squareup/noho/NohoButton;->updateTransformation()V

    if-eqz p1, :cond_1

    .line 63
    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-nez p1, :cond_2

    .line 64
    invoke-virtual {p0, v1}, Lcom/squareup/noho/NohoButton;->setTwoLines(Z)V

    :cond_2
    return-void
.end method

.method public final setTwoLines(Z)V
    .locals 1

    .line 41
    iget-boolean v0, p0, Lcom/squareup/noho/NohoButton;->isTwoLines:Z

    if-eq v0, p1, :cond_0

    .line 42
    iput-boolean p1, p0, Lcom/squareup/noho/NohoButton;->isTwoLines:Z

    .line 44
    invoke-direct {p0}, Lcom/squareup/noho/NohoButton;->updateMinHeight()V

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 47
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoButton;->setSubText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
