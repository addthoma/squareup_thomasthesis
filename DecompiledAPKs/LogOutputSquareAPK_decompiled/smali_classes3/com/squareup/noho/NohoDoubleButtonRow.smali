.class public final Lcom/squareup/noho/NohoDoubleButtonRow;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "NohoDoubleButtonRow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\r\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u0017\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u001f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\u001e\u001a\u00020\u000e2\u0006\u0010\u001f\u001a\u00020 J\u0010\u0010!\u001a\u00020\u000e2\u0008\u0010\"\u001a\u0004\u0018\u00010#J\u0012\u0010$\u001a\u00020\u000e2\u0008\u0010\"\u001a\u0004\u0018\u00010%H\u0016J\u000e\u0010&\u001a\u00020\u000e2\u0006\u0010\u001f\u001a\u00020 J\u0010\u0010\'\u001a\u00020\u000e2\u0008\u0010\"\u001a\u0004\u0018\u00010#J\u0008\u0010(\u001a\u00020\u000eH\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010R(\u0010\u0013\u001a\u0004\u0018\u00010\u00122\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u00128F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0014\u0010\u0015\"\u0004\u0008\u0016\u0010\u0017R\u000e\u0010\u0018\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r8F\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u0010R(\u0010\u001b\u001a\u0004\u0018\u00010\u00122\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u00128F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u001c\u0010\u0015\"\u0004\u0008\u001d\u0010\u0017\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/noho/NohoDoubleButtonRow;",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "defStyle",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "leadingButton",
        "Lcom/squareup/noho/NohoButton;",
        "leadingDebouncedOnClick",
        "Lio/reactivex/Observable;",
        "",
        "getLeadingDebouncedOnClick",
        "()Lio/reactivex/Observable;",
        "value",
        "",
        "leadingTitle",
        "getLeadingTitle",
        "()Ljava/lang/CharSequence;",
        "setLeadingTitle",
        "(Ljava/lang/CharSequence;)V",
        "trailingButton",
        "trailingDebouncedOnClick",
        "getTrailingDebouncedOnClick",
        "trailingTitle",
        "getTrailingTitle",
        "setTrailingTitle",
        "setLeadingButtonType",
        "type",
        "Lcom/squareup/noho/NohoButtonType;",
        "setLeadingOnClickListener",
        "l",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "setOnClickListener",
        "Landroid/view/View$OnClickListener;",
        "setTrailingButtonType",
        "setTrailingOnClickListener",
        "updateConstraints",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final leadingButton:Lcom/squareup/noho/NohoButton;

.field private final trailingButton:Lcom/squareup/noho/NohoButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 58
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/noho/NohoDoubleButtonRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 12

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoDoubleButtonRow:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    const-string p3, "context.obtainStyledAttr\u2026leButtonRow, defStyle, 0)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    sget p3, Lcom/squareup/noho/R$styleable;->NohoDoubleButtonRow_sqLeadingText:I

    invoke-virtual {p2, p3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 70
    sget v0, Lcom/squareup/noho/R$styleable;->NohoDoubleButtonRow_sqLeadingButtonType:I

    invoke-static {}, Lcom/squareup/noho/NohoButtonType;->values()[Lcom/squareup/noho/NohoButtonType;

    move-result-object v2

    check-cast v2, [Ljava/lang/Enum;

    const/4 v3, 0x0

    invoke-static {p2, v0, v2, v3}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;I[Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    .line 69
    check-cast v0, Lcom/squareup/noho/NohoButtonType;

    .line 72
    sget v2, Lcom/squareup/noho/R$styleable;->NohoDoubleButtonRow_sqTrailingText:I

    invoke-virtual {p2, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 74
    sget v4, Lcom/squareup/noho/R$styleable;->NohoDoubleButtonRow_sqTrailingButtonType:I

    invoke-static {}, Lcom/squareup/noho/NohoButtonType;->values()[Lcom/squareup/noho/NohoButtonType;

    move-result-object v5

    check-cast v5, [Ljava/lang/Enum;

    invoke-static {p2, v4, v5, v3}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;I[Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    .line 73
    check-cast v3, Lcom/squareup/noho/NohoButtonType;

    .line 76
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 78
    new-instance p2, Lcom/squareup/noho/NohoButton;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v4 .. v9}, Lcom/squareup/noho/NohoButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    .line 79
    iget-object p2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    new-instance v4, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    const/4 v5, -0x2

    invoke-direct {v4, v1, v5}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    check-cast v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p2, v4}, Lcom/squareup/noho/NohoButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    iget-object p2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v4

    invoke-virtual {p2, v4}, Lcom/squareup/noho/NohoButton;->setId(I)V

    .line 81
    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {p0, p3}, Lcom/squareup/noho/NohoDoubleButtonRow;->setLeadingTitle(Ljava/lang/CharSequence;)V

    if-eqz v0, :cond_0

    .line 82
    iget-object p2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoButtonType;->applyTo(Lcom/squareup/noho/NohoButton;)V

    .line 83
    :cond_0
    iget-object p2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    check-cast p2, Landroid/view/View;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoDoubleButtonRow;->addView(Landroid/view/View;)V

    .line 85
    new-instance p2, Lcom/squareup/noho/NohoButton;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    move-object v6, p2

    move-object v7, p1

    invoke-direct/range {v6 .. v11}, Lcom/squareup/noho/NohoButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    .line 86
    iget-object p1, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    new-instance p2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    invoke-direct {p2, v1, v5}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    check-cast p2, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    iget-object p1, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setId(I)V

    .line 88
    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p0, v2}, Lcom/squareup/noho/NohoDoubleButtonRow;->setTrailingTitle(Ljava/lang/CharSequence;)V

    if-eqz v3, :cond_1

    .line 89
    iget-object p1, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v3, p1}, Lcom/squareup/noho/NohoButtonType;->applyTo(Lcom/squareup/noho/NohoButton;)V

    .line 90
    :cond_1
    iget-object p1, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoDoubleButtonRow;->addView(Landroid/view/View;)V

    .line 92
    invoke-direct {p0}, Lcom/squareup/noho/NohoDoubleButtonRow;->updateConstraints()V

    return-void
.end method

.method private final updateConstraints()V
    .locals 7

    .line 128
    new-instance v0, Landroidx/constraintlayout/widget/ConstraintSet;

    invoke-direct {v0}, Landroidx/constraintlayout/widget/ConstraintSet;-><init>()V

    .line 129
    move-object v1, p0

    check-cast v1, Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v0, v1}, Landroidx/constraintlayout/widget/ConstraintSet;->clone(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 132
    iget-object v2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoButton;->getId()I

    move-result v2

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4, v3}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 133
    iget-object v2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoButton;->getId()I

    move-result v2

    const/4 v5, 0x4

    invoke-virtual {v0, v2, v5, v4, v5}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 134
    iget-object v2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoButton;->getId()I

    move-result v2

    invoke-virtual {v0, v2, v3, v4, v3}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 135
    iget-object v2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoButton;->getId()I

    move-result v2

    invoke-virtual {v0, v2, v5, v4, v5}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 138
    iget-object v2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoButton;->getId()I

    move-result v2

    const/4 v3, 0x6

    invoke-virtual {v0, v2, v3, v4, v3}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 139
    iget-object v2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoButton;->getId()I

    move-result v2

    iget-object v5, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v5}, Lcom/squareup/noho/NohoButton;->getId()I

    move-result v5

    const/4 v6, 0x7

    invoke-virtual {v0, v2, v6, v5, v3}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 140
    iget-object v2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoButton;->getId()I

    move-result v2

    iget-object v5, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v5}, Lcom/squareup/noho/NohoButton;->getId()I

    move-result v5

    invoke-virtual {v0, v2, v3, v5, v6}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 141
    iget-object v2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoButton;->getId()I

    move-result v2

    invoke-virtual {v0, v2, v6, v4, v6}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 145
    iget-object v2, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoButton;->getId()I

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/noho/NohoDoubleButtonRow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/noho/R$dimen;->noho_row_gap_size:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 144
    invoke-virtual {v0, v2, v6, v3}, Landroidx/constraintlayout/widget/ConstraintSet;->setMargin(III)V

    .line 148
    invoke-virtual {v0, v1}, Landroidx/constraintlayout/widget/ConstraintSet;->applyTo(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    return-void
.end method


# virtual methods
.method public final getLeadingDebouncedOnClick()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 105
    iget-object v0, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final getLeadingTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getTrailingDebouncedOnClick()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final getTrailingTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final setLeadingButtonType(Lcom/squareup/noho/NohoButtonType;)V
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButtonType;->applyTo(Lcom/squareup/noho/NohoButton;)V

    return-void
.end method

.method public final setLeadingOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setLeadingTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->leadingButton:Lcom/squareup/noho/NohoButton;

    if-eqz p1, :cond_0

    invoke-static {p1}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 122
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Use #setLeadingOnClickListener or #setTrailingOnClickListener instead!"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final setTrailingButtonType(Lcom/squareup/noho/NohoButtonType;)V
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButtonType;->applyTo(Lcom/squareup/noho/NohoButton;)V

    return-void
.end method

.method public final setTrailingOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setTrailingTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/noho/NohoDoubleButtonRow;->trailingButton:Lcom/squareup/noho/NohoButton;

    if-eqz p1, :cond_0

    invoke-static {p1}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
