.class public Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;
.super Landroid/widget/LinearLayout$LayoutParams;
.source "NohoLinearLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoLinearLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DividerLinearLayoutParams"
.end annotation


# instance fields
.field private edges:I

.field private insetEdges:Z


# direct methods
.method public constructor <init>(II)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 114
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;-><init>(IIIZ)V

    return-void
.end method

.method public constructor <init>(IIF)V
    .locals 6

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    .line 134
    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;-><init>(IIFIZ)V

    return-void
.end method

.method public constructor <init>(IIFI)V
    .locals 6

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    .line 138
    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;-><init>(IIFIZ)V

    return-void
.end method

.method public constructor <init>(IIFIZ)V
    .locals 0

    .line 143
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 144
    iput p4, p0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->edges:I

    .line 145
    iput-boolean p5, p0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->insetEdges:Z

    return-void
.end method

.method public constructor <init>(III)V
    .locals 1

    const/4 v0, 0x1

    .line 118
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;-><init>(IIIZ)V

    return-void
.end method

.method public constructor <init>(IIIZ)V
    .locals 0

    .line 122
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 123
    iput p3, p0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->edges:I

    .line 124
    iput-boolean p4, p0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->insetEdges:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 106
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 107
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoLinearLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 108
    sget p2, Lcom/squareup/noho/R$styleable;->NohoLinearLayout_layout_edges:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->edges:I

    .line 109
    sget p2, Lcom/squareup/noho/R$styleable;->NohoLinearLayout_layout_insetEdges:I

    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->insetEdges:Z

    .line 110
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .line 128
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 p1, 0x0

    .line 129
    iput p1, p0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->edges:I

    const/4 p1, 0x1

    .line 130
    iput-boolean p1, p0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->insetEdges:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;)I
    .locals 0

    .line 98
    iget p0, p0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->edges:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;)Z
    .locals 0

    .line 98
    iget-boolean p0, p0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->insetEdges:Z

    return p0
.end method
