.class final Lcom/squareup/noho/NohoRow$ValueFeature;
.super Lcom/squareup/noho/NohoRow$FeatureProperty;
.source "NohoRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ValueFeature"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/noho/NohoRow$FeatureProperty<",
        "Ljava/lang/CharSequence;",
        "Lcom/squareup/noho/NohoLabel;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoRow.kt\ncom/squareup/noho/NohoRow$ValueFeature\n*L\n1#1,1059:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u00c2\u0002\u0018\u00002\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0008\u001a\u00020\tH\u0014J\u001a\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00032\u0008\u0010\r\u001a\u0004\u0018\u00010\u0002H\u0014J\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00022\u0008\u0010\r\u001a\u0004\u0018\u00010\u0002H\u0014J\u0012\u0010\u000f\u001a\u00020\u00102\u0008\u0010\r\u001a\u0004\u0018\u00010\u0002H\u0014J\u001a\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0008\u001a\u00020\tH\u0014\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/noho/NohoRow$ValueFeature;",
        "Lcom/squareup/noho/NohoRow$FeatureProperty;",
        "",
        "Lcom/squareup/noho/NohoLabel;",
        "()V",
        "create",
        "row",
        "Lcom/squareup/noho/NohoRow;",
        "styleId",
        "",
        "doSetValue",
        "",
        "view",
        "value",
        "normalize",
        "shouldShowFor",
        "",
        "updateStyleId",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/noho/NohoRow$ValueFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 628
    new-instance v0, Lcom/squareup/noho/NohoRow$ValueFeature;

    invoke-direct {v0}, Lcom/squareup/noho/NohoRow$ValueFeature;-><init>()V

    sput-object v0, Lcom/squareup/noho/NohoRow$ValueFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ValueFeature;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .line 630
    sget v1, Lcom/squareup/noho/R$id;->value:I

    .line 631
    sget v2, Lcom/squareup/noho/R$dimen;->noho_row_gap_size:I

    .line 632
    sget v3, Lcom/squareup/noho/R$attr;->sqValueAppearance:I

    .line 633
    sget v4, Lcom/squareup/noho/R$styleable;->NohoRow_sqValueAppearance:I

    .line 634
    sget v5, Lcom/squareup/noho/R$style;->TextAppearance_Widget_Noho_Row_Value:I

    const/4 v6, 0x0

    move-object v0, p0

    .line 629
    invoke-direct/range {v0 .. v6}, Lcom/squareup/noho/NohoRow$FeatureProperty;-><init>(IIIIILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lcom/squareup/noho/NohoRow;I)Landroid/view/View;
    .locals 0

    .line 628
    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoRow$ValueFeature;->create(Lcom/squareup/noho/NohoRow;I)Lcom/squareup/noho/NohoLabel;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method protected create(Lcom/squareup/noho/NohoRow;I)Lcom/squareup/noho/NohoLabel;
    .locals 8

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 647
    new-instance v0, Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string p1, "row.context"

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/noho/NohoLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 p1, 0x0

    .line 648
    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 649
    sget-object p1, Lcom/squareup/noho/NohoRow$ValueFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ValueFeature;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/noho/NohoRow$ValueFeature;->updateStyleId(Lcom/squareup/noho/NohoLabel;I)V

    const/4 p1, 0x1

    .line 650
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setDuplicateParentStateEnabled(Z)V

    .line 651
    new-instance p1, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    const/4 p2, -0x2

    invoke-direct {p1, p2, p2}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    check-cast p1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public bridge synthetic doSetValue(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .line 628
    check-cast p1, Lcom/squareup/noho/NohoLabel;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoRow$ValueFeature;->doSetValue(Lcom/squareup/noho/NohoLabel;Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected doSetValue(Lcom/squareup/noho/NohoLabel;Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 655
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected normalize(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .line 642
    invoke-static {p1}, Lcom/squareup/noho/NohoRowKt;->access$nullIfBlank(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic normalize(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 628
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow$ValueFeature;->normalize(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method protected shouldShowFor(Ljava/lang/CharSequence;)Z
    .locals 0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic shouldShowFor(Ljava/lang/Object;)Z
    .locals 0

    .line 628
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow$ValueFeature;->shouldShowFor(Ljava/lang/CharSequence;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic updateStyleId(Landroid/view/View;I)V
    .locals 0

    .line 628
    check-cast p1, Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoRow$ValueFeature;->updateStyleId(Lcom/squareup/noho/NohoLabel;I)V

    return-void
.end method

.method protected updateStyleId(Lcom/squareup/noho/NohoLabel;I)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 639
    sget-object v0, Lcom/squareup/textappearance/SquareTextAppearance;->Companion:Lcom/squareup/textappearance/SquareTextAppearance$Companion;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoLabel;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "view.context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p2}, Lcom/squareup/textappearance/SquareTextAppearance$Companion;->loadFromStyle(Landroid/content/Context;I)Lcom/squareup/textappearance/SquareTextAppearance;

    move-result-object p2

    .line 638
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoLabel;->apply(Lcom/squareup/textappearance/SquareTextAppearance;)V

    return-void
.end method
