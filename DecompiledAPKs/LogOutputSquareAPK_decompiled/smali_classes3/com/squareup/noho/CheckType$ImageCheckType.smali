.class public abstract Lcom/squareup/noho/CheckType$ImageCheckType;
.super Lcom/squareup/noho/CheckType;
.source "NohoCheckableRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/CheckType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ImageCheckType"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoCheckableRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoCheckableRow.kt\ncom/squareup/noho/CheckType$ImageCheckType\n*L\n1#1,205:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008&\u0018\u00002\u00020\u0001B#\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J<\u0010\u000b\u001a\u001a\u0012\u0004\u0012\u00020\r\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e0\u000c2\u0006\u0010\u0010\u001a\u00020\u00112\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000eH\u0016R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\t\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/noho/CheckType$ImageCheckType;",
        "Lcom/squareup/noho/CheckType;",
        "canDeselect",
        "",
        "drawableId",
        "",
        "attrColorsId",
        "(ZII)V",
        "getAttrColorsId",
        "()I",
        "getDrawableId",
        "createView",
        "Lkotlin/Pair;",
        "Landroid/view/View;",
        "Lkotlin/Function1;",
        "",
        "context",
        "Landroid/content/Context;",
        "callback",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final attrColorsId:I

.field private final drawableId:I


# direct methods
.method public constructor <init>(ZII)V
    .locals 1

    const/4 v0, 0x0

    .line 144
    invoke-direct {p0, p1, v0}, Lcom/squareup/noho/CheckType;-><init>(ZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p2, p0, Lcom/squareup/noho/CheckType$ImageCheckType;->drawableId:I

    iput p3, p0, Lcom/squareup/noho/CheckType$ImageCheckType;->attrColorsId:I

    return-void
.end method


# virtual methods
.method public createView(Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Lkotlin/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)",
            "Lkotlin/Pair<",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    new-instance p2, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-direct {p2, p1}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;)V

    .line 151
    iget v0, p0, Lcom/squareup/noho/CheckType$ImageCheckType;->attrColorsId:I

    invoke-static {p1, v0}, Lcom/squareup/util/StyledAttributesKt;->getStyleResAsTheme(Landroid/content/Context;I)Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 152
    invoke-virtual {p2}, Landroidx/appcompat/widget/AppCompatImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    const-string v2, "context.classLoader"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget v2, p0, Lcom/squareup/noho/CheckType$ImageCheckType;->drawableId:I

    invoke-static {v1, p1, v2, v0}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable(Landroid/content/res/Resources;Ljava/lang/ClassLoader;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 p1, 0x0

    .line 154
    invoke-virtual {p2, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setClickable(Z)V

    const/4 p1, 0x1

    .line 156
    invoke-virtual {p2, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setDuplicateParentStateEnabled(Z)V

    const/4 p1, 0x0

    .line 157
    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 159
    new-instance p1, Lkotlin/Pair;

    sget-object v0, Lcom/squareup/noho/CheckType$ImageCheckType$createView$1;->INSTANCE:Lcom/squareup/noho/CheckType$ImageCheckType$createView$1;

    invoke-direct {p1, p2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method

.method public final getAttrColorsId()I
    .locals 1

    .line 143
    iget v0, p0, Lcom/squareup/noho/CheckType$ImageCheckType;->attrColorsId:I

    return v0
.end method

.method public final getDrawableId()I
    .locals 1

    .line 142
    iget v0, p0, Lcom/squareup/noho/CheckType$ImageCheckType;->drawableId:I

    return v0
.end method
