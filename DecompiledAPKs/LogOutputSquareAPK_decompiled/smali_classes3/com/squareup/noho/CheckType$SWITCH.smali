.class public final Lcom/squareup/noho/CheckType$SWITCH;
.super Lcom/squareup/noho/CheckType;
.source "NohoCheckableRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/CheckType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SWITCH"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoCheckableRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoCheckableRow.kt\ncom/squareup/noho/CheckType$SWITCH\n*L\n1#1,205:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J<\u0010\u0003\u001a\u001a\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u00060\u00042\u0006\u0010\t\u001a\u00020\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u0006H\u0016\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/noho/CheckType$SWITCH;",
        "Lcom/squareup/noho/CheckType;",
        "()V",
        "createView",
        "Lkotlin/Pair;",
        "Landroid/view/View;",
        "Lkotlin/Function1;",
        "",
        "",
        "context",
        "Landroid/content/Context;",
        "callback",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/noho/CheckType$SWITCH;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 175
    new-instance v0, Lcom/squareup/noho/CheckType$SWITCH;

    invoke-direct {v0}, Lcom/squareup/noho/CheckType$SWITCH;-><init>()V

    sput-object v0, Lcom/squareup/noho/CheckType$SWITCH;->INSTANCE:Lcom/squareup/noho/CheckType$SWITCH;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 175
    invoke-direct {p0, v0, v1}, Lcom/squareup/noho/CheckType;-><init>(ZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public createView(Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Lkotlin/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)",
            "Lkotlin/Pair<",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    new-instance v0, Lcom/squareup/noho/NohoSwitch;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoSwitch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 182
    new-instance p1, Lcom/squareup/noho/CheckType$SWITCH$createView$$inlined$apply$lambda$1;

    invoke-direct {p1, p2}, Lcom/squareup/noho/CheckType$SWITCH$createView$$inlined$apply$lambda$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoSwitch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 184
    new-instance p1, Lkotlin/Pair;

    new-instance p2, Lcom/squareup/noho/CheckType$SWITCH$createView$1;

    invoke-direct {p2, v0}, Lcom/squareup/noho/CheckType$SWITCH$createView$1;-><init>(Lcom/squareup/noho/NohoSwitch;)V

    invoke-direct {p1, v0, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method
