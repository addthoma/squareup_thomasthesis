.class public final Lcom/squareup/noho/NohoLabel;
.super Landroid/widget/TextView;
.source "NohoLabel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoLabel$Type;,
        Lcom/squareup/noho/NohoLabel$SpannableFactory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoLabel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoLabel.kt\ncom/squareup/noho/NohoLabel\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,187:1\n37#2,6:188\n37#2,6:194\n*E\n*S KotlinDebug\n*F\n+ 1 NohoLabel.kt\ncom/squareup/noho/NohoLabel\n*L\n98#1,6:188\n63#1,6:194\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\r\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u00020\u0001:\u0002\u001f B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0003\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0015J&\u0010\u0016\u001a\u00020\u00112\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0001\u0010\u0017\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0018\u001a\u00020\u0007H\u0002J\u001a\u0010\u0019\u001a\u00020\u00112\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u0012\u001a\u00020\u001cH\u0016J\u0018\u0010\u001d\u001a\u00020\u00112\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u001e\u001a\u00020\u0007H\u0017J\u0010\u0010\u001d\u001a\u00020\u00112\u0006\u0010\u001e\u001a\u00020\u0007H\u0017R\u001c\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000f\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/noho/NohoLabel;",
        "Landroid/widget/TextView;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "defStyleRes",
        "(Landroid/content/Context;Landroid/util/AttributeSet;II)V",
        "fontSelection",
        "Lcom/squareup/textappearance/FontSelection;",
        "getFontSelection",
        "()Lcom/squareup/textappearance/FontSelection;",
        "setFontSelection",
        "(Lcom/squareup/textappearance/FontSelection;)V",
        "apply",
        "",
        "type",
        "Lcom/squareup/noho/NohoLabel$Type;",
        "textAppearance",
        "Lcom/squareup/textappearance/SquareTextAppearance;",
        "applyToMyProperties",
        "styleAttr",
        "styleRes",
        "setText",
        "text",
        "",
        "Landroid/widget/TextView$BufferType;",
        "setTextAppearance",
        "resId",
        "SpannableFactory",
        "Type",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private fontSelection:Lcom/squareup/textappearance/FontSelection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/noho/NohoLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/noho/NohoLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/noho/NohoLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-static {p1, p2, p3, p4}, Lcom/squareup/noho/NohoLabelKt;->access$defStyleFromLabelType(Landroid/content/Context;Landroid/util/AttributeSet;II)I

    move-result v0

    .line 33
    invoke-direct {p0, p1, p2, v0, p4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 62
    invoke-virtual {p0}, Lcom/squareup/noho/NohoLabel;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    invoke-static {v0}, Lcom/squareup/marketfont/MarketUtils;->configureOptimalPaintFlags(Landroid/graphics/Paint;)V

    .line 65
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoLabel:[I

    const-string v1, "R.styleable.NohoLabel"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string v0, "a"

    .line 196
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    sget v0, Lcom/squareup/noho/R$styleable;->NohoLabel_sqLabelType:I

    invoke-static {}, Lcom/squareup/noho/NohoLabel$Type;->values()[Lcom/squareup/noho/NohoLabel$Type;

    move-result-object v1

    check-cast v1, [Ljava/lang/Enum;

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;I[Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLabel$Type;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {v0}, Lcom/squareup/noho/NohoLabel$Type;->getStyleAttr()I

    move-result p1

    invoke-virtual {v0}, Lcom/squareup/noho/NohoLabel$Type;->getStyleRes()I

    move-result p3

    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/noho/NohoLabel;->applyToMyProperties(Landroid/util/AttributeSet;II)V

    goto :goto_0

    .line 73
    :cond_0
    invoke-direct {p0, p2, p3, p4}, Lcom/squareup/noho/NohoLabel;->applyToMyProperties(Landroid/util/AttributeSet;II)V

    .line 75
    :goto_0
    new-instance p1, Lcom/squareup/noho/NohoLabel$SpannableFactory;

    invoke-direct {p1, p0}, Lcom/squareup/noho/NohoLabel$SpannableFactory;-><init>(Lcom/squareup/noho/NohoLabel;)V

    check-cast p1, Landroid/text/Spannable$Factory;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoLabel;->setSpannableFactory(Landroid/text/Spannable$Factory;)V

    return-void

    :catchall_0
    move-exception p2

    .line 198
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/4 p2, 0x0

    .line 30
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    .line 31
    sget p3, Lcom/squareup/noho/R$attr;->sqLabelStyle:I

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    .line 32
    sget p4, Lcom/squareup/noho/R$style;->Widget_Noho_Label_Body:I

    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/noho/NohoLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method private final applyToMyProperties(Landroid/util/AttributeSet;II)V
    .locals 4

    .line 98
    invoke-virtual {p0}, Lcom/squareup/noho/NohoLabel;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    sget-object v2, Lcom/squareup/noho/R$styleable;->NohoLabel:[I

    const-string v3, "R.styleable.NohoLabel"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 188
    invoke-virtual {v0, p1, v2, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    :try_start_0
    const-string p3, "a"

    .line 190
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    sget p3, Lcom/squareup/noho/R$styleable;->NohoLabel_android_textAppearance:I

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 107
    sget-object p2, Lcom/squareup/textappearance/SquareTextAppearance;->Companion:Lcom/squareup/textappearance/SquareTextAppearance$Companion;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoLabel;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v0, p1, p3}, Lcom/squareup/textappearance/SquareTextAppearance$Companion;->loadFromStyle(Landroid/content/Context;Landroid/util/AttributeSet;I)Lcom/squareup/textappearance/SquareTextAppearance;

    move-result-object p1

    .line 108
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoLabel;->apply(Lcom/squareup/textappearance/SquareTextAppearance;)V

    return-void

    :catchall_0
    move-exception p1

    .line 192
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method


# virtual methods
.method public final apply(Lcom/squareup/noho/NohoLabel$Type;)V
    .locals 2

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-virtual {p1}, Lcom/squareup/noho/NohoLabel$Type;->getStyleAttr()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/noho/NohoLabel$Type;->getStyleRes()I

    move-result p1

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0, p1}, Lcom/squareup/noho/NohoLabel;->applyToMyProperties(Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public final apply(Lcom/squareup/textappearance/SquareTextAppearance;)V
    .locals 2

    const-string v0, "textAppearance"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p1}, Lcom/squareup/textappearance/SquareTextAppearance;->getFontSelection()Lcom/squareup/textappearance/FontSelection;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/noho/NohoLabel;->fontSelection:Lcom/squareup/textappearance/FontSelection;

    .line 113
    invoke-virtual {p0}, Lcom/squareup/noho/NohoLabel;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/textappearance/SquareTextAppearance;->getTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoLabel;->setTypeface(Landroid/graphics/Typeface;)V

    .line 114
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/textappearance/SquareTextAppearance;->getTextSize()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 115
    invoke-virtual {p1}, Lcom/squareup/textappearance/SquareTextAppearance;->getTextSize()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/noho/NohoLabel;->setTextSize(IF)V

    .line 117
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/noho/NohoLabel;->getTransformationMethod()Landroid/text/method/TransformationMethod;

    move-result-object v0

    instance-of v0, v0, Landroidx/appcompat/text/AllCapsTransformationMethod;

    .line 118
    invoke-virtual {p1}, Lcom/squareup/textappearance/SquareTextAppearance;->getTextAllCaps()Z

    move-result v1

    if-eq v0, v1, :cond_3

    .line 120
    invoke-virtual {p1}, Lcom/squareup/textappearance/SquareTextAppearance;->getTextAllCaps()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroidx/appcompat/text/AllCapsTransformationMethod;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoLabel;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/appcompat/text/AllCapsTransformationMethod;-><init>(Landroid/content/Context;)V

    check-cast v0, Landroid/text/method/TransformationMethod;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoLabel;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 122
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/textappearance/SquareTextAppearance;->getLetterSpacing()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoLabel;->setLetterSpacing(F)V

    .line 123
    invoke-virtual {p1}, Lcom/squareup/textappearance/SquareTextAppearance;->getTextColor()Landroid/content/res/ColorStateList;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoLabel;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_4
    return-void
.end method

.method public final getFontSelection()Lcom/squareup/textappearance/FontSelection;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/noho/NohoLabel;->fontSelection:Lcom/squareup/textappearance/FontSelection;

    return-object v0
.end method

.method public final setFontSelection(Lcom/squareup/textappearance/FontSelection;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/squareup/noho/NohoLabel;->fontSelection:Lcom/squareup/textappearance/FontSelection;

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    instance-of v0, p1, Landroid/text/Spannable;

    if-eqz v0, :cond_0

    sget-object p2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    .line 80
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    return-void
.end method

.method public setTextAppearance(I)V
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        message = "Don\'t use Android text appearances. Use SquareTextAppearances with sqFontSelection."
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "apply(SquareTextAppearance.loadFromStyle(context, resId))"
            imports = {
                "com.squareup.textappearance.SquareTextAppearance"
            }
        .end subannotation
    .end annotation

    .line 148
    invoke-virtual {p0}, Lcom/squareup/noho/NohoLabel;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p1}, Lcom/squareup/noho/NohoLabel;->setTextAppearance(Landroid/content/Context;I)V

    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 1
    .annotation runtime Lkotlin/Deprecated;
        message = "Don\'t use Android text appearances. Use SquareTextAppearances with sqFontSelection."
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "apply(SquareTextAppearance.loadFromStyle(context, resId))"
            imports = {
                "com.squareup.textappearance.SquareTextAppearance"
            }
        .end subannotation
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    sget-object v0, Lcom/squareup/textappearance/SquareTextAppearance;->Companion:Lcom/squareup/textappearance/SquareTextAppearance$Companion;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/textappearance/SquareTextAppearance$Companion;->loadFromStyle(Landroid/content/Context;I)Lcom/squareup/textappearance/SquareTextAppearance;

    move-result-object p1

    .line 162
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoLabel;->apply(Lcom/squareup/textappearance/SquareTextAppearance;)V

    return-void
.end method
