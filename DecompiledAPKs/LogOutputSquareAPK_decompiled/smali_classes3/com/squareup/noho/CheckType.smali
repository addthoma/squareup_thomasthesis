.class public abstract Lcom/squareup/noho/CheckType;
.super Ljava/lang/Object;
.source "NohoCheckableRow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/CheckType$ImageCheckType;,
        Lcom/squareup/noho/CheckType$RADIO;,
        Lcom/squareup/noho/CheckType$CHECK;,
        Lcom/squareup/noho/CheckType$SWITCH;,
        Lcom/squareup/noho/CheckType$Custom;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0005\u000f\u0010\u0011\u0012\u0013B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J<\u0010\u0007\u001a\u001a\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000b0\n0\u00082\u0006\u0010\u000c\u001a\u00020\r2\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000b0\nH&R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0003\u0014\u0015\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/noho/CheckType;",
        "",
        "canDeselect",
        "",
        "(Z)V",
        "getCanDeselect",
        "()Z",
        "createView",
        "Lkotlin/Pair;",
        "Landroid/view/View;",
        "Lkotlin/Function1;",
        "",
        "context",
        "Landroid/content/Context;",
        "callback",
        "CHECK",
        "Custom",
        "ImageCheckType",
        "RADIO",
        "SWITCH",
        "Lcom/squareup/noho/CheckType$ImageCheckType;",
        "Lcom/squareup/noho/CheckType$SWITCH;",
        "Lcom/squareup/noho/CheckType$Custom;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final canDeselect:Z


# direct methods
.method private constructor <init>(Z)V
    .locals 0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/noho/CheckType;->canDeselect:Z

    return-void
.end method

.method public synthetic constructor <init>(ZLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 133
    invoke-direct {p0, p1}, Lcom/squareup/noho/CheckType;-><init>(Z)V

    return-void
.end method


# virtual methods
.method public abstract createView(Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Lkotlin/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)",
            "Lkotlin/Pair<",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end method

.method public final getCanDeselect()Z
    .locals 1

    .line 133
    iget-boolean v0, p0, Lcom/squareup/noho/CheckType;->canDeselect:Z

    return v0
.end method
