.class public final Lcom/squareup/noho/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final noho_background:I = 0x7f0601de

.field public static final noho_banner_background:I = 0x7f0601df

.field public static final noho_button_secondary_border_disabled:I = 0x7f0601e0

.field public static final noho_button_secondary_border_enabled:I = 0x7f0601e1

.field public static final noho_button_secondary_border_pressed:I = 0x7f0601e2

.field public static final noho_button_tertiary_border_disabled:I = 0x7f0601e3

.field public static final noho_button_tertiary_border_enabled:I = 0x7f0601e4

.field public static final noho_button_tertiary_border_pressed:I = 0x7f0601e5

.field public static final noho_color_selectable_label:I = 0x7f0601e6

.field public static final noho_color_selectable_value:I = 0x7f0601e7

.field public static final noho_color_selector_destructive_button_subtitle:I = 0x7f0601e8

.field public static final noho_color_selector_destructive_button_text:I = 0x7f0601e9

.field public static final noho_color_selector_dialog_button_text:I = 0x7f0601ea

.field public static final noho_color_selector_link_button_text:I = 0x7f0601eb

.field public static final noho_color_selector_primary_button_subtitle:I = 0x7f0601ec

.field public static final noho_color_selector_primary_button_text:I = 0x7f0601ed

.field public static final noho_color_selector_row_action_icon:I = 0x7f0601ee

.field public static final noho_color_selector_row_action_link:I = 0x7f0601ef

.field public static final noho_color_selector_row_description:I = 0x7f0601f0

.field public static final noho_color_selector_row_icon:I = 0x7f0601f1

.field public static final noho_color_selector_row_label:I = 0x7f0601f2

.field public static final noho_color_selector_row_value:I = 0x7f0601f3

.field public static final noho_color_selector_secondary_button_border:I = 0x7f0601f4

.field public static final noho_color_selector_secondary_button_subtitle:I = 0x7f0601f5

.field public static final noho_color_selector_secondary_button_text:I = 0x7f0601f6

.field public static final noho_color_selector_tertiary_button_border:I = 0x7f0601f7

.field public static final noho_color_selector_tertiary_button_subtitle:I = 0x7f0601f8

.field public static final noho_color_selector_tertiary_button_text:I = 0x7f0601f9

.field public static final noho_color_selector_time_picker:I = 0x7f0601fa

.field public static final noho_color_selector_topbar_icon:I = 0x7f0601fb

.field public static final noho_divider_hairline:I = 0x7f0601fc

.field public static final noho_dropdown_button:I = 0x7f0601fd

.field public static final noho_dropdown_menu_divider:I = 0x7f0601fe

.field public static final noho_dropdown_menu_text_color:I = 0x7f0601ff

.field public static final noho_dropdown_selected_item_background:I = 0x7f060200

.field public static final noho_dropdown_selected_text_color:I = 0x7f060201

.field public static final noho_edit_background_border:I = 0x7f060202

.field public static final noho_edit_background_color:I = 0x7f060203

.field public static final noho_edit_background_disabled_color:I = 0x7f060204

.field public static final noho_edit_background_inner_shadow:I = 0x7f060205

.field public static final noho_edit_background_underline_error:I = 0x7f060206

.field public static final noho_edit_background_underline_selected:I = 0x7f060207

.field public static final noho_edit_background_underline_validated:I = 0x7f060208

.field public static final noho_edit_cursor:I = 0x7f060209

.field public static final noho_edit_hint_color:I = 0x7f06020a

.field public static final noho_edit_note_color:I = 0x7f06020b

.field public static final noho_edit_text_color:I = 0x7f06020c

.field public static final noho_edit_view_password_color:I = 0x7f06020d

.field public static final noho_icon_color:I = 0x7f06020e

.field public static final noho_inner_shadow_color:I = 0x7f06020f

.field public static final noho_input_details:I = 0x7f060210

.field public static final noho_input_details_link:I = 0x7f060211

.field public static final noho_input_error:I = 0x7f060212

.field public static final noho_label_default:I = 0x7f060213

.field public static final noho_notification_balloon:I = 0x7f060214

.field public static final noho_notification_fatal_balloon:I = 0x7f060215

.field public static final noho_notification_highpri_balloon:I = 0x7f060216

.field public static final noho_notification_text:I = 0x7f060217

.field public static final noho_number_picker_selected:I = 0x7f060218

.field public static final noho_number_picker_unselected:I = 0x7f060219

.field public static final noho_recycler_edges:I = 0x7f06021a

.field public static final noho_row_action_icon:I = 0x7f06021b

.field public static final noho_row_activated_action_icon:I = 0x7f06021c

.field public static final noho_row_activated_icon:I = 0x7f06021d

.field public static final noho_row_disabled_action_icon:I = 0x7f06021e

.field public static final noho_row_disabled_icon:I = 0x7f06021f

.field public static final noho_row_icon:I = 0x7f060220

.field public static final noho_row_legacy_icon:I = 0x7f060221

.field public static final noho_row_legacy_icon_destructive:I = 0x7f060222

.field public static final noho_selectable_label_checked:I = 0x7f060223

.field public static final noho_selectable_label_default:I = 0x7f060224

.field public static final noho_selectable_label_disabled:I = 0x7f060225

.field public static final noho_selectable_value_checked:I = 0x7f060226

.field public static final noho_selectable_value_default:I = 0x7f060227

.field public static final noho_selectable_value_disabled:I = 0x7f060228

.field public static final noho_standard_black:I = 0x7f060229

.field public static final noho_standard_blue:I = 0x7f06022a

.field public static final noho_standard_brown:I = 0x7f06022b

.field public static final noho_standard_cyan:I = 0x7f06022c

.field public static final noho_standard_cyan_dark:I = 0x7f06022d

.field public static final noho_standard_gold:I = 0x7f06022e

.field public static final noho_standard_gray_10:I = 0x7f06022f

.field public static final noho_standard_gray_20:I = 0x7f060230

.field public static final noho_standard_gray_30:I = 0x7f060231

.field public static final noho_standard_gray_40:I = 0x7f060232

.field public static final noho_standard_gray_50:I = 0x7f060233

.field public static final noho_standard_gray_60:I = 0x7f060234

.field public static final noho_standard_gray_70:I = 0x7f060235

.field public static final noho_standard_gray_80:I = 0x7f060236

.field public static final noho_standard_green:I = 0x7f060237

.field public static final noho_standard_indigo:I = 0x7f060238

.field public static final noho_standard_lilac:I = 0x7f060239

.field public static final noho_standard_lime:I = 0x7f06023a

.field public static final noho_standard_maroon:I = 0x7f06023b

.field public static final noho_standard_night_almost_black:I = 0x7f06023c

.field public static final noho_standard_night_dark:I = 0x7f06023d

.field public static final noho_standard_night_extra_dark:I = 0x7f06023e

.field public static final noho_standard_night_gray:I = 0x7f06023f

.field public static final noho_standard_night_gray_35_opacity:I = 0x7f060240

.field public static final noho_standard_night_gray_pressed:I = 0x7f060241

.field public static final noho_standard_orange:I = 0x7f060242

.field public static final noho_standard_pink:I = 0x7f060243

.field public static final noho_standard_plum:I = 0x7f060244

.field public static final noho_standard_purple:I = 0x7f060245

.field public static final noho_standard_system_error:I = 0x7f060246

.field public static final noho_standard_system_error_dark:I = 0x7f060247

.field public static final noho_standard_system_success:I = 0x7f060248

.field public static final noho_standard_system_warning:I = 0x7f060249

.field public static final noho_standard_system_yellow:I = 0x7f06024a

.field public static final noho_standard_teal:I = 0x7f06024b

.field public static final noho_standard_white:I = 0x7f06024c

.field public static final noho_switch_base_off_color:I = 0x7f06024d

.field public static final noho_switch_base_on_color:I = 0x7f06024e

.field public static final noho_switch_thumb_color:I = 0x7f06024f

.field public static final noho_text_banner:I = 0x7f060250

.field public static final noho_text_body:I = 0x7f060251

.field public static final noho_text_button_buyer_facing_secondary:I = 0x7f060252

.field public static final noho_text_button_destructive:I = 0x7f060253

.field public static final noho_text_button_link_disabled:I = 0x7f060254

.field public static final noho_text_button_link_enabled:I = 0x7f060255

.field public static final noho_text_button_primary_disabled:I = 0x7f060256

.field public static final noho_text_button_primary_enabled:I = 0x7f060257

.field public static final noho_text_button_primary_subtitle_disabled:I = 0x7f060258

.field public static final noho_text_button_primary_subtitle_enabled:I = 0x7f060259

.field public static final noho_text_button_secondary_disabled:I = 0x7f06025a

.field public static final noho_text_button_secondary_enabled:I = 0x7f06025b

.field public static final noho_text_button_secondary_pressed:I = 0x7f06025c

.field public static final noho_text_button_secondary_subtitle_disabled:I = 0x7f06025d

.field public static final noho_text_button_secondary_subtitle_enabled:I = 0x7f06025e

.field public static final noho_text_button_secondary_subtitle_pressed:I = 0x7f06025f

.field public static final noho_text_button_tertiary_disabled:I = 0x7f060260

.field public static final noho_text_button_tertiary_enabled:I = 0x7f060261

.field public static final noho_text_button_tertiary_pressed:I = 0x7f060262

.field public static final noho_text_button_tertiary_subtitle_disabled:I = 0x7f060263

.field public static final noho_text_button_tertiary_subtitle_enabled:I = 0x7f060264

.field public static final noho_text_button_tertiary_subtitle_pressed:I = 0x7f060265

.field public static final noho_text_button_tutorial_secondary_enabled:I = 0x7f060266

.field public static final noho_text_help:I = 0x7f060267

.field public static final noho_text_help_link:I = 0x7f060268

.field public static final noho_text_hint:I = 0x7f060269

.field public static final noho_text_input:I = 0x7f06026a

.field public static final noho_text_label:I = 0x7f06026b

.field public static final noho_text_row_action_link:I = 0x7f06026c

.field public static final noho_text_row_activated_label:I = 0x7f06026d

.field public static final noho_text_row_activated_value:I = 0x7f06026e

.field public static final noho_text_row_description:I = 0x7f06026f

.field public static final noho_text_row_disabled_action_link:I = 0x7f060270

.field public static final noho_text_row_disabled_description:I = 0x7f060271

.field public static final noho_text_row_disabled_label:I = 0x7f060272

.field public static final noho_text_row_disabled_value:I = 0x7f060273

.field public static final noho_text_row_label:I = 0x7f060274

.field public static final noho_text_row_text_icon:I = 0x7f060275

.field public static final noho_text_row_value:I = 0x7f060276

.field public static final noho_text_subheader:I = 0x7f060277

.field public static final noho_text_topbar_action_disabled:I = 0x7f060278

.field public static final noho_text_topbar_action_enabled:I = 0x7f060279

.field public static final noho_text_topbar_title:I = 0x7f06027a

.field public static final noho_text_warning:I = 0x7f06027b

.field public static final noho_topbar_background:I = 0x7f06027c

.field public static final noho_topbar_divider:I = 0x7f06027d

.field public static final noho_topbar_icon_disabled:I = 0x7f06027e

.field public static final noho_topbar_icon_enabled:I = 0x7f06027f


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
