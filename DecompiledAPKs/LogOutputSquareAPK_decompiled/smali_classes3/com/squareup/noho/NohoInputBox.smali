.class public final Lcom/squareup/noho/NohoInputBox;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "NohoInputBox.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoInputBox$Suggestion;,
        Lcom/squareup/noho/NohoInputBox$Error;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoInputBox.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoInputBox.kt\ncom/squareup/noho/NohoInputBox\n+ 2 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n+ 3 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,330:1\n192#1,6:331\n180#1,18:337\n185#1,13:355\n208#1,18:368\n140#2,2:386\n34#3,9:388\n*E\n*S KotlinDebug\n*F\n+ 1 NohoInputBox.kt\ncom/squareup/noho/NohoInputBox\n*L\n180#1,6:331\n281#1,2:386\n91#1,9:388\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0083\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u001e\n\u0002\u0008\u0007*\u0001\n\u0018\u00002\u00020\u0001:\u0002OPB%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0006\u00105\u001a\u000206J$\u00107\u001a\u0002062\u0019\u0008\u0004\u00108\u001a\u0013\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020604\u00a2\u0006\u0002\u00089H\u0086\u0008J\u001c\u0010:\u001a\u00020\u001e2\u0008\u0008\u0001\u0010;\u001a\u00020\u00072\u0008\u0008\u0001\u0010<\u001a\u00020\u0007H\u0002J\u0006\u0010=\u001a\u000206J,\u0010>\u001a\u0002062$\u00108\u001a \u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a\u0012\u0006\u0012\u0004\u0018\u00010\u001b0\u0018j\u0008\u0012\u0004\u0012\u00020\u001b`\u001cJ3\u0010>\u001a\u0002062\u0008\u0008\u0001\u0010?\u001a\u00020\u00072\u0008\u0008\u0002\u0010@\u001a\u00020\u001a2\u0014\u0008\u0004\u0010A\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a04H\u0086\u0008J1\u0010>\u001a\u0002062\u0006\u0010B\u001a\u00020\'2\u0008\u0008\u0002\u0010@\u001a\u00020\u001a2\u0014\u0008\u0004\u0010A\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a04H\u0086\u0008J\u0008\u0010C\u001a\u000206H\u0002J\u0012\u0010D\u001a\u0002062\u0008\u0008\u0002\u0010E\u001a\u00020\u001aH\u0002J,\u0010F\u001a\u0002062$\u00108\u001a \u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a\u0012\u0006\u0012\u0004\u0018\u00010%0\u0018j\u0008\u0012\u0004\u0012\u00020%`\u001cJ1\u0010F\u001a\u0002062\u0006\u0010B\u001a\u00020%2\u0008\u0008\u0002\u0010@\u001a\u00020\u001a2\u0014\u0008\u0004\u0010A\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a04H\u0086\u0008J1\u0010F\u001a\u0002062\u0006\u0010B\u001a\u00020\'2\u0008\u0008\u0002\u0010@\u001a\u00020\u001a2\u0014\u0008\u0004\u0010A\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a04H\u0086\u0008J\u001a\u0010G\u001a\u0002062\u0012\u00108\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a04JM\u0010H\u001a\u0004\u0018\u0001HI\"\u0004\u0008\u0000\u0010I*&\u0012\"\u0012 \u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a\u0012\u0006\u0012\u0004\u0018\u0001HI0\u0018j\u0008\u0012\u0004\u0012\u0002HI`\u001c0J2\u0006\u0010K\u001a\u00020\u001a2\u0006\u0010(\u001a\u00020\u0019H\u0002\u00a2\u0006\u0002\u0010LJ\u000c\u0010M\u001a\u000206*\u00020\u001bH\u0002J\u000c\u0010N\u001a\u000206*\u00020%H\u0002R\u0010\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000bR\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u000e\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000f\u001a\u00020\u00108\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0011\u0010\u0012\u001a\u0004\u0008\u0013\u0010\u0014R\u0012\u0010\u0015\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R2\u0010\u0016\u001a&\u0012\"\u0012 \u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a\u0012\u0006\u0012\u0004\u0018\u00010\u001b0\u0018j\u0008\u0012\u0004\u0012\u00020\u001b`\u001c0\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u001d\u001a\u00020\u001e8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008!\u0010\"\u001a\u0004\u0008\u001f\u0010 R\u000e\u0010#\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010$\u001a&\u0012\"\u0012 \u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a\u0012\u0006\u0012\u0004\u0018\u00010%0\u0018j\u0008\u0012\u0004\u0012\u00020%`\u001c0\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010(\u001a\u00020\'2\u0006\u0010&\u001a\u00020\'8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008)\u0010*\"\u0004\u0008+\u0010,R$\u0010-\u001a\u00020\'2\u0006\u0010&\u001a\u00020\'8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008.\u0010*\"\u0004\u0008/\u0010,R\u001b\u00100\u001a\u00020\u001e8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u00082\u0010\"\u001a\u0004\u00081\u0010 R\u001c\u00103\u001a\u0010\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a\u0018\u000104X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006Q"
    }
    d2 = {
        "Lcom/squareup/noho/NohoInputBox;",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "checkerListener",
        "com/squareup/noho/NohoInputBox$checkerListener$1",
        "Lcom/squareup/noho/NohoInputBox$checkerListener$1;",
        "details",
        "Landroidx/appcompat/widget/AppCompatTextView;",
        "detailsAppearance",
        "edit",
        "Lcom/squareup/noho/NohoEditRow;",
        "edit$annotations",
        "()V",
        "getEdit",
        "()Lcom/squareup/noho/NohoEditRow;",
        "errorAppearance",
        "errorCheckers",
        "",
        "Lkotlin/Function2;",
        "",
        "",
        "Lcom/squareup/noho/NohoInputBox$Error;",
        "Lcom/squareup/noho/Evaluation;",
        "errorPlugin",
        "Lcom/squareup/noho/HideablePlugin;",
        "getErrorPlugin",
        "()Lcom/squareup/noho/HideablePlugin;",
        "errorPlugin$delegate",
        "Lkotlin/Lazy;",
        "label",
        "suggestionCheckers",
        "Lcom/squareup/noho/NohoInputBox$Suggestion;",
        "value",
        "",
        "text",
        "getText",
        "()Ljava/lang/String;",
        "setText",
        "(Ljava/lang/String;)V",
        "title",
        "getTitle",
        "setTitle",
        "validatedPlugin",
        "getValidatedPlugin",
        "validatedPlugin$delegate",
        "validationChecker",
        "Lkotlin/Function1;",
        "checkAll",
        "",
        "configureEdit",
        "block",
        "Lkotlin/ExtensionFunctionType;",
        "createStatusPlugin",
        "drawableId",
        "colorId",
        "disableActionsExceptPaste",
        "error",
        "id",
        "checkAlways",
        "checker",
        "message",
        "hideDetails",
        "refreshState",
        "ignoreFocus",
        "suggestion",
        "validation",
        "firstNotNull",
        "T",
        "",
        "isFocused",
        "(Ljava/util/Collection;ZLjava/lang/CharSequence;)Ljava/lang/Object;",
        "show",
        "showSuggestion",
        "Error",
        "Suggestion",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final checkerListener:Lcom/squareup/noho/NohoInputBox$checkerListener$1;

.field private final details:Landroidx/appcompat/widget/AppCompatTextView;

.field private detailsAppearance:I

.field private final edit:Lcom/squareup/noho/NohoEditRow;

.field private errorAppearance:I

.field private errorCheckers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lkotlin/jvm/functions/Function2<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/noho/NohoInputBox$Error;",
            ">;>;"
        }
    .end annotation
.end field

.field private final errorPlugin$delegate:Lkotlin/Lazy;

.field private final label:Landroidx/appcompat/widget/AppCompatTextView;

.field private suggestionCheckers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lkotlin/jvm/functions/Function2<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/noho/NohoInputBox$Suggestion;",
            ">;>;"
        }
    .end annotation
.end field

.field private final validatedPlugin$delegate:Lkotlin/Lazy;

.field private validationChecker:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoInputBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoInputBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    new-instance v0, Landroidx/appcompat/widget/AppCompatTextView;

    invoke-direct {v0, p1}, Landroidx/appcompat/widget/AppCompatTextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/noho/NohoInputBox;->label:Landroidx/appcompat/widget/AppCompatTextView;

    .line 75
    new-instance v0, Lcom/squareup/noho/NohoEditRow;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoEditRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    .line 76
    new-instance v0, Landroidx/appcompat/widget/AppCompatTextView;

    invoke-direct {v0, p1}, Landroidx/appcompat/widget/AppCompatTextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/noho/NohoInputBox;->details:Landroidx/appcompat/widget/AppCompatTextView;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/noho/NohoInputBox;->errorCheckers:Ljava/util/List;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/noho/NohoInputBox;->suggestionCheckers:Ljava/util/List;

    .line 84
    new-instance v0, Lcom/squareup/noho/NohoInputBox$checkerListener$1;

    invoke-direct {v0, p0}, Lcom/squareup/noho/NohoInputBox$checkerListener$1;-><init>(Lcom/squareup/noho/NohoInputBox;)V

    iput-object v0, p0, Lcom/squareup/noho/NohoInputBox;->checkerListener:Lcom/squareup/noho/NohoInputBox$checkerListener$1;

    .line 89
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->details:Landroidx/appcompat/widget/AppCompatTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 91
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoInputBox:[I

    const-string v1, "R.styleable.NohoInputBox"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 391
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    :try_start_0
    const-string p3, "a"

    .line 393
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    sget p3, Lcom/squareup/noho/R$styleable;->NohoInputBox_sqLabelAppearance:I

    invoke-virtual {p2, p3, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p3

    .line 93
    sget v0, Lcom/squareup/noho/R$styleable;->NohoInputBox_sqDetailsAppearance:I

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/noho/NohoInputBox;->access$setDetailsAppearance$p(Lcom/squareup/noho/NohoInputBox;I)V

    .line 94
    sget v0, Lcom/squareup/noho/R$styleable;->NohoInputBox_sqErrorAppearance:I

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/noho/NohoInputBox;->access$setErrorAppearance$p(Lcom/squareup/noho/NohoInputBox;I)V

    .line 95
    invoke-static {p0}, Lcom/squareup/noho/NohoInputBox;->access$getLabel$p(Lcom/squareup/noho/NohoInputBox;)Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0, p3}, Landroidx/core/widget/TextViewCompat;->setTextAppearance(Landroid/widget/TextView;I)V

    .line 97
    invoke-static {p0}, Lcom/squareup/noho/NohoInputBox;->access$getLabel$p(Lcom/squareup/noho/NohoInputBox;)Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object p3

    sget v0, Lcom/squareup/noho/R$styleable;->NohoInputBox_sqLabel:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p3, v0}, Landroidx/appcompat/widget/AppCompatTextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    sget p3, Lcom/squareup/noho/R$styleable;->NohoInputBox_sqClearButton:I

    invoke-virtual {p2, p3, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/noho/NohoInputBox;->getEdit()Lcom/squareup/noho/NohoEditRow;

    move-result-object p3

    new-instance v0, Lcom/squareup/noho/ClearPlugin;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xe

    const/4 v8, 0x0

    move-object v2, v0

    move-object v3, p1

    invoke-direct/range {v2 .. v8}, Lcom/squareup/noho/ClearPlugin;-><init>(Landroid/content/Context;ILjava/util/Set;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {p3, v0}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 101
    :cond_1
    sget-object p3, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 104
    iget-object p2, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    sget-object p3, Lcom/squareup/noho/NohoEditRow$PositionInList;->FIRST:Lcom/squareup/noho/NohoEditRow$PositionInList;

    invoke-virtual {p2, p3}, Lcom/squareup/noho/NohoEditRow;->setPositionInList(Lcom/squareup/noho/NohoEditRow$PositionInList;)V

    .line 106
    iget-object p2, p0, Lcom/squareup/noho/NohoInputBox;->label:Landroidx/appcompat/widget/AppCompatTextView;

    sget p3, Lcom/squareup/noho/R$id;->label:I

    invoke-virtual {p2, p3}, Landroidx/appcompat/widget/AppCompatTextView;->setId(I)V

    .line 107
    iget-object p2, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    sget p3, Lcom/squareup/noho/R$id;->edit:I

    invoke-virtual {p2, p3}, Lcom/squareup/noho/NohoEditRow;->setId(I)V

    .line 108
    iget-object p2, p0, Lcom/squareup/noho/NohoInputBox;->details:Landroidx/appcompat/widget/AppCompatTextView;

    sget p3, Lcom/squareup/noho/R$id;->details:I

    invoke-virtual {p2, p3}, Landroidx/appcompat/widget/AppCompatTextView;->setId(I)V

    .line 110
    iget-object p2, p0, Lcom/squareup/noho/NohoInputBox;->label:Landroidx/appcompat/widget/AppCompatTextView;

    check-cast p2, Landroid/view/View;

    new-instance p3, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    const/4 v0, -0x2

    invoke-direct {p3, v1, v0}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    check-cast p3, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, p2, p3}, Lcom/squareup/noho/NohoInputBox;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 111
    iget-object p2, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    check-cast p2, Landroid/view/View;

    new-instance p3, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    invoke-direct {p3, v1, v0}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    check-cast p3, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, p2, p3}, Lcom/squareup/noho/NohoInputBox;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 112
    iget-object p2, p0, Lcom/squareup/noho/NohoInputBox;->details:Landroidx/appcompat/widget/AppCompatTextView;

    check-cast p2, Landroid/view/View;

    new-instance p3, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    invoke-direct {p3, v1, v0}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    check-cast p3, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, p2, p3}, Lcom/squareup/noho/NohoInputBox;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 114
    new-instance p2, Landroidx/constraintlayout/widget/ConstraintSet;

    invoke-direct {p2}, Landroidx/constraintlayout/widget/ConstraintSet;-><init>()V

    .line 115
    move-object p3, p0

    check-cast p3, Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {p2, p3}, Landroidx/constraintlayout/widget/ConstraintSet;->clone(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 117
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/noho/R$dimen;->noho_gap_8:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    .line 119
    sget v3, Lcom/squareup/noho/R$id;->label:I

    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x3

    const/4 v7, 0x0

    move-object v2, p2

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 120
    sget v3, Lcom/squareup/noho/R$id;->label:I

    const/4 v4, 0x6

    const/4 v6, 0x6

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 121
    sget v3, Lcom/squareup/noho/R$id;->label:I

    const/4 v4, 0x7

    const/4 v6, 0x7

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 122
    sget v3, Lcom/squareup/noho/R$id;->label:I

    const/4 v4, 0x4

    sget v5, Lcom/squareup/noho/R$id;->edit:I

    const/4 v6, 0x3

    move v7, p1

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 124
    sget v3, Lcom/squareup/noho/R$id;->edit:I

    const/4 v4, 0x3

    sget v5, Lcom/squareup/noho/R$id;->label:I

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 125
    sget v3, Lcom/squareup/noho/R$id;->edit:I

    const/4 v4, 0x6

    const/4 v5, 0x0

    const/4 v6, 0x6

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 126
    sget v3, Lcom/squareup/noho/R$id;->edit:I

    const/4 v4, 0x7

    const/4 v6, 0x7

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 127
    sget v3, Lcom/squareup/noho/R$id;->edit:I

    const/4 v4, 0x4

    sget v5, Lcom/squareup/noho/R$id;->details:I

    const/4 v6, 0x3

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 129
    sget v3, Lcom/squareup/noho/R$id;->details:I

    const/4 v4, 0x3

    sget v5, Lcom/squareup/noho/R$id;->edit:I

    const/4 v6, 0x4

    move v7, p1

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 130
    sget v3, Lcom/squareup/noho/R$id;->details:I

    const/4 v4, 0x6

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 131
    sget v3, Lcom/squareup/noho/R$id;->details:I

    const/4 v4, 0x7

    const/4 v6, 0x7

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 132
    sget v3, Lcom/squareup/noho/R$id;->details:I

    const/4 v4, 0x4

    const/4 v6, 0x4

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 133
    invoke-virtual {p2, p3}, Landroidx/constraintlayout/widget/ConstraintSet;->applyTo(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 135
    iget-object p1, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    iget-object p2, p0, Lcom/squareup/noho/NohoInputBox;->checkerListener:Lcom/squareup/noho/NohoInputBox$checkerListener$1;

    check-cast p2, Landroid/text/TextWatcher;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 136
    iget-object p1, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    new-instance p2, Lcom/squareup/noho/NohoInputBox$2;

    invoke-direct {p2, p0}, Lcom/squareup/noho/NohoInputBox$2;-><init>(Lcom/squareup/noho/NohoInputBox;)V

    check-cast p2, Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditRow;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const/4 p1, 0x1

    const/4 p2, 0x0

    .line 138
    invoke-static {p0, v1, p1, p2}, Lcom/squareup/noho/NohoInputBox;->refreshState$default(Lcom/squareup/noho/NohoInputBox;ZILjava/lang/Object;)V

    .line 141
    new-instance p1, Lcom/squareup/noho/NohoInputBox$validatedPlugin$2;

    invoke-direct {p1, p0}, Lcom/squareup/noho/NohoInputBox$validatedPlugin$2;-><init>(Lcom/squareup/noho/NohoInputBox;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/noho/NohoInputBox;->validatedPlugin$delegate:Lkotlin/Lazy;

    .line 148
    new-instance p1, Lcom/squareup/noho/NohoInputBox$errorPlugin$2;

    invoke-direct {p1, p0}, Lcom/squareup/noho/NohoInputBox$errorPlugin$2;-><init>(Lcom/squareup/noho/NohoInputBox;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/noho/NohoInputBox;->errorPlugin$delegate:Lkotlin/Lazy;

    return-void

    :catchall_0
    move-exception p1

    .line 395
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 59
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 60
    sget p3, Lcom/squareup/noho/R$attr;->sqInputBoxStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoInputBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$createStatusPlugin(Lcom/squareup/noho/NohoInputBox;II)Lcom/squareup/noho/HideablePlugin;
    .locals 0

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/NohoInputBox;->createStatusPlugin(II)Lcom/squareup/noho/HideablePlugin;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDetailsAppearance$p(Lcom/squareup/noho/NohoInputBox;)I
    .locals 0

    .line 55
    iget p0, p0, Lcom/squareup/noho/NohoInputBox;->detailsAppearance:I

    return p0
.end method

.method public static final synthetic access$getErrorAppearance$p(Lcom/squareup/noho/NohoInputBox;)I
    .locals 0

    .line 55
    iget p0, p0, Lcom/squareup/noho/NohoInputBox;->errorAppearance:I

    return p0
.end method

.method public static final synthetic access$getLabel$p(Lcom/squareup/noho/NohoInputBox;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/noho/NohoInputBox;->label:Landroidx/appcompat/widget/AppCompatTextView;

    return-object p0
.end method

.method public static final synthetic access$setDetailsAppearance$p(Lcom/squareup/noho/NohoInputBox;I)V
    .locals 0

    .line 55
    iput p1, p0, Lcom/squareup/noho/NohoInputBox;->detailsAppearance:I

    return-void
.end method

.method public static final synthetic access$setErrorAppearance$p(Lcom/squareup/noho/NohoInputBox;I)V
    .locals 0

    .line 55
    iput p1, p0, Lcom/squareup/noho/NohoInputBox;->errorAppearance:I

    return-void
.end method

.method private final createStatusPlugin(II)Lcom/squareup/noho/HideablePlugin;
    .locals 8

    .line 160
    invoke-virtual {p0}, Lcom/squareup/noho/NohoInputBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/noho/NohoInputBox;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-static {v0, p1, v2}, Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;->create(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v0, "VectorDrawableCompat.cre\u2026wableId, context.theme)!!"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    invoke-virtual {p0}, Lcom/squareup/noho/NohoInputBox;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    .line 162
    invoke-virtual {p1, p2}, Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;->setTint(I)V

    .line 163
    sget-object p2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, p2}, Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 164
    invoke-virtual {p0}, Lcom/squareup/noho/NohoInputBox;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/noho/R$dimen;->noho_edit_default_margin:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 165
    new-instance p2, Lcom/squareup/noho/HideablePlugin;

    .line 166
    new-instance v0, Lcom/squareup/noho/IconPlugin;

    sget-object v2, Lcom/squareup/noho/NohoEditRow$Side;->END:Lcom/squareup/noho/NohoEditRow$Side;

    move-object v3, p1

    check-cast v3, Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/noho/IconPlugin;-><init>(Lcom/squareup/noho/NohoEditRow$Side;Landroid/graphics/drawable/Drawable;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/noho/NohoEditRow$Plugin;

    const/4 p1, 0x0

    .line 165
    invoke-direct {p2, v0, p1}, Lcom/squareup/noho/HideablePlugin;-><init>(Lcom/squareup/noho/NohoEditRow$Plugin;Z)V

    .line 168
    iget-object p1, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    move-object v0, p2

    check-cast v0, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    return-object p2
.end method

.method public static synthetic edit$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic error$default(Lcom/squareup/noho/NohoInputBox;IZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const/4 p2, 0x0

    :cond_0
    const-string p4, "checker"

    .line 177
    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 338
    invoke-virtual {p0}, Lcom/squareup/noho/NohoInputBox;->getContext()Landroid/content/Context;

    move-result-object p4

    invoke-virtual {p4, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p4, "context.getString(id)"

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 349
    new-instance p4, Lcom/squareup/noho/NohoInputBox$error$1;

    invoke-direct {p4, p2, p3, p1}, Lcom/squareup/noho/NohoInputBox$error$1;-><init>(ZLkotlin/jvm/functions/Function1;Ljava/lang/String;)V

    check-cast p4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, p4}, Lcom/squareup/noho/NohoInputBox;->error(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static synthetic error$default(Lcom/squareup/noho/NohoInputBox;Ljava/lang/String;ZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const/4 p2, 0x0

    :cond_0
    const-string p4, "message"

    .line 189
    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "checker"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 362
    new-instance p4, Lcom/squareup/noho/NohoInputBox$error$1;

    invoke-direct {p4, p2, p3, p1}, Lcom/squareup/noho/NohoInputBox$error$1;-><init>(ZLkotlin/jvm/functions/Function1;Ljava/lang/String;)V

    check-cast p4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, p4}, Lcom/squareup/noho/NohoInputBox;->error(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method private final firstNotNull(Ljava/util/Collection;ZLjava/lang/CharSequence;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "+",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/CharSequence;",
            "-",
            "Ljava/lang/Boolean;",
            "+TT;>;>;Z",
            "Ljava/lang/CharSequence;",
            ")TT;"
        }
    .end annotation

    .line 281
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p1

    new-instance v0, Lcom/squareup/noho/NohoInputBox$firstNotNull$1;

    invoke-direct {v0, p3, p2}, Lcom/squareup/noho/NohoInputBox$firstNotNull$1;-><init>(Ljava/lang/CharSequence;Z)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 386
    invoke-interface {p1}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_1

    const/4 p3, 0x1

    goto :goto_0

    :cond_1
    const/4 p3, 0x0

    :goto_0
    if-eqz p3, :cond_0

    goto :goto_1

    :cond_2
    const/4 p2, 0x0

    :goto_1
    return-object p2
.end method

.method private final getErrorPlugin()Lcom/squareup/noho/HideablePlugin;
    .locals 1

    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->errorPlugin$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/HideablePlugin;

    return-object v0
.end method

.method private final getValidatedPlugin()Lcom/squareup/noho/HideablePlugin;
    .locals 1

    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->validatedPlugin$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/HideablePlugin;

    return-object v0
.end method

.method private final hideDetails()V
    .locals 2

    .line 298
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->details:Landroidx/appcompat/widget/AppCompatTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatTextView;->setVisibility(I)V

    return-void
.end method

.method private final refreshState(Z)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_0

    .line 256
    iget-object p1, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditRow;->isFocused()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 257
    :goto_0
    iget-object v2, p0, Lcom/squareup/noho/NohoInputBox;->errorCheckers:Ljava/util/List;

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoInputBox;->getText()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {p0, v2, p1, v3}, Lcom/squareup/noho/NohoInputBox;->firstNotNull(Ljava/util/Collection;ZLjava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/noho/NohoInputBox$Error;

    if-eqz v2, :cond_1

    .line 258
    invoke-direct {p0, v2}, Lcom/squareup/noho/NohoInputBox;->show(Lcom/squareup/noho/NohoInputBox$Error;)V

    .line 260
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    const/4 v2, 0x1

    goto :goto_2

    :cond_1
    iget-object v2, p0, Lcom/squareup/noho/NohoInputBox;->suggestionCheckers:Ljava/util/List;

    check-cast v2, Ljava/util/Collection;

    .line 261
    invoke-virtual {p0}, Lcom/squareup/noho/NohoInputBox;->getText()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {p0, v2, p1, v3}, Lcom/squareup/noho/NohoInputBox;->firstNotNull(Ljava/util/Collection;ZLjava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoInputBox$Suggestion;

    if-eqz p1, :cond_2

    .line 262
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoInputBox;->showSuggestion(Lcom/squareup/noho/NohoInputBox$Suggestion;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    const/4 v2, 0x0

    :goto_2
    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    invoke-direct {p0}, Lcom/squareup/noho/NohoInputBox;->hideDetails()V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 264
    :goto_3
    iget-object p1, p0, Lcom/squareup/noho/NohoInputBox;->errorCheckers:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v1

    if-eqz p1, :cond_4

    .line 266
    iget-object p1, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 267
    invoke-direct {p0}, Lcom/squareup/noho/NohoInputBox;->getErrorPlugin()Lcom/squareup/noho/HideablePlugin;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoEditRow;->isError()Z

    move-result v2

    invoke-virtual {p1, v2}, Lcom/squareup/noho/HideablePlugin;->setVisible(Z)V

    .line 270
    :cond_4
    iget-object p1, p0, Lcom/squareup/noho/NohoInputBox;->validationChecker:Lkotlin/jvm/functions/Function1;

    if-eqz p1, :cond_6

    .line 272
    iget-object v2, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoInputBox;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v2, p1}, Lcom/squareup/noho/NohoEditRow;->setValidated(Z)V

    .line 273
    invoke-direct {p0}, Lcom/squareup/noho/NohoInputBox;->getValidatedPlugin()Lcom/squareup/noho/HideablePlugin;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoEditRow;->isValidated()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoEditRow;->isError()Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v0, 0x1

    :cond_5
    invoke-virtual {p1, v0}, Lcom/squareup/noho/HideablePlugin;->setVisible(Z)V

    :cond_6
    return-void
.end method

.method static synthetic refreshState$default(Lcom/squareup/noho/NohoInputBox;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 254
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoInputBox;->refreshState(Z)V

    return-void
.end method

.method private final show(Lcom/squareup/noho/NohoInputBox$Error;)V
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->details:Landroidx/appcompat/widget/AppCompatTextView;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoInputBox$Error;->getMessage()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/AppCompatTextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    iget-object p1, p0, Lcom/squareup/noho/NohoInputBox;->details:Landroidx/appcompat/widget/AppCompatTextView;

    check-cast p1, Landroid/widget/TextView;

    iget v0, p0, Lcom/squareup/noho/NohoInputBox;->errorAppearance:I

    invoke-static {p1, v0}, Landroidx/core/widget/TextViewCompat;->setTextAppearance(Landroid/widget/TextView;I)V

    .line 287
    iget-object p1, p0, Lcom/squareup/noho/NohoInputBox;->details:Landroidx/appcompat/widget/AppCompatTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatTextView;->setVisibility(I)V

    return-void
.end method

.method private final showSuggestion(Lcom/squareup/noho/NohoInputBox$Suggestion;)V
    .locals 1

    .line 291
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoInputBox$Suggestion;->setEditToFix$noho_release(Lcom/squareup/noho/NohoEditRow;)V

    .line 292
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->details:Landroidx/appcompat/widget/AppCompatTextView;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoInputBox$Suggestion;->getDisplayText$noho_release()Landroid/text/SpannableString;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/AppCompatTextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    iget-object p1, p0, Lcom/squareup/noho/NohoInputBox;->details:Landroidx/appcompat/widget/AppCompatTextView;

    check-cast p1, Landroid/widget/TextView;

    iget v0, p0, Lcom/squareup/noho/NohoInputBox;->detailsAppearance:I

    invoke-static {p1, v0}, Landroidx/core/widget/TextViewCompat;->setTextAppearance(Landroid/widget/TextView;I)V

    .line 294
    iget-object p1, p0, Lcom/squareup/noho/NohoInputBox;->details:Landroidx/appcompat/widget/AppCompatTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatTextView;->setVisibility(I)V

    return-void
.end method

.method public static synthetic suggestion$default(Lcom/squareup/noho/NohoInputBox;Lcom/squareup/noho/NohoInputBox$Suggestion;ZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const/4 p2, 0x0

    :cond_0
    const-string p4, "message"

    .line 217
    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "checker"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 380
    new-instance p4, Lcom/squareup/noho/NohoInputBox$suggestion$2;

    invoke-direct {p4, p2, p3, p1}, Lcom/squareup/noho/NohoInputBox$suggestion$2;-><init>(ZLkotlin/jvm/functions/Function1;Lcom/squareup/noho/NohoInputBox$Suggestion;)V

    check-cast p4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, p4}, Lcom/squareup/noho/NohoInputBox;->suggestion(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static synthetic suggestion$default(Lcom/squareup/noho/NohoInputBox;Ljava/lang/String;ZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const/4 p2, 0x0

    :cond_0
    const-string p4, "message"

    .line 205
    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "checker"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 368
    new-instance p4, Lcom/squareup/noho/NohoInputBox$suggestion$1;

    invoke-direct {p4, p2, p3, p1}, Lcom/squareup/noho/NohoInputBox$suggestion$1;-><init>(ZLkotlin/jvm/functions/Function1;Ljava/lang/String;)V

    check-cast p4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, p4}, Lcom/squareup/noho/NohoInputBox;->suggestion(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method


# virtual methods
.method public final checkAll()V
    .locals 1

    const/4 v0, 0x1

    .line 242
    invoke-direct {p0, v0}, Lcom/squareup/noho/NohoInputBox;->refreshState(Z)V

    return-void
.end method

.method public final configureEdit(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/noho/NohoEditRow;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    invoke-virtual {p0}, Lcom/squareup/noho/NohoInputBox;->getEdit()Lcom/squareup/noho/NohoEditRow;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final disableActionsExceptPaste()V
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    check-cast v0, Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/EditTexts;->disableActionsExceptPaste(Landroid/widget/EditText;)V

    return-void
.end method

.method public final error(IZLkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "checker"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    invoke-virtual {p0}, Lcom/squareup/noho/NohoInputBox;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(id)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 331
    new-instance v0, Lcom/squareup/noho/NohoInputBox$error$1;

    invoke-direct {v0, p2, p3, p1}, Lcom/squareup/noho/NohoInputBox$error$1;-><init>(ZLkotlin/jvm/functions/Function1;Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoInputBox;->error(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public final error(Ljava/lang/String;ZLkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checker"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    new-instance v0, Lcom/squareup/noho/NohoInputBox$error$1;

    invoke-direct {v0, p2, p3, p1}, Lcom/squareup/noho/NohoInputBox$error$1;-><init>(ZLkotlin/jvm/functions/Function1;Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoInputBox;->error(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public final error(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/CharSequence;",
            "-",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/noho/NohoInputBox$Error;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->errorCheckers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final getEdit()Lcom/squareup/noho/NohoEditRow;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    return-object v0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getText()Landroid/text/Editable;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->label:Landroidx/appcompat/widget/AppCompatTextView;

    invoke-virtual {v0}, Landroidx/appcompat/widget/AppCompatTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final setText(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->edit:Lcom/squareup/noho/NohoEditRow;

    check-cast p1, Ljava/lang/CharSequence;

    sget-object v1, Landroid/widget/TextView$BufferType;->EDITABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    return-void
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->label:Landroidx/appcompat/widget/AppCompatTextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/AppCompatTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final suggestion(Lcom/squareup/noho/NohoInputBox$Suggestion;ZLkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoInputBox$Suggestion;",
            "Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checker"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    new-instance v0, Lcom/squareup/noho/NohoInputBox$suggestion$2;

    invoke-direct {v0, p2, p3, p1}, Lcom/squareup/noho/NohoInputBox$suggestion$2;-><init>(ZLkotlin/jvm/functions/Function1;Lcom/squareup/noho/NohoInputBox$Suggestion;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoInputBox;->suggestion(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public final suggestion(Ljava/lang/String;ZLkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checker"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    new-instance v0, Lcom/squareup/noho/NohoInputBox$suggestion$1;

    invoke-direct {v0, p2, p3, p1}, Lcom/squareup/noho/NohoInputBox$suggestion$1;-><init>(ZLkotlin/jvm/functions/Function1;Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoInputBox;->suggestion(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public final suggestion(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/CharSequence;",
            "-",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/noho/NohoInputBox$Suggestion;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox;->suggestionCheckers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final validation(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    iput-object p1, p0, Lcom/squareup/noho/NohoInputBox;->validationChecker:Lkotlin/jvm/functions/Function1;

    return-void
.end method
