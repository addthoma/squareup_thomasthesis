.class public final Lcom/squareup/noho/NohoSelectableKt;
.super Ljava/lang/Object;
.source "NohoSelectable.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u001a\u0016\u0010\u0002\u001a\u00020\u0003*\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "SELECTABLE_CHECKED_STATES",
        "",
        "setAppearanceId",
        "",
        "Lcom/squareup/noho/NohoLabel;",
        "styleId",
        "",
        "noho_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final SELECTABLE_CHECKED_STATES:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    .line 200
    sput-object v0, Lcom/squareup/noho/NohoSelectableKt;->SELECTABLE_CHECKED_STATES:[I

    return-void
.end method

.method public static final synthetic access$getSELECTABLE_CHECKED_STATES$p()[I
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/noho/NohoSelectableKt;->SELECTABLE_CHECKED_STATES:[I

    return-object v0
.end method

.method public static final synthetic access$setAppearanceId(Lcom/squareup/noho/NohoLabel;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoSelectableKt;->setAppearanceId(Lcom/squareup/noho/NohoLabel;I)V

    return-void
.end method

.method private static final setAppearanceId(Lcom/squareup/noho/NohoLabel;I)V
    .locals 3

    if-eqz p1, :cond_0

    .line 205
    sget-object v0, Lcom/squareup/textappearance/SquareTextAppearance;->Companion:Lcom/squareup/textappearance/SquareTextAppearance$Companion;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoLabel;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1}, Lcom/squareup/textappearance/SquareTextAppearance$Companion;->loadFromStyle(Landroid/content/Context;I)Lcom/squareup/textappearance/SquareTextAppearance;

    move-result-object p1

    .line 204
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoLabel;->apply(Lcom/squareup/textappearance/SquareTextAppearance;)V

    :cond_0
    return-void
.end method
