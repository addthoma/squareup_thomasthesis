.class public final Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;
.super Ljava/lang/Object;
.source "CommonLocationModule_ProvideLastBestLocationPersistentFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/core/location/LastBestLocationStore;",
        ">;"
    }
.end annotation


# instance fields
.field private final dataDirectoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final fileThreadExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final persistentFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistent/PersistentFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistent/PersistentFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;->gsonProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;->dataDirectoryProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;->persistentFactoryProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistent/PersistentFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;)",
            "Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideLastBestLocationPersistent(Lcom/google/gson/Gson;Ljava/io/File;Lcom/squareup/persistent/PersistentFactory;Ljava/util/concurrent/Executor;)Lcom/squareup/core/location/LastBestLocationStore;
    .locals 0

    .line 52
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/core/location/CommonLocationModule;->provideLastBestLocationPersistent(Lcom/google/gson/Gson;Ljava/io/File;Lcom/squareup/persistent/PersistentFactory;Ljava/util/concurrent/Executor;)Lcom/squareup/core/location/LastBestLocationStore;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/core/location/LastBestLocationStore;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/core/location/LastBestLocationStore;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/Gson;

    iget-object v1, p0, Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;->dataDirectoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    iget-object v2, p0, Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;->persistentFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/persistent/PersistentFactory;

    iget-object v3, p0, Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;->provideLastBestLocationPersistent(Lcom/google/gson/Gson;Ljava/io/File;Lcom/squareup/persistent/PersistentFactory;Ljava/util/concurrent/Executor;)Lcom/squareup/core/location/LastBestLocationStore;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/core/location/CommonLocationModule_ProvideLastBestLocationPersistentFactory;->get()Lcom/squareup/core/location/LastBestLocationStore;

    move-result-object v0

    return-object v0
.end method
