.class public Lcom/squareup/core/location/comparer/LocationComparer;
.super Ljava/lang/Object;
.source "LocationComparer.java"


# static fields
.field public static final MIN_ACCURACY:F = 200.0f

.field public static final TIME_DELTA_THRESHOLD_MS:J = 0xea60L


# instance fields
.field private final constraints:Lcom/squareup/core/location/constraint/CompositeLocationConstraint;


# direct methods
.method public varargs constructor <init>([Lcom/squareup/core/location/constraint/LocationConstraint;)V
    .locals 3

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 33
    new-instance v1, Lcom/squareup/core/location/constraint/BuggyNaNConstraint;

    invoke-direct {v1}, Lcom/squareup/core/location/constraint/BuggyNaNConstraint;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    new-instance v1, Lcom/squareup/core/location/constraint/AccuracyConstraint;

    const/high16 v2, 0x43480000    # 200.0f

    invoke-direct {v1, v2}, Lcom/squareup/core/location/constraint/AccuracyConstraint;-><init>(F)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 36
    new-instance p1, Lcom/squareup/core/location/constraint/CompositeLocationConstraint;

    .line 38
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/squareup/core/location/constraint/LocationConstraint;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/core/location/constraint/LocationConstraint;

    invoke-direct {p1, v0}, Lcom/squareup/core/location/constraint/CompositeLocationConstraint;-><init>([Lcom/squareup/core/location/constraint/LocationConstraint;)V

    iput-object p1, p0, Lcom/squareup/core/location/comparer/LocationComparer;->constraints:Lcom/squareup/core/location/constraint/CompositeLocationConstraint;

    return-void
.end method

.method private isSameProvider(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 0

    if-nez p1, :cond_1

    if-nez p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1

    .line 126
    :cond_1
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method


# virtual methods
.method public varargs getBestLocation([Landroid/location/Location;)Landroid/location/Location;
    .locals 5

    .line 46
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    .line 47
    invoke-virtual {p0, v1, v3}, Lcom/squareup/core/location/comparer/LocationComparer;->isBetterLocation(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v1, v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method isBetterLocation(Landroid/location/Location;Landroid/location/Location;)Z
    .locals 10

    .line 66
    invoke-virtual {p0, p2}, Lcom/squareup/core/location/comparer/LocationComparer;->isValidLocation(Landroid/location/Location;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x1

    if-nez p1, :cond_1

    return v0

    .line 72
    :cond_1
    invoke-virtual {p2}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-eqz v6, :cond_2

    .line 73
    invoke-virtual {p1}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v2

    cmp-long v6, v2, v4

    if-eqz v6, :cond_2

    .line 75
    invoke-virtual {p2}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v6

    sub-long/2addr v2, v6

    const-wide/32 v6, 0xf4240

    div-long/2addr v2, v6

    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual {p2}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    sub-long/2addr v2, v6

    :goto_0
    const-wide/32 v6, 0xea60

    cmp-long v8, v2, v6

    if-lez v8, :cond_3

    const/4 v6, 0x1

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    :goto_1
    const-wide/32 v7, -0xea60

    cmp-long v9, v2, v7

    if-gez v9, :cond_4

    const/4 v7, 0x1

    goto :goto_2

    :cond_4
    const/4 v7, 0x0

    :goto_2
    cmp-long v8, v2, v4

    if-lez v8, :cond_5

    const/4 v2, 0x1

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :goto_3
    if-eqz v6, :cond_6

    return v0

    :cond_6
    if-eqz v7, :cond_7

    return v1

    .line 95
    :cond_7
    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    if-nez v3, :cond_8

    const/4 v4, 0x1

    goto :goto_4

    :cond_8
    const/4 v4, 0x0

    :goto_4
    if-gez v3, :cond_9

    const/4 v3, 0x1

    goto :goto_5

    :cond_9
    const/4 v3, 0x0

    .line 101
    :goto_5
    invoke-virtual {p2}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/core/location/comparer/LocationComparer;->isSameProvider(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz v3, :cond_a

    return v0

    :cond_a
    if-eqz v2, :cond_b

    if-eqz v4, :cond_b

    return v0

    :cond_b
    if-eqz v2, :cond_c

    if-eqz p1, :cond_c

    return v0

    :cond_c
    return v1
.end method

.method public isValidLocation(Landroid/location/Location;)Z
    .locals 1

    if-eqz p1, :cond_0

    .line 118
    iget-object v0, p0, Lcom/squareup/core/location/comparer/LocationComparer;->constraints:Lcom/squareup/core/location/constraint/CompositeLocationConstraint;

    invoke-virtual {v0, p1}, Lcom/squareup/core/location/constraint/CompositeLocationConstraint;->isSatisfied(Landroid/location/Location;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
