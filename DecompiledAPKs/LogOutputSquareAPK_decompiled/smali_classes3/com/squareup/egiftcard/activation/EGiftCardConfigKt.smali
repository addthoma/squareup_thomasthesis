.class public final Lcom/squareup/egiftcard/activation/EGiftCardConfigKt;
.super Ljava/lang/Object;
.source "EGiftCardConfig.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEGiftCardConfig.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EGiftCardConfig.kt\ncom/squareup/egiftcard/activation/EGiftCardConfigKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,95:1\n1360#2:96\n1429#2,2:97\n215#2,2:99\n1431#2:101\n*E\n*S KotlinDebug\n*F\n+ 1 EGiftCardConfig.kt\ncom/squareup/egiftcard/activation/EGiftCardConfigKt\n*L\n60#1:96\n60#1,2:97\n60#1,2:99\n60#1:101\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\u001a\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0001\u001a\u000e\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b\u001a(\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\r2\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u001a\u000c\u0010\u0012\u001a\u00020\u0013*\u0004\u0018\u00010\u0014\u001a\n\u0010\u0012\u001a\u00020\u0013*\u00020\u000b\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "EGIFTCARD_IMAGE_HEIGHT",
        "",
        "EGIFTCARD_IMAGE_WIDTH",
        "createEGiftCardPlaceholder",
        "Landroid/graphics/drawable/BitmapDrawable;",
        "resource",
        "Landroid/content/res/Resources;",
        "color",
        "generateEGiftCardConfig",
        "Lcom/squareup/egiftcard/activation/EGiftCardConfig;",
        "response",
        "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;",
        "generateEGiftCardEnabledThemes",
        "",
        "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
        "enabled_theme_tokens",
        "",
        "all_egift_themes",
        "isEGiftCardVisible",
        "",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
        "giftcard-activation_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final EGIFTCARD_IMAGE_HEIGHT:I = 0x190

.field public static final EGIFTCARD_IMAGE_WIDTH:I = 0x280


# direct methods
.method public static final createEGiftCardPlaceholder(Landroid/content/res/Resources;I)Landroid/graphics/drawable/BitmapDrawable;
    .locals 3

    const-string v0, "resource"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/16 v1, 0x280

    const/16 v2, 0x190

    invoke-static {v1, v2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 92
    invoke-virtual {v0, p1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 89
    new-instance p1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {p1, p0, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object p1
.end method

.method public static synthetic createEGiftCardPlaceholder$default(Landroid/content/res/Resources;IILjava/lang/Object;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 0

    and-int/lit8 p2, p2, 0x2

    if-eqz p2, :cond_0

    const/4 p1, -0x1

    .line 87
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/egiftcard/activation/EGiftCardConfigKt;->createEGiftCardPlaceholder(Landroid/content/res/Resources;I)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object p0

    return-object p0
.end method

.method public static final generateEGiftCardConfig(Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;)Lcom/squareup/egiftcard/activation/EGiftCardConfig;
    .locals 7

    const-string v0, "response"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v0, Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    .line 40
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iget-object v2, v1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->host_unit_token:Ljava/lang/String;

    const-string v1, "response.order_configuration.host_unit_token"

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iget-object v1, v1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->enabled_theme_tokens:Ljava/util/List;

    const-string v3, "response.order_configuration.enabled_theme_tokens"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v3, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    const-string v4, "response.all_egift_themes"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-static {v1, v3}, Lcom/squareup/egiftcard/activation/EGiftCardConfigKt;->generateEGiftCardEnabledThemes(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 45
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iget-object v4, v1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->denomination_amounts:Ljava/util/List;

    const-string v1, "response.order_configuration.denomination_amounts"

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iget-object v5, v1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->min_load_amount:Lcom/squareup/protos/common/Money;

    const-string v1, "response.order_configuration.min_load_amount"

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object p0, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iget-object v6, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->max_load_amount:Lcom/squareup/protos/common/Money;

    const-string p0, "response.order_configuration.max_load_amount"

    invoke-static {v6, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    .line 39
    invoke-direct/range {v1 .. v6}, Lcom/squareup/egiftcard/activation/EGiftCardConfig;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-object v0
.end method

.method public static final generateEGiftCardEnabledThemes(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation

    const-string v0, "enabled_theme_tokens"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "all_egift_themes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    check-cast p0, Ljava/lang/Iterable;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 97
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 98
    check-cast v1, Ljava/lang/String;

    .line 61
    move-object v2, p1

    check-cast v2, Ljava/lang/Iterable;

    .line 99
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/giftcards/EGiftTheme;

    .line 62
    iget-object v4, v3, Lcom/squareup/protos/client/giftcards/EGiftTheme;->read_only_token:Ljava/lang/String;

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 100
    :cond_1
    new-instance p0, Ljava/util/NoSuchElementException;

    const-string p1, "Collection contains no element matching the predicate."

    invoke-direct {p0, p1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 101
    :cond_2
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final isEGiftCardVisible(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Z
    .locals 3

    const/4 v0, 0x1

    if-eqz p0, :cond_0

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_in_store:Ljava/lang/Boolean;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->enabled_theme_tokens:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 75
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->denomination_amounts:Ljava/util/List;

    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    const/4 v1, 0x4

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static final isEGiftCardVisible(Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;)Z
    .locals 1

    const-string v0, "$this$isEGiftCardVisible"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object p0, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    invoke-static {p0}, Lcom/squareup/egiftcard/activation/EGiftCardConfigKt;->isEGiftCardVisible(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Z

    move-result p0

    return p0
.end method
