.class public final Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;
.super Ljava/lang/Object;
.source "ChooseCustomAmountCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001d\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J+\u0010\u0008\u001a\u00020\t2\u001c\u0010\n\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000cj\u0002`\u000f0\u000bH\u0000\u00a2\u0006\u0002\u0008\u0010R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;",
        "",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "(Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;)V",
        "build",
        "Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/ChooseCustomAmountScreen;",
        "build$egiftcard_activation_release",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-void
.end method


# virtual methods
.method public final build$egiftcard_activation_release(Lio/reactivex/Observable;)Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;>;)",
            "Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;)V

    return-object v0
.end method
