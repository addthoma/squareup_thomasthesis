.class public final Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;
.super Ljava/lang/Object;
.source "X2ActivateEGiftCardWorkflowStarter.kt"

# interfaces
.implements Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowStarter;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ`\u0010\r\u001aP\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0011\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0012j\u0002`\u00130\u0010j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0011`\u00140\u000f\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00160\u000ej\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u0016`\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowStarter;",
        "reactor",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;",
        "maybeX2SellerScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "branEvents",
        "Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardEventsRelay;",
        "screenDataRenderer",
        "Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;",
        "(Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardEventsRelay;Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;)V",
        "renderer",
        "Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardRenderer;",
        "start",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;",
        "Lcom/squareup/container/PosWorkflow;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final branEvents:Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardEventsRelay;

.field private final maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

.field private final reactor:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;

.field private final renderer:Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardRenderer;


# direct methods
.method public constructor <init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardEventsRelay;Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "reactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maybeX2SellerScreenRunner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "branEvents"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenDataRenderer"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;->reactor:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iput-object p3, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;->branEvents:Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardEventsRelay;

    .line 37
    new-instance p1, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardRenderer;

    invoke-direct {p1, p4}, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardRenderer;-><init>(Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;)V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;->renderer:Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardRenderer;

    return-void
.end method

.method public static final synthetic access$getMaybeX2SellerScreenRunner$p(Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;)Lcom/squareup/x2/MaybeX2SellerScreenRunner;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    return-object p0
.end method

.method public static final synthetic access$getRenderer$p(Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;)Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardRenderer;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;->renderer:Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardRenderer;

    return-object p0
.end method


# virtual methods
.method public start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;",
            ">;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-direct {v0}, Lcom/squareup/workflow/legacy/WorkflowPool;-><init>()V

    .line 42
    iget-object v1, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;->reactor:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;

    .line 43
    sget-object v2, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;->Companion:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$Companion;

    invoke-virtual {v2, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$Companion;->fromSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;->INSTANCE:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;

    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    .line 42
    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->launch(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 48
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    .line 49
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkflowsKt;->getState(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$1;

    invoke-direct {v3, p0}, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$1;-><init>(Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const-string/jumbo v3, "workflow.state.subscribe\u2026rdActivation(state)\n    }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    .line 52
    iget-object v2, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;->branEvents:Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardEventsRelay;

    invoke-virtual {v2}, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardEventsRelay;->getEvents()Lio/reactivex/Observable;

    move-result-object v2

    .line 54
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkflowsKt;->getState(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Observable;

    move-result-object v3

    check-cast v3, Lio/reactivex/ObservableSource;

    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v4

    .line 53
    invoke-virtual {v2, v3, v4}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v2

    .line 58
    sget-object v3, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$2;->INSTANCE:Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$2;

    check-cast v3, Lio/reactivex/functions/Predicate;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    .line 59
    new-instance v3, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$3;

    invoke-direct {v3, p1}, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$3;-><init>(Lcom/squareup/workflow/legacy/Workflow;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const-string v3, "branEvents.events\n      \u2026t(eventWithState.event) }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {v1, v2}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    .line 61
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->toCompletable(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Completable;

    move-result-object v2

    .line 62
    new-instance v3, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$4;

    invoke-direct {v3, v1}, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$4;-><init>(Lio/reactivex/disposables/CompositeDisposable;)V

    check-cast v3, Lio/reactivex/functions/Action;

    invoke-virtual {v2, v3}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const-string/jumbo v3, "workflow.toCompletable()\u2026ubscribe { subs.clear() }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-static {v1, v2}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    .line 66
    new-instance v1, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$5;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v0, v2}, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$5;-><init>(Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;Lcom/squareup/workflow/legacy/Workflow;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {p1, v1}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->mapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method
