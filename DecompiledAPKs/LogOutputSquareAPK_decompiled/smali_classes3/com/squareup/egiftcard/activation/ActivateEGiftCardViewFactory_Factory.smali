.class public final Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;
.super Ljava/lang/Object;
.source "ActivateEGiftCardViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$Factory;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p4, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$Factory;",
            ">;)",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$Factory;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;-><init>(Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;
    .locals 4

    .line 33
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;

    iget-object v3, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$Factory;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;->newInstance(Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$Factory;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory_Factory;->get()Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;

    move-result-object v0

    return-object v0
.end method
