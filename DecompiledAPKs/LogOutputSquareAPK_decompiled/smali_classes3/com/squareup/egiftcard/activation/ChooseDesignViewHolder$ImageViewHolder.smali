.class public final Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder$ImageViewHolder;
.super Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder;
.source "ChooseDesignViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ImageViewHolder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseDesignViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseDesignViewHolder.kt\ncom/squareup/egiftcard/activation/ChooseDesignViewHolder$ImageViewHolder\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,50:1\n1103#2,7:51\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseDesignViewHolder.kt\ncom/squareup/egiftcard/activation/ChooseDesignViewHolder$ImageViewHolder\n*L\n29#1,7:51\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder$ImageViewHolder;",
        "Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "image",
        "Landroid/widget/ImageView;",
        "bind",
        "",
        "item",
        "Lcom/squareup/egiftcard/activation/ChooseDesignItem;",
        "picasso",
        "Lcom/squareup/picasso/Picasso;",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final image:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, p1, v0}, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder;-><init>(Landroid/view/View;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 21
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_design_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.egiftcard_design_image)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder$ImageViewHolder;->image:Landroid/widget/ImageView;

    return-void
.end method


# virtual methods
.method public bind(Lcom/squareup/egiftcard/activation/ChooseDesignItem;Lcom/squareup/picasso/Picasso;)V
    .locals 4

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "picasso"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    move-object v0, p1

    check-cast v0, Lcom/squareup/egiftcard/activation/ChooseDesignItem$ImageItem;

    .line 26
    invoke-virtual {v0}, Lcom/squareup/egiftcard/activation/ChooseDesignItem$ImageItem;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p2

    .line 27
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder$ImageViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "image.resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/egiftcard/activation/EGiftCardConfigKt;->createEGiftCardPlaceholder$default(Landroid/content/res/Resources;IILjava/lang/Object;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, v0}, Lcom/squareup/picasso/RequestCreator;->placeholder(Landroid/graphics/drawable/Drawable;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p2

    .line 28
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder$ImageViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {p2, v0}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 29
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder$ImageViewHolder;->image:Landroid/widget/ImageView;

    check-cast p2, Landroid/view/View;

    .line 51
    new-instance v0, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder$ImageViewHolder$bind$$inlined$onClickDebounced$1;

    invoke-direct {v0, p1}, Lcom/squareup/egiftcard/activation/ChooseDesignViewHolder$ImageViewHolder$bind$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/egiftcard/activation/ChooseDesignItem;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
