.class final Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$5;
.super Ljava/lang/Object;
.source "LibraryListManager.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/RealLibraryListManager;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012F\u0010\u0002\u001aB\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00010\u0001 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00010\u0001\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/librarylist/LibraryListState;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/cogs/CatalogUpdateEvent;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/librarylist/RealLibraryListManager;


# direct methods
.method constructor <init>(Lcom/squareup/librarylist/RealLibraryListManager;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$5;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lkotlin/Pair;)Lcom/squareup/librarylist/LibraryListState;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/cogs/CatalogUpdateEvent;",
            "Lcom/squareup/librarylist/LibraryListState;",
            ">;)",
            "Lcom/squareup/librarylist/LibraryListState;"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/CatalogUpdateEvent;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListState;

    .line 196
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v1

    sget-object v2, Lcom/squareup/librarylist/LibraryListState$Filter;->SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;

    const/4 v3, 0x0

    if-eq v1, v2, :cond_0

    return-object v3

    .line 200
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasMultipleUpdateBatches()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 201
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$5;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-static {p1}, Lcom/squareup/librarylist/RealLibraryListManager;->access$topLevelFilter(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListState;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x0

    .line 204
    sget-object v4, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v4, v1, v2

    invoke-virtual {v0, v1}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "event"

    .line 205
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->getDeleted()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogObject;

    const-string v2, "deletedObject"

    .line 206
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v2

    sget-object v4, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v2, v4, :cond_2

    .line 207
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->getCurrentCategoryId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 208
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$5;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-static {p1}, Lcom/squareup/librarylist/RealLibraryListManager;->access$topLevelFilter(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListState;

    move-result-object p1

    return-object p1

    :cond_3
    return-object v3
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 83
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$5;->call(Lkotlin/Pair;)Lcom/squareup/librarylist/LibraryListState;

    move-result-object p1

    return-object p1
.end method
