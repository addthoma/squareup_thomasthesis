.class public Lcom/squareup/librarylist/CogsSelectedEntryDataLocator;
.super Ljava/lang/Object;
.source "CogsSelectedEntryDataLocator.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getEntryDataForItem(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 33
    const-class v2, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-interface {v1, v2, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 38
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItem;->hasImage()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    const-class v2, Lcom/squareup/shared/catalog/models/CatalogItemImage;

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItem;->getImageId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v2, v5}, Lcom/squareup/shared/catalog/Catalog$Local;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemImage;

    move-object v5, v2

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    .line 41
    :goto_0
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItem;->hasMenuCategory()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 42
    const-class v2, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 43
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItem;->getMenuCategoryId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v2, v6}, Lcom/squareup/shared/catalog/Catalog$Local;->findByIdOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    if-nez v2, :cond_1

    const/4 v6, 0x0

    goto :goto_1

    .line 44
    :cond_1
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->object()Lcom/squareup/wire/Message;

    move-result-object v6

    check-cast v6, Lcom/squareup/api/items/MenuCategory;

    :goto_1
    move-object v7, v6

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 46
    :goto_2
    invoke-interface {v1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->findItemTaxes(Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    .line 47
    invoke-interface {v1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->findItemVariations(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 48
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_3

    .line 50
    new-instance v6, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-direct {v6, v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    :cond_3
    move-object v10, v6

    .line 54
    invoke-interface {v1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->findEnabledItemModifiers(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v11

    .line 56
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 59
    invoke-interface {v11}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    .line 60
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Map$Entry;

    .line 61
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 62
    invoke-interface {v12}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_4
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    .line 63
    invoke-virtual {v15}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->object()Lcom/squareup/wire/Message;

    move-result-object v15

    invoke-interface {v13, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 66
    :cond_4
    new-instance v14, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;

    invoke-direct {v14}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;-><init>()V

    .line 67
    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    invoke-virtual {v12}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->object()Lcom/squareup/wire/Message;

    move-result-object v12

    check-cast v12, Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {v14, v12}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;

    move-result-object v12

    .line 68
    invoke-virtual {v12, v13}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_option(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;

    move-result-object v12

    .line 69
    invoke-virtual {v12}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;

    move-result-object v12

    .line 66
    invoke-interface {v6, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 71
    :cond_5
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 72
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 73
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_6
    :goto_5
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_7

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 74
    invoke-virtual {v14}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v14

    check-cast v14, Lcom/squareup/api/items/ItemVariation;

    .line 75
    invoke-interface {v8, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v14, v14, Lcom/squareup/api/items/ItemVariation;->measurement_unit_token:Ljava/lang/String;

    .line 77
    invoke-static {v14}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_6

    .line 78
    invoke-interface {v12, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_6

    .line 79
    const-class v15, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 80
    invoke-interface {v1, v15, v14}, Lcom/squareup/shared/catalog/Catalog$Local;->findCatalogConnectV2Object(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object v15

    .line 79
    invoke-interface {v12, v14, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 85
    :cond_7
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 86
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 88
    const-class v15, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItem;->getAllItemOptionIds()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v15, v3}, Lcom/squareup/shared/catalog/Catalog$Local;->findCatalogConnectV2Objects(Ljava/lang/Class;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 90
    invoke-virtual {v15}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v13, v0, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    goto :goto_6

    .line 92
    :cond_8
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItem;->getAllItemOptionIds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_9
    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 93
    invoke-interface {v13, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    if-eqz v3, :cond_9

    .line 95
    invoke-interface {v14, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 99
    :cond_a
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;-><init>()V

    .line 100
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Item;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item(Lcom/squareup/api/items/Item;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    move-result-object v0

    if-nez v5, :cond_b

    const/4 v3, 0x0

    goto :goto_8

    .line 103
    :cond_b
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->object()Lcom/squareup/wire/Message;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemImage;

    .line 101
    :goto_8
    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item_image(Lcom/squareup/api/items/ItemImage;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    move-result-object v0

    if-nez v2, :cond_c

    const/4 v3, 0x0

    goto :goto_9

    .line 106
    :cond_c
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->object()Lcom/squareup/wire/Message;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/api/items/MenuCategory;

    .line 104
    :goto_9
    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    move-result-object v0

    new-instance v2, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;-><init>()V

    .line 108
    invoke-virtual {v2, v8}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->item_variation(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;

    move-result-object v2

    .line 109
    invoke-virtual {v2, v6}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->modifier_list(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;

    move-result-object v2

    .line 110
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    move-result-object v2

    .line 107
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->available_options(Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object v6

    .line 114
    invoke-static/range {p0 .. p1}, Lcom/squareup/librarylist/CogsSelectedEntryDataLocator;->getModifierOverride(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/Map;

    move-result-object v0

    .line 116
    new-instance v1, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;

    move-object v3, v1

    move-object v8, v12

    move-object v12, v0

    move-object v13, v14

    invoke-direct/range {v3 .. v13}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;-><init>(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/shared/catalog/models/CatalogItemImage;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/api/items/MenuCategory;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;)V

    return-object v1
.end method

.method public static getModifierOverride(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/ItemModifierOverride;",
            ">;"
        }
    .end annotation

    .line 124
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 126
    invoke-interface {p1, p0}, Lcom/squareup/shared/catalog/Catalog$Local;->findItemModifierMemberships(Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    .line 127
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    .line 128
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->isAdvancedModifierEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getModifierListId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getItemModifierOverride()Lcom/squareup/shared/catalog/models/ItemModifierOverride;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v0
.end method
