.class public final Lcom/squareup/librarylist/RealLibraryListPresenter;
.super Lmortar/ViewPresenter;
.source "RealLibraryListPresenter.kt"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/librarylist/LibraryListView;",
        ">;",
        "Lcom/squareup/librarylist/LibraryListPresenter;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003BG\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u0012\u0010\u0017\u001a\u00020\u00182\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0002J\"\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0002J\"\u0010#\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0002J\u0010\u0010$\u001a\u00020\u001c2\u0006\u0010%\u001a\u00020&H\u0014J\u0012\u0010\'\u001a\u00020\u001c2\u0008\u0010(\u001a\u0004\u0018\u00010)H\u0014J\u0010\u0010*\u001a\u00020\u001c2\u0006\u0010+\u001a\u00020,H\u0002J\u001a\u0010-\u001a\u00020\u001c2\u0006\u0010.\u001a\u00020\"2\u0008\u0010/\u001a\u0004\u0018\u00010\"H\u0002J\u0010\u00100\u001a\u00020\u001c2\u0006\u00101\u001a\u000202H\u0007R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00063"
    }
    d2 = {
        "Lcom/squareup/librarylist/RealLibraryListPresenter;",
        "Lmortar/ViewPresenter;",
        "Lcom/squareup/librarylist/LibraryListView;",
        "Lcom/squareup/librarylist/LibraryListPresenter;",
        "device",
        "Lcom/squareup/util/Device;",
        "res",
        "Lcom/squareup/util/Res;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "libraryListManager",
        "Lcom/squareup/librarylist/LibraryListManager;",
        "libraryListAssistant",
        "Lcom/squareup/librarylist/LibraryListAssistant;",
        "entryHandler",
        "Lcom/squareup/librarylist/LibraryListEntryHandler;",
        "configuration",
        "Lcom/squareup/librarylist/LibraryListConfiguration;",
        "libraryListAdapterFactory",
        "Lcom/squareup/librarylist/LibraryListAdapter$Factory;",
        "(Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/librarylist/LibraryListAssistant;Lcom/squareup/librarylist/LibraryListEntryHandler;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListAdapter$Factory;)V",
        "adapter",
        "Lcom/squareup/librarylist/LibraryListAdapter;",
        "isLibraryEntryEnabled",
        "",
        "entry",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "libraryListItemClicked",
        "",
        "view",
        "Landroid/view/View;",
        "position",
        "",
        "searchFilter",
        "",
        "libraryListItemLongClicked",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "showEmptyLibrary",
        "state",
        "Lcom/squareup/librarylist/LibraryListState;",
        "showNoContent",
        "title",
        "message",
        "updateAdapter",
        "result",
        "Lcom/squareup/librarylist/LibraryListResults;",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final adapter:Lcom/squareup/librarylist/LibraryListAdapter;

.field private final configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

.field private final device:Lcom/squareup/util/Device;

.field private final entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

.field private final libraryListAssistant:Lcom/squareup/librarylist/LibraryListAssistant;

.field private final libraryListManager:Lcom/squareup/librarylist/LibraryListManager;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/librarylist/LibraryListAssistant;Lcom/squareup/librarylist/LibraryListEntryHandler;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListAdapter$Factory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "libraryListManager"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "libraryListAssistant"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "entryHandler"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "libraryListAdapterFactory"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->device:Lcom/squareup/util/Device;

    iput-object p2, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p4, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->libraryListManager:Lcom/squareup/librarylist/LibraryListManager;

    iput-object p5, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->libraryListAssistant:Lcom/squareup/librarylist/LibraryListAssistant;

    iput-object p6, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    iput-object p7, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

    .line 51
    new-instance p1, Lcom/squareup/librarylist/RealLibraryListPresenter$adapter$1;

    invoke-direct {p1, p0}, Lcom/squareup/librarylist/RealLibraryListPresenter$adapter$1;-><init>(Lcom/squareup/librarylist/RealLibraryListPresenter;)V

    check-cast p1, Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;

    invoke-virtual {p8, p1}, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->build(Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;)Lcom/squareup/librarylist/LibraryListAdapter;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->adapter:Lcom/squareup/librarylist/LibraryListAdapter;

    return-void
.end method

.method public static final synthetic access$getLibraryListAssistant$p(Lcom/squareup/librarylist/RealLibraryListPresenter;)Lcom/squareup/librarylist/LibraryListAssistant;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->libraryListAssistant:Lcom/squareup/librarylist/LibraryListAssistant;

    return-object p0
.end method

.method public static final synthetic access$isLibraryEntryEnabled(Lcom/squareup/librarylist/RealLibraryListPresenter;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/RealLibraryListPresenter;->isLibraryEntryEnabled(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$libraryListItemClicked(Lcom/squareup/librarylist/RealLibraryListPresenter;Landroid/view/View;ILjava/lang/String;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/librarylist/RealLibraryListPresenter;->libraryListItemClicked(Landroid/view/View;ILjava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$libraryListItemLongClicked(Lcom/squareup/librarylist/RealLibraryListPresenter;Landroid/view/View;ILjava/lang/String;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/librarylist/RealLibraryListPresenter;->libraryListItemLongClicked(Landroid/view/View;ILjava/lang/String;)V

    return-void
.end method

.method private final isLibraryEntryEnabled(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z
    .locals 3

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 218
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v1, v2, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    .line 219
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    const-string v2, "entry.type"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object p1

    const-string v2, "entry.objectId"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    invoke-interface {v0, v1, p1}, Lcom/squareup/librarylist/LibraryListEntryHandler;->isEntryEnabled(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0
.end method

.method private final libraryListItemClicked(Landroid/view/View;ILjava/lang/String;)V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->libraryListAssistant:Lcom/squareup/librarylist/LibraryListAssistant;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListAssistant;->logLibraryListItemClicked()V

    .line 106
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->adapter:Lcom/squareup/librarylist/LibraryListAdapter;

    invoke-virtual {v0, p2}, Lcom/squareup/librarylist/LibraryListAdapter;->isEnabled(I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_6

    .line 108
    check-cast p1, Lcom/squareup/librarylist/LibraryItemListNohoRow;

    .line 109
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->adapter:Lcom/squareup/librarylist/LibraryListAdapter;

    invoke-virtual {v0, p2}, Lcom/squareup/librarylist/LibraryListAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v0

    if-nez v0, :cond_2

    .line 113
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->adapter:Lcom/squareup/librarylist/LibraryListAdapter;

    invoke-virtual {p1, p2}, Lcom/squareup/librarylist/LibraryListAdapter;->getPlaceholder(I)Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    move-result-object p1

    .line 114
    sget-object p2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->CUSTOM_AMOUNT:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    if-ne p1, p2, :cond_1

    .line 115
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    invoke-interface {p1}, Lcom/squareup/librarylist/LibraryListEntryHandler;->customAmountClicked()V

    goto :goto_0

    .line 117
    :cond_1
    iget-object p2, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->libraryListManager:Lcom/squareup/librarylist/LibraryListManager;

    invoke-interface {p2, p1}, Lcom/squareup/librarylist/LibraryListManager;->viewPlaceholder(Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;)V

    goto :goto_0

    .line 119
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne p2, v1, :cond_3

    .line 120
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->libraryListManager:Lcom/squareup/librarylist/LibraryListManager;

    invoke-interface {p1, v0}, Lcom/squareup/librarylist/LibraryListManager;->viewCategory(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    goto :goto_0

    .line 121
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne p2, v1, :cond_4

    .line 122
    iget-object p2, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    .line 123
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getItemThumbnail()Landroid/widget/ImageView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    const/4 p3, 0x1

    .line 122
    invoke-interface {p2, p1, v0, p3}, Lcom/squareup/librarylist/LibraryListEntryHandler;->discountClicked(Landroid/view/View;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Z)V

    .line 125
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListView;->hideKeyboard()V

    goto :goto_0

    .line 126
    :cond_4
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne p2, v1, :cond_5

    .line 127
    iget-object p2, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getItemThumbnail()Landroid/widget/ImageView;

    move-result-object p1

    invoke-interface {p2, p1, v0, p3}, Lcom/squareup/librarylist/LibraryListEntryHandler;->itemClicked(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Ljava/lang/String;)V

    .line 128
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListView;->hideKeyboard()V

    :cond_5
    :goto_0
    return-void

    .line 108
    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.librarylist.LibraryItemListNohoRow"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final libraryListItemLongClicked(Landroid/view/View;ILjava/lang/String;)V
    .locals 3

    .line 137
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->libraryListAssistant:Lcom/squareup/librarylist/LibraryListAssistant;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListAssistant;->logLibraryListItemLongClicked()V

    .line 138
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListConfiguration;->getCanLongClickOnItem()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->adapter:Lcom/squareup/librarylist/LibraryListAdapter;

    invoke-virtual {v0, p2}, Lcom/squareup/librarylist/LibraryListAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 142
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v1

    sget-object v2, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    if-ne v1, v2, :cond_1

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    .line 147
    check-cast p1, Lcom/squareup/librarylist/LibraryItemListNohoRow;

    .line 148
    iget-object p2, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getItemThumbnail()Landroid/widget/ImageView;

    move-result-object p1

    invoke-interface {p2, p1, v0, p3}, Lcom/squareup/librarylist/LibraryListEntryHandler;->itemLongClicked(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Ljava/lang/String;)V

    .line 150
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListView;->hideKeyboard()V

    return-void

    .line 147
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.librarylist.LibraryItemListNohoRow"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 143
    :cond_3
    :goto_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/librarylist/RealLibraryListPresenter;->libraryListItemClicked(Landroid/view/View;ILjava/lang/String;)V

    return-void
.end method

.method private final showEmptyLibrary(Lcom/squareup/librarylist/LibraryListState;)V
    .locals 4

    .line 154
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v0

    sget-object v1, Lcom/squareup/librarylist/RealLibraryListPresenter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListState$Filter;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_2

    .line 192
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhone()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 198
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    .line 199
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 200
    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/librarylist/R$string;->item_library_empty_services_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 198
    invoke-virtual {p1, v0, v1}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 194
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/librarylist/R$string;->item_library_empty_note_services_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 195
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/librarylist/R$string;->item_library_empty_note_services_message_items_applet:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 193
    invoke-direct {p0, p1, v0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->showNoContent(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 181
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhone()Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_1

    .line 187
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    .line 188
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 189
    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/librarylist/R$string;->item_library_empty_items_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 187
    invoke-virtual {p1, v0, v1}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 183
    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/librarylist/R$string;->item_library_empty_note_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 184
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/librarylist/R$string;->item_library_empty_note_message_items_applet:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 182
    invoke-direct {p0, p1, v0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->showNoContent(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 174
    :pswitch_2
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    .line 175
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 176
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/librarylist/R$string;->item_library_empty_category_note_title:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 177
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->getCurrentCategoryName()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v3, "category_name"

    invoke-virtual {v2, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 178
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 179
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 174
    invoke-virtual {v0, v1, p1}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V

    goto :goto_2

    .line 170
    :pswitch_3
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    .line 171
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_GIFT_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 172
    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/librarylist/R$string;->item_library_empty_gift_cards_note_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 170
    invoke-virtual {p1, v0, v1}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V

    goto :goto_2

    .line 166
    :pswitch_4
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    .line 167
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_TAG:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 168
    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/librarylist/R$string;->item_library_empty_discounts_note_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 166
    invoke-virtual {p1, v0, v1}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V

    goto :goto_2

    .line 155
    :pswitch_5
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 156
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    .line 157
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 158
    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/librarylist/R$string;->item_library_empty_categories:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 156
    invoke-virtual {p1, v0, v1}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V

    goto :goto_2

    .line 162
    :cond_4
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/librarylist/R$string;->item_library_empty_note_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 163
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/librarylist/R$string;->item_library_empty_note_message_items_applet:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 161
    invoke-direct {p0, p1, v0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->showNoContent(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final showNoContent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 210
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

    invoke-interface {v2}, Lcom/squareup/librarylist/LibraryListConfiguration;->getCanShowEditItemsButton()Z

    move-result v2

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public bridge synthetic dropView(Lcom/squareup/librarylist/LibraryListView;)V
    .locals 0

    .line 40
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/RealLibraryListPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->libraryListManager:Lcom/squareup/librarylist/LibraryListManager;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListManager;->getResults()Lrx/Observable;

    move-result-object v0

    .line 58
    new-instance v1, Lcom/squareup/librarylist/RealLibraryListPresenter$onEnterScope$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/librarylist/RealLibraryListPresenter;

    invoke-direct {v1, v2}, Lcom/squareup/librarylist/RealLibraryListPresenter$onEnterScope$1;-><init>(Lcom/squareup/librarylist/RealLibraryListPresenter;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 62
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->adapter:Lcom/squareup/librarylist/LibraryListAdapter;

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-virtual {p1, v0}, Lcom/squareup/librarylist/LibraryListView;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 63
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListView;->getAction()Lrx/Observable;

    move-result-object p1

    .line 64
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->libraryListManager:Lcom/squareup/librarylist/LibraryListManager;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListManager;->getResults()Lrx/Observable;

    move-result-object v0

    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    const-string/jumbo v0, "view.action\n        .wit\u2026anager.results, toPair())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    const-string/jumbo v1, "view"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/librarylist/RealLibraryListPresenter$onLoad$1;

    invoke-direct {v1, p0}, Lcom/squareup/librarylist/RealLibraryListPresenter$onLoad$1;-><init>(Lcom/squareup/librarylist/RealLibraryListPresenter;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    return-void
.end method

.method public bridge synthetic takeView(Lcom/squareup/librarylist/LibraryListView;)V
    .locals 0

    .line 40
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/RealLibraryListPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public final updateAdapter(Lcom/squareup/librarylist/LibraryListResults;)V
    .locals 3

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->adapter:Lcom/squareup/librarylist/LibraryListAdapter;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListResults;->getCursor()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListResults;->getPlaceholders()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/librarylist/LibraryListAdapter;->update(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->adapter:Lcom/squareup/librarylist/LibraryListAdapter;

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListResults;->getCurrentState()Lcom/squareup/librarylist/LibraryListState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListState;->getSearchQuery()Lcom/squareup/librarylist/LibraryListState$SearchQuery;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListState$SearchQuery;->isBlank()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListResults;->getCurrentState()Lcom/squareup/librarylist/LibraryListState;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/librarylist/RealLibraryListPresenter;->showEmptyLibrary(Lcom/squareup/librarylist/LibraryListState;)V

    goto :goto_0

    .line 90
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListView;->showNoResults()V

    goto :goto_0

    .line 93
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter;->adapter:Lcom/squareup/librarylist/LibraryListAdapter;

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-virtual {p1, v0}, Lcom/squareup/librarylist/LibraryListView;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 94
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListView;->showLibrary()V

    :goto_0
    return-void
.end method
