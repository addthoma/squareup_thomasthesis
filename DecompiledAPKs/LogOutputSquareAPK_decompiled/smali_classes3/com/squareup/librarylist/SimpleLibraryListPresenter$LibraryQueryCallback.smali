.class public Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;
.super Ljava/lang/Object;
.source "SimpleLibraryListPresenter.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/librarylist/SimpleLibraryListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LibraryQueryCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogCallback<",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        ">;"
    }
.end annotation


# instance fields
.field private canceled:Z

.field private final queryId:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/librarylist/SimpleLibraryListPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/librarylist/SimpleLibraryListPresenter;Ljava/lang/String;)V
    .locals 0

    .line 652
    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;->this$0:Lcom/squareup/librarylist/SimpleLibraryListPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 653
    sget-object p1, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_CATEGORIES:Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState$Filter;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 656
    iput-object p2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;->queryId:Ljava/lang/String;

    return-void

    .line 654
    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Use LibraryCategoriesQueryCallback instead"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic access$000(Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;)Ljava/lang/String;
    .locals 0

    .line 643
    iget-object p0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;->queryId:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            ">;)V"
        }
    .end annotation

    .line 660
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 661
    iget-boolean v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;->canceled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;->this$0:Lcom/squareup/librarylist/SimpleLibraryListPresenter;

    invoke-static {v0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->access$100(Lcom/squareup/librarylist/SimpleLibraryListPresenter;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 664
    :cond_0
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;->this$0:Lcom/squareup/librarylist/SimpleLibraryListPresenter;

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;->queryId:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->setLibraryQueryResults(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/lang/String;)V

    goto :goto_1

    .line 662
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    .line 666
    :goto_1
    iget-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;->this$0:Lcom/squareup/librarylist/SimpleLibraryListPresenter;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->access$202(Lcom/squareup/librarylist/SimpleLibraryListPresenter;Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;)Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;

    return-void
.end method

.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 670
    iput-boolean v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;->canceled:Z

    return-void
.end method
