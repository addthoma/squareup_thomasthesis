.class public final Lcom/squareup/librarylist/RealLibraryListSearcher;
.super Ljava/lang/Object;
.source "LibraryListSearcher.kt"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListSearcher;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J$\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\t2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u0002J$\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\t2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u0002J$\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\t2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u0016J$\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0010\u001a\u00020\t2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u0016J$\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\t2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u0016J&\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u000b2\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u0016H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/librarylist/RealLibraryListSearcher;",
        "Lcom/squareup/librarylist/LibraryListSearcher;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "(Lcom/squareup/cogs/Cogs;)V",
        "performReadFromFilterSearch",
        "Lrx/Single;",
        "Lcom/squareup/librarylist/CatalogQueryResult;",
        "state",
        "Lcom/squareup/librarylist/LibraryListState;",
        "topLevelPlaceholders",
        "",
        "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
        "performTopLevelSearch",
        "readFromFilter",
        "searchByName",
        "currentState",
        "topLevelSearch",
        "typesFor",
        "Lcom/squareup/api/items/Item$Type;",
        "placeholders",
        "includeServices",
        "",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cogs:Lcom/squareup/cogs/Cogs;


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/Cogs;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cogs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListSearcher;->cogs:Lcom/squareup/cogs/Cogs;

    return-void
.end method

.method public static final synthetic access$typesFor(Lcom/squareup/librarylist/RealLibraryListSearcher;Ljava/util/List;Z)Ljava/util/List;
    .locals 0

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/squareup/librarylist/RealLibraryListSearcher;->typesFor(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private final performReadFromFilterSearch(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/LibraryListState;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;)",
            "Lrx/Single<",
            "Lcom/squareup/librarylist/CatalogQueryResult;",
            ">;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListSearcher;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/librarylist/RealLibraryListSearcher$performReadFromFilterSearch$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/librarylist/RealLibraryListSearcher$performReadFromFilterSearch$1;-><init>(Lcom/squareup/librarylist/RealLibraryListSearcher;Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)V

    check-cast v1, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p2

    .line 157
    new-instance v0, Lcom/squareup/librarylist/RealLibraryListSearcher$performReadFromFilterSearch$2;

    invoke-direct {v0, p1}, Lcom/squareup/librarylist/RealLibraryListSearcher$performReadFromFilterSearch$2;-><init>(Lcom/squareup/librarylist/LibraryListState;)V

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p2, v0}, Lrx/Single;->map(Lrx/functions/Func1;)Lrx/Single;

    move-result-object p1

    const-string p2, "cogs.asSingle { catalog \u2026FilterResult(state, it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final performTopLevelSearch(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/LibraryListState;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;)",
            "Lrx/Single<",
            "Lcom/squareup/librarylist/CatalogQueryResult;",
            ">;"
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListSearcher;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/librarylist/RealLibraryListSearcher$performTopLevelSearch$1;

    invoke-direct {v1, p0, p2}, Lcom/squareup/librarylist/RealLibraryListSearcher$performTopLevelSearch$1;-><init>(Lcom/squareup/librarylist/RealLibraryListSearcher;Ljava/util/List;)V

    check-cast v1, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p2

    .line 170
    new-instance v0, Lcom/squareup/librarylist/RealLibraryListSearcher$performTopLevelSearch$2;

    invoke-direct {v0, p1}, Lcom/squareup/librarylist/RealLibraryListSearcher$performTopLevelSearch$2;-><init>(Lcom/squareup/librarylist/LibraryListState;)V

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p2, v0}, Lrx/Single;->map(Lrx/functions/Func1;)Lrx/Single;

    move-result-object p1

    const-string p2, "cogs.asSingle { catalog \u2026pLevelResult(state, it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final typesFor(Ljava/util/List;Z)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;Z)",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation

    .line 177
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p2, :cond_0

    .line 179
    sget-object p2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_SERVICES:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 180
    sget-object p2, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    :cond_0
    sget-object p2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 184
    sget-object p2, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    :cond_1
    sget-object p2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_GIFT_CARDS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 188
    sget-object p1, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    :cond_2
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method static synthetic typesFor$default(Lcom/squareup/librarylist/RealLibraryListSearcher;Ljava/util/List;ZILjava/lang/Object;)Ljava/util/List;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x1

    .line 175
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/librarylist/RealLibraryListSearcher;->typesFor(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public readFromFilter(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/LibraryListState;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;)",
            "Lrx/Single<",
            "Lcom/squareup/librarylist/CatalogQueryResult;",
            ">;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "topLevelPlaceholders"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v0

    sget-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_CATEGORIES:Lcom/squareup/librarylist/LibraryListState$Filter;

    if-ne v0, v1, :cond_0

    .line 100
    invoke-direct {p0, p1, p2}, Lcom/squareup/librarylist/RealLibraryListSearcher;->performTopLevelSearch(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 102
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/librarylist/RealLibraryListSearcher;->performReadFromFilterSearch(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public searchByName(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/LibraryListState;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;)",
            "Lrx/Single<",
            "Lcom/squareup/librarylist/CatalogQueryResult;",
            ">;"
        }
    .end annotation

    const-string v0, "currentState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "topLevelPlaceholders"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListSearcher;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/librarylist/RealLibraryListSearcher$searchByName$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/librarylist/RealLibraryListSearcher$searchByName$1;-><init>(Lcom/squareup/librarylist/RealLibraryListSearcher;Ljava/util/List;Lcom/squareup/librarylist/LibraryListState;)V

    check-cast v1, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p2

    .line 121
    new-instance v0, Lcom/squareup/librarylist/RealLibraryListSearcher$searchByName$2;

    invoke-direct {v0, p1}, Lcom/squareup/librarylist/RealLibraryListSearcher$searchByName$2;-><init>(Lcom/squareup/librarylist/LibraryListState;)V

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p2, v0}, Lrx/Single;->map(Lrx/functions/Func1;)Lrx/Single;

    move-result-object p1

    const-string p2, "cogs.asSingle { catalog \u2026esult(currentState, it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public topLevelSearch(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/LibraryListState;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;)",
            "Lrx/Single<",
            "Lcom/squareup/librarylist/CatalogQueryResult;",
            ">;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "topLevelPlaceholders"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/squareup/librarylist/RealLibraryListSearcher;->readFromFilter(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 87
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/librarylist/RealLibraryListSearcher;->performTopLevelSearch(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;

    move-result-object p1

    :goto_0
    return-object p1
.end method
