.class final Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3$1;
.super Ljava/lang/Object;
.source "LibraryItemListNohoRow.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3;->invoke()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;",
        "+",
        "Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u000126\u0010\u0002\u001a2\u0012\u0004\u0012\u00020\u0004\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0018\u0012\u0004\u0012\u00020\u0004\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;",
        "Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3;


# direct methods
.method constructor <init>(Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3$1;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 56
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3$1;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;",
            "+",
            "Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;

    .line 170
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;->isTextTile()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 172
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3$1;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3;

    iget-object p1, p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getItemListNohoRow()Lcom/squareup/noho/NohoRow;

    move-result-object p1

    const/4 v0, 0x0

    check-cast v0, Lcom/squareup/noho/NohoRow$Icon;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    goto :goto_0

    .line 174
    :cond_0
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3$1;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3;

    iget-object p1, p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getItemListNohoRow()Lcom/squareup/noho/NohoRow;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3$1;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3;

    iget-object v1, v1, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow;

    invoke-static {v1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->access$getTransitionTime$p(Lcom/squareup/librarylist/LibraryItemListNohoRow;)I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/squareup/librarylist/LibraryItemListNohoRowKt;->loadIconIntoRow(Lcom/squareup/noho/NohoRow;Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;I)V

    :goto_0
    return-void
.end method
