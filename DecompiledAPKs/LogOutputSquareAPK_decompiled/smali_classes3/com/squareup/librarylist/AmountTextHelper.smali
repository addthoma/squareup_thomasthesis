.class public final Lcom/squareup/librarylist/AmountTextHelper;
.super Ljava/lang/Object;
.source "AmountTextHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J<\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0010\u0008\u0002\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u00122\n\u0008\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0016\u001a\u00020\u0017J\u0018\u0010\u0018\u001a\u00020\u000c2\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u001e\u0010\u001b\u001a\u00020\u000c2\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00122\u0006\u0010\u001c\u001a\u00020\u0013H\u0002J&\u0010\u001d\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012H\u0002J \u0010\u001e\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0010\u0010\u001f\u001a\u00020\u000c2\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0010\u0010 \u001a\u00020\u000c2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/librarylist/AmountTextHelper;",
        "",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "getContext",
        "()Landroid/content/Context;",
        "multiplePrices",
        "Lcom/squareup/phrase/Phrase;",
        "variablePricing",
        "",
        "getAmountText",
        "",
        "entry",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "priceFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "res",
        "Lcom/squareup/util/Res;",
        "getFixedDiscountAmount",
        "price",
        "Lcom/squareup/protos/common/Money;",
        "getFixedDiscountPercentage",
        "percentage",
        "getFixedDiscountText",
        "getItemPricing",
        "getVariableDiscountAmount",
        "getVariableDiscountPercentage",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final multiplePrices:Lcom/squareup/phrase/Phrase;

.field private final variablePricing:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/AmountTextHelper;->context:Landroid/content/Context;

    .line 25
    iget-object p1, p0, Lcom/squareup/librarylist/AmountTextHelper;->context:Landroid/content/Context;

    sget v0, Lcom/squareup/librarylist/R$string;->item_library_variable_price:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(R.stri\u2026m_library_variable_price)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/AmountTextHelper;->variablePricing:Ljava/lang/String;

    .line 26
    iget-object p1, p0, Lcom/squareup/librarylist/AmountTextHelper;->context:Landroid/content/Context;

    sget v0, Lcom/squareup/librarylist/R$string;->item_library_multiple_prices:I

    invoke-static {p1, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iput-object p1, p0, Lcom/squareup/librarylist/AmountTextHelper;->multiplePrices:Lcom/squareup/phrase/Phrase;

    return-void
.end method

.method public static synthetic getAmountText$default(Lcom/squareup/librarylist/AmountTextHelper;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;ILjava/lang/Object;)Ljava/lang/CharSequence;
    .locals 7

    and-int/lit8 p7, p6, 0x4

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    .line 31
    move-object p3, v0

    check-cast p3, Lcom/squareup/text/Formatter;

    :cond_0
    move-object v4, p3

    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_1

    .line 32
    move-object p4, v0

    check-cast p4, Lcom/squareup/protos/common/CurrencyCode;

    :cond_1
    move-object v5, p4

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/librarylist/AmountTextHelper;->getAmountText(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method private final getFixedDiscountAmount(Lcom/squareup/protos/common/Money;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/lang/CharSequence;
    .locals 3

    .line 75
    iget-object v0, p0, Lcom/squareup/librarylist/AmountTextHelper;->context:Landroid/content/Context;

    sget v1, Lcom/squareup/librarylist/R$string;->item_library_variable_price_negation:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 76
    invoke-static {p2, p1, v1, v2, v1}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "amount"

    invoke-virtual {v0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "Phrase.from(context, R.s\u2026price))\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getFixedDiscountPercentage(Lcom/squareup/text/Formatter;Lcom/squareup/util/Percentage;)Ljava/lang/CharSequence;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/util/Percentage;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .line 84
    invoke-interface {p1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "percentageFormatter.format(percentage)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getFixedDiscountText(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .line 63
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getPercentage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v0

    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object p1

    const-string p3, "entry.price"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/librarylist/AmountTextHelper;->getFixedDiscountAmount(Lcom/squareup/protos/common/Money;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 67
    :cond_0
    invoke-direct {p0, p3, v0}, Lcom/squareup/librarylist/AmountTextHelper;->getFixedDiscountPercentage(Lcom/squareup/text/Formatter;Lcom/squareup/util/Percentage;)Ljava/lang/CharSequence;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final getItemPricing(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 3

    .line 104
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 105
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getVariations()I

    move-result v1

    .line 106
    sget-object v2, Lcom/squareup/quantity/UnitDisplayData;->Companion:Lcom/squareup/quantity/UnitDisplayData$Companion;

    .line 107
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getCatalogMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    invoke-virtual {v2, p3, p1}, Lcom/squareup/quantity/UnitDisplayData$Companion;->fromCatalogMeasurementUnit(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object p1

    const/4 p3, 0x1

    if-ne v1, p3, :cond_0

    if-nez v0, :cond_0

    .line 111
    iget-object p2, p0, Lcom/squareup/librarylist/AmountTextHelper;->variablePricing:Ljava/lang/String;

    invoke-static {p2, p1}, Lcom/squareup/quantity/ItemQuantities;->formatWithUnit(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_0

    :cond_0
    if-ne v1, p3, :cond_1

    if-eqz v0, :cond_1

    .line 115
    invoke-virtual {p2, v0, p1}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 118
    :cond_1
    iget-object p1, p0, Lcom/squareup/librarylist/AmountTextHelper;->multiplePrices:Lcom/squareup/phrase/Phrase;

    const-string p2, "count"

    invoke-virtual {p1, p2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 119
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "multiplePrices.put(\"coun\u2026ns)\n            .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method private final getVariableDiscountAmount(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/CharSequence;
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/librarylist/AmountTextHelper;->context:Landroid/content/Context;

    sget v1, Lcom/squareup/librarylist/R$string;->item_library_variable_amount_discount:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 90
    invoke-static {p1}, Lcom/squareup/currency_db/Currencies;->getCurrencySymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "currency_symbol"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 91
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "Phrase.from(context, R.s\u2026yCode))\n        .format()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getVariableDiscountPercentage(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 1

    .line 96
    sget v0, Lcom/squareup/librarylist/R$string;->item_library_variable_percentage_discount:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method


# virtual methods
.method public final getAmountText(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Res;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    const-string v0, "entry"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/librarylist/AmountTextHelper$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    if-eq v0, v1, :cond_1

    :goto_0
    const-string p1, ""

    .line 53
    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_1

    .line 50
    :cond_1
    invoke-direct {p0, p1, p2, p5}, Lcom/squareup/librarylist/AmountTextHelper;->getItemPricing(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_1

    .line 37
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object v0

    if-eqz v0, :cond_7

    sget-object v3, Lcom/squareup/librarylist/AmountTextHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/api/items/Discount$DiscountType;->ordinal()I

    move-result v0

    aget v0, v3, v0

    if-eq v0, v2, :cond_5

    if-eq v0, v1, :cond_3

    const/4 p1, 0x3

    if-ne v0, p1, :cond_7

    .line 45
    invoke-direct {p0, p5}, Lcom/squareup/librarylist/AmountTextHelper;->getVariableDiscountPercentage(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_1

    :cond_3
    if-nez p4, :cond_4

    .line 42
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    invoke-direct {p0, p4}, Lcom/squareup/librarylist/AmountTextHelper;->getVariableDiscountAmount(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_1

    :cond_5
    if-nez p3, :cond_6

    .line 39
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_6
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/librarylist/AmountTextHelper;->getFixedDiscountText(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object p1

    :goto_1
    return-object p1

    .line 45
    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/librarylist/AmountTextHelper;->context:Landroid/content/Context;

    return-object v0
.end method
