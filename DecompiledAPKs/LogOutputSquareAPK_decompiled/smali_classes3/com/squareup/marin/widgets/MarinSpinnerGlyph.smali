.class public Lcom/squareup/marin/widgets/MarinSpinnerGlyph;
.super Landroid/widget/ProgressBar;
.source "MarinSpinnerGlyph.java"


# static fields
.field private static final FAILURE_TRANSITION_PROGRESS:Ljava/lang/String; = "Failure_Transition_Progress_State"

.field private static final SUCCESS_TRANSITION_PROGRESS:Ljava/lang/String; = "Success_Transition_Progress_State"

.field private static final SUPER_STATE:Ljava/lang/String; = "Marin_Spinner_Glyph_Super_State"


# instance fields
.field private final duration:I

.field private final failureDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

.field private failureTransitionProgress:F

.field private final successDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

.field private successTransitionProgress:F

.field private transitionToFailure:Landroid/animation/ValueAnimator;

.field private transitionToSuccess:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 43
    sget v0, Lcom/squareup/marin/R$attr;->marinSpinnerGlyphStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    sget-object v0, Lcom/squareup/marin/R$styleable;->MarinSpinnerGlyph:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const/high16 p3, 0x10e0000

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p2

    .line 52
    sget p3, Lcom/squareup/marin/R$styleable;->MarinSpinnerGlyph_transitionDuration:I

    invoke-virtual {p1, p3, p2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->duration:I

    .line 55
    sget p2, Lcom/squareup/marin/R$styleable;->MarinSpinnerGlyph_glyphColor:I

    .line 56
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    .line 55
    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p2

    .line 57
    sget p3, Lcom/squareup/marin/R$styleable;->MarinSpinnerGlyph_glyph:I

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ALL_GLYPHS:Ljava/util/List;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-static {p1, p3, v0, v1}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;ILjava/util/List;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p3

    check-cast p3, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 58
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    .line 59
    invoke-virtual {v0, p3}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p3

    .line 60
    invoke-virtual {p3, p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->color(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p3

    .line 61
    invoke-virtual {p3}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p3

    iput-object p3, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    .line 63
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->getFailureGlyphColor(Landroid/content/res/TypedArray;I)I

    move-result p2

    .line 64
    sget p3, Lcom/squareup/marin/R$styleable;->MarinSpinnerGlyph_failureGlyph:I

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ALL_GLYPHS:Ljava/util/List;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 65
    invoke-static {p1, p3, v0, v1}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;ILjava/util/List;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p3

    check-cast p3, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 66
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    .line 67
    invoke-virtual {v0, p3}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p3

    .line 68
    invoke-virtual {p3, p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->color(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p2

    .line 69
    invoke-virtual {p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    .line 71
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    const/4 p1, 0x0

    .line 73
    iput p1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    .line 74
    iput p1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    .line 75
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    iget p2, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->updateDrawableAlphas(Landroid/graphics/drawable/Drawable;F)V

    .line 76
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    iget p2, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->updateDrawableAlphas(Landroid/graphics/drawable/Drawable;F)V

    return-void
.end method

.method private cancelTransitions()V
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess:Landroid/animation/ValueAnimator;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 172
    iput-object v1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess:Landroid/animation/ValueAnimator;

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToFailure:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToFailure:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 176
    iput-object v1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToFailure:Landroid/animation/ValueAnimator;

    :cond_1
    return-void
.end method

.method private getFailureGlyphColor(Landroid/content/res/TypedArray;I)I
    .locals 2

    .line 160
    sget v0, Lcom/squareup/marin/R$styleable;->MarinSpinnerGlyph_failureGlyphColor:I

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p1

    if-ne p1, v1, :cond_0

    move p1, p2

    :cond_0
    return p1
.end method

.method private static intAlphaFromFloat(F)I
    .locals 1

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float p0, p0, v0

    float-to-int p0, p0

    return p0
.end method

.method private updateDrawableAlphas(Landroid/graphics/drawable/Drawable;F)V
    .locals 2

    .line 181
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p2

    invoke-static {v1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->intAlphaFromFloat(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 182
    invoke-static {p2}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->intAlphaFromFloat(F)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$transitionToFailure$1$MarinSpinnerGlyph(Landroid/animation/ValueAnimator;)V
    .locals 1

    .line 131
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p1

    iput p1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    .line 132
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    iget v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    invoke-direct {p0, p1, v0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->updateDrawableAlphas(Landroid/graphics/drawable/Drawable;F)V

    .line 133
    invoke-static {p0}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$transitionToSuccess$0$MarinSpinnerGlyph(Landroid/animation/ValueAnimator;)V
    .locals 1

    .line 111
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p1

    iput p1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    .line 112
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    iget v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    invoke-direct {p0, p1, v0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->updateDrawableAlphas(Landroid/graphics/drawable/Drawable;F)V

    .line 113
    invoke-static {p0}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .line 92
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 93
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->onDraw(Landroid/graphics/Canvas;)V

    .line 96
    :cond_0
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 100
    :cond_1
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    .line 101
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphDrawable;->draw(Landroid/graphics/Canvas;)V

    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .line 80
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    invoke-virtual {v1}, Lcom/squareup/glyph/SquareGlyphDrawable;->getIntrinsicHeight()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    .line 82
    invoke-virtual {v2}, Lcom/squareup/glyph/SquareGlyphDrawable;->getIntrinsicWidth()I

    move-result v2

    .line 81
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 83
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 84
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 85
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->getPaddingLeft()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    .line 86
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->getPaddingTop()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    const/4 v2, 0x0

    .line 87
    invoke-static {v1, p1, v2}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->resolveSizeAndState(III)I

    move-result p1

    .line 88
    invoke-static {v0, p2, v2}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->resolveSizeAndState(III)I

    move-result p2

    .line 87
    invoke-virtual {p0, p1, p2}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->setMeasuredDimension(II)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 199
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "Marin_Spinner_Glyph_Super_State"

    .line 200
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/ProgressBar;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "Success_Transition_Progress_State"

    .line 201
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    const-string v0, "Failure_Transition_Progress_State"

    .line 202
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result p1

    iput p1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    .line 203
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    iget v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    invoke-direct {p0, p1, v0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->updateDrawableAlphas(Landroid/graphics/drawable/Drawable;F)V

    .line 204
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    iget v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    invoke-direct {p0, p1, v0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->updateDrawableAlphas(Landroid/graphics/drawable/Drawable;F)V

    .line 205
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->invalidate()V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 190
    invoke-super {p0}, Landroid/widget/ProgressBar;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 191
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "Marin_Spinner_Glyph_Super_State"

    .line 192
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 193
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    const-string v2, "Success_Transition_Progress_State"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 194
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    const-string v2, "Failure_Transition_Progress_State"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    return-object v1
.end method

.method public reset()V
    .locals 2

    .line 149
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->cancelTransitions()V

    const/4 v0, 0x0

    .line 150
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    .line 151
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    .line 152
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->intAlphaFromFloat(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    return-void
.end method

.method public setToFailure()V
    .locals 2

    .line 139
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->cancelTransitions()V

    const/4 v0, 0x0

    .line 140
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    const/high16 v0, 0x3f800000    # 1.0f

    .line 141
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    .line 142
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    iget v1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    invoke-direct {p0, v0, v1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->updateDrawableAlphas(Landroid/graphics/drawable/Drawable;F)V

    return-void
.end method

.method public setToSuccess()V
    .locals 2

    .line 119
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->cancelTransitions()V

    const/high16 v0, 0x3f800000    # 1.0f

    .line 120
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    const/4 v0, 0x0

    .line 121
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    .line 122
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    iget v1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    invoke-direct {p0, v0, v1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->updateDrawableAlphas(Landroid/graphics/drawable/Drawable;F)V

    return-void
.end method

.method public transitionToFailure()V
    .locals 3

    const/4 v0, 0x0

    .line 126
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    .line 127
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v2, 0x0

    aput v0, v1, v2

    .line 128
    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToFailure:Landroid/animation/ValueAnimator;

    .line 129
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToFailure:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->duration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 130
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToFailure:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/squareup/marin/widgets/-$$Lambda$MarinSpinnerGlyph$0fR8ngmQ0r72o5F7AyCAI237UcQ;

    invoke-direct {v1, p0}, Lcom/squareup/marin/widgets/-$$Lambda$MarinSpinnerGlyph$0fR8ngmQ0r72o5F7AyCAI237UcQ;-><init>(Lcom/squareup/marin/widgets/MarinSpinnerGlyph;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToFailure:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method public transitionToSuccess()V
    .locals 3

    const/4 v0, 0x0

    .line 106
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->successTransitionProgress:F

    .line 107
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->failureTransitionProgress:F

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v2, 0x0

    aput v0, v1, v2

    .line 108
    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess:Landroid/animation/ValueAnimator;

    .line 109
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->duration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 110
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/squareup/marin/widgets/-$$Lambda$MarinSpinnerGlyph$DtBeZi_DSEQDT-8eykpqB4Gx5xw;

    invoke-direct {v1, p0}, Lcom/squareup/marin/widgets/-$$Lambda$MarinSpinnerGlyph$DtBeZi_DSEQDT-8eykpqB4Gx5xw;-><init>(Lcom/squareup/marin/widgets/MarinSpinnerGlyph;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method
