.class public Lcom/squareup/marin/widgets/MarinTagDrawable;
.super Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;
.source "MarinTagDrawable.java"


# static fields
.field private static final GEN_ROTATE_ANGLE:F = -45.0f

.field private static final LARGE_TEXT_BUMP_DP:F = 1.0f

.field private static final LARGE_TEXT_WIDTH_DP:F = 33.0f

.field private static final MEDIUM_TEXT_BUMP_DP:F = 1.0f

.field private static final MEDIUM_TEXT_WIDTH_DP:F = 26.0f


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected calculateTextBoundBottom(ZI)I
    .locals 0

    .line 49
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinTagDrawable;->getIntrinsicHeight()I

    move-result p1

    return p1
.end method

.method protected calculateTextBoundTop(ZI)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method protected calculateTextBoundWidth(ZI)I
    .locals 0

    if-eqz p1, :cond_0

    const/high16 p1, 0x42040000    # 33.0f

    goto :goto_0

    :cond_0
    const/high16 p1, 0x41d00000    # 26.0f

    .line 53
    :goto_0
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->dpToPxRounded(FI)I

    move-result p1

    return p1
.end method

.method protected calculateTextPositionX(ZI)I
    .locals 0

    const/high16 p1, 0x3f800000    # 1.0f

    .line 57
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->dpToPxRounded(FI)I

    move-result p1

    .line 58
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinTagDrawable;->getIntrinsicWidth()I

    move-result p2

    div-int/lit8 p2, p2, 0x2

    add-int/2addr p2, p1

    return p2
.end method

.method protected doDrawText(Landroid/graphics/Canvas;)V
    .locals 4

    .line 32
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinTagDrawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 33
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinTagDrawable;->getIntrinsicHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 34
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v0, v0

    int-to-float v1, v1

    const/high16 v2, -0x3dcc0000    # -45.0f

    .line 35
    invoke-virtual {p1, v2, v0, v1}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 36
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinTagDrawable;->text:Ljava/lang/String;

    iget v1, p0, Lcom/squareup/marin/widgets/MarinTagDrawable;->textXPosition:F

    iget v2, p0, Lcom/squareup/marin/widgets/MarinTagDrawable;->textYPosition:F

    iget-object v3, p0, Lcom/squareup/marin/widgets/MarinTagDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 37
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method protected fetchTextShadowDx(Landroid/content/res/Resources;)F
    .locals 1

    .line 41
    sget v0, Lcom/squareup/glyph/R$dimen;->glyph_shadow_dx_45_angle:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    neg-int p1, p1

    int-to-float p1, p1

    return p1
.end method

.method protected getGlyph(Z)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    if-eqz p1, :cond_0

    .line 62
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    return-object p1
.end method

.method public setAlpha(I)V
    .locals 0

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method
