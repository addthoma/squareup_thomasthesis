.class public Lcom/squareup/marin/widgets/MarinActionBar;
.super Ljava/lang/Object;
.source "MarinActionBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/marin/widgets/MarinActionBar$Config;
    }
.end annotation


# instance fields
.field private config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

.field private view:Lcom/squareup/marin/widgets/MarinBaseActionBarView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar;->view:Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    return-void
.end method

.method private static fire(Ljava/lang/Runnable;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 462
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method private getView()Lcom/squareup/marin/widgets/MarinBaseActionBarView;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar;->view:Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    return-object v0
.end method

.method private setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V
    .locals 1

    .line 67
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;Z)V

    return-void
.end method

.method private setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;Z)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    if-eqz p2, :cond_0

    .line 62
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateView()V

    :cond_0
    return-void
.end method

.method private updateBackground()V
    .locals 2

    .line 429
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getView()Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    move-result-object v0

    .line 430
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->background:I

    if-eqz v1, :cond_0

    .line 431
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->background:I

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setBackgroundResource(I)V

    :cond_0
    return-void
.end method

.method private updateBadges()V
    .locals 2

    .line 238
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 242
    :cond_0
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getView()Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    move-result-object v0

    .line 244
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-boolean v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->canShowBadges:Z

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->canShowBadges(Z)V

    return-void
.end method

.method private updateCustomView()V
    .locals 4

    .line 396
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 400
    :cond_0
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getView()Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    move-result-object v0

    .line 402
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;

    .line 403
    iget-object v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->customViewFactory:Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;

    if-nez v2, :cond_1

    const/4 v1, 0x0

    .line 404
    invoke-virtual {v0, v1, v1}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setCustomView(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 405
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->hideCustomView()V

    return-void

    .line 409
    :cond_1
    iget-object v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->customViewFactory:Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;->buildView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v2

    .line 410
    iget-object v3, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->tooltip:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setCustomView(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 411
    iget-boolean v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->enabled:Z

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setCustomViewEnabled(Z)V

    .line 413
    iget-boolean v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->visible:Z

    if-eqz v1, :cond_2

    .line 414
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->showCustomView()V

    goto :goto_0

    .line 416
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->hideCustomView()V

    :goto_0
    return-void
.end method

.method private updateFourthButton()V
    .locals 5

    .line 370
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 374
    :cond_0
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getView()Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    move-result-object v0

    .line 375
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->fourthButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    .line 377
    iget-boolean v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->enabled:Z

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->command:Ljava/lang/Runnable;

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setFourthButtonEnabled(Z)V

    .line 379
    iget-object v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_3

    .line 381
    iget-object v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->tooltip:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setFourthButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    .line 383
    invoke-virtual {v0, v2, v2}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setFourthButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 388
    :goto_2
    iget-boolean v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->visible:Z

    if-eqz v1, :cond_4

    .line 389
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->showFourthButton()V

    goto :goto_3

    .line 391
    :cond_4
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->hideFourthButton()V

    :goto_3
    return-void
.end method

.method private updatePrimaryButton()V
    .locals 2

    .line 293
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 297
    :cond_0
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getView()Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    move-result-object v0

    .line 299
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-boolean v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->primaryButtonEnabled:Z

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setPrimaryButtonEnabled(Z)V

    .line 300
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->primaryButtonText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 302
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-boolean v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->primaryButtonVisible:Z

    if-eqz v1, :cond_1

    .line 303
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->showPrimaryButton()V

    goto :goto_0

    .line 305
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->hidePrimaryButton()V

    :goto_0
    return-void
.end method

.method private updateSecondaryButton()V
    .locals 5

    .line 310
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 314
    :cond_0
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getView()Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    move-result-object v0

    .line 315
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->secondaryButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    .line 317
    iget-boolean v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->enabled:Z

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->command:Ljava/lang/Runnable;

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setSecondaryButtonEnabled(Z)V

    .line 319
    iget-object v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_3

    .line 321
    iget-object v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->tooltip:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setSecondaryButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    .line 323
    invoke-virtual {v0, v2, v2}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setSecondaryButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 326
    :goto_2
    iget-object v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->text:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setSecondaryButtonText(Ljava/lang/CharSequence;)V

    .line 328
    iget v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->textColor:I

    if-eqz v2, :cond_4

    .line 329
    iget v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->textColor:I

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setSecondaryButtonTextColor(I)V

    .line 332
    :cond_4
    iget v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->backgroundColor:I

    if-eqz v2, :cond_5

    .line 333
    iget v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->backgroundColor:I

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setSecondaryButtonBackgroundColor(I)V

    .line 336
    :cond_5
    iget-boolean v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->visible:Z

    if-eqz v1, :cond_6

    .line 337
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->showSecondaryButton()V

    goto :goto_3

    .line 339
    :cond_6
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->hideSecondaryButton()V

    :goto_3
    return-void
.end method

.method private updateThirdButton()V
    .locals 5

    .line 344
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 348
    :cond_0
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getView()Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    move-result-object v0

    .line 349
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->thirdButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    .line 351
    iget-boolean v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->enabled:Z

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->command:Ljava/lang/Runnable;

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setThirdButtonEnabled(Z)V

    .line 353
    iget-object v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_3

    .line 355
    iget-object v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->tooltip:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setThirdButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    .line 357
    invoke-virtual {v0, v2, v2}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setThirdButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 360
    :goto_2
    iget-object v2, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->text:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setThirdButtonText(Ljava/lang/CharSequence;)V

    .line 362
    iget-boolean v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->visible:Z

    if-eqz v1, :cond_4

    .line 363
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->showThirdButton()V

    goto :goto_3

    .line 365
    :cond_4
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->hideThirdButton()V

    :goto_3
    return-void
.end method

.method private updateUpButton()V
    .locals 4

    .line 248
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 252
    :cond_0
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getView()Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    move-result-object v0

    .line 253
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-boolean v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonEnabled:Z

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonCommand:Ljava/lang/Runnable;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setUpButtonEnabled(Z)V

    .line 255
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz v1, :cond_2

    const/4 v2, 0x1

    :cond_2
    if-eqz v2, :cond_4

    .line 257
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonTooltip:Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonTooltip:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 259
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->getUpButtonTooltip(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 260
    :goto_1
    iget-object v2, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v2, v2, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 262
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonGlyphColor:I

    if-eqz v1, :cond_5

    .line 263
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonGlyphColor:I

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setUpGlyphColor(I)V

    goto :goto_2

    .line 266
    :cond_4
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->clearUpGlyph()V

    .line 269
    :cond_5
    :goto_2
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonText:Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/2addr v1, v3

    if-eqz v1, :cond_7

    .line 271
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setUpText(Ljava/lang/CharSequence;)V

    .line 272
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-boolean v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonRightPadded:Z

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setUpTextRightPadded(Z)V

    .line 273
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-boolean v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonCentered:Z

    if-eqz v1, :cond_6

    .line 274
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setUpCenteredText()V

    .line 276
    :cond_6
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->showUpText()V

    .line 278
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonTextColor:I

    if-eqz v1, :cond_8

    .line 279
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonTextColor:I

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->setUpTextColor(I)V

    goto :goto_3

    .line 282
    :cond_7
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->hideUpText()V

    .line 285
    :cond_8
    :goto_3
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-boolean v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonVisible:Z

    if-eqz v1, :cond_9

    .line 286
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->showUpGlyph()V

    goto :goto_4

    .line 288
    :cond_9
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->hideUpGlyph()V

    :goto_4
    return-void
.end method

.method private updateView()V
    .locals 1

    .line 207
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 211
    :cond_0
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateBadges()V

    .line 212
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateUpButton()V

    .line 213
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updatePrimaryButton()V

    .line 214
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateSecondaryButton()V

    .line 215
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateThirdButton()V

    .line 216
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateFourthButton()V

    .line 217
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateCustomView()V

    .line 218
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateVisibility()V

    .line 219
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateBackground()V

    .line 221
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->announceForAccessibility()V

    return-void
.end method

.method private updateVisibility()V
    .locals 2

    .line 421
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 425
    :cond_0
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getView()Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-boolean v1, v1, Lcom/squareup/marin/widgets/MarinActionBar$Config;->hidden:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method


# virtual methods
.method protected announceForAccessibility()V
    .locals 2

    .line 234
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getView()Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/accessibility/Accessibility;->announceForAccessibility(Landroid/view/View;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final dropView(Lcom/squareup/marin/widgets/MarinBaseActionBarView;)V
    .locals 1

    if-eqz p1, :cond_1

    .line 35
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar;->view:Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    .line 36
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->view:Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    :cond_0
    return-void

    .line 34
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "dropped view must not be null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    return-object v0
.end method

.method protected getContentDescription()Ljava/lang/CharSequence;
    .locals 2

    .line 226
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 227
    iget-object v1, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->contentDescription:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 228
    iget-object v0, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->contentDescription:Ljava/lang/String;

    return-object v0

    .line 230
    :cond_0
    iget-object v0, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final hasView()Z
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar;->view:Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hide()V
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hide()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 77
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateVisibility()V

    return-void
.end method

.method public hideButton4()V
    .locals 1

    .line 201
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideButton4()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 202
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateFourthButton()V

    return-void
.end method

.method public hidePrimaryButton()V
    .locals 1

    .line 132
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 133
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updatePrimaryButton()V

    return-void
.end method

.method public hideSecondaryButton()V
    .locals 1

    .line 158
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 159
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateSecondaryButton()V

    return-void
.end method

.method public hideThirdButton()V
    .locals 1

    .line 184
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideThirdButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 185
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateThirdButton()V

    return-void
.end method

.method public hideUpButton()V
    .locals 1

    .line 111
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 112
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateUpButton()V

    return-void
.end method

.method public onCustomViewClicked()V
    .locals 1

    .line 457
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v0, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;

    iget-object v0, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->command:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->fire(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onFourthButtonClicked()V
    .locals 1

    .line 453
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v0, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->fourthButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    iget-object v0, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->command:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->fire(Ljava/lang/Runnable;)V

    return-void
.end method

.method onPrimaryButtonClicked()V
    .locals 1

    .line 441
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v0, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->primaryButtonCommand:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->fire(Ljava/lang/Runnable;)V

    return-void
.end method

.method onSecondaryButtonClicked()V
    .locals 1

    .line 445
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v0, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->secondaryButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    iget-object v0, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->command:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->fire(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onThirdButtonClicked()V
    .locals 1

    .line 449
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v0, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->thirdButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    iget-object v0, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->command:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->fire(Ljava/lang/Runnable;)V

    return-void
.end method

.method onUpButtonClicked()V
    .locals 1

    .line 437
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar;->config:Lcom/squareup/marin/widgets/MarinActionBar$Config;

    iget-object v0, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonCommand:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->fire(Ljava/lang/Runnable;)V

    return-void
.end method

.method public resetConfig()V
    .locals 1

    .line 57
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method public setButton4GlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V
    .locals 1

    .line 196
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setButton4GlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 197
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateFourthButton()V

    return-void
.end method

.method public setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    const/4 v0, 0x1

    .line 53
    invoke-direct {p0, p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;Z)V

    return-void
.end method

.method public setPrimaryButtonEnabled(Z)V
    .locals 1

    .line 137
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 138
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updatePrimaryButton()V

    return-void
.end method

.method public setPrimaryButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 122
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 123
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updatePrimaryButton()V

    return-void
.end method

.method public setSecondaryButtonEnabled(Z)V
    .locals 1

    .line 163
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 164
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateSecondaryButton()V

    return-void
.end method

.method public setSecondaryButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V
    .locals 1

    .line 143
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 144
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateSecondaryButton()V

    return-void
.end method

.method public setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)V
    .locals 1

    .line 148
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 149
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateSecondaryButton()V

    return-void
.end method

.method public setThirdButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V
    .locals 1

    .line 179
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setThirdButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 180
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateThirdButton()V

    return-void
.end method

.method public setThirdButtonTextNoGlyph(Ljava/lang/CharSequence;)V
    .locals 1

    .line 174
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setThirdButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 175
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateThirdButton()V

    return-void
.end method

.method public setTitleNoUpButton(Ljava/lang/CharSequence;)V
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setTitleNoUpButton(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 83
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateUpButton()V

    return-void
.end method

.method public setUpButtonEnabled(Z)V
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 117
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateUpButton()V

    return-void
.end method

.method public setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V
    .locals 1

    .line 87
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 88
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateUpButton()V

    return-void
.end method

.method public setUpButtonTextBackArrow(Ljava/lang/CharSequence;)V
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 93
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateUpButton()V

    return-void
.end method

.method public setUpButtonTextX(Ljava/lang/CharSequence;)V
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextX(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 98
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateUpButton()V

    return-void
.end method

.method public show()V
    .locals 1

    .line 71
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->show()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 72
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateVisibility()V

    return-void
.end method

.method public showButton4(Ljava/lang/Runnable;)V
    .locals 1

    .line 191
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showButton4(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 192
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateFourthButton()V

    return-void
.end method

.method public showPrimaryButton(Ljava/lang/Runnable;)V
    .locals 1

    .line 127
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 128
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updatePrimaryButton()V

    return-void
.end method

.method public showSecondaryButton(Ljava/lang/Runnable;)V
    .locals 1

    .line 153
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 154
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateSecondaryButton()V

    return-void
.end method

.method public showThirdButton(Ljava/lang/Runnable;)V
    .locals 1

    .line 169
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showThirdButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 170
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateThirdButton()V

    return-void
.end method

.method public showUpButton(Ljava/lang/Runnable;)V
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    .line 107
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateUpButton()V

    return-void
.end method

.method public showUpButtonNotClickable()V
    .locals 1

    const/4 v0, 0x0

    .line 102
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final takeView(Lcom/squareup/marin/widgets/MarinBaseActionBarView;)V
    .locals 1

    if-eqz p1, :cond_2

    .line 26
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar;->view:Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    if-eq v0, p1, :cond_1

    if-eqz v0, :cond_0

    .line 27
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->dropView(Lcom/squareup/marin/widgets/MarinBaseActionBarView;)V

    .line 28
    :cond_0
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar;->view:Lcom/squareup/marin/widgets/MarinBaseActionBarView;

    .line 29
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinActionBar;->updateView()V

    :cond_1
    return-void

    .line 24
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "new view must not be null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
