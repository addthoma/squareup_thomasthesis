.class public Lcom/squareup/marin/widgets/MarinDatePicker;
.super Landroid/widget/DatePicker;
.source "MarinDatePicker.java"


# instance fields
.field protected dayPicker:Landroid/widget/NumberPicker;

.field private final marinGapTiny:I

.field protected monthPicker:Landroid/widget/NumberPicker;

.field protected yearPicker:Landroid/widget/NumberPicker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 22
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/marin/widgets/MarinDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/DatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinDatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gap_tiny:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->marinGapTiny:I

    const/4 p1, 0x0

    .line 30
    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinDatePicker;->setCalendarViewShown(Z)V

    return-void
.end method

.method private setPickerSize(Landroid/widget/NumberPicker;II)V
    .locals 1

    .line 94
    invoke-virtual {p1}, Landroid/widget/NumberPicker;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    mul-int/lit8 p2, p2, 0x3

    .line 95
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 96
    iput p3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 97
    invoke-virtual {p1, v0}, Landroid/widget/NumberPicker;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private setPickerWeight(Landroid/widget/NumberPicker;)V
    .locals 2

    .line 111
    invoke-virtual {p1}, Landroid/widget/NumberPicker;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    .line 112
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    const/high16 v1, 0x3f800000    # 1.0f

    .line 113
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 114
    invoke-virtual {p1, v0}, Landroid/widget/NumberPicker;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public getInnerMargin()I
    .locals 1

    .line 56
    iget v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->marinGapTiny:I

    return v0
.end method

.method public getYearPicker()Landroid/widget/NumberPicker;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->yearPicker:Landroid/widget/NumberPicker;

    return-object v0
.end method

.method public isYearLeftmost()Z
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->yearPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->monthPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getX()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->yearPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->dayPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getX()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 34
    invoke-super {p0}, Landroid/widget/DatePicker;->onFinishInflate()V

    .line 40
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "android"

    const-string v2, "id"

    const-string v3, "day"

    invoke-virtual {v0, v3, v2, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->dayPicker:Landroid/widget/NumberPicker;

    .line 41
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v3, "month"

    invoke-virtual {v0, v3, v2, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->monthPicker:Landroid/widget/NumberPicker;

    .line 42
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v3, "year"

    invoke-virtual {v0, v3, v2, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/NumberPicker;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->yearPicker:Landroid/widget/NumberPicker;

    .line 44
    iget v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->marinGapTiny:I

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v1, v0, v0}, Lcom/squareup/marin/widgets/MarinDatePicker;->setInnerMargins(IIII)V

    return-void
.end method

.method protected setInnerMargins(IIII)V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->yearPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 61
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 62
    iput p3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 63
    iput p4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 64
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 66
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->monthPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 67
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 68
    iput p3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 69
    iput p4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 70
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 72
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->dayPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 73
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 74
    iput p3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 75
    iput p4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 76
    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    return-void
.end method

.method protected setPickerSize(II)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->yearPicker:Landroid/widget/NumberPicker;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/marin/widgets/MarinDatePicker;->setPickerSize(Landroid/widget/NumberPicker;II)V

    .line 89
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->monthPicker:Landroid/widget/NumberPicker;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/marin/widgets/MarinDatePicker;->setPickerSize(Landroid/widget/NumberPicker;II)V

    .line 90
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->dayPicker:Landroid/widget/NumberPicker;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/marin/widgets/MarinDatePicker;->setPickerSize(Landroid/widget/NumberPicker;II)V

    return-void
.end method

.method public setSameWeightForPickers()V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->yearPicker:Landroid/widget/NumberPicker;

    invoke-direct {p0, v0}, Lcom/squareup/marin/widgets/MarinDatePicker;->setPickerWeight(Landroid/widget/NumberPicker;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->monthPicker:Landroid/widget/NumberPicker;

    invoke-direct {p0, v0}, Lcom/squareup/marin/widgets/MarinDatePicker;->setPickerWeight(Landroid/widget/NumberPicker;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinDatePicker;->dayPicker:Landroid/widget/NumberPicker;

    invoke-direct {p0, v0}, Lcom/squareup/marin/widgets/MarinDatePicker;->setPickerWeight(Landroid/widget/NumberPicker;)V

    return-void
.end method
