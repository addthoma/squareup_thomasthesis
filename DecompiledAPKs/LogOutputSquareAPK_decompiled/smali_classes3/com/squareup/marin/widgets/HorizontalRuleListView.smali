.class public Lcom/squareup/marin/widgets/HorizontalRuleListView;
.super Landroid/widget/ListView;
.source "HorizontalRuleListView.java"


# instance fields
.field private final rulePaint:Landroid/graphics/Paint;

.field private rulePosition:I

.field private ruler:Lcom/squareup/marin/widgets/HorizontalRule;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/marin/widgets/HorizontalRuleListView;->rulePaint:Landroid/graphics/Paint;

    .line 25
    iget-object p1, p0, Lcom/squareup/marin/widgets/HorizontalRuleListView;->rulePaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/HorizontalRuleListView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 27
    iget-object p1, p0, Lcom/squareup/marin/widgets/HorizontalRuleListView;->rulePaint:Landroid/graphics/Paint;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 p1, 0x0

    .line 28
    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/HorizontalRuleListView;->setClipToPadding(Z)V

    return-void
.end method


# virtual methods
.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 6

    .line 32
    instance-of v0, p2, Lcom/squareup/marin/widgets/HorizontalRule;

    if-eqz v0, :cond_2

    .line 33
    move-object v0, p2

    check-cast v0, Lcom/squareup/marin/widgets/HorizontalRule;

    iput-object v0, p0, Lcom/squareup/marin/widgets/HorizontalRuleListView;->ruler:Lcom/squareup/marin/widgets/HorizontalRule;

    .line 34
    iget-object v0, p0, Lcom/squareup/marin/widgets/HorizontalRuleListView;->ruler:Lcom/squareup/marin/widgets/HorizontalRule;

    invoke-interface {v0}, Lcom/squareup/marin/widgets/HorizontalRule;->getRulePosition()I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/HorizontalRuleListView;->rulePosition:I

    .line 35
    iget-object v0, p0, Lcom/squareup/marin/widgets/HorizontalRuleListView;->ruler:Lcom/squareup/marin/widgets/HorizontalRule;

    invoke-interface {v0}, Lcom/squareup/marin/widgets/HorizontalRule;->drawSelf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 39
    :cond_0
    iget p2, p0, Lcom/squareup/marin/widgets/HorizontalRuleListView;->rulePosition:I

    const/4 p3, -0x1

    const/4 p4, 0x1

    if-eq p2, p3, :cond_1

    const/4 v1, 0x0

    int-to-float v2, p2

    .line 40
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result p2

    int-to-float v3, p2

    iget p2, p0, Lcom/squareup/marin/widgets/HorizontalRuleListView;->rulePosition:I

    add-int/2addr p2, p4

    int-to-float v4, p2

    iget-object v5, p0, Lcom/squareup/marin/widgets/HorizontalRuleListView;->rulePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_1
    const/4 p1, 0x0

    .line 43
    iput-object p1, p0, Lcom/squareup/marin/widgets/HorizontalRuleListView;->ruler:Lcom/squareup/marin/widgets/HorizontalRule;

    return p4

    .line 46
    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result p1

    return p1
.end method
