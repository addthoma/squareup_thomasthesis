.class final Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$6;
.super Lkotlin/jvm/internal/Lambda;
.source "CustomizeReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->handleCustomizeAllFieldsEvent(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "-",
        "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

.field final synthetic this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$6;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    iput-object p2, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$6;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$6;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/salesreport/customize/CustomizeReportState;",
            "-",
            "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 223
    iget-object v1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$6;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    .line 224
    invoke-virtual {v1}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$6;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    invoke-virtual {v2}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/customreport/data/ReportConfig;->getAllDay()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    invoke-static {v0, v2}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->withAllDay(Lcom/squareup/customreport/data/ReportConfig;Z)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v2

    .line 225
    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$6;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    invoke-static {v0}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->access$getEmployeeManagement$p(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;)Lcom/squareup/permissions/EmployeeManagement;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/salesreport/util/EmployeeManagementsKt;->isEnabled(Lcom/squareup/permissions/EmployeeManagement;)Z

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    .line 223
    invoke-static/range {v1 .. v6}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->copy$default(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/customreport/data/ReportConfig;ZZILjava/lang/Object;)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
