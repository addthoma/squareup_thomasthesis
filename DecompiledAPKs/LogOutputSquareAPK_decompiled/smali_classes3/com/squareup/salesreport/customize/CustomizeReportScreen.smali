.class public abstract Lcom/squareup/salesreport/customize/CustomizeReportScreen;
.super Ljava/lang/Object;
.source "CustomizeReportScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;,
        Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;,
        Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0007\u0008\tB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\n\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "state",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "(Lcom/squareup/salesreport/customize/CustomizeReportState;)V",
        "getState",
        "()Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "CustomizeAllFieldsScreen",
        "CustomizeReportEvent",
        "EmployeeSelectionScreen",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final state:Lcom/squareup/salesreport/customize/CustomizeReportState;


# direct methods
.method private constructor <init>(Lcom/squareup/salesreport/customize/CustomizeReportState;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen;->state:Lcom/squareup/salesreport/customize/CustomizeReportState;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/salesreport/customize/CustomizeReportState;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportState;)V

    return-void
.end method


# virtual methods
.method public getState()Lcom/squareup/salesreport/customize/CustomizeReportState;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen;->state:Lcom/squareup/salesreport/customize/CustomizeReportState;

    return-object v0
.end method
