.class final Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$4;
.super Lkotlin/jvm/internal/Lambda;
.source "EmployeeSelectionCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->onScreen(Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Update<",
        "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEmployeeSelectionCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EmployeeSelectionCoordinator.kt\ncom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$4\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,188:1\n1360#2:189\n1429#2,3:190\n*E\n*S KotlinDebug\n*F\n+ 1 EmployeeSelectionCoordinator.kt\ncom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$4\n*L\n124#1:189\n124#1,3:190\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/Update;",
        "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;

.field final synthetic this$0:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$4;->this$0:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;

    iput-object p2, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$4;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/cycler/Update;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$4;->invoke(Lcom/squareup/cycler/Update;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/Update;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Update<",
            "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$4;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;

    check-cast v0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;

    invoke-virtual {v0}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;->getDisplayedEmployeeFilters()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 189
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 190
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 191
    check-cast v2, Lcom/squareup/customreport/data/EmployeeFilter;

    .line 127
    iget-object v3, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$4;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;

    invoke-virtual {v3}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/customreport/data/ReportConfig;->getEmployeeFiltersSelection()Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    move-result-object v3

    .line 128
    instance-of v4, v3, Lcom/squareup/customreport/data/EmployeeFiltersSelection$AllEmployeesSelection;

    if-nez v4, :cond_1

    .line 129
    instance-of v4, v3, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;

    if-eqz v4, :cond_0

    check-cast v3, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->getEmployeeFilters()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    goto :goto_2

    :cond_1
    :goto_1
    const/4 v3, 0x1

    .line 125
    :goto_2
    new-instance v4, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;

    invoke-direct {v4, v2, v3}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;-><init>(Lcom/squareup/customreport/data/EmployeeFilter;Z)V

    .line 131
    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 192
    :cond_2
    check-cast v1, Ljava/util/List;

    .line 133
    invoke-static {v1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setData(Lcom/squareup/cycler/DataSource;)V

    .line 134
    iget-object p1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$4;->this$0:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;

    invoke-static {p1}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->access$getEmployeeSelectionAnimator$p(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;)Lcom/squareup/widgets/SquareViewAnimator;

    move-result-object p1

    .line 135
    iget-object v0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$4;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;

    check-cast v0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;

    invoke-virtual {v0}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;->getDisplayedEmployeeFilters()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$onScreen$4;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;

    check-cast v0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;

    invoke-virtual {v0}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;->getAllEmployeeFilters()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 136
    sget v0, Lcom/squareup/salesreport/impl/R$id;->employee_selection_no_search_results:I

    goto :goto_3

    .line 138
    :cond_3
    sget v0, Lcom/squareup/salesreport/impl/R$id;->employee_selection_recycler_view:I

    .line 134
    :goto_3
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method
