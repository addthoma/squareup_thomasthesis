.class final Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$1;
.super Ljava/lang/Object;
.source "CustomizeReportWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->render(Lcom/squareup/salesreport/customize/CustomizeReportInput;Lcom/squareup/salesreport/customize/CustomizeReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomizeReportWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomizeReportWorkflow.kt\ncom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,387:1\n1360#2:388\n1429#2,3:389\n950#2:392\n*E\n*S KotlinDebug\n*F\n+ 1 CustomizeReportWorkflow.kt\ncom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$1\n*L\n105#1:388\n105#1,3:389\n113#1:392\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012(\u0010\u0003\u001a$\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00070\u0004H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/customreport/data/EmployeeFilter;",
        "employeesSet",
        "",
        "Lcom/squareup/permissions/Employee;",
        "kotlin.jvm.PlatformType",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$1;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$1;->apply(Ljava/util/Set;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Ljava/util/Set;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/customreport/data/EmployeeFilter;",
            ">;"
        }
    .end annotation

    const-string v0, "employeesSet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    sget-object v0, Lcom/squareup/customreport/data/EmployeeFilter$UnattributedEmployeeFilter;->INSTANCE:Lcom/squareup/customreport/data/EmployeeFilter$UnattributedEmployeeFilter;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 104
    check-cast p1, Ljava/lang/Iterable;

    .line 388
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 389
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 390
    check-cast v2, Lcom/squareup/permissions/Employee;

    .line 106
    new-instance v3, Lcom/squareup/customreport/data/EmployeeFilter$SingleEmployeeFilter;

    .line 108
    iget-object v4, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$1;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    invoke-static {v4}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->access$getRes$p(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;)Lcom/squareup/util/Res;

    move-result-object v4

    iget-object v5, v2, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    iget-object v6, v2, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    iget-object v7, v2, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    .line 107
    invoke-static {v4, v5, v6, v7}, Lcom/squareup/permissions/Employee;->getShortDisplayName(Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "Employee.getShortDisplay\u2026                        )"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iget-object v2, v2, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    const-string v5, "employee.token"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-direct {v3, v4, v2}, Lcom/squareup/customreport/data/EmployeeFilter$SingleEmployeeFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 391
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 392
    new-instance p1, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$1$$special$$inlined$sortedBy$1;

    invoke-direct {p1}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$1$$special$$inlined$sortedBy$1;-><init>()V

    check-cast p1, Ljava/util/Comparator;

    invoke-static {v1, p1}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 103
    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
