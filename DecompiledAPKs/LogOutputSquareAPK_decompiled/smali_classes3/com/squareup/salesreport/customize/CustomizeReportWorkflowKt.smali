.class public final Lcom/squareup/salesreport/customize/CustomizeReportWorkflowKt;
.super Ljava/lang/Object;
.source "CustomizeReportWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomizeReportWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomizeReportWorkflow.kt\ncom/squareup/salesreport/customize/CustomizeReportWorkflowKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,387:1\n704#2:388\n777#2,2:389\n*E\n*S KotlinDebug\n*F\n+ 1 CustomizeReportWorkflow.kt\ncom/squareup/salesreport/customize/CustomizeReportWorkflowKt\n*L\n373#1:388\n373#1,2:389\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0002\u001a\u001c\u0010\u0006\u001a\u00020\u0007*\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0002\u00a8\u0006\u000c"
    }
    d2 = {
        "getStateFromSearchTermFiltering",
        "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;",
        "searchTerm",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "shouldResetTimes",
        "",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "newStartDate",
        "Lorg/threeten/bp/LocalDate;",
        "newEndDate",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getStateFromSearchTermFiltering(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;Ljava/lang/String;Lcom/squareup/util/Res;)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/salesreport/customize/CustomizeReportWorkflowKt;->getStateFromSearchTermFiltering(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;Ljava/lang/String;Lcom/squareup/util/Res;)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$shouldResetTimes(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/salesreport/customize/CustomizeReportWorkflowKt;->shouldResetTimes(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)Z

    move-result p0

    return p0
.end method

.method private static final getStateFromSearchTermFiltering(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;Ljava/lang/String;Lcom/squareup/util/Res;)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;
    .locals 10

    .line 373
    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;->getAllEmployeeFilters()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 388
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 389
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/customreport/data/EmployeeFilter;

    .line 374
    invoke-static {v3, p2}, Lcom/squareup/salesreport/util/EmployeeFiltersKt;->displayValue(Lcom/squareup/customreport/data/EmployeeFilter;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 375
    move-object v4, p1

    check-cast v4, Ljava/lang/CharSequence;

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lkotlin/text/StringsKt;->contains(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 390
    :cond_1
    move-object v7, v1

    check-cast v7, Ljava/util/List;

    const/4 v8, 0x3

    const/4 v9, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v4, p0

    .line 373
    invoke-static/range {v4 .. v9}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;->copy$default(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;Lcom/squareup/customreport/data/ReportConfig;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;

    move-result-object p0

    return-object p0
.end method

.method private static final shouldResetTimes(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)Z
    .locals 1

    .line 386
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getAllDay()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartTime()Lorg/threeten/bp/LocalTime;

    move-result-object p1

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndTime()Lorg/threeten/bp/LocalTime;

    move-result-object p0

    invoke-virtual {p1, p0}, Lorg/threeten/bp/LocalTime;->compareTo(Lorg/threeten/bp/LocalTime;)I

    move-result p0

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
