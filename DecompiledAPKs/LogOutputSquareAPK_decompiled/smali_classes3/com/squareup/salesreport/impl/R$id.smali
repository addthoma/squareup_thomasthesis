.class public final Lcom/squareup/salesreport/impl/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_bar_with_two_action_icons:I = 0x7f0a013d

.field public static final action_icon_2:I = 0x7f0a0145

.field public static final amount_cell_label:I = 0x7f0a01b9

.field public static final amount_cell_percentage_value:I = 0x7f0a01ba

.field public static final amount_cell_value:I = 0x7f0a01bb

.field public static final bar_chart:I = 0x7f0a021d

.field public static final cancel_button:I = 0x7f0a0299

.field public static final compare_label:I = 0x7f0a036f

.field public static final compare_value:I = 0x7f0a0370

.field public static final comparison_range_label:I = 0x7f0a0371

.field public static final config_all_day:I = 0x7f0a0375

.field public static final config_all_devices:I = 0x7f0a0376

.field public static final config_device_filter:I = 0x7f0a0377

.field public static final config_device_filter_container:I = 0x7f0a0378

.field public static final config_employee_filter:I = 0x7f0a0379

.field public static final config_employee_filter_container:I = 0x7f0a037a

.field public static final config_end_time_row:I = 0x7f0a037b

.field public static final config_start_time_row:I = 0x7f0a037d

.field public static final config_this_device:I = 0x7f0a037e

.field public static final current_label:I = 0x7f0a051f

.field public static final current_value:I = 0x7f0a0523

.field public static final customize_report_date_picker:I = 0x7f0a053f

.field public static final email_button:I = 0x7f0a06b4

.field public static final email_edit:I = 0x7f0a06bf

.field public static final email_items_selected:I = 0x7f0a06c1

.field public static final email_report_address_view:I = 0x7f0a06c7

.field public static final email_report_animator:I = 0x7f0a06c8

.field public static final email_report_message_view:I = 0x7f0a06c9

.field public static final email_report_view:I = 0x7f0a06ca

.field public static final employee_selection_animator:I = 0x7f0a06ea

.field public static final employee_selection_container:I = 0x7f0a06eb

.field public static final employee_selection_list_animator:I = 0x7f0a06ec

.field public static final employee_selection_no_search_results:I = 0x7f0a06ed

.field public static final employee_selection_progress_bar:I = 0x7f0a06ee

.field public static final employee_selection_recycler_view:I = 0x7f0a06ef

.field public static final employee_selection_search:I = 0x7f0a06f0

.field public static final export_report_view:I = 0x7f0a073c

.field public static final guideline:I = 0x7f0a07c1

.field public static final line_chart:I = 0x7f0a093c

.field public static final popup_actions_container:I = 0x7f0a0c3b

.field public static final print_button:I = 0x7f0a0c62

.field public static final print_items_selected:I = 0x7f0a0c68

.field public static final print_report_animator:I = 0x7f0a0c72

.field public static final print_report_configure_view:I = 0x7f0a0c73

.field public static final print_report_message_view:I = 0x7f0a0c74

.field public static final report_config_sheet_view:I = 0x7f0a0d67

.field public static final sales_details_caret:I = 0x7f0a0dae

.field public static final sales_details_row_name:I = 0x7f0a0daf

.field public static final sales_details_row_sub_value:I = 0x7f0a0db0

.field public static final sales_details_row_two_col_name:I = 0x7f0a0db1

.field public static final sales_details_row_two_col_value:I = 0x7f0a0db2

.field public static final sales_details_row_value:I = 0x7f0a0db3

.field public static final sales_report_animator:I = 0x7f0a0db8

.field public static final sales_report_chart_animator:I = 0x7f0a0db9

.field public static final sales_report_chart_view:I = 0x7f0a0dba

.field public static final sales_report_details_empty_message_view:I = 0x7f0a0dbd

.field public static final sales_report_details_failure_message_view:I = 0x7f0a0dbe

.field public static final sales_report_fee_amount:I = 0x7f0a0dc0

.field public static final sales_report_fee_name:I = 0x7f0a0dc1

.field public static final sales_report_payment_method_bar:I = 0x7f0a0dc9

.field public static final sales_report_payment_method_empty_bar:I = 0x7f0a0dca

.field public static final sales_report_payment_method_name:I = 0x7f0a0dcb

.field public static final sales_report_payment_method_percentage:I = 0x7f0a0dcc

.field public static final sales_report_payment_method_sales:I = 0x7f0a0dcd

.field public static final sales_report_payment_method_total_name:I = 0x7f0a0dce

.field public static final sales_report_payment_method_total_sales:I = 0x7f0a0dcf

.field public static final sales_report_progress_bar:I = 0x7f0a0dd0

.field public static final sales_report_recycler_view:I = 0x7f0a0dd1

.field public static final sales_report_section_header_button:I = 0x7f0a0dd3

.field public static final sales_report_section_header_title:I = 0x7f0a0dd4

.field public static final sales_report_section_header_with_image_button:I = 0x7f0a0dd5

.field public static final sales_report_section_header_with_image_title:I = 0x7f0a0dd6

.field public static final sales_report_subsection_header_label_1:I = 0x7f0a0dd7

.field public static final sales_report_subsection_header_label_2:I = 0x7f0a0dd8

.field public static final sales_report_subsection_header_label_3:I = 0x7f0a0dd9

.field public static final sales_report_tab_1:I = 0x7f0a0dda

.field public static final sales_report_tab_2:I = 0x7f0a0ddb

.field public static final sales_report_time_selector_1d:I = 0x7f0a0ddc

.field public static final sales_report_time_selector_1m:I = 0x7f0a0ddd

.field public static final sales_report_time_selector_1w:I = 0x7f0a0dde

.field public static final sales_report_time_selector_1y:I = 0x7f0a0ddf

.field public static final sales_report_time_selector_3m:I = 0x7f0a0de0

.field public static final sales_report_time_selector_container:I = 0x7f0a0de1

.field public static final sales_report_top_bar_compare_icon:I = 0x7f0a0de2

.field public static final sales_report_top_bar_compare_icon_spacer:I = 0x7f0a0de3

.field public static final sales_report_top_bar_container:I = 0x7f0a0de4

.field public static final sales_report_top_bar_customize_icon:I = 0x7f0a0de5

.field public static final sales_report_top_bar_subtitle:I = 0x7f0a0de6

.field public static final sales_report_top_bar_title:I = 0x7f0a0de7

.field public static final sales_report_top_bar_title_container:I = 0x7f0a0de8

.field public static final sales_report_two_col_payment_method_bar:I = 0x7f0a0de9

.field public static final sales_report_two_col_payment_method_empty_bar:I = 0x7f0a0dea

.field public static final sales_report_two_col_payment_method_name:I = 0x7f0a0deb

.field public static final sales_report_two_col_payment_method_sales:I = 0x7f0a0dec

.field public static final sales_report_v2:I = 0x7f0a0ded

.field public static final sales_report_v2_barrier_between_row_one_and_two:I = 0x7f0a0dee

.field public static final sales_report_v2_barrier_between_row_two_and_three:I = 0x7f0a0def

.field public static final sales_report_v2_bottom_barrier:I = 0x7f0a0df0

.field public static final sales_report_v2_bottom_line:I = 0x7f0a0df1

.field public static final sales_report_v2_line_between_row_one_and_two:I = 0x7f0a0df2

.field public static final sales_report_v2_line_between_row_two_and_three:I = 0x7f0a0df3

.field public static final sales_report_v2_middle_barrier:I = 0x7f0a0df4

.field public static final sales_report_v2_middle_line:I = 0x7f0a0df5

.field public static final sales_report_v2_overview_average_sale:I = 0x7f0a0df6

.field public static final sales_report_v2_overview_discount:I = 0x7f0a0df7

.field public static final sales_report_v2_overview_gross_sales:I = 0x7f0a0df8

.field public static final sales_report_v2_overview_net_sales:I = 0x7f0a0df9

.field public static final sales_report_v2_overview_number_of_sales:I = 0x7f0a0dfa

.field public static final sales_report_v2_overview_refunds:I = 0x7f0a0dfb

.field public static final send_button:I = 0x7f0a0e63


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
