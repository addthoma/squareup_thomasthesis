.class public final Lcom/squareup/salesreport/impl/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final amount_cell_label:I = 0x7f070066

.field public static final amount_cell_label_responsive_13_14:I = 0x7f070067

.field public static final amount_cell_value:I = 0x7f070068

.field public static final amount_cell_value_responsive_28_44:I = 0x7f070069

.field public static final popup_actions_view_button_gap:I = 0x7f07043b

.field public static final popup_actions_view_padding:I = 0x7f07043c

.field public static final popup_actions_view_width:I = 0x7f07043d

.field public static final sales_report_amount_count_toggle_height:I = 0x7f07048c

.field public static final sales_report_caret_size:I = 0x7f07048e

.field public static final sales_report_chart_bar_spacing:I = 0x7f07048f

.field public static final sales_report_chart_dot_size:I = 0x7f070490

.field public static final sales_report_chart_height:I = 0x7f070491

.field public static final sales_report_chart_top_padding:I = 0x7f070492

.field public static final sales_report_horizontal_padding:I = 0x7f070493

.field public static final sales_report_horizontal_padding_responsive_16_64:I = 0x7f070494

.field public static final sales_report_payment_method_bar_width:I = 0x7f070495

.field public static final sales_report_payment_method_text_height:I = 0x7f070496

.field public static final sales_report_payment_method_text_height_responsive_56_57:I = 0x7f070497

.field public static final sales_report_summary_details_row_left_padding:I = 0x7f070498

.field public static final sales_report_summary_details_subrow_left_padding:I = 0x7f070499

.field public static final sales_report_time_selector_edge_prominent:I = 0x7f07049a

.field public static final sales_report_time_selector_height:I = 0x7f07049b

.field public static final sales_report_time_selector_height_responsive_40_56:I = 0x7f07049c

.field public static final sales_report_top_bar_title_padding:I = 0x7f07049d

.field public static final sales_report_top_bar_title_padding_responsive_0_4:I = 0x7f07049e

.field public static final sales_report_vertical_padding:I = 0x7f07049f

.field public static final sales_report_vertical_padding_responsive_32_64:I = 0x7f0704a0


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
