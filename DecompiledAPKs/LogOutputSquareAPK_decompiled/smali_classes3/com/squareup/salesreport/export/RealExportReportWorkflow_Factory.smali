.class public final Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealExportReportWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/analytics/SalesReportAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTimeZone;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/analytics/SalesReportAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTimeZone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 46
    iput-object p7, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 47
    iput-object p8, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg7Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/analytics/SalesReportAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTimeZone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;"
        }
    .end annotation

    .line 61
    new-instance v9, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;Lcom/squareup/salesreport/analytics/SalesReportAnalytics;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Ljava/lang/String;)Lcom/squareup/salesreport/export/RealExportReportWorkflow;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;",
            "Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;",
            "Lcom/squareup/salesreport/analytics/SalesReportAnalytics;",
            "Lcom/squareup/time/CurrentTimeZone;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/salesreport/export/RealExportReportWorkflow;"
        }
    .end annotation

    .line 67
    new-instance v9, Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;-><init>(Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;Lcom/squareup/salesreport/analytics/SalesReportAnalytics;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Ljava/lang/String;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/export/RealExportReportWorkflow;
    .locals 9

    .line 52
    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;

    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;

    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/time/CurrentTimeZone;

    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/String;

    invoke-static/range {v1 .. v8}, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->newInstance(Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;Lcom/squareup/salesreport/analytics/SalesReportAnalytics;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Ljava/lang/String;)Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/salesreport/export/RealExportReportWorkflow_Factory;->get()Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    move-result-object v0

    return-object v0
.end method
