.class public final Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;
.super Ljava/lang/Object;
.source "PrintReportLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/salesreport/export/print/PrintReportScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPrintReportLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PrintReportLayoutRunner.kt\ncom/squareup/salesreport/export/print/PrintReportLayoutRunner\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,87:1\n1103#2,7:88\n*E\n*S KotlinDebug\n*F\n+ 1 PrintReportLayoutRunner.kt\ncom/squareup/salesreport/export/print/PrintReportLayoutRunner\n*L\n60#1,7:88\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001aB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0002H\u0002J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0002H\u0002J\u0018\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0019H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/salesreport/export/print/PrintReportScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "animator",
        "Lcom/squareup/widgets/SquareViewAnimator;",
        "kotlin.jvm.PlatformType",
        "itemSelectedRow",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "messageView",
        "Lcom/squareup/noho/NohoMessageView;",
        "printButton",
        "Lcom/squareup/noho/NohoButton;",
        "resetListeners",
        "",
        "showConfigureView",
        "screen",
        "showMessageView",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Binding",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final animator:Lcom/squareup/widgets/SquareViewAnimator;

.field private final itemSelectedRow:Lcom/squareup/noho/NohoCheckableRow;

.field private final messageView:Lcom/squareup/noho/NohoMessageView;

.field private final printButton:Lcom/squareup/noho/NohoButton;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->view:Landroid/view/View;

    .line 26
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 27
    iget-object p1, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/salesreport/impl/R$id;->print_report_animator:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 28
    check-cast p1, Lcom/squareup/widgets/SquareViewAnimator;

    sget v0, Lcom/squareup/salesreport/impl/R$id;->print_report_configure_view:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    iput-object p1, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 29
    iget-object p1, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/salesreport/impl/R$id;->print_items_selected:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.print_items_selected)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    iput-object p1, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->itemSelectedRow:Lcom/squareup/noho/NohoCheckableRow;

    .line 30
    iget-object p1, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/salesreport/impl/R$id;->print_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.print_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->printButton:Lcom/squareup/noho/NohoButton;

    .line 31
    iget-object p1, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/salesreport/impl/R$id;->print_report_message_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.print_report_message_view)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->messageView:Lcom/squareup/noho/NohoMessageView;

    return-void
.end method

.method private final resetListeners()V
    .locals 3

    .line 66
    iget-object v0, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->printButton:Lcom/squareup/noho/NohoButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->messageView:Lcom/squareup/noho/NohoMessageView;

    sget-object v1, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner$resetListeners$1;->INSTANCE:Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner$resetListeners$1;

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    const-string v2, "debounce {}"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method

.method private final showConfigureView(Lcom/squareup/salesreport/export/print/PrintReportScreen;)V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    sget v1, Lcom/squareup/salesreport/impl/R$id;->print_report_configure_view:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 60
    iget-object v0, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->printButton:Lcom/squareup/noho/NohoButton;

    check-cast v0, Landroid/view/View;

    .line 88
    new-instance v1, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner$showConfigureView$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner$showConfigureView$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/salesreport/export/print/PrintReportScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final showMessageView(Lcom/squareup/salesreport/export/print/PrintReportScreen;)V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    sget v1, Lcom/squareup/salesreport/impl/R$id;->print_report_message_view:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 73
    iget-object v0, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->messageView:Lcom/squareup/noho/NohoMessageView;

    .line 74
    new-instance v1, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner$showMessageView$1;

    invoke-direct {v1, p1}, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner$showMessageView$1;-><init>(Lcom/squareup/salesreport/export/print/PrintReportScreen;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    const-string v1, "debounceRunnable { screen.onClose() }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 77
    iget-object p1, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->messageView:Lcom/squareup/noho/NohoMessageView;

    sget v0, Lcom/squareup/salesreport/impl/R$string;->print_report_report_printed_title:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 78
    iget-object p1, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->messageView:Lcom/squareup/noho/NohoMessageView;

    sget v0, Lcom/squareup/vectoricons/R$drawable;->circle_check_96:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/salesreport/export/print/PrintReportScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 3

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->resetListeners()V

    .line 39
    iget-object p2, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->itemSelectedRow:Lcom/squareup/noho/NohoCheckableRow;

    .line 40
    invoke-virtual {p1}, Lcom/squareup/salesreport/export/print/PrintReportScreen;->getIncludeItems()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 41
    new-instance v0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner$showRendering$$inlined$run$lambda$1;

    invoke-direct {v0, p1}, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner$showRendering$$inlined$run$lambda$1;-><init>(Lcom/squareup/salesreport/export/print/PrintReportScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 46
    iget-object p2, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/salesreport/export/print/PrintReportScreen;->getOnClose()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 50
    iget-object p2, p0, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 47
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 48
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/salesreport/impl/R$string;->print_report_header:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 49
    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner$showRendering$2;

    invoke-direct {v2, p1}, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner$showRendering$2;-><init>(Lcom/squareup/salesreport/export/print/PrintReportScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 52
    invoke-virtual {p1}, Lcom/squareup/salesreport/export/print/PrintReportScreen;->getState()Lcom/squareup/salesreport/export/ExportReportState;

    move-result-object p2

    .line 53
    instance-of v0, p2, Lcom/squareup/salesreport/export/ExportReportState$PrintingReportStart;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->showConfigureView(Lcom/squareup/salesreport/export/print/PrintReportScreen;)V

    goto :goto_0

    .line 54
    :cond_0
    instance-of p2, p2, Lcom/squareup/salesreport/export/ExportReportState$PrintingReportComplete;

    if-eqz p2, :cond_1

    invoke-direct {p0, p1}, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->showMessageView(Lcom/squareup/salesreport/export/print/PrintReportScreen;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/salesreport/export/print/PrintReportScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/salesreport/export/print/PrintReportLayoutRunner;->showRendering(Lcom/squareup/salesreport/export/print/PrintReportScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
