.class public final Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository_Factory;
.super Ljava/lang/Object;
.source "PersistedBasicSalesReportConfigRepository_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/DefaultReportConfigProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/DefaultReportConfigProvider;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/DefaultReportConfigProvider;",
            ">;)",
            "Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository_Factory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;Lcom/squareup/salesreport/DefaultReportConfigProvider;)Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;

    invoke-direct {v0, p0, p1}, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;-><init>(Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;Lcom/squareup/salesreport/DefaultReportConfigProvider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;

    iget-object v1, p0, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/salesreport/DefaultReportConfigProvider;

    invoke-static {v0, v1}, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository_Factory;->newInstance(Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;Lcom/squareup/salesreport/DefaultReportConfigProvider;)Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository_Factory;->get()Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;

    move-result-object v0

    return-object v0
.end method
