.class public final Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;
.super Ljava/lang/Object;
.source "SalesReportPrintPayloadFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSalesReportPrintPayloadFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SalesReportPrintPayloadFactory.kt\ncom/squareup/salesreport/printing/SalesReportPrintPayloadFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,303:1\n1360#2:304\n1429#2,3:305\n1360#2:308\n1429#2,3:309\n1360#2:312\n1429#2,3:313\n1360#2:316\n1429#2,2:317\n1360#2:319\n1429#2,3:320\n1431#2:323\n1360#2:324\n1429#2,3:325\n*E\n*S KotlinDebug\n*F\n+ 1 SalesReportPrintPayloadFactory.kt\ncom/squareup/salesreport/printing/SalesReportPrintPayloadFactory\n*L\n130#1:304\n130#1,3:305\n148#1:308\n148#1,3:309\n171#1:312\n171#1,3:313\n201#1:316\n201#1,2:317\n201#1:319\n201#1,3:320\n201#1:323\n246#1:324\n246#1,3:325\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00d0\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0004\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001Bi\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\t\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016\u00a2\u0006\u0002\u0010\u0018J(\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u001f2\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020(2\u0008\u0010)\u001a\u0004\u0018\u00010\u001eJ\u0010\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-H\u0002J\u0010\u0010.\u001a\u00020/2\u0006\u00100\u001a\u000201H\u0002J\"\u00102\u001a\u0002032\u0006\u0010$\u001a\u00020\u001f2\u0006\u00104\u001a\u00020&2\u0008\u0010)\u001a\u0004\u0018\u00010\u001eH\u0002J\u001a\u00105\u001a\u0004\u0018\u0001062\u0006\u00107\u001a\u0002082\u0006\u0010\'\u001a\u00020(H\u0002J\u0010\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020<H\u0002J\u0010\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020@H\u0002J\u0012\u0010A\u001a\u0008\u0012\u0004\u0012\u00020C0B*\u000208H\u0002J\u001e\u0010D\u001a\u00020\u001e*\u00020&2\u0006\u0010\u0002\u001a\u00020\u00032\u0008\u0010)\u001a\u0004\u0018\u00010\u001eH\u0002R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0019\u001a\u0012\u0012\u0004\u0012\u00020\u001b0\u001aj\u0008\u0012\u0004\u0012\u00020\u001b`\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u001d\u001a\u00020\u001e*\u00020\u001f8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008 \u0010!\u00a8\u0006E"
    }
    d2 = {
        "Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "resources",
        "Landroid/content/res/Resources;",
        "printFormatter",
        "Lcom/squareup/salesreport/print/SalesPrintFormatter;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "numberFormatter",
        "",
        "percentageChangeFormatter",
        "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
        "nameOrTranslationTypeFormatter",
        "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "(Lcom/squareup/util/Res;Landroid/content/res/Resources;Lcom/squareup/salesreport/print/SalesPrintFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;)V",
        "topItemsComparator",
        "Ljava/util/Comparator;",
        "Lcom/squareup/customreport/data/SalesItem;",
        "Lkotlin/Comparator;",
        "reportedOnDateText",
        "",
        "Lcom/squareup/customreport/data/WithSalesReport;",
        "getReportedOnDateText",
        "(Lcom/squareup/customreport/data/WithSalesReport;)Ljava/lang/String;",
        "buildPayload",
        "Lcom/squareup/salesreport/print/SalesReportPayload;",
        "salesReport",
        "reportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "showItems",
        "",
        "deviceName",
        "createCategorySection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;",
        "topCategoriesReport",
        "Lcom/squareup/customreport/data/SalesTopCategoriesReport;",
        "createDiscountSection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;",
        "discountsReport",
        "Lcom/squareup/customreport/data/SalesDiscountsReport;",
        "createHeaderSection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;",
        "requestConfig",
        "createItemSection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;",
        "topItemsReport",
        "Lcom/squareup/customreport/data/SalesTopItemsReport;",
        "createPaymentsSection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;",
        "salesPaymentMethodsReport",
        "Lcom/squareup/customreport/data/SalesPaymentMethodsReport;",
        "createSalesSummarySection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;",
        "salesSummaryReport",
        "Lcom/squareup/customreport/data/WithSalesSummaryReport;",
        "createItemSectionRows",
        "",
        "Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;",
        "deviceSelectionDescription",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

.field private final numberFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageChangeFormatter:Lcom/squareup/salesreport/util/PercentageChangeFormatter;

.field private final printFormatter:Lcom/squareup/salesreport/print/SalesPrintFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final resources:Landroid/content/res/Resources;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final topItemsComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/customreport/data/SalesItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Landroid/content/res/Resources;Lcom/squareup/salesreport/print/SalesPrintFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/salesreport/print/SalesPrintFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "numberFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageChangeFormatter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameOrTranslationTypeFormatter"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->resources:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->printFormatter:Lcom/squareup/salesreport/print/SalesPrintFormatter;

    iput-object p4, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p5, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->numberFormatter:Lcom/squareup/text/Formatter;

    iput-object p6, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->percentageChangeFormatter:Lcom/squareup/salesreport/util/PercentageChangeFormatter;

    iput-object p7, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    iput-object p8, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p9, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    iput-object p10, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->localeProvider:Ljavax/inject/Provider;

    .line 301
    new-instance p1, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory$$special$$inlined$compareBy$1;

    invoke-direct {p1, p0}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory$$special$$inlined$compareBy$1;-><init>(Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;)V

    check-cast p1, Ljava/util/Comparator;

    iput-object p1, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->topItemsComparator:Ljava/util/Comparator;

    return-void
.end method

.method public static final synthetic access$getNameOrTranslationTypeFormatter$p(Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;)Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    return-object p0
.end method

.method private final createCategorySection(Lcom/squareup/customreport/data/SalesTopCategoriesReport;)Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;
    .locals 7

    .line 168
    instance-of v0, p1, Lcom/squareup/customreport/data/WithSalesTopCategoriesReport;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/customreport/data/WithSalesTopCategoriesReport;

    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesTopCategoriesReport;->getTopCategories()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 170
    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->topItemsComparator:Ljava/util/Comparator;

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 312
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 313
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 314
    check-cast v1, Lcom/squareup/customreport/data/SalesItem;

    .line 172
    iget-object v2, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->printFormatter:Lcom/squareup/salesreport/print/SalesPrintFormatter;

    .line 173
    iget-object v3, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesItem;->getName()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object v3

    .line 174
    iget-object v4, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->numberFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesItem;->getCount()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 172
    invoke-virtual {v2, v3, v4}, Lcom/squareup/salesreport/print/SalesPrintFormatter;->nameAndQuantity(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 176
    new-instance v3, Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection$CategorySectionRow;

    iget-object v4, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesItem;->getGrossSales()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v4, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v2, v1}, Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection$CategorySectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 315
    :cond_0
    check-cast v0, Ljava/util/List;

    goto :goto_1

    .line 178
    :cond_1
    sget-object v0, Lcom/squareup/customreport/data/NoSalesTopCategoriesReport;->INSTANCE:Lcom/squareup/customreport/data/NoSalesTopCategoriesReport;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    const-string p1, "emptyList()"

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    :goto_1
    new-instance p1, Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;

    .line 182
    iget-object v1, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/salesreport/impl/R$string;->sales_report_print_category_sales_uppercase:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 181
    invoke-direct {p1, v1, v0}, Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object p1

    .line 178
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final createDiscountSection(Lcom/squareup/customreport/data/SalesDiscountsReport;)Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;
    .locals 7

    .line 148
    instance-of v0, p1, Lcom/squareup/customreport/data/WithSalesDiscountsReport;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/customreport/data/WithSalesDiscountsReport;

    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesDiscountsReport;->getDiscounts()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 308
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 309
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 310
    check-cast v1, Lcom/squareup/customreport/data/SalesDiscount;

    .line 149
    iget-object v2, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->printFormatter:Lcom/squareup/salesreport/print/SalesPrintFormatter;

    .line 150
    iget-object v3, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesDiscount;->getName()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object v3

    .line 151
    iget-object v4, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->numberFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesDiscount;->getCount()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 149
    invoke-virtual {v2, v3, v4}, Lcom/squareup/salesreport/print/SalesPrintFormatter;->nameAndQuantity(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 153
    new-instance v3, Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection$DiscountSectionRow;

    iget-object v4, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesDiscount;->getDiscountMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v4, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v2, v1}, Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection$DiscountSectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 311
    :cond_0
    check-cast v0, Ljava/util/List;

    goto :goto_1

    .line 155
    :cond_1
    sget-object v0, Lcom/squareup/customreport/data/NoSalesDiscountsReport;->INSTANCE:Lcom/squareup/customreport/data/NoSalesDiscountsReport;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    const-string p1, "emptyList()"

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    :goto_1
    new-instance p1, Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;

    .line 159
    iget-object v1, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/salesreport/impl/R$string;->sales_report_print_discounts_applied_uppercase:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 158
    invoke-direct {p1, v1, v0}, Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object p1

    .line 155
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final createHeaderSection(Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Ljava/lang/String;)Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;
    .locals 7

    .line 92
    new-instance v6, Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;

    .line 93
    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/salesreport/impl/R$string;->sales_report_print_title_uppercase:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 94
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->getReportedOnDateText(Lcom/squareup/customreport/data/WithSalesReport;)Ljava/lang/String;

    move-result-object v2

    .line 95
    iget-object p1, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->printFormatter:Lcom/squareup/salesreport/print/SalesPrintFormatter;

    .line 96
    invoke-static {p2}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->getStartDateTime(Lcom/squareup/customreport/data/ReportConfig;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/utilities/threeten/compat/LocalDateTimesKt;->toDate(Lorg/threeten/bp/LocalDateTime;)Ljava/util/Date;

    move-result-object v0

    .line 97
    invoke-static {p2}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->getEndDateTime(Lcom/squareup/customreport/data/ReportConfig;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/utilities/threeten/compat/LocalDateTimesKt;->toDate(Lorg/threeten/bp/LocalDateTime;)Ljava/util/Date;

    move-result-object v3

    .line 95
    invoke-virtual {p1, v0, v3}, Lcom/squareup/salesreport/print/SalesPrintFormatter;->reportDateRange(Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 99
    iget-object p1, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    invoke-static {p2, p1}, Lcom/squareup/salesreport/util/ReportConfigsKt;->employeeSelectionDescription(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v4

    .line 100
    iget-object p1, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->deviceSelectionDescription(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, v6

    .line 92
    invoke-direct/range {v0 .. v5}, Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v6
.end method

.method private final createItemSection(Lcom/squareup/customreport/data/SalesTopItemsReport;Z)Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;
    .locals 2

    if-eqz p2, :cond_0

    .line 191
    new-instance p2, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;

    .line 192
    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/salesreport/impl/R$string;->sales_report_print_item_sales_uppercase:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 193
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->createItemSectionRows(Lcom/squareup/customreport/data/SalesTopItemsReport;)Ljava/util/List;

    move-result-object p1

    .line 191
    invoke-direct {p2, v0, p1}, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;-><init>(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    return-object p2
.end method

.method private final createItemSectionRows(Lcom/squareup/customreport/data/SalesTopItemsReport;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/SalesTopItemsReport;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;",
            ">;"
        }
    .end annotation

    .line 199
    instance-of v0, p1, Lcom/squareup/customreport/data/WithSalesTopItemsReport;

    const-string v1, "emptyList()"

    if-eqz v0, :cond_4

    check-cast p1, Lcom/squareup/customreport/data/WithSalesTopItemsReport;

    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesTopItemsReport;->getTopItems()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 200
    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->topItemsComparator:Ljava/util/Comparator;

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 316
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 317
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 318
    check-cast v3, Lcom/squareup/customreport/data/SalesItem;

    .line 202
    iget-object v4, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->printFormatter:Lcom/squareup/salesreport/print/SalesPrintFormatter;

    .line 203
    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesItem;->getSubItems()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 204
    iget-object v5, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesItem;->getName()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    move-result-object v6

    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesItem;->getSubItems()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/customreport/data/SalesItem;

    invoke-virtual {v7}, Lcom/squareup/customreport/data/SalesItem;->getName()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatNameAndVariation(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 206
    :cond_0
    iget-object v5, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesItem;->getName()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object v5

    .line 208
    :goto_1
    iget-object v6, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->numberFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesItem;->getCount()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 202
    invoke-virtual {v4, v5, v6}, Lcom/squareup/salesreport/print/SalesPrintFormatter;->nameAndQuantity(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 212
    iget-object v5, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesItem;->getGrossSales()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 213
    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesItem;->getSubItems()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x2

    if-ge v6, v7, :cond_1

    .line 214
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    .line 216
    :cond_1
    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesItem;->getSubItems()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 319
    new-instance v6, Ljava/util/ArrayList;

    invoke-static {v3, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v6, Ljava/util/Collection;

    .line 320
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 321
    check-cast v7, Lcom/squareup/customreport/data/SalesItem;

    .line 217
    new-instance v8, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;

    .line 218
    iget-object v9, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->printFormatter:Lcom/squareup/salesreport/print/SalesPrintFormatter;

    .line 219
    iget-object v10, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    invoke-virtual {v7}, Lcom/squareup/customreport/data/SalesItem;->getName()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object v10

    .line 220
    iget-object v11, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->numberFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v7}, Lcom/squareup/customreport/data/SalesItem;->getCount()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-interface {v11, v12}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    .line 218
    invoke-virtual {v9, v10, v11}, Lcom/squareup/salesreport/print/SalesPrintFormatter;->nameAndQuantity(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 222
    iget-object v10, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v7}, Lcom/squareup/customreport/data/SalesItem;->getGrossSales()Lcom/squareup/protos/common/Money;

    move-result-object v7

    invoke-interface {v10, v7}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v10, 0x0

    .line 217
    invoke-direct {v8, v9, v7, v10}, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 224
    invoke-interface {v6, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 322
    :cond_2
    move-object v3, v6

    check-cast v3, Ljava/util/List;

    .line 210
    :goto_3
    new-instance v6, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;

    invoke-direct {v6, v4, v5, v3}, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 227
    invoke-interface {v0, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 323
    :cond_3
    check-cast v0, Ljava/util/List;

    goto :goto_4

    .line 229
    :cond_4
    instance-of p1, p1, Lcom/squareup/customreport/data/NoSalesTopItemsReport;

    if-eqz p1, :cond_5

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_4
    return-object v0

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final createPaymentsSection(Lcom/squareup/customreport/data/SalesPaymentMethodsReport;)Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;
    .locals 7

    .line 236
    sget-object v0, Lcom/squareup/customreport/data/NoSalesPaymentMethodsReport;->INSTANCE:Lcom/squareup/customreport/data/NoSalesPaymentMethodsReport;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    const-string v0, "emptyList()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 237
    :cond_0
    instance-of v0, p1, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;

    if-eqz v0, :cond_3

    .line 238
    new-instance v0, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;

    .line 239
    iget-object v1, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_total_collected:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 240
    iget-object v2, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 241
    check-cast p1, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;

    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->getTotalCollectedMoney()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 240
    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 242
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 243
    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 238
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 237
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 246
    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->getPaymentMethods()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 324
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 325
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 326
    check-cast v3, Lcom/squareup/customreport/data/SalesPaymentMethod;

    .line 247
    new-instance v4, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;

    .line 248
    iget-object v5, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesPaymentMethod;->getPaymentMethod()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    move-result-object v6

    invoke-static {v6}, Lcom/squareup/salesreport/util/PaymentMethodsKt;->getTitle(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;)I

    move-result v6

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 249
    iget-object v6, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesPaymentMethod;->getNetCollected()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v6, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 250
    sget-object v6, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 247
    invoke-direct {v4, v5, v3, v6}, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 251
    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 327
    :cond_1
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 245
    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 255
    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->getFees()Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 256
    new-instance v1, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;

    .line 257
    iget-object v2, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_fees:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 258
    iget-object v3, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->getFees()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 259
    sget-object v4, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 256
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/marketfont/MarketFont$Weight;)V

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    .line 254
    :goto_1
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOfNotNull(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 253
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 266
    new-instance v1, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;

    .line 267
    iget-object v2, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_net_total:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 268
    iget-object v3, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->getNetTotalMoney()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v3, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 269
    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 266
    invoke-direct {v1, v2, p1, v3}, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 265
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 273
    :goto_2
    new-instance v0, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;

    .line 274
    iget-object v1, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/salesreport/impl/R$string;->sales_report_print_payments_uppercase:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 273
    invoke-direct {v0, v1, p1}, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object v0

    .line 265
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final createSalesSummarySection(Lcom/squareup/customreport/data/WithSalesSummaryReport;)Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;
    .locals 5

    .line 125
    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 126
    iget-object v1, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->percentageChangeFormatter:Lcom/squareup/salesreport/util/PercentageChangeFormatter;

    .line 127
    iget-object v2, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 128
    iget-object v3, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    .line 124
    invoke-static {p1, v0, v1, v2, v3}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->salesDetailsRows(Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 304
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 305
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 306
    check-cast v1, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    .line 131
    new-instance v2, Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection$SalesSectionRow;

    .line 132
    invoke-virtual {v1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;->getName()Lcom/squareup/util/ViewString;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->resources:Landroid/content/res/Resources;

    invoke-virtual {v3, v4}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 133
    invoke-virtual {v1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;->getMoney()Ljava/lang/String;

    move-result-object v4

    .line 134
    invoke-virtual {v1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;->getBold()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    goto :goto_1

    :cond_0
    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 131
    :goto_1
    invoke-direct {v2, v3, v4, v1}, Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection$SalesSectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 135
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 307
    :cond_1
    check-cast v0, Ljava/util/List;

    .line 138
    new-instance p1, Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;

    .line 139
    iget-object v1, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/salesreport/impl/R$string;->sales_report_print_sales_uppercase:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 138
    invoke-direct {p1, v1, v0}, Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object p1
.end method

.method private final deviceSelectionDescription(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 289
    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getThisDeviceOnly()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 290
    move-object p1, p3

    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-eqz p1, :cond_3

    .line 291
    sget p1, Lcom/squareup/salesreport/impl/R$string;->customize_report_device_filter:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    goto :goto_2

    .line 296
    :cond_2
    sget p1, Lcom/squareup/salesreport/impl/R$string;->customize_report_all_devices:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    :cond_3
    :goto_2
    return-object p3
.end method

.method private final getReportedOnDateText(Lcom/squareup/customreport/data/WithSalesReport;)Ljava/lang/String;
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/salesreport/impl/R$string;->sales_report_print_report_date_printout_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    .line 111
    iget-object v1, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/format/DateTimeFormatter;->withLocale(Ljava/util/Locale;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    .line 112
    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesReport;->getReportDate()Lorg/threeten/bp/ZonedDateTime;

    move-result-object p1

    check-cast p1, Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/format/DateTimeFormatter;->format(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/String;

    move-result-object p1

    .line 113
    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->res:Lcom/squareup/util/Res;

    .line 114
    sget v1, Lcom/squareup/salesreport/impl/R$string;->sales_report_print_report_date_printout_pattern:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 115
    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "formatted_date_time"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 116
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 117
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final buildPayload(Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;ZLjava/lang/String;)Lcom/squareup/salesreport/print/SalesReportPayload;
    .locals 8

    const-string v0, "salesReport"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reportConfig"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-direct {p0, p1, p2, p4}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->createHeaderSection(Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Ljava/lang/String;)Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;

    move-result-object v2

    .line 76
    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesReport;->getSalesSummaryReport()Lcom/squareup/customreport/data/WithSalesSummaryReport;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->createSalesSummarySection(Lcom/squareup/customreport/data/WithSalesSummaryReport;)Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;

    move-result-object v3

    .line 77
    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesReport;->getPaymentMethodsReport()Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->createPaymentsSection(Lcom/squareup/customreport/data/SalesPaymentMethodsReport;)Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;

    move-result-object v4

    .line 78
    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesReport;->getDiscountsReport()Lcom/squareup/customreport/data/SalesDiscountsReport;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->createDiscountSection(Lcom/squareup/customreport/data/SalesDiscountsReport;)Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;

    move-result-object v5

    .line 79
    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesReport;->getTopCategoriesReport()Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->createCategorySection(Lcom/squareup/customreport/data/SalesTopCategoriesReport;)Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;

    move-result-object v6

    .line 80
    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesReport;->getTopItemsReport()Lcom/squareup/customreport/data/SalesTopItemsReport;

    move-result-object p1

    invoke-direct {p0, p1, p3}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->createItemSection(Lcom/squareup/customreport/data/SalesTopItemsReport;Z)Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;

    move-result-object v7

    .line 81
    new-instance p1, Lcom/squareup/salesreport/print/SalesReportPayload;

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/salesreport/print/SalesReportPayload;-><init>(Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;)V

    return-object p1
.end method
