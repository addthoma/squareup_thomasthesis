.class public final Lcom/squareup/depositschedule/DepositScheduleSettings$State;
.super Ljava/lang/Object;
.source "DepositScheduleSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/depositschedule/DepositScheduleSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDepositScheduleSettings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DepositScheduleSettings.kt\ncom/squareup/depositschedule/DepositScheduleSettings$State\n*L\n1#1,153:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001BG\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u001d\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\tH\u00c6\u0003J\t\u0010!\u001a\u00020\u000bH\u00c6\u0003J\t\u0010\"\u001a\u00020\rH\u00c6\u0003JK\u0010#\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\rH\u00c6\u0001J\u0013\u0010$\u001a\u00020\u00172\u0008\u0010%\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010&\u001a\u00020\rH\u00d6\u0001J\u0008\u0010\'\u001a\u00020\u0005H\u0002J\t\u0010(\u001a\u00020)H\u00d6\u0001R\u0010\u0010\u000c\u001a\u00020\r8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00108G\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0012R\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0013\u001a\u00020\u00148G\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0015R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0008\u001a\u0004\u0018\u00010\t8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0016\u001a\u00020\u00178G\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0018R\u0011\u0010\u0019\u001a\u00020\u00178G\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u0018R\u0011\u0010\u001a\u001a\u00020\u001b8G\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u001cR\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
        "",
        "depositScheduleState",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;",
        "weeklyDepositSchedule",
        "Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;",
        "defaultCutoffTime",
        "Lcom/squareup/protos/common/time/LocalTime;",
        "failureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "depositSpeed",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;",
        "dayOfWeek",
        "",
        "(Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;I)V",
        "dayOfWeekDepositSettings",
        "",
        "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
        "()Ljava/util/List;",
        "depositInterval",
        "Lcom/squareup/protos/client/depositsettings/DepositInterval;",
        "()Lcom/squareup/protos/client/depositsettings/DepositInterval;",
        "pausedSettlement",
        "",
        "()Z",
        "settleIfOptional",
        "timeZone",
        "Ljava/util/TimeZone;",
        "()Ljava/util/TimeZone;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "requireWeeklyDepositSchedule",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final dayOfWeek:I

.field public final defaultCutoffTime:Lcom/squareup/protos/common/time/LocalTime;

.field public final depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

.field public final depositSpeed:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

.field public final failureMessage:Lcom/squareup/receiving/FailureMessage;

.field public final weeklyDepositSchedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;


# direct methods
.method public constructor <init>()V
    .locals 9

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3f

    const/4 v8, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;-><init>(Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;I)V
    .locals 1

    const-string v0, "depositScheduleState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositSpeed"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    iput-object p2, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->weeklyDepositSchedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iput-object p3, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->defaultCutoffTime:Lcom/squareup/protos/common/time/LocalTime;

    iput-object p4, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    iput-object p5, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositSpeed:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    iput p6, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeek:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    .line 80
    sget-object p1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->LOADING:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    const/4 v0, 0x0

    if-eqz p8, :cond_1

    .line 81
    move-object p2, v0

    check-cast p2, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    .line 82
    move-object p3, v0

    check-cast p3, Lcom/squareup/protos/common/time/LocalTime;

    :cond_2
    move-object v1, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    .line 83
    move-object p4, v0

    check-cast p4, Lcom/squareup/receiving/FailureMessage;

    :cond_3
    move-object v0, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    .line 84
    sget-object p5, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->NO_SELECTION:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    const/4 p6, -0x1

    const/4 v3, -0x1

    goto :goto_0

    :cond_5
    move v3, p6

    :goto_0
    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v1

    move-object p6, v0

    move-object p7, v2

    move p8, v3

    .line 85
    invoke-direct/range {p2 .. p8}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;-><init>(Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;I)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILjava/lang/Object;)Lcom/squareup/depositschedule/DepositScheduleSettings$State;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->weeklyDepositSchedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->defaultCutoffTime:Lcom/squareup/protos/common/time/LocalTime;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositSpeed:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget p6, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeek:I

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->copy(Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;I)Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object p0

    return-object p0
.end method

.method private final requireWeeklyDepositSchedule()Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->weeklyDepositSchedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expect weeklyDepositSchedule to be present."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;
    .locals 1

    iget-object v0, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->weeklyDepositSchedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/common/time/LocalTime;
    .locals 1

    iget-object v0, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->defaultCutoffTime:Lcom/squareup/protos/common/time/LocalTime;

    return-object v0
.end method

.method public final component4()Lcom/squareup/receiving/FailureMessage;
    .locals 1

    iget-object v0, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    return-object v0
.end method

.method public final component5()Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;
    .locals 1

    iget-object v0, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositSpeed:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    return-object v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeek:I

    return v0
.end method

.method public final copy(Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;I)Lcom/squareup/depositschedule/DepositScheduleSettings$State;
    .locals 8

    const-string v0, "depositScheduleState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositSpeed"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;-><init>(Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;I)V

    return-object v0
.end method

.method public final dayOfWeekDepositSettings()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
            ">;"
        }
    .end annotation

    .line 93
    invoke-direct {p0}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->requireWeeklyDepositSchedule()Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->day_of_week_deposit_settings:Ljava/util/List;

    const-string v1, "requireWeeklyDepositSche\u2026_of_week_deposit_settings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final depositInterval()Lcom/squareup/protos/client/depositsettings/DepositInterval;
    .locals 2

    .line 89
    invoke-virtual {p0}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeekDepositSettings()Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeek:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    iget-object v0, v0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    const-string v1, "dayOfWeekDepositSettings\u2026yOfWeek].deposit_interval"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    iget-object v0, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    iget-object v1, p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->weeklyDepositSchedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iget-object v1, p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->weeklyDepositSchedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->defaultCutoffTime:Lcom/squareup/protos/common/time/LocalTime;

    iget-object v1, p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->defaultCutoffTime:Lcom/squareup/protos/common/time/LocalTime;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    iget-object v1, p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositSpeed:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    iget-object v1, p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositSpeed:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeek:I

    iget p1, p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeek:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->weeklyDepositSchedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->defaultCutoffTime:Lcom/squareup/protos/common/time/LocalTime;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositSpeed:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeek:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final pausedSettlement()Z
    .locals 1

    .line 105
    invoke-direct {p0}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->requireWeeklyDepositSchedule()Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->paused_settlement:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final settleIfOptional()Z
    .locals 1

    .line 101
    invoke-direct {p0}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->requireWeeklyDepositSchedule()Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->settle_if_optional:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final timeZone()Ljava/util/TimeZone;
    .locals 2

    .line 97
    invoke-direct {p0}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->requireWeeklyDepositSchedule()Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->timezone:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    const-string v1, "getTimeZone(requireWeekl\u2026positSchedule().timezone)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "State(depositScheduleState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", weeklyDepositSchedule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->weeklyDepositSchedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", defaultCutoffTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->defaultCutoffTime:Lcom/squareup/protos/common/time/LocalTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", failureMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", depositSpeed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositSpeed:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", dayOfWeek="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeek:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
