.class final Lcom/squareup/communications/service/RealRemoteMessagesStore$messages$1;
.super Ljava/lang/Object;
.source "RealRemoteMessagesStore.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/communications/service/RealRemoteMessagesStore;->messages(Ljava/lang/String;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u00030\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "",
        "Lcom/squareup/communications/Message;",
        "it",
        "Lcom/squareup/communications/service/sync/SyncEvent;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $placementId:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/communications/service/RealRemoteMessagesStore;


# direct methods
.method constructor <init>(Lcom/squareup/communications/service/RealRemoteMessagesStore;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/communications/service/RealRemoteMessagesStore$messages$1;->this$0:Lcom/squareup/communications/service/RealRemoteMessagesStore;

    iput-object p2, p0, Lcom/squareup/communications/service/RealRemoteMessagesStore$messages$1;->$placementId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/communications/service/sync/SyncEvent;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/communications/service/sync/SyncEvent;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Ljava/util/List<",
            "Lcom/squareup/communications/Message;",
            ">;>;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object p1, p0, Lcom/squareup/communications/service/RealRemoteMessagesStore$messages$1;->this$0:Lcom/squareup/communications/service/RealRemoteMessagesStore;

    iget-object v0, p0, Lcom/squareup/communications/service/RealRemoteMessagesStore$messages$1;->$placementId:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/squareup/communications/service/RealRemoteMessagesStore;->access$requestMessages(Lcom/squareup/communications/service/RealRemoteMessagesStore;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/communications/service/sync/SyncEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/communications/service/RealRemoteMessagesStore$messages$1;->apply(Lcom/squareup/communications/service/sync/SyncEvent;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
