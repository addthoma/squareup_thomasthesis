.class public final Lcom/squareup/communications/service/sync/MessagesSyncNotifier;
.super Ljava/lang/Object;
.source "MessagesSyncNotifier.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/communications/service/sync/MessagesSyncNotifier$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMessagesSyncNotifier.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MessagesSyncNotifier.kt\ncom/squareup/communications/service/sync/MessagesSyncNotifier\n+ 2 PushMessageDelegate.kt\ncom/squareup/pushmessages/PushMessageDelegateKt\n*L\n1#1,80:1\n16#2:81\n*E\n*S KotlinDebug\n*F\n+ 1 MessagesSyncNotifier.kt\ncom/squareup/communications/service/sync/MessagesSyncNotifier\n*L\n66#1:81\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u000c2\u00020\u0001:\u0001\u000cB\u0019\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008R\u0016\u0010\u0007\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/communications/service/sync/MessagesSyncNotifier;",
        "",
        "pushMessageDelegate",
        "Lcom/squareup/pushmessages/PushMessageDelegate;",
        "scheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/pushmessages/PushMessageDelegate;Lio/reactivex/Scheduler;)V",
        "pollEvents",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/communications/service/sync/SyncEvent;",
        "pushEvents",
        "events",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/communications/service/sync/MessagesSyncNotifier$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final POLLING_INTERVAL_SECONDS:J = 0x3cL


# instance fields
.field private final pollEvents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/communications/service/sync/SyncEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final pushEvents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/communications/service/sync/SyncEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final scheduler:Lio/reactivex/Scheduler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/communications/service/sync/MessagesSyncNotifier$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/communications/service/sync/MessagesSyncNotifier$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/communications/service/sync/MessagesSyncNotifier;->Companion:Lcom/squareup/communications/service/sync/MessagesSyncNotifier$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/pushmessages/PushMessageDelegate;Lio/reactivex/Scheduler;)V
    .locals 2
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pushMessageDelegate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/communications/service/sync/MessagesSyncNotifier;->scheduler:Lio/reactivex/Scheduler;

    .line 81
    const-class p2, Lcom/squareup/pushmessages/PushMessage$MessagesSync;

    invoke-interface {p1, p2}, Lcom/squareup/pushmessages/PushMessageDelegate;->observe(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    .line 67
    sget-object p2, Lcom/squareup/communications/service/sync/MessagesSyncNotifier$pushEvents$1;->INSTANCE:Lcom/squareup/communications/service/sync/MessagesSyncNotifier$pushEvents$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "pushMessageDelegate\n    \u2026>()\n        .map { Push }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/communications/service/sync/MessagesSyncNotifier;->pushEvents:Lio/reactivex/Observable;

    .line 73
    sget-object p1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object p2, p0, Lcom/squareup/communications/service/sync/MessagesSyncNotifier;->scheduler:Lio/reactivex/Scheduler;

    const-wide/16 v0, 0x3c

    invoke-static {v0, v1, p1, p2}, Lio/reactivex/Observable;->interval(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 74
    sget-object p2, Lcom/squareup/communications/service/sync/MessagesSyncNotifier$pollEvents$1;->INSTANCE:Lcom/squareup/communications/service/sync/MessagesSyncNotifier$pollEvents$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable\n      .interv\u2026uler)\n      .map { Poll }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/communications/service/sync/MessagesSyncNotifier;->pollEvents:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getPollEvents$p(Lcom/squareup/communications/service/sync/MessagesSyncNotifier;)Lio/reactivex/Observable;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/communications/service/sync/MessagesSyncNotifier;->pollEvents:Lio/reactivex/Observable;

    return-object p0
.end method


# virtual methods
.method public final events()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/communications/service/sync/SyncEvent;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/communications/service/sync/MessagesSyncNotifier;->pushEvents:Lio/reactivex/Observable;

    .line 49
    new-instance v1, Lcom/squareup/communications/service/sync/MessagesSyncNotifier$events$1;

    invoke-direct {v1, p0}, Lcom/squareup/communications/service/sync/MessagesSyncNotifier$events$1;-><init>(Lcom/squareup/communications/service/sync/MessagesSyncNotifier;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 57
    sget-object v1, Lcom/squareup/communications/service/sync/SyncEvent$Manual;->INSTANCE:Lcom/squareup/communications/service/sync/SyncEvent$Manual;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "pushEvents\n        .publ\u2026       .startWith(Manual)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
