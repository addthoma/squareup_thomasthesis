.class public final Lcom/squareup/communications/RealMessagesRepository;
.super Ljava/lang/Object;
.source "RealMessagesRepository.kt"

# interfaces
.implements Lcom/squareup/communications/MessagesRepository;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\tH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/communications/RealMessagesRepository;",
        "Lcom/squareup/communications/MessagesRepository;",
        "remoteMessagesStore",
        "Lcom/squareup/communications/service/RemoteMessagesStore;",
        "(Lcom/squareup/communications/service/RemoteMessagesStore;)V",
        "messages",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/communications/MessagesRepository$Result;",
        "placementId",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final remoteMessagesStore:Lcom/squareup/communications/service/RemoteMessagesStore;


# direct methods
.method public constructor <init>(Lcom/squareup/communications/service/RemoteMessagesStore;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "remoteMessagesStore"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/communications/RealMessagesRepository;->remoteMessagesStore:Lcom/squareup/communications/service/RemoteMessagesStore;

    return-void
.end method


# virtual methods
.method public messages(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/communications/MessagesRepository$Result;",
            ">;"
        }
    .end annotation

    const-string v0, "placementId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lcom/squareup/communications/RealMessagesRepository;->remoteMessagesStore:Lcom/squareup/communications/service/RemoteMessagesStore;

    .line 20
    invoke-interface {v0, p1}, Lcom/squareup/communications/service/RemoteMessagesStore;->messages(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    .line 21
    sget-object v0, Lcom/squareup/communications/RealMessagesRepository$messages$1;->INSTANCE:Lcom/squareup/communications/RealMessagesRepository$messages$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "remoteMessagesStore\n    \u2026())\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
