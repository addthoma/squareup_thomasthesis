.class final Lcom/squareup/mailorder/OrderReactor$sendOrder$1;
.super Ljava/lang/Object;
.source "OrderReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/OrderReactor;->sendOrder(Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "response",
        "Lcom/squareup/mailorder/OrderServiceHelper$OrderResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;


# direct methods
.method constructor <init>(Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/OrderReactor$sendOrder$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/mailorder/OrderServiceHelper$OrderResult;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderServiceHelper$OrderResult;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            ">;"
        }
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 443
    instance-of v0, p1, Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Success;

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$OrderSuccess;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$sendOrder$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getItemToken()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$sendOrder$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    invoke-virtual {v2}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/mailorder/OrderReactor$OrderState$OrderSuccess;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;)V

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    .line 444
    :cond_0
    instance-of v0, p1, Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 445
    new-instance v1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$OrderError;

    .line 446
    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$sendOrder$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    invoke-virtual {v2}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getItemToken()Ljava/lang/String;

    move-result-object v2

    .line 447
    iget-object v3, p0, Lcom/squareup/mailorder/OrderReactor$sendOrder$1;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    invoke-virtual {v3}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v3

    .line 448
    check-cast p1, Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 449
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper$OrderResult$Failure;->getMessage()Ljava/lang/String;

    move-result-object p1

    .line 445
    invoke-direct {v1, v2, v3, v4, p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$OrderError;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    move-object p1, v0

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/mailorder/OrderServiceHelper$OrderResult;

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/OrderReactor$sendOrder$1;->apply(Lcom/squareup/mailorder/OrderServiceHelper$OrderResult;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
