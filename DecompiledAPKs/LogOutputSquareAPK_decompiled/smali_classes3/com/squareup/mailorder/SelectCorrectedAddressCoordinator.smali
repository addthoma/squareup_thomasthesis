.class public final Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SelectCorrectedAddressCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;,
        Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectCorrectedAddressCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectCorrectedAddressCoordinator.kt\ncom/squareup/mailorder/SelectCorrectedAddressCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,174:1\n1103#2,7:175\n*E\n*S KotlinDebug\n*F\n+ 1 SelectCorrectedAddressCoordinator.kt\ncom/squareup/mailorder/SelectCorrectedAddressCoordinator\n*L\n130#1,7:175\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0002+,B5\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0018\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0010\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u000e\u0010\u001e\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u001dJ(\u0010\u001f\u001a\u00020\u00182\u0016\u0010 \u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010!\u001a\u00020\u0016H\u0002J(\u0010\"\u001a\u00020\u00182\u0016\u0010 \u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0008\u0010#\u001a\u00020\u0016H\u0002J(\u0010$\u001a\u00020\u00182\u0016\u0010 \u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0010\u0010%\u001a\u00020\u00182\u0006\u0010&\u001a\u00020\'H\u0002J\u0010\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020\u0005H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00160\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
        "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent;",
        "Lcom/squareup/mailorder/SelectCorrectedAddressScreen;",
        "configuration",
        "Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;",
        "spinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "(Lio/reactivex/Observable;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;Lcom/squareup/register/widgets/GlassSpinner;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "addressOriginal",
        "Lcom/squareup/mailorder/SelectableAddressLayout;",
        "addressSuggested",
        "orderButton",
        "Lcom/squareup/noho/NohoButton;",
        "selectableGroup",
        "Lcom/squareup/noho/RadioGroup;",
        "",
        "applyConfigurations",
        "",
        "res",
        "Landroid/content/res/Resources;",
        "attach",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "observeClicks",
        "screen",
        "selectedId",
        "populateLayout",
        "selectedItem",
        "setupNavigation",
        "showSelectAddressState",
        "selectAddressState",
        "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;",
        "spinnerData",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "data",
        "Configuration",
        "Factory",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private addressOriginal:Lcom/squareup/mailorder/SelectableAddressLayout;

.field private addressSuggested:Lcom/squareup/mailorder/SelectableAddressLayout;

.field private final configuration:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;

.field private orderButton:Lcom/squareup/noho/NohoButton;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private selectableGroup:Lcom/squareup/noho/RadioGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/noho/RadioGroup<",
            "Lcom/squareup/mailorder/SelectableAddressLayout;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final spinner:Lcom/squareup/register/widgets/GlassSpinner;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent;",
            ">;>;",
            "Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ")V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->configuration:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;

    iput-object p3, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;Lcom/squareup/register/widgets/GlassSpinner;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-void
.end method

.method public static final synthetic access$getAddressOriginal$p(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;)Lcom/squareup/mailorder/SelectableAddressLayout;
    .locals 1

    .line 33
    iget-object p0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->addressOriginal:Lcom/squareup/mailorder/SelectableAddressLayout;

    if-nez p0, :cond_0

    const-string v0, "addressOriginal"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getAddressSuggested$p(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;)Lcom/squareup/mailorder/SelectableAddressLayout;
    .locals 1

    .line 33
    iget-object p0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->addressSuggested:Lcom/squareup/mailorder/SelectableAddressLayout;

    if-nez p0, :cond_0

    const-string v0, "addressSuggested"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$observeClicks(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;Lcom/squareup/workflow/legacy/Screen;I)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->observeClicks(Lcom/squareup/workflow/legacy/Screen;I)V

    return-void
.end method

.method public static final synthetic access$populateLayout(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->populateLayout(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$selectedItem(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;)I
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->selectedItem()I

    move-result p0

    return p0
.end method

.method public static final synthetic access$setAddressOriginal$p(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;Lcom/squareup/mailorder/SelectableAddressLayout;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->addressOriginal:Lcom/squareup/mailorder/SelectableAddressLayout;

    return-void
.end method

.method public static final synthetic access$setAddressSuggested$p(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;Lcom/squareup/mailorder/SelectableAddressLayout;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->addressSuggested:Lcom/squareup/mailorder/SelectableAddressLayout;

    return-void
.end method

.method public static final synthetic access$spinnerData(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->spinnerData(Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method private final applyConfigurations(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;Landroid/content/res/Resources;)V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->orderButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_0

    const-string v1, "orderButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;->getButtonLabel()I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final observeClicks(Lcom/squareup/workflow/legacy/Screen;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent;",
            ">;I)V"
        }
    .end annotation

    .line 129
    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->orderButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_0

    const-string v1, "orderButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    .line 175
    new-instance v1, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;Lcom/squareup/workflow/legacy/Screen;I)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final populateLayout(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 105
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    instance-of v1, v0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;

    if-eqz v0, :cond_1

    move-object v1, p0

    check-cast v1, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;

    invoke-direct {v1, v0}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->showSelectAddressState(Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;)V

    .line 107
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->setupNavigation(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    return-void
.end method

.method private final selectedItem()I
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->selectableGroup:Lcom/squareup/noho/RadioGroup;

    if-nez v0, :cond_0

    const-string v1, "selectableGroup"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/RadioGroup;->getSelectedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->addressSuggested:Lcom/squareup/mailorder/SelectableAddressLayout;

    if-nez v0, :cond_2

    const-string v1, "addressSuggested"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/mailorder/SelectableAddressLayout;->getId()I

    move-result v0

    :goto_0
    return v0
.end method

.method private final setupNavigation(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 151
    new-instance v0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$setupNavigation$navigateBack$1;

    invoke-direct {v0, p1}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$setupNavigation$navigateBack$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 158
    iget-object p1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 153
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 154
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    iget-object v3, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->configuration:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;

    invoke-virtual {v3}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;->getActionBarTitle()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 155
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$setupNavigation$1;

    invoke-direct {v3, v0}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$setupNavigation$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 158
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 159
    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final showSelectAddressState(Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;)V
    .locals 3

    .line 111
    invoke-virtual {p1}, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/mailorder/ContactInfo;->getName()Ljava/lang/String;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->addressSuggested:Lcom/squareup/mailorder/SelectableAddressLayout;

    if-nez v1, :cond_0

    const-string v2, "addressSuggested"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 114
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->getCorrectedAddress()Lcom/squareup/address/Address;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/mailorder/SelectableAddressLayout;->populateAddress(Lcom/squareup/address/Address;)V

    .line 115
    invoke-virtual {v1, v0}, Lcom/squareup/mailorder/SelectableAddressLayout;->populateName(Ljava/lang/String;)V

    const/4 v2, 0x1

    .line 116
    invoke-virtual {v1, v2}, Lcom/squareup/mailorder/SelectableAddressLayout;->isRecommended(Z)V

    .line 118
    iget-object v1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->addressOriginal:Lcom/squareup/mailorder/SelectableAddressLayout;

    if-nez v1, :cond_1

    const-string v2, "addressOriginal"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 119
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->getOriginalAddress()Lcom/squareup/address/Address;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/mailorder/SelectableAddressLayout;->populateAddress(Lcom/squareup/address/Address;)V

    .line 120
    invoke-virtual {v1, v0}, Lcom/squareup/mailorder/SelectableAddressLayout;->populateName(Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 121
    invoke-virtual {v1, p1}, Lcom/squareup/mailorder/SelectableAddressLayout;->isRecommended(Z)V

    return-void
.end method

.method private final spinnerData(Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 4

    .line 96
    instance-of p1, p1, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$InProgress;

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v2, v3}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    goto :goto_0

    .line 97
    :cond_0
    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->hideSpinner()Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    :goto_0
    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 67
    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->bindViews(Landroid/view/View;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->configuration:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "view.resources"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->applyConfigurations(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;Landroid/content/res/Resources;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 73
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "spinner\n        .showOrHideSpinner(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 75
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->screens:Lio/reactivex/Observable;

    .line 78
    iget-object v1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v2, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$1;

    invoke-direct {v2, p0}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$1;-><init>(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2(Lrx/functions/Func1;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens\n        .compose\u2026{ spinnerData(it.data) })"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    new-instance v1, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$2;-><init>(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public final bindViews(Landroid/view/View;)V
    .locals 6

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 164
    sget v0, Lcom/squareup/mailorder/R$id;->select_address_1:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.select_address_1)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/mailorder/SelectableAddressLayout;

    iput-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->addressSuggested:Lcom/squareup/mailorder/SelectableAddressLayout;

    .line 165
    sget v0, Lcom/squareup/mailorder/R$id;->select_address_2:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.select_address_2)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/mailorder/SelectableAddressLayout;

    iput-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->addressOriginal:Lcom/squareup/mailorder/SelectableAddressLayout;

    .line 166
    sget v0, Lcom/squareup/mailorder/R$id;->select_address_order_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.s\u2026ect_address_order_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->orderButton:Lcom/squareup/noho/NohoButton;

    .line 167
    new-instance p1, Lcom/squareup/noho/RadioGroup;

    .line 168
    new-instance v0, Lcom/squareup/noho/ListenerAttacher$ListenableCheckableAttacher;

    invoke-direct {v0}, Lcom/squareup/noho/ListenerAttacher$ListenableCheckableAttacher;-><init>()V

    check-cast v0, Lcom/squareup/noho/ListenerAttacher;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/Pair;

    .line 169
    iget-object v2, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->addressSuggested:Lcom/squareup/mailorder/SelectableAddressLayout;

    const-string v3, "addressSuggested"

    if-nez v2, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v4, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->addressSuggested:Lcom/squareup/mailorder/SelectableAddressLayout;

    if-nez v4, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v4}, Lcom/squareup/mailorder/SelectableAddressLayout;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    .line 170
    iget-object v3, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->addressOriginal:Lcom/squareup/mailorder/SelectableAddressLayout;

    const-string v4, "addressOriginal"

    if-nez v3, :cond_2

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v5, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->addressOriginal:Lcom/squareup/mailorder/SelectableAddressLayout;

    if-nez v5, :cond_3

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v5}, Lcom/squareup/mailorder/SelectableAddressLayout;->getId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    aput-object v3, v1, v2

    .line 167
    invoke-direct {p1, v0, v1}, Lcom/squareup/noho/RadioGroup;-><init>(Lcom/squareup/noho/ListenerAttacher;[Lkotlin/Pair;)V

    iput-object p1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->selectableGroup:Lcom/squareup/noho/RadioGroup;

    return-void
.end method
