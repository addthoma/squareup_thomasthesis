.class final Lcom/squareup/mailorder/OrderReactor$onReact$4$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/OrderReactor$onReact$4;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$BackFromCorrectedAddress;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;",
        "it",
        "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$BackFromCorrectedAddress;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/mailorder/OrderReactor$onReact$4;


# direct methods
.method constructor <init>(Lcom/squareup/mailorder/OrderReactor$onReact$4;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$4$1;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$4;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$BackFromCorrectedAddress;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$BackFromCorrectedAddress;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    .line 285
    new-instance v7, Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;

    .line 286
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$onReact$4$1;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$4;

    iget-object v0, v0, Lcom/squareup/mailorder/OrderReactor$onReact$4;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    invoke-virtual {v0}, Lcom/squareup/mailorder/OrderReactor$OrderState;->getItemToken()Ljava/lang/String;

    move-result-object v1

    .line 287
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$onReact$4$1;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$4;

    iget-object v0, v0, Lcom/squareup/mailorder/OrderReactor$onReact$4;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    invoke-virtual {v0}, Lcom/squareup/mailorder/OrderReactor$OrderState;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v2

    .line 288
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$onReact$4$1;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$4;

    iget-object v0, v0, Lcom/squareup/mailorder/OrderReactor$onReact$4;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    check-cast v0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;

    invoke-virtual {v0}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, v7

    .line 285
    invoke-direct/range {v0 .. v6}, Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 284
    invoke-direct {p1, v7}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$BackFromCorrectedAddress;

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/OrderReactor$onReact$4$1;->invoke(Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$BackFromCorrectedAddress;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
