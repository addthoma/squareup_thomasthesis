.class public final Lcom/squareup/mailorder/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final order_address_recommended_label:I = 0x7f1211d8

.field public static final order_confirmed:I = 0x7f1211f7

.field public static final order_email_confirmation:I = 0x7f1211f8

.field public static final order_missing_info_message:I = 0x7f1211fe

.field public static final order_missing_info_title:I = 0x7f1211ff

.field public static final shipping_address_confirmation_message:I = 0x7f1217e7

.field public static final shipping_address_confirmation_title:I = 0x7f1217e8

.field public static final shipping_address_unverified_message:I = 0x7f1217e9

.field public static final shipping_address_unverified_title:I = 0x7f1217ea

.field public static final validating_address:I = 0x7f121b91


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
