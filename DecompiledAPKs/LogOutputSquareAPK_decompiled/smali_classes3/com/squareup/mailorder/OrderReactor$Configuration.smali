.class public final Lcom/squareup/mailorder/OrderReactor$Configuration;
.super Ljava/lang/Object;
.source "OrderReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderReactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Configuration"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderReactor$Configuration;",
        "",
        "verifyAddressBeforeSendingOrder",
        "",
        "allowSubmittingNonUSPSRecognizedAddress",
        "(ZZ)V",
        "getAllowSubmittingNonUSPSRecognizedAddress",
        "()Z",
        "getVerifyAddressBeforeSendingOrder",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allowSubmittingNonUSPSRecognizedAddress:Z

.field private final verifyAddressBeforeSendingOrder:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/mailorder/OrderReactor$Configuration;->verifyAddressBeforeSendingOrder:Z

    iput-boolean p2, p0, Lcom/squareup/mailorder/OrderReactor$Configuration;->allowSubmittingNonUSPSRecognizedAddress:Z

    return-void
.end method


# virtual methods
.method public final getAllowSubmittingNonUSPSRecognizedAddress()Z
    .locals 1

    .line 86
    iget-boolean v0, p0, Lcom/squareup/mailorder/OrderReactor$Configuration;->allowSubmittingNonUSPSRecognizedAddress:Z

    return v0
.end method

.method public final getVerifyAddressBeforeSendingOrder()Z
    .locals 1

    .line 85
    iget-boolean v0, p0, Lcom/squareup/mailorder/OrderReactor$Configuration;->verifyAddressBeforeSendingOrder:Z

    return v0
.end method
