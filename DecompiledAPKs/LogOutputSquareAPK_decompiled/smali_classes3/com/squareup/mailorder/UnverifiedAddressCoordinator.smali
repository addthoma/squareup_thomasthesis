.class public final Lcom/squareup/mailorder/UnverifiedAddressCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "UnverifiedAddressCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;,
        Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUnverifiedAddressCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UnverifiedAddressCoordinator.kt\ncom/squareup/mailorder/UnverifiedAddressCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,152:1\n1103#2,7:153\n1103#2,7:160\n*E\n*S KotlinDebug\n*F\n+ 1 UnverifiedAddressCoordinator.kt\ncom/squareup/mailorder/UnverifiedAddressCoordinator\n*L\n118#1,7:153\n127#1,7:160\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0002%&B5\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0018\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u000e\u0010\u001b\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u001aJ \u0010\u001c\u001a\u00020\u00152\u0016\u0010\u001d\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J\u0010\u0010\u001e\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020 H\u0002J(\u0010!\u001a\u00020\u00152\u0016\u0010\u001d\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J(\u0010\"\u001a\u00020\u00152\u0016\u0010\u001d\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0010\u0010#\u001a\u00020$2\u0006\u0010\u001f\u001a\u00020\u0005H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/mailorder/UnverifiedAddressCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;",
        "Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent;",
        "Lcom/squareup/mailorder/UnverifiedAddressScreen;",
        "configuration",
        "Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;",
        "spinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "(Lio/reactivex/Observable;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;Lcom/squareup/register/widgets/GlassSpinner;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "addressLayout",
        "Lcom/squareup/mailorder/SelectableAddressLayout;",
        "proceedWithAddressButton",
        "Lcom/squareup/noho/NohoButton;",
        "reEnterAddressButton",
        "applyConfiguration",
        "",
        "res",
        "Landroid/content/res/Resources;",
        "attach",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "observeClicks",
        "screen",
        "populateAddressLayout",
        "data",
        "Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$ConfirmAddress;",
        "populateLayout",
        "setupNavigation",
        "spinnerData",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "Configuration",
        "Factory",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private addressLayout:Lcom/squareup/mailorder/SelectableAddressLayout;

.field private final configuration:Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;

.field private proceedWithAddressButton:Lcom/squareup/noho/NohoButton;

.field private reEnterAddressButton:Lcom/squareup/noho/NohoButton;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private final spinner:Lcom/squareup/register/widgets/GlassSpinner;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent;",
            ">;>;",
            "Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ")V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->configuration:Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;

    iput-object p3, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;Lcom/squareup/register/widgets/GlassSpinner;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-void
.end method

.method public static final synthetic access$observeClicks(Lcom/squareup/mailorder/UnverifiedAddressCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->observeClicks(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method public static final synthetic access$populateLayout(Lcom/squareup/mailorder/UnverifiedAddressCoordinator;Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->populateLayout(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$spinnerData(Lcom/squareup/mailorder/UnverifiedAddressCoordinator;Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->spinnerData(Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method private final applyConfiguration(Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;Landroid/content/res/Resources;)V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->proceedWithAddressButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_0

    const-string v1, "proceedWithAddressButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;->getButtonLabel()I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final observeClicks(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent;",
            ">;)V"
        }
    .end annotation

    .line 117
    iget-object v0, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->proceedWithAddressButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_0

    const-string v1, "proceedWithAddressButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    .line 153
    new-instance v1, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->reEnterAddressButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_1

    const-string v1, "reEnterAddressButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    .line 160
    new-instance v1, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$observeClicks$$inlined$onClickDebounced$2;

    invoke-direct {v1, p1}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$observeClicks$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final populateAddressLayout(Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$ConfirmAddress;)V
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->addressLayout:Lcom/squareup/mailorder/SelectableAddressLayout;

    if-nez v0, :cond_0

    const-string v1, "addressLayout"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 109
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$ConfirmAddress;->getAddress()Lcom/squareup/address/Address;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/mailorder/SelectableAddressLayout;->populateAddress(Lcom/squareup/address/Address;)V

    .line 110
    invoke-virtual {p1}, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$ConfirmAddress;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/mailorder/ContactInfo;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/mailorder/SelectableAddressLayout;->populateName(Ljava/lang/String;)V

    return-void
.end method

.method private final populateLayout(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 102
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    instance-of v1, v0, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$ConfirmAddress;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$ConfirmAddress;

    if-eqz v0, :cond_1

    move-object v1, p0

    check-cast v1, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;

    invoke-direct {v1, v0}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->populateAddressLayout(Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$ConfirmAddress;)V

    .line 104
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->setupNavigation(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    return-void
.end method

.method private final setupNavigation(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 134
    new-instance v0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$setupNavigation$navigateBack$1;

    invoke-direct {v0, p1}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$setupNavigation$navigateBack$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 141
    iget-object p1, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 136
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 137
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    iget-object v3, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->configuration:Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;

    invoke-virtual {v3}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;->getActionBarTitle()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 138
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$setupNavigation$1;

    invoke-direct {v3, v0}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$setupNavigation$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 141
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 142
    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final spinnerData(Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 4

    .line 93
    instance-of p1, p1, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$InProgress;

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v2, v3}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    goto :goto_0

    .line 94
    :cond_0
    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->hideSpinner()Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    :goto_0
    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 67
    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->bindViews(Landroid/view/View;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->configuration:Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "view.resources"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->applyConfiguration(Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;Landroid/content/res/Resources;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 72
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "spinner\n        .showOrHideSpinner(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 74
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->screens:Lio/reactivex/Observable;

    .line 77
    iget-object v1, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v2, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$attach$1;

    invoke-direct {v2, p0}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$attach$1;-><init>(Lcom/squareup/mailorder/UnverifiedAddressCoordinator;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2(Lrx/functions/Func1;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens\n        .compose\u2026{ spinnerData(it.data) })"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    new-instance v1, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$attach$2;-><init>(Lcom/squareup/mailorder/UnverifiedAddressCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public final bindViews(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 147
    sget v0, Lcom/squareup/mailorder/R$id;->unverified_address:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.unverified_address)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/mailorder/SelectableAddressLayout;

    iput-object v0, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->addressLayout:Lcom/squareup/mailorder/SelectableAddressLayout;

    .line 148
    sget v0, Lcom/squareup/mailorder/R$id;->unverified_address_re_enter_address:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.u\u2026address_re_enter_address)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->reEnterAddressButton:Lcom/squareup/noho/NohoButton;

    .line 149
    sget v0, Lcom/squareup/mailorder/R$id;->unverified_address_order_free_card:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.u\u2026_address_order_free_card)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->proceedWithAddressButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method
