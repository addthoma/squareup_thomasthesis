.class public final enum Lcom/squareup/mailorder/CardCustomizationOption;
.super Ljava/lang/Enum;
.source "OrderWorkflowStartArgument.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/mailorder/CardCustomizationOption;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/mailorder/CardCustomizationOption;",
        "",
        "(Ljava/lang/String;I)V",
        "NO_CUSTOMIZATION",
        "SIGNATURE",
        "STAMPS",
        "STAMPS_AND_SIGNATURE",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/mailorder/CardCustomizationOption;

.field public static final enum NO_CUSTOMIZATION:Lcom/squareup/mailorder/CardCustomizationOption;

.field public static final enum SIGNATURE:Lcom/squareup/mailorder/CardCustomizationOption;

.field public static final enum STAMPS:Lcom/squareup/mailorder/CardCustomizationOption;

.field public static final enum STAMPS_AND_SIGNATURE:Lcom/squareup/mailorder/CardCustomizationOption;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/mailorder/CardCustomizationOption;

    new-instance v1, Lcom/squareup/mailorder/CardCustomizationOption;

    const/4 v2, 0x0

    const-string v3, "NO_CUSTOMIZATION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/mailorder/CardCustomizationOption;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/mailorder/CardCustomizationOption;->NO_CUSTOMIZATION:Lcom/squareup/mailorder/CardCustomizationOption;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/mailorder/CardCustomizationOption;

    const/4 v2, 0x1

    const-string v3, "SIGNATURE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/mailorder/CardCustomizationOption;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/mailorder/CardCustomizationOption;->SIGNATURE:Lcom/squareup/mailorder/CardCustomizationOption;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/mailorder/CardCustomizationOption;

    const/4 v2, 0x2

    const-string v3, "STAMPS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/mailorder/CardCustomizationOption;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/mailorder/CardCustomizationOption;->STAMPS:Lcom/squareup/mailorder/CardCustomizationOption;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/mailorder/CardCustomizationOption;

    const/4 v2, 0x3

    const-string v3, "STAMPS_AND_SIGNATURE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/mailorder/CardCustomizationOption;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/mailorder/CardCustomizationOption;->STAMPS_AND_SIGNATURE:Lcom/squareup/mailorder/CardCustomizationOption;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/mailorder/CardCustomizationOption;->$VALUES:[Lcom/squareup/mailorder/CardCustomizationOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/mailorder/CardCustomizationOption;
    .locals 1

    const-class v0, Lcom/squareup/mailorder/CardCustomizationOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/mailorder/CardCustomizationOption;

    return-object p0
.end method

.method public static values()[Lcom/squareup/mailorder/CardCustomizationOption;
    .locals 1

    sget-object v0, Lcom/squareup/mailorder/CardCustomizationOption;->$VALUES:[Lcom/squareup/mailorder/CardCustomizationOption;

    invoke-virtual {v0}, [Lcom/squareup/mailorder/CardCustomizationOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/mailorder/CardCustomizationOption;

    return-object v0
.end method
