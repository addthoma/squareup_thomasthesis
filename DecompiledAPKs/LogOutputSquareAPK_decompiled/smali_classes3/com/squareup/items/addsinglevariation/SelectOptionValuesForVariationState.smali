.class public abstract Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;
.super Ljava/lang/Object;
.source "SelectOptionValuesForVariationState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValues;,
        Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValueForCustomVariation;,
        Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$DuplicateVariation;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\t\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u001e\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\u0082\u0001\u0003\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
        "Landroid/os/Parcelable;",
        "()V",
        "selectedValuesByOptionIds",
        "",
        "",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "getSelectedValuesByOptionIds",
        "()Ljava/util/Map;",
        "DuplicateVariation",
        "SelectOptionValueForCustomVariation",
        "SelectOptionValues",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValues;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValueForCustomVariation;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$DuplicateVariation;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getSelectedValuesByOptionIds()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation
.end method
