.class public final Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;
.super Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action;
.source "SelectOptionValuesForVariationWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectMoreOptionValues"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionValuesForVariationWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionValuesForVariationWorkflow.kt\ncom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues\n*L\n1#1,322:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0015\u0010\u0008\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\u0018\u0010\u0011\u001a\u00020\u0012*\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00150\u0013H\u0016R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action;",
        "newSelectedOptionValue",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "(Lcom/squareup/cogs/itemoptions/ItemOptionValue;)V",
        "getNewSelectedOptionValue",
        "()Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final newSelectedOptionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;)V
    .locals 1

    const/4 v0, 0x0

    .line 253
    invoke-direct {p0, v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;->newSelectedOptionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ILjava/lang/Object;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;->newSelectedOptionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;->copy(Lcom/squareup/cogs/itemoptions/ItemOptionValue;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
            "-",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;

    invoke-virtual {v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;->getSelectedValuesByOptionIds()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 257
    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;->newSelectedOptionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    if-eqz v1, :cond_0

    .line 258
    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getOptionId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    :cond_0
    new-instance v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValues;

    invoke-static {v0}, Lkotlin/collections/MapsKt;->toMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValues;-><init>(Ljava/util/Map;)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;->newSelectedOptionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public final copy(Lcom/squareup/cogs/itemoptions/ItemOptionValue;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;
    .locals 1

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;

    invoke-direct {v0, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;->newSelectedOptionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iget-object p1, p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;->newSelectedOptionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNewSelectedOptionValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;->newSelectedOptionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;->newSelectedOptionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SelectMoreOptionValues(newSelectedOptionValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;->newSelectedOptionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
