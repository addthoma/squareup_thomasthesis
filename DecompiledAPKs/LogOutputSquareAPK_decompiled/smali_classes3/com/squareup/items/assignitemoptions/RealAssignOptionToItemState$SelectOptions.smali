.class public final Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;
.super Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;
.source "RealAssignOptionToItemState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Creator;,
        Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0087\u0008\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\t\u0010\t\u001a\u00020\nH\u00d6\u0001J\u0013\u0010\u000b\u001a\u00020\u00032\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\nH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\u0019\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\nH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;",
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;",
        "hasAssignedOptions",
        "",
        "(Z)V",
        "getHasAssignedOptions",
        "()Z",
        "component1",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final Companion:Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Companion;


# instance fields
.field private final hasAssignedOptions:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->Companion:Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Companion;

    new-instance v0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Creator;

    invoke-direct {v0}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Creator;-><init>()V

    sput-object v0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 14
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->hasAssignedOptions:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;ZILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-boolean p1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->hasAssignedOptions:Z

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->copy(Z)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->hasAssignedOptions:Z

    return v0
.end method

.method public final copy(Z)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;
    .locals 1

    new-instance v0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;

    invoke-direct {v0, p1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;-><init>(Z)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->hasAssignedOptions:Z

    iget-boolean p1, p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->hasAssignedOptions:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHasAssignedOptions()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->hasAssignedOptions:Z

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->hasAssignedOptions:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SelectOptions(hasAssignedOptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->hasAssignedOptions:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean p2, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->hasAssignedOptions:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
