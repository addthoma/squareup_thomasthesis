.class public final Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;
.super Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action;
.source "RealAssignOptionToItemWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CheckFetchedVariations"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAssignOptionToItemWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAssignOptionToItemWorkflow.kt\ncom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,143:1\n704#2:144\n777#2,2:145\n1360#2:147\n1429#2,3:148\n1550#2,3:151\n*E\n*S KotlinDebug\n*F\n+ 1 RealAssignOptionToItemWorkflow.kt\ncom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations\n*L\n117#1:144\n117#1,2:145\n118#1:147\n118#1,3:148\n123#1,3:151\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005\u00a2\u0006\u0002\u0010\tJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\u000f\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005H\u00c6\u0003J3\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0008H\u00d6\u0001J\u0018\u0010\u001a\u001a\u00020\u001b*\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e0\u001cH\u0016R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000b\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;",
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action;",
        "engine",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "allVariations",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
        "locallyDeletedVariationTokens",
        "",
        "(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Ljava/util/List;Ljava/util/List;)V",
        "getAllVariations",
        "()Ljava/util/List;",
        "getEngine",
        "()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "getLocallyDeletedVariationTokens",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allVariations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field private final engine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

.field private final locallyDeletedVariationTokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "engine"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allVariations"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locallyDeletedVariationTokens"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 114
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->engine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->allVariations:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->engine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->allVariations:Ljava/util/List;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->copy(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Ljava/util/List;Ljava/util/List;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->engine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getAllVariations()Ljava/util/List;

    move-result-object v0

    const-string v1, "localVariations"

    .line 117
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 144
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 145
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, "it"

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 117
    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 146
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 147
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 148
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 149
    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 118
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 150
    :cond_3
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 119
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 123
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->allVariations:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 151
    instance-of v2, v1, Ljava/util/Collection;

    if-eqz v2, :cond_4

    move-object v2, v1

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_3

    .line 152
    :cond_4
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;

    .line 123
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;->getKey()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    move-result-object v2

    const-string v3, "it.key"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr v2, v4

    if-eqz v2, :cond_5

    const/4 v5, 0x1

    :cond_6
    :goto_3
    if-eqz v5, :cond_7

    .line 126
    sget-object v0, Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput$AssignmentAbortedWithRemoteVariations;->INSTANCE:Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput$AssignmentAbortedWithRemoteVariations;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void

    .line 129
    :cond_7
    sget-object v0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->Companion:Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Companion;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->engine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0, v1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Companion;->from(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->engine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->allVariations:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Ljava/util/List;Ljava/util/List;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;"
        }
    .end annotation

    const-string v0, "engine"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allVariations"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locallyDeletedVariationTokens"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;-><init>(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->engine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->engine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->allVariations:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->allVariations:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllVariations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;"
        }
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->allVariations:Ljava/util/List;

    return-object v0
.end method

.method public final getEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->engine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final getLocallyDeletedVariationTokens()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->engine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->allVariations:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CheckFetchedVariations(engine="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->engine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allVariations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->allVariations:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", locallyDeletedVariationTokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
