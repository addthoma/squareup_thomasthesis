.class public final Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;
.super Ljava/lang/Object;
.source "ChangeOrEnableOptionProps.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChangeOrEnableOptionProps.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChangeOrEnableOptionProps.kt\ncom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,22:1\n250#2,2:23\n1360#2:25\n1429#2,3:26\n*E\n*S KotlinDebug\n*F\n+ 1 ChangeOrEnableOptionProps.kt\ncom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps\n*L\n16#1,2:23\n20#1:25\n20#1,3:26\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B;\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\t\u00a2\u0006\u0002\u0010\rJ\t\u0010\u001c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\tH\u00c6\u0003J\t\u0010 \u001a\u00020\u000bH\u00c6\u0003J\t\u0010!\u001a\u00020\tH\u00c6\u0003JK\u0010\"\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\tH\u00c6\u0001J\u0013\u0010#\u001a\u00020\t2\u0008\u0010$\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010%\u001a\u00020&H\u00d6\u0001J\t\u0010\'\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0017\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u000c\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u0013R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0013R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0016\u001a\u00020\u00178F\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0015R\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0012\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;",
        "",
        "itemName",
        "",
        "optionId",
        "originalSelectedValues",
        "",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "isIncrementalUpdate",
        "",
        "assignmentEngine",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "isChange",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Z)V",
        "getAssignmentEngine",
        "()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "currentSelectedValues",
        "getCurrentSelectedValues",
        "()Ljava/util/List;",
        "()Z",
        "getItemName",
        "()Ljava/lang/String;",
        "option",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "getOption",
        "()Lcom/squareup/cogs/itemoptions/ItemOption;",
        "getOptionId",
        "getOriginalSelectedValues",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

.field private final isChange:Z

.field private final isIncrementalUpdate:Z

.field private final itemName:Ljava/lang/String;

.field private final optionId:Ljava/lang/String;

.field private final originalSelectedValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;Z",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
            "Z)V"
        }
    .end annotation

    const-string v0, "itemName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalSelectedValues"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignmentEngine"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->itemName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->optionId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->originalSelectedValues:Ljava/util/List;

    iput-boolean p4, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isIncrementalUpdate:Z

    iput-object p5, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iput-boolean p6, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isChange:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ZILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->itemName:Ljava/lang/String;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->optionId:Ljava/lang/String;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->originalSelectedValues:Ljava/util/List;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isIncrementalUpdate:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isChange:Z

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move p6, v1

    move-object p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Z)Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->itemName:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->optionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->originalSelectedValues:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isIncrementalUpdate:Z

    return v0
.end method

.method public final component5()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isChange:Z

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Z)Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;Z",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
            "Z)",
            "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;"
        }
    .end annotation

    const-string v0, "itemName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalSelectedValues"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignmentEngine"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->itemName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->itemName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->optionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->optionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->originalSelectedValues:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->originalSelectedValues:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isIncrementalUpdate:Z

    iget-boolean v1, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isIncrementalUpdate:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isChange:Z

    iget-boolean p1, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isChange:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final getCurrentSelectedValues()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getItemOptionValuesInUseByOptionIds()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->optionId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    check-cast v0, Ljava/lang/Iterable;

    .line 25
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 26
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 27
    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    const-string v3, "it"

    .line 20
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValueKt;->toItemOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 28
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public final getItemName()Ljava/lang/String;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->itemName:Ljava/lang/String;

    return-object v0
.end method

.method public final getOption()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 4

    .line 16
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getAllItemOptions()Ljava/util/List;

    move-result-object v0

    const-string v1, "assignmentEngine.allItemOptions"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 23
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    const-string v3, "it"

    .line 16
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->optionId:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_2

    .line 24
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 16
    invoke-static {v1}, Lcom/squareup/cogs/itemoptions/ItemOptionKt;->toItemOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v0

    return-object v0
.end method

.method public final getOptionId()Ljava/lang/String;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->optionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getOriginalSelectedValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->originalSelectedValues:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->itemName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->optionId:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->originalSelectedValues:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isIncrementalUpdate:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isChange:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public final isChange()Z
    .locals 1

    .line 14
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isChange:Z

    return v0
.end method

.method public final isIncrementalUpdate()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isIncrementalUpdate:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChangeOrEnableOptionProps(itemName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->itemName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", optionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->optionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", originalSelectedValues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->originalSelectedValues:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isIncrementalUpdate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isIncrementalUpdate:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", assignmentEngine="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isChange="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isChange:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
