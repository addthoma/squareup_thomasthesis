.class public interface abstract Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner;
.super Ljava/lang/Object;
.source "AssignOptionToItemWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowRunner<",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u0000 \u00102\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0010JL\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0008\u001a\u00020\t2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u000b2\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000bH&\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner;",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;",
        "start",
        "",
        "itemName",
        "",
        "itemToken",
        "canSkipFetchingVariations",
        "",
        "existingVariations",
        "",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "locallyDeletedVariationTokens",
        "existingItemOptions",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner$Companion;

    sput-object v0, Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner;->Companion:Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner$Companion;

    return-void
.end method


# virtual methods
.method public abstract start(Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)V"
        }
    .end annotation
.end method
