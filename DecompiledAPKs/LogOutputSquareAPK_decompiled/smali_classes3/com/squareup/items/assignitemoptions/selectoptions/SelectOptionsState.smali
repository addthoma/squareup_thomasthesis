.class public abstract Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;
.super Ljava/lang/Object;
.source "SelectOptionsState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$SelectOption;,
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;,
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateOption;,
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateVariations;,
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$TooManyAssignedOptionsError;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0005\u0008\t\n\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u0004R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0005\r\u000e\u000f\u0010\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
        "Landroid/os/Parcelable;",
        "()V",
        "searchText",
        "",
        "getSearchText",
        "()Ljava/lang/String;",
        "update",
        "ChangeOrEnableOption",
        "CreateOption",
        "CreateVariations",
        "SelectOption",
        "TooManyAssignedOptionsError",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$SelectOption;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateOption;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateVariations;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$TooManyAssignedOptionsError;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;-><init>()V

    return-void
.end method

.method public static synthetic update$default(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;
    .locals 0

    if-nez p3, :cond_1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 13
    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;->getSearchText()Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;->update(Ljava/lang/String;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: update"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public abstract getSearchText()Ljava/lang/String;
.end method

.method public final update(Ljava/lang/String;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;
    .locals 4

    const-string v0, "searchText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    instance-of v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$SelectOption;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$SelectOption;

    invoke-direct {v0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$SelectOption;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;

    goto :goto_0

    .line 17
    :cond_0
    instance-of v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;

    .line 18
    move-object v1, p0

    check-cast v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;

    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v2

    .line 19
    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;->isChange()Z

    move-result v3

    .line 20
    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;->getInitiallySelectedOptionValues()Ljava/util/List;

    move-result-object v1

    .line 17
    invoke-direct {v0, v2, v3, v1, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;ZLjava/util/List;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;

    goto :goto_0

    .line 22
    :cond_1
    instance-of v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateOption;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateOption;

    invoke-direct {v0, p1, v2, v1, v2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateOption;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;

    goto :goto_0

    .line 23
    :cond_2
    instance-of v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateVariations;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateVariations;

    move-object v1, p0

    check-cast v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateVariations;

    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateVariations;->getNumberOfExistingVariations()I

    move-result v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateVariations;-><init>(Ljava/lang/String;I)V

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;

    goto :goto_0

    .line 24
    :cond_3
    instance-of v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$TooManyAssignedOptionsError;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$TooManyAssignedOptionsError;

    invoke-direct {v0, p1, v2, v1, v2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$TooManyAssignedOptionsError;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;

    :goto_0
    return-object v0

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
