.class final Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$6;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectOptionsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->render(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$6;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$6;->$props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 58
    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$6;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 135
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$6;->$sink:Lcom/squareup/workflow/Sink;

    .line 136
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Next;

    .line 137
    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$6;->$props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->isIncrementalAssignment()Z

    move-result v2

    .line 138
    iget-object v3, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$6;->$props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    invoke-virtual {v3}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v3

    .line 136
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Next;-><init>(ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V

    .line 135
    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
