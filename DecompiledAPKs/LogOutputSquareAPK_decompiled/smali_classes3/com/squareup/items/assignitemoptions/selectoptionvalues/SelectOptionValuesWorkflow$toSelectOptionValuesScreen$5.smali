.class final Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$5;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectOptionValuesWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->toSelectOptionValuesScreen(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/workflow/Sink;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "optionValue",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "isSelected",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $actionSink:Lcom/squareup/workflow/Sink;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$5;->$actionSink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$5;->invoke(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V
    .locals 2

    const-string v0, "optionValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 427
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$5;->$actionSink:Lcom/squareup/workflow/Sink;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;

    invoke-direct {v1, p1, p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
