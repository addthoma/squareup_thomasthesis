.class public final Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;
.super Ljava/lang/Object;
.source "SelectOptionValuesScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionValuesScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionValuesScreen.kt\ncom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen\n*L\n1#1,70:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u00084\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u00cf\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u0008\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0003\u0012\u0006\u0010\u0012\u001a\u00020\u0003\u0012\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00150\u0014\u0012\u0018\u0010\u0016\u001a\u0014\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00150\u0017\u0012\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001a\u0012\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001a\u0012\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001a\u0012\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001a\u0012\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001a\u00a2\u0006\u0002\u0010\u001fJ\t\u0010;\u001a\u00020\u0003H\u00c6\u0003J\t\u0010<\u001a\u00020\u0003H\u00c6\u0003J\u0015\u0010=\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00150\u0014H\u00c6\u0003J\u001b\u0010>\u001a\u0014\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00150\u0017H\u00c6\u0003J\u000f\u0010?\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001aH\u00c6\u0003J\u000f\u0010@\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001aH\u00c6\u0003J\u000f\u0010A\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001aH\u00c6\u0003J\u000f\u0010B\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001aH\u00c6\u0003J\u000f\u0010C\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001aH\u00c6\u0003J\t\u0010D\u001a\u00020\u0003H\u00c6\u0003J\t\u0010E\u001a\u00020\u0006H\u00c6\u0003J\t\u0010F\u001a\u00020\u0008H\u00c6\u0003J\t\u0010G\u001a\u00020\nH\u00c6\u0003J\t\u0010H\u001a\u00020\u0008H\u00c6\u0003J\u000f\u0010I\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003J\t\u0010J\u001a\u00020\u0010H\u00c6\u0003J\t\u0010K\u001a\u00020\u0003H\u00c6\u0003J\u00f5\u0001\u0010L\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00082\u000e\u0008\u0002\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u00102\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u00032\u0014\u0008\u0002\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00150\u00142\u001a\u0008\u0002\u0010\u0016\u001a\u0014\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00150\u00172\u000e\u0008\u0002\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001a2\u000e\u0008\u0002\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001a2\u000e\u0008\u0002\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001a2\u000e\u0008\u0002\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001a2\u000e\u0008\u0002\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001aH\u00c6\u0001J\u0013\u0010M\u001a\u00020\u00032\u0008\u0010N\u001a\u0004\u0018\u00010OH\u00d6\u0003J\t\u0010P\u001a\u00020\u0010H\u00d6\u0001J\u0006\u0010Q\u001a\u00020\u0003J\t\u0010R\u001a\u00020SH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010!R\u0011\u0010\"\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010!R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010!R\u0011\u0010\u000b\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010%R\u001d\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00150\u0014\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\'R\u0017\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001a\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010)R\u0017\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001a\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010)R\u0017\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001a\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008+\u0010)R\u0017\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001a\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008,\u0010)R#\u0010\u0016\u001a\u0014\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00150\u0017\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010.R\u0017\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001a\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008/\u0010)R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00080\u00101R\u0017\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00082\u00103R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00084\u00105R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00086\u0010%R\u0011\u0010\u0012\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00087\u0010!R\u0011\u0010\u0011\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00088\u0010!R\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00089\u0010:\u00a8\u0006T"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "isDoneButtonEnabled",
        "",
        "areAllOptionsShown",
        "removeButtonState",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;",
        "searchText",
        "Lcom/squareup/workflow/text/WorkflowEditableText;",
        "optionName",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;",
        "newValueInEditing",
        "optionValueSelections",
        "",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
        "sizeOfAllOptionValuesInOption",
        "",
        "shouldHighlightDuplicateNewValueName",
        "shouldFocusOnNewValueRowAndShowKeyboard",
        "onAllOptionsToggled",
        "Lkotlin/Function1;",
        "",
        "onOptionToggled",
        "Lkotlin/Function2;",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "onNewOptionValuePromoted",
        "Lkotlin/Function0;",
        "onNewValueFromCreateButton",
        "onRemoveOptionSet",
        "onCancel",
        "onDone",
        "(ZZLcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;IZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V",
        "getAreAllOptionsShown",
        "()Z",
        "hasReachedOptionValueNumberLimit",
        "getHasReachedOptionValueNumberLimit",
        "getNewValueInEditing",
        "()Lcom/squareup/workflow/text/WorkflowEditableText;",
        "getOnAllOptionsToggled",
        "()Lkotlin/jvm/functions/Function1;",
        "getOnCancel",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnDone",
        "getOnNewOptionValuePromoted",
        "getOnNewValueFromCreateButton",
        "getOnOptionToggled",
        "()Lkotlin/jvm/functions/Function2;",
        "getOnRemoveOptionSet",
        "getOptionName",
        "()Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;",
        "getOptionValueSelections",
        "()Ljava/util/List;",
        "getRemoveButtonState",
        "()Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;",
        "getSearchText",
        "getShouldFocusOnNewValueRowAndShowKeyboard",
        "getShouldHighlightDuplicateNewValueName",
        "getSizeOfAllOptionValuesInOption",
        "()I",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component15",
        "component16",
        "component17",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "showingSearchResults",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final areAllOptionsShown:Z

.field private final hasReachedOptionValueNumberLimit:Z

.field private final isDoneButtonEnabled:Z

.field private final newValueInEditing:Lcom/squareup/workflow/text/WorkflowEditableText;

.field private final onAllOptionsToggled:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onCancel:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onDone:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onOptionToggled:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onRemoveOptionSet:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final optionName:Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

.field private final optionValueSelections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final removeButtonState:Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

.field private final searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

.field private final shouldFocusOnNewValueRowAndShowKeyboard:Z

.field private final shouldHighlightDuplicateNewValueName:Z

.field private final sizeOfAllOptionValuesInOption:I


# direct methods
.method public constructor <init>(ZZLcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;IZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
            ">;IZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p11

    move-object/from16 v7, p12

    move-object/from16 v8, p13

    move-object/from16 v9, p14

    move-object/from16 v10, p15

    move-object/from16 v11, p16

    move-object/from16 v12, p17

    const-string v13, "removeButtonState"

    invoke-static {v1, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "searchText"

    invoke-static {v2, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "optionName"

    invoke-static {v3, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "newValueInEditing"

    invoke-static {v4, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "optionValueSelections"

    invoke-static {v5, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "onAllOptionsToggled"

    invoke-static {v6, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "onOptionToggled"

    invoke-static {v7, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "onNewOptionValuePromoted"

    invoke-static {v8, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "onNewValueFromCreateButton"

    invoke-static {v9, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "onRemoveOptionSet"

    invoke-static {v10, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "onCancel"

    invoke-static {v11, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "onDone"

    invoke-static {v12, v13}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move v13, p1

    iput-boolean v13, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->isDoneButtonEnabled:Z

    move/from16 v13, p2

    iput-boolean v13, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->areAllOptionsShown:Z

    iput-object v1, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->removeButtonState:Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    iput-object v2, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    iput-object v3, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionName:Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    iput-object v4, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->newValueInEditing:Lcom/squareup/workflow/text/WorkflowEditableText;

    iput-object v5, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionValueSelections:Ljava/util/List;

    move/from16 v1, p8

    iput v1, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->sizeOfAllOptionValuesInOption:I

    move/from16 v1, p9

    iput-boolean v1, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldHighlightDuplicateNewValueName:Z

    move/from16 v1, p10

    iput-boolean v1, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    iput-object v6, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onAllOptionsToggled:Lkotlin/jvm/functions/Function1;

    iput-object v7, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onOptionToggled:Lkotlin/jvm/functions/Function2;

    iput-object v8, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    iput-object v9, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    iput-object v10, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onRemoveOptionSet:Lkotlin/jvm/functions/Function0;

    iput-object v11, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onCancel:Lkotlin/jvm/functions/Function0;

    iput-object v12, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onDone:Lkotlin/jvm/functions/Function0;

    .line 57
    iget v1, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->sizeOfAllOptionValuesInOption:I

    const/16 v2, 0xfa

    if-lt v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->hasReachedOptionValueNumberLimit:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;ZZLcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;IZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p18

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->isDoneButtonEnabled:Z

    goto :goto_0

    :cond_0
    move/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->areAllOptionsShown:Z

    goto :goto_1

    :cond_1
    move/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->removeButtonState:Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionName:Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->newValueInEditing:Lcom/squareup/workflow/text/WorkflowEditableText;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionValueSelections:Ljava/util/List;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget v9, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->sizeOfAllOptionValuesInOption:I

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-boolean v10, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldHighlightDuplicateNewValueName:Z

    goto :goto_8

    :cond_8
    move/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-boolean v11, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    goto :goto_9

    :cond_9
    move/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget-object v12, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onAllOptionsToggled:Lkotlin/jvm/functions/Function1;

    goto :goto_a

    :cond_a
    move-object/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget-object v13, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onOptionToggled:Lkotlin/jvm/functions/Function2;

    goto :goto_b

    :cond_b
    move-object/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget-object v14, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    goto :goto_c

    :cond_c
    move-object/from16 v14, p13

    :goto_c
    and-int/lit16 v15, v1, 0x2000

    if-eqz v15, :cond_d

    iget-object v15, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    goto :goto_d

    :cond_d
    move-object/from16 v15, p14

    :goto_d
    move-object/from16 p14, v15

    and-int/lit16 v15, v1, 0x4000

    if-eqz v15, :cond_e

    iget-object v15, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onRemoveOptionSet:Lkotlin/jvm/functions/Function0;

    goto :goto_e

    :cond_e
    move-object/from16 v15, p15

    :goto_e
    const v16, 0x8000

    and-int v16, v1, v16

    move-object/from16 p15, v15

    if-eqz v16, :cond_f

    iget-object v15, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onCancel:Lkotlin/jvm/functions/Function0;

    goto :goto_f

    :cond_f
    move-object/from16 v15, p16

    :goto_f
    const/high16 v16, 0x10000

    and-int v1, v1, v16

    if-eqz v1, :cond_10

    iget-object v1, v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onDone:Lkotlin/jvm/functions/Function0;

    goto :goto_10

    :cond_10
    move-object/from16 v1, p17

    :goto_10
    move/from16 p1, v2

    move/from16 p2, v3

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move/from16 p8, v9

    move/from16 p9, v10

    move/from16 p10, v11

    move-object/from16 p11, v12

    move-object/from16 p12, v13

    move-object/from16 p13, v14

    move-object/from16 p16, v15

    move-object/from16 p17, v1

    invoke-virtual/range {p0 .. p17}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->copy(ZZLcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;IZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->isDoneButtonEnabled:Z

    return v0
.end method

.method public final component10()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    return v0
.end method

.method public final component11()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onAllOptionsToggled:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component12()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onOptionToggled:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final component13()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component14()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component15()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onRemoveOptionSet:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component16()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onCancel:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component17()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onDone:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->areAllOptionsShown:Z

    return v0
.end method

.method public final component3()Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->removeButtonState:Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    return-object v0
.end method

.method public final component4()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final component5()Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionName:Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    return-object v0
.end method

.method public final component6()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->newValueInEditing:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final component7()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionValueSelections:Ljava/util/List;

    return-object v0
.end method

.method public final component8()I
    .locals 1

    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->sizeOfAllOptionValuesInOption:I

    return v0
.end method

.method public final component9()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldHighlightDuplicateNewValueName:Z

    return v0
.end method

.method public final copy(ZZLcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;IZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
            ">;IZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;"
        }
    .end annotation

    move/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v16, p16

    move-object/from16 v17, p17

    const-string v0, "removeButtonState"

    move-object/from16 v1, p3

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchText"

    move-object/from16 v1, p4

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionName"

    move-object/from16 v1, p5

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newValueInEditing"

    move-object/from16 v1, p6

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionValueSelections"

    move-object/from16 v1, p7

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onAllOptionsToggled"

    move-object/from16 v1, p11

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onOptionToggled"

    move-object/from16 v1, p12

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onNewOptionValuePromoted"

    move-object/from16 v1, p13

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onNewValueFromCreateButton"

    move-object/from16 v1, p14

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onRemoveOptionSet"

    move-object/from16 v1, p15

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCancel"

    move-object/from16 v1, p16

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDone"

    move-object/from16 v1, p17

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v18, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-direct/range {v0 .. v17}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;-><init>(ZZLcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;IZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-object v18
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->isDoneButtonEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->isDoneButtonEnabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->areAllOptionsShown:Z

    iget-boolean v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->areAllOptionsShown:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->removeButtonState:Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->removeButtonState:Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionName:Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionName:Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->newValueInEditing:Lcom/squareup/workflow/text/WorkflowEditableText;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->newValueInEditing:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionValueSelections:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionValueSelections:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->sizeOfAllOptionValuesInOption:I

    iget v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->sizeOfAllOptionValuesInOption:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldHighlightDuplicateNewValueName:Z

    iget-boolean v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldHighlightDuplicateNewValueName:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    iget-boolean v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onAllOptionsToggled:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onAllOptionsToggled:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onOptionToggled:Lkotlin/jvm/functions/Function2;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onOptionToggled:Lkotlin/jvm/functions/Function2;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onRemoveOptionSet:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onRemoveOptionSet:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onCancel:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onCancel:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onDone:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onDone:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAreAllOptionsShown()Z
    .locals 1

    .line 36
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->areAllOptionsShown:Z

    return v0
.end method

.method public final getHasReachedOptionValueNumberLimit()Z
    .locals 1

    .line 57
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->hasReachedOptionValueNumberLimit:Z

    return v0
.end method

.method public final getNewValueInEditing()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->newValueInEditing:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final getOnAllOptionsToggled()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onAllOptionsToggled:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnCancel()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onCancel:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnDone()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onDone:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnNewOptionValuePromoted()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnNewValueFromCreateButton()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnOptionToggled()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onOptionToggled:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final getOnRemoveOptionSet()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onRemoveOptionSet:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOptionName()Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionName:Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    return-object v0
.end method

.method public final getOptionValueSelections()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionValueSelections:Ljava/util/List;

    return-object v0
.end method

.method public final getRemoveButtonState()Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->removeButtonState:Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    return-object v0
.end method

.method public final getSearchText()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final getShouldFocusOnNewValueRowAndShowKeyboard()Z
    .locals 1

    .line 46
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    return v0
.end method

.method public final getShouldHighlightDuplicateNewValueName()Z
    .locals 1

    .line 45
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldHighlightDuplicateNewValueName:Z

    return v0
.end method

.method public final getSizeOfAllOptionValuesInOption()I
    .locals 1

    .line 44
    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->sizeOfAllOptionValuesInOption:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->isDoneButtonEnabled:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->areAllOptionsShown:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->removeButtonState:Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionName:Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->newValueInEditing:Lcom/squareup/workflow/text/WorkflowEditableText;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionValueSelections:Ljava/util/List;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_6
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->sizeOfAllOptionValuesInOption:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldHighlightDuplicateNewValueName:Z

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :cond_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    if-eqz v2, :cond_8

    goto :goto_5

    :cond_8
    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onAllOptionsToggled:Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_9
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onOptionToggled:Lkotlin/jvm/functions/Function2;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_a
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_b
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_c
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onRemoveOptionSet:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_d
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onCancel:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_e
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onDone:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_f
    add-int/2addr v0, v3

    return v0
.end method

.method public final isDoneButtonEnabled()Z
    .locals 1

    .line 35
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->isDoneButtonEnabled:Z

    return v0
.end method

.method public final showingSearchResults()Z
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-virtual {v0}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SelectOptionValuesScreen(isDoneButtonEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->isDoneButtonEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", areAllOptionsShown="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->areAllOptionsShown:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", removeButtonState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->removeButtonState:Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", searchText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", optionName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionName:Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", newValueInEditing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->newValueInEditing:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", optionValueSelections="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->optionValueSelections:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", sizeOfAllOptionValuesInOption="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->sizeOfAllOptionValuesInOption:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", shouldHighlightDuplicateNewValueName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldHighlightDuplicateNewValueName:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", shouldFocusOnNewValueRowAndShowKeyboard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", onAllOptionsToggled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onAllOptionsToggled:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onOptionToggled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onOptionToggled:Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onNewOptionValuePromoted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onNewValueFromCreateButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onRemoveOptionSet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onRemoveOptionSet:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onCancel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onCancel:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onDone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->onDone:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
