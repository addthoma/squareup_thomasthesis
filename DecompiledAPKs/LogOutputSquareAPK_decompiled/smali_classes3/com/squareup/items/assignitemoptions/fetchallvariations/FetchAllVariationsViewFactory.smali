.class public final Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "FetchAllVariationsViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory;

    invoke-direct {v0}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory;-><init>()V

    sput-object v0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory;->INSTANCE:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 9

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 9
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 10
    const-class v2, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchInProgressScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    .line 11
    sget v3, Lcom/squareup/items/assignitemoptions/impl/R$layout;->fetch_in_progress:I

    .line 12
    sget-object v4, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory$1;

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 9
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 21
    sget-object v1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory;->Companion:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 8
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
