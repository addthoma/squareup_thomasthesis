.class public final Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;
.super Ljava/lang/Object;
.source "SelectVariationsToCreateLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$Factory;,
        Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectVariationsToCreateLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectVariationsToCreateLayoutRunner.kt\ncom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,251:1\n1360#2:252\n1429#2,3:253\n704#2:256\n777#2,2:257\n1550#2,3:259\n49#3:262\n50#3,3:268\n53#3:303\n599#4,4:263\n601#4:267\n310#5,6:271\n310#5,6:277\n310#5,6:283\n310#5,6:289\n310#5,6:295\n43#6,2:301\n*E\n*S KotlinDebug\n*F\n+ 1 SelectVariationsToCreateLayoutRunner.kt\ncom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner\n*L\n169#1:252\n169#1,3:253\n207#1:256\n207#1,2:257\n208#1,3:259\n49#1:262\n49#1,3:268\n49#1:303\n49#1,4:263\n49#1:267\n49#1,6:271\n49#1,6:277\n49#1,6:283\n49#1,6:289\n49#1,6:295\n49#1,2:301\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0016\u0017B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0002J\u0018\u0010\u0013\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0015H\u0016R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n \n*\u0004\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;",
        "view",
        "Landroid/view/View;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "kotlin.jvm.PlatformType",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "configureActionBar",
        "",
        "rendering",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Factory",
        "SelectVariationsToCreateRow",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;->view:Landroid/view/View;

    .line 46
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 48
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$id;->select_variations_to_create_recycler:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 49
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    sget-object v0, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 263
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 264
    new-instance v0, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v0}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 268
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 269
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 50
    sget-object p2, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 272
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$$special$$inlined$row$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 56
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$2$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$2$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 67
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$2$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$2$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 271
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 278
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$$special$$inlined$row$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 71
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 102
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 277
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 284
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$$special$$inlined$row$3;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$$special$$inlined$row$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 106
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$4$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$4$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 284
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 283
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 290
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$$special$$inlined$row$4;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$$special$$inlined$row$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 118
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$5$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$5$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 290
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 289
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 296
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$$special$$inlined$row$5;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$$special$$inlined$row$5;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 134
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$6$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$6$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 296
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 295
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 301
    new-instance p2, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {p2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/16 v1, 0x8

    .line 151
    invoke-virtual {p2, v1}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    check-cast p2, Lcom/squareup/cycler/ExtensionSpec;

    .line 301
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 266
    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 263
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final configureActionBar(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;)V
    .locals 6

    .line 207
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;->getOptionValueCombinationSelections()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 256
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 257
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;

    .line 207
    invoke-virtual {v3}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 258
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 259
    instance-of v0, v1, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    .line 260
    :cond_2
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;

    const/4 v2, 0x1

    .line 219
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 209
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 210
    new-instance v3, Lcom/squareup/resources/ResourceString;

    sget v4, Lcom/squareup/items/assignitemoptions/impl/R$string;->select_variations_to_create_screen_title:I

    invoke-direct {v3, v4}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v3, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 211
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;->getShowBackArrowInActionBar()Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    goto :goto_2

    :cond_4
    sget-object v3, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    :goto_2
    new-instance v4, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$configureActionBar$1;

    invoke-direct {v4, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$configureActionBar$1;-><init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v3, v4}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 215
    sget-object v3, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 216
    new-instance v4, Lcom/squareup/resources/ResourceString;

    sget v5, Lcom/squareup/common/strings/R$string;->done:I

    invoke-direct {v4, v5}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v4, Lcom/squareup/resources/TextModel;

    .line 218
    new-instance v5, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$configureActionBar$2;

    invoke-direct {v5, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$configureActionBar$2;-><init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 214
    invoke-virtual {v1, v3, v4, v2, v5}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 219
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 9

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;->configureActionBar(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;)V

    .line 169
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;->getOptionValueCombinationSelections()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 252
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 253
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 254
    check-cast v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;

    .line 170
    new-instance v4, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow$CombinationCheckableRow;

    .line 171
    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->getName()Ljava/lang/String;

    move-result-object v5

    .line 172
    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->isSelected()Z

    move-result v6

    .line 173
    new-instance v7, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$showRendering$$inlined$map$lambda$1;

    invoke-direct {v7, v1, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$showRendering$$inlined$map$lambda$1;-><init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 176
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;->getShouldDisableUnselectedVariations()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->isSelected()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    .line 170
    :goto_1
    invoke-direct {v4, v5, v6, v7, v2}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow$CombinationCheckableRow;-><init>(Ljava/lang/String;ZLkotlin/jvm/functions/Function1;Z)V

    .line 177
    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 255
    :cond_1
    check-cast v0, Ljava/util/List;

    .line 180
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;->getNameOfValueUsedToExtendExistingVariations()Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x2

    if-eqz p2, :cond_2

    new-array v4, v1, [Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow;

    .line 182
    new-instance v5, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow$UpdateExistingVariationsWithOptionValueRow;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;->getOnExtendVariationValueTapped()Lkotlin/jvm/functions/Function0;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow$UpdateExistingVariationsWithOptionValueRow;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    check-cast v5, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow;

    aput-object v5, v4, v3

    .line 183
    new-instance p2, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow$UpdateExistingVariationsHelpText;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;->getItemName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p2, v5}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow$UpdateExistingVariationsHelpText;-><init>(Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow;

    aput-object p2, v4, v2

    .line 181
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    if-eqz p2, :cond_2

    goto :goto_2

    .line 185
    :cond_2
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p2

    .line 186
    :goto_2
    check-cast p2, Ljava/util/Collection;

    new-array v1, v1, [Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow;

    .line 188
    sget-object v4, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow$VariationsLabel;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow$VariationsLabel;

    check-cast v4, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow;

    aput-object v4, v1, v3

    .line 189
    new-instance v3, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow$AllCombinationsCheckableRow;

    .line 190
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;->getAreAllCombinationsSelected()Z

    move-result v4

    .line 191
    new-instance v5, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$showRendering$rows$1;

    invoke-direct {v5, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$showRendering$rows$1;-><init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 194
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;->getShouldDisableAllCombinationsCheckableRow()Z

    move-result v6

    .line 189
    invoke-direct {v3, v4, v5, v6}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow$AllCombinationsCheckableRow;-><init>(ZLkotlin/jvm/functions/Function1;Z)V

    check-cast v3, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow;

    aput-object v3, v1, v2

    .line 187
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 186
    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/util/Collection;

    .line 197
    check-cast v0, Ljava/lang/Iterable;

    .line 196
    invoke-static {p2, v0}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p2

    .line 199
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    new-instance v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$showRendering$1;

    invoke-direct {v1, p2}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$showRendering$1;-><init>(Ljava/util/List;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    .line 202
    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$showRendering$2;

    invoke-direct {v0, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$showRendering$2;-><init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;->showRendering(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
