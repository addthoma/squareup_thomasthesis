.class public final Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;
.super Ljava/lang/Object;
.source "CreateItemTutorial_Creator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
        ">;"
    }
.end annotation


# instance fields
.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final precogServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/precog/PrecogService;",
            ">;"
        }
    .end annotation
.end field

.field private final prefsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/precog/PrecogService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->tutorialProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p2, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->tutorialPresenterProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p4, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p5, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->precogServiceProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p6, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p7, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->busProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p8, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p9, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->prefsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/precog/PrecogService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;)",
            "Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;"
        }
    .end annotation

    .line 73
    new-instance v10, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/precog/PrecogService;Ljavax/inject/Provider;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/server/precog/PrecogService;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;"
        }
    .end annotation

    .line 81
    new-instance v10, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/precog/PrecogService;Ljavax/inject/Provider;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;
    .locals 9

    .line 62
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->tutorialProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->tutorialPresenterProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    iget-object v3, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v4, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->precogServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/server/precog/PrecogService;

    iget-object v5, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->cogsProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/badbus/BadBus;

    iget-object v7, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iget-object v8, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->prefsProvider:Ljavax/inject/Provider;

    invoke-interface {v8}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    invoke-static/range {v0 .. v8}, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->newInstance(Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/precog/PrecogService;Ljavax/inject/Provider;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorial_Creator_Factory;->get()Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;

    move-result-object v0

    return-object v0
.end method
