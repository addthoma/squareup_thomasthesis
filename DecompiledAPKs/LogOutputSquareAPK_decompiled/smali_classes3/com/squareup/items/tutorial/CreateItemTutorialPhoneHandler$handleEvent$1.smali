.class final Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$handleEvent$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CreateItemTutorialPhoneHandler.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/tutorialv2/TutorialState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;


# direct methods
.method constructor <init>(Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$handleEvent$1;->this$0:Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/tutorialv2/TutorialState;
    .locals 3

    .line 224
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$handleEvent$1;->this$0:Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->TUTORIAL_COMPLETE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$handleEvent$1;->this$0:Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;

    const-string v1, "Tutorial Complete"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 50
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$handleEvent$1;->invoke()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method
