.class public Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;
.super Ljava/lang/Object;
.source "RealCreateItemTutorialRunner.kt"

# interfaces
.implements Lcom/squareup/items/tutorial/CreateItemTutorialRunner;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\u0008\u0016\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0008\u0010\r\u001a\u00020\u000eH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;",
        "Lcom/squareup/items/tutorial/CreateItemTutorialRunner;",
        "flow",
        "Lflow/Flow;",
        "createItemTutorialCreator",
        "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
        "orderEntryAppletGateway",
        "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "home",
        "Lcom/squareup/ui/main/Home;",
        "(Lflow/Flow;Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/main/Home;)V",
        "showCreateItemTutorial",
        "",
        "items-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final createItemTutorialCreator:Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;

.field private final flow:Lflow/Flow;

.field private final home:Lcom/squareup/ui/main/Home;

.field private final orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/main/Home;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "createItemTutorialCreator"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryAppletGateway"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "home"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;->createItemTutorialCreator:Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;

    iput-object p3, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iput-object p4, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p5, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;->home:Lcom/squareup/ui/main/Home;

    return-void
.end method


# virtual methods
.method public showCreateItemTutorial()V
    .locals 4

    .line 20
    iget-object v0, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->hasOrderEntryApplet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    iget-object v0, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_SHOW_MODAL_VIA_SUPPORT_APPLET:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->FAVORITES:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-interface {v0, v1}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->historyFactoryForMode(Lcom/squareup/orderentry/OrderEntryMode;)Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v0

    .line 23
    iget-object v1, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;->flow:Lflow/Flow;

    iget-object v2, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;->home:Lcom/squareup/ui/main/Home;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/squareup/ui/main/HistoryFactory;->createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;

    move-result-object v0

    sget-object v2, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-virtual {v1, v0, v2}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/items/tutorial/RealCreateItemTutorialRunner;->createItemTutorialCreator:Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->ready(Z)V

    :cond_0
    return-void
.end method
