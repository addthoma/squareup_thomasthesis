.class final Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "EditUnitWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/RealEditUnitWorkflow;->render(Lcom/squareup/items/unit/EditUnitInput;Lcom/squareup/items/unit/EditUnitState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/unit/EditUnitScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/unit/EditUnitState;",
        "+",
        "Lcom/squareup/items/unit/EditUnitResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditUnitWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditUnitWorkflow.kt\ncom/squareup/items/unit/RealEditUnitWorkflow$render$3\n*L\n1#1,531:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/unit/EditUnitState;",
        "Lcom/squareup/items/unit/EditUnitResult;",
        "event",
        "Lcom/squareup/items/unit/EditUnitScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Lcom/squareup/items/unit/EditUnitInput;

.field final synthetic $state:Lcom/squareup/items/unit/EditUnitState;

.field final synthetic this$0:Lcom/squareup/items/unit/RealEditUnitWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/RealEditUnitWorkflow;Lcom/squareup/items/unit/EditUnitState;Lcom/squareup/items/unit/EditUnitInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->this$0:Lcom/squareup/items/unit/RealEditUnitWorkflow;

    iput-object p2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    iput-object p3, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$input:Lcom/squareup/items/unit/EditUnitInput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/unit/EditUnitScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/unit/EditUnitScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/unit/EditUnitState;",
            "Lcom/squareup/items/unit/EditUnitResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    instance-of v0, p1, Lcom/squareup/items/unit/EditUnitScreen$Event$SaveEdits;

    const-string v1, "UUID.randomUUID()\n                .toString()"

    const-string v2, "state.currentUnitInEditing.build()"

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eqz v0, :cond_a

    .line 172
    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v0, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v0

    check-cast p1, Lcom/squareup/items/unit/EditUnitScreen$Event$SaveEdits;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen$Event$SaveEdits;->getNewPrecision()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->setPrecision(I)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v0

    .line 176
    move-object v5, v4

    check-cast v5, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    .line 177
    iget-object v6, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v6, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v6}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v6

    invoke-static {v6, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    const-string v6, "state.currentUnitInEditing.build().measurementUnit"

    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/items/unit/ui/EditUnitCoordinatorKt;->isCustomUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 179
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen$Event$SaveEdits;->getNewName()Ljava/lang/String;

    move-result-object v2

    const-string v6, "null cannot be cast to non-null type kotlin.CharSequence"

    if-eqz v2, :cond_4

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->setCustomUnitName(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v0

    .line 180
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen$Event$SaveEdits;->getNewAbbreviation()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->setCustomUnitAbbreviation(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v0

    .line 181
    iget-object v2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->this$0:Lcom/squareup/items/unit/RealEditUnitWorkflow;

    .line 182
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen$Event$SaveEdits;->getNewName()Ljava/lang/String;

    move-result-object v6

    .line 183
    iget-object v7, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$input:Lcom/squareup/items/unit/EditUnitInput;

    invoke-virtual {v7}, Lcom/squareup/items/unit/EditUnitInput;->getUnitsInUse()Ljava/util/Set;

    move-result-object v7

    .line 184
    iget-object v8, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$input:Lcom/squareup/items/unit/EditUnitInput;

    invoke-virtual {v8}, Lcom/squareup/items/unit/EditUnitInput;->getUnitToEdit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    :cond_0
    move-object v8, v4

    .line 181
    :goto_0
    invoke-static {v2, v6, v7, v8}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->access$doesNewNameDuplicateExistingNames(Lcom/squareup/items/unit/RealEditUnitWorkflow;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 187
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 188
    new-instance v1, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;

    .line 189
    iget-object v2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v2, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v2}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v2

    .line 190
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v0

    const-string v5, "newOrUpdatedUnitBuilder.build()"

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    iget-object v5, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v5, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v5}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->isDefaultUnit()Z

    move-result v5

    .line 188
    invoke-direct {v1, v2, v0, v5}, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Z)V

    .line 187
    invoke-static {p1, v1, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 194
    :cond_1
    iget-object v2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v2, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v2}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->isCreatingNewUnit()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 201
    iget-object v2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->this$0:Lcom/squareup/items/unit/RealEditUnitWorkflow;

    .line 199
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen$Event$SaveEdits;->getNewName()Ljava/lang/String;

    move-result-object v5

    .line 200
    iget-object v6, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->this$0:Lcom/squareup/items/unit/RealEditUnitWorkflow;

    invoke-static {v6}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->access$getAllStandardUnits$p(Lcom/squareup/items/unit/RealEditUnitWorkflow;)Lcom/squareup/items/unit/AllPredefinedStandardUnits;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/items/unit/AllPredefinedStandardUnits;->getUnits()Ljava/util/List;

    move-result-object v6

    .line 198
    invoke-static {v2, v5, v6}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->access$matchingStandardUnit(Lcom/squareup/items/unit/RealEditUnitWorkflow;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 206
    iget-object v5, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v5, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v5}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getBackingObject()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v5

    .line 209
    new-instance v6, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen$Event$SaveEdits;->getNewPrecision()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v6, v2, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Ljava/lang/Integer;)V

    .line 207
    invoke-virtual {v5, v6}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->measurement_unit_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object p1

    .line 211
    new-instance v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    new-instance v5, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object p1

    invoke-direct {v5, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    invoke-direct {v2, v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    move-object v5, v2

    goto :goto_1

    :cond_2
    move-object v5, v4

    goto :goto_1

    .line 180
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v6}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 179
    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v6}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 215
    :cond_5
    :goto_1
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    .line 216
    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    iget-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast p1, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->isCreatingNewUnit()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 218
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x23

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v0

    if-eqz v5, :cond_6

    .line 219
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object p1

    move-object v5, p1

    goto :goto_2

    :cond_6
    move-object v5, v4

    .line 221
    :cond_7
    :goto_2
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v9

    if-eqz v5, :cond_8

    .line 222
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    goto :goto_3

    :cond_8
    move-object p1, v4

    .line 223
    :goto_3
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 224
    new-instance v1, Lcom/squareup/items/unit/EditUnitState$Saving;

    const-string v2, "newOrUpdatedUnitToSave"

    if-eqz p1, :cond_9

    move-object v8, p1

    goto :goto_4

    .line 226
    :cond_9
    invoke-static {v9, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v8, v9

    .line 227
    :goto_4
    invoke-static {v9, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    iget-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast p1, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v10

    .line 229
    sget-object v11, Lcom/squareup/items/unit/SaveUnitAction;->UPDATE:Lcom/squareup/items/unit/SaveUnitAction;

    .line 230
    iget-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast p1, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->isDefaultUnit()Z

    move-result v12

    move-object v6, v1

    .line 224
    invoke-direct/range {v6 .. v12}, Lcom/squareup/items/unit/EditUnitState$Saving;-><init>(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/items/unit/SaveUnitAction;Z)V

    .line 223
    invoke-static {v0, v1, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 235
    :cond_a
    instance-of v0, p1, Lcom/squareup/items/unit/EditUnitScreen$Event$DiscardEdits;

    if-eqz v0, :cond_c

    .line 236
    iget-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast p1, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v0, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_b

    .line 237
    iget-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->this$0:Lcom/squareup/items/unit/RealEditUnitWorkflow;

    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    move-object v1, v0

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;

    check-cast v0, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->access$discardAction(Lcom/squareup/items/unit/RealEditUnitWorkflow;Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 239
    :cond_b
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 240
    new-instance v0, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;

    .line 241
    iget-object v1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    .line 242
    iget-object v5, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v5, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v5}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v5

    invoke-static {v5, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    iget-object v2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v2, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v2}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->isDefaultUnit()Z

    move-result v2

    .line 240
    invoke-direct {v0, v1, v5, v2}, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Z)V

    .line 239
    invoke-static {p1, v0, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 249
    :cond_c
    instance-of p1, p1, Lcom/squareup/items/unit/EditUnitScreen$Event$DeleteUnit;

    if-eqz p1, :cond_d

    .line 250
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    .line 251
    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 253
    new-instance v0, Lcom/squareup/items/unit/EditUnitState$Saving;

    .line 255
    iget-object v1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v7

    invoke-static {v7, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v8, 0x0

    .line 256
    iget-object v1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v9

    .line 257
    sget-object v10, Lcom/squareup/items/unit/SaveUnitAction;->DELETE:Lcom/squareup/items/unit/SaveUnitAction;

    .line 258
    iget-object v1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->isDefaultUnit()Z

    move-result v11

    const/4 v12, 0x4

    const/4 v13, 0x0

    move-object v5, v0

    .line 253
    invoke-direct/range {v5 .. v13}, Lcom/squareup/items/unit/EditUnitState$Saving;-><init>(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/items/unit/SaveUnitAction;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 252
    invoke-static {p1, v0, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    :cond_d
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 116
    check-cast p1, Lcom/squareup/items/unit/EditUnitScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;->invoke(Lcom/squareup/items/unit/EditUnitScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
