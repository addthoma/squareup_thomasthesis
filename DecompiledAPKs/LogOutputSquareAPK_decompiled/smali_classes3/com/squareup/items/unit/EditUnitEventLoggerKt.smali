.class public final Lcom/squareup/items/unit/EditUnitEventLoggerKt;
.super Ljava/lang/Object;
.source "EditUnitEventLogger.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "unitEventDetail",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "edit-unit_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final unitEventDetail(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$unitEventDetail"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    if-eqz p0, :cond_0

    const-string p0, "Custom"

    goto :goto_0

    :cond_0
    const-string p0, "Standard"

    :goto_0
    return-object p0
.end method
