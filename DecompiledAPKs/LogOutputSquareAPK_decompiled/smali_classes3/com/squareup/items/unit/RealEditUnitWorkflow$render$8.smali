.class final Lcom/squareup/items/unit/RealEditUnitWorkflow$render$8;
.super Lkotlin/jvm/internal/Lambda;
.source "EditUnitWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/RealEditUnitWorkflow;->render(Lcom/squareup/items/unit/EditUnitInput;Lcom/squareup/items/unit/EditUnitState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/unit/InternetNotAvailableAlertScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/unit/EditUnitState;",
        "+",
        "Lcom/squareup/items/unit/EditUnitResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/unit/EditUnitState;",
        "Lcom/squareup/items/unit/EditUnitResult;",
        "event",
        "Lcom/squareup/items/unit/InternetNotAvailableAlertScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/items/unit/EditUnitState;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/EditUnitState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$8;->$state:Lcom/squareup/items/unit/EditUnitState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/unit/InternetNotAvailableAlertScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/unit/InternetNotAvailableAlertScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/unit/EditUnitState;",
            "Lcom/squareup/items/unit/EditUnitResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 374
    instance-of v0, p1, Lcom/squareup/items/unit/InternetNotAvailableAlertScreen$Event$Cancel;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/items/unit/EditUnitResult$EditDiscarded;->INSTANCE:Lcom/squareup/items/unit/EditUnitResult$EditDiscarded;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 375
    :cond_0
    instance-of p1, p1, Lcom/squareup/items/unit/InternetNotAvailableAlertScreen$Event$Retry;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;

    iget-object v1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$8;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$WarnInternetUnavailable;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$WarnInternetUnavailable;->getBlockedState()Lcom/squareup/items/unit/EditUnitState$RequiresInternet;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;-><init>(Lcom/squareup/items/unit/EditUnitState$RequiresInternet;)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 116
    check-cast p1, Lcom/squareup/items/unit/InternetNotAvailableAlertScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$8;->invoke(Lcom/squareup/items/unit/InternetNotAvailableAlertScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
