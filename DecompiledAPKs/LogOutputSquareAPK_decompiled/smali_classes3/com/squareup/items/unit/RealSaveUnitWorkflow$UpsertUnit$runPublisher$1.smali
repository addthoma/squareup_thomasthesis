.class final Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit$runPublisher$1;
.super Ljava/lang/Object;
.source "SaveUnitWorkflow.kt"

# interfaces
.implements Lio/reactivex/SingleOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;->runPublisher()Lio/reactivex/Flowable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/SingleOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012 \u0010\u0002\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0005 \u0006*\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/SingleEmitter;",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit$runPublisher$1;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/SingleEmitter;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/SingleEmitter<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit$runPublisher$1;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;

    iget-object v0, v0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;

    invoke-static {v0}, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->access$getCogs$p(Lcom/squareup/items/unit/RealSaveUnitWorkflow;)Lcom/squareup/cogs/Cogs;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit$runPublisher$1;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;

    invoke-static {v1}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;->access$getIdempotencyKey$p(Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit$runPublisher$1;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;

    invoke-static {v2}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;->access$getUnit$p(Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    new-instance v3, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit$runPublisher$1$1;

    invoke-direct {v3, p1}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit$runPublisher$1$1;-><init>(Lio/reactivex/SingleEmitter;)V

    check-cast v3, Lcom/squareup/shared/catalog/sync/SyncCallback;

    const/4 p1, 0x1

    invoke-interface {v0, v1, v2, p1, v3}, Lcom/squareup/cogs/Cogs;->upsertCatalogConnectV2Object(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;ZLcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method
