.class public final Lcom/squareup/items/unit/ui/EditUnitCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditUnitCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditUnitCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditUnitCoordinator.kt\ncom/squareup/items/unit/ui/EditUnitCoordinator\n+ 2 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,247:1\n151#2,2:248\n*E\n*S KotlinDebug\n*F\n+ 1 EditUnitCoordinator.kt\ncom/squareup/items/unit/ui/EditUnitCoordinator\n*L\n233#1,2:248\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\u0018\u00002\u00020\u0001B1\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010 \u001a\u00020!2\u0006\u0010\u0002\u001a\u00020\u0005H\u0002J\u0010\u0010\"\u001a\u00020\u00192\u0006\u0010#\u001a\u00020\u0019H\u0002J\u0010\u0010$\u001a\u00020\u00192\u0006\u0010%\u001a\u00020\u0019H\u0002J\u0008\u0010&\u001a\u00020\u0019H\u0002J\u0010\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0002J\u0010\u0010+\u001a\u00020,2\u0006\u0010)\u001a\u00020*H\u0002J\u0010\u0010-\u001a\u00020\u001c2\u0006\u0010\u0002\u001a\u00020\u0005H\u0002J\u0010\u0010.\u001a\u00020\u001c2\u0006\u0010\u0002\u001a\u00020\u0005H\u0002J\u0018\u0010/\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0002\u001a\u00020\u0005H\u0002J\u0010\u00100\u001a\u00020\u001c2\u0006\u0010\u0002\u001a\u00020\u0005H\u0002J\u0010\u00101\u001a\u00020\u001c2\u0006\u0010\u0002\u001a\u00020\u0005H\u0002J\u0010\u00102\u001a\u00020\u001c2\u0006\u0010\u0002\u001a\u00020\u0005H\u0002J\u0010\u00103\u001a\u00020\u001c2\u0006\u0010\u0002\u001a\u00020\u0005H\u0002J\u0010\u00104\u001a\u00020\u001c2\u0006\u0010\u0002\u001a\u00020\u0005H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00190\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/items/unit/ui/EditUnitCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screen",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/items/unit/EditUnitScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/util/Res;)V",
        "abbreviationEditText",
        "Lcom/squareup/noho/NohoEditText;",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "compositeDisposable",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "customUnitFields",
        "Lcom/squareup/noho/NohoLinearLayout;",
        "deleteUnitButton",
        "Lcom/squareup/ui/ConfirmButton;",
        "precisionGroup",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "precisionIdToPrecisionMap",
        "",
        "",
        "unitNameEditText",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "canSave",
        "",
        "getPrecisionIdByIndex",
        "index",
        "getPrecisionIndexById",
        "id",
        "getSelectedPrecisionIndex",
        "getTitleText",
        "",
        "state",
        "Lcom/squareup/items/unit/EditUnitState$EditUnit;",
        "getUpButtonGlyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "onCancel",
        "onSave",
        "onScreenUpdate",
        "setupActionBar",
        "setupDeleteButton",
        "setupPrecision",
        "setupTextHandlers",
        "setupUnitNameAndAbbreviation",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private abbreviationEditText:Lcom/squareup/noho/NohoEditText;

.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

.field private customUnitFields:Lcom/squareup/noho/NohoLinearLayout;

.field private deleteUnitButton:Lcom/squareup/ui/ConfirmButton;

.field private precisionGroup:Lcom/squareup/noho/NohoCheckableGroup;

.field private final precisionIdToPrecisionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final screen:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private unitNameEditText:Lcom/squareup/noho/NohoEditText;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/util/Res;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->screen:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->res:Lcom/squareup/util/Res;

    const/4 p1, 0x6

    new-array p1, p1, [Lkotlin/Pair;

    .line 49
    sget p2, Lcom/squareup/editunit/R$id;->edit_unit_precision_0:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    aput-object p2, p1, v0

    .line 50
    sget p2, Lcom/squareup/editunit/R$id;->edit_unit_precision_1:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    aput-object p2, p1, v0

    .line 51
    sget p2, Lcom/squareup/editunit/R$id;->edit_unit_precision_2:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    aput-object p2, p1, v0

    .line 52
    sget p2, Lcom/squareup/editunit/R$id;->edit_unit_precision_3:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    aput-object p2, p1, v0

    .line 53
    sget p2, Lcom/squareup/editunit/R$id;->edit_unit_precision_4:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    aput-object p2, p1, v0

    .line 54
    sget p2, Lcom/squareup/editunit/R$id;->edit_unit_precision_5:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    aput-object p2, p1, v0

    .line 48
    invoke-static {p1}, Lkotlin/collections/MapsKt;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->precisionIdToPrecisionMap:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic access$getPrecisionIndexById(Lcom/squareup/items/unit/ui/EditUnitCoordinator;I)I
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->getPrecisionIndexById(I)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$onCancel(Lcom/squareup/items/unit/ui/EditUnitCoordinator;Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->onCancel(Lcom/squareup/items/unit/EditUnitScreen;)V

    return-void
.end method

.method public static final synthetic access$onSave(Lcom/squareup/items/unit/ui/EditUnitCoordinator;Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->onSave(Lcom/squareup/items/unit/EditUnitScreen;)V

    return-void
.end method

.method public static final synthetic access$onScreenUpdate(Lcom/squareup/items/unit/ui/EditUnitCoordinator;Landroid/view/View;Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->onScreenUpdate(Landroid/view/View;Lcom/squareup/items/unit/EditUnitScreen;)V

    return-void
.end method

.method public static final synthetic access$setupActionBar(Lcom/squareup/items/unit/ui/EditUnitCoordinator;Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->setupActionBar(Lcom/squareup/items/unit/EditUnitScreen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 74
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 76
    sget v0, Lcom/squareup/editunit/R$id;->edit_unit_name:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->unitNameEditText:Lcom/squareup/noho/NohoEditText;

    .line 78
    sget v0, Lcom/squareup/editunit/R$id;->edit_unit_abbreviation:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->abbreviationEditText:Lcom/squareup/noho/NohoEditText;

    .line 80
    sget v0, Lcom/squareup/editunit/R$id;->edit_unit_custom_unit_fields:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLinearLayout;

    iput-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->customUnitFields:Lcom/squareup/noho/NohoLinearLayout;

    .line 81
    sget v0, Lcom/squareup/editunit/R$id;->precision_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->precisionGroup:Lcom/squareup/noho/NohoCheckableGroup;

    .line 82
    sget v0, Lcom/squareup/editunit/R$id;->edit_unit_delete:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ConfirmButton;

    iput-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->deleteUnitButton:Lcom/squareup/ui/ConfirmButton;

    return-void
.end method

.method private final canSave(Lcom/squareup/items/unit/EditUnitScreen;)Z
    .locals 3

    .line 187
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen;->getState()Lcom/squareup/items/unit/EditUnitState$EditUnit;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    const-string v0, "screen.state.originalUnit.measurementUnit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinatorKt;->isCustomUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Z

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_5

    .line 188
    iget-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->unitNameEditText:Lcom/squareup/noho/NohoEditText;

    if-nez p1, :cond_0

    const-string/jumbo v2, "unitNameEditText"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->abbreviationEditText:Lcom/squareup/noho/NohoEditText;

    if-nez p1, :cond_2

    const-string v2, "abbreviationEditText"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    if-eqz p1, :cond_3

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_3

    const/4 p1, 0x1

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_4

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :cond_5
    :goto_2
    return v0
.end method

.method private final getPrecisionIdByIndex(I)I
    .locals 3

    .line 233
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->precisionIdToPrecisionMap:Ljava/util/Map;

    .line 248
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    if-ne p1, v1, :cond_0

    return v2

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method private final getPrecisionIndexById(I)I
    .locals 1

    .line 229
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->precisionIdToPrecisionMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final getSelectedPrecisionIndex()I
    .locals 2

    .line 225
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->precisionGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez v0, :cond_0

    const-string v1, "precisionGroup"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableGroup;->getCheckedId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->getPrecisionIndexById(I)I

    move-result v0

    return v0
.end method

.method private final getTitleText(Lcom/squareup/items/unit/EditUnitState$EditUnit;)Ljava/lang/CharSequence;
    .locals 5

    .line 103
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v0

    const-string v1, "state.currentUnitInEditing.build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v0

    const-string v1, "currentUnit"

    .line 104
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/items/unit/ui/EditUnitCoordinatorKt;->isCustomUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Z

    move-result v1

    .line 105
    iget-object v2, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {v0, v2}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v2

    .line 106
    iget-object v3, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {v0, v3}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    .line 108
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->isCreatingNewUnit()Z

    move-result p1

    const-string/jumbo v3, "unit_abbreviation"

    const-string/jumbo v4, "unit_name"

    if-eqz p1, :cond_2

    if-eqz v1, :cond_0

    .line 111
    iget-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/editunit/R$string;->edit_unit_screen_title_create_custom:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_0

    .line 114
    :cond_0
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_1

    .line 116
    iget-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/editunit/R$string;->edit_unit_screen_title_create_standard:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 117
    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v4, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 118
    invoke-virtual {p1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 119
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    :goto_0
    const-string v0, "if (isCustom) {\n        \u2026        .format()\n      }"

    .line 109
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 114
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Check failed."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_2
    if-eqz v1, :cond_3

    .line 124
    iget-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/editunit/R$string;->edit_unit_screen_title_edit_custom:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_1

    .line 126
    :cond_3
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_4

    .line 128
    iget-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/editunit/R$string;->edit_unit_screen_title_edit_standard:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 129
    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v4, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 130
    invoke-virtual {p1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 131
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_1

    .line 134
    :cond_4
    iget-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/editunit/R$string;->edit_unit_screen_title_edit_unknown_or_generic:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 135
    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v4, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 136
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    :goto_1
    const-string v0, "if (isCustom) {\n        \u2026ormat()\n        }\n      }"

    .line 122
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object p1
.end method

.method private final getUpButtonGlyph(Lcom/squareup/items/unit/EditUnitState$EditUnit;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 96
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->shouldShowBackButton(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 97
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    .line 99
    :cond_0
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    return-object p1
.end method

.method private final onCancel(Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 3

    .line 205
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 206
    :cond_0
    new-instance v1, Lcom/squareup/items/unit/EditUnitScreen$Event$DiscardEdits;

    .line 207
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen;->getState()Lcom/squareup/items/unit/EditUnitState$EditUnit;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v2

    .line 208
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen;->getState()Lcom/squareup/items/unit/EditUnitState$EditUnit;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object p1

    .line 206
    invoke-direct {v1, v2, p1}, Lcom/squareup/items/unit/EditUnitScreen$Event$DiscardEdits;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;)V

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final onSave(Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 4

    .line 195
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 196
    :cond_0
    new-instance v0, Lcom/squareup/items/unit/EditUnitScreen$Event$SaveEdits;

    .line 197
    iget-object v1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->unitNameEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v1, :cond_1

    const-string/jumbo v2, "unitNameEditText"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 198
    iget-object v2, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->abbreviationEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v2, :cond_2

    const-string v3, "abbreviationEditText"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v2}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 199
    invoke-direct {p0}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->getSelectedPrecisionIndex()I

    move-result v3

    .line 196
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/items/unit/EditUnitScreen$Event$SaveEdits;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final onScreenUpdate(Landroid/view/View;Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 1

    .line 216
    new-instance v0, Lcom/squareup/items/unit/ui/EditUnitCoordinator$onScreenUpdate$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/items/unit/ui/EditUnitCoordinator$onScreenUpdate$1;-><init>(Lcom/squareup/items/unit/ui/EditUnitCoordinator;Lcom/squareup/items/unit/EditUnitScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 217
    invoke-direct {p0, p2}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->setupActionBar(Lcom/squareup/items/unit/EditUnitScreen;)V

    .line 218
    invoke-direct {p0, p2}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->setupPrecision(Lcom/squareup/items/unit/EditUnitScreen;)V

    .line 219
    invoke-direct {p0, p2}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->setupUnitNameAndAbbreviation(Lcom/squareup/items/unit/EditUnitScreen;)V

    .line 220
    invoke-direct {p0, p2}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->setupTextHandlers(Lcom/squareup/items/unit/EditUnitScreen;)V

    .line 221
    invoke-direct {p0, p2}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->setupDeleteButton(Lcom/squareup/items/unit/EditUnitScreen;)V

    return-void
.end method

.method private final setupActionBar(Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 4

    .line 92
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBar.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 87
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen;->getState()Lcom/squareup/items/unit/EditUnitState$EditUnit;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->getUpButtonGlyph(Lcom/squareup/items/unit/EditUnitState$EditUnit;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen;->getState()Lcom/squareup/items/unit/EditUnitState$EditUnit;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->getTitleText(Lcom/squareup/items/unit/EditUnitState$EditUnit;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 88
    new-instance v2, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupActionBar$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupActionBar$1;-><init>(Lcom/squareup/items/unit/ui/EditUnitCoordinator;Lcom/squareup/items/unit/EditUnitScreen;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 89
    iget-object v2, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->save:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 90
    invoke-direct {p0, p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->canSave(Lcom/squareup/items/unit/EditUnitScreen;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 91
    new-instance v2, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupActionBar$2;

    invoke-direct {v2, p0, p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupActionBar$2;-><init>(Lcom/squareup/items/unit/ui/EditUnitCoordinator;Lcom/squareup/items/unit/EditUnitScreen;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 92
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final setupDeleteButton(Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 3

    .line 180
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->deleteUnitButton:Lcom/squareup/ui/ConfirmButton;

    const-string v1, "deleteUnitButton"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen;->getState()Lcom/squareup/items/unit/EditUnitState$EditUnit;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->isCreatingNewUnit()Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x8

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0, v2}, Lcom/squareup/ui/ConfirmButton;->setVisibility(I)V

    .line 181
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->deleteUnitButton:Lcom/squareup/ui/ConfirmButton;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v1, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupDeleteButton$1;

    invoke-direct {v1, p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupDeleteButton$1;-><init>(Lcom/squareup/items/unit/EditUnitScreen;)V

    check-cast v1, Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    return-void
.end method

.method private final setupPrecision(Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 3

    .line 143
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen;->getState()Lcom/squareup/items/unit/EditUnitState$EditUnit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v0

    const-string v1, "screen.state.currentUnitInEditing.build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result v0

    .line 144
    iget-object v1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->precisionGroup:Lcom/squareup/noho/NohoCheckableGroup;

    const-string v2, "precisionGroup"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->getPrecisionIdByIndex(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    .line 145
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->precisionGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupPrecision$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupPrecision$1;-><init>(Lcom/squareup/items/unit/ui/EditUnitCoordinator;Lcom/squareup/items/unit/EditUnitScreen;)V

    check-cast v1, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method private final setupTextHandlers(Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 5

    .line 160
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    const-string v1, "compositeDisposable"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 161
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen;->getState()Lcom/squareup/items/unit/EditUnitState$EditUnit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v0

    const-string v2, "screen.state.originalUnit.measurementUnit"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/items/unit/ui/EditUnitCoordinatorKt;->isCustomUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 162
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v1, 0x2

    new-array v1, v1, [Lio/reactivex/disposables/Disposable;

    const/4 v2, 0x0

    .line 163
    iget-object v3, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->unitNameEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v3, :cond_2

    const-string/jumbo v4, "unitNameEditText"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v3, Landroid/widget/TextView;

    invoke-static {v3}, Lcom/squareup/util/rx2/Rx2Views;->debouncedShortText(Landroid/widget/TextView;)Lio/reactivex/Observable;

    move-result-object v3

    .line 164
    new-instance v4, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupTextHandlers$1;

    invoke-direct {v4, p0, p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupTextHandlers$1;-><init>(Lcom/squareup/items/unit/ui/EditUnitCoordinator;Lcom/squareup/items/unit/EditUnitScreen;)V

    check-cast v4, Lio/reactivex/functions/Consumer;

    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 168
    iget-object v3, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->abbreviationEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v3, :cond_3

    const-string v4, "abbreviationEditText"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v3, Landroid/widget/TextView;

    invoke-static {v3}, Lcom/squareup/util/rx2/Rx2Views;->debouncedShortText(Landroid/widget/TextView;)Lio/reactivex/Observable;

    move-result-object v3

    .line 169
    new-instance v4, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupTextHandlers$2;

    invoke-direct {v4, p0, p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupTextHandlers$2;-><init>(Lcom/squareup/items/unit/ui/EditUnitCoordinator;Lcom/squareup/items/unit/EditUnitScreen;)V

    check-cast v4, Lio/reactivex/functions/Consumer;

    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    aput-object p1, v1, v2

    .line 162
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->addAll([Lio/reactivex/disposables/Disposable;)Z

    goto :goto_0

    .line 175
    :cond_4
    iget-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->customUnitFields:Lcom/squareup/noho/NohoLinearLayout;

    if-nez p1, :cond_5

    const-string v0, "customUnitFields"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final setupUnitNameAndAbbreviation(Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 5

    .line 151
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->unitNameEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_0

    const-string/jumbo v1, "unitNameEditText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 152
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen;->getState()Lcom/squareup/items/unit/EditUnitState$EditUnit;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    const-string v2, "screen.state.currentUnitInEditing.build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const-string v3, "screen.state.currentUnit\u2026g.build().measurementUnit"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {v1, v4}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 151
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->abbreviationEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_1

    const-string v1, "abbreviationEditText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 155
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen;->getState()Lcom/squareup/items/unit/EditUnitState$EditUnit;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 154
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->bindViews(Landroid/view/View;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->unitNameEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_0

    const-string/jumbo v1, "unitNameEditText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/editunit/R$string;->edit_unit_name_hint:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->abbreviationEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_1

    const-string v1, "abbreviationEditText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/editunit/R$string;->edit_unit_abbreviation_hint:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 65
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 66
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    if-nez v0, :cond_2

    const-string v1, "compositeDisposable"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Lio/reactivex/disposables/Disposable;

    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->screen:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/items/unit/ui/EditUnitCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator$attach$1;-><init>(Lcom/squareup/items/unit/ui/EditUnitCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screen.subscribe { s -> \u2026view, s.unwrapV2Screen) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
