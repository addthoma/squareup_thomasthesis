.class public final enum Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;
.super Ljava/lang/Enum;
.source "StandardUnitsListCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UnitFamily"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;",
        "",
        "displayOrdinal",
        "",
        "(Ljava/lang/String;II)V",
        "getDisplayOrdinal",
        "()I",
        "WEIGHT",
        "VOLUME",
        "LENGTH",
        "AREA",
        "TIME",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

.field public static final enum AREA:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

.field public static final enum LENGTH:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

.field public static final enum TIME:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

.field public static final enum VOLUME:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

.field public static final enum WEIGHT:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;


# instance fields
.field private final displayOrdinal:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x5

    new-array v1, v0, [Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    new-instance v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string v5, "WEIGHT"

    .line 257
    invoke-direct {v2, v5, v3, v4}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->WEIGHT:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    aput-object v2, v1, v3

    new-instance v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    const/4 v3, 0x2

    const-string v5, "VOLUME"

    .line 258
    invoke-direct {v2, v5, v4, v3}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->VOLUME:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    aput-object v2, v1, v4

    new-instance v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    const/4 v4, 0x3

    const-string v5, "LENGTH"

    .line 259
    invoke-direct {v2, v5, v3, v4}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->LENGTH:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    aput-object v2, v1, v3

    new-instance v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    const/4 v3, 0x4

    const-string v5, "AREA"

    .line 260
    invoke-direct {v2, v5, v4, v3}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->AREA:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    aput-object v2, v1, v4

    new-instance v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    const-string v4, "TIME"

    .line 261
    invoke-direct {v2, v4, v3, v0}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->TIME:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    aput-object v2, v1, v3

    sput-object v1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->$VALUES:[Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 256
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->displayOrdinal:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;
    .locals 1

    const-class v0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    return-object p0
.end method

.method public static values()[Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;
    .locals 1

    sget-object v0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->$VALUES:[Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    invoke-virtual {v0}, [Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    return-object v0
.end method


# virtual methods
.method public final getDisplayOrdinal()I
    .locals 1

    .line 256
    iget v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->displayOrdinal:I

    return v0
.end method
