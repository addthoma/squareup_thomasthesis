.class public final Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RecyclerNoho.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->createRecycler(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;",
        "Lcom/squareup/noho/NohoRow;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerNoho.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$2\n+ 2 StandardUnitsListCoordinator.kt\ncom/squareup/items/unit/ui/StandardUnitsListCoordinator\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,173:1\n184#2,4:174\n186#2:178\n185#2,6:179\n195#2:192\n1103#3,7:185\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u0002H\u00042\u0006\u0010\u0008\u001a\u00020\tH\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "<anonymous parameter 0>",
        "",
        "item",
        "row",
        "Lcom/squareup/noho/NohoRow;",
        "invoke",
        "(ILjava/lang/Object;Lcom/squareup/noho/NohoRow;)V",
        "com/squareup/noho/dsl/RecyclerNohoKt$nohoRow$2",
        "com/squareup/items/unit/ui/StandardUnitsListCoordinator$$special$$inlined$nohoRow$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;


# direct methods
.method public constructor <init>(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p3, Lcom/squareup/noho/NohoRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$2;->invoke(ILjava/lang/Object;Lcom/squareup/noho/NohoRow;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;Lcom/squareup/noho/NohoRow;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;",
            "Lcom/squareup/noho/NohoRow;",
            ")V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "row"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    check-cast p2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;

    .line 174
    iget-object p1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;

    invoke-static {p1}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->access$getRes$p(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/editunit/R$string;->standard_units_list_unit_label:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 179
    invoke-virtual {p2}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;->getStandardUnit()Lcom/squareup/items/unit/LocalizedStandardUnit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string/jumbo v1, "unit_name"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 178
    invoke-virtual {p2}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;->getStandardUnit()Lcom/squareup/items/unit/LocalizedStandardUnit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getAbbreviation()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string/jumbo v1, "unit_abbreviation"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 177
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 182
    invoke-virtual {p2}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;->getStandardUnit()Lcom/squareup/items/unit/LocalizedStandardUnit;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getName()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoRow;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 184
    check-cast p3, Landroid/view/View;

    .line 185
    new-instance p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$2$1;

    invoke-direct {p1, p2}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$2$1;-><init>(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;)V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
