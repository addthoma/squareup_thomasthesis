.class final Lcom/squareup/items/unit/SelectableUnit$writeToParcel$snapshot$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectableUnit.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/SelectableUnit;->writeToParcel(Landroid/os/Parcel;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "bufferedSink",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/unit/SelectableUnit;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/SelectableUnit;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/SelectableUnit$writeToParcel$snapshot$1;->this$0:Lcom/squareup/items/unit/SelectableUnit;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/SelectableUnit$writeToParcel$snapshot$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "bufferedSink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object v0, Lcom/squareup/items/unit/SelectableUnit;->Companion:Lcom/squareup/items/unit/SelectableUnit$Companion;

    iget-object v1, p0, Lcom/squareup/items/unit/SelectableUnit$writeToParcel$snapshot$1;->this$0:Lcom/squareup/items/unit/SelectableUnit;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/items/unit/SelectableUnit$Companion;->writeSelectableUnit(Lokio/BufferedSink;Lcom/squareup/items/unit/SelectableUnit;)V

    return-void
.end method
