.class public final Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;
.super Ljava/lang/Object;
.source "CatalogMeasurementUnitSerializationHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0005J\u0012\u0010\u0006\u001a\u00020\u0007*\u00020\u00082\u0006\u0010\t\u001a\u00020\u0004\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;",
        "",
        "()V",
        "readCatalogMeasurementUnit",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "Lokio/BufferedSource;",
        "writeCatalogMeasurementUnit",
        "",
        "Lokio/BufferedSink;",
        "unit",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
    .locals 2

    const-string v0, "$this$readCatalogMeasurementUnit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v0

    int-to-long v0, v0

    .line 22
    invoke-interface {p1, v0, v1}, Lokio/BufferedSource;->readByteArray(J)[B

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->fromByteArray([B)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    const-string v0, "fromByteArray(readByteAr\u2026nitEncodedSize.toLong()))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 1

    const-string v0, "$this$writeCatalogMeasurementUnit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->toByteArray()[B

    move-result-object p2

    .line 16
    array-length v0, p2

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    const-string/jumbo v0, "unitEncoded"

    .line 17
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lokio/BufferedSink;->write([B)Lokio/BufferedSink;

    return-void
.end method
