.class public final Lcom/squareup/items/unit/SaveUnitState$Companion;
.super Ljava/lang/Object;
.source "SaveUnitState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/unit/SaveUnitState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSaveUnitState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SaveUnitState.kt\ncom/squareup/items/unit/SaveUnitState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,149:1\n180#2:150\n148#2:151\n*E\n*S KotlinDebug\n*F\n+ 1 SaveUnitState.kt\ncom/squareup/items/unit/SaveUnitState$Companion\n*L\n79#1:150\n106#1:151\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000c\u0010\u0007\u001a\u00020\u0008*\u00020\tH\u0002J\u000c\u0010\n\u001a\u00020\u000b*\u00020\tH\u0002J\u0014\u0010\u000c\u001a\u00020\r*\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0008H\u0002J\u0014\u0010\u0010\u001a\u00020\r*\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u000bH\u0002\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/items/unit/SaveUnitState$Companion;",
        "",
        "()V",
        "fromSnapshot",
        "Lcom/squareup/items/unit/SaveUnitState;",
        "byteString",
        "Lokio/ByteString;",
        "readSaving",
        "Lcom/squareup/items/unit/SaveUnitState$Saving;",
        "Lokio/BufferedSource;",
        "readUnitSaveFailed",
        "Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;",
        "writeSaving",
        "",
        "Lokio/BufferedSink;",
        "saving",
        "writeUnitSaveFailed",
        "unitSaveFailed",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/squareup/items/unit/SaveUnitState$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$writeSaving(Lcom/squareup/items/unit/SaveUnitState$Companion;Lokio/BufferedSink;Lcom/squareup/items/unit/SaveUnitState$Saving;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/unit/SaveUnitState$Companion;->writeSaving(Lokio/BufferedSink;Lcom/squareup/items/unit/SaveUnitState$Saving;)V

    return-void
.end method

.method public static final synthetic access$writeUnitSaveFailed(Lcom/squareup/items/unit/SaveUnitState$Companion;Lokio/BufferedSink;Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/unit/SaveUnitState$Companion;->writeUnitSaveFailed(Lokio/BufferedSink;Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;)V

    return-void
.end method

.method private final readSaving(Lokio/BufferedSource;)Lcom/squareup/items/unit/SaveUnitState$Saving;
    .locals 3

    .line 104
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 105
    sget-object v1, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    .line 151
    const-class v2, Lcom/squareup/items/unit/SaveUnitAction;

    invoke-virtual {v2}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Enum;

    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result p1

    aget-object p1, v2, p1

    const-string v2, "T::class.java.enumConstants[readInt()]"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/items/unit/SaveUnitAction;

    .line 103
    new-instance v2, Lcom/squareup/items/unit/SaveUnitState$Saving;

    invoke-direct {v2, v0, v1, p1}, Lcom/squareup/items/unit/SaveUnitState$Saving;-><init>(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/items/unit/SaveUnitAction;)V

    return-object v2
.end method

.method private final readUnitSaveFailed(Lokio/BufferedSource;)Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;
    .locals 5

    .line 121
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 122
    const-class v1, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$SavingFailed;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$SavingFailed;

    .line 123
    move-object v1, p0

    check-cast v1, Lcom/squareup/items/unit/SaveUnitState$Companion;

    invoke-direct {v1, p1}, Lcom/squareup/items/unit/SaveUnitState$Companion;->readSaving(Lokio/BufferedSource;)Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object v1

    .line 124
    new-instance v2, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    .line 125
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v3

    .line 126
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v4

    .line 127
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    .line 124
    invoke-direct {v2, v3, v4, p1}, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;-><init>(IIZ)V

    .line 122
    invoke-direct {v0, v1, v2}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$SavingFailed;-><init>(Lcom/squareup/items/unit/SaveUnitState$Saving;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;)V

    check-cast v0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;

    goto :goto_0

    .line 130
    :cond_0
    const-class v1, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;

    .line 131
    new-instance v1, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    .line 132
    move-object v2, p0

    check-cast v2, Lcom/squareup/items/unit/SaveUnitState$Companion;

    invoke-direct {v2, p1}, Lcom/squareup/items/unit/SaveUnitState$Companion;->readSaving(Lokio/BufferedSource;)Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object v2

    .line 131
    invoke-direct {v1, v2}, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;-><init>(Lcom/squareup/items/unit/SaveUnitState$Saving;)V

    .line 134
    new-instance v2, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    .line 135
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v3

    .line 136
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v4

    .line 137
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    .line 134
    invoke-direct {v2, v3, v4, p1}, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;-><init>(IIZ)V

    .line 130
    invoke-direct {v0, v1, v2}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;-><init>(Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;)V

    check-cast v0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;

    :goto_0
    return-object v0

    .line 140
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final writeSaving(Lokio/BufferedSink;Lcom/squareup/items/unit/SaveUnitState$Saving;)V
    .locals 2

    .line 98
    invoke-virtual {p2}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getIdempotencyKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 99
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {p2}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getUnitToSave()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 100
    invoke-virtual {p2}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getSaveUnitAction()Lcom/squareup/items/unit/SaveUnitAction;

    move-result-object p2

    check-cast p2, Ljava/lang/Enum;

    invoke-static {p1, p2}, Lcom/squareup/workflow/SnapshotKt;->writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    return-void
.end method

.method private final writeUnitSaveFailed(Lokio/BufferedSink;Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;)V
    .locals 2

    .line 110
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unitSaveFailed::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 112
    instance-of v0, p2, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$SavingFailed;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/items/unit/SaveUnitState$Companion;

    move-object v1, p2

    check-cast v1, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$SavingFailed;

    invoke-virtual {v1}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$SavingFailed;->getPreviousSaving()Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/items/unit/SaveUnitState$Companion;->writeSaving(Lokio/BufferedSink;Lcom/squareup/items/unit/SaveUnitState$Saving;)V

    goto :goto_0

    .line 113
    :cond_0
    instance-of v0, p2, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/squareup/items/unit/SaveUnitState$Companion;

    move-object v1, p2

    check-cast v1, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;

    invoke-virtual {v1}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->getPreviousFetchingCount()Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;->getIncomingSaving()Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/items/unit/SaveUnitState$Companion;->writeSaving(Lokio/BufferedSink;Lcom/squareup/items/unit/SaveUnitState$Saving;)V

    .line 115
    :cond_1
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;->getConfiguration()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;->getUnitSaveFailedAlertTitleId()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 116
    invoke-virtual {p2}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;->getConfiguration()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;->getUnitSaveFailedAlertMessageId()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 117
    invoke-virtual {p2}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;->getConfiguration()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;->getShouldAllowRetry()Z

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    return-void
.end method


# virtual methods
.method public final fromSnapshot(Lokio/ByteString;)Lcom/squareup/items/unit/SaveUnitState;
    .locals 3

    const-string v0, "byteString"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 80
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 81
    const-class v1, Lcom/squareup/items/unit/SaveUnitState$Saving;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/squareup/items/unit/SaveUnitState;->Companion:Lcom/squareup/items/unit/SaveUnitState$Companion;

    invoke-direct {v0, p1}, Lcom/squareup/items/unit/SaveUnitState$Companion;->readSaving(Lokio/BufferedSource;)Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object p1

    check-cast p1, Lcom/squareup/items/unit/SaveUnitState;

    goto :goto_1

    .line 82
    :cond_0
    const-class v1, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    .line 83
    sget-object v1, Lcom/squareup/items/unit/SaveUnitState;->Companion:Lcom/squareup/items/unit/SaveUnitState$Companion;

    invoke-direct {v1, p1}, Lcom/squareup/items/unit/SaveUnitState$Companion;->readSaving(Lokio/BufferedSource;)Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object p1

    .line 82
    invoke-direct {v0, p1}, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;-><init>(Lcom/squareup/items/unit/SaveUnitState$Saving;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/items/unit/SaveUnitState;

    goto :goto_1

    .line 85
    :cond_1
    const-class v1, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;

    .line 86
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v1

    .line 87
    sget-object v2, Lcom/squareup/items/unit/SaveUnitState;->Companion:Lcom/squareup/items/unit/SaveUnitState$Companion;

    invoke-direct {v2, p1}, Lcom/squareup/items/unit/SaveUnitState$Companion;->readSaving(Lokio/BufferedSource;)Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object p1

    .line 85
    invoke-direct {v0, v1, p1}, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;-><init>(ILcom/squareup/items/unit/SaveUnitState$Saving;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/items/unit/SaveUnitState;

    goto :goto_1

    .line 89
    :cond_2
    const-class v1, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$SavingFailed;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    const-class v1, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 90
    :goto_0
    sget-object v0, Lcom/squareup/items/unit/SaveUnitState;->Companion:Lcom/squareup/items/unit/SaveUnitState$Companion;

    invoke-direct {v0, p1}, Lcom/squareup/items/unit/SaveUnitState$Companion;->readUnitSaveFailed(Lokio/BufferedSource;)Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;

    move-result-object p1

    check-cast p1, Lcom/squareup/items/unit/SaveUnitState;

    :goto_1
    return-object p1

    .line 92
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
