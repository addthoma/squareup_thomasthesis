.class final Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditOptionLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/editoption/EditOptionLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cycler/StandardRowSpec$Creator<",
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;",
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;",
        "Lcom/squareup/items/editoption/ui/EditOptionValueRow;",
        ">;",
        "Landroid/content/Context;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditOptionLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditOptionLayoutRunner.kt\ncom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1\n+ 2 EditOptionValueRow.kt\ncom/squareup/items/editoption/ui/EditOptionValueRow\n+ 3 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator\n*L\n1#1,309:1\n79#2,2:310\n64#3,2:312\n*E\n*S KotlinDebug\n*F\n+ 1 EditOptionLayoutRunner.kt\ncom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1\n*L\n122#1,2:310\n127#1,2:312\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;",
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;",
        "Lcom/squareup/items/editoption/ui/EditOptionValueRow;",
        "context",
        "Landroid/content/Context;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1;

    invoke-direct {v0}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1;-><init>()V

    sput-object v0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/cycler/StandardRowSpec$Creator;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;",
            "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;",
            "Lcom/squareup/items/editoption/ui/EditOptionValueRow;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/items/editoption/R$integer;->maximum_length_of_option_value_names:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 121
    new-instance v1, Lcom/squareup/items/editoption/ui/EditOptionValueRow;

    invoke-direct {v1, p2}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;-><init>(Landroid/content/Context;)V

    check-cast v1, Landroid/view/View;

    invoke-virtual {p1, v1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->setView(Landroid/view/View;)V

    .line 122
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/editoption/ui/EditOptionValueRow;

    .line 310
    invoke-virtual {v1}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->getEditRow()Lcom/squareup/noho/NohoEditRow;

    move-result-object v1

    const/4 v2, 0x5

    .line 123
    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoEditRow;->setImeOptions(I)V

    const/16 v2, 0x2001

    .line 124
    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoEditRow;->setInputType(I)V

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter$LengthFilter;

    .line 125
    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v3, v0}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v0, 0x0

    aput-object v3, v2, v0

    check-cast v2, [Landroid/text/InputFilter;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoEditRow;->setFilters([Landroid/text/InputFilter;)V

    .line 312
    new-instance v0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1$$special$$inlined$bind$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1$$special$$inlined$bind$1;-><init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->bind(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method
