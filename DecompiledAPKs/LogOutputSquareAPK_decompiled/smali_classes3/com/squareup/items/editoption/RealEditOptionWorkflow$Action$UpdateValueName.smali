.class public final Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;
.super Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;
.source "RealEditOptionWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UpdateValueName"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001J\u0018\u0010\u0013\u001a\u00020\u0014*\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00170\u0015H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;",
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;",
        "valueId",
        "",
        "valueName",
        "(Ljava/lang/String;Ljava/lang/String;)V",
        "getValueId",
        "()Ljava/lang/String;",
        "getValueName",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/editoption/EditOptionState;",
        "Lcom/squareup/items/editoption/EditOptionOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final valueId:Ljava/lang/String;

.field private final valueName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "valueId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "valueName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 279
    invoke-direct {p0, v0}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueId:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueName:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueId:Ljava/lang/String;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueName:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->copy(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/editoption/EditOptionState;",
            "-",
            "Lcom/squareup/items/editoption/EditOptionOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 281
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {v1}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueId:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueName:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/squareup/items/editoption/RealEditOptionWorkflowKt;->access$updateValueName(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/items/editoption/EditOptionState;->updateOption(Lcom/squareup/cogs/itemoptions/ItemOption;)Lcom/squareup/items/editoption/EditOptionState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueId:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueName:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;
    .locals 1

    const-string/jumbo v0, "valueId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "valueName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;

    invoke-direct {v0, p1, p2}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueName:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getValueId()Ljava/lang/String;
    .locals 1

    .line 277
    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueId:Ljava/lang/String;

    return-object v0
.end method

.method public final getValueName()Ljava/lang/String;
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueId:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdateValueName(valueId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", valueName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;->valueName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
