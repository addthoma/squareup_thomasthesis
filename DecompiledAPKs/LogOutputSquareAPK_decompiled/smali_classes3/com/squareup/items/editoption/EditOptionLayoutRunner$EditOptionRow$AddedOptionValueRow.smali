.class public final Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;
.super Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;
.source "EditOptionLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AddedOptionValueRow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c6\u0003J\u000f\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0003JA\u0010\u0019\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00072\u000e\u0008\u0002\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0001J\u0013\u0010\u001a\u001a\u00020\u00072\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001J\t\u0010\u001f\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u000fR\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u000fR\u0017\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;",
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;",
        "id",
        "",
        "value",
        "Lcom/squareup/workflow/text/WorkflowEditableText;",
        "isAddValueRow",
        "",
        "isDuplicate",
        "onDeleteValue",
        "Lkotlin/Function0;",
        "",
        "(Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;ZZLkotlin/jvm/functions/Function0;)V",
        "getId",
        "()Ljava/lang/String;",
        "()Z",
        "getOnDeleteValue",
        "()Lkotlin/jvm/functions/Function0;",
        "getValue",
        "()Lcom/squareup/workflow/text/WorkflowEditableText;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final id:Ljava/lang/String;

.field private final isAddValueRow:Z

.field private final isDuplicate:Z

.field private final onDeleteValue:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final value:Lcom/squareup/workflow/text/WorkflowEditableText;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;ZZLkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "ZZ",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDeleteValue"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 296
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "add-option-value-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;-><init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->value:Lcom/squareup/workflow/text/WorkflowEditableText;

    iput-boolean p3, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isAddValueRow:Z

    iput-boolean p4, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isDuplicate:Z

    iput-object p5, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->onDeleteValue:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;ZZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->id:Ljava/lang/String;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->value:Lcom/squareup/workflow/text/WorkflowEditableText;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isAddValueRow:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isDuplicate:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->onDeleteValue:Lkotlin/jvm/functions/Function0;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->copy(Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;ZZLkotlin/jvm/functions/Function0;)Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->value:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isAddValueRow:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isDuplicate:Z

    return v0
.end method

.method public final component5()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->onDeleteValue:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;ZZLkotlin/jvm/functions/Function0;)Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "ZZ",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;"
        }
    .end annotation

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDeleteValue"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;-><init>(Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;ZZLkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->value:Lcom/squareup/workflow/text/WorkflowEditableText;

    iget-object v1, p1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->value:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isAddValueRow:Z

    iget-boolean v1, p1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isAddValueRow:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isDuplicate:Z

    iget-boolean v1, p1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isDuplicate:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->onDeleteValue:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->onDeleteValue:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getId()Ljava/lang/String;
    .locals 1

    .line 291
    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final getOnDeleteValue()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 295
    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->onDeleteValue:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getValue()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    .line 292
    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->value:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->id:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->value:Lcom/squareup/workflow/text/WorkflowEditableText;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isAddValueRow:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isDuplicate:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->onDeleteValue:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public final isAddValueRow()Z
    .locals 1

    .line 293
    iget-boolean v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isAddValueRow:Z

    return v0
.end method

.method public final isDuplicate()Z
    .locals 1

    .line 294
    iget-boolean v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isDuplicate:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AddedOptionValueRow(id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->value:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isAddValueRow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isAddValueRow:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isDuplicate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isDuplicate:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", onDeleteValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->onDeleteValue:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
