.class public final Lcom/squareup/items/editoption/RealEditOptionWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealEditOptionWorkflow.kt"

# interfaces
.implements Lcom/squareup/items/editoption/EditOptionWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;,
        Lcom/squareup/items/editoption/RealEditOptionWorkflow$DialogAction;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/editoption/EditOptionProps;",
        "Lcom/squareup/items/editoption/EditOptionState;",
        "Lcom/squareup/items/editoption/EditOptionOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/items/editoption/EditOptionWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEditOptionWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEditOptionWorkflow.kt\ncom/squareup/items/editoption/RealEditOptionWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,355:1\n32#2,12:356\n149#3,5:368\n149#3,5:373\n149#3,5:378\n1360#4:383\n1429#4,3:384\n1360#4:387\n1429#4,3:388\n*E\n*S KotlinDebug\n*F\n+ 1 RealEditOptionWorkflow.kt\ncom/squareup/items/editoption/RealEditOptionWorkflow\n*L\n53#1,12:356\n132#1,5:368\n175#1,5:373\n176#1,5:378\n191#1:383\n191#1,3:384\n199#1:387\n199#1,3:388\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0002#$B\u001d\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\u0002\u0010\u0010J\u001a\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00032\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J6\u0010\u0016\u001a\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00190\u0018\u0012\u0004\u0012\u00020\u001a0\u00172\u0006\u0010\u001b\u001a\u00020\u00042\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001dH\u0002JN\u0010\u001e\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u00042\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001dH\u0016J\u0010\u0010\u001f\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\u0004H\u0016J\u000c\u0010 \u001a\u00020!*\u00020\"H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow;",
        "Lcom/squareup/items/editoption/EditOptionWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/editoption/EditOptionProps;",
        "Lcom/squareup/items/editoption/EditOptionState;",
        "Lcom/squareup/items/editoption/EditOptionOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "idGenerator",
        "Lcom/squareup/cogs/NewIDGenerator;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "(Lcom/squareup/cogs/NewIDGenerator;Ljavax/inject/Provider;)V",
        "initialState",
        "Lcom/squareup/items/editoption/EditOptionState$EditOption;",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "prepareOptionValues",
        "Lkotlin/Pair;",
        "",
        "Lcom/squareup/items/editoption/OptionValue;",
        "",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "render",
        "snapshotState",
        "validate",
        "",
        "Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;",
        "Action",
        "DialogAction",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final idGenerator:Lcom/squareup/cogs/NewIDGenerator;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/NewIDGenerator;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/NewIDGenerator;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "idGenerator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow;->idGenerator:Lcom/squareup/cogs/NewIDGenerator;

    iput-object p2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static final synthetic access$getIdGenerator$p(Lcom/squareup/items/editoption/RealEditOptionWorkflow;)Lcom/squareup/cogs/NewIDGenerator;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow;->idGenerator:Lcom/squareup/cogs/NewIDGenerator;

    return-object p0
.end method

.method public static final synthetic access$getLocaleProvider$p(Lcom/squareup/items/editoption/RealEditOptionWorkflow;)Ljavax/inject/Provider;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow;->localeProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method private final prepareOptionValues(Lcom/squareup/items/editoption/EditOptionState;Lcom/squareup/workflow/RenderContext;)Lkotlin/Pair;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/editoption/EditOptionState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/editoption/EditOptionState;",
            "-",
            "Lcom/squareup/items/editoption/EditOptionOutput;",
            ">;)",
            "Lkotlin/Pair<",
            "Ljava/util/List<",
            "Lcom/squareup/items/editoption/OptionValue;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 189
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v0, Ljava/util/Set;

    .line 190
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v1, Ljava/util/Set;

    .line 191
    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 383
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 384
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const-string v6, "localeProvider.get()"

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 385
    check-cast v5, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 191
    iget-object v7, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    invoke-static {v7, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Ljava/util/Locale;

    invoke-virtual {v5, v7}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->normalizedName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 386
    :cond_0
    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 192
    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 193
    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 194
    :cond_2
    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 195
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 199
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 387
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 388
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 389
    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 200
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getValueId()Ljava/lang/String;

    move-result-object v3

    .line 201
    new-instance v4, Lcom/squareup/items/editoption/OptionValue;

    .line 203
    iget-object v5, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/util/Locale;

    invoke-virtual {v2, v5}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->normalizedName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 205
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getName()Ljava/lang/String;

    move-result-object v2

    .line 206
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "option-value-name-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 207
    new-instance v8, Lcom/squareup/items/editoption/RealEditOptionWorkflow$prepareOptionValues$2$1;

    invoke-direct {v8, v3}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$prepareOptionValues$2$1;-><init>(Ljava/lang/String;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 204
    invoke-static {p2, v2, v7, v8}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v2

    .line 201
    invoke-direct {v4, v3, v2, v5}, Lcom/squareup/items/editoption/OptionValue;-><init>(Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;Z)V

    .line 208
    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 390
    :cond_4
    check-cast v0, Ljava/util/List;

    .line 210
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result p1

    if-lez p1, :cond_5

    const/4 p1, 0x1

    goto :goto_3

    :cond_5
    const/4 p1, 0x0

    :goto_3
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    .line 198
    new-instance p2, Lkotlin/Pair;

    invoke-direct {p2, v0, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p2
.end method

.method private final validate(Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;)V
    .locals 1

    .line 74
    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 77
    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;->getExistingOptionNames()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Expected the ItemOption passed into EditOptionProps.EditExistingOption to be included in the set of existing names"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 74
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Expected an ItemOption with a name in EditOptionProps.EditExistingOption"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/editoption/EditOptionProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/editoption/EditOptionState$EditOption;
    .locals 6

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p2, :cond_4

    .line 356
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v0

    :goto_1
    if-eqz p2, :cond_3

    .line 361
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    const-string v3, "Parcel.obtain()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 362
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 363
    array-length v3, p2

    invoke-virtual {v2, p2, v1, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 364
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 365
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v2, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v3, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 366
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p2, v0

    .line 367
    :goto_2
    check-cast p2, Lcom/squareup/items/editoption/EditOptionState$EditOption;

    if-eqz p2, :cond_4

    goto :goto_7

    .line 53
    :cond_4
    new-instance p2, Lcom/squareup/items/editoption/EditOptionState$EditOption;

    .line 55
    instance-of v2, p1, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;

    if-eqz v2, :cond_7

    move-object v3, p1

    check-cast v3, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;

    invoke-virtual {v3}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v4

    if-eqz v4, :cond_5

    goto :goto_4

    :cond_5
    sget-object v4, Lcom/squareup/cogs/itemoptions/ItemOption;->Companion:Lcom/squareup/cogs/itemoptions/ItemOption$Companion;

    .line 56
    iget-object v5, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow;->idGenerator:Lcom/squareup/cogs/NewIDGenerator;

    invoke-virtual {v5}, Lcom/squareup/cogs/NewIDGenerator;->getId()Ljava/lang/String;

    move-result-object v5

    .line 57
    invoke-virtual {v3}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->getDefaultOptionName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    goto :goto_3

    :cond_6
    const-string v3, ""

    .line 55
    :goto_3
    invoke-virtual {v4, v5, v3}, Lcom/squareup/cogs/itemoptions/ItemOption$Companion;->new(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v4

    goto :goto_4

    .line 59
    :cond_7
    instance-of v3, p1, Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;

    if-eqz v3, :cond_c

    .line 60
    move-object v3, p1

    check-cast v3, Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;

    invoke-direct {p0, v3}, Lcom/squareup/items/editoption/RealEditOptionWorkflow;->validate(Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;)V

    .line 61
    invoke-virtual {v3}, Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v4

    .line 64
    :goto_4
    iget-object v3, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow;->idGenerator:Lcom/squareup/cogs/NewIDGenerator;

    invoke-virtual {v3}, Lcom/squareup/cogs/NewIDGenerator;->getId()Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_a

    .line 66
    check-cast p1, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;

    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_8
    move-object v1, v0

    :goto_5
    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object p1

    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    :cond_9
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_6

    .line 67
    :cond_a
    instance-of p1, p1, Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;

    if-eqz p1, :cond_b

    .line 53
    :goto_6
    invoke-direct {p2, v4, v3, v1}, Lcom/squareup/items/editoption/EditOptionState$EditOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Z)V

    :goto_7
    return-object p2

    .line 67
    :cond_b
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 61
    :cond_c
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/items/editoption/EditOptionProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/editoption/RealEditOptionWorkflow;->initialState(Lcom/squareup/items/editoption/EditOptionProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/editoption/EditOptionState$EditOption;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/items/editoption/EditOptionProps;

    check-cast p2, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/editoption/RealEditOptionWorkflow;->render(Lcom/squareup/items/editoption/EditOptionProps;Lcom/squareup/items/editoption/EditOptionState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/editoption/EditOptionProps;Lcom/squareup/items/editoption/EditOptionState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/editoption/EditOptionProps;",
            "Lcom/squareup/items/editoption/EditOptionState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/editoption/EditOptionState;",
            "-",
            "Lcom/squareup/items/editoption/EditOptionOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    const-string v4, "props"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "state"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "context"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/items/editoption/RealEditOptionWorkflowKt;->access$validForSave(Lcom/squareup/cogs/itemoptions/ItemOption;)Z

    move-result v6

    .line 89
    invoke-direct {v0, v2, v3}, Lcom/squareup/items/editoption/RealEditOptionWorkflow;->prepareOptionValues(Lcom/squareup/items/editoption/EditOptionState;Lcom/squareup/workflow/RenderContext;)Lkotlin/Pair;

    move-result-object v4

    invoke-virtual {v4}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v5

    move-object v9, v5

    check-cast v9, Ljava/util/List;

    invoke-virtual {v4}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 91
    instance-of v5, v2, Lcom/squareup/items/editoption/EditOptionState$EditOption;

    const-string v7, "add-new-value-text"

    const-string v8, "option-display-name-value"

    const-string v10, "option-name-value"

    const/16 v17, 0x0

    const/16 v18, 0x1

    const-string v15, ""

    if-eqz v5, :cond_2

    .line 92
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v5

    .line 93
    new-instance v19, Lcom/squareup/items/editoption/EditOptionScreen;

    .line 96
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v11

    invoke-virtual {v11}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object v11

    .line 98
    sget-object v12, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$1;->INSTANCE:Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$1;

    check-cast v12, Lkotlin/jvm/functions/Function1;

    .line 95
    invoke-static {v3, v11, v10, v12}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v10

    .line 100
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v11

    invoke-virtual {v11}, Lcom/squareup/cogs/itemoptions/ItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v11

    .line 102
    sget-object v12, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$2;->INSTANCE:Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$2;

    check-cast v12, Lkotlin/jvm/functions/Function1;

    .line 99
    invoke-static {v3, v11, v8, v12}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v8

    .line 107
    new-instance v11, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$3;

    invoke-direct {v11, v0, v2}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$3;-><init>(Lcom/squareup/items/editoption/RealEditOptionWorkflow;Lcom/squareup/items/editoption/EditOptionState;)V

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 104
    invoke-static {v3, v15, v7, v11}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v3

    .line 114
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/editoption/EditOptionState;->getAddOptionRowId()Ljava/lang/String;

    move-result-object v11

    .line 116
    instance-of v2, v1, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;

    if-eqz v2, :cond_0

    const/4 v12, 0x1

    goto :goto_0

    .line 117
    :cond_0
    instance-of v2, v1, Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;

    if-eqz v2, :cond_1

    const/4 v12, 0x0

    .line 120
    :goto_0
    new-instance v2, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$4;

    invoke-direct {v2, v5}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$4;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v13, v2

    check-cast v13, Lkotlin/jvm/functions/Function0;

    .line 121
    new-instance v2, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$5;

    invoke-direct {v2, v0, v5, v1, v4}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$5;-><init>(Lcom/squareup/items/editoption/RealEditOptionWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/items/editoption/EditOptionProps;Z)V

    move-object v14, v2

    check-cast v14, Lkotlin/jvm/functions/Function0;

    .line 130
    new-instance v1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$6;

    invoke-direct {v1, v5}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$6;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 131
    new-instance v2, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$7;

    invoke-direct {v2, v5}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$7;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object/from16 v16, v2

    check-cast v16, Lkotlin/jvm/functions/Function2;

    move-object/from16 v5, v19

    move-object v7, v10

    move-object v10, v3

    move-object v4, v15

    move-object v15, v1

    .line 93
    invoke-direct/range {v5 .. v16}, Lcom/squareup/items/editoption/EditOptionScreen;-><init>(ZLcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/lang/String;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V

    move-object/from16 v1, v19

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 369
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 370
    const-class v3, Lcom/squareup/items/editoption/EditOptionScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 371
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 369
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 133
    sget-object v1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {v2, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    return-object v1

    .line 117
    :cond_1
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    :cond_2
    move-object v4, v15

    .line 135
    instance-of v5, v2, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    if-eqz v5, :cond_5

    .line 136
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v15

    .line 137
    new-instance v19, Lcom/squareup/items/editoption/EditOptionScreen;

    .line 140
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object v5

    .line 142
    sget-object v11, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$editOptionScreen$1;->INSTANCE:Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$editOptionScreen$1;

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 139
    invoke-static {v3, v5, v10, v11}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v10

    .line 144
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/cogs/itemoptions/ItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    .line 146
    sget-object v11, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$editOptionScreen$2;->INSTANCE:Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$editOptionScreen$2;

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 143
    invoke-static {v3, v5, v8, v11}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v8

    .line 151
    new-instance v5, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$editOptionScreen$3;

    invoke-direct {v5, v0, v2}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$editOptionScreen$3;-><init>(Lcom/squareup/items/editoption/RealEditOptionWorkflow;Lcom/squareup/items/editoption/EditOptionState;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 148
    invoke-static {v3, v4, v7, v5}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v3

    .line 158
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/editoption/EditOptionState;->getAddOptionRowId()Ljava/lang/String;

    move-result-object v11

    .line 160
    instance-of v5, v1, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;

    if-eqz v5, :cond_3

    const/4 v12, 0x1

    goto :goto_1

    .line 161
    :cond_3
    instance-of v1, v1, Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;

    if-eqz v1, :cond_4

    const/4 v12, 0x0

    .line 164
    :goto_1
    sget-object v1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$editOptionScreen$4;->INSTANCE:Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$editOptionScreen$4;

    move-object v13, v1

    check-cast v13, Lkotlin/jvm/functions/Function0;

    .line 165
    sget-object v1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$editOptionScreen$5;->INSTANCE:Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$editOptionScreen$5;

    move-object v14, v1

    check-cast v14, Lkotlin/jvm/functions/Function0;

    .line 166
    sget-object v1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$editOptionScreen$6;->INSTANCE:Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$editOptionScreen$6;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 167
    sget-object v5, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$editOptionScreen$7;->INSTANCE:Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$editOptionScreen$7;

    move-object/from16 v16, v5

    check-cast v16, Lkotlin/jvm/functions/Function2;

    move-object/from16 v5, v19

    move-object v7, v10

    move-object v10, v3

    move-object v3, v15

    move-object v15, v1

    .line 137
    invoke-direct/range {v5 .. v16}, Lcom/squareup/items/editoption/EditOptionScreen;-><init>(ZLcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/lang/String;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V

    .line 169
    new-instance v1, Lcom/squareup/items/editoption/InvalidOptionScreen;

    .line 170
    check-cast v2, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    .line 171
    new-instance v5, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$invalidOptionScreen$1;

    invoke-direct {v5, v3}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$invalidOptionScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 169
    invoke-direct {v1, v2, v5}, Lcom/squareup/items/editoption/InvalidOptionScreen;-><init>(Lcom/squareup/items/editoption/EditOptionState$InvalidOption;Lkotlin/jvm/functions/Function0;)V

    .line 174
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v3, 0x2

    new-array v3, v3, [Lkotlin/Pair;

    .line 175
    sget-object v5, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    move-object/from16 v6, v19

    check-cast v6, Lcom/squareup/workflow/legacy/V2Screen;

    .line 374
    new-instance v7, Lcom/squareup/workflow/legacy/Screen;

    .line 375
    const-class v8, Lcom/squareup/items/editoption/EditOptionScreen;

    invoke-static {v8}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v8

    invoke-static {v8, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v8

    .line 376
    sget-object v9, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v9}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v9

    .line 374
    invoke-direct {v7, v8, v6, v9}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 175
    new-instance v6, Lkotlin/Pair;

    invoke-direct {v6, v5, v7}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v6, v3, v17

    .line 176
    sget-object v5, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 379
    new-instance v6, Lcom/squareup/workflow/legacy/Screen;

    .line 380
    const-class v7, Lcom/squareup/items/editoption/InvalidOptionScreen;

    invoke-static {v7}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v7

    invoke-static {v7, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 381
    sget-object v7, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v7}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v7

    .line 379
    invoke-direct {v6, v4, v1, v7}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 176
    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, v5, v6}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v3, v18

    .line 174
    invoke-virtual {v2, v3}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    return-object v1

    .line 161
    :cond_4
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    .line 174
    :cond_5
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public snapshotState(Lcom/squareup/items/editoption/EditOptionState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/editoption/RealEditOptionWorkflow;->snapshotState(Lcom/squareup/items/editoption/EditOptionState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
