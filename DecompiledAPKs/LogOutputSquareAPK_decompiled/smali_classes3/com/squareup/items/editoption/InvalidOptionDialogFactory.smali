.class public final Lcom/squareup/items/editoption/InvalidOptionDialogFactory;
.super Ljava/lang/Object;
.source "InvalidOptionDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u0005H\u0002J\u0008\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0010\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0015H\u0002R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/items/editoption/InvalidOptionDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/items/editoption/InvalidOptionScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "Landroid/app/AlertDialog;",
        "screen",
        "getButtonTextId",
        "",
        "getMessagedId",
        "reason",
        "Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;",
        "getTitleId",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/editoption/InvalidOptionDialogFactory;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/items/editoption/InvalidOptionDialogFactory;Landroid/content/Context;Lcom/squareup/items/editoption/InvalidOptionScreen;)Landroid/app/AlertDialog;
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/editoption/InvalidOptionDialogFactory;->createDialog(Landroid/content/Context;Lcom/squareup/items/editoption/InvalidOptionScreen;)Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/items/editoption/InvalidOptionScreen;)Landroid/app/AlertDialog;
    .locals 2

    .line 32
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 33
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setVerticalButtonOrientation()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 34
    sget v0, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 35
    sget v0, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 36
    invoke-direct {p0}, Lcom/squareup/items/editoption/InvalidOptionDialogFactory;->getButtonTextId()I

    move-result v0

    new-instance v1, Lcom/squareup/items/editoption/InvalidOptionDialogFactory$createDialog$1;

    invoke-direct {v1, p2}, Lcom/squareup/items/editoption/InvalidOptionDialogFactory$createDialog$1;-><init>(Lcom/squareup/items/editoption/InvalidOptionScreen;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 39
    invoke-virtual {p2}, Lcom/squareup/items/editoption/InvalidOptionScreen;->getState()Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getReason()Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/items/editoption/InvalidOptionDialogFactory;->getMessagedId(Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 40
    invoke-virtual {p2}, Lcom/squareup/items/editoption/InvalidOptionScreen;->getState()Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getReason()Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/items/editoption/InvalidOptionDialogFactory;->getTitleId(Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 p2, 0x0

    .line 41
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 42
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026                .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getButtonTextId()I
    .locals 1

    .line 45
    sget v0, Lcom/squareup/common/strings/R$string;->ok:I

    return v0
.end method

.method private final getMessagedId(Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)I
    .locals 1

    .line 47
    sget-object v0, Lcom/squareup/items/editoption/InvalidOptionDialogFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 50
    sget p1, Lcom/squareup/items/editoption/impl/R$string;->edit_option_invalid_option_empty_value_name_message:I

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 49
    :cond_1
    sget p1, Lcom/squareup/items/editoption/impl/R$string;->edit_option_invalid_option_duplicate_value_name_message:I

    goto :goto_0

    .line 48
    :cond_2
    sget p1, Lcom/squareup/items/editoption/impl/R$string;->edit_option_invalid_option_duplicate_option_name_message:I

    :goto_0
    return p1
.end method

.method private final getTitleId(Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)I
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/items/editoption/InvalidOptionDialogFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 56
    sget p1, Lcom/squareup/items/editoption/impl/R$string;->edit_option_invalid_option_empty_value_name_title:I

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 55
    :cond_1
    sget p1, Lcom/squareup/items/editoption/impl/R$string;->edit_option_invalid_option_duplicate_value_name_title:I

    goto :goto_0

    .line 54
    :cond_2
    sget p1, Lcom/squareup/items/editoption/impl/R$string;->edit_option_invalid_option_duplicate_option_name_title:I

    :goto_0
    return p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/items/editoption/InvalidOptionDialogFactory;->screens:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 23
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 25
    new-instance v1, Lcom/squareup/items/editoption/InvalidOptionDialogFactory$create$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/items/editoption/InvalidOptionDialogFactory$create$1;-><init>(Lcom/squareup/items/editoption/InvalidOptionDialogFactory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screens\n                \u2026 screen.unwrapV2Screen) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
