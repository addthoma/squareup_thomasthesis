.class public final Lcom/squareup/items/editoption/impl/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final edit_option_value_color_picker_black:I = 0x7f0600c3

.field public static final edit_option_value_color_picker_blue:I = 0x7f0600c4

.field public static final edit_option_value_color_picker_brown:I = 0x7f0600c5

.field public static final edit_option_value_color_picker_gold:I = 0x7f0600c6

.field public static final edit_option_value_color_picker_green:I = 0x7f0600c7

.field public static final edit_option_value_color_picker_indigo:I = 0x7f0600c8

.field public static final edit_option_value_color_picker_lime:I = 0x7f0600c9

.field public static final edit_option_value_color_picker_maroon:I = 0x7f0600ca

.field public static final edit_option_value_color_picker_orange:I = 0x7f0600cb

.field public static final edit_option_value_color_picker_pink:I = 0x7f0600cc

.field public static final edit_option_value_color_picker_plum:I = 0x7f0600cd

.field public static final edit_option_value_color_picker_purple:I = 0x7f0600ce

.field public static final edit_option_value_color_picker_teal:I = 0x7f0600cf

.field public static final edit_option_value_color_picker_white:I = 0x7f0600d0


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
