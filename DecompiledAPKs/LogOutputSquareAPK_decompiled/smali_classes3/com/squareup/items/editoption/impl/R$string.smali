.class public final Lcom/squareup/items/editoption/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final edit_option_create_screen_title:I = 0x7f120937

.field public static final edit_option_display_name_section_hint:I = 0x7f120938

.field public static final edit_option_display_name_section_title:I = 0x7f120939

.field public static final edit_option_edit_screen_title:I = 0x7f12093a

.field public static final edit_option_invalid_option_duplicate_option_name_message:I = 0x7f12093b

.field public static final edit_option_invalid_option_duplicate_option_name_title:I = 0x7f12093c

.field public static final edit_option_invalid_option_duplicate_value_name_message:I = 0x7f12093d

.field public static final edit_option_invalid_option_duplicate_value_name_title:I = 0x7f12093e

.field public static final edit_option_invalid_option_empty_value_name_message:I = 0x7f12093f

.field public static final edit_option_invalid_option_empty_value_name_title:I = 0x7f120940

.field public static final edit_option_name_section_hint:I = 0x7f120941

.field public static final edit_option_name_section_title:I = 0x7f120942

.field public static final edit_option_new_value_hint:I = 0x7f120943

.field public static final edit_option_option_list_section_header:I = 0x7f120944

.field public static final edit_option_value_color_field_label:I = 0x7f120945

.field public static final edit_option_value_screen_delete_button_text:I = 0x7f120946

.field public static final edit_option_value_screen_title:I = 0x7f120947

.field public static final edit_option_values_section_header:I = 0x7f120948


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
