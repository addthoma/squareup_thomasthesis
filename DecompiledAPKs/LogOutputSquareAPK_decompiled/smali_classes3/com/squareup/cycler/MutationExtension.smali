.class final Lcom/squareup/cycler/MutationExtension;
.super Ljava/lang/Object;
.source "RecyclerMutations.kt"

# interfaces
.implements Lcom/squareup/cycler/Extension;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/cycler/Extension<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0002\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003:\u0001\u001bB\u0013\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0015\u001a\u00020\u00162\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0010H\u0016J\u0015\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u0019H\u0000\u00a2\u0006\u0002\u0008\u001aR\u0018\u0010\u0007\u001a\u000c0\u0008R\u0008\u0012\u0004\u0012\u00028\u00000\u0000X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\t\u001a\u0008\u0012\u0004\u0012\u00028\u00000\nX\u0096.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/cycler/MutationExtension;",
        "T",
        "",
        "Lcom/squareup/cycler/Extension;",
        "spec",
        "Lcom/squareup/cycler/MutationExtensionSpec;",
        "(Lcom/squareup/cycler/MutationExtensionSpec;)V",
        "callback",
        "Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;",
        "data",
        "Lcom/squareup/cycler/RecyclerData;",
        "getData",
        "()Lcom/squareup/cycler/RecyclerData;",
        "setData",
        "(Lcom/squareup/cycler/RecyclerData;)V",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "getSpec",
        "()Lcom/squareup/cycler/MutationExtensionSpec;",
        "touchHelper",
        "Landroidx/recyclerview/widget/ItemTouchHelper;",
        "attach",
        "",
        "setUpDragHandle",
        "view",
        "Landroid/view/View;",
        "setUpDragHandle$lib_release",
        "MyItemTouchCallback",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private final callback:Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/MutationExtension<",
            "TT;>.MyItemTouchCallback;"
        }
    .end annotation
.end field

.field public data:Lcom/squareup/cycler/RecyclerData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/RecyclerData<",
            "TT;>;"
        }
    .end annotation
.end field

.field private recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final spec:Lcom/squareup/cycler/MutationExtensionSpec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/MutationExtensionSpec<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final touchHelper:Landroidx/recyclerview/widget/ItemTouchHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/MutationExtensionSpec;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/MutationExtensionSpec<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "spec"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cycler/MutationExtension;->spec:Lcom/squareup/cycler/MutationExtensionSpec;

    .line 117
    new-instance p1, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;

    invoke-direct {p1, p0}, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;-><init>(Lcom/squareup/cycler/MutationExtension;)V

    iput-object p1, p0, Lcom/squareup/cycler/MutationExtension;->callback:Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;

    .line 118
    new-instance p1, Landroidx/recyclerview/widget/ItemTouchHelper;

    iget-object v0, p0, Lcom/squareup/cycler/MutationExtension;->callback:Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;

    check-cast v0, Landroidx/recyclerview/widget/ItemTouchHelper$Callback;

    invoke-direct {p1, v0}, Landroidx/recyclerview/widget/ItemTouchHelper;-><init>(Landroidx/recyclerview/widget/ItemTouchHelper$Callback;)V

    iput-object p1, p0, Lcom/squareup/cycler/MutationExtension;->touchHelper:Landroidx/recyclerview/widget/ItemTouchHelper;

    return-void
.end method

.method public static final synthetic access$getRecycler$p(Lcom/squareup/cycler/MutationExtension;)Lcom/squareup/cycler/Recycler;
    .locals 1

    .line 114
    iget-object p0, p0, Lcom/squareup/cycler/MutationExtension;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez p0, :cond_0

    const-string v0, "recycler"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getTouchHelper$p(Lcom/squareup/cycler/MutationExtension;)Landroidx/recyclerview/widget/ItemTouchHelper;
    .locals 0

    .line 114
    iget-object p0, p0, Lcom/squareup/cycler/MutationExtension;->touchHelper:Landroidx/recyclerview/widget/ItemTouchHelper;

    return-object p0
.end method

.method public static final synthetic access$setRecycler$p(Lcom/squareup/cycler/MutationExtension;Lcom/squareup/cycler/Recycler;)V
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/cycler/MutationExtension;->recycler:Lcom/squareup/cycler/Recycler;

    return-void
.end method


# virtual methods
.method public attach(Lcom/squareup/cycler/Recycler;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    iput-object p1, p0, Lcom/squareup/cycler/MutationExtension;->recycler:Lcom/squareup/cycler/Recycler;

    .line 122
    iget-object p1, p0, Lcom/squareup/cycler/MutationExtension;->touchHelper:Landroidx/recyclerview/widget/ItemTouchHelper;

    iget-object v1, p0, Lcom/squareup/cycler/MutationExtension;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez v1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/ItemTouchHelper;->attachToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 123
    iget-object p1, p0, Lcom/squareup/cycler/MutationExtension;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cycler/MutationExtension;->touchHelper:Landroidx/recyclerview/widget/ItemTouchHelper;

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void
.end method

.method public getData()Lcom/squareup/cycler/RecyclerData;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/RecyclerData<",
            "TT;>;"
        }
    .end annotation

    .line 126
    iget-object v0, p0, Lcom/squareup/cycler/MutationExtension;->data:Lcom/squareup/cycler/RecyclerData;

    if-nez v0, :cond_0

    const-string v1, "data"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getSpec()Lcom/squareup/cycler/MutationExtensionSpec;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/MutationExtensionSpec<",
            "TT;>;"
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/squareup/cycler/MutationExtension;->spec:Lcom/squareup/cycler/MutationExtensionSpec;

    return-object v0
.end method

.method public setData(Lcom/squareup/cycler/RecyclerData;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/RecyclerData<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    iput-object p1, p0, Lcom/squareup/cycler/MutationExtension;->data:Lcom/squareup/cycler/RecyclerData;

    return-void
.end method

.method public final setUpDragHandle$lib_release(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    new-instance v0, Lcom/squareup/cycler/MutationExtension$setUpDragHandle$1;

    invoke-direct {v0, p0}, Lcom/squareup/cycler/MutationExtension$setUpDragHandle$1;-><init>(Lcom/squareup/cycler/MutationExtension;)V

    check-cast v0, Landroid/view/View$OnTouchListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method
