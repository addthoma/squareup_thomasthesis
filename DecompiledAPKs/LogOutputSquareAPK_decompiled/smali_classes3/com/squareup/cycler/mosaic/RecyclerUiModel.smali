.class public final Lcom/squareup/cycler/mosaic/RecyclerUiModel;
.super Lcom/squareup/mosaic/core/StandardUiModel;
.source "RecyclerUiModel.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "I:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/mosaic/core/StandardUiModel<",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "TP;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerUiModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerUiModel.kt\ncom/squareup/cycler/mosaic/RecyclerUiModel\n*L\n1#1,77:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0017\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u00010\u0004BJ\u0012\u0006\u0010\u0006\u001a\u00028\u0000\u0012\u001f\u0008\u0002\u0010\u0007\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\t\u0012\u0004\u0012\u00020\n0\u0008\u00a2\u0006\u0002\u0008\u000b\u0012\u000e\u0008\u0002\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00028\u00010\r\u0012\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u00a2\u0006\u0002\u0010\u000fJ\u000e\u0010\u001e\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0019J \u0010\u001f\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\t\u0012\u0004\u0012\u00020\n0\u0008\u00a2\u0006\u0002\u0008\u000bH\u00c6\u0003J\u000f\u0010 \u001a\u0008\u0012\u0004\u0012\u00028\u00010\rH\u00c6\u0003J\u000b\u0010!\u001a\u0004\u0018\u00010\u0002H\u00c6\u0003Ja\u0010\"\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0008\u0008\u0002\u0010\u0006\u001a\u00028\u00002\u001f\u0008\u0002\u0010\u0007\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\t\u0012\u0004\u0012\u00020\n0\u0008\u00a2\u0006\u0002\u0008\u000b2\u000e\u0008\u0002\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00028\u00010\r2\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0002H\u00c6\u0001\u00a2\u0006\u0002\u0010#J\u0018\u0010$\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030%2\u0006\u0010&\u001a\u00020\'H\u0016J\u0013\u0010(\u001a\u00020)2\u0008\u0010*\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010+\u001a\u00020,H\u00d6\u0001J\t\u0010-\u001a\u00020.H\u00d6\u0001R1\u0010\u0007\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\t\u0012\u0004\u0012\u00020\n0\u0008\u00a2\u0006\u0002\u0008\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R \u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00028\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\"\u0004\u0008\u0016\u0010\u0017R\u001c\u0010\u000e\u001a\u0004\u0018\u00010\u0002X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\"\u0004\u0008\u001a\u0010\u001bR\u0016\u0010\u0006\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u001d\u001a\u0004\u0008\u001c\u0010\u0019\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/cycler/mosaic/RecyclerUiModel;",
        "P",
        "",
        "I",
        "Lcom/squareup/mosaic/core/StandardUiModel;",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "params",
        "config",
        "Lkotlin/Function1;",
        "Lcom/squareup/cycler/Recycler$Config;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "data",
        "Lcom/squareup/cycler/DataSource;",
        "extraItem",
        "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lcom/squareup/cycler/DataSource;Ljava/lang/Object;)V",
        "getConfig",
        "()Lkotlin/jvm/functions/Function1;",
        "setConfig",
        "(Lkotlin/jvm/functions/Function1;)V",
        "getData",
        "()Lcom/squareup/cycler/DataSource;",
        "setData",
        "(Lcom/squareup/cycler/DataSource;)V",
        "getExtraItem",
        "()Ljava/lang/Object;",
        "setExtraItem",
        "(Ljava/lang/Object;)V",
        "getParams",
        "Ljava/lang/Object;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lcom/squareup/cycler/DataSource;Ljava/lang/Object;)Lcom/squareup/cycler/mosaic/RecyclerUiModel;",
        "createViewRef",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "context",
        "Landroid/content/Context;",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "recycler-mosaic_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private config:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private data:Lcom/squareup/cycler/DataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/DataSource<",
            "+TI;>;"
        }
    .end annotation
.end field

.field private extraItem:Ljava/lang/Object;

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lcom/squareup/cycler/DataSource;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/cycler/DataSource<",
            "+TI;>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "config"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-direct {p0}, Lcom/squareup/mosaic/core/StandardUiModel;-><init>()V

    iput-object p1, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->params:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->config:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->data:Lcom/squareup/cycler/DataSource;

    iput-object p4, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->extraItem:Ljava/lang/Object;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lcom/squareup/cycler/DataSource;Ljava/lang/Object;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    .line 70
    sget-object p2, Lcom/squareup/cycler/mosaic/RecyclerUiModel$1;->INSTANCE:Lcom/squareup/cycler/mosaic/RecyclerUiModel$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    .line 71
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p3

    invoke-static {p3}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object p3

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    const/4 p4, 0x0

    .line 72
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;-><init>(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lcom/squareup/cycler/DataSource;Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cycler/mosaic/RecyclerUiModel;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lcom/squareup/cycler/DataSource;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/cycler/mosaic/RecyclerUiModel;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->config:Lkotlin/jvm/functions/Function1;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->data:Lcom/squareup/cycler/DataSource;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->extraItem:Ljava/lang/Object;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->copy(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lcom/squareup/cycler/DataSource;Ljava/lang/Object;)Lcom/squareup/cycler/mosaic/RecyclerUiModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->config:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component3()Lcom/squareup/cycler/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/DataSource<",
            "TI;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->data:Lcom/squareup/cycler/DataSource;

    return-object v0
.end method

.method public final component4()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->extraItem:Ljava/lang/Object;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lcom/squareup/cycler/DataSource;Ljava/lang/Object;)Lcom/squareup/cycler/mosaic/RecyclerUiModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/cycler/DataSource<",
            "+TI;>;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/squareup/cycler/mosaic/RecyclerUiModel<",
            "TP;TI;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "config"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;-><init>(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lcom/squareup/cycler/DataSource;Ljava/lang/Object;)V

    return-object v0
.end method

.method public createViewRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    new-instance v0, Lcom/squareup/cycler/mosaic/RecyclerViewRef;

    invoke-direct {v0, p1}, Lcom/squareup/cycler/mosaic/RecyclerViewRef;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/squareup/mosaic/core/ViewRef;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cycler/mosaic/RecyclerUiModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cycler/mosaic/RecyclerUiModel;

    invoke-virtual {p0}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->config:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->config:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->data:Lcom/squareup/cycler/DataSource;

    iget-object v1, p1, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->data:Lcom/squareup/cycler/DataSource;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->extraItem:Ljava/lang/Object;

    iget-object p1, p1, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->extraItem:Ljava/lang/Object;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getConfig()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->config:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getData()Lcom/squareup/cycler/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/DataSource<",
            "TI;>;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->data:Lcom/squareup/cycler/DataSource;

    return-object v0
.end method

.method public final getExtraItem()Ljava/lang/Object;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->extraItem:Ljava/lang/Object;

    return-object v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->config:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->data:Lcom/squareup/cycler/DataSource;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->extraItem:Ljava/lang/Object;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final setConfig(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iput-object p1, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->config:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setData(Lcom/squareup/cycler/DataSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/DataSource<",
            "+TI;>;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iput-object p1, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->data:Lcom/squareup/cycler/DataSource;

    return-void
.end method

.method public final setExtraItem(Ljava/lang/Object;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->extraItem:Ljava/lang/Object;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RecyclerUiModel(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->config:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->data:Lcom/squareup/cycler/DataSource;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", extraItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->extraItem:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
