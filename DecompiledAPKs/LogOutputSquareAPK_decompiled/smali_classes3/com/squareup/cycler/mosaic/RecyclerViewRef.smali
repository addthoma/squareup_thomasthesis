.class public final Lcom/squareup/cycler/mosaic/RecyclerViewRef;
.super Lcom/squareup/mosaic/core/StandardViewRef;
.source "RecyclerViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/mosaic/core/StandardViewRef<",
        "Lcom/squareup/cycler/mosaic/RecyclerUiModel<",
        "*TI;>;",
        "Landroidx/recyclerview/widget/RecyclerView;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerViewRef.kt\ncom/squareup/cycler/mosaic/RecyclerViewRef\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n*L\n1#1,54:1\n49#2:55\n50#2,4:61\n599#3,4:56\n601#3:60\n*E\n*S KotlinDebug\n*F\n+ 1 RecyclerViewRef.kt\ncom/squareup/cycler/mosaic/RecyclerViewRef\n*L\n28#1:55\n28#1,4:61\n28#1,4:56\n28#1:60\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0018\u0012\u000e\u0012\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u0002H\u00010\u0004\u0012\u0004\u0012\u00020\u00050\u0003B\r\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J,\u0010\u000b\u001a\u00020\u000c2\u0010\u0010\r\u001a\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00028\u00000\u00042\u0010\u0010\u000e\u001a\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00028\u00000\u0004H\u0014J\"\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0010\u0010\u0010\u001a\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00028\u00000\u0004H\u0016J.\u0010\u0011\u001a\u00020\u00122\u0012\u0010\u0013\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u00042\u0010\u0010\u000e\u001a\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00028\u00000\u0004H\u0016R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00028\u00000\nX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/cycler/mosaic/RecyclerViewRef;",
        "I",
        "",
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "Lcom/squareup/cycler/mosaic/RecyclerUiModel;",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "canUpdateTo",
        "",
        "currentModel",
        "newModel",
        "createView",
        "model",
        "doBind",
        "",
        "oldModel",
        "recycler-mosaic_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "TI;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/core/StandardViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected canUpdateTo(Lcom/squareup/cycler/mosaic/RecyclerUiModel;Lcom/squareup/cycler/mosaic/RecyclerUiModel;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/mosaic/RecyclerUiModel<",
            "*TI;>;",
            "Lcom/squareup/cycler/mosaic/RecyclerUiModel<",
            "*TI;>;)Z"
        }
    .end annotation

    const-string v0, "currentModel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p1}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getConfig()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getConfig()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic canUpdateTo(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)Z
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/cycler/mosaic/RecyclerUiModel;

    check-cast p2, Lcom/squareup/cycler/mosaic/RecyclerUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cycler/mosaic/RecyclerViewRef;->canUpdateTo(Lcom/squareup/cycler/mosaic/RecyclerUiModel;Lcom/squareup/cycler/mosaic/RecyclerUiModel;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic canUpdateTo(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)Z
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/cycler/mosaic/RecyclerUiModel;

    check-cast p2, Lcom/squareup/cycler/mosaic/RecyclerUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cycler/mosaic/RecyclerViewRef;->canUpdateTo(Lcom/squareup/cycler/mosaic/RecyclerUiModel;Lcom/squareup/cycler/mosaic/RecyclerUiModel;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .locals 0

    .line 12
    check-cast p2, Lcom/squareup/cycler/mosaic/RecyclerUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cycler/mosaic/RecyclerViewRef;->createView(Landroid/content/Context;Lcom/squareup/cycler/mosaic/RecyclerUiModel;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public createView(Landroid/content/Context;Lcom/squareup/cycler/mosaic/RecyclerUiModel;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/cycler/mosaic/RecyclerUiModel<",
            "*TI;>;)",
            "Landroidx/recyclerview/widget/RecyclerView;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance p2, Landroidx/recyclerview/widget/RecyclerView;

    invoke-direct {p2, p1}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 22
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v0, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    return-object p2
.end method

.method public doBind(Lcom/squareup/cycler/mosaic/RecyclerUiModel;Lcom/squareup/cycler/mosaic/RecyclerUiModel;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/mosaic/RecyclerUiModel<",
            "*TI;>;",
            "Lcom/squareup/cycler/mosaic/RecyclerUiModel<",
            "*TI;>;)V"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    move-object v0, p1

    check-cast v0, Lcom/squareup/mosaic/core/StandardUiModel;

    move-object v1, p2

    check-cast v1, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-super {p0, v0, v1}, Lcom/squareup/mosaic/core/StandardViewRef;->doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V

    .line 27
    invoke-virtual {p2}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getConfig()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getConfig()Lkotlin/jvm/functions/Function1;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 28
    invoke-static {}, Lcom/squareup/cycler/mosaic/RecyclerViewRefKt;->getRecyclerFactory()Lcom/squareup/recycler/RecyclerFactory;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/cycler/mosaic/RecyclerViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p2}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getConfig()Lkotlin/jvm/functions/Function1;

    move-result-object v3

    .line 55
    sget-object v4, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 56
    invoke-virtual {v2}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 57
    new-instance v4, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v4}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 61
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 62
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 63
    invoke-interface {v3, v4}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    invoke-virtual {v4, v2}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cycler/mosaic/RecyclerViewRef;->recycler:Lcom/squareup/cycler/Recycler;

    goto :goto_1

    .line 56
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_2
    :goto_1
    if-eqz p1, :cond_3

    .line 30
    invoke-virtual {p1}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v1

    :cond_3
    invoke-virtual {p2}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getExtraItem()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/cycler/mosaic/RecyclerUiModel;->getExtraItem()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_6

    .line 33
    :cond_4
    iget-object p1, p0, Lcom/squareup/cycler/mosaic/RecyclerViewRef;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez p1, :cond_5

    const-string v0, "recycler"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v0, Lcom/squareup/cycler/mosaic/RecyclerViewRef$doBind$1;

    invoke-direct {v0, p2}, Lcom/squareup/cycler/mosaic/RecyclerViewRef$doBind$1;-><init>(Lcom/squareup/cycler/mosaic/RecyclerUiModel;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    :cond_6
    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/cycler/mosaic/RecyclerUiModel;

    check-cast p2, Lcom/squareup/cycler/mosaic/RecyclerUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cycler/mosaic/RecyclerViewRef;->doBind(Lcom/squareup/cycler/mosaic/RecyclerUiModel;Lcom/squareup/cycler/mosaic/RecyclerUiModel;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/cycler/mosaic/RecyclerUiModel;

    check-cast p2, Lcom/squareup/cycler/mosaic/RecyclerUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cycler/mosaic/RecyclerViewRef;->doBind(Lcom/squareup/cycler/mosaic/RecyclerUiModel;Lcom/squareup/cycler/mosaic/RecyclerUiModel;)V

    return-void
.end method
