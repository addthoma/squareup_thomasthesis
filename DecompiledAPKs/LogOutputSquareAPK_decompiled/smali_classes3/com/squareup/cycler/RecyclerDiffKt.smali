.class public final Lcom/squareup/cycler/RecyclerDiffKt;
.super Ljava/lang/Object;
.source "RecyclerDiff.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000f\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u001c\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u000e\u0008\u0000\u0010\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u0003\u001a*\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0005\"\u0004\u0008\u0000\u0010\u00022\u0016\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u0002H\u00020\u0001j\u0008\u0012\u0004\u0012\u0002H\u0002`\u0006\u001a\u001c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0005\"\u000e\u0008\u0000\u0010\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u0003\u00a8\u0006\u0008"
    }
    d2 = {
        "comparator",
        "Ljava/util/Comparator;",
        "T",
        "",
        "itemComparatorFor",
        "Lcom/squareup/cycler/ComparatorItemComparator;",
        "Lkotlin/Comparator;",
        "naturalItemComparator",
        "lib_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# direct methods
.method public static final comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable<",
            "-TT;>;>()",
            "Ljava/util/Comparator<",
            "TT;>;"
        }
    .end annotation

    .line 44
    sget-object v0, Lcom/squareup/cycler/RecyclerDiffKt$comparator$1;->INSTANCE:Lcom/squareup/cycler/RecyclerDiffKt$comparator$1;

    check-cast v0, Ljava/util/Comparator;

    return-object v0
.end method

.method public static final itemComparatorFor(Ljava/util/Comparator;)Lcom/squareup/cycler/ComparatorItemComparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator<",
            "TT;>;)",
            "Lcom/squareup/cycler/ComparatorItemComparator<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "comparator"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/squareup/cycler/ComparatorItemComparator;

    invoke-direct {v0, p0}, Lcom/squareup/cycler/ComparatorItemComparator;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method public static final naturalItemComparator()Lcom/squareup/cycler/ComparatorItemComparator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable<",
            "-TT;>;>()",
            "Lcom/squareup/cycler/ComparatorItemComparator<",
            "TT;>;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/cycler/ComparatorItemComparator;

    invoke-static {}, Lcom/squareup/cycler/RecyclerDiffKt;->comparator()Ljava/util/Comparator;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/cycler/ComparatorItemComparator;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method
