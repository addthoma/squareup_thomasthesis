.class public interface abstract Lcom/squareup/cycler/Extension;
.super Ljava/lang/Object;
.source "Extension.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ItemType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0002J\u0016\u0010\t\u001a\u00020\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000cH&R\u001e\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0004X\u00a6\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/cycler/Extension;",
        "ItemType",
        "",
        "data",
        "Lcom/squareup/cycler/RecyclerData;",
        "getData",
        "()Lcom/squareup/cycler/RecyclerData;",
        "setData",
        "(Lcom/squareup/cycler/RecyclerData;)V",
        "attach",
        "",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# virtual methods
.method public abstract attach(Lcom/squareup/cycler/Recycler;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler<",
            "TItemType;>;)V"
        }
    .end annotation
.end method

.method public abstract getData()Lcom/squareup/cycler/RecyclerData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/RecyclerData<",
            "TItemType;>;"
        }
    .end annotation
.end method

.method public abstract setData(Lcom/squareup/cycler/RecyclerData;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/RecyclerData<",
            "TItemType;>;)V"
        }
    .end annotation
.end method
