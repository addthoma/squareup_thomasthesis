.class public interface abstract Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;
.super Ljava/lang/Object;
.source "OrderEntryScreen.java"

# interfaces
.implements Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/orderentry/OrderEntryScreen$PhoneModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PhoneComponent"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/orderentry/CategoryDropDownView;)V
.end method

.method public abstract inject(Lcom/squareup/orderentry/LibraryBarPhone;)V
.end method

.method public abstract inject(Lcom/squareup/orderentry/PaymentPadPhoneView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/cart/header/CartHeaderPhoneView;)V
.end method
