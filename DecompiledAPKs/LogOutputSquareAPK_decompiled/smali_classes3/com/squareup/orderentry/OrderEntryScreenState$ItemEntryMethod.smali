.class public final enum Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;
.super Ljava/lang/Enum;
.source "OrderEntryScreenState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryScreenState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ItemEntryMethod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

.field public static final enum CLICKED:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

.field public static final enum LONG_CLICKED:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

.field public static final enum SCANNED:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 440
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    const/4 v1, 0x0

    const-string v2, "CLICKED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;->CLICKED:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    .line 444
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    const/4 v2, 0x1

    const-string v3, "LONG_CLICKED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;->LONG_CLICKED:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    .line 448
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    const/4 v3, 0x2

    const-string v4, "SCANNED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;->SCANNED:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    .line 436
    sget-object v4, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;->CLICKED:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;->LONG_CLICKED:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;->SCANNED:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;->$VALUES:[Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 436
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;
    .locals 1

    .line 436
    const-class v0, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;
    .locals 1

    .line 436
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;->$VALUES:[Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    invoke-virtual {v0}, [Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    return-object v0
.end method
