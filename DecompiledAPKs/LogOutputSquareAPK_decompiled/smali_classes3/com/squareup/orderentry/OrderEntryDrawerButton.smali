.class public Lcom/squareup/orderentry/OrderEntryDrawerButton;
.super Lcom/squareup/widgets/CenteredFrameLayout;
.source "OrderEntryDrawerButton.java"

# interfaces
.implements Lcom/squareup/marin/widgets/Badgeable;


# instance fields
.field private badge:Lcom/squareup/widgets/MarinBadgeView;

.field badgePresenter:Lcom/squareup/applet/BadgePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field private normalBackground:Landroid/graphics/drawable/Drawable;

.field private normalGlyphBackground:Landroid/graphics/drawable/Drawable;

.field private normalGlyphColor:I

.field presenter:Lcom/squareup/applet/AppletsDrawerPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 40
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/CenteredFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    const-class v0, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-interface {v0, p0}, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;->inject(Lcom/squareup/orderentry/OrderEntryDrawerButton;)V

    .line 43
    sget-object v0, Lcom/squareup/orderentry/R$styleable;->OrderEntryDrawerButton:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 44
    sget v0, Lcom/squareup/orderentry/R$styleable;->OrderEntryDrawerButton_customLayout:I

    sget v1, Lcom/squareup/orderentry/R$layout;->home_drawer_button:I

    .line 45
    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 47
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 48
    invoke-static {p1, v0, p0}, Lcom/squareup/orderentry/OrderEntryDrawerButton;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 112
    sget v0, Lcom/squareup/orderentry/R$id;->badge_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MarinBadgeView;

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->badge:Lcom/squareup/widgets/MarinBadgeView;

    .line 113
    sget v0, Lcom/squareup/orderentry/R$id;->applets_drawer_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    return-void
.end method


# virtual methods
.method public hideBadge()V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->badge:Lcom/squareup/widgets/MarinBadgeView;

    invoke-virtual {v0}, Lcom/squareup/widgets/MarinBadgeView;->hideBadge()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 64
    invoke-super {p0}, Lcom/squareup/widgets/CenteredFrameLayout;->onAttachedToWindow()V

    .line 65
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/applet/BadgePresenter;->takeView(Ljava/lang/Object;)V

    .line 67
    new-instance v0, Lcom/squareup/orderentry/OrderEntryDrawerButton$1;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/OrderEntryDrawerButton$1;-><init>(Lcom/squareup/orderentry/OrderEntryDrawerButton;)V

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/OrderEntryDrawerButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/applet/BadgePresenter;->dropView(Lcom/squareup/marin/widgets/Badgeable;)V

    .line 76
    invoke-super {p0}, Lcom/squareup/widgets/CenteredFrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 52
    invoke-super {p0}, Lcom/squareup/widgets/CenteredFrameLayout;->onFinishInflate()V

    .line 53
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryDrawerButton;->bindViews()V

    .line 54
    invoke-static {p0}, Lcom/squareup/widgets/CheatSheet;->setup(Landroid/view/View;)V

    .line 58
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryDrawerButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->normalBackground:Landroid/graphics/drawable/Drawable;

    .line 59
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->normalGlyphBackground:Landroid/graphics/drawable/Drawable;

    .line 60
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphView;->getGlyphColor()I

    move-result v0

    iput v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->normalGlyphColor:I

    return-void
.end method

.method public setShowingThroughScrim(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 101
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    sget v0, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColorRes(I)V

    .line 102
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_scrim:I

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundResource(I)V

    .line 103
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryDrawerButton;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$color;->marin_screen_scrim:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryDrawerButton;->setBackgroundColor(I)V

    goto :goto_0

    .line 105
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    iget v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->normalGlyphColor:I

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    .line 106
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->normalGlyphBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 107
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->normalBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryDrawerButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method

.method public showBadge(Ljava/lang/CharSequence;)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->badge:Lcom/squareup/widgets/MarinBadgeView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MarinBadgeView;->showBadge(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showFatalPriorityBadge()V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->badge:Lcom/squareup/widgets/MarinBadgeView;

    invoke-virtual {v0}, Lcom/squareup/widgets/MarinBadgeView;->showFatalPriorityBadge()V

    return-void
.end method

.method public showHighPriorityBadge(Ljava/lang/CharSequence;)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryDrawerButton;->badge:Lcom/squareup/widgets/MarinBadgeView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MarinBadgeView;->showHighPriorityBadge(Ljava/lang/CharSequence;)V

    return-void
.end method
