.class public abstract Lcom/squareup/orderentry/PaymentPadPresenter;
.super Lmortar/ViewPresenter;
.source "PaymentPadPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/orderentry/PaymentPadView;",
        ">",
        "Lmortar/ViewPresenter<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field protected final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 25
    iput-object p2, p0, Lcom/squareup/orderentry/PaymentPadPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method

.method private updateMode(Lcom/squareup/orderentry/PaymentPadView;Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Z)V
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne p2, v0, :cond_0

    .line 66
    invoke-virtual {p1, p3}, Lcom/squareup/orderentry/PaymentPadView;->showEditMode(Z)V

    goto :goto_0

    .line 68
    :cond_0
    invoke-virtual {p1, p3}, Lcom/squareup/orderentry/PaymentPadView;->showSaleMode(Z)V

    :goto_0
    return-void
.end method


# virtual methods
.method endAnimation()V
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->setInteractionModeAnimating(Z)V

    return-void
.end method

.method isAnimating()Z
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->isInteractionModeAnimating()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onEnterScope$0$PaymentPadPresenter(Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 34
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/PaymentPadView;

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/orderentry/PaymentPadPresenter;->updateMode(Lcom/squareup/orderentry/PaymentPadView;Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Z)V

    :cond_0
    return-void
.end method

.method abstract onCurrentSaleClicked()V
.end method

.method onDoneEditingClicked()V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne v0, v1, :cond_0

    .line 51
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->endEditing()V

    .line 52
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Favorites Grid Done Editing Tapped"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 32
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$PaymentPadPresenter$jGyYQjdntr8TMU342VF2yZsnKF8;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$PaymentPadPresenter$jGyYQjdntr8TMU342VF2yZsnKF8;-><init>(Lcom/squareup/orderentry/PaymentPadPresenter;)V

    .line 33
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 31
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 41
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/PaymentPadView;

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/orderentry/PaymentPadPresenter;->updateMode(Lcom/squareup/orderentry/PaymentPadView;Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Z)V

    return-void
.end method

.method startAnimation()V
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->setInteractionModeAnimating(Z)V

    return-void
.end method
