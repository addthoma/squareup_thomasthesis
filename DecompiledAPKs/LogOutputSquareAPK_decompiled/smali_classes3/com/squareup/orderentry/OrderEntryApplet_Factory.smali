.class public final Lcom/squareup/orderentry/OrderEntryApplet_Factory;
.super Ljava/lang/Object;
.source "OrderEntryApplet_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/OrderEntryApplet;",
        ">;"
    }
.end annotation


# instance fields
.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/orderentry/OrderEntryApplet_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/orderentry/OrderEntryApplet_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/orderentry/OrderEntryApplet_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/OrderEntryApplet_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ldagger/Lazy;)Lcom/squareup/orderentry/OrderEntryApplet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/orderentry/OrderEntryApplet;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/orderentry/OrderEntryApplet;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/OrderEntryApplet;-><init>(Ldagger/Lazy;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/OrderEntryApplet;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/orderentry/OrderEntryApplet_Factory;->newInstance(Ldagger/Lazy;)Lcom/squareup/orderentry/OrderEntryApplet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryApplet_Factory;->get()Lcom/squareup/orderentry/OrderEntryApplet;

    move-result-object v0

    return-object v0
.end method
