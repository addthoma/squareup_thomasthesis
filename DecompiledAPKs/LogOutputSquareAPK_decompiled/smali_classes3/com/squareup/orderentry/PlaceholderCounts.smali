.class public Lcom/squareup/orderentry/PlaceholderCounts;
.super Ljava/lang/Object;
.source "PlaceholderCounts.java"


# instance fields
.field private final categoryCount:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final discountCount:I

.field private final giftCardCount:I

.field private final itemCount:I

.field private final pageTiles:Lcom/squareup/shared/catalog/models/PageTiles;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method private constructor <init>(Lcom/squareup/util/Res;IIILjava/util/Map;Lcom/squareup/shared/catalog/models/PageTiles;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "III",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/squareup/shared/catalog/models/PageTiles;",
            ")V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/orderentry/PlaceholderCounts;->res:Lcom/squareup/util/Res;

    .line 46
    iput p2, p0, Lcom/squareup/orderentry/PlaceholderCounts;->itemCount:I

    .line 47
    iput p3, p0, Lcom/squareup/orderentry/PlaceholderCounts;->discountCount:I

    .line 48
    iput p4, p0, Lcom/squareup/orderentry/PlaceholderCounts;->giftCardCount:I

    .line 49
    iput-object p5, p0, Lcom/squareup/orderentry/PlaceholderCounts;->categoryCount:Ljava/util/Map;

    .line 50
    iput-object p6, p0, Lcom/squareup/orderentry/PlaceholderCounts;->pageTiles:Lcom/squareup/shared/catalog/models/PageTiles;

    return-void
.end method

.method public static from(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/models/PageTiles;)Lcom/squareup/orderentry/PlaceholderCounts;
    .locals 7

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/api/items/Item$Type;

    .line 31
    sget-object v1, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object p0

    .line 32
    invoke-interface {p1, p0}, Lcom/squareup/shared/catalog/Catalog$Local;->countItems(Ljava/util/List;)I

    move-result v2

    .line 33
    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->countDiscounts()I

    move-result v0

    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->readCompDiscounts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v3, v0, v1

    .line 34
    sget-object v0, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->countItems(Ljava/util/List;)I

    move-result v4

    .line 35
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 37
    invoke-virtual {p1, p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->countCatalogItemsForAllNonEmptyCategory(Ljava/util/List;)Ljava/util/Map;

    move-result-object v5

    .line 39
    new-instance p0, Lcom/squareup/orderentry/PlaceholderCounts;

    move-object v0, p0

    move-object v1, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/orderentry/PlaceholderCounts;-><init>(Lcom/squareup/util/Res;IIILjava/util/Map;Lcom/squareup/shared/catalog/models/PageTiles;)V

    return-object p0
.end method

.method private getItemCountString(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 77
    iget-object p1, p0, Lcom/squareup/orderentry/PlaceholderCounts;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderentry/R$string;->item_count_single:I

    .line 78
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/PlaceholderCounts;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->item_count_plural:I

    .line 79
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "count"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method


# virtual methods
.method public categoryCount(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/orderentry/PlaceholderCounts;->categoryCount:Ljava/util/Map;

    .line 60
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/PlaceholderCounts;->categoryCount:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 59
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/PlaceholderCounts;->getItemCountString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getItemCountString(Lcom/squareup/api/items/Placeholder$PlaceholderType;)Ljava/lang/String;
    .locals 3

    .line 64
    sget-object v0, Lcom/squareup/orderentry/PlaceholderCounts$1;->$SwitchMap$com$squareup$api$items$Placeholder$PlaceholderType:[I

    invoke-virtual {p1}, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 70
    iget p1, p0, Lcom/squareup/orderentry/PlaceholderCounts;->giftCardCount:I

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/PlaceholderCounts;->getItemCountString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 72
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Do not have count for type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_1
    iget p1, p0, Lcom/squareup/orderentry/PlaceholderCounts;->discountCount:I

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/PlaceholderCounts;->getItemCountString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 66
    :cond_2
    iget p1, p0, Lcom/squareup/orderentry/PlaceholderCounts;->itemCount:I

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/PlaceholderCounts;->getItemCountString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getPageTiles()Lcom/squareup/shared/catalog/models/PageTiles;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/orderentry/PlaceholderCounts;->pageTiles:Lcom/squareup/shared/catalog/models/PageTiles;

    return-object v0
.end method
