.class public final Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;
.super Ljava/lang/Object;
.source "PaymentPadTabletPortraitPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cartDropDownPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CartDropDownPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final cartRecyclerViewPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartRecyclerViewPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartRecyclerViewPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CartDropDownPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p2, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p3, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p4, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p5, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p6, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->cartRecyclerViewPresenterProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p7, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p8, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->cartDropDownPresenterProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p9, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartRecyclerViewPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CartDropDownPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)",
            "Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;"
        }
    .end annotation

    .line 74
    new-instance v10, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/orderentry/CartDropDownPresenter;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/cart/CartRecyclerViewPresenter;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/orderentry/CartDropDownPresenter;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ")",
            "Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;"
        }
    .end annotation

    .line 82
    new-instance v10, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;-><init>(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/orderentry/CartDropDownPresenter;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;
    .locals 10

    .line 63
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->cartRecyclerViewPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->cartDropDownPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/orderentry/CartDropDownPresenter;

    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static/range {v1 .. v9}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->newInstance(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/orderentry/CartDropDownPresenter;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter_Factory;->get()Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;

    move-result-object v0

    return-object v0
.end method
