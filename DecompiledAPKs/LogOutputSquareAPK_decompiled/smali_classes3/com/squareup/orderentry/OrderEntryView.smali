.class public Lcom/squareup/orderentry/OrderEntryView;
.super Landroid/widget/FrameLayout;
.source "OrderEntryView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/user/NotificationPresenter$View;
.implements Lcom/squareup/container/VisualTransitionListener;
.implements Lcom/squareup/container/spot/HasSpot;


# static fields
.field private static final MIN_DISTANCE_FOR_FLING_IN_DP:I = 0x19


# instance fields
.field private final detector:Landroidx/core/view/GestureDetectorCompat;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final maybeShadowPainter:Lcom/squareup/marin/widgets/BorderPainter;

.field private maybeShadowedChild:Landroid/view/View;

.field private final notificationPopup:Lcom/squareup/user/NotificationPopup;

.field private paymentPadPortraitTablet:Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

.field private phoneFlyByDestination:Landroid/widget/TextView;

.field presenter:Lcom/squareup/orderentry/OrderEntryScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final switchEmployeeEducationPopup:Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;

.field private final warningPopup:Lcom/squareup/flowlegacy/WarningPopup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 62
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-interface {p2, p0}, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;->inject(Lcom/squareup/orderentry/OrderEntryView;)V

    .line 65
    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryView;->device:Lcom/squareup/util/Device;

    invoke-interface {p2}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryView;->device:Lcom/squareup/util/Device;

    invoke-interface {p2}, Lcom/squareup/util/Device;->isLandscape()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 66
    new-instance p2, Lcom/squareup/marin/widgets/BorderPainter;

    sget v0, Lcom/squareup/orderentry/R$dimen;->home_view_sales_frame_shadow_width:I

    sget v1, Lcom/squareup/orderentry/R$color;->home_view_sales_frame_shadow:I

    invoke-direct {p2, p0, v0, v1}, Lcom/squareup/marin/widgets/BorderPainter;-><init>(Landroid/view/View;II)V

    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryView;->maybeShadowPainter:Lcom/squareup/marin/widgets/BorderPainter;

    .line 68
    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryView;->maybeShadowPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {p2}, Lcom/squareup/marin/widgets/BorderPainter;->drawBorderOutside()V

    .line 69
    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryView;->maybeShadowPainter:Lcom/squareup/marin/widgets/BorderPainter;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/BorderPainter;->addBorder(I)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 71
    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryView;->maybeShadowPainter:Lcom/squareup/marin/widgets/BorderPainter;

    .line 74
    :goto_0
    new-instance p2, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->device:Lcom/squareup/util/Device;

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryView;->presenter:Lcom/squareup/orderentry/OrderEntryScreen$Presenter;

    .line 75
    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getSwitchEmployeesEducationMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p2, p1, v0, v1}, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;-><init>(Landroid/content/Context;Lcom/squareup/util/Device;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryView;->switchEmployeeEducationPopup:Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;

    .line 76
    new-instance p2, Lcom/squareup/user/NotificationPopup;

    invoke-direct {p2, p1}, Lcom/squareup/user/NotificationPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryView;->notificationPopup:Lcom/squareup/user/NotificationPopup;

    .line 77
    new-instance p2, Lcom/squareup/flowlegacy/WarningPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/WarningPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    .line 79
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    const/high16 p2, 0x41c80000    # 25.0f

    mul-float p1, p1, p2

    float-to-int p1, p1

    .line 81
    new-instance p2, Landroidx/core/view/GestureDetectorCompat;

    .line 82
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/OrderEntryView$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/orderentry/OrderEntryView$1;-><init>(Lcom/squareup/orderentry/OrderEntryView;I)V

    invoke-direct {p2, v0, v1}, Landroidx/core/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryView;->detector:Landroidx/core/view/GestureDetectorCompat;

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .line 151
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->maybeShadowPainter:Lcom/squareup/marin/widgets/BorderPainter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->maybeShadowedChild:Landroid/view/View;

    .line 154
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->presenter:Lcom/squareup/orderentry/OrderEntryScreen$Presenter;

    .line 155
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->shouldDrawShadow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->maybeShadowPainter:Lcom/squareup/marin/widgets/BorderPainter;

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryView;->maybeShadowedChild:Landroid/view/View;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/marin/widgets/BorderPainter;->drawChildBorders(Landroid/graphics/Canvas;Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public getNotificationPopup()Lcom/squareup/mortar/Popup;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mortar/Popup<",
            "Lcom/squareup/server/account/protos/Notification;",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            ">;"
        }
    .end annotation

    .line 165
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->notificationPopup:Lcom/squareup/user/NotificationPopup;

    return-object v0
.end method

.method protected getPhoneFlyByDestination()Landroid/widget/TextView;
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->phoneFlyByDestination:Landroid/widget/TextView;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 1

    .line 142
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryScreen;

    .line 143
    iget-object p1, p1, Lcom/squareup/orderentry/OrderEntryScreen;->mode:Lcom/squareup/orderentry/OrderEntryMode;

    sget-object v0, Lcom/squareup/orderentry/OrderEntryMode;->EDIT_FAVORITES_FROM_ITEMS_APPLET:Lcom/squareup/orderentry/OrderEntryMode;

    if-ne p1, v0, :cond_0

    .line 144
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1

    .line 146
    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method protected getSwitchEmployeeEducationPopup()Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->switchEmployeeEducationPopup:Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;

    return-object v0
.end method

.method protected getTabletFlyByDestination()Landroid/widget/TextView;
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->paymentPadPortraitTablet:Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 184
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->getSaleQuantity()Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public hideDrawerButton()V
    .locals 2

    .line 169
    sget v0, Lcom/squareup/orderentry/R$id;->drawer_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$OrderEntryView(Landroid/graphics/Rect;I)V
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->maybeShadowedChild:Landroid/view/View;

    .line 119
    invoke-static {v0, p0}, Lcom/squareup/util/Views;->getLeftRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result v0

    sub-int/2addr v0, p2

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 120
    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryView;->maybeShadowedChild:Landroid/view/View;

    invoke-static {p2, p0}, Lcom/squareup/util/Views;->getTopRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result p2

    iput p2, p1, Landroid/graphics/Rect;->top:I

    .line 121
    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryView;->maybeShadowedChild:Landroid/view/View;

    invoke-static {p2, p0}, Lcom/squareup/util/Views;->getLeftRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result p2

    iput p2, p1, Landroid/graphics/Rect;->right:I

    .line 122
    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryView;->maybeShadowedChild:Landroid/view/View;

    invoke-static {p2, p0}, Lcom/squareup/util/Views;->getBottomRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result p2

    iput p2, p1, Landroid/graphics/Rect;->bottom:I

    .line 124
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryView;->invalidate(Landroid/graphics/Rect;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->presenter:Lcom/squareup/orderentry/OrderEntryScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->presenter:Lcom/squareup/orderentry/OrderEntryScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->dropView(Lcom/squareup/orderentry/OrderEntryView;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->presenter:Lcom/squareup/orderentry/OrderEntryScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getWarningPopupPresenter()Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 138
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 95
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 97
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Views;->isPortrait(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    sget v0, Lcom/squareup/orderentry/R$id;->cart_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 100
    sget v1, Lcom/squareup/orderentry/R$layout;->cart_recycler_view:I

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->maybeShadowPainter:Lcom/squareup/marin/widgets/BorderPainter;

    if-eqz v0, :cond_1

    .line 106
    sget v0, Lcom/squareup/orderentry/R$id;->payment_pad_landscape_tablet:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    const-string v1, "payment_pad_landscape_tablet"

    .line 107
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 108
    invoke-virtual {v0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->getSaleFrame()Landroid/view/View;

    move-result-object v1

    const-string v2, "salesFrame"

    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iput-object v1, p0, Lcom/squareup/orderentry/OrderEntryView;->maybeShadowedChild:Landroid/view/View;

    .line 111
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/orderentry/R$dimen;->home_view_sales_frame_shadow_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 112
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 117
    new-instance v3, Lcom/squareup/orderentry/-$$Lambda$OrderEntryView$gZw1BHH-C0CMglV559A_zPt4dl8;

    invoke-direct {v3, p0, v2, v1}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryView$gZw1BHH-C0CMglV559A_zPt4dl8;-><init>(Lcom/squareup/orderentry/OrderEntryView;Landroid/graphics/Rect;I)V

    .line 126
    invoke-virtual {v0, v3}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->setSaleFrameAnimationRunnable(Ljava/lang/Runnable;)V

    .line 129
    :cond_1
    sget v0, Lcom/squareup/orderentry/R$id;->payment_pad_portrait:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->paymentPadPortraitTablet:Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    .line 130
    sget v0, Lcom/squareup/orderentry/R$id;->cart_header_sale_quantity:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->phoneFlyByDestination:Landroid/widget/TextView;

    .line 131
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->presenter:Lcom/squareup/orderentry/OrderEntryScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->getWarningPopupPresenter()Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->presenter:Lcom/squareup/orderentry/OrderEntryScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onStartVisualTransition()V
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryView;->presenter:Lcom/squareup/orderentry/OrderEntryScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->onStartVisualTransition()V

    return-void
.end method
