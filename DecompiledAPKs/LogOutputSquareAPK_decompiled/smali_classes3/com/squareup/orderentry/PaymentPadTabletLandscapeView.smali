.class public Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;
.super Lcom/squareup/orderentry/PaymentPadView;
.source "PaymentPadTabletLandscapeView.java"


# static fields
.field protected static final FALL_DURATION_MS:J = 0x145L


# instance fields
.field private animateOverlay:Landroid/widget/ImageView;

.field private cartFooterBanner:Landroid/view/View;

.field private cartFooterBannerText:Landroid/widget/TextView;

.field private chargeAndTicketButtons:Landroid/view/View;

.field private container:Landroid/view/ViewGroup;

.field private currentAnimator:Landroid/animation/ObjectAnimator;

.field presenter:Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private saleFrameRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/squareup/orderentry/PaymentPadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)Landroid/animation/ObjectAnimator;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    return-object p0
.end method

.method static synthetic access$002(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)Landroid/view/View;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->chargeAndTicketButtons:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)Landroid/widget/ImageView;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->animateOverlay:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)Ljava/lang/Runnable;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrameRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method private startSaleFrameRunnable()V
    .locals 2

    .line 190
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrameRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 191
    new-instance v0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$5;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$5;-><init>(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)V

    .line 199
    iget-object v1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrame:Landroid/view/View;

    invoke-static {v1, v0}, Landroidx/core/view/ViewCompat;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method


# virtual methods
.method animateCurrentTicketContentsAway()V
    .locals 4

    .line 157
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrame:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->tryCopyToBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->animateOverlay:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 160
    iget-object v1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->animateOverlay:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 162
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->animateOverlay:Landroid/widget/ImageView;

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrame:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    aput v3, v1, v2

    const-string/jumbo v2, "y"

    invoke-static {v0, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v1, 0x145

    .line 163
    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 164
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    const v2, 0x3fa66666    # 1.3f

    invoke-direct {v1, v2}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 165
    invoke-static {v0, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 166
    new-instance v1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$4;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$4;-><init>(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 180
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_0
    return-void
.end method

.method protected animateToCartList()V
    .locals 4

    .line 127
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrame:Landroid/view/View;

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrame:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getX()F

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput v3, v1, v2

    const-string/jumbo v2, "x"

    invoke-static {v0, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    .line 131
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 132
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 134
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$3;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$3;-><init>(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 152
    invoke-direct {p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->startSaleFrameRunnable()V

    .line 153
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method protected animateToEditMode()V
    .locals 4

    .line 97
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrame:Landroid/view/View;

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrame:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getX()F

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrame:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    aput v3, v1, v2

    const-string/jumbo v2, "x"

    invoke-static {v0, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    .line 101
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 102
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$2;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$2;-><init>(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 122
    invoke-direct {p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->startSaleFrameRunnable()V

    .line 123
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method bridge synthetic getPresenter()Lcom/squareup/orderentry/PaymentPadPresenter;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->getPresenter()Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;

    move-result-object v0

    return-object v0
.end method

.method getPresenter()Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->presenter:Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;

    return-object v0
.end method

.method public getSaleFrame()Landroid/view/View;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrame:Landroid/view/View;

    return-object v0
.end method

.method protected inject()V
    .locals 2

    .line 48
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-interface {v0, p0}, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;->inject(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->presenter:Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->dropView(Ljava/lang/Object;)V

    .line 89
    invoke-super {p0}, Lcom/squareup/orderentry/PaymentPadView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 52
    invoke-super {p0}, Lcom/squareup/orderentry/PaymentPadView;->onFinishInflate()V

    .line 54
    sget v0, Lcom/squareup/orderentry/R$id;->cart_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->container:Landroid/view/ViewGroup;

    .line 55
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->container:Landroid/view/ViewGroup;

    sget v1, Lcom/squareup/orderentry/R$layout;->cart_recycler_view:I

    iget-object v2, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->container:Landroid/view/ViewGroup;

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 57
    sget v0, Lcom/squareup/orderentry/R$id;->charge_and_ticket_buttons:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->chargeAndTicketButtons:Landroid/view/View;

    .line 58
    sget v0, Lcom/squareup/orderentry/R$id;->cart_ticket_animation_overlay:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->animateOverlay:Landroid/widget/ImageView;

    .line 59
    sget v0, Lcom/squareup/orderentry/R$id;->seller_cart_footer_banner:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->cartFooterBanner:Landroid/view/View;

    .line 60
    sget v0, Lcom/squareup/orderentry/R$id;->seller_cart_footer_banner_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->cartFooterBannerText:Landroid/widget/TextView;

    .line 61
    sget v0, Lcom/squareup/orderentry/R$id;->cart_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 62
    new-instance v1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$1;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$1;-><init>(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->presenter:Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setSaleFrameAnimationRunnable(Ljava/lang/Runnable;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrameRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method updateCartFooterBanner(Ljava/lang/String;)V
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->cartFooterBanner:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 186
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->cartFooterBannerText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
