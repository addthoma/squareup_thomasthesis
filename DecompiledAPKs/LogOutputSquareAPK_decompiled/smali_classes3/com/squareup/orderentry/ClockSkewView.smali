.class public Lcom/squareup/orderentry/ClockSkewView;
.super Lcom/squareup/ui/StickyNoOverscrollScrollView;
.source "ClockSkewView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/spot/HasSpot;


# instance fields
.field private clockSettingsButton:Lcom/squareup/marketfont/MarketButton;

.field private incoming:Z

.field presenter:Lcom/squareup/orderentry/ClockSkewPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/StickyNoOverscrollScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const-class p2, Lcom/squareup/orderentry/ClockSkewScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/ClockSkewScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/ClockSkewScreen$Component;->inject(Lcom/squareup/orderentry/ClockSkewView;)V

    const/4 p1, 0x1

    .line 30
    iput-boolean p1, p0, Lcom/squareup/orderentry/ClockSkewView;->incoming:Z

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 56
    iget-boolean p1, p0, Lcom/squareup/orderentry/ClockSkewView;->incoming:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 57
    iput-boolean p1, p0, Lcom/squareup/orderentry/ClockSkewView;->incoming:Z

    const/4 p1, 0x0

    return-object p1

    .line 60
    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public onAttachedToWindow()V
    .locals 2

    .line 34
    invoke-super {p0}, Lcom/squareup/ui/StickyNoOverscrollScrollView;->onAttachedToWindow()V

    .line 36
    sget v0, Lcom/squareup/orderentry/R$id;->clock_settings_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/orderentry/ClockSkewView;->clockSettingsButton:Lcom/squareup/marketfont/MarketButton;

    .line 37
    iget-object v0, p0, Lcom/squareup/orderentry/ClockSkewView;->clockSettingsButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/orderentry/ClockSkewView$1;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/ClockSkewView$1;-><init>(Lcom/squareup/orderentry/ClockSkewView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/orderentry/ClockSkewView;->presenter:Lcom/squareup/orderentry/ClockSkewPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/ClockSkewPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/orderentry/ClockSkewView;->presenter:Lcom/squareup/orderentry/ClockSkewPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/ClockSkewPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/orderentry/ClockSkewView;->presenter:Lcom/squareup/orderentry/ClockSkewPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/ClockSkewPresenter;->dropView(Ljava/lang/Object;)V

    .line 48
    invoke-super {p0}, Lcom/squareup/ui/StickyNoOverscrollScrollView;->onDetachedFromWindow()V

    return-void
.end method
