.class public Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;
.super Lcom/squareup/orderentry/PaymentPadPresenter;
.source "PaymentPadTabletLandscapePresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/orderentry/PaymentPadPresenter<",
        "Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;",
        ">;"
    }
.end annotation


# instance fields
.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final cartMenuDropDownPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

.field private final loyaltySellerCartBannerFormatter:Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 30
    invoke-direct {p0, p1, p4}, Lcom/squareup/orderentry/PaymentPadPresenter;-><init>(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/tutorialv2/TutorialCore;)V

    .line 31
    iput-object p2, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->bus:Lcom/squareup/badbus/BadBus;

    .line 32
    iput-object p3, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->cartMenuDropDownPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    .line 33
    iput-object p5, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->loyaltySellerCartBannerFormatter:Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;

    return-void
.end method

.method private onTicketSaved()V
    .locals 1

    .line 60
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->animateCurrentTicketContentsAway()V

    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$PaymentPadTabletLandscapePresenter(Lcom/squareup/payment/OrderEntryEvents$TicketSaved;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->onTicketSaved()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$PaymentPadTabletLandscapePresenter(Lcom/squareup/loyaltycheckin/LoyaltySellerCartBanner;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 45
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 48
    instance-of v1, p1, Lcom/squareup/loyaltycheckin/LoyaltySellerCartBanner$Banner;

    if-eqz v1, :cond_0

    .line 49
    check-cast p1, Lcom/squareup/loyaltycheckin/LoyaltySellerCartBanner$Banner;

    invoke-virtual {p1}, Lcom/squareup/loyaltycheckin/LoyaltySellerCartBanner$Banner;->getText()Ljava/lang/String;

    move-result-object v0

    .line 52
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->updateCartFooterBanner(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method onCurrentSaleClicked()V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->cartMenuDropDownPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->toggle()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 37
    invoke-super {p0, p1}, Lcom/squareup/orderentry/PaymentPadPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$TicketSaved;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$PaymentPadTabletLandscapePresenter$YriHlOqSWpzmqrgkwQPGsSs1RrQ;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$PaymentPadTabletLandscapePresenter$YriHlOqSWpzmqrgkwQPGsSs1RrQ;-><init>(Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->loyaltySellerCartBannerFormatter:Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;

    .line 43
    invoke-interface {v0}, Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;->banner()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$PaymentPadTabletLandscapePresenter$9bpJrp2YudTrSYcKLg3sik5qUrg;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$PaymentPadTabletLandscapePresenter$9bpJrp2YudTrSYcKLg3sik5qUrg;-><init>(Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;)V

    .line 44
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 42
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
