.class public Lcom/squareup/orderentry/KeypadEntryCardPresenter$MoneyEntryKeypadListener;
.super Lcom/squareup/padlock/MoneyKeypadListener;
.source "KeypadEntryCardPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/KeypadEntryCardPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MoneyEntryKeypadListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/KeypadEntryCardPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)V
    .locals 3

    .line 140
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter$MoneyEntryKeypadListener;->this$0:Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    .line 141
    invoke-static {p1}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->access$000(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/KeypadEntryCardView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryCardView;->getKeypad()Lcom/squareup/padlock/Padlock;

    move-result-object v0

    invoke-static {p1}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->access$100(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)Landroid/os/Vibrator;

    move-result-object p1

    sget-wide v1, Lcom/squareup/money/MaxMoneyScrubber;->MAX_MONEY:J

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/squareup/padlock/MoneyKeypadListener;-><init>(Lcom/squareup/padlock/Padlock;Landroid/os/Vibrator;J)V

    return-void
.end method


# virtual methods
.method public getAmount()J
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter$MoneyEntryKeypadListener;->this$0:Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->access$200(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;->keypadInfo()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public onSubmitClicked()V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter$MoneyEntryKeypadListener;->this$0:Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->finish()V

    return-void
.end method

.method public updateAmount(J)V
    .locals 2

    .line 149
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter$MoneyEntryKeypadListener;->this$0:Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->access$300(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    invoke-static {p1, p2, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->setMoney(Lcom/squareup/protos/common/Money;)V

    .line 150
    iget-object p1, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter$MoneyEntryKeypadListener;->this$0:Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    invoke-static {p1}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->access$400(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)V

    .line 151
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter$MoneyEntryKeypadListener;->updateKeyStates()V

    return-void
.end method
