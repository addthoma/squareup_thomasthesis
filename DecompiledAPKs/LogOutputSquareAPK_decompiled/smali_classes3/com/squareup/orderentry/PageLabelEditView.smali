.class public Lcom/squareup/orderentry/PageLabelEditView;
.super Landroid/widget/LinearLayout;
.source "PageLabelEditView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/spot/HasSpot;


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private labelEditText:Landroid/widget/EditText;

.field presenter:Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const-class p2, Lcom/squareup/orderentry/PageLabelEditScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/PageLabelEditScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/PageLabelEditScreen$Component;->inject(Lcom/squareup/orderentry/PageLabelEditView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 85
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/orderentry/PageLabelEditView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 86
    sget v0, Lcom/squareup/orderentry/R$id;->page_label:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/orderentry/PageLabelEditView;->labelEditText:Landroid/widget/EditText;

    return-void
.end method


# virtual methods
.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/orderentry/PageLabelEditView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method getPageName()Ljava/lang/String;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/orderentry/PageLabelEditView;->labelEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 76
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method hideKeyboard()V
    .locals 0

    .line 68
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$requestInitialFocus$0$PageLabelEditView()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/orderentry/PageLabelEditView;->labelEditText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 36
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 37
    invoke-direct {p0}, Lcom/squareup/orderentry/PageLabelEditView;->bindViews()V

    .line 38
    iget-object v0, p0, Lcom/squareup/orderentry/PageLabelEditView;->labelEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/orderentry/PageLabelEditView$1;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/PageLabelEditView$1;-><init>(Lcom/squareup/orderentry/PageLabelEditView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/orderentry/PageLabelEditView;->presenter:Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/orderentry/PageLabelEditView;->presenter:Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/orderentry/PageLabelEditView;->presenter:Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 51
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public requestInitialFocus()V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/orderentry/PageLabelEditView;->labelEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 81
    iget-object v0, p0, Lcom/squareup/orderentry/PageLabelEditView;->labelEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$PageLabelEditView$rrUh8rEju1iF-NfIMAJdYNcEbxk;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$PageLabelEditView$rrUh8rEju1iF-NfIMAJdYNcEbxk;-><init>(Lcom/squareup/orderentry/PageLabelEditView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method setPageName(Ljava/lang/String;)V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/orderentry/PageLabelEditView;->labelEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
