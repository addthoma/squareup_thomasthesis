.class public Lcom/squareup/orderentry/KeypadEntryCardPresenter$PercentageEntryKeypadListener;
.super Lcom/squareup/padlock/PercentageKeypadListener;
.source "KeypadEntryCardPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/KeypadEntryCardPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PercentageEntryKeypadListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/KeypadEntryCardPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)V
    .locals 1

    .line 161
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter$PercentageEntryKeypadListener;->this$0:Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    .line 162
    invoke-static {p1}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->access$500(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/KeypadEntryCardView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryCardView;->getKeypad()Lcom/squareup/padlock/Padlock;

    move-result-object v0

    invoke-static {p1}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->access$100(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)Landroid/os/Vibrator;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/padlock/PercentageKeypadListener;-><init>(Lcom/squareup/padlock/Padlock;Landroid/os/Vibrator;)V

    return-void
.end method


# virtual methods
.method public onPercentUpdated(Ljava/lang/String;Z)V
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter$PercentageEntryKeypadListener;->this$0:Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->setPercentage(Ljava/lang/String;)V

    .line 171
    iget-object p1, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter$PercentageEntryKeypadListener;->this$0:Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    invoke-static {p1, p2}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->access$600(Lcom/squareup/orderentry/KeypadEntryCardPresenter;Z)V

    return-void
.end method

.method public onSubmitClicked()V
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter$PercentageEntryKeypadListener;->this$0:Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->finish()V

    return-void
.end method
