.class public final enum Lcom/squareup/orderentry/OrderEntryMode;
.super Ljava/lang/Enum;
.source "OrderEntryMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orderentry/OrderEntryMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orderentry/OrderEntryMode;

.field public static final enum EDIT_FAVORITES_FROM_ITEMS_APPLET:Lcom/squareup/orderentry/OrderEntryMode;

.field public static final enum FAVORITES:Lcom/squareup/orderentry/OrderEntryMode;

.field public static final enum KEYPAD:Lcom/squareup/orderentry/OrderEntryMode;

.field public static final enum LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryMode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 9
    new-instance v0, Lcom/squareup/orderentry/OrderEntryMode;

    const/4 v1, 0x0

    const-string v2, "LAST_SELECTED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/orderentry/OrderEntryMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryMode;->LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryMode;

    .line 13
    new-instance v0, Lcom/squareup/orderentry/OrderEntryMode;

    const/4 v2, 0x1

    const-string v3, "FAVORITES"

    invoke-direct {v0, v3, v2}, Lcom/squareup/orderentry/OrderEntryMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryMode;->FAVORITES:Lcom/squareup/orderentry/OrderEntryMode;

    .line 17
    new-instance v0, Lcom/squareup/orderentry/OrderEntryMode;

    const/4 v3, 0x2

    const-string v4, "KEYPAD"

    invoke-direct {v0, v4, v3}, Lcom/squareup/orderentry/OrderEntryMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryMode;->KEYPAD:Lcom/squareup/orderentry/OrderEntryMode;

    .line 25
    new-instance v0, Lcom/squareup/orderentry/OrderEntryMode;

    const/4 v4, 0x3

    const-string v5, "EDIT_FAVORITES_FROM_ITEMS_APPLET"

    invoke-direct {v0, v5, v4}, Lcom/squareup/orderentry/OrderEntryMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryMode;->EDIT_FAVORITES_FROM_ITEMS_APPLET:Lcom/squareup/orderentry/OrderEntryMode;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/orderentry/OrderEntryMode;

    .line 3
    sget-object v5, Lcom/squareup/orderentry/OrderEntryMode;->LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryMode;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->FAVORITES:Lcom/squareup/orderentry/OrderEntryMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->KEYPAD:Lcom/squareup/orderentry/OrderEntryMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->EDIT_FAVORITES_FROM_ITEMS_APPLET:Lcom/squareup/orderentry/OrderEntryMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/orderentry/OrderEntryMode;->$VALUES:[Lcom/squareup/orderentry/OrderEntryMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orderentry/OrderEntryMode;
    .locals 1

    .line 3
    const-class v0, Lcom/squareup/orderentry/OrderEntryMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orderentry/OrderEntryMode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orderentry/OrderEntryMode;
    .locals 1

    .line 3
    sget-object v0, Lcom/squareup/orderentry/OrderEntryMode;->$VALUES:[Lcom/squareup/orderentry/OrderEntryMode;

    invoke-virtual {v0}, [Lcom/squareup/orderentry/OrderEntryMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orderentry/OrderEntryMode;

    return-object v0
.end method
