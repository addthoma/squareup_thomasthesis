.class public final Lcom/squareup/orderentry/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_bar:I = 0x7f0a0135

.field public static final amount_container:I = 0x7f0a01bc

.field public static final amount_field:I = 0x7f0a01bd

.field public static final applets_drawer_glyph:I = 0x7f0a01ca

.field public static final applets_drawer_order_entry_applet:I = 0x7f0a01cc

.field public static final badge_view:I = 0x7f0a01f0

.field public static final card_container:I = 0x7f0a02b4

.field public static final cart_container:I = 0x7f0a02cb

.field public static final cart_header:I = 0x7f0a02d1

.field public static final cart_header_current_sale:I = 0x7f0a02d2

.field public static final cart_header_current_sale_label:I = 0x7f0a02d3

.field public static final cart_header_flyout:I = 0x7f0a02d4

.field public static final cart_header_no_sale:I = 0x7f0a02d5

.field public static final cart_header_sale_quantity:I = 0x7f0a02d6

.field public static final cart_list:I = 0x7f0a02d7

.field public static final cart_menu_add_customer:I = 0x7f0a02d8

.field public static final cart_menu_arrow_button:I = 0x7f0a02d9

.field public static final cart_menu_clear_new_items:I = 0x7f0a02da

.field public static final cart_menu_drop_down_container:I = 0x7f0a02db

.field public static final cart_menu_edit_ticket:I = 0x7f0a02dc

.field public static final cart_menu_merge_ticket:I = 0x7f0a02dd

.field public static final cart_menu_move_ticket:I = 0x7f0a02de

.field public static final cart_menu_print_bill:I = 0x7f0a02df

.field public static final cart_menu_reprint_ticket:I = 0x7f0a02e0

.field public static final cart_menu_split_ticket:I = 0x7f0a02e1

.field public static final cart_menu_transfer_ticket:I = 0x7f0a02e2

.field public static final cart_menu_view_customer:I = 0x7f0a02e3

.field public static final cart_recycler_view:I = 0x7f0a02e5

.field public static final cart_ticket_animation_overlay:I = 0x7f0a02e6

.field public static final category_drop_down_arrow:I = 0x7f0a02f4

.field public static final category_drop_down_title:I = 0x7f0a02f5

.field public static final category_list:I = 0x7f0a02f6

.field public static final charge_and_ticket_buttons:I = 0x7f0a0307

.field public static final charge_and_ticket_saved_alert:I = 0x7f0a0308

.field public static final charge_and_tickets_buttons_confirm_overlay:I = 0x7f0a0309

.field public static final charge_and_tickets_buttons_container:I = 0x7f0a030a

.field public static final charge_button_confirm_overlay:I = 0x7f0a030b

.field public static final charge_button_container:I = 0x7f0a030c

.field public static final charge_button_subtitle:I = 0x7f0a030d

.field public static final charge_button_title:I = 0x7f0a030e

.field public static final checkable_ticket_group:I = 0x7f0a0317

.field public static final clock_settings_button:I = 0x7f0a0356

.field public static final clock_skew_glyph:I = 0x7f0a0357

.field public static final clock_skew_message:I = 0x7f0a0358

.field public static final clock_skew_title:I = 0x7f0a0359

.field public static final create_discount_button:I = 0x7f0a03c3

.field public static final create_item_button:I = 0x7f0a03c4

.field public static final custom_ticket_button:I = 0x7f0a0528

.field public static final delete:I = 0x7f0a0560

.field public static final detail_confirmation_glyph:I = 0x7f0a0594

.field public static final detail_confirmation_message:I = 0x7f0a0596

.field public static final detail_confirmation_title:I = 0x7f0a0597

.field public static final dining_option:I = 0x7f0a05ba

.field public static final dining_option_app_bar:I = 0x7f0a05bb

.field public static final dismiss_button:I = 0x7f0a05d6

.field public static final done_editing:I = 0x7f0a05e6

.field public static final drawer_button:I = 0x7f0a05fb

.field public static final edit_frame:I = 0x7f0a061d

.field public static final edit_notes:I = 0x7f0a064f

.field public static final edit_split_ticket_view:I = 0x7f0a067c

.field public static final edit_ticket_recycler_view:I = 0x7f0a067e

.field public static final employee_lock_button:I = 0x7f0a06db

.field public static final employee_lock_button_glyph:I = 0x7f0a06dc

.field public static final employee_lock_button_text:I = 0x7f0a06dd

.field public static final favorite_view_position_tag:I = 0x7f0a0746

.field public static final favorite_view_type_tag:I = 0x7f0a0747

.field public static final fee:I = 0x7f0a0748

.field public static final gift_card_amount:I = 0x7f0a078c

.field public static final gift_card_figure:I = 0x7f0a0797

.field public static final gift_card_number:I = 0x7f0a0799

.field public static final hairline:I = 0x7f0a07c4

.field public static final home_actionbar_view:I = 0x7f0a07e1

.field public static final home_navigation_bar_edit_view:I = 0x7f0a07e2

.field public static final home_navigation_bar_sale_view:I = 0x7f0a07e3

.field public static final home_panel_keypad:I = 0x7f0a07e4

.field public static final item_card_empty_note:I = 0x7f0a08ca

.field public static final item_card_empty_note_glyph:I = 0x7f0a08cb

.field public static final item_card_empty_note_message:I = 0x7f0a08cc

.field public static final item_card_empty_note_title:I = 0x7f0a08cd

.field public static final item_card_empty_open_items_button:I = 0x7f0a08ce

.field public static final item_list:I = 0x7f0a08da

.field public static final item_list_create_item:I = 0x7f0a08db

.field public static final keypad:I = 0x7f0a08f9

.field public static final keypad_landscape_shim:I = 0x7f0a0908

.field public static final library_back_button:I = 0x7f0a092a

.field public static final library_bar_search:I = 0x7f0a092b

.field public static final library_default_container:I = 0x7f0a092c

.field public static final library_list:I = 0x7f0a092e

.field public static final library_search:I = 0x7f0a0930

.field public static final library_search_activator:I = 0x7f0a0931

.field public static final library_search_container:I = 0x7f0a0932

.field public static final library_search_exit:I = 0x7f0a0933

.field public static final library_title_bar:I = 0x7f0a0934

.field public static final master_detail_ticket_view_action_bar_edit_button:I = 0x7f0a09ae

.field public static final master_detail_ticket_view_action_bar_merge_transaction_ticket_button:I = 0x7f0a09af

.field public static final master_detail_ticket_view_action_bar_new_ticket_button:I = 0x7f0a09b0

.field public static final master_detail_ticket_view_action_bar_title:I = 0x7f0a09b1

.field public static final master_detail_ticket_view_action_bar_up_button_glyph:I = 0x7f0a09b2

.field public static final master_detail_ticket_view_edit_bar:I = 0x7f0a09b3

.field public static final master_detail_ticket_view_edit_bar_bulk_delete_button:I = 0x7f0a09b4

.field public static final master_detail_ticket_view_edit_bar_bulk_merge_button:I = 0x7f0a09b5

.field public static final master_detail_ticket_view_edit_bar_bulk_move_button:I = 0x7f0a09b6

.field public static final master_detail_ticket_view_edit_bar_bulk_reprint_ticket_button:I = 0x7f0a09b7

.field public static final master_detail_ticket_view_edit_bar_bulk_transfer_button:I = 0x7f0a09b8

.field public static final master_detail_ticket_view_edit_bar_bulk_void_button:I = 0x7f0a09b9

.field public static final master_detail_ticket_view_edit_bar_cancel_button:I = 0x7f0a09ba

.field public static final master_detail_ticket_view_edit_bar_dismiss_button:I = 0x7f0a09bb

.field public static final master_detail_ticket_view_edit_bar_title:I = 0x7f0a09bc

.field public static final master_detail_ticket_view_sort_type_menu_container:I = 0x7f0a09bd

.field public static final master_detail_ticket_view_sort_type_menu_drop_down:I = 0x7f0a09be

.field public static final master_group_list_view:I = 0x7f0a09bf

.field public static final master_group_list_view_log_out_button:I = 0x7f0a09c0

.field public static final master_group_list_view_log_out_button_divider:I = 0x7f0a09c1

.field public static final master_group_list_view_log_out_button_padder:I = 0x7f0a09c2

.field public static final master_group_list_view_progress_bar:I = 0x7f0a09c3

.field public static final master_group_list_view_recycler_view:I = 0x7f0a09c4

.field public static final master_group_row:I = 0x7f0a09c5

.field public static final merge_ticket_row_checkbox:I = 0x7f0a09d0

.field public static final merge_ticket_row_name:I = 0x7f0a09d1

.field public static final merge_ticket_view_recycler_view:I = 0x7f0a09d2

.field public static final mobile_payment_pad:I = 0x7f0a09e0

.field public static final move_ticket_list_view:I = 0x7f0a09f5

.field public static final move_ticket_view_recycler_view:I = 0x7f0a09f6

.field public static final nav_bar_content:I = 0x7f0a0a0f

.field public static final new_ticket_no_predefined_tickets_message:I = 0x7f0a0a20

.field public static final new_ticket_no_predefined_tickets_title:I = 0x7f0a0a21

.field public static final note_button:I = 0x7f0a0a48

.field public static final open_tickets_new_ticket_button:I = 0x7f0a0ab5

.field public static final order_entry_page_id:I = 0x7f0a0acb

.field public static final order_entry_view_container:I = 0x7f0a0acc

.field public static final order_entry_view_pager:I = 0x7f0a0acd

.field public static final page_label:I = 0x7f0a0b84

.field public static final payment_pad_landscape_tablet:I = 0x7f0a0be6

.field public static final payment_pad_left_half:I = 0x7f0a0be7

.field public static final payment_pad_portrait:I = 0x7f0a0be8

.field public static final payment_pad_portrait_amount:I = 0x7f0a0be9

.field public static final payment_pad_right_half:I = 0x7f0a0bea

.field public static final payment_pad_tabs:I = 0x7f0a0beb

.field public static final phone_payment_pad_charge_and_tickets_buttons:I = 0x7f0a0c26

.field public static final phone_ticket_row:I = 0x7f0a0c27

.field public static final predefined_ticket_row:I = 0x7f0a0c45

.field public static final preserved_label:I = 0x7f0a0c49

.field public static final price_field:I = 0x7f0a0c52

.field public static final recycler_view:I = 0x7f0a0d21

.field public static final right_keypad_container:I = 0x7f0a0d90

.field public static final sale_frame:I = 0x7f0a0da2

.field public static final sale_frame_animating_content:I = 0x7f0a0da3

.field public static final search_field:I = 0x7f0a0e1e

.field public static final section_header_row:I = 0x7f0a0e39

.field public static final seller_cart_footer_banner:I = 0x7f0a0e5d

.field public static final seller_cart_footer_banner_text:I = 0x7f0a0e5e

.field public static final sku_not_found_glyph:I = 0x7f0a0ea4

.field public static final sku_not_found_ok_button:I = 0x7f0a0ea5

.field public static final sku_not_found_title:I = 0x7f0a0ea6

.field public static final split_ticket_check_box:I = 0x7f0a0ee4

.field public static final split_ticket_container:I = 0x7f0a0ee5

.field public static final split_ticket_drop_down_caret:I = 0x7f0a0ee6

.field public static final split_ticket_drop_down_container:I = 0x7f0a0ee7

.field public static final split_ticket_footer_button:I = 0x7f0a0ee8

.field public static final split_ticket_header:I = 0x7f0a0ee9

.field public static final split_ticket_item:I = 0x7f0a0eea

.field public static final split_ticket_item_row:I = 0x7f0a0eeb

.field public static final split_ticket_menu_add_customer:I = 0x7f0a0eec

.field public static final split_ticket_menu_edit_ticket:I = 0x7f0a0eed

.field public static final split_ticket_menu_print_bill:I = 0x7f0a0eee

.field public static final split_ticket_menu_view:I = 0x7f0a0eef

.field public static final split_ticket_menu_view_customer:I = 0x7f0a0ef0

.field public static final split_ticket_recycler_view:I = 0x7f0a0ef1

.field public static final split_ticket_scroll_view:I = 0x7f0a0ef2

.field public static final split_ticket_tax_total:I = 0x7f0a0ef3

.field public static final split_ticket_ticket_name:I = 0x7f0a0ef4

.field public static final start_drawer_button:I = 0x7f0a0f2c

.field public static final start_drawer_starting_cash:I = 0x7f0a0f2e

.field public static final swiped_gift_card:I = 0x7f0a0f57

.field public static final tab_keypad:I = 0x7f0a0f62

.field public static final tab_library:I = 0x7f0a0f63

.field public static final ticket_card_not_stored_hint:I = 0x7f0a0fac

.field public static final ticket_check_box:I = 0x7f0a0fad

.field public static final ticket_comp:I = 0x7f0a0fae

.field public static final ticket_convert_to_custom_ticket:I = 0x7f0a0faf

.field public static final ticket_delete:I = 0x7f0a0fb1

.field public static final ticket_detail_view:I = 0x7f0a0fb2

.field public static final ticket_error_container:I = 0x7f0a0fb3

.field public static final ticket_error_message:I = 0x7f0a0fb4

.field public static final ticket_error_title:I = 0x7f0a0fb5

.field public static final ticket_list_no_tickets:I = 0x7f0a0fb6

.field public static final ticket_list_row:I = 0x7f0a0fb7

.field public static final ticket_list_text_row_text:I = 0x7f0a0fb8

.field public static final ticket_list_view:I = 0x7f0a0fb9

.field public static final ticket_list_view_progress:I = 0x7f0a0fba

.field public static final ticket_list_view_recycler_view:I = 0x7f0a0fbb

.field public static final ticket_list_view_search_bar:I = 0x7f0a0fbc

.field public static final ticket_name:I = 0x7f0a0fbd

.field public static final ticket_note:I = 0x7f0a0fc0

.field public static final ticket_row_amount:I = 0x7f0a0fc1

.field public static final ticket_row_block_employee:I = 0x7f0a0fc2

.field public static final ticket_row_employee:I = 0x7f0a0fc3

.field public static final ticket_row_name:I = 0x7f0a0fc4

.field public static final ticket_row_time:I = 0x7f0a0fc5

.field public static final ticket_row_time_with_employee:I = 0x7f0a0fc6

.field public static final ticket_sort_amount:I = 0x7f0a0fc7

.field public static final ticket_sort_employee:I = 0x7f0a0fc8

.field public static final ticket_sort_name:I = 0x7f0a0fc9

.field public static final ticket_sort_recent:I = 0x7f0a0fca

.field public static final ticket_template_name:I = 0x7f0a0fcd

.field public static final ticket_template_search:I = 0x7f0a0fce

.field public static final ticket_transfer_employee_name:I = 0x7f0a0fcf

.field public static final ticket_transfer_employee_radiobox:I = 0x7f0a0fd0

.field public static final ticket_uncomp:I = 0x7f0a0fd1

.field public static final ticket_void:I = 0x7f0a0fd2

.field public static final tickets_button_confirm_overlay:I = 0x7f0a0fd3

.field public static final tickets_button_container:I = 0x7f0a0fd4

.field public static final tickets_button_subtitle:I = 0x7f0a0fd5

.field public static final tickets_button_title:I = 0x7f0a0fd6

.field public static final tickets_transfer_employee_progress:I = 0x7f0a0fd7

.field public static final tickets_transfer_employee_recycler_view:I = 0x7f0a0fd8

.field public static final tickets_transfer_employee_search:I = 0x7f0a0fd9

.field public static final warning:I = 0x7f0a110f


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
