.class Lcom/squareup/orderentry/PageLabelEditView$1;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "PageLabelEditView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/PageLabelEditView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/PageLabelEditView;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/PageLabelEditView;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/orderentry/PageLabelEditView$1;->this$0:Lcom/squareup/orderentry/PageLabelEditView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x6

    if-ne p2, p1, :cond_0

    .line 41
    iget-object p1, p0, Lcom/squareup/orderentry/PageLabelEditView$1;->this$0:Lcom/squareup/orderentry/PageLabelEditView;

    iget-object p1, p1, Lcom/squareup/orderentry/PageLabelEditView;->presenter:Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->save()V

    :cond_0
    const/4 p1, 0x1

    return p1
.end method
