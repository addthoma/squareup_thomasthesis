.class final enum Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;
.super Ljava/lang/Enum;
.source "ChargeAndTicketButtonsPresenter.java"

# interfaces
.implements Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ButtonState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "TicketState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;",
        ">;",
        "Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ButtonState;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

.field public static final enum DISABLED:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

.field public static final enum SAVE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

.field public static final enum TICKETS:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;


# instance fields
.field final phoneBackgroundSelectorId:I

.field final phoneTextColorId:I

.field final tabletBackgroundSelectorId:I

.field final tabletTextColorId:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 84
    new-instance v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget v2, Lcom/squareup/orderentry/R$drawable;->selector_payment_pad_button_ticket_tickets:I

    const/4 v3, 0x0

    const-string v4, "TICKETS"

    invoke-direct {v0, v4, v3, v1, v2}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->TICKETS:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    .line 87
    new-instance v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    sget v2, Lcom/squareup/orderentry/R$drawable;->selector_payment_pad_button_ticket_save:I

    const/4 v4, 0x1

    const-string v5, "SAVE"

    invoke-direct {v0, v5, v4, v1, v2}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->SAVE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    .line 90
    new-instance v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    sget v1, Lcom/squareup/marin/R$color;->marin_light_gray:I

    sget v2, Lcom/squareup/orderentry/R$drawable;->selector_payment_pad_button:I

    const/4 v5, 0x2

    const-string v6, "DISABLED"

    invoke-direct {v0, v6, v5, v1, v2}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->DISABLED:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    .line 82
    sget-object v1, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->TICKETS:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->SAVE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->DISABLED:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->$VALUES:[Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 97
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 98
    iput p3, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->tabletTextColorId:I

    .line 99
    iput p4, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->tabletBackgroundSelectorId:I

    .line 101
    sget p1, Lcom/squareup/marin/R$color;->marin_white:I

    iput p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->phoneTextColorId:I

    .line 102
    sget p1, Lcom/squareup/orderentry/R$drawable;->selector_payment_pad_button_ticket:I

    iput p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->phoneBackgroundSelectorId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;
    .locals 1

    .line 82
    const-class v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;
    .locals 1

    .line 82
    sget-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->$VALUES:[Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    invoke-virtual {v0}, [Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    return-object v0
.end method


# virtual methods
.method public getBackgroundId(Z)I
    .locals 0

    if-eqz p1, :cond_0

    .line 112
    iget p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->tabletBackgroundSelectorId:I

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->phoneBackgroundSelectorId:I

    :goto_0
    return p1
.end method

.method public getTextColorId(Z)I
    .locals 0

    if-eqz p1, :cond_0

    .line 106
    iget p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->tabletTextColorId:I

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->phoneTextColorId:I

    :goto_0
    return p1
.end method
