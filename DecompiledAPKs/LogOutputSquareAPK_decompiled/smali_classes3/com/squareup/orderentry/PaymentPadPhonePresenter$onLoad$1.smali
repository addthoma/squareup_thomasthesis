.class final Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;
.super Lkotlin/jvm/internal/Lambda;
.source "PaymentPadPhonePresenter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/PaymentPadPhonePresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lio/reactivex/disposables/Disposable;
    .locals 5

    .line 52
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 53
    iget-object v1, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    invoke-static {v1}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->access$getOrderEntryScreenState$p(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)Lcom/squareup/orderentry/OrderEntryScreenState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->librarySearchIsActive()Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "orderEntryScreenState.librarySearchIsActive()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v2, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    invoke-static {v2}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->access$getDevice$p(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)Lcom/squareup/util/Device;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/util/Device;->getScreenSize()Lio/reactivex/Observable;

    move-result-object v2

    .line 55
    iget-object v3, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    invoke-static {v3}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->access$getFeatures$p(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)Lcom/squareup/settings/server/Features;

    move-result-object v3

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->USE_ORDER_ENTRY_SCREEN_V2_ANDROID:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v3, v4}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v3

    const-string v4, "features.featureEnabled(\u2026_ENTRY_SCREEN_V2_ANDROID)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 57
    new-instance v1, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1$1;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1$1;-><init>(Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "combineLatest(\n         \u2026            )\n          }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;->invoke()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
