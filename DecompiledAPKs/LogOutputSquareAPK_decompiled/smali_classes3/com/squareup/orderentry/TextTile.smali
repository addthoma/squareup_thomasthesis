.class public Lcom/squareup/orderentry/TextTile;
.super Landroid/widget/LinearLayout;
.source "TextTile.java"

# interfaces
.implements Lcom/squareup/orderentry/HasEnabledLook;


# instance fields
.field private colorView:Landroid/view/View;

.field private final defaultColor:I

.field private glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field private nameView:Landroid/widget/TextView;

.field private final selector:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 30
    sget p2, Lcom/squareup/marin/R$drawable;->marin_selector_dim_translucent_pressed:I

    .line 31
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orderentry/TextTile;->selector:Landroid/graphics/drawable/Drawable;

    .line 32
    sget p2, Lcom/squareup/widgets/pos/R$color;->edit_item_gray:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/orderentry/TextTile;->defaultColor:I

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 82
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 83
    invoke-virtual {p0}, Lcom/squareup/orderentry/TextTile;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/TextTile;->selector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .line 70
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    .line 71
    iget-object v0, p0, Lcom/squareup/orderentry/TextTile;->selector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/squareup/orderentry/TextTile;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 72
    invoke-virtual {p0}, Lcom/squareup/orderentry/TextTile;->invalidate()V

    return-void
.end method

.method public getName()Ljava/lang/CharSequence;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/orderentry/TextTile;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public looksEnabled()Z
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/orderentry/TextTile;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isEnabled()Z

    move-result v0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 36
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 37
    sget v0, Lcom/squareup/widgets/pos/R$id;->text_tile_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/orderentry/TextTile;->nameView:Landroid/widget/TextView;

    .line 38
    sget v0, Lcom/squareup/widgets/pos/R$id;->text_tile_color:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/TextTile;->colorView:Landroid/view/View;

    .line 39
    sget v0, Lcom/squareup/widgets/pos/R$id;->text_tile_icon:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/orderentry/TextTile;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .line 76
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 77
    iget-object p3, p0, Lcom/squareup/orderentry/TextTile;->selector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/squareup/orderentry/TextTile;->getPaddingLeft()I

    move-result p4

    invoke-virtual {p0}, Lcom/squareup/orderentry/TextTile;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/orderentry/TextTile;->getPaddingRight()I

    move-result v1

    sub-int/2addr p1, v1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/orderentry/TextTile;->getPaddingBottom()I

    move-result v1

    sub-int/2addr p2, v1

    .line 77
    invoke-virtual {p3, p4, v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    return-void
.end method

.method public setColor(Ljava/lang/String;)V
    .locals 1

    .line 61
    iget v0, p0, Lcom/squareup/orderentry/TextTile;->defaultColor:I

    invoke-static {p1, v0}, Lcom/squareup/util/Colors;->parseHex(Ljava/lang/String;I)I

    move-result p1

    .line 62
    iget-object v0, p0, Lcom/squareup/orderentry/TextTile;->colorView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method

.method public setContent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/TextTile;->setName(Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0, p2}, Lcom/squareup/orderentry/TextTile;->setColor(Ljava/lang/String;)V

    return-void
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/orderentry/TextTile;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 49
    iget-object v0, p0, Lcom/squareup/orderentry/TextTile;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method public setLooksEnabled(Z)V
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/orderentry/TextTile;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/orderentry/TextTile;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setStackedBackground()V
    .locals 1

    .line 53
    sget v0, Lcom/squareup/widgets/pos/R$drawable;->stacked_panel_background:I

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/TextTile;->setBackgroundResource(I)V

    return-void
.end method
