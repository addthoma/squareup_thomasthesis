.class public Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;
.super Lmortar/ViewPresenter;
.source "ChargeAndTicketButtonsPresenter.java"

# interfaces
.implements Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;,
        Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;,
        Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ButtonState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/ChargeAndTicketsButtons;",
        ">;",
        "Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

.field private final chargeAndTicketButtonFormatter:Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;

.field private chargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

.field private final device:Lcom/squareup/util/Device;

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final glassController:Lcom/squareup/widgets/glass/GlassConfirmController;

.field private final hasSeparatedPrintouts:Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;

.field private final loyaltySellerCartBannerFormatter:Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

.field private final sellerScopeRunner:Lcom/squareup/ui/seller/SellerScopeRunner;

.field private final smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

.field private final swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private ticketState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

.field private updateWithAnimation:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/payment/SwipeHandler;Lflow/Flow;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/util/Device;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;Lcom/squareup/settings/server/Features;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 202
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 183
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->updateWithAnimation:Lcom/jakewharton/rxrelay2/PublishRelay;

    move-object v1, p1

    .line 203
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeAndTicketButtonFormatter:Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;

    move-object v1, p2

    .line 204
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p3

    .line 205
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->badBus:Lcom/squareup/badbus/BadBus;

    move-object v1, p4

    .line 206
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    move-object v1, p5

    .line 207
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p6

    .line 208
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    move-object v1, p7

    .line 209
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->flow:Lflow/Flow;

    move-object v1, p8

    .line 210
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    move-object v1, p9

    .line 211
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    move-object v1, p10

    .line 212
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->glassController:Lcom/squareup/widgets/glass/GlassConfirmController;

    move-object v1, p11

    .line 213
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object v1, p12

    .line 214
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    move-object v1, p13

    .line 215
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-object/from16 v1, p14

    .line 216
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    move-object/from16 v1, p15

    .line 217
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    move-object/from16 v1, p16

    .line 218
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    move-object/from16 v1, p17

    .line 219
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-object/from16 v1, p18

    .line 220
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->device:Lcom/squareup/util/Device;

    move-object/from16 v1, p19

    .line 221
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    move-object/from16 v1, p20

    .line 222
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    move-object/from16 v1, p21

    .line 223
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    move-object/from16 v1, p22

    .line 224
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->hasSeparatedPrintouts:Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;

    move-object/from16 v1, p23

    .line 225
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->sellerScopeRunner:Lcom/squareup/ui/seller/SellerScopeRunner;

    move-object/from16 v1, p24

    .line 226
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->loyaltySellerCartBannerFormatter:Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;

    move-object/from16 v1, p25

    .line 227
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;)Lcom/squareup/cashdrawer/CashDrawerTracker;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->performNoSalePaymentAndReset()V

    return-void
.end method

.method private isChargeDisabledByBran()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 598
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_X2_FRONT_OF_TRANSACTION_CHECKIN_SELLER_OVERRIDE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 601
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0

    .line 603
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->loyaltySellerCartBannerFormatter:Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;

    invoke-interface {v0}, Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;->isCheckInFlowActive()Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$2(Lkotlin/Unit;)Ljava/lang/Boolean;
    .locals 0

    const/4 p0, 0x1

    .line 259
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$3(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 p0, 0x1

    .line 260
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private logItemizedCheckout()V
    .locals 3

    .line 291
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 292
    iget-object v2, v1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    sget-object v2, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_AMOUNT:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-eq v1, v2, :cond_0

    .line 293
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CHECKOUT_ITEMIZED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    :cond_1
    return-void
.end method

.method private performNoSalePaymentAndReset()V
    .locals 3

    .line 582
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 583
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 585
    :goto_0
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v1, v2}, Lcom/squareup/payment/Transaction;->performNoSalePaymentAndReset(Lcom/squareup/payment/tender/TenderFactory;)V

    if-eqz v0, :cond_1

    .line 588
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->flow:Lflow/Flow;

    invoke-static {}, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;->forLoadTicket()Lcom/squareup/ui/ticket/MasterDetailTicketScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method private showMasterDetailTicketScreen()Z
    .locals 1

    .line 593
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 594
    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private startSeparatedPrintoutsFlow(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 494
    invoke-static {p1, p2, p3, p4, p5}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;->forUnsavedOpenTicket(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object p1

    .line 496
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->sellerScopeRunner:Lcom/squareup/ui/seller/SellerScopeRunner;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/seller/SellerScopeRunner;->startSeparatedPrintouts(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V

    return-void
.end method

.method private updateChargeButtonCopy(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;Z)V
    .locals 4

    .line 566
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeAndTicketButtonFormatter:Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;

    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 568
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 569
    invoke-interface {v2}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v2

    iget-object v3, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    .line 570
    invoke-virtual {v3}, Lcom/squareup/cashdrawer/CashDrawerTracker;->hasAvailableCashDrawers()Z

    move-result v3

    .line 566
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->formatChargeButtonCopy(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;

    move-result-object v0

    .line 573
    invoke-virtual {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    iget-object v2, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;->title:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$ChargeButtonCopy;->subtitle:Ljava/lang/String;

    invoke-virtual {v1, p1, p2, v2, v0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->updateChargeButton(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private updateChargeStateAndButton(ZZ)V
    .locals 4

    .line 533
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    sget-object v1, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CONFIRMING:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    if-ne v0, v1, :cond_0

    const/4 p1, 0x0

    .line 535
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->glassController:Lcom/squareup/widgets/glass/GlassConfirmController;

    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmController;->removeGlass()Z

    :cond_0
    if-eqz p2, :cond_1

    .line 539
    sget-object p2, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->DISABLED:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    goto :goto_0

    .line 540
    :cond_1
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_2

    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {p2}, Lcom/squareup/cashdrawer/CashDrawerTracker;->hasAvailableCashDrawers()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 542
    sget-object p2, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CONFIRMABLE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    goto :goto_0

    .line 543
    :cond_2
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p2, v0, v2

    if-gtz p2, :cond_6

    .line 546
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->hasDiscounts()Z

    move-result p2

    if-nez p2, :cond_3

    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result p2

    if-eqz p2, :cond_4

    :cond_3
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->hasItems()Z

    move-result p2

    if-nez p2, :cond_4

    .line 548
    sget-object p2, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->DISABLED:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    goto :goto_0

    .line 549
    :cond_4
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_5

    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result p2

    if-nez p2, :cond_5

    .line 551
    sget-object p2, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CONFIRMABLE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    goto :goto_0

    .line 555
    :cond_5
    sget-object p2, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CHARGE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    goto :goto_0

    .line 559
    :cond_6
    sget-object p2, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CHARGE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    .line 562
    :goto_0
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    invoke-direct {p0, p2, p1}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->updateChargeButtonCopy(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;Z)V

    return-void
.end method

.method private updateTicketStateAndButton(ZZ)V
    .locals 3

    .line 501
    invoke-virtual {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    .line 502
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v1}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 503
    invoke-virtual {v0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->hideTicketsButton()V

    return-void

    .line 507
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->showTicketsButton()V

    .line 510
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 511
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz p2, :cond_2

    .line 514
    sget-object p2, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->DISABLED:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->ticketState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_3

    .line 516
    sget-object p2, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->TICKETS:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->ticketState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    goto :goto_1

    .line 518
    :cond_3
    sget-object p2, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->SAVE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->ticketState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    .line 521
    :goto_1
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeAndTicketButtonFormatter:Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;

    iget-object v2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 522
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getUnlockedItemCount()I

    move-result v2

    .line 521
    invoke-virtual {p2, v1, v2}, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;->formatTicketButtonCopy(ZI)Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$TicketButtonCopy;

    move-result-object p2

    .line 524
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->ticketState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    iget-object v2, p2, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$TicketButtonCopy;->title:Ljava/lang/String;

    iget-object p2, p2, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter$TicketButtonCopy;->subtitle:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, v2, p2}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->updateTicketsButton(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public cashDrawerConnected(Lcom/squareup/cashdrawer/CashDrawer;)V
    .locals 1

    .line 281
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->updateWithAnimation:Lcom/jakewharton/rxrelay2/PublishRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public cashDrawerDisconnected(Lcom/squareup/cashdrawer/CashDrawer;Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V
    .locals 0

    .line 287
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->updateWithAnimation:Lcom/jakewharton/rxrelay2/PublishRelay;

    const/4 p2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public cashDrawersOpened()V
    .locals 0

    return-void
.end method

.method public synthetic lambda$null$0$ChargeAndTicketButtonsPresenter(Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 249
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 250
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryScreenState;->clearPendingTicketSavedAlert()V

    .line 251
    invoke-virtual {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    invoke-virtual {p1}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->initializeToTicketSavedAnimation()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$4$ChargeAndTicketButtonsPresenter(Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 265
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->updateChargeStateAndButton(ZZ)V

    .line 266
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->updateTicketStateAndButton(ZZ)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$ChargeAndTicketButtonsPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 247
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->ticketSavedAlertIsPending()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketButtonsPresenter$B9OAY67AIQeU9zhcanPbQZtQlSw;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketButtonsPresenter$B9OAY67AIQeU9zhcanPbQZtQlSw;-><init>(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;)V

    .line 248
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$5$ChargeAndTicketButtonsPresenter()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 257
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->updateWithAnimation:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 259
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->onCustomerChanged()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketButtonsPresenter$-wJtOBaceR_zwVS66npeOaIlBF0;->INSTANCE:Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketButtonsPresenter$-wJtOBaceR_zwVS66npeOaIlBF0;

    invoke-virtual {v1, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    invoke-static {v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->mergeWith(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v2, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    .line 260
    invoke-virtual {v1, v2}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketButtonsPresenter$hA9-Lk_rUuHDzP1Ogy7H_I-xesc;->INSTANCE:Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketButtonsPresenter$hA9-Lk_rUuHDzP1Ogy7H_I-xesc;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->mergeWith(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x0

    .line 261
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 262
    invoke-direct {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->isChargeDisabledByBran()Lio/reactivex/Observable;

    move-result-object v1

    .line 263
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    .line 257
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketButtonsPresenter$3sWlaYlc_WZ6klM1BcTv0D4O63Q;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketButtonsPresenter$3sWlaYlc_WZ6klM1BcTv0D4O63Q;-><init>(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;)V

    .line 264
    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method onCancelConfirmClicked()V
    .locals 3

    .line 469
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    sget-object v1, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CONFIRMING:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    if-ne v0, v1, :cond_1

    .line 472
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->glassController:Lcom/squareup/widgets/glass/GlassConfirmController;

    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmController;->removeGlass()Z

    .line 476
    invoke-virtual {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->updateWithAnimation:Lcom/jakewharton/rxrelay2/PublishRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void

    .line 470
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot cancel CONFIRMING from state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    invoke-virtual {v2}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method onChargeClicked()V
    .locals 3

    .line 303
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-eq v0, v1, :cond_0

    return-void

    .line 307
    :cond_0
    invoke-direct {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->logItemizedCheckout()V

    .line 309
    sget-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$2;->$SwitchMap$com$squareup$orderentry$ChargeAndTicketButtonsPresenter$ChargeState:[I

    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    invoke-virtual {v1}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    return-void

    .line 385
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Um, how did you click a disabled charge button??"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 361
    :cond_2
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->glassController:Lcom/squareup/widgets/glass/GlassConfirmController;

    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmController;->removeGlass()Z

    .line 362
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_PAD_CHARGE_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 364
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 366
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$1;

    invoke-direct {v2, p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$1;-><init>(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void

    .line 379
    :cond_3
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->openAllCashDrawers()V

    .line 380
    invoke-direct {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->performNoSalePaymentAndReset()V

    return-void

    .line 353
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    .line 354
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->glassController:Lcom/squareup/widgets/glass/GlassConfirmController;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->installGlassCancel(Lcom/squareup/widgets/glass/GlassConfirmController;)V

    .line 355
    invoke-virtual {v0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->animateConfirmChargeBeforeSettingText()V

    .line 356
    sget-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CONFIRMING:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    iput-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    .line 357
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->chargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->updateChargeButtonCopy(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;Z)V

    return-void

    .line 314
    :cond_5
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {v0}, Lcom/squareup/cardreader/DippedCardTracker;->mustReinsertDippedCard()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 315
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastReinsertChipCardToCharge()Z

    return-void

    .line 320
    :cond_6
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    invoke-interface {v0}, Lcom/squareup/ui/main/TransactionMetrics;->beginTransaction()V

    .line 321
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_PAD_CHARGE_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 323
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->attributeChargeIfPossible()V

    .line 325
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 326
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isCardInsertedOnCartMonitor()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 327
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastRemoveChipCard()Z

    goto :goto_0

    .line 329
    :cond_7
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0}, Lcom/squareup/ui/tender/TenderStarter;->startTenderFlow()V

    :goto_0
    return-void

    .line 334
    :cond_8
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 335
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/payment/SwipeHandler;->authorize()V

    return-void

    .line 339
    :cond_9
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->cardOptionShouldBeEnabled()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 340
    invoke-virtual {v0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkCanStartSingleTenderEmvInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    if-ne v0, v1, :cond_a

    .line 341
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 342
    invoke-virtual {v0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getSingleTenderDipWithChargeButtonResult()Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object v0

    .line 343
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    return-void

    .line 347
    :cond_a
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0}, Lcom/squareup/ui/tender/TenderStarter;->startTenderFlow()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 231
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {p1, p0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->addListener(Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 271
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->glassController:Lcom/squareup/widgets/glass/GlassConfirmController;

    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmController;->removeGlass()Z

    .line 272
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {v0, p0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->removeListener(Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 235
    invoke-virtual {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    .line 238
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->device:Lcom/squareup/util/Device;

    .line 239
    invoke-interface {v0}, Lcom/squareup/util/Device;->isPortrait()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 240
    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    invoke-virtual {p1}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->useLargerChargeTextAppearance()V

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketButtonsPresenter$URvjXFCqKpL2ipFUreU35RiPExI;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketButtonsPresenter$URvjXFCqKpL2ipFUreU35RiPExI;-><init>(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 256
    :cond_1
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketButtonsPresenter$C1QssxSEB7DMAym4zsmM_TNG5Qw;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketButtonsPresenter$C1QssxSEB7DMAym4zsmM_TNG5Qw;-><init>(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method onTicketsClicked()V
    .locals 8

    .line 392
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 398
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-eq v0, v1, :cond_0

    return-void

    .line 402
    :cond_0
    sget-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$2;->$SwitchMap$com$squareup$orderentry$ChargeAndTicketButtonsPresenter$TicketState:[I

    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->ticketState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    invoke-virtual {v1}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_7

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    .line 412
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 414
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 415
    invoke-virtual {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    invoke-virtual {v0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->animateToTicketSavedAndBack()V

    .line 418
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/print/OrderPrintingDispatcher;->getSnapshotOfOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/OrderSnapshot;

    move-result-object v2

    .line 419
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getDisplayNameOrDefault()Ljava/lang/String;

    move-result-object v3

    .line 420
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCurrentTicket()Lcom/squareup/tickets/OpenTicket;

    move-result-object v0

    invoke-static {v0}, Landroidx/core/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/tickets/OpenTicket;

    .line 421
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getUnsavedVoidedItems()Ljava/util/List;

    move-result-object v5

    .line 422
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getUniqueKey()Ljava/lang/String;

    move-result-object v6

    .line 424
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->hasSeparatedPrintouts:Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;

    .line 425
    invoke-interface {v0, v2, v4, v5}, Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;->hasPrintoutsForUnsavedOpenTicket(Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 430
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/print/OrderPrintingDispatcher;->printStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    .line 431
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v1}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 432
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {v1, v2, v4, v5}, Lcom/squareup/print/OrderPrintingDispatcher;->printVoidTicket(Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)V

    .line 438
    :cond_2
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->updateCurrentTicketBeforeReset()V

    .line 440
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->badBus:Lcom/squareup/badbus/BadBus;

    new-instance v7, Lcom/squareup/payment/OrderEntryEvents$TicketSaved;

    invoke-direct {v7}, Lcom/squareup/payment/OrderEntryEvents$TicketSaved;-><init>()V

    invoke-virtual {v1, v7}, Lcom/squareup/badbus/BadBus;->post(Ljava/lang/Object;)V

    .line 441
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->reset()V

    if-eqz v0, :cond_3

    move-object v1, p0

    .line 446
    invoke-direct/range {v1 .. v6}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->startSeparatedPrintoutsFlow(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/lang/String;)V

    goto :goto_1

    .line 451
    :cond_3
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->sellerScopeRunner:Lcom/squareup/ui/seller/SellerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/seller/SellerScopeRunner;->goToScreenAfterSaveOpenTicket()V

    goto :goto_1

    .line 456
    :cond_4
    invoke-direct {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->showMasterDetailTicketScreen()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 457
    invoke-static {}, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;->forSaveToTicket()Lcom/squareup/ui/ticket/MasterDetailTicketScreen;

    move-result-object v0

    goto :goto_0

    .line 458
    :cond_5
    invoke-static {}, Lcom/squareup/ui/ticket/TicketListScreen;->forSaveToTicket()Lcom/squareup/ui/ticket/TicketListScreen;

    move-result-object v0

    .line 459
    :goto_0
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->flow:Lflow/Flow;

    invoke-virtual {v1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_1
    return-void

    .line 464
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected TicketState "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->ticketState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    invoke-virtual {v2}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 405
    :cond_7
    invoke-direct {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->showMasterDetailTicketScreen()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 406
    invoke-static {}, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;->forLoadTicket()Lcom/squareup/ui/ticket/MasterDetailTicketScreen;

    move-result-object v0

    goto :goto_2

    .line 407
    :cond_8
    invoke-static {}, Lcom/squareup/ui/ticket/TicketListScreen;->forLoadTicket()Lcom/squareup/ui/ticket/TicketListScreen;

    move-result-object v0

    .line 408
    :goto_2
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->flow:Lflow/Flow;

    invoke-virtual {v1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 393
    :cond_9
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot interact with tickets button without tickets on!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
