.class public final Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;
.super Ljava/lang/Object;
.source "CheckoutLibraryListAssistant_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/CheckoutLibraryListAssistant;",
        ">;"
    }
.end annotation


# instance fields
.field private final itemsAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionInteractionsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletGateway;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;->itemsAppletGatewayProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletGateway;",
            ">;)",
            "Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/ui/items/ItemsAppletGateway;)Lcom/squareup/orderentry/CheckoutLibraryListAssistant;
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/orderentry/CheckoutLibraryListAssistant;-><init>(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/ui/items/ItemsAppletGateway;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/CheckoutLibraryListAssistant;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v1, p0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/log/cart/TransactionInteractionsLogger;

    iget-object v2, p0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;->itemsAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/items/ItemsAppletGateway;

    invoke-static {v0, v1, v2}, Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;->newInstance(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/ui/items/ItemsAppletGateway;)Lcom/squareup/orderentry/CheckoutLibraryListAssistant;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/orderentry/CheckoutLibraryListAssistant_Factory;->get()Lcom/squareup/orderentry/CheckoutLibraryListAssistant;

    move-result-object v0

    return-object v0
.end method
