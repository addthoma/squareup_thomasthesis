.class public Lcom/squareup/orderentry/LibraryBarPhonePresenter;
.super Lmortar/ViewPresenter;
.source "LibraryBarPhonePresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/LibraryBarPhone;",
        ">;"
    }
.end annotation


# instance fields
.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private final categoryDropDownPresenter:Lcom/squareup/orderentry/CategoryDropDownPresenter;

.field private final libraryListPresenter:Lcom/squareup/orderentry/CheckoutLibraryListPresenter;

.field private final libraryListStateManager:Lcom/squareup/librarylist/LibraryListStateManager;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/CheckoutLibraryListPresenter;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/orderentry/CategoryDropDownPresenter;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->libraryListPresenter:Lcom/squareup/orderentry/CheckoutLibraryListPresenter;

    .line 36
    iput-object p2, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 37
    iput-object p3, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->categoryDropDownPresenter:Lcom/squareup/orderentry/CategoryDropDownPresenter;

    .line 38
    iput-object p4, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->libraryListStateManager:Lcom/squareup/librarylist/LibraryListStateManager;

    .line 39
    iput-object p5, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->res:Lcom/squareup/util/Res;

    .line 40
    iput-object p6, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/orderentry/LibraryBarPhone;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 51
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    invoke-virtual {p0, p2, p1}, Lcom/squareup/orderentry/LibraryBarPhone;->setSearch(ZZ)V

    return-void
.end method

.method static synthetic lambda$null$2(Lcom/squareup/orderentry/LibraryBarPhone;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/lang/Boolean;)V
    .locals 0

    .line 60
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    invoke-virtual {p0, p2, p1}, Lcom/squareup/orderentry/LibraryBarPhone;->toggleSearchButton(ZZ)V

    return-void
.end method

.method static synthetic lambda$null$4(Lcom/squareup/orderentry/LibraryBarPhone;Ljava/lang/Boolean;)V
    .locals 0

    .line 67
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/LibraryBarPhone;->setCategoryDropDownEnabled(Z)V

    return-void
.end method


# virtual methods
.method public categoryDropDownClicked()V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->categoryDropDownPresenter:Lcom/squareup/orderentry/CategoryDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->toggle()V

    return-void
.end method

.method public synthetic lambda$null$6$LibraryBarPhonePresenter(Lcom/squareup/orderentry/LibraryBarPhone;Lcom/squareup/librarylist/CheckoutLibraryListState;)V
    .locals 3

    .line 72
    invoke-virtual {p2}, Lcom/squareup/librarylist/CheckoutLibraryListState;->getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v0

    sget-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;

    if-ne v0, v1, :cond_0

    .line 73
    invoke-virtual {p2}, Lcom/squareup/librarylist/CheckoutLibraryListState;->getCurrentCategoryName()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->res:Lcom/squareup/util/Res;

    .line 75
    invoke-virtual {p2}, Lcom/squareup/librarylist/CheckoutLibraryListState;->getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object p2

    iget-object v1, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    const/4 v2, 0x1

    invoke-virtual {p2, v1, v2}, Lcom/squareup/librarylist/LibraryListState$Filter;->getStringResId(Lcom/squareup/catalogapi/CatalogIntegrationController;Z)I

    move-result p2

    .line 74
    invoke-interface {v0, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 72
    :goto_0
    invoke-virtual {p1, p2}, Lcom/squareup/orderentry/LibraryBarPhone;->setLibraryBarTitle(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$LibraryBarPhonePresenter(Lcom/squareup/orderentry/LibraryBarPhone;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 49
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 50
    iget-object v1, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->librarySearchIsActive()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$lcVvTqSTIMpLMkTl66iItXJFhLo;

    invoke-direct {v2, p1, v0}, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$lcVvTqSTIMpLMkTl66iItXJFhLo;-><init>(Lcom/squareup/orderentry/LibraryBarPhone;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 51
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const/4 v1, 0x1

    .line 52
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$LibraryBarPhonePresenter(Lcom/squareup/orderentry/LibraryBarPhone;)Lrx/Subscription;
    .locals 3

    .line 58
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 59
    iget-object v1, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->categoryDropDownPresenter:Lcom/squareup/orderentry/CategoryDropDownPresenter;

    invoke-virtual {v1}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->dropDownIsOpen()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$5nsaCtYFhlkJmVy0fKWSFlBLQqw;

    invoke-direct {v2, p1, v0}, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$5nsaCtYFhlkJmVy0fKWSFlBLQqw;-><init>(Lcom/squareup/orderentry/LibraryBarPhone;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 60
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    const/4 v1, 0x1

    .line 61
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$LibraryBarPhonePresenter(Lcom/squareup/orderentry/LibraryBarPhone;)Lrx/Subscription;
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->categoryDropDownPresenter:Lcom/squareup/orderentry/CategoryDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->libraryIsEmpty()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$Gm4oEq3Kf8donv1dY2-3K0OQK1k;

    invoke-direct {v1, p1}, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$Gm4oEq3Kf8donv1dY2-3K0OQK1k;-><init>(Lcom/squareup/orderentry/LibraryBarPhone;)V

    .line 67
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$7$LibraryBarPhonePresenter(Lcom/squareup/orderentry/LibraryBarPhone;)Lrx/Subscription;
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->libraryListStateManager:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListStateManager;->holder()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$JxMckLr9Bm__vr6wcXjYgmvBcis;

    invoke-direct {v1, p0, p1}, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$JxMckLr9Bm__vr6wcXjYgmvBcis;-><init>(Lcom/squareup/orderentry/LibraryBarPhonePresenter;Lcom/squareup/orderentry/LibraryBarPhone;)V

    .line 71
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 44
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {p0}, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/LibraryBarPhone;

    .line 47
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$2_OqGycuGhAZ9jmoMNRcumB8_fY;

    invoke-direct {v0, p0, p1}, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$2_OqGycuGhAZ9jmoMNRcumB8_fY;-><init>(Lcom/squareup/orderentry/LibraryBarPhonePresenter;Lcom/squareup/orderentry/LibraryBarPhone;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 56
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$7SRe1kWUiWreF544egSB6VeoYmU;

    invoke-direct {v0, p0, p1}, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$7SRe1kWUiWreF544egSB6VeoYmU;-><init>(Lcom/squareup/orderentry/LibraryBarPhonePresenter;Lcom/squareup/orderentry/LibraryBarPhone;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 65
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$T_rAsnc4wUShNJYhs8rPXQruBRg;

    invoke-direct {v0, p0, p1}, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$T_rAsnc4wUShNJYhs8rPXQruBRg;-><init>(Lcom/squareup/orderentry/LibraryBarPhonePresenter;Lcom/squareup/orderentry/LibraryBarPhone;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 69
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$7eondQxY0z5K3cOeWgNPj1_KYUw;

    invoke-direct {v0, p0, p1}, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhonePresenter$7eondQxY0z5K3cOeWgNPj1_KYUw;-><init>(Lcom/squareup/orderentry/LibraryBarPhonePresenter;Lcom/squareup/orderentry/LibraryBarPhone;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onTextSearched(Ljava/lang/String;)V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->setHasLibrarySearchText(Z)V

    .line 88
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->libraryListPresenter:Lcom/squareup/orderentry/CheckoutLibraryListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->newLibrarySearch(Ljava/lang/String;)V

    return-void
.end method

.method protected searchClicked()V
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->setLibrarySearchActive(Z)V

    return-void
.end method

.method protected searchExitClicked()V
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->setLibrarySearchActive(Z)V

    return-void
.end method
