.class public final Lcom/squareup/container/DefaultBackPressedHandlerKt;
.super Ljava/lang/Object;
.source "DefaultBackPressedHandler.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\"!\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006*\"\u0010\u0007\"\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001\u00a8\u0006\u0008"
    }
    d2 = {
        "FinishAfterTransition",
        "Lkotlin/Function1;",
        "Landroid/app/Activity;",
        "",
        "Lcom/squareup/container/DefaultBackPressedHandler;",
        "getFinishAfterTransition",
        "()Lkotlin/jvm/functions/Function1;",
        "DefaultBackPressedHandler",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final FinishAfterTransition:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/app/Activity;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/container/DefaultBackPressedHandlerKt$FinishAfterTransition$1;->INSTANCE:Lcom/squareup/container/DefaultBackPressedHandlerKt$FinishAfterTransition$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    sput-object v0, Lcom/squareup/container/DefaultBackPressedHandlerKt;->FinishAfterTransition:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final getFinishAfterTransition()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/app/Activity;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 16
    sget-object v0, Lcom/squareup/container/DefaultBackPressedHandlerKt;->FinishAfterTransition:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method
