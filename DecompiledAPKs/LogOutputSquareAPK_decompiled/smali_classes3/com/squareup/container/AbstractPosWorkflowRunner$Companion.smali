.class public final Lcom/squareup/container/AbstractPosWorkflowRunner$Companion;
.super Ljava/lang/Object;
.source "AbstractPosWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u001f\u0010\u0003\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0008\u0001\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/container/AbstractPosWorkflowRunner$Companion;",
        "",
        "()V",
        "NoWorkflowSentinel",
        "Lcom/squareup/workflow/rx2/RxWorkflowHost;",
        "",
        "getNoWorkflowSentinel",
        "()Lcom/squareup/workflow/rx2/RxWorkflowHost;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 520
    invoke-direct {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getNoWorkflowSentinel()Lcom/squareup/workflow/rx2/RxWorkflowHost;
    .locals 1

    .line 521
    invoke-static {}, Lcom/squareup/container/AbstractPosWorkflowRunner;->access$getNoWorkflowSentinel$cp()Lcom/squareup/workflow/rx2/RxWorkflowHost;

    move-result-object v0

    return-object v0
.end method
