.class public abstract Lcom/squareup/container/BlockingBootstrapTreeKey;
.super Lcom/squareup/container/ContainerTreeKey;
.source "BlockingBootstrapTreeKey.kt"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008&\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH&J\u000e\u0010\u000c\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bR\u0014\u0010\u0005\u001a\u00020\u0006X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0007\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/container/BlockingBootstrapTreeKey;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "Lcom/squareup/container/RegistersInScope;",
        "Lcom/squareup/container/MaybePersistent;",
        "()V",
        "isPersistent",
        "",
        "()Z",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "register",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final isPersistent:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract doRegister(Lmortar/MortarScope;)V
.end method

.method public final isPersistent()Z
    .locals 1

    .line 22
    iget-boolean v0, p0, Lcom/squareup/container/BlockingBootstrapTreeKey;->isPersistent:Z

    return v0
.end method

.method public final register(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-virtual {p0, p1}, Lcom/squareup/container/BlockingBootstrapTreeKey;->doRegister(Lmortar/MortarScope;)V

    .line 26
    invoke-virtual {p1}, Lmortar/MortarScope;->destroy()V

    return-void
.end method
