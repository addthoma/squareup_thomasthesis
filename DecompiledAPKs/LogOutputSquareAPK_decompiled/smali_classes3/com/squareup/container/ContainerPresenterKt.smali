.class public final Lcom/squareup/container/ContainerPresenterKt;
.super Ljava/lang/Object;
.source "ContainerPresenter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000f\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0008\u0003*\u0001\u0003\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "CURRENT_SCREEN_KEY",
        "",
        "NULL_LAYER",
        "com/squareup/container/ContainerPresenterKt$NULL_LAYER$1",
        "Lcom/squareup/container/ContainerPresenterKt$NULL_LAYER$1;",
        "container_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CURRENT_SCREEN_KEY:Ljava/lang/String; = "CURRENT_SCREEN_KEY"

.field private static final NULL_LAYER:Lcom/squareup/container/ContainerPresenterKt$NULL_LAYER$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/container/ContainerPresenterKt$NULL_LAYER$1;

    invoke-direct {v0}, Lcom/squareup/container/ContainerPresenterKt$NULL_LAYER$1;-><init>()V

    sput-object v0, Lcom/squareup/container/ContainerPresenterKt;->NULL_LAYER:Lcom/squareup/container/ContainerPresenterKt$NULL_LAYER$1;

    return-void
.end method

.method public static final synthetic access$getNULL_LAYER$p()Lcom/squareup/container/ContainerPresenterKt$NULL_LAYER$1;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/container/ContainerPresenterKt;->NULL_LAYER:Lcom/squareup/container/ContainerPresenterKt$NULL_LAYER$1;

    return-object v0
.end method
