.class public final Lcom/squareup/container/Traversals;
.super Ljava/lang/Object;
.source "Traversals.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u001a\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u000e\u0010\u0003\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00050\u0004\u001a\u0012\u0010\u0006\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0005\u001a\u001a\u0010\u0008\u001a\u00020\u0001*\u00020\u00022\u000e\u0010\u0003\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00050\u0004\u001a\u0012\u0010\t\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0005\u001a\n\u0010\n\u001a\u00020\u0005*\u00020\u000b\u001a\n\u0010\u000c\u001a\u00020\u0002*\u00020\u0002\u00a8\u0006\r"
    }
    d2 = {
        "enteringScope",
        "",
        "Lflow/Traversal;",
        "type",
        "Ljava/lang/Class;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "enteringScreen",
        "screen",
        "leavingScope",
        "leavingScreen",
        "topNonDialog",
        "Lflow/History;",
        "withPreCalcChainOrigin",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final enteringScope(Lflow/Traversal;Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Traversal;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "$this$enteringScope"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object p0, p0, Lflow/Traversal;->destination:Lflow/History;

    const-string v0, "destination"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/container/Traversals;->topNonDialog(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p0

    .line 27
    invoke-virtual {p0, p1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p0

    return p0
.end method

.method public static final enteringScreen(Lflow/Traversal;Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    const-string v0, "$this$enteringScreen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object p0, p0, Lflow/Traversal;->destination:Lflow/History;

    const-string v0, "destination"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/container/Traversals;->topNonDialog(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p0

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static final leavingScope(Lflow/Traversal;Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Traversal;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "$this$leavingScope"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object p0, p0, Lflow/Traversal;->origin:Lflow/History;

    const-string v0, "origin"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/container/Traversals;->topNonDialog(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p0

    return p0
.end method

.method public static final leavingScreen(Lflow/Traversal;Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    const-string v0, "$this$leavingScreen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object p0, p0, Lflow/Traversal;->origin:Lflow/History;

    const-string v0, "origin"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/container/Traversals;->topNonDialog(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p0

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static final topNonDialog(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;
    .locals 2

    const-string v0, "$this$topNonDialog"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-virtual {p0}, Lflow/History;->framesFromTop()Ljava/lang/Iterable;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 15
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey;->isDialogScreen(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string p0, "key"

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 18
    :cond_1
    new-instance p0, Ljava/lang/AssertionError;

    const-string v0, "Every history must have a non-dialog screen."

    invoke-direct {p0, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final withPreCalcChainOrigin(Lflow/Traversal;)Lflow/Traversal;
    .locals 3

    const-string v0, "$this$withPreCalcChainOrigin"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/squareup/container/CalculatedKey;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/squareup/container/CalculatedKey;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/container/CalculatedKey;->precalcOrigin:Lflow/History;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lflow/Traversal;->origin:Lflow/History;

    const-string v1, "origin"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    :goto_0
    new-instance v1, Lflow/Traversal;

    iget-object v2, p0, Lflow/Traversal;->destination:Lflow/History;

    iget-object p0, p0, Lflow/Traversal;->direction:Lflow/Direction;

    invoke-direct {v1, v0, v2, p0}, Lflow/Traversal;-><init>(Lflow/History;Lflow/History;Lflow/Direction;)V

    return-object v1
.end method
