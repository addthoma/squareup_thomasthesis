.class public Lcom/squareup/container/RedirectStep$Result;
.super Ljava/lang/Object;
.source "RedirectStep.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/RedirectStep;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Result"
.end annotation


# instance fields
.field public final command:Lcom/squareup/container/Command;

.field public final logString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/container/Command;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/container/RedirectStep$Result;->logString:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/squareup/container/RedirectStep$Result;->command:Lcom/squareup/container/Command;

    return-void
.end method
