.class public abstract Lcom/squareup/container/ContainerTreeKey;
.super Lflow/path/Path;
.source "ContainerTreeKey.java"

# interfaces
.implements Lcom/squareup/container/TreeKeyLike;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/ContainerTreeKey$PathCreator;
    }
.end annotation


# static fields
.field static final COORDINATOR_SERVICE_NAME:Ljava/lang/String;

.field public static final SERVICE_NAME:Ljava/lang/String;

.field private static final TREE_ROOT:Ljava/lang/Object;


# instance fields
.field private viewState:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 76
    const-class v0, Lcom/squareup/coordinators/CoordinatorProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/container/ContainerTreeKey;->COORDINATOR_SERVICE_NAME:Ljava/lang/String;

    .line 79
    const-class v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/container/ContainerTreeKey;->SERVICE_NAME:Ljava/lang/String;

    .line 80
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/squareup/container/ContainerTreeKey;->TREE_ROOT:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .line 117
    invoke-direct {p0}, Lflow/path/Path;-><init>()V

    .line 118
    instance-of v0, p0, Landroid/os/Parcelable;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/squareup/container/MaybePersistent;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    new-array v2, v2, [Ljava/lang/Object;

    .line 119
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v1, "%s must implement either Parcelable or MaybePersistent"

    .line 118
    invoke-static {v0, v1, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private static buildTreePath(Lcom/squareup/container/TreeKeyLike;Lflow/path/Path$Builder;)V
    .locals 2

    .line 107
    invoke-interface {p0}, Lcom/squareup/container/TreeKeyLike;->getParentKey()Ljava/lang/Object;

    move-result-object v0

    .line 108
    instance-of v1, v0, Lcom/squareup/container/TreeKeyLike;

    if-eqz v1, :cond_0

    .line 109
    check-cast v0, Lcom/squareup/container/TreeKeyLike;

    invoke-static {v0, p1}, Lcom/squareup/container/ContainerTreeKey;->buildTreePath(Lcom/squareup/container/TreeKeyLike;Lflow/path/Path$Builder;)V

    goto :goto_0

    .line 110
    :cond_0
    sget-object v1, Lcom/squareup/container/ContainerTreeKey;->TREE_ROOT:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    .line 114
    :goto_0
    check-cast p0, Lflow/path/Path;

    invoke-virtual {p1, p0}, Lflow/path/Path$Builder;->append(Lflow/path/Path;)V

    return-void

    .line 111
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const-string p0, "All paths should lead to TREE_ROOT. %s doesn\'t"

    .line 112
    invoke-static {p0, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method public static get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">(",
            "Lmortar/MortarScope;",
            ")TT;"
        }
    .end annotation

    .line 86
    sget-object v0, Lcom/squareup/container/ContainerTreeKey;->SERVICE_NAME:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/container/ContainerTreeKey;

    return-object p0
.end method

.method public static hasKey(Lmortar/MortarScope;)Z
    .locals 1

    .line 90
    sget-object v0, Lcom/squareup/container/ContainerTreeKey;->SERVICE_NAME:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmortar/MortarScope;->hasService(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static isDialogScreen(Ljava/lang/Object;)Z
    .locals 1

    .line 94
    const-class v0, Lcom/squareup/container/layer/DialogScreen;

    invoke-static {p0, v0}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    const-class v0, Lcom/squareup/container/layer/DialogCardScreen;

    .line 95
    invoke-static {p0, v0}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static isEnteringScope(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;Ljava/lang/Class;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/ContainerTreeKey;",
            "Lcom/squareup/container/ContainerTreeKey;",
            "Ljava/lang/Class<",
            "*>;)Z"
        }
    .end annotation

    .line 101
    invoke-virtual {p0, p2}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {p1, p2}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public anyElementsNotPersistent()Z
    .locals 3

    .line 300
    invoke-virtual {p0}, Lcom/squareup/container/ContainerTreeKey;->elements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflow/path/Path;

    .line 301
    instance-of v2, v1, Lcom/squareup/container/MaybePersistent;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/squareup/container/MaybePersistent;

    invoke-interface {v1}, Lcom/squareup/container/MaybePersistent;->isPersistent()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public asPath()Lflow/path/Path;
    .locals 0

    return-object p0
.end method

.method public assertInScopeOf(Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)V"
        }
    .end annotation

    .line 268
    invoke-virtual {p0, p1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 269
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 270
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 271
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    const-string p1, "Unexpected screen. Found %s but needed %s"

    .line 270
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public varargs assertInScopeOf([Ljava/lang/Class;)V
    .locals 4

    .line 276
    invoke-virtual {p0, p1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf([Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 277
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 278
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 279
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    const-string p1, "Unexpected screen. Found %s but needed one of %s"

    .line 278
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public assertNotInScopeOf(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)V"
        }
    .end annotation

    .line 284
    invoke-virtual {p0, p1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 285
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "Unexpected screen. Already on %s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method public final build(Lflow/path/Path$Builder;)V
    .locals 0

    .line 151
    invoke-super {p0, p1}, Lflow/path/Path;->build(Lflow/path/Path$Builder;)V

    .line 152
    invoke-static {p0, p1}, Lcom/squareup/container/ContainerTreeKey;->buildTreePath(Lcom/squareup/container/TreeKeyLike;Lflow/path/Path$Builder;)V

    return-void
.end method

.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 3

    .line 165
    invoke-virtual {p1}, Lmortar/MortarScope;->buildChild()Lmortar/MortarScope$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/container/ContainerTreeKey;->SERVICE_NAME:Ljava/lang/String;

    invoke-virtual {p1, v0, p0}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    move-result-object p1

    .line 167
    instance-of v0, p0, Lcom/squareup/coordinators/CoordinatorProvider;

    if-eqz v0, :cond_0

    .line 168
    sget-object v0, Lcom/squareup/container/ContainerTreeKey;->COORDINATOR_SERVICE_NAME:Ljava/lang/String;

    invoke-virtual {p1, v0, p0}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    .line 171
    :cond_0
    instance-of v0, p0, Lcom/squareup/container/RegistersInScope;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 172
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "%s-registers-in-scope"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 174
    new-instance v1, Lcom/squareup/container/ContainerTreeKey$1;

    invoke-direct {v1, p0}, Lcom/squareup/container/ContainerTreeKey$1;-><init>(Lcom/squareup/container/ContainerTreeKey;)V

    invoke-virtual {p1, v0, v1}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Lmortar/Scoped;)Lmortar/MortarScope$Builder;

    :cond_1
    return-object p1
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    return-void
.end method

.method public elementOfType(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 235
    invoke-virtual {p0}, Lcom/squareup/container/ContainerTreeKey;->elements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflow/path/Path;

    .line 236
    invoke-virtual {p1, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 237
    invoke-virtual {p1, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    .line 189
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 190
    :cond_1
    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    .line 191
    invoke-virtual {p0}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public getElements()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lflow/path/Path;",
            ">;"
        }
    .end annotation

    .line 142
    invoke-virtual {p0}, Lcom/squareup/container/ContainerTreeKey;->elements()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 128
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 156
    sget-object v0, Lcom/squareup/container/ContainerTreeKey;->TREE_ROOT:Ljava/lang/Object;

    return-object v0
.end method

.method public getViewState()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 331
    iget-object v0, p0, Lcom/squareup/container/ContainerTreeKey;->viewState:Landroid/util/SparseArray;

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .line 195
    invoke-virtual {p0}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final isAppRoot()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 138
    invoke-virtual {p0}, Lcom/squareup/container/ContainerTreeKey;->elements()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/container/ContainerTreeKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isInScopeOf(Ljava/lang/Class;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)Z"
        }
    .end annotation

    .line 248
    invoke-virtual {p0, p1}, Lcom/squareup/container/ContainerTreeKey;->elementOfType(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public varargs isInScopeOf([Ljava/lang/Class;)Z
    .locals 5

    .line 256
    invoke-virtual {p0}, Lcom/squareup/container/ContainerTreeKey;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflow/path/Path;

    .line 257
    array-length v3, p1

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, p1, v2

    .line 258
    invoke-virtual {v4, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return v2
.end method

.method public setViewState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 326
    iput-object p1, p0, Lcom/squareup/container/ContainerTreeKey;->viewState:Landroid/util/SparseArray;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 199
    invoke-virtual {p0}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/squareup/container/ContainerTreeKey;->viewState:Landroid/util/SparseArray;

    .line 226
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSparseArray(Landroid/util/SparseArray;)V

    .line 227
    invoke-virtual {p0, p1, p2}, Lcom/squareup/container/ContainerTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    return-void
.end method
