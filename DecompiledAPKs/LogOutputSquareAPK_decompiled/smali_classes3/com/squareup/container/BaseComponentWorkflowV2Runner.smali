.class public abstract Lcom/squareup/container/BaseComponentWorkflowV2Runner;
.super Lcom/squareup/container/AbstractPosWorkflowRunner;
.source "BaseComponentWorkflowV2Runner.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C:",
        "Ljava/lang/Object;",
        "I:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/container/AbstractPosWorkflowRunner<",
        "TI;TO;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008&\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u0002*\u0008\u0008\u0002\u0010\u0004*\u00020\u00022:\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u0005B\u00a7\u0001\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0011\u0012\u001c\u0010\u0012\u001a\u0018\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00140\u0013j\u0008\u0012\u0004\u0012\u00028\u0000`\u0015\u0012J\u0010\u0016\u001aF\u0012\u0004\u0012\u00028\u0000\u0012<\u0012:\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u00170\u0013\u0012\u0008\u0008\u0002\u0010\u0018\u001a\u00020\u0019\u0012\u0008\u0008\u0002\u0010\u001a\u001a\u00020\u001b\u00a2\u0006\u0002\u0010\u001cJ\u0010\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)H\u0015J\\\u0010*\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n2,\u0010+\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\nH\u0004R\u0010\u0010\u001d\u001a\u00028\u0000X\u0082.\u00a2\u0006\u0004\n\u0002\u0010\u001eR\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010 \u001a\u00020\u00142\u0006\u0010\u001f\u001a\u00020\u0014@BX\u0084.\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R$\u0010\u0012\u001a\u0018\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00140\u0013j\u0008\u0012\u0004\u0012\u00028\u0000`\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u008e\u0001\u0010#\u001a:\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u00172>\u0010\u001f\u001a:\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u0017@BX\u0084.\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010%RR\u0010\u0016\u001aF\u0012\u0004\u0012\u00028\u0000\u0012<\u0012:\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u00170\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/container/BaseComponentWorkflowV2Runner;",
        "C",
        "",
        "I",
        "O",
        "Lcom/squareup/container/AbstractPosWorkflowRunner;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "serviceName",
        "",
        "nextHistory",
        "Lio/reactivex/Observable;",
        "Lflow/History;",
        "componentClass",
        "Ljava/lang/Class;",
        "viewFactoryFactory",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "Lcom/squareup/container/WorkflowComponentViewFactory;",
        "workflowFactory",
        "Lcom/squareup/workflow/Workflow;",
        "cancelAfterOneResult",
        "",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "(Ljava/lang/String;Lio/reactivex/Observable;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZLkotlinx/coroutines/CoroutineDispatcher;)V",
        "component",
        "Ljava/lang/Object;",
        "<set-?>",
        "viewFactory",
        "getViewFactory",
        "()Lcom/squareup/workflow/WorkflowViewFactory;",
        "workflow",
        "getWorkflow",
        "()Lcom/squareup/workflow/Workflow;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "renderingToLayering",
        "rendering",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private component:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TC;"
        }
    .end annotation
.end field

.field private final componentClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TC;>;"
        }
    .end annotation
.end field

.field private viewFactory:Lcom/squareup/workflow/WorkflowViewFactory;

.field private final viewFactoryFactory:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "TC;",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private workflow:Lcom/squareup/workflow/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Workflow<",
            "-TI;+TO;+",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation
.end field

.field private final workflowFactory:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "TC;",
            "Lcom/squareup/workflow/Workflow<",
            "TI;TO;",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lio/reactivex/Observable;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZLkotlinx/coroutines/CoroutineDispatcher;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/reactivex/Observable<",
            "Lflow/History;",
            ">;",
            "Ljava/lang/Class<",
            "TC;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TC;+",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-TC;+",
            "Lcom/squareup/workflow/Workflow<",
            "-TI;+TO;+",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;>;Z",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ")V"
        }
    .end annotation

    const-string v0, "serviceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextHistory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "componentClass"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactoryFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflowFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0, p1, p2, p6, p7}, Lcom/squareup/container/AbstractPosWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;ZLkotlinx/coroutines/CoroutineDispatcher;)V

    iput-object p3, p0, Lcom/squareup/container/BaseComponentWorkflowV2Runner;->componentClass:Ljava/lang/Class;

    iput-object p4, p0, Lcom/squareup/container/BaseComponentWorkflowV2Runner;->viewFactoryFactory:Lkotlin/jvm/functions/Function1;

    iput-object p5, p0, Lcom/squareup/container/BaseComponentWorkflowV2Runner;->workflowFactory:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lio/reactivex/Observable;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v7, 0x0

    goto :goto_0

    :cond_0
    move v7, p6

    :goto_0
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_1

    .line 20
    invoke-static {}, Lcom/squareup/container/CoroutineDispatcherKt;->getMainImmediateDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    move-object v8, v0

    goto :goto_1

    :cond_1
    move-object/from16 v8, p7

    :goto_1
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/BaseComponentWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZLkotlinx/coroutines/CoroutineDispatcher;)V

    return-void
.end method


# virtual methods
.method protected final getViewFactory()Lcom/squareup/workflow/WorkflowViewFactory;
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/container/BaseComponentWorkflowV2Runner;->viewFactory:Lcom/squareup/workflow/WorkflowViewFactory;

    if-nez v0, :cond_0

    const-string/jumbo v1, "viewFactory"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected final getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Workflow<",
            "TI;TO;",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/container/BaseComponentWorkflowV2Runner;->workflow:Lcom/squareup/workflow/Workflow;

    if-nez v0, :cond_0

    const-string/jumbo v1, "workflow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/container/BaseComponentWorkflowV2Runner;->componentClass:Ljava/lang/Class;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/container/BaseComponentWorkflowV2Runner;->component:Ljava/lang/Object;

    .line 46
    iget-object v0, p0, Lcom/squareup/container/BaseComponentWorkflowV2Runner;->viewFactoryFactory:Lkotlin/jvm/functions/Function1;

    iget-object v1, p0, Lcom/squareup/container/BaseComponentWorkflowV2Runner;->component:Ljava/lang/Object;

    const-string v2, "component"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/WorkflowViewFactory;

    iput-object v0, p0, Lcom/squareup/container/BaseComponentWorkflowV2Runner;->viewFactory:Lcom/squareup/workflow/WorkflowViewFactory;

    .line 47
    iget-object v0, p0, Lcom/squareup/container/BaseComponentWorkflowV2Runner;->workflowFactory:Lkotlin/jvm/functions/Function1;

    iget-object v1, p0, Lcom/squareup/container/BaseComponentWorkflowV2Runner;->component:Ljava/lang/Object;

    if-nez v1, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    iput-object v0, p0, Lcom/squareup/container/BaseComponentWorkflowV2Runner;->workflow:Lcom/squareup/workflow/Workflow;

    .line 48
    invoke-super {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V

    return-void
.end method

.method public bridge synthetic renderingToLayering(Ljava/lang/Object;)Ljava/util/Map;
    .locals 0

    .line 13
    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/squareup/container/BaseComponentWorkflowV2Runner;->renderingToLayering(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method protected final renderingToLayering(Ljava/util/Map;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
