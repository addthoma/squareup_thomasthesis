.class final Lcom/squareup/container/ContainerPresenter$topScreensByLayer$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ContainerPresenter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/ContainerPresenter;->topScreensByLayer(Lflow/History;Lflow/History;)Ljava/util/Collection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lflow/History;",
        "Ljava/util/Collection<",
        "+",
        "Lcom/squareup/container/ContainerTreeKey;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nContainerPresenter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ContainerPresenter.kt\ncom/squareup/container/ContainerPresenter$topScreensByLayer$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 4 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,594:1\n1288#2:595\n1313#2,2:596\n215#2,2:598\n1315#2:600\n1316#2,3:608\n347#3,7:601\n67#4:611\n92#4,3:612\n*E\n*S KotlinDebug\n*F\n+ 1 ContainerPresenter.kt\ncom/squareup/container/ContainerPresenter$topScreensByLayer$1\n*L\n500#1:595\n500#1,2:596\n500#1,2:598\n500#1:600\n500#1,3:608\n500#1,7:601\n501#1:611\n501#1,3:612\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "layerTops",
        "",
        "Lcom/squareup/container/ContainerTreeKey;",
        "V",
        "Lcom/squareup/container/ContainerView;",
        "Lflow/History;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/container/ContainerPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/container/ContainerPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter$topScreensByLayer$1;->this$0:Lcom/squareup/container/ContainerPresenter;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lflow/History;

    invoke-virtual {p0, p1}, Lcom/squareup/container/ContainerPresenter$topScreensByLayer$1;->invoke(Lflow/History;)Ljava/util/Collection;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lflow/History;)Ljava/util/Collection;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Ljava/util/Collection<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$layerTops"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 499
    invoke-virtual {p1}, Lflow/History;->framesFromTop()Ljava/lang/Iterable;

    move-result-object p1

    const-string v0, "framesFromTop<ContainerTreeKey>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 595
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 596
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 597
    move-object v2, v1

    check-cast v2, Lcom/squareup/container/ContainerTreeKey;

    .line 500
    iget-object v3, p0, Lcom/squareup/container/ContainerPresenter$topScreensByLayer$1;->this$0:Lcom/squareup/container/ContainerPresenter;

    invoke-static {v3}, Lcom/squareup/container/ContainerPresenter;->access$getLayers$p(Lcom/squareup/container/ContainerPresenter;)Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v3, Ljava/lang/Iterable;

    .line 598
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/container/layer/ContainerLayer;

    .line 500
    invoke-interface {v4, v2}, Lcom/squareup/container/layer/ContainerLayer;->owns(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 601
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    .line 600
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 604
    invoke-interface {v0, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 600
    :cond_2
    check-cast v2, Ljava/util/List;

    .line 608
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 599
    :cond_3
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string v0, "Collection contains no element matching the predicate."

    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 611
    :cond_4
    new-instance p1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 612
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 501
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/container/ContainerTreeKey;

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 614
    :cond_5
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    return-object p1
.end method
