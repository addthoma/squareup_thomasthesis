.class final Lcom/squareup/container/ContainerPresenter$onLoad$2;
.super Ljava/lang/Object;
.source "ContainerPresenter.kt"

# interfaces
.implements Lflow/Dispatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/ContainerPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "V",
        "Lcom/squareup/container/ContainerView;",
        "realTraversal",
        "Lflow/Traversal;",
        "callback",
        "Lflow/TraversalCallback;",
        "dispatch"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/container/ContainerPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/container/ContainerPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter$onLoad$2;->this$0:Lcom/squareup/container/ContainerPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final dispatch(Lflow/Traversal;Lflow/TraversalCallback;)V
    .locals 1

    const-string v0, "realTraversal"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    invoke-static {p1}, Lcom/squareup/container/Traversals;->withPreCalcChainOrigin(Lflow/Traversal;)Lflow/Traversal;

    move-result-object p1

    .line 186
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter$onLoad$2;->this$0:Lcom/squareup/container/ContainerPresenter;

    invoke-static {v0, p1, p2}, Lcom/squareup/container/ContainerPresenter;->access$dispatch(Lcom/squareup/container/ContainerPresenter;Lflow/Traversal;Lflow/TraversalCallback;)V

    return-void
.end method
