.class public final Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;
.super Ljava/lang/Object;
.source "SpotHelper.kt"

# interfaces
.implements Lcom/squareup/util/OnMeasuredCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/spot/SpotHelper$Companion;->runAnimation(Lcom/squareup/container/spot/Spot;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lflow/Direction;Lkotlin/jvm/functions/Function0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0007H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1",
        "Lcom/squareup/util/OnMeasuredCallback;",
        "onMeasured",
        "",
        "view",
        "Landroid/view/View;",
        "width",
        "",
        "height",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $container:Landroid/view/ViewGroup;

.field final synthetic $direction:Lflow/Direction;

.field final synthetic $from:Landroid/view/View;

.field final synthetic $onAnimationEnd:Lkotlin/jvm/functions/Function0;

.field final synthetic $spot:Lcom/squareup/container/spot/Spot;

.field final synthetic $to:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/container/spot/Spot;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lflow/Direction;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$spot:Lcom/squareup/container/spot/Spot;

    iput-object p2, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$container:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$to:Landroid/view/View;

    iput-object p4, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$from:Landroid/view/View;

    iput-object p5, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$direction:Lflow/Direction;

    iput-object p6, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$onAnimationEnd:Lkotlin/jvm/functions/Function0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMeasured(Landroid/view/View;II)V
    .locals 6

    const-string/jumbo p2, "view"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    new-instance p1, Landroid/animation/AnimatorSet;

    invoke-direct {p1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 164
    iget-object v0, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$spot:Lcom/squareup/container/spot/Spot;

    iget-object v2, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$container:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$to:Landroid/view/View;

    iget-object v4, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$from:Landroid/view/View;

    sget-object p2, Lcom/squareup/container/spot/SpotHelper;->Companion:Lcom/squareup/container/spot/SpotHelper$Companion;

    iget-object p3, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$direction:Lflow/Direction;

    invoke-virtual {p2, p3}, Lcom/squareup/container/spot/SpotHelper$Companion;->flowToSpotDirection(Lflow/Direction;)Lcom/squareup/container/spot/Spot$Direction;

    move-result-object v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/container/spot/Spot;->addEnterAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lcom/squareup/container/spot/Spot$Direction;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$spot:Lcom/squareup/container/spot/Spot;

    iget-object v2, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$container:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$to:Landroid/view/View;

    iget-object v4, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$from:Landroid/view/View;

    sget-object p2, Lcom/squareup/container/spot/SpotHelper;->Companion:Lcom/squareup/container/spot/SpotHelper$Companion;

    iget-object p3, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$direction:Lflow/Direction;

    invoke-virtual {p2, p3}, Lcom/squareup/container/spot/SpotHelper$Companion;->flowToSpotDirection(Lflow/Direction;)Lcom/squareup/container/spot/Spot$Direction;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/container/spot/Spot;->addExitAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lcom/squareup/container/spot/Spot$Direction;)V

    .line 167
    new-instance p2, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1$onMeasured$1;

    invoke-direct {p2, p0}, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1$onMeasured$1;-><init>(Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;)V

    check-cast p2, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {p1, p2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 177
    invoke-virtual {p1}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method
