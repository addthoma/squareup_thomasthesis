.class Lcom/squareup/container/spot/Spots$BelowSpot;
.super Lcom/squareup/container/spot/Spot;
.source "Spots.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/spot/Spots;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BelowSpot"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/container/spot/Spots$BelowSpot;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/container/spot/Spots$BelowSpot;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 133
    new-instance v0, Lcom/squareup/container/spot/Spots$BelowSpot;

    invoke-direct {v0}, Lcom/squareup/container/spot/Spots$BelowSpot;-><init>()V

    sput-object v0, Lcom/squareup/container/spot/Spots$BelowSpot;->INSTANCE:Lcom/squareup/container/spot/Spots$BelowSpot;

    .line 161
    sget-object v0, Lcom/squareup/container/spot/Spots$BelowSpot;->INSTANCE:Lcom/squareup/container/spot/Spots$BelowSpot;

    invoke-static {v0}, Lcom/squareup/container/spot/Spots$BelowSpot;->forSpotSingleton(Lcom/squareup/container/spot/Spot;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/container/spot/Spots$BelowSpot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    .line 136
    invoke-direct {p0, v0}, Lcom/squareup/container/spot/Spot;-><init>(Z)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/container/spot/Spots$1;)V
    .locals 0

    .line 132
    invoke-direct {p0}, Lcom/squareup/container/spot/Spots$BelowSpot;-><init>()V

    return-void
.end method


# virtual methods
.method protected backwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method protected backwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .line 156
    sget-object p2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 p3, 0x2

    new-array p3, p3, [F

    const/4 v0, 0x0

    const/4 v1, 0x0

    aput v1, p3, v0

    invoke-virtual {p4}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const/4 v1, 0x1

    aput v0, p3, v1

    invoke-static {p4, p2, p3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p2

    .line 157
    invoke-static {p4}, Lcom/squareup/container/spot/Spots$BelowSpot;->shortDuration(Landroid/view/View;)I

    move-result p3

    int-to-long p3, p3

    invoke-virtual {p2, p3, p4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 158
    invoke-virtual {p1, p2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    return-void
.end method

.method protected forwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .line 141
    sget-object p2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 p4, 0x2

    new-array p4, p4, [F

    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const/4 v1, 0x0

    aput v0, p4, v1

    const/4 v0, 0x1

    const/4 v1, 0x0

    aput v1, p4, v0

    invoke-static {p3, p2, p4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p2

    .line 142
    invoke-static {p3}, Lcom/squareup/container/spot/Spots$BelowSpot;->shortDuration(Landroid/view/View;)I

    move-result p3

    int-to-long p3, p3

    invoke-virtual {p2, p3, p4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 143
    invoke-virtual {p1, p2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    return-void
.end method

.method protected forwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    return-void
.end method
