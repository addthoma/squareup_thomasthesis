.class public final Lcom/squareup/container/RenderedPropsWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RenderedPropsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PropsT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        "RenderingT:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "TPropsT;",
        "Lcom/squareup/workflow/Workflow<",
        "-TPropsT;+TOutputT;+TRenderingT;>;TOutputT;",
        "Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering<",
        "TPropsT;TRenderingT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0008\u0008\u0001\u0010\u0002*\u00020\u0003*\u0004\u0008\u0002\u0010\u000428\u0012\u0004\u0012\u0002H\u0001\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00040\u0006\u0012\u0004\u0012\u0002H\u0002\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00040\u00070\u0005:\u0001\u0015B\u001f\u0012\u0018\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0006\u00a2\u0006\u0002\u0010\tJ1\u0010\n\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00062\u0006\u0010\u000b\u001a\u00028\u00002\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0016\u00a2\u0006\u0002\u0010\u000eJa\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020\u00072\u0006\u0010\u000b\u001a\u00028\u00002\u0018\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00062$\u0010\u0011\u001a \u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0006\u0012\u0004\u0012\u00028\u00010\u0012H\u0016\u00a2\u0006\u0002\u0010\u0013J\"\u0010\u0014\u001a\u00020\r2\u0018\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0006H\u0016R \u0010\u0008\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/container/RenderedPropsWorkflow;",
        "PropsT",
        "OutputT",
        "",
        "RenderingT",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/workflow/Workflow;",
        "Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;",
        "wrapped",
        "(Lcom/squareup/workflow/Workflow;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/Workflow;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/Workflow;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;",
        "snapshotState",
        "PropsAndRendering",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final wrapped:Lcom/squareup/workflow/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Workflow<",
            "TPropsT;TOutputT;TRenderingT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/Workflow;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Workflow<",
            "-TPropsT;+TOutputT;+TRenderingT;>;)V"
        }
    .end annotation

    const-string/jumbo v0, "wrapped"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/container/RenderedPropsWorkflow;->wrapped:Lcom/squareup/workflow/Workflow;

    return-void
.end method


# virtual methods
.method public initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/Workflow;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPropsT;",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/Workflow<",
            "TPropsT;TOutputT;TRenderingT;>;"
        }
    .end annotation

    .line 30
    iget-object p1, p0, Lcom/squareup/container/RenderedPropsWorkflow;->wrapped:Lcom/squareup/workflow/Workflow;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/squareup/container/RenderedPropsWorkflow;->initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public render(Ljava/lang/Object;Lcom/squareup/workflow/Workflow;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPropsT;",
            "Lcom/squareup/workflow/Workflow<",
            "-TPropsT;+TOutputT;+TRenderingT;>;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/workflow/Workflow<",
            "TPropsT;TOutputT;TRenderingT;>;-TOutputT;>;)",
            "Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering<",
            "TPropsT;TRenderingT;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "context"

    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    new-instance p2, Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;

    .line 39
    iget-object v1, p0, Lcom/squareup/container/RenderedPropsWorkflow;->wrapped:Lcom/squareup/workflow/Workflow;

    sget-object v0, Lcom/squareup/container/RenderedPropsWorkflow$render$1;->INSTANCE:Lcom/squareup/container/RenderedPropsWorkflow$render$1;

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    move-object v2, p1

    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    .line 37
    invoke-direct {p2, p1, p3}, Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p2
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p2, Lcom/squareup/workflow/Workflow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/container/RenderedPropsWorkflow;->render(Ljava/lang/Object;Lcom/squareup/workflow/Workflow;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/workflow/Workflow;)Lcom/squareup/workflow/Snapshot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Workflow<",
            "-TPropsT;+TOutputT;+TRenderingT;>;)",
            "Lcom/squareup/workflow/Snapshot;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/workflow/Workflow;

    invoke-virtual {p0, p1}, Lcom/squareup/container/RenderedPropsWorkflow;->snapshotState(Lcom/squareup/workflow/Workflow;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
