.class public abstract Lcom/squareup/container/BootstrapTreeKey;
.super Lcom/squareup/container/ContainerTreeKey;
.source "BootstrapTreeKey.kt"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/BootstrapTreeKey$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008&\u0018\u0000 \u00122\u00020\u00012\u00020\u00022\u00020\u0003:\u0001\u0012B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H&J\u000e\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\nX\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/container/BootstrapTreeKey;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "Lcom/squareup/container/RegistersInScope;",
        "Lcom/squareup/container/MaybePersistent;",
        "()V",
        "directionOverride",
        "Lflow/Direction;",
        "getDirectionOverride",
        "()Lflow/Direction;",
        "hasRegistered",
        "",
        "isPersistent",
        "()Z",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "register",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/container/BootstrapTreeKey$Companion;


# instance fields
.field private hasRegistered:Z

.field private final isPersistent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/container/BootstrapTreeKey$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/BootstrapTreeKey$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/container/BootstrapTreeKey;->Companion:Lcom/squareup/container/BootstrapTreeKey$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    return-void
.end method

.method public static final synthetic access$getHasRegistered$p(Lcom/squareup/container/BootstrapTreeKey;)Z
    .locals 0

    .line 34
    iget-boolean p0, p0, Lcom/squareup/container/BootstrapTreeKey;->hasRegistered:Z

    return p0
.end method

.method public static final synthetic access$setHasRegistered$p(Lcom/squareup/container/BootstrapTreeKey;Z)V
    .locals 0

    .line 34
    iput-boolean p1, p0, Lcom/squareup/container/BootstrapTreeKey;->hasRegistered:Z

    return-void
.end method


# virtual methods
.method public abstract doRegister(Lmortar/MortarScope;)V
.end method

.method public getDirectionOverride()Lflow/Direction;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final isPersistent()Z
    .locals 1

    .line 46
    iget-boolean v0, p0, Lcom/squareup/container/BootstrapTreeKey;->isPersistent:Z

    return v0
.end method

.method public final register(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0, p1}, Lcom/squareup/container/BootstrapTreeKey;->doRegister(Lmortar/MortarScope;)V

    const/4 v0, 0x1

    .line 50
    iput-boolean v0, p0, Lcom/squareup/container/BootstrapTreeKey;->hasRegistered:Z

    .line 51
    invoke-virtual {p1}, Lmortar/MortarScope;->destroy()V

    return-void
.end method
