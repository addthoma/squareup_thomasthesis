.class final Lcom/squareup/container/AbstractPosWorkflowRunner$containerHints$2;
.super Lkotlin/jvm/internal/Lambda;
.source "AbstractPosWorkflowRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;ZLkotlinx/coroutines/CoroutineDispatcher;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0004\u0008\u0002\u0010\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "P",
        "",
        "O",
        "R",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/container/AbstractPosWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$containerHints$2;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/workflow/ui/ContainerHints;
    .locals 3

    .line 224
    new-instance v0, Lcom/squareup/workflow/ui/ContainerHints;

    iget-object v1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$containerHints$2;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    invoke-virtual {v1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->getViewFactory()Lcom/squareup/workflow/WorkflowViewFactory;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistryKt;->asViewRegistry(Lcom/squareup/workflow/WorkflowViewFactory;)Lcom/squareup/workflow/ui/ViewRegistry;

    move-result-object v1

    sget-object v2, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->Companion:Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion;

    check-cast v2, Lcom/squareup/workflow/ui/ViewBinding;

    invoke-static {v1, v2}, Lcom/squareup/workflow/ui/ViewRegistryKt;->plus(Lcom/squareup/workflow/ui/ViewRegistry;Lcom/squareup/workflow/ui/ViewBinding;)Lcom/squareup/workflow/ui/ViewRegistry;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/workflow/ui/ContainerHints;-><init>(Lcom/squareup/workflow/ui/ViewRegistry;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 140
    invoke-virtual {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner$containerHints$2;->invoke()Lcom/squareup/workflow/ui/ContainerHints;

    move-result-object v0

    return-object v0
.end method
