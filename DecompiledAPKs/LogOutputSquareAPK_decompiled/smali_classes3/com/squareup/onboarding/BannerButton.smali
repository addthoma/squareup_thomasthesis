.class public final Lcom/squareup/onboarding/BannerButton;
.super Ljava/lang/Object;
.source "BannerButton.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/BannerButton$Kind;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBannerButton.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BannerButton.kt\ncom/squareup/onboarding/BannerButton\n*L\n1#1,58:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001\u0017B\u000f\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rJ\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u000f\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00062\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\u000e\u0010\u0014\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rJ\t\u0010\u0015\u001a\u00020\u000bH\u00d6\u0001J\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000c\u001a\u00020\rR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/onboarding/BannerButton;",
        "",
        "kind",
        "Lcom/squareup/onboarding/BannerButton$Kind;",
        "(Lcom/squareup/onboarding/BannerButton$Kind;)V",
        "isPresent",
        "",
        "()Z",
        "getKind",
        "()Lcom/squareup/onboarding/BannerButton$Kind;",
        "callToAction",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "component1",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "subtitle",
        "toString",
        "url",
        "Kind",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final isPresent:Z

.field private final kind:Lcom/squareup/onboarding/BannerButton$Kind;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/BannerButton$Kind;)V
    .locals 1

    const-string v0, "kind"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/BannerButton;->kind:Lcom/squareup/onboarding/BannerButton$Kind;

    .line 13
    iget-object p1, p0, Lcom/squareup/onboarding/BannerButton;->kind:Lcom/squareup/onboarding/BannerButton$Kind;

    sget-object v0, Lcom/squareup/onboarding/BannerButton$Kind;->NONE:Lcom/squareup/onboarding/BannerButton$Kind;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/onboarding/BannerButton;->isPresent:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/onboarding/BannerButton;Lcom/squareup/onboarding/BannerButton$Kind;ILjava/lang/Object;)Lcom/squareup/onboarding/BannerButton;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/onboarding/BannerButton;->kind:Lcom/squareup/onboarding/BannerButton$Kind;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/BannerButton;->copy(Lcom/squareup/onboarding/BannerButton$Kind;)Lcom/squareup/onboarding/BannerButton;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final callToAction(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iget-object v0, p0, Lcom/squareup/onboarding/BannerButton;->kind:Lcom/squareup/onboarding/BannerButton$Kind;

    invoke-virtual {v0}, Lcom/squareup/onboarding/BannerButton$Kind;->getCallToAction$onboarding_release()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(kind.callToAction)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final component1()Lcom/squareup/onboarding/BannerButton$Kind;
    .locals 1

    iget-object v0, p0, Lcom/squareup/onboarding/BannerButton;->kind:Lcom/squareup/onboarding/BannerButton$Kind;

    return-object v0
.end method

.method public final copy(Lcom/squareup/onboarding/BannerButton$Kind;)Lcom/squareup/onboarding/BannerButton;
    .locals 1

    const-string v0, "kind"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/onboarding/BannerButton;

    invoke-direct {v0, p1}, Lcom/squareup/onboarding/BannerButton;-><init>(Lcom/squareup/onboarding/BannerButton$Kind;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/onboarding/BannerButton;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/onboarding/BannerButton;

    iget-object v0, p0, Lcom/squareup/onboarding/BannerButton;->kind:Lcom/squareup/onboarding/BannerButton$Kind;

    iget-object p1, p1, Lcom/squareup/onboarding/BannerButton;->kind:Lcom/squareup/onboarding/BannerButton$Kind;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getKind()Lcom/squareup/onboarding/BannerButton$Kind;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/onboarding/BannerButton;->kind:Lcom/squareup/onboarding/BannerButton$Kind;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/onboarding/BannerButton;->kind:Lcom/squareup/onboarding/BannerButton$Kind;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isPresent()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/squareup/onboarding/BannerButton;->isPresent:Z

    return v0
.end method

.method public final subtitle(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lcom/squareup/onboarding/BannerButton;->kind:Lcom/squareup/onboarding/BannerButton$Kind;

    invoke-virtual {v0}, Lcom/squareup/onboarding/BannerButton$Kind;->getSubtitle$onboarding_release()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(kind.subtitle)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BannerButton(kind="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onboarding/BannerButton;->kind:Lcom/squareup/onboarding/BannerButton$Kind;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final url(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/squareup/onboarding/BannerButton;->kind:Lcom/squareup/onboarding/BannerButton$Kind;

    invoke-virtual {v0}, Lcom/squareup/onboarding/BannerButton$Kind;->getUrl$onboarding_release()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method
