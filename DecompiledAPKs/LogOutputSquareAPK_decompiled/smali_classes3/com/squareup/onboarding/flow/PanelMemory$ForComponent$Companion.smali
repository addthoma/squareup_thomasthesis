.class public final Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;
.super Ljava/lang/Object;
.source "PanelMemory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;",
        "",
        "()V",
        "EMPTY",
        "Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;",
        "getEMPTY",
        "()Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;

.field private static final EMPTY:Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 104
    new-instance v0, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;

    invoke-direct {v0}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;->$$INSTANCE:Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;

    .line 105
    sget-object v0, Lcom/squareup/onboarding/flow/PanelMemory$UnmodifiableForComponent;->INSTANCE:Lcom/squareup/onboarding/flow/PanelMemory$UnmodifiableForComponent;

    check-cast v0, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    sput-object v0, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;->EMPTY:Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getEMPTY()Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;
    .locals 1

    .line 105
    sget-object v0, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;->EMPTY:Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    return-object v0
.end method
