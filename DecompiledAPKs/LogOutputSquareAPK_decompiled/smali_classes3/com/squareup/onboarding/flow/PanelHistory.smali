.class public final Lcom/squareup/onboarding/flow/PanelHistory;
.super Ljava/lang/Object;
.source "OnboardingState.kt"

# interfaces
.implements Ljava/lang/Iterable;
.implements Lkotlin/jvm/internal/markers/KMappedMarker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/PanelHistory$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "Lcom/squareup/onboarding/flow/PanelScreen;",
        ">;",
        "Lkotlin/jvm/internal/markers/KMappedMarker;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010(\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u001c2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001cB\u0013\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0004H\u00c2\u0003J\u0019\u0010\u000e\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0004H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0007H\u00d6\u0001J\u0006\u0010\u0014\u001a\u00020\u0010J\u000f\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0016H\u0096\u0003J\u0011\u0010\u0017\u001a\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u0002H\u0086\u0002J\u0006\u0010\u0019\u001a\u00020\u0000J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0006\u001a\u00020\u00078F\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\n\u001a\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/PanelHistory;",
        "",
        "Lcom/squareup/onboarding/flow/PanelScreen;",
        "history",
        "",
        "(Ljava/util/List;)V",
        "size",
        "",
        "getSize",
        "()I",
        "top",
        "getTop",
        "()Lcom/squareup/onboarding/flow/PanelScreen;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "isEmpty",
        "iterator",
        "",
        "plus",
        "screen",
        "pop",
        "toString",
        "",
        "Companion",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/onboarding/flow/PanelHistory$Companion;

.field private static final EMPTY:Lcom/squareup/onboarding/flow/PanelHistory;


# instance fields
.field private final history:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/onboarding/flow/PanelScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onboarding/flow/PanelHistory$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onboarding/flow/PanelHistory$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onboarding/flow/PanelHistory;->Companion:Lcom/squareup/onboarding/flow/PanelHistory$Companion;

    .line 53
    new-instance v0, Lcom/squareup/onboarding/flow/PanelHistory;

    sget-object v1, Lcom/squareup/onboarding/flow/PanelScreen;->Companion:Lcom/squareup/onboarding/flow/PanelScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/onboarding/flow/PanelScreen$Companion;->getEMPTY()Lcom/squareup/onboarding/flow/PanelScreen;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/onboarding/flow/PanelHistory;-><init>(Ljava/util/List;)V

    sput-object v0, Lcom/squareup/onboarding/flow/PanelHistory;->EMPTY:Lcom/squareup/onboarding/flow/PanelHistory;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/onboarding/flow/PanelScreen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/PanelHistory;->history:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$getEMPTY$cp()Lcom/squareup/onboarding/flow/PanelHistory;
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/onboarding/flow/PanelHistory;->EMPTY:Lcom/squareup/onboarding/flow/PanelHistory;

    return-object v0
.end method

.method private final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/onboarding/flow/PanelScreen;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/onboarding/flow/PanelHistory;->history:Ljava/util/List;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/onboarding/flow/PanelHistory;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/onboarding/flow/PanelHistory;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/onboarding/flow/PanelHistory;->history:Ljava/util/List;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/PanelHistory;->copy(Ljava/util/List;)Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Ljava/util/List;)Lcom/squareup/onboarding/flow/PanelHistory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/onboarding/flow/PanelScreen;",
            ">;)",
            "Lcom/squareup/onboarding/flow/PanelHistory;"
        }
    .end annotation

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/onboarding/flow/PanelHistory;

    invoke-direct {v0, p1}, Lcom/squareup/onboarding/flow/PanelHistory;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/onboarding/flow/PanelHistory;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/onboarding/flow/PanelHistory;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/PanelHistory;->history:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/onboarding/flow/PanelHistory;->history:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getSize()I
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/onboarding/flow/PanelHistory;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getTop()Lcom/squareup/onboarding/flow/PanelScreen;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/onboarding/flow/PanelHistory;->history:Ljava/util/List;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/onboarding/flow/PanelScreen;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/onboarding/flow/PanelHistory;->history:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isEmpty()Z
    .locals 2

    .line 46
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/PanelHistory;->getSize()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    .line 48
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/PanelHistory;->getTop()Lcom/squareup/onboarding/flow/PanelScreen;

    move-result-object v0

    sget-object v1, Lcom/squareup/onboarding/flow/PanelScreen;->Companion:Lcom/squareup/onboarding/flow/PanelScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/onboarding/flow/PanelScreen$Companion;->getEMPTY()Lcom/squareup/onboarding/flow/PanelScreen;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    :cond_1
    :goto_0
    return v1
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/squareup/onboarding/flow/PanelScreen;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/onboarding/flow/PanelHistory;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final plus(Lcom/squareup/onboarding/flow/PanelScreen;)Lcom/squareup/onboarding/flow/PanelHistory;
    .locals 2

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lcom/squareup/onboarding/flow/PanelHistory;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/PanelHistory;->history:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/onboarding/flow/PanelHistory;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public final pop()Lcom/squareup/onboarding/flow/PanelHistory;
    .locals 3

    .line 43
    new-instance v0, Lcom/squareup/onboarding/flow/PanelHistory;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/PanelHistory;->history:Ljava/util/List;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->dropLast(Ljava/util/List;I)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/onboarding/flow/PanelHistory;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PanelHistory(history="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/PanelHistory;->history:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
