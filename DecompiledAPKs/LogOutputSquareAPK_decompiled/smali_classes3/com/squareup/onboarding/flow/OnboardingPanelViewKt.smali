.class public final Lcom/squareup/onboarding/flow/OnboardingPanelViewKt;
.super Ljava/lang/Object;
.source "OnboardingPanelView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0004"
    }
    d2 = {
        "KEY_MEMORY",
        "",
        "KEY_SUPER_STATE",
        "KEY_TIMEOUT",
        "onboarding_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final KEY_MEMORY:Ljava/lang/String; = "memory"

.field private static final KEY_SUPER_STATE:Ljava/lang/String; = "super_state"

.field private static final KEY_TIMEOUT:Ljava/lang/String; = "timeout"
