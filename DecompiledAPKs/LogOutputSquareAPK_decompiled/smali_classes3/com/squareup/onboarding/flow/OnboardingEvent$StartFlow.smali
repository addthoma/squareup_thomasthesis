.class public final Lcom/squareup/onboarding/flow/OnboardingEvent$StartFlow;
.super Lcom/squareup/onboarding/flow/OnboardingEvent;
.source "OnboardingEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/OnboardingEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StartFlow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingEvent$StartFlow;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        "flow",
        "Lcom/squareup/onboarding/OnboardingFlow;",
        "(Lcom/squareup/onboarding/OnboardingFlow;)V",
        "getFlow",
        "()Lcom/squareup/onboarding/OnboardingFlow;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final flow:Lcom/squareup/onboarding/OnboardingFlow;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/OnboardingFlow;)V
    .locals 1

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0, v0}, Lcom/squareup/onboarding/flow/OnboardingEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingEvent$StartFlow;->flow:Lcom/squareup/onboarding/OnboardingFlow;

    return-void
.end method


# virtual methods
.method public final getFlow()Lcom/squareup/onboarding/OnboardingFlow;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingEvent$StartFlow;->flow:Lcom/squareup/onboarding/OnboardingFlow;

    return-object v0
.end method
