.class final Lcom/squareup/onboarding/flow/OnboardingReactor$launch$workflow$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OnboardingReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/OnboardingReactor;->launch(Lcom/squareup/onboarding/flow/OnboardingState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        "kotlin.jvm.PlatformType",
        "state",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onboarding/flow/OnboardingReactor$launch$workflow$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingReactor$launch$workflow$1;

    invoke-direct {v0}, Lcom/squareup/onboarding/flow/OnboardingReactor$launch$workflow$1;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/flow/OnboardingReactor$launch$workflow$1;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingReactor$launch$workflow$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/onboarding/flow/OnboardingState;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            ">;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    instance-of v0, p1, Lcom/squareup/onboarding/flow/OnboardingState$Waiting;

    if-eqz v0, :cond_0

    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    .line 91
    :cond_0
    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 71
    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingState;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor$launch$workflow$1;->invoke(Lcom/squareup/onboarding/flow/OnboardingState;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
