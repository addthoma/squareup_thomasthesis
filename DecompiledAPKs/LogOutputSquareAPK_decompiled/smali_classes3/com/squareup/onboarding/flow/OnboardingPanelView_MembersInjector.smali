.class public final Lcom/squareup/onboarding/flow/OnboardingPanelView_MembersInjector;
.super Ljava/lang/Object;
.source "OnboardingPanelView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/onboarding/flow/OnboardingPanelView;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView_MembersInjector;->mainThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/onboarding/flow/OnboardingPanelView;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingPanelView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/onboarding/flow/OnboardingPanelView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectMainThread(Lcom/squareup/onboarding/flow/OnboardingPanelView;Lcom/squareup/thread/executor/MainThread;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/onboarding/flow/OnboardingPanelView;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView_MembersInjector;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/MainThread;

    invoke-static {p1, v0}, Lcom/squareup/onboarding/flow/OnboardingPanelView_MembersInjector;->injectMainThread(Lcom/squareup/onboarding/flow/OnboardingPanelView;Lcom/squareup/thread/executor/MainThread;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingPanelView;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingPanelView_MembersInjector;->injectMembers(Lcom/squareup/onboarding/flow/OnboardingPanelView;)V

    return-void
.end method
