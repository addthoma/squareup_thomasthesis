.class public final Lcom/squareup/onboarding/flow/data/OnboardingParagraphItem;
.super Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;
.source "OnboardingParagraphItem.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingParagraphItem.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingParagraphItem.kt\ncom/squareup/onboarding/flow/data/OnboardingParagraphItem\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,45:1\n1203#2,2:46\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingParagraphItem.kt\ncom/squareup/onboarding/flow/data/OnboardingParagraphItem\n*L\n20#1,2:46\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006J\u0006\u0010\u0007\u001a\u00020\u0006J\u0006\u0010\u0008\u001a\u00020\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/data/OnboardingParagraphItem;",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
        "component",
        "Lcom/squareup/protos/client/onboard/Component;",
        "(Lcom/squareup/protos/client/onboard/Component;)V",
        "html",
        "",
        "label",
        "style",
        "Lcom/squareup/onboarding/flow/data/ParagraphStyle;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/onboard/Component;)V
    .locals 2

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x2

    .line 11
    invoke-direct {p0, p1, v0, v1, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public final html()Ljava/lang/CharSequence;
    .locals 4

    const/4 v0, 0x0

    const-string v1, "text"

    const/4 v2, 0x2

    .line 15
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/text/html/TagParser;

    new-instance v2, Lcom/squareup/text/html/ListTagParser;

    invoke-direct {v2}, Lcom/squareup/text/html/ListTagParser;-><init>()V

    check-cast v2, Lcom/squareup/text/html/TagParser;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/text/html/HtmlText;->fromHtml(Ljava/lang/String;[Lcom/squareup/text/html/TagParser;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "HtmlText.fromHtml(string\u2026\"text\"), ListTagParser())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final label()Ljava/lang/CharSequence;
    .locals 3

    const/4 v0, 0x0

    const-string v1, "label"

    const/4 v2, 0x2

    .line 13
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final style()Lcom/squareup/onboarding/flow/data/ParagraphStyle;
    .locals 6

    .line 19
    sget-object v0, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->NONE:Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->name()Ljava/lang/String;

    move-result-object v0

    const-string v1, "style"

    invoke-virtual {p0, v1, v0}, Lcom/squareup/onboarding/flow/data/OnboardingParagraphItem;->stringProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.String).toUpperCase()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-static {}, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->values()[Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    move-result-object v1

    .line 46
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 20
    invoke-virtual {v4}, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_2

    goto :goto_2

    :cond_2
    sget-object v4, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->NONE:Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    :goto_2
    return-object v4

    .line 19
    :cond_3
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
