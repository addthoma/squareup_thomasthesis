.class public final Lcom/squareup/onboarding/flow/data/OnboardingEditItem;
.super Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;
.source "OnboardingEditItem.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/data/OnboardingEditItem$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingEditItem.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingEditItem.kt\ncom/squareup/onboarding/flow/data/OnboardingEditItem\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,59:1\n704#2:60\n777#2,2:61\n1360#2:63\n1429#2,3:64\n1866#2,7:67\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingEditItem.kt\ncom/squareup/onboarding/flow/data/OnboardingEditItem\n*L\n29#1:60\n29#1,2:61\n31#1:63\n31#1,3:64\n35#1,7:67\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u0007\u001a\u00020\u0008J\u0006\u0010\t\u001a\u00020\u0008J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0014J\u0006\u0010\u000e\u001a\u00020\u0008J\u0006\u0010\u000f\u001a\u00020\u0010J\u0006\u0010\u0011\u001a\u00020\u0008J\u0006\u0010\u0012\u001a\u00020\u0010J\u000e\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0008\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/data/OnboardingEditItem;",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
        "component",
        "Lcom/squareup/protos/client/onboard/Component;",
        "componentData",
        "Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;",
        "(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V",
        "defaultValue",
        "",
        "format",
        "getValidatorHelper",
        "Lcom/squareup/onboarding/flow/data/OnboardingValidator;",
        "outputs",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;",
        "hint",
        "inputFlags",
        "",
        "label",
        "maxLength",
        "textOutput",
        "Lcom/squareup/protos/client/onboard/Output;",
        "text",
        "Companion",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/onboarding/flow/data/OnboardingEditItem$Companion;

.field private static final TEXT_INPUT_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingEditItem$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->Companion:Lcom/squareup/onboarding/flow/data/OnboardingEditItem$Companion;

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/Pair;

    const/16 v1, 0x81

    .line 51
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "is_password"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const v1, 0x20001

    .line 52
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "is_multiline"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const/4 v1, 0x2

    .line 53
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "is_numeric"

    invoke-static {v3, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1002

    .line 54
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "is_numeric_signed"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const/16 v1, 0x2002

    .line 55
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "is_numeric_decimal"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 50
    invoke-static {v0}, Lkotlin/collections/MapsKt;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->TEXT_INPUT_MAP:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "componentData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    return-void
.end method


# virtual methods
.method public final defaultValue()Ljava/lang/String;
    .locals 4

    .line 38
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->getComponentData()Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "default_text"

    const/4 v3, 0x2

    invoke-static {p0, v2, v1, v3, v1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "text"

    invoke-interface {v0, v2, v1}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final format()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    const-string v1, "format"

    const/4 v2, 0x2

    .line 22
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getValidatorHelper(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;
    .locals 8

    const-string v0, "outputs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingValidator;

    .line 44
    new-instance v1, Lcom/squareup/onboarding/flow/data/OnboardingEditItem$getValidatorHelper$1;

    invoke-direct {v1, p1}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem$getValidatorHelper$1;-><init>(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)V

    move-object v3, v1

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 45
    new-instance v1, Lcom/squareup/onboarding/flow/data/OnboardingEditItem$getValidatorHelper$2;

    invoke-direct {v1, p1}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem$getValidatorHelper$2;-><init>(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)V

    move-object v4, v1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x9

    const/4 v7, 0x0

    move-object v1, v0

    .line 43
    invoke-direct/range {v1 .. v7}, Lcom/squareup/onboarding/flow/data/OnboardingValidator;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final hint()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    const-string v1, "placeholder_text"

    const/4 v2, 0x2

    .line 20
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final inputFlags()I
    .locals 5

    .line 27
    sget-object v0, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->TEXT_INPUT_MAP:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 60
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 61
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    const/4 v4, 0x0

    .line 29
    invoke-virtual {p0, v3, v4}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->booleanProperty(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 62
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 64
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 65
    check-cast v2, Ljava/lang/String;

    .line 31
    sget-object v3, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->TEXT_INPUT_MAP:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 66
    :cond_3
    check-cast v0, Ljava/util/List;

    .line 33
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_4

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_5

    goto :goto_3

    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_3
    check-cast v0, Ljava/lang/Iterable;

    .line 67
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 68
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 69
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 70
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 71
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    or-int/2addr v1, v2

    .line 35
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_4

    .line 73
    :cond_6
    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0

    .line 68
    :cond_7
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Empty collection can\'t be reduced."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final label()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    const-string v1, "label"

    const/4 v2, 0x2

    .line 18
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final maxLength()I
    .locals 2

    const-string v0, "max_length"

    const v1, 0x7fffffff

    .line 24
    invoke-virtual {p0, v0, v1}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->intProperty(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final textOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0, v0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->createOutput$onboarding_release(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    const-string v0, "createOutput(\"text\", text)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
