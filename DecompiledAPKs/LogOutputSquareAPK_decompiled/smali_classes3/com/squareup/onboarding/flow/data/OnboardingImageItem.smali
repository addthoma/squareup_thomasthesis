.class public final Lcom/squareup/onboarding/flow/data/OnboardingImageItem;
.super Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;
.source "OnboardingImageItem.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingImageItem.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingImageItem.kt\ncom/squareup/onboarding/flow/data/OnboardingImageItem\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,30:1\n1203#2,2:31\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingImageItem.kt\ncom/squareup/onboarding/flow/data/OnboardingImageItem\n*L\n17#1,2:31\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006J\u0006\u0010\u0007\u001a\u00020\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/data/OnboardingImageItem;",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
        "component",
        "Lcom/squareup/protos/client/onboard/Component;",
        "(Lcom/squareup/protos/client/onboard/Component;)V",
        "imageSize",
        "Lcom/squareup/onboarding/flow/data/ImageSize;",
        "url",
        "",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/onboard/Component;)V
    .locals 2

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x2

    .line 9
    invoke-direct {p0, p1, v0, v1, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public final imageSize()Lcom/squareup/onboarding/flow/data/ImageSize;
    .locals 7

    const/4 v0, 0x0

    const-string v1, "size"

    const/4 v2, 0x2

    .line 16
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 17
    invoke-static {}, Lcom/squareup/onboarding/flow/data/ImageSize;->values()[Lcom/squareup/onboarding/flow/data/ImageSize;

    move-result-object v2

    .line 31
    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    aget-object v5, v2, v4

    .line 17
    invoke-virtual {v5}, Lcom/squareup/onboarding/flow/data/ImageSize;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move-object v0, v5

    goto :goto_1

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    sget-object v0, Lcom/squareup/onboarding/flow/data/ImageSize;->ORIGINAL:Lcom/squareup/onboarding/flow/data/ImageSize;

    :goto_2
    return-object v0
.end method

.method public final url()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    const-string v1, "image_url"

    const/4 v2, 0x2

    .line 11
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
