.class public final enum Lcom/squareup/onboarding/flow/data/ParagraphStyle;
.super Ljava/lang/Enum;
.source "OnboardingParagraphItem.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/onboarding/flow/data/ParagraphStyle;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u001b\u0008\u0002\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/data/ParagraphStyle;",
        "",
        "backgroundRes",
        "",
        "paddingRes",
        "(Ljava/lang/String;III)V",
        "getBackgroundRes",
        "()I",
        "getPaddingRes",
        "NONE",
        "INFO",
        "ERROR",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/onboarding/flow/data/ParagraphStyle;

.field public static final enum ERROR:Lcom/squareup/onboarding/flow/data/ParagraphStyle;

.field public static final enum INFO:Lcom/squareup/onboarding/flow/data/ParagraphStyle;

.field public static final enum NONE:Lcom/squareup/onboarding/flow/data/ParagraphStyle;


# instance fields
.field private final backgroundRes:I

.field private final paddingRes:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    new-instance v1, Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    .line 34
    sget v2, Lcom/squareup/onboarding/flow/R$dimen;->onboarding_paragraph_no_padding:I

    const/4 v3, 0x0

    const-string v4, "NONE"

    .line 32
    invoke-direct {v1, v4, v3, v3, v2}, Lcom/squareup/onboarding/flow/data/ParagraphStyle;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->NONE:Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    .line 37
    sget v2, Lcom/squareup/onboarding/flow/R$drawable;->onboarding_paragraph_info:I

    .line 38
    sget v3, Lcom/squareup/onboarding/flow/R$dimen;->onboarding_paragraph_padded:I

    const/4 v4, 0x1

    const-string v5, "INFO"

    .line 36
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/onboarding/flow/data/ParagraphStyle;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->INFO:Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    .line 41
    sget v2, Lcom/squareup/onboarding/flow/R$drawable;->onboarding_paragraph_error:I

    .line 42
    sget v3, Lcom/squareup/onboarding/flow/R$dimen;->onboarding_paragraph_padded:I

    const/4 v4, 0x2

    const-string v5, "ERROR"

    .line 40
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/onboarding/flow/data/ParagraphStyle;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->ERROR:Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->$VALUES:[Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->backgroundRes:I

    iput p4, p0, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->paddingRes:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/onboarding/flow/data/ParagraphStyle;
    .locals 1

    const-class v0, Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    return-object p0
.end method

.method public static values()[Lcom/squareup/onboarding/flow/data/ParagraphStyle;
    .locals 1

    sget-object v0, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->$VALUES:[Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    invoke-virtual {v0}, [Lcom/squareup/onboarding/flow/data/ParagraphStyle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    return-object v0
.end method


# virtual methods
.method public final getBackgroundRes()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->backgroundRes:I

    return v0
.end method

.method public final getPaddingRes()I
    .locals 1

    .line 30
    iget v0, p0, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->paddingRes:I

    return v0
.end method
