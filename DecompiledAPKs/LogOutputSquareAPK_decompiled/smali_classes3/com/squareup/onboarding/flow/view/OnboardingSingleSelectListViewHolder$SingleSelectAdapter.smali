.class abstract Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "OnboardingSingleSelectListViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "SingleSelectAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        ">",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "TVH;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingSingleSelectListViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingSingleSelectListViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,159:1\n1103#2,7:160\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingSingleSelectListViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter\n*L\n70#1,7:160\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\"\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0010J\u0015\u0010\u0014\u001a\u00028\u00002\u0006\u0010\u0015\u001a\u00020\u0016H&\u00a2\u0006\u0002\u0010\u0017J\u001b\u0010\u0014\u001a\u00028\u00002\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0019J\u0015\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\u001dR\u001a\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR&\u0010\u000c\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\r0\u0008X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000b\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;",
        "VH",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "component",
        "Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;",
        "(Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;)V",
        "keys",
        "",
        "",
        "getKeys",
        "()Ljava/util/List;",
        "values",
        "Lkotlin/Pair;",
        "getValues",
        "getItemCount",
        "",
        "getItemId",
        "",
        "position",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "viewType",
        "(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "onRowTapped",
        "",
        "holder",
        "(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final keys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 58
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;->keys()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;->keys:Ljava/util/List;

    .line 59
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;->values()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;->values:Ljava/util/List;

    const/4 p1, 0x1

    .line 62
    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;->setHasStableIds(Z)V

    return-void
.end method


# virtual methods
.method public final getItemCount()I
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;->keys:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;->values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public final getItemId(I)J
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;->keys:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method protected final getKeys()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;->keys:Ljava/util/List;

    return-object v0
.end method

.method protected final getValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;->values:Ljava/util/List;

    return-object v0
.end method

.method public abstract onCreateViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")TVH;"
        }
    .end annotation
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)TVH;"
        }
    .end annotation

    const-string p2, "parent"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p1

    .line 70
    iget-object p2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 160
    new-instance v0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter$onCreateViewHolder$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter$onCreateViewHolder$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1
.end method

.method public abstract onRowTapped(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;)V"
        }
    .end annotation
.end method
