.class public final Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;
.super Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
.source "OnboardingBankAccountViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
        "Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingBankAccountViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingBankAccountViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder\n*L\n1#1,83:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u00102\u0006\u0010\n\u001a\u00020\u0002H\u0014J\u0018\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0008\u0010\u0017\u001a\u00020\u0010H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V",
        "bankAccount",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView;",
        "component",
        "header",
        "Lcom/squareup/marketfont/MarketTextView;",
        "subs",
        "Lrx/subscriptions/CompositeSubscription;",
        "observeBankAccountChanges",
        "",
        "onBindComponent",
        "onFieldChanged",
        "field",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;",
        "text",
        "",
        "onHighlightError",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bankAccount:Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

.field private component:Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;

.field private final header:Lcom/squareup/marketfont/MarketTextView;

.field private final subs:Lrx/subscriptions/CompositeSubscription;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V
    .locals 1

    const-string v0, "inputHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_bank_account:I

    .line 21
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V

    .line 24
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_bank_account_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    .line 25
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget p2, Lcom/squareup/onboarding/flow/R$id;->onboarding_bank_account:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->bankAccount:Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    .line 29
    new-instance p1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->subs:Lrx/subscriptions/CompositeSubscription;

    .line 32
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->bankAccount:Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    new-instance p2, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder$1;

    invoke-direct {p2, p0}, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder$1;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;)V

    check-cast p2, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p1, p2}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    return-void
.end method

.method public static final synthetic access$getSubs$p(Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;)Lrx/subscriptions/CompositeSubscription;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->subs:Lrx/subscriptions/CompositeSubscription;

    return-object p0
.end method

.method public static final synthetic access$observeBankAccountChanges(Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->observeBankAccountChanges()V

    return-void
.end method

.method public static final synthetic access$onFieldChanged(Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Ljava/lang/String;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->onFieldChanged(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Ljava/lang/String;)V

    return-void
.end method

.method private final observeBankAccountChanges()V
    .locals 7

    .line 63
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    .line 64
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->subs:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->bankAccount:Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    invoke-virtual {v1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->fieldChanged()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder$observeBankAccountChanges$1;

    invoke-direct {v2, p0}, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder$observeBankAccountChanges$1;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;)V

    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    const-string v2, "bankAccount.fieldChanged\u2026nged(it.field, it.text) }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx/RxKt;->plusAssign(Lrx/subscriptions/CompositeSubscription;Lrx/Subscription;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->component:Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;

    if-eqz v0, :cond_0

    .line 67
    invoke-static {}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->values()[Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v1, v3

    .line 68
    iget-object v5, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->bankAccount:Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    invoke-virtual {v0, v4}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->getField(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->setField(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private final onFieldChanged(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Ljava/lang/String;)V
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->component:Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;

    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    move-result-object v1

    invoke-virtual {v0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->fieldOutput(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onOutput(Lcom/squareup/protos/client/onboard/Output;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;)V
    .locals 2

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->component:Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;

    .line 46
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->bankAccount:Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    invoke-virtual {v1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->countryOutput(Lcom/squareup/CountryCode;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onOutput(Lcom/squareup/protos/client/onboard/Output;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->label()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 50
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->bankAccount:Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    invoke-virtual {p1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->isAttachedToWindow()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 51
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->observeBankAccountChanges()V

    :cond_0
    return-void
.end method

.method public bridge synthetic onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;)V

    return-void
.end method

.method public onHighlightError()V
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->bankAccount:Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    invoke-virtual {v0}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->emptyField()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 57
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 58
    instance-of v1, v0, Landroid/widget/EditText;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Landroid/widget/EditText;

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->setSelectionEnd(Landroid/widget/EditText;)V

    :cond_1
    return-void
.end method
