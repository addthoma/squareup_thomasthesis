.class public final Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;
.super Ljava/lang/Object;
.source "OnboardingWorkflowMonitor.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingWorkflowMonitor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingWorkflowMonitor.kt\ncom/squareup/onboarding/flow/OnboardingWorkflowMonitor\n+ 2 PosContainer.kt\ncom/squareup/ui/main/PosContainers\n*L\n1#1,132:1\n152#2:133\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingWorkflowMonitor.kt\ncom/squareup/onboarding/flow/OnboardingWorkflowMonitor\n*L\n105#1:133\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B]\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015\u00a2\u0006\u0002\u0010\u0017J\u0008\u0010\u001e\u001a\u00020\u001aH\u0002J\u0008\u0010\u001f\u001a\u00020\u001aH\u0002J\u0010\u0010 \u001a\u00020\u001a2\u0006\u0010!\u001a\u00020\"H\u0016J\u0008\u0010#\u001a\u00020\u001aH\u0002J\u0008\u0010$\u001a\u00020\u001aH\u0016J\u0008\u0010%\u001a\u00020\u001aH\u0002J\u0010\u0010&\u001a\u00020\u001a2\u0006\u0010\'\u001a\u00020(H\u0002R\u001c\u0010\u0018\u001a\u0010\u0012\u000c\u0012\n \u001b*\u0004\u0018\u00010\u001a0\u001a0\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;",
        "Lmortar/Scoped;",
        "activationServiceHelper",
        "Lcom/squareup/onboarding/ActivationServiceHelper;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "orderEntryAppletGateway",
        "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "bankSettings",
        "Lcom/squareup/banklinking/BankAccountSettings;",
        "onboardingType",
        "Lcom/squareup/onboarding/OnboardingType;",
        "adAnalytics",
        "Lcom/squareup/adanalytics/AdAnalytics;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "setupGuideIntegrationRunner",
        "Ldagger/Lazy;",
        "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
        "(Lcom/squareup/onboarding/ActivationServiceHelper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;)V",
        "activateViaWeb",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "onClientUnsupported",
        "onCompleted",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onErrorOrCancelled",
        "onExitScope",
        "onMerchantActivated",
        "onWorkflowFinished",
        "result",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final activateViaWeb:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final activationServiceHelper:Lcom/squareup/onboarding/ActivationServiceHelper;

.field private final adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bankSettings:Lcom/squareup/banklinking/BankAccountSettings;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final onboardingType:Lcom/squareup/onboarding/OnboardingType;

.field private final orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final setupGuideIntegrationRunner:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/ActivationServiceHelper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/ActivationServiceHelper;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            "Lcom/squareup/onboarding/OnboardingType;",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Ldagger/Lazy<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "activationServiceHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "browserLauncher"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryAppletGateway"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bankSettings"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingType"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adAnalytics"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "setupGuideIntegrationRunner"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->activationServiceHelper:Lcom/squareup/onboarding/ActivationServiceHelper;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iput-object p3, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iput-object p4, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p5, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->bankSettings:Lcom/squareup/banklinking/BankAccountSettings;

    iput-object p6, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->onboardingType:Lcom/squareup/onboarding/OnboardingType;

    iput-object p7, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    iput-object p8, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p9, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p10, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->setupGuideIntegrationRunner:Ldagger/Lazy;

    .line 52
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->activateViaWeb:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 53
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    return-void
.end method

.method public static final synthetic access$getActivationServiceHelper$p(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)Lcom/squareup/onboarding/ActivationServiceHelper;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->activationServiceHelper:Lcom/squareup/onboarding/ActivationServiceHelper;

    return-object p0
.end method

.method public static final synthetic access$getBrowserLauncher$p(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)Lcom/squareup/util/BrowserLauncher;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-object p0
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method

.method public static final synthetic access$getOrderEntryAppletGateway$p(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)Lcom/squareup/orderentry/OrderEntryAppletGateway;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    return-object p0
.end method

.method public static final synthetic access$getSettings$p(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method public static final synthetic access$onMerchantActivated(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->onMerchantActivated()V

    return-void
.end method

.method public static final synthetic access$onWorkflowFinished(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;Lcom/squareup/onboarding/OnboardingWorkflowResult;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->onWorkflowFinished(Lcom/squareup/onboarding/OnboardingWorkflowResult;)V

    return-void
.end method

.method private final onClientUnsupported()V
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->activateViaWeb:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final onCompleted()V
    .locals 5

    .line 110
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->refresh()V

    .line 111
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->bankSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v1}, Lcom/squareup/banklinking/BankAccountSettings;->refresh()Lio/reactivex/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 112
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 113
    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->setupGuideIntegrationRunner:Ldagger/Lazy;

    invoke-interface {v1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    .line 114
    sget-object v2, Lcom/squareup/protos/checklist/common/ActionItemName;->FINISH_ACTIVATING:Lcom/squareup/protos/checklist/common/ActionItemName;

    sget-object v3, Lcom/squareup/ui/main/MainActivityScope;->INSTANCE:Lcom/squareup/ui/main/MainActivityScope;

    const-string v4, "MainActivityScope.INSTANCE"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 115
    new-instance v4, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onCompleted$1;

    invoke-direct {v4, p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onCompleted$1;-><init>(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 113
    invoke-interface {v1, v2, v3, v4}, Lcom/squareup/setupguide/SetupGuideIntegrationRunner;->handleCompletion(Lcom/squareup/protos/checklist/common/ActionItemName;Lcom/squareup/ui/main/RegisterTreeKey;Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;

    move-result-object v1

    .line 117
    invoke-virtual {v1}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 112
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private final onErrorOrCancelled()V
    .locals 4

    .line 105
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->container:Lcom/squareup/ui/main/PosContainer;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    .line 133
    const-class v2, Lcom/squareup/container/WorkflowTreeKey;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/PosContainer;->goBackPast([Ljava/lang/Class;)V

    return-void
.end method

.method private final onMerchantActivated()V
    .locals 5

    .line 126
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    .line 127
    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string/jumbo v3, "token!!"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v3

    const-string v4, "countryCode"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getMerchantToken()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v3, v0}, Lcom/squareup/adanalytics/AdAnalytics;->recordActivation(Ljava/lang/String;Lcom/squareup/CountryCode;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/onboarding/common/ActivationEvent;

    invoke-direct {v1}, Lcom/squareup/onboarding/common/ActivationEvent;-><init>()V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private final onWorkflowFinished(Lcom/squareup/onboarding/OnboardingWorkflowResult;)V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/onboarding/flow/FinishFlowEvent;

    invoke-direct {v1, p1}, Lcom/squareup/onboarding/flow/FinishFlowEvent;-><init>(Lcom/squareup/onboarding/OnboardingWorkflowResult;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 97
    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowResult$Errored;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Errored;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 98
    :cond_0
    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowResult$Cancelled;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Cancelled;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->onErrorOrCancelled()V

    goto :goto_1

    .line 99
    :cond_1
    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowResult$Finished;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Finished;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->onCompleted()V

    goto :goto_1

    .line 100
    :cond_2
    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowResult$Unsupported;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Unsupported;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-direct {p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->onClientUnsupported()V

    :cond_3
    :goto_1
    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "settings.settingsAvailable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->onboardingType:Lcom/squareup/onboarding/OnboardingType;

    invoke-interface {v1}, Lcom/squareup/onboarding/OnboardingType;->variant()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$1;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$1;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "onboardingType.variant()\u2026p { it == SERVER_DRIVEN }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2TransformersKt;->gateBy(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 62
    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$2;-><init>(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)V

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 63
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "settings.settingsAvailab\u2026ds() }\n          .take(1)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$3;

    invoke-direct {v1, p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$3;-><init>(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 67
    :cond_0
    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowRunner;->Companion:Lcom/squareup/onboarding/OnboardingWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/onboarding/OnboardingWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/onboarding/OnboardingWorkflowRunner;

    move-result-object v0

    .line 68
    invoke-interface {v0}, Lcom/squareup/onboarding/OnboardingWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v1

    .line 69
    sget-object v2, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$4$1;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$4$1;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 70
    new-instance v2, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$4$2;

    iget-object v3, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v2, v3}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$4$2;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$sam$i$io_reactivex_functions_Consumer$0;

    invoke-direct {v3, v2}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$sam$i$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "onUpdateScreens()\n      \u2026ibe(container::pushStack)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-static {v1, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 73
    invoke-interface {v0}, Lcom/squareup/onboarding/OnboardingWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 74
    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$4$3;

    move-object v2, p0

    check-cast v2, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;

    invoke-direct {v1, v2}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$4$3;-><init>(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$sam$i$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$sam$i$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onResult()\n          .su\u2026ibe(::onWorkflowFinished)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->activateViaWeb:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 79
    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$5;

    invoke-direct {v1, p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$5;-><init>(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "activateViaWeb\n        .\u2026cessOrFailure()\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$6;

    invoke-direct {v1, p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$6;-><init>(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method
