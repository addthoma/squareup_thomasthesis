.class public final Lcom/squareup/onboarding/flow/OnboardingViewFactory_Factory;
.super Ljava/lang/Object;
.source "OnboardingViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onboarding/flow/OnboardingViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final coordinatorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/OnboardingCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/OnboardingCoordinator$Factory;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingViewFactory_Factory;->coordinatorFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/onboarding/flow/OnboardingViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/OnboardingCoordinator$Factory;",
            ">;)",
            "Lcom/squareup/onboarding/flow/OnboardingViewFactory_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingViewFactory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/onboarding/flow/OnboardingViewFactory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/onboarding/flow/OnboardingCoordinator$Factory;)Lcom/squareup/onboarding/flow/OnboardingViewFactory;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingViewFactory;

    invoke-direct {v0, p0}, Lcom/squareup/onboarding/flow/OnboardingViewFactory;-><init>(Lcom/squareup/onboarding/flow/OnboardingCoordinator$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/onboarding/flow/OnboardingViewFactory;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingViewFactory_Factory;->coordinatorFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingCoordinator$Factory;

    invoke-static {v0}, Lcom/squareup/onboarding/flow/OnboardingViewFactory_Factory;->newInstance(Lcom/squareup/onboarding/flow/OnboardingCoordinator$Factory;)Lcom/squareup/onboarding/flow/OnboardingViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/OnboardingViewFactory_Factory;->get()Lcom/squareup/onboarding/flow/OnboardingViewFactory;

    move-result-object v0

    return-object v0
.end method
