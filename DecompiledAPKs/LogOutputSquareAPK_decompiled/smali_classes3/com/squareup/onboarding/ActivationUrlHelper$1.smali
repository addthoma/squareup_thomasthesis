.class Lcom/squareup/onboarding/ActivationUrlHelper$1;
.super Ljava/lang/Object;
.source "ActivationUrlHelper.java"

# interfaces
.implements Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/ActivationUrlHelper;-><init>(Lcom/squareup/util/Res;Lcom/squareup/onboarding/ActivationServiceHelper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener<",
        "Lcom/squareup/server/activation/ActivationUrl;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onboarding/ActivationUrlHelper;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/ActivationUrlHelper;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/squareup/onboarding/ActivationUrlHelper$1;->this$0:Lcom/squareup/onboarding/ActivationUrlHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailureViewDismissed(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 35
    iget-object p1, p0, Lcom/squareup/onboarding/ActivationUrlHelper$1;->this$0:Lcom/squareup/onboarding/ActivationUrlHelper;

    invoke-virtual {p1}, Lcom/squareup/onboarding/ActivationUrlHelper;->loadAndOpen()V

    :cond_0
    return-void
.end method

.method public onProgressViewDismissed(Lcom/squareup/server/activation/ActivationUrl;)V
    .locals 0

    return-void
.end method

.method public bridge synthetic onProgressViewDismissed(Ljava/lang/Object;)V
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/server/activation/ActivationUrl;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/ActivationUrlHelper$1;->onProgressViewDismissed(Lcom/squareup/server/activation/ActivationUrl;)V

    return-void
.end method
