.class public Lcom/squareup/encryption/HmacSha256Generator;
.super Lcom/squareup/encryption/AbstractCryptoPrimitive;
.source "HmacSha256Generator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/encryption/AbstractCryptoPrimitive<",
        "TK;>;"
    }
.end annotation


# static fields
.field private static final HMAC_SHA256_ALGORITHM:Ljava/lang/String; = "HmacSHA256"


# instance fields
.field private final mac:Ljavax/crypto/Mac;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/encryption/CryptoKeyAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/squareup/encryption/CryptoKeyAdapter<",
            "TK;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/squareup/encryption/AbstractCryptoPrimitive;-><init>(Ljava/lang/Object;Lcom/squareup/encryption/CryptoKeyAdapter;)V

    .line 18
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {p2, p1}, Lcom/squareup/encryption/CryptoKeyAdapter;->getRawKey(Ljava/lang/Object;)[B

    move-result-object p1

    const-string p2, "HmacSHA256"

    invoke-direct {v0, p1, p2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 20
    :try_start_0
    invoke-static {p2}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/encryption/HmacSha256Generator;->mac:Ljavax/crypto/Mac;

    .line 21
    iget-object p1, p0, Lcom/squareup/encryption/HmacSha256Generator;->mac:Ljavax/crypto/Mac;

    invoke-virtual {p1, v0}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 24
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method


# virtual methods
.method protected doCompute([B)Lcom/squareup/encryption/CryptoResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Lcom/squareup/encryption/CryptoResult<",
            "TK;>;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/encryption/HmacSha256Generator;->mac:Ljavax/crypto/Mac;

    invoke-virtual {v0}, Ljavax/crypto/Mac;->reset()V

    .line 30
    new-instance v0, Lcom/squareup/encryption/CryptoResult;

    invoke-virtual {p0}, Lcom/squareup/encryption/HmacSha256Generator;->getKey()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/encryption/HmacSha256Generator;->mac:Ljavax/crypto/Mac;

    invoke-virtual {v2, p1}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/encryption/CryptoResult;-><init>(Ljava/lang/Object;[B)V

    return-object v0
.end method
