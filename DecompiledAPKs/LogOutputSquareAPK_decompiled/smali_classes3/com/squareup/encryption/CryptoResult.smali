.class public Lcom/squareup/encryption/CryptoResult;
.super Ljava/lang/Object;
.source "CryptoResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final key:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field private final value:[B


# direct methods
.method constructor <init>(Ljava/lang/Object;[B)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;[B)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/encryption/CryptoResult;->key:Ljava/lang/Object;

    .line 17
    iput-object p2, p0, Lcom/squareup/encryption/CryptoResult;->value:[B

    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/encryption/CryptoResult;->key:Ljava/lang/Object;

    return-object v0
.end method

.method public getValue()[B
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/encryption/CryptoResult;->value:[B

    return-object v0
.end method
