.class public Lcom/squareup/internet/RealInternetStatusMonitor;
.super Ljava/lang/Object;
.source "RealInternetStatusMonitor.java"

# interfaces
.implements Lcom/squareup/internet/InternetStatusMonitor;


# static fields
.field static final COOLDOWN_DURATION_MS:J = 0x3e8L


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final clock:Lcom/squareup/util/Clock;

.field private lastSentStatusMS:J

.field private final manager:Landroid/net/ConnectivityManager;

.field private previousState:Lcom/squareup/internet/InternetConnection;


# direct methods
.method constructor <init>(Landroid/net/ConnectivityManager;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/internet/RealInternetStatusMonitor;->manager:Landroid/net/ConnectivityManager;

    .line 28
    iput-object p2, p0, Lcom/squareup/internet/RealInternetStatusMonitor;->analytics:Lcom/squareup/analytics/Analytics;

    .line 29
    iput-object p3, p0, Lcom/squareup/internet/RealInternetStatusMonitor;->clock:Lcom/squareup/util/Clock;

    return-void
.end method


# virtual methods
.method public getNetworkConnection()Lcom/squareup/internet/InternetConnection;
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/internet/RealInternetStatusMonitor;->manager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 45
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 46
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 48
    sget-object v0, Lcom/squareup/internet/InternetConnection;->WIFI:Lcom/squareup/internet/InternetConnection;

    return-object v0

    :cond_0
    if-nez v0, :cond_1

    .line 51
    sget-object v0, Lcom/squareup/internet/InternetConnection;->CELLULAR:Lcom/squareup/internet/InternetConnection;

    return-object v0

    :cond_1
    const/16 v1, 0x9

    if-ne v0, v1, :cond_2

    .line 54
    sget-object v0, Lcom/squareup/internet/InternetConnection;->ETHERNET:Lcom/squareup/internet/InternetConnection;

    return-object v0

    .line 58
    :cond_2
    sget-object v0, Lcom/squareup/internet/InternetConnection;->OFFLINE:Lcom/squareup/internet/InternetConnection;

    return-object v0
.end method

.method public logNetworkConnectionStatus()V
    .locals 6

    .line 33
    iget-object v0, p0, Lcom/squareup/internet/RealInternetStatusMonitor;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/squareup/internet/RealInternetStatusMonitor;->lastSentStatusMS:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/internet/RealInternetStatusMonitor;->getNetworkConnection()Lcom/squareup/internet/InternetConnection;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/squareup/internet/RealInternetStatusMonitor;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/analytics/event/v1/StatusEvent;

    invoke-virtual {v0}, Lcom/squareup/internet/InternetConnection;->name()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/internet/RealInternetStatusMonitor;->previousState:Lcom/squareup/internet/InternetConnection;

    if-eqz v4, :cond_0

    .line 36
    invoke-virtual {v4}, Lcom/squareup/internet/InternetConnection;->name()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    const-string v5, "Internet Connection"

    invoke-direct {v2, v5, v3, v4}, Lcom/squareup/analytics/event/v1/StatusEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 37
    iput-object v0, p0, Lcom/squareup/internet/RealInternetStatusMonitor;->previousState:Lcom/squareup/internet/InternetConnection;

    .line 38
    iget-object v0, p0, Lcom/squareup/internet/RealInternetStatusMonitor;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/internet/RealInternetStatusMonitor;->lastSentStatusMS:J

    :cond_1
    return-void
.end method
