.class Lcom/squareup/jail/JailPresenter;
.super Lmortar/ViewPresenter;
.source "JailPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/jail/JailView;",
        ">;"
    }
.end annotation


# static fields
.field static final FIRST_BATCH_ANIM_DURATION:J = 0xfa0L


# instance fields
.field private final authenticator:Lcom/squareup/account/LegacyAuthenticator;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final clock:Lcom/squareup/util/Clock;

.field private final cogsSyncProgress:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private isFirstBatch:Z

.field private final jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

.field private final jailScreenConfiguration:Lcom/squareup/jailkeeper/JailScreenConfiguration;

.field private leaveJailAfterDelay:Ljava/lang/Runnable;

.field private final loggedOutStarter:Lcom/squareup/loggedout/LoggedOutStarter;

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private showingLoadingIndicatorSince:J

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method constructor <init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Clock;Lcom/squareup/jailkeeper/JailScreenConfiguration;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/loggedout/LoggedOutStarter;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 56
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const/4 v0, 0x1

    .line 51
    iput-boolean v0, p0, Lcom/squareup/jail/JailPresenter;->isFirstBatch:Z

    .line 57
    iput-object p1, p0, Lcom/squareup/jail/JailPresenter;->bus:Lcom/squareup/badbus/BadBus;

    .line 58
    iput-object p2, p0, Lcom/squareup/jail/JailPresenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 59
    iput-object p3, p0, Lcom/squareup/jail/JailPresenter;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    .line 60
    iput-object p5, p0, Lcom/squareup/jail/JailPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 61
    iput-object p6, p0, Lcom/squareup/jail/JailPresenter;->clock:Lcom/squareup/util/Clock;

    .line 62
    invoke-interface {p4}, Lcom/squareup/cogs/Cogs;->cogsSyncProgress()Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/jail/JailPresenter;->cogsSyncProgress:Lrx/Observable;

    .line 63
    iput-object p7, p0, Lcom/squareup/jail/JailPresenter;->jailScreenConfiguration:Lcom/squareup/jailkeeper/JailScreenConfiguration;

    .line 64
    iput-object p8, p0, Lcom/squareup/jail/JailPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    .line 65
    iput-object p9, p0, Lcom/squareup/jail/JailPresenter;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    .line 66
    iput-object p10, p0, Lcom/squareup/jail/JailPresenter;->loggedOutStarter:Lcom/squareup/loggedout/LoggedOutStarter;

    .line 67
    iput-object p11, p0, Lcom/squareup/jail/JailPresenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private executeAfterMinimumDelay(JLjava/lang/Runnable;)V
    .locals 3

    .line 173
    iget-wide v0, p0, Lcom/squareup/jail/JailPresenter;->showingLoadingIndicatorSince:J

    add-long/2addr v0, p1

    iget-object p1, p0, Lcom/squareup/jail/JailPresenter;->clock:Lcom/squareup/util/Clock;

    invoke-interface {p1}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide p1

    sub-long/2addr v0, p1

    const-wide/16 p1, 0x0

    cmp-long v2, v0, p1

    if-lez v2, :cond_0

    .line 175
    iget-object p1, p0, Lcom/squareup/jail/JailPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {p1, p3, v0, v1}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 177
    :cond_0
    invoke-interface {p3}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void
.end method


# virtual methods
.method exit()Lkotlin/Unit;
    .locals 2

    .line 160
    invoke-virtual {p0}, Lcom/squareup/jail/JailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/jail/JailView;

    invoke-virtual {v0}, Lcom/squareup/jail/JailView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 162
    iget-object v1, p0, Lcom/squareup/jail/JailPresenter;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    invoke-interface {v1}, Lcom/squareup/account/LegacyAuthenticator;->logOut()V

    .line 163
    iget-object v1, p0, Lcom/squareup/jail/JailPresenter;->loggedOutStarter:Lcom/squareup/loggedout/LoggedOutStarter;

    invoke-interface {v1, v0}, Lcom/squareup/loggedout/LoggedOutStarter;->showLogin(Landroid/content/Context;)V

    .line 165
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public synthetic lambda$null$0$JailPresenter(Lcom/squareup/jail/JailView;Ljava/lang/Integer;)V
    .locals 4

    .line 90
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 91
    invoke-virtual {p1}, Lcom/squareup/jail/JailView;->resetProgress()V

    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/squareup/jail/JailPresenter;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    .line 96
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    iget-boolean v2, p0, Lcom/squareup/jail/JailPresenter;->isFirstBatch:Z

    if-eqz v2, :cond_1

    const-wide/16 v2, 0xfa0

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Lcom/squareup/jail/JailPresenter;->showingLoadingIndicatorSince:J

    sub-long v2, v0, v2

    :goto_0
    invoke-virtual {p1, p2, v2, v3}, Lcom/squareup/jail/JailView;->setProgress(IJ)V

    .line 98
    iput-wide v0, p0, Lcom/squareup/jail/JailPresenter;->showingLoadingIndicatorSince:J

    const/4 p1, 0x0

    .line 99
    iput-boolean p1, p0, Lcom/squareup/jail/JailPresenter;->isFirstBatch:Z

    return-void
.end method

.method public synthetic lambda$onLoad$1$JailPresenter(Lcom/squareup/jail/JailView;)Lrx/Subscription;
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/jail/JailPresenter;->cogsSyncProgress:Lrx/Observable;

    new-instance v1, Lcom/squareup/jail/-$$Lambda$JailPresenter$jNpYBub1hwWGa_OnC8D-_zblwW0;

    invoke-direct {v1, p0, p1}, Lcom/squareup/jail/-$$Lambda$JailPresenter$jNpYBub1hwWGa_OnC8D-_zblwW0;-><init>(Lcom/squareup/jail/JailPresenter;Lcom/squareup/jail/JailView;)V

    .line 89
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/jail/JailPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/jailkeeper/JailKeeper$State;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/jail/-$$Lambda$VDV28fH-P16hFLl43k--kjWlTvg;

    invoke-direct {v1, p0}, Lcom/squareup/jail/-$$Lambda$VDV28fH-P16hFLl43k--kjWlTvg;-><init>(Lcom/squareup/jail/JailPresenter;)V

    .line 72
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 71
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/jail/JailPresenter;->leaveJailAfterDelay:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 77
    iget-object v1, p0, Lcom/squareup/jail/JailPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v1, v0}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    .line 78
    iput-object v0, p0, Lcom/squareup/jail/JailPresenter;->leaveJailAfterDelay:Ljava/lang/Runnable;

    .line 80
    :cond_0
    invoke-super {p0}, Lmortar/ViewPresenter;->onExitScope()V

    return-void
.end method

.method onInitializerStateChange(Lcom/squareup/jailkeeper/JailKeeper$State;)V
    .locals 4

    .line 108
    invoke-virtual {p0}, Lcom/squareup/jail/JailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/jail/JailView;

    .line 110
    sget-object v1, Lcom/squareup/jail/JailPresenter$1;->$SwitchMap$com$squareup$jailkeeper$JailKeeper$State:[I

    invoke-virtual {p1}, Lcom/squareup/jailkeeper/JailKeeper$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_5

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    const/4 v2, 0x4

    if-eq v1, v2, :cond_2

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    if-eqz v0, :cond_7

    .line 140
    iget-object p1, p0, Lcom/squareup/jail/JailPresenter;->jailScreenConfiguration:Lcom/squareup/jailkeeper/JailScreenConfiguration;

    invoke-interface {p1}, Lcom/squareup/jailkeeper/JailScreenConfiguration;->shouldShowNetworkSettingsButton()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 141
    iget-object p1, p0, Lcom/squareup/jail/JailPresenter;->jailScreenConfiguration:Lcom/squareup/jailkeeper/JailScreenConfiguration;

    invoke-interface {p1}, Lcom/squareup/jailkeeper/JailScreenConfiguration;->networkSettingsIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/jail/JailView;->showNetworkButton(Landroid/content/Intent;)V

    goto :goto_0

    .line 143
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/jail/JailView;->hideNetworkButton()V

    .line 145
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/jail/JailView;->syncFailed()V

    goto :goto_2

    .line 150
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_2
    if-eqz v0, :cond_7

    .line 135
    invoke-virtual {v0}, Lcom/squareup/jail/JailView;->syncInProgress()V

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "Got UNKNOWN state in JailPresenter. Requesting a sync."

    .line 121
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    iget-object p1, p0, Lcom/squareup/jail/JailPresenter;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    invoke-interface {p1}, Lcom/squareup/jailkeeper/JailKeeper;->foregroundSync()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object p1

    .line 123
    sget-object v0, Lcom/squareup/jailkeeper/JailKeeper$State;->UNKNOWN:Lcom/squareup/jailkeeper/JailKeeper$State;

    if-eq p1, v0, :cond_4

    .line 129
    invoke-virtual {p0, p1}, Lcom/squareup/jail/JailPresenter;->onInitializerStateChange(Lcom/squareup/jailkeeper/JailKeeper$State;)V

    goto :goto_2

    .line 125
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "JailKeeper state is still UNKNOWN, even after we requested a sync."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 112
    :cond_5
    iget-object p1, p0, Lcom/squareup/jail/JailPresenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/jail/-$$Lambda$gcxX8XMq_cRvOSoXVg-eiwYpV6A;

    invoke-direct {v0, p1}, Lcom/squareup/jail/-$$Lambda$gcxX8XMq_cRvOSoXVg-eiwYpV6A;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    iput-object v0, p0, Lcom/squareup/jail/JailPresenter;->leaveJailAfterDelay:Ljava/lang/Runnable;

    .line 117
    iget-wide v0, p0, Lcom/squareup/jail/JailPresenter;->showingLoadingIndicatorSince:J

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-nez p1, :cond_6

    goto :goto_1

    :cond_6
    const-wide/16 v2, 0x384

    :goto_1
    iget-object p1, p0, Lcom/squareup/jail/JailPresenter;->leaveJailAfterDelay:Ljava/lang/Runnable;

    invoke-direct {p0, v2, v3, p1}, Lcom/squareup/jail/JailPresenter;->executeAfterMinimumDelay(JLjava/lang/Runnable;)V

    :cond_7
    :goto_2
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 84
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 85
    invoke-virtual {p0}, Lcom/squareup/jail/JailPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/jail/JailView;

    .line 87
    new-instance v0, Lcom/squareup/jail/-$$Lambda$JailPresenter$HuKAswlZJkostYVhtf9TrJX4V4s;

    invoke-direct {v0, p0, p1}, Lcom/squareup/jail/-$$Lambda$JailPresenter$HuKAswlZJkostYVhtf9TrJX4V4s;-><init>(Lcom/squareup/jail/JailPresenter;Lcom/squareup/jail/JailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 102
    iget-object p1, p0, Lcom/squareup/jail/JailPresenter;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    invoke-interface {p1}, Lcom/squareup/jailkeeper/JailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/jail/JailPresenter;->onInitializerStateChange(Lcom/squareup/jailkeeper/JailKeeper$State;)V

    .line 104
    iget-object p1, p0, Lcom/squareup/jail/JailPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Shown JailScreen"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method retry()V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/jail/JailPresenter;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    invoke-interface {v0}, Lcom/squareup/jailkeeper/JailKeeper;->foregroundSync()Lcom/squareup/jailkeeper/JailKeeper$State;

    return-void
.end method

.method shouldShowActionBar()Z
    .locals 2

    .line 169
    iget-object v0, p0, Lcom/squareup/jail/JailPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ALLOW_EXIT_JAIL_SCREEN_ON_ERROR:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method
