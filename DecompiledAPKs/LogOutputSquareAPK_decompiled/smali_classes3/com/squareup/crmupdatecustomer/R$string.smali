.class public final Lcom/squareup/crmupdatecustomer/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crmupdatecustomer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final crm_add_customer_email_address_validation_error:I = 0x7f1205fc

.field public static final crm_add_customer_first_name_validation_error:I = 0x7f1205fd

.field public static final crm_add_customer_last_name_validation_error:I = 0x7f1205fe

.field public static final crm_choose_groups_title:I = 0x7f120648

.field public static final crm_choose_many:I = 0x7f120649

.field public static final crm_choose_one:I = 0x7f12064a

.field public static final crm_contact_saving_error:I = 0x7f12065d

.field public static final crm_create_group_label:I = 0x7f120676

.field public static final crm_create_new_customer_title:I = 0x7f12067c

.field public static final crm_display_to_customer:I = 0x7f1206a3

.field public static final crm_group_loading_error:I = 0x7f1206d7

.field public static final crm_no_groups_exist:I = 0x7f12071e

.field public static final invoice_choose_contact:I = 0x7f120c99

.field public static final invoice_default_headertext:I = 0x7f120cb8

.field public static final invoice_no_contacts_message:I = 0x7f120d36

.field public static final invoice_no_contacts_title:I = 0x7f120d37

.field public static final invoice_no_search_results_message:I = 0x7f120d3d

.field public static final search_hint:I = 0x7f121788


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
