.class public final Lcom/squareup/crmupdatecustomer/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crmupdatecustomer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final contact_container:I = 0x7f0a0394

.field public static final contact_email:I = 0x7f0a0395

.field public static final contact_name:I = 0x7f0a0396

.field public static final contact_phone:I = 0x7f0a0397

.field public static final contacts_empty_view:I = 0x7f0a039d

.field public static final contacts_header:I = 0x7f0a039e

.field public static final contacts_list:I = 0x7f0a039f

.field public static final contacts_search:I = 0x7f0a03a0

.field public static final crm_add_from_address_book:I = 0x7f0a03e3

.field public static final crm_address:I = 0x7f0a03e6

.field public static final crm_birthday_field:I = 0x7f0a03f1

.field public static final crm_company_field:I = 0x7f0a040d

.field public static final crm_contact_edit:I = 0x7f0a040e

.field public static final crm_custom_date_attr_datepicker:I = 0x7f0a0420

.field public static final crm_edit_attributes:I = 0x7f0a0439

.field public static final crm_email_field:I = 0x7f0a043c

.field public static final crm_enum_options:I = 0x7f0a0440

.field public static final crm_first_name_field:I = 0x7f0a044d

.field public static final crm_groups:I = 0x7f0a045a

.field public static final crm_groups_container:I = 0x7f0a045b

.field public static final crm_groups_create_new:I = 0x7f0a045c

.field public static final crm_groups_message:I = 0x7f0a045e

.field public static final crm_groups_progress_bar:I = 0x7f0a045f

.field public static final crm_groups_scroll_view:I = 0x7f0a0461

.field public static final crm_last_name_field:I = 0x7f0a0467

.field public static final crm_note_field:I = 0x7f0a04a3

.field public static final crm_phone_field:I = 0x7f0a04af

.field public static final crm_progress_bar:I = 0x7f0a04cb

.field public static final crm_reference_id_field:I = 0x7f0a04d5

.field public static final crm_scroll_view:I = 0x7f0a04e6

.field public static final crm_update_customer_root:I = 0x7f0a0508

.field public static final header:I = 0x7f0a07c8

.field public static final permission_cancel:I = 0x7f0a0c12

.field public static final permission_enable:I = 0x7f0a0c17

.field public static final permission_explanation:I = 0x7f0a0c18

.field public static final permission_glyph:I = 0x7f0a0c19

.field public static final permission_title:I = 0x7f0a0c1b


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
