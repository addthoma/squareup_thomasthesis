.class public final Lcom/squareup/comms/protos/seller/CheckingGiftCardBalance$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CheckingGiftCardBalance.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/CheckingGiftCardBalance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/CheckingGiftCardBalance;",
        "Lcom/squareup/comms/protos/seller/CheckingGiftCardBalance$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0004\u001a\u00020\u0002H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/CheckingGiftCardBalance$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/CheckingGiftCardBalance;",
        "()V",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 45
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/seller/CheckingGiftCardBalance;
    .locals 2

    .line 46
    new-instance v0, Lcom/squareup/comms/protos/seller/CheckingGiftCardBalance;

    .line 47
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CheckingGiftCardBalance$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    .line 46
    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/seller/CheckingGiftCardBalance;-><init>(Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CheckingGiftCardBalance$Builder;->build()Lcom/squareup/comms/protos/seller/CheckingGiftCardBalance;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method
