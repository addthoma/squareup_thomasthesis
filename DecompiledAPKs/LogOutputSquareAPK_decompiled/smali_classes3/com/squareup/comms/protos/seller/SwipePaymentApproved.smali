.class public final Lcom/squareup/comms/protos/seller/SwipePaymentApproved;
.super Lcom/squareup/wire/AndroidMessage;
.source "SwipePaymentApproved.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Builder;,
        Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/seller/SwipePaymentApproved;",
        "Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSwipePaymentApproved.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SwipePaymentApproved.kt\ncom/squareup/comms/protos/seller/SwipePaymentApproved\n*L\n1#1,230:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00192\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0018\u0019B;\u0012\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ>\u0010\u000e\u001a\u00020\u00002\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cJ\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0096\u0002J\u0008\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u0002H\u0016J\u0008\u0010\u0016\u001a\u00020\u0017H\u0016R\u0010\u0010\t\u001a\u00020\n8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/SwipePaymentApproved;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Builder;",
        "tip_requirements",
        "Lcom/squareup/comms/protos/seller/TipRequirements;",
        "signature_requirements",
        "Lcom/squareup/comms/protos/seller/SignatureRequirements;",
        "receipt_options",
        "Lcom/squareup/comms/protos/seller/ReceiptOptions;",
        "amount_covered",
        "",
        "unknownFields",
        "Lokio/ByteString;",
        "(Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/seller/SwipePaymentApproved;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/seller/SwipePaymentApproved;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Companion;


# instance fields
.field public final amount_covered:J
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation
.end field

.field public final receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.seller.ReceiptOptions#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.seller.SignatureRequirements#ADAPTER"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.seller.TipRequirements#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->Companion:Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Companion;

    .line 175
    new-instance v0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Companion$ADAPTER$1;

    .line 176
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 177
    const-class v2, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 227
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V
    .locals 1

    const-string/jumbo v0, "unknownFields"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    sget-object v0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    iput-object p2, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    iput-object p3, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    iput-wide p4, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->amount_covered:J

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p7, 0x1

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 32
    move-object v0, v1

    check-cast v0, Lcom/squareup/comms/protos/seller/TipRequirements;

    move-object v3, v0

    goto :goto_0

    :cond_0
    move-object v3, p1

    :goto_0
    and-int/lit8 v0, p7, 0x2

    if-eqz v0, :cond_1

    .line 42
    move-object v0, v1

    check-cast v0, Lcom/squareup/comms/protos/seller/SignatureRequirements;

    move-object v4, v0

    goto :goto_1

    :cond_1
    move-object v4, p2

    :goto_1
    and-int/lit8 v0, p7, 0x4

    if-eqz v0, :cond_2

    .line 51
    move-object v0, v1

    check-cast v0, Lcom/squareup/comms/protos/seller/ReceiptOptions;

    move-object v5, v0

    goto :goto_2

    :cond_2
    move-object v5, p3

    :goto_2
    and-int/lit8 v0, p7, 0x10

    if-eqz v0, :cond_3

    .line 63
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v8, v0

    goto :goto_3

    :cond_3
    move-object v8, p6

    :goto_3
    move-object v2, p0

    move-wide v6, p4

    invoke-direct/range {v2 .. v8}, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;-><init>(Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/seller/SwipePaymentApproved;Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/seller/SwipePaymentApproved;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    .line 108
    iget-object p1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    .line 109
    iget-object p2, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    .line 110
    iget-object p3, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    .line 111
    iget-wide p4, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->amount_covered:J

    :cond_3
    move-wide v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    .line 112
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->unknownFields()Lokio/ByteString;

    move-result-object p6

    :cond_4
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-wide p6, v1

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->copy(Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)Lcom/squareup/comms/protos/seller/SwipePaymentApproved;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)Lcom/squareup/comms/protos/seller/SwipePaymentApproved;
    .locals 8

    const-string/jumbo v0, "unknownFields"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    new-instance v0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide v5, p4

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;-><init>(Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .line 76
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 77
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 82
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v3, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->amount_covered:J

    iget-wide v5, p1, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->amount_covered:J

    cmp-long p1, v3, v5

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 86
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_3

    .line 88
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 91
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-wide v1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->amount_covered:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Builder;
    .locals 3

    .line 66
    new-instance v0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Builder;-><init>()V

    .line 67
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Builder;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    .line 68
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Builder;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    .line 69
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Builder;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    .line 70
    iget-wide v1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->amount_covered:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Builder;->amount_covered:Ljava/lang/Long;

    .line 71
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->newBuilder()Lcom/squareup/comms/protos/seller/SwipePaymentApproved$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 100
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "tip_requirements="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    const-string v2, "signature_requirements=\u2588\u2588"

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 102
    :cond_1
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    if-eqz v1, :cond_2

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receipt_options="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_2
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "amount_covered="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/squareup/comms/protos/seller/SwipePaymentApproved;->amount_covered:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 104
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "SwipePaymentApproved{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string/jumbo v0, "}"

    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
