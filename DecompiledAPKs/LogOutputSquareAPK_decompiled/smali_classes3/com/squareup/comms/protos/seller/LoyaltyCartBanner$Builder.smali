.class public final Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoyaltyCartBanner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/LoyaltyCartBanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/LoyaltyCartBanner;",
        "Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u0008\u0010\u000b\u001a\u00020\u0002H\u0016J\u0010\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008J\u000e\u0010\t\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0008J\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u0008R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/LoyaltyCartBanner;",
        "()V",
        "already_checked_in",
        "",
        "Ljava/lang/Boolean;",
        "potential_points",
        "",
        "text",
        "text_abbreviated",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public already_checked_in:Ljava/lang/Boolean;

.field public potential_points:Ljava/lang/String;

.field public text:Ljava/lang/String;

.field public text_abbreviated:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final already_checked_in(Z)Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;
    .locals 0

    .line 145
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;->already_checked_in:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/seller/LoyaltyCartBanner;
    .locals 9

    .line 149
    new-instance v6, Lcom/squareup/comms/protos/seller/LoyaltyCartBanner;

    .line 150
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;->text:Ljava/lang/String;

    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v1, :cond_2

    .line 151
    iget-object v4, p0, Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;->text_abbreviated:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 153
    iget-object v5, p0, Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;->potential_points:Ljava/lang/String;

    .line 154
    iget-object v7, p0, Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;->already_checked_in:Ljava/lang/Boolean;

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 156
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v6

    move-object v2, v4

    move-object v3, v5

    move v4, v7

    move-object v5, v8

    .line 149
    invoke-direct/range {v0 .. v5}, Lcom/squareup/comms/protos/seller/LoyaltyCartBanner;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLokio/ByteString;)V

    return-object v6

    :cond_0
    new-array v1, v3, [Ljava/lang/Object;

    aput-object v7, v1, v2

    const-string v2, "already_checked_in"

    aput-object v2, v1, v0

    .line 154
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v1, v3, [Ljava/lang/Object;

    aput-object v4, v1, v2

    const-string/jumbo v2, "text_abbreviated"

    aput-object v2, v1, v0

    .line 151
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v2

    const-string v1, "text"

    aput-object v1, v3, v0

    .line 150
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;->build()Lcom/squareup/comms/protos/seller/LoyaltyCartBanner;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final potential_points(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;->potential_points:Ljava/lang/String;

    return-object p0
.end method

.method public final text(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;->text:Ljava/lang/String;

    return-object p0
.end method

.method public final text_abbreviated(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;
    .locals 1

    const-string/jumbo v0, "text_abbreviated"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/LoyaltyCartBanner$Builder;->text_abbreviated:Ljava/lang/String;

    return-object p0
.end method
