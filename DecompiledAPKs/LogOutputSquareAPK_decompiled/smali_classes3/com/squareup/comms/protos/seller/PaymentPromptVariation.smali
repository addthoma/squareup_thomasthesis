.class public final enum Lcom/squareup/comms/protos/seller/PaymentPromptVariation;
.super Ljava/lang/Enum;
.source "PaymentPromptVariation.kt"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/seller/PaymentPromptVariation$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/comms/protos/seller/PaymentPromptVariation;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0008\u0008\u0086\u0001\u0018\u0000 \u000b2\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u0001\u000bB\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\n\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/PaymentPromptVariation;",
        "",
        "Lcom/squareup/wire/WireEnum;",
        "value",
        "",
        "(Ljava/lang/String;II)V",
        "getValue",
        "()I",
        "CLASSIC",
        "MAXIMUS",
        "JUST_INSTRUCTIONS",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/seller/PaymentPromptVariation;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CLASSIC:Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

.field public static final Companion:Lcom/squareup/comms/protos/seller/PaymentPromptVariation$Companion;

.field public static final enum JUST_INSTRUCTIONS:Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

.field public static final enum MAXIMUS:Lcom/squareup/comms/protos/seller/PaymentPromptVariation;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    new-instance v1, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    const/4 v2, 0x0

    const-string v3, "CLASSIC"

    .line 18
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;->CLASSIC:Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    const/4 v2, 0x1

    const-string v3, "MAXIMUS"

    .line 23
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;->MAXIMUS:Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    const/4 v2, 0x2

    const-string v3, "JUST_INSTRUCTIONS"

    .line 28
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;->JUST_INSTRUCTIONS:Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;->$VALUES:[Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    new-instance v0, Lcom/squareup/comms/protos/seller/PaymentPromptVariation$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/seller/PaymentPromptVariation$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;->Companion:Lcom/squareup/comms/protos/seller/PaymentPromptVariation$Companion;

    .line 32
    new-instance v0, Lcom/squareup/comms/protos/seller/PaymentPromptVariation$Companion$ADAPTER$1;

    .line 34
    const-class v1, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/seller/PaymentPromptVariation$Companion$ADAPTER$1;-><init>(Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;->value:I

    return-void
.end method

.method public static final fromValue(I)Lcom/squareup/comms/protos/seller/PaymentPromptVariation;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;->Companion:Lcom/squareup/comms/protos/seller/PaymentPromptVariation$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/comms/protos/seller/PaymentPromptVariation$Companion;->fromValue(I)Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/PaymentPromptVariation;
    .locals 1

    const-class v0, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    return-object p0
.end method

.method public static values()[Lcom/squareup/comms/protos/seller/PaymentPromptVariation;
    .locals 1

    sget-object v0, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;->$VALUES:[Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    invoke-virtual {v0}, [Lcom/squareup/comms/protos/seller/PaymentPromptVariation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 13
    iget v0, p0, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;->value:I

    return v0
.end method
