.class public final Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EmvPaymentSuccessResult.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;",
        "Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u0010\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008J\u0008\u0010\u000f\u001a\u00020\u0002H\u0016J\u0010\u0010\t\u001a\u00020\u00002\u0008\u0010\t\u001a\u0004\u0018\u00010\nJ\u0010\u0010\u000b\u001a\u00020\u00002\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cJ\u0010\u0010\r\u001a\u00020\u00002\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u000c8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;",
        "()V",
        "amount_covered",
        "",
        "Ljava/lang/Long;",
        "arpc",
        "Lokio/ByteString;",
        "receipt_options",
        "Lcom/squareup/comms/protos/seller/ReceiptOptions;",
        "signature_requirements",
        "Lcom/squareup/comms/protos/seller/SignatureRequirements;",
        "tip_requirements",
        "Lcom/squareup/comms/protos/seller/TipRequirements;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public amount_covered:Ljava/lang/Long;

.field public arpc:Lokio/ByteString;

.field public receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

.field public signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

.field public tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 134
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final amount_covered(J)Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;
    .locals 0

    .line 188
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->amount_covered:Ljava/lang/Long;

    return-object p0
.end method

.method public final arpc(Lokio/ByteString;)Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->arpc:Lokio/ByteString;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;
    .locals 9

    .line 192
    new-instance v8, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    .line 193
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->arpc:Lokio/ByteString;

    .line 194
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    .line 195
    iget-object v3, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    .line 196
    iget-object v4, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    .line 197
    iget-object v0, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->amount_covered:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 199
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    .line 192
    invoke-direct/range {v0 .. v7}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;-><init>(Lokio/ByteString;Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V

    return-object v8

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    const-string v2, "amount_covered"

    aput-object v2, v1, v0

    .line 197
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 134
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->build()Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final receipt_options(Lcom/squareup/comms/protos/seller/ReceiptOptions;)Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    return-object p0
.end method

.method public final signature_requirements(Lcom/squareup/comms/protos/seller/SignatureRequirements;)Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    return-object p0
.end method

.method public final tip_requirements(Lcom/squareup/comms/protos/seller/TipRequirements;)Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Builder;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    return-object p0
.end method
