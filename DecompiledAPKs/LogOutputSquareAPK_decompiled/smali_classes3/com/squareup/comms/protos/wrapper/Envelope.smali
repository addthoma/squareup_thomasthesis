.class public final Lcom/squareup/comms/protos/wrapper/Envelope;
.super Lcom/squareup/wire/Message;
.source "Envelope.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/wrapper/Envelope$ProtoAdapter_Envelope;,
        Lcom/squareup/comms/protos/wrapper/Envelope$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/comms/protos/wrapper/Envelope;",
        "Lcom/squareup/comms/protos/wrapper/Envelope$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/wrapper/Envelope;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MESSAGE_BODY:Lokio/ByteString;

.field public static final DEFAULT_MESSAGE_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_UUID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final message_body:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x2
    .end annotation
.end field

.field public final message_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final uuid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/comms/protos/wrapper/Envelope$ProtoAdapter_Envelope;

    invoke-direct {v0}, Lcom/squareup/comms/protos/wrapper/Envelope$ProtoAdapter_Envelope;-><init>()V

    sput-object v0, Lcom/squareup/comms/protos/wrapper/Envelope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 26
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/comms/protos/wrapper/Envelope;->DEFAULT_MESSAGE_BODY:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lokio/ByteString;Ljava/lang/String;)V
    .locals 1

    .line 49
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/comms/protos/wrapper/Envelope;-><init>(Ljava/lang/String;Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/comms/protos/wrapper/Envelope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 55
    iput-object p1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->message_name:Ljava/lang/String;

    .line 56
    iput-object p2, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->message_body:Lokio/ByteString;

    .line 57
    iput-object p3, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->uuid:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 73
    :cond_0
    instance-of v1, p1, Lcom/squareup/comms/protos/wrapper/Envelope;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 74
    :cond_1
    check-cast p1, Lcom/squareup/comms/protos/wrapper/Envelope;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/comms/protos/wrapper/Envelope;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/comms/protos/wrapper/Envelope;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->message_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/comms/protos/wrapper/Envelope;->message_name:Ljava/lang/String;

    .line 76
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->message_body:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/comms/protos/wrapper/Envelope;->message_body:Lokio/ByteString;

    .line 77
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->uuid:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/comms/protos/wrapper/Envelope;->uuid:Ljava/lang/String;

    .line 78
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 83
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 85
    invoke-virtual {p0}, Lcom/squareup/comms/protos/wrapper/Envelope;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->message_name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 87
    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->message_body:Lokio/ByteString;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 88
    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->uuid:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 89
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/wrapper/Envelope$Builder;
    .locals 2

    .line 62
    new-instance v0, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;-><init>()V

    .line 63
    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->message_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->message_name:Ljava/lang/String;

    .line 64
    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->message_body:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->message_body:Lokio/ByteString;

    .line 65
    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->uuid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->uuid:Ljava/lang/String;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/comms/protos/wrapper/Envelope;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/comms/protos/wrapper/Envelope;->newBuilder()Lcom/squareup/comms/protos/wrapper/Envelope$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->message_name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", message_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->message_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    :cond_0
    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->message_body:Lokio/ByteString;

    if-eqz v1, :cond_1

    const-string v1, ", message_body="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->message_body:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 99
    :cond_1
    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->uuid:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", uuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->uuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Envelope{"

    .line 100
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
