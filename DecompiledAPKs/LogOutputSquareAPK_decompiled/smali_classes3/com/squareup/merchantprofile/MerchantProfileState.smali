.class public Lcom/squareup/merchantprofile/MerchantProfileState;
.super Ljava/lang/Object;
.source "MerchantProfileState.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# static fields
.field private static final ADDRESS:Ljava/lang/String; = "ADDRESS"

.field private static final APPEAR_IN_DIRECTORY:Ljava/lang/String; = "APPEAR_IN_DIRECTORY"

.field private static final BUSINESS_NAME:Ljava/lang/String; = "BUSINESS_NAME"

.field private static final CARD_COLOR:Ljava/lang/String; = "CARD_COLOR"

.field private static final CONTACT_INFO:Ljava/lang/String; = "CONTACT_INFO"

.field private static final DESCRIPTION:Ljava/lang/String; = "DESCRIPTION"

.field private static final FEATURED_IMAGE_HEIGHT:Ljava/lang/String; = "FEATURED_IMAGE_HEIGHT"

.field private static final FEATURED_IMAGE_URL:Ljava/lang/String; = "FEATURED_IMAGE_URL"

.field private static final FEATURED_IMAGE_WIDTH:Ljava/lang/String; = "FEATURED_IMAGE_WIDTH"

.field private static final LOGO:Ljava/lang/String; = "LOGO"

.field private static final MOBILE_BUSINESS:Ljava/lang/String; = "MOBILE_BUSINESS"

.field private static final PENDING_FEATURED_IMAGE:Ljava/lang/String; = "FEATURED_IMAGE"

.field private static final PENDING_FEATURED_IMAGE_HEIGHT:Ljava/lang/String; = "PENDING_FEATURED_IMAGE_HEIGHT"

.field private static final PENDING_FEATURED_IMAGE_WIDTH:Ljava/lang/String; = "PENDING_FEATURED_IMAGE_WIDTH"

.field private static final PHOTO_ON_RECEIPT:Ljava/lang/String; = "PHOTO_ON_RECEIPT"

.field private static final PROFILE_IMAGE:Ljava/lang/String; = "PROFILE_IMAGE"

.field private static final PUBLISH_ITEMS:Ljava/lang/String; = "PUBLISH_ITEMS"


# instance fields
.field private address:Lcom/squareup/address/Address;

.field private final addressBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/address/Address;",
            ">;"
        }
    .end annotation
.end field

.field public appearInMarket:Z

.field private final businessAddressRelay:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject<",
            "Lkotlin/Pair<",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/address/Address;",
            ">;>;"
        }
    .end annotation
.end field

.field public businessName:Ljava/lang/String;

.field public cardColor:I

.field public contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

.field private final contactInfoBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/merchantprofile/ContactInfo;",
            ">;"
        }
    .end annotation
.end field

.field public description:Ljava/lang/String;

.field public featuredImageHeight:I

.field public featuredImageUrl:Ljava/lang/String;

.field public featuredImageWidth:I

.field private logo:Ljava/io/File;

.field private mobileBusiness:Z

.field public pendingFeaturedImage:Landroid/net/Uri;

.field public pendingFeaturedImageHeight:I

.field public pendingFeaturedImageWidth:I

.field public photoOnReceipt:Z

.field public profileImageUrl:Ljava/lang/String;

.field public publishItems:Z

.field private final tempLogoBitmap:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    invoke-static {}, Lio/reactivex/subjects/BehaviorSubject;->create()Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->tempLogoBitmap:Lio/reactivex/subjects/BehaviorSubject;

    .line 84
    invoke-static {}, Lio/reactivex/subjects/BehaviorSubject;->create()Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->businessAddressRelay:Lio/reactivex/subjects/BehaviorSubject;

    .line 90
    const-class v0, Lcom/squareup/address/Address;

    const-string v1, "ADDRESS"

    invoke-static {p1, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->addressBundleKey:Lcom/squareup/BundleKey;

    .line 91
    const-class v0, Lcom/squareup/merchantprofile/ContactInfo;

    const-string v1, "CONTACT_INFO"

    invoke-static {p1, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfoBundleKey:Lcom/squareup/BundleKey;

    return-void
.end method

.method private static parseFeaturedImage(Lcom/squareup/server/account/MerchantProfileResponse$Entity;)Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->merchant_images:[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->merchant_images:[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 34
    iget-object p0, p0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->merchant_images:[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;

    const/4 v0, 0x0

    aget-object p0, p0, v0

    iget-object p0, p0, Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;->url:Ljava/lang/String;

    return-object p0

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->featured_image:Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;

    if-eqz v0, :cond_1

    iget-object p0, p0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->featured_image:Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;

    iget-object p0, p0, Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;->original_url:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method private parseFeaturedImageDimensions(Lcom/squareup/server/account/MerchantProfileResponse$Entity;)V
    .locals 2

    .line 241
    iget-object v0, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->merchant_images:[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->merchant_images:[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 242
    iget-object v0, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->merchant_images:[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget v0, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;->width:I

    iput v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageWidth:I

    .line 243
    iget-object p1, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->merchant_images:[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;

    aget-object p1, p1, v1

    iget p1, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;->height:I

    iput p1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageHeight:I

    :cond_0
    return-void
.end method

.method private setTempLogoBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    .line 255
    invoke-virtual {p0}, Lcom/squareup/merchantprofile/MerchantProfileState;->tempLogoBitmap()Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Observable;->firstElement()Lio/reactivex/Maybe;

    move-result-object v0

    new-instance v1, Lcom/squareup/merchantprofile/-$$Lambda$MerchantProfileState$CUQVCIUbLFk3DL3nhdsZs3s1G9s;

    invoke-direct {v1, p0, p1}, Lcom/squareup/merchantprofile/-$$Lambda$MerchantProfileState$CUQVCIUbLFk3DL3nhdsZs3s1G9s;-><init>(Lcom/squareup/merchantprofile/MerchantProfileState;Landroid/graphics/Bitmap;)V

    .line 256
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public businessAddress()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Pair<",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/address/Address;",
            ">;>;"
        }
    .end annotation

    .line 107
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->businessAddressRelay:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->address:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public getLocalLogo()Ljava/io/File;
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->logo:Ljava/io/File;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 146
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasLogo()Z
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->logo:Ljava/io/File;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->profileImageUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isMobileBusiness()Ljava/lang/Boolean;
    .locals 1

    .line 125
    iget-boolean v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->mobileBusiness:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$setTempLogoBitmap$0$MerchantProfileState(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 257
    invoke-static {p2}, Lcom/squareup/ui/Bitmaps;->safeRecycle(Landroid/graphics/Bitmap;)V

    .line 258
    iget-object p2, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->tempLogoBitmap:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {p2, p1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public loadFrom(Lcom/squareup/server/account/MerchantProfileResponse;Lcom/squareup/settings/server/MerchantProfileSettings;)V
    .locals 8

    .line 215
    iget-object p1, p1, Lcom/squareup/server/account/MerchantProfileResponse;->entity:Lcom/squareup/server/account/MerchantProfileResponse$Entity;

    .line 216
    iget-boolean v0, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->published:Z

    iput-boolean v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->appearInMarket:Z

    .line 217
    iget-object v0, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->businessName:Ljava/lang/String;

    .line 218
    iget-object v0, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->profile_image:Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->profile_image:Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;

    iget-object v0, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;->original_url:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->profileImageUrl:Ljava/lang/String;

    .line 219
    invoke-static {p1}, Lcom/squareup/merchantprofile/MerchantProfileState;->parseFeaturedImage(Lcom/squareup/server/account/MerchantProfileResponse$Entity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageUrl:Ljava/lang/String;

    .line 220
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 221
    invoke-direct {p0, p1}, Lcom/squareup/merchantprofile/MerchantProfileState;->parseFeaturedImageDimensions(Lcom/squareup/server/account/MerchantProfileResponse$Entity;)V

    .line 224
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->card_color:Ljava/lang/String;

    const v1, -0x777778

    invoke-static {v0, v1}, Lcom/squareup/util/Colors;->parseHex(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->cardColor:I

    .line 225
    iget-object v0, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->country_code:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    goto :goto_1

    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->country_code:Ljava/lang/String;

    .line 227
    invoke-static {v0}, Lcom/squareup/CountryCode;->parseCountryCode(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object v0

    :goto_1
    move-object v7, v0

    .line 228
    new-instance v6, Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v1, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->phone:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->email:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->facebook:Ljava/lang/String;

    iget-object v4, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->twitter:Ljava/lang/String;

    iget-object v5, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->website:Ljava/lang/String;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/merchantprofile/ContactInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v6, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    .line 230
    iget-object v0, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->bio:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->description:Ljava/lang/String;

    .line 231
    iget-boolean v0, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->use_generated_menu:Z

    iput-boolean v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->publishItems:Z

    .line 232
    invoke-virtual {p2}, Lcom/squareup/settings/server/MerchantProfileSettings;->shouldUseCuratedImageForReceipt()Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->photoOnReceipt:Z

    .line 234
    iget-boolean p2, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->mobile_business:Z

    iput-boolean p2, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->mobileBusiness:Z

    .line 235
    new-instance p2, Lcom/squareup/address/Address;

    iget-object v2, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->street1:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->street2:Ljava/lang/String;

    iget-object v4, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->city:Ljava/lang/String;

    iget-object v5, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->state:Ljava/lang/String;

    iget-object v6, p1, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->postal_code:Ljava/lang/String;

    move-object v1, p2

    invoke-direct/range {v1 .. v7}, Lcom/squareup/address/Address;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V

    iput-object p2, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->address:Lcom/squareup/address/Address;

    .line 237
    iget-object p1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->businessAddressRelay:Lio/reactivex/subjects/BehaviorSubject;

    new-instance p2, Lkotlin/Pair;

    iget-boolean v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->mobileBusiness:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->address:Lcom/squareup/address/Address;

    invoke-direct {p2, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p1, p2}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 0

    if-nez p1, :cond_0

    return-void

    .line 157
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/merchantprofile/MerchantProfileState;->restoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 0

    .line 161
    invoke-virtual {p0, p1}, Lcom/squareup/merchantprofile/MerchantProfileState;->saveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method restoreInstanceState(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "APPEAR_IN_DIRECTORY"

    .line 189
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->appearInMarket:Z

    const-string v0, "BUSINESS_NAME"

    .line 190
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->businessName:Ljava/lang/String;

    const-string v0, "PROFILE_IMAGE"

    .line 191
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->profileImageUrl:Ljava/lang/String;

    const-string v0, "LOGO"

    .line 192
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 193
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->logo:Ljava/io/File;

    :cond_0
    const-string v0, "CARD_COLOR"

    .line 195
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->cardColor:I

    .line 196
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfoBundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/merchantprofile/ContactInfo;

    iput-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    const-string v0, "DESCRIPTION"

    .line 197
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->description:Ljava/lang/String;

    const-string v0, "FEATURED_IMAGE"

    .line 198
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 199
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImage:Landroid/net/Uri;

    const-string v0, "PENDING_FEATURED_IMAGE_WIDTH"

    .line 200
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImageWidth:I

    const-string v0, "PENDING_FEATURED_IMAGE_HEIGHT"

    .line 201
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImageHeight:I

    :cond_1
    const-string v0, "FEATURED_IMAGE_URL"

    .line 203
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageUrl:Ljava/lang/String;

    const-string v0, "FEATURED_IMAGE_WIDTH"

    .line 204
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageWidth:I

    const-string v0, "FEATURED_IMAGE_HEIGHT"

    .line 205
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageHeight:I

    const-string v0, "PUBLISH_ITEMS"

    .line 206
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->publishItems:Z

    const-string v0, "PHOTO_ON_RECEIPT"

    .line 207
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->photoOnReceipt:Z

    const-string v0, "MOBILE_BUSINESS"

    .line 209
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->mobileBusiness:Z

    .line 210
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->addressBundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/address/Address;

    iput-object p1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->address:Lcom/squareup/address/Address;

    .line 211
    iget-object p1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->businessAddressRelay:Lio/reactivex/subjects/BehaviorSubject;

    new-instance v0, Lkotlin/Pair;

    iget-boolean v1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->mobileBusiness:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->address:Lcom/squareup/address/Address;

    invoke-direct {v0, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p1, v0}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method saveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 165
    iget-boolean v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->appearInMarket:Z

    const-string v1, "APPEAR_IN_DIRECTORY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 166
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->businessName:Ljava/lang/String;

    const-string v1, "BUSINESS_NAME"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->profileImageUrl:Ljava/lang/String;

    const-string v1, "PROFILE_IMAGE"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->logo:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LOGO"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->mobileBusiness:Z

    const-string v1, "MOBILE_BUSINESS"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 172
    iget v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->cardColor:I

    const-string v1, "CARD_COLOR"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 173
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->addressBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->address:Lcom/squareup/address/Address;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfoBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->description:Ljava/lang/String;

    const-string v1, "DESCRIPTION"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImage:Landroid/net/Uri;

    if-eqz v0, :cond_1

    const-string v1, "FEATURED_IMAGE"

    .line 177
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 178
    iget v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImageWidth:I

    const-string v1, "PENDING_FEATURED_IMAGE_WIDTH"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 179
    iget v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImageHeight:I

    const-string v1, "PENDING_FEATURED_IMAGE_HEIGHT"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageUrl:Ljava/lang/String;

    const-string v1, "FEATURED_IMAGE_URL"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    iget v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageWidth:I

    const-string v1, "FEATURED_IMAGE_WIDTH"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 183
    iget v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageHeight:I

    const-string v1, "FEATURED_IMAGE_HEIGHT"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 184
    iget-boolean v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->publishItems:Z

    const-string v1, "PUBLISH_ITEMS"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 185
    iget-boolean v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->photoOnReceipt:Z

    const-string v1, "PHOTO_ON_RECEIPT"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public setAddress(Lcom/squareup/address/Address;)V
    .locals 3

    .line 111
    iput-object p1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->address:Lcom/squareup/address/Address;

    .line 112
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->businessAddressRelay:Lio/reactivex/subjects/BehaviorSubject;

    new-instance v1, Lkotlin/Pair;

    iget-boolean v2, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->mobileBusiness:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public setLogo(Ljava/io/File;Landroid/graphics/Bitmap;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez p2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-ne v2, v0, :cond_2

    .line 141
    iput-object p1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->logo:Ljava/io/File;

    .line 142
    invoke-direct {p0, p2}, Lcom/squareup/merchantprofile/MerchantProfileState;->setTempLogoBitmap(Landroid/graphics/Bitmap;)V

    return-void

    .line 139
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Both logo and bitmap must be null or non-null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setMobileBusiness(Ljava/lang/Boolean;)V
    .locals 3

    .line 120
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->mobileBusiness:Z

    .line 121
    iget-object p1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->businessAddressRelay:Lio/reactivex/subjects/BehaviorSubject;

    new-instance v0, Lkotlin/Pair;

    iget-boolean v1, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->mobileBusiness:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->address:Lcom/squareup/address/Address;

    invoke-direct {v0, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p1, v0}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public tempLogoBitmap()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/merchantprofile/MerchantProfileState;->tempLogoBitmap:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public toSnapshot()Lcom/squareup/merchantprofile/MerchantProfileSnapshot;
    .locals 21

    move-object/from16 v0, p0

    .line 248
    new-instance v19, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;

    move-object/from16 v1, v19

    iget-boolean v2, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->appearInMarket:Z

    iget-object v3, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->businessName:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->profileImageUrl:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageUrl:Ljava/lang/String;

    iget v6, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageWidth:I

    iget v7, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->featuredImageHeight:I

    iget-object v8, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->logo:Ljava/io/File;

    iget-boolean v9, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->mobileBusiness:Z

    iget v10, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->cardColor:I

    iget-object v11, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->address:Lcom/squareup/address/Address;

    iget-object v12, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->contactInfo:Lcom/squareup/merchantprofile/ContactInfo;

    iget-object v13, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->description:Ljava/lang/String;

    iget-boolean v14, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->publishItems:Z

    iget-boolean v15, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->photoOnReceipt:Z

    move-object/from16 v20, v1

    iget-object v1, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImage:Landroid/net/Uri;

    move-object/from16 v16, v1

    iget v1, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImageWidth:I

    move/from16 v17, v1

    iget v1, v0, Lcom/squareup/merchantprofile/MerchantProfileState;->pendingFeaturedImageHeight:I

    move/from16 v18, v1

    move-object/from16 v1, v20

    invoke-direct/range {v1 .. v18}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/io/File;ZILcom/squareup/address/Address;Lcom/squareup/merchantprofile/ContactInfo;Ljava/lang/String;ZZLandroid/net/Uri;II)V

    return-object v19
.end method
