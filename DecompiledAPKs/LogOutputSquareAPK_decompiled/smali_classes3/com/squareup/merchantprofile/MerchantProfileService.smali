.class public interface abstract Lcom/squareup/merchantprofile/MerchantProfileService;
.super Ljava/lang/Object;
.source "MerchantProfileService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\'J[\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\n\u0008\u0001\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\u0008\u0001\u0010\u0008\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\n\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\u000b\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\u000c\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\r\u001a\u0004\u0018\u00010\tH\'\u00a2\u0006\u0002\u0010\u000eJ\u00eb\u0001\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\n\u0008\u0001\u0010\u0010\u001a\u0004\u0018\u00010\u00072\n\u0008\u0001\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\u0008\u0001\u0010\u0011\u001a\u0004\u0018\u00010\u00072\n\u0008\u0001\u0010\u0012\u001a\u0004\u0018\u00010\u00132\n\u0008\u0001\u0010\u0014\u001a\u0004\u0018\u00010\u00132\n\u0008\u0001\u0010\u0015\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\u0016\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\u0008\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\n\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\u000b\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\u000c\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\r\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\u0017\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\u0018\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\u0019\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\u001a\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\u001b\u001a\u0004\u0018\u00010\t2\n\u0008\u0001\u0010\u001c\u001a\u0004\u0018\u00010\tH\'\u00a2\u0006\u0002\u0010\u001d\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/merchantprofile/MerchantProfileService;",
        "",
        "getMerchantProfile",
        "Lcom/squareup/server/SimpleStandardResponse;",
        "Lcom/squareup/server/account/MerchantProfileResponse;",
        "updateBusinessAddress",
        "mobileBusiness",
        "",
        "street1",
        "Lokhttp3/RequestBody;",
        "street2",
        "city",
        "state",
        "postalCode",
        "(Ljava/lang/Boolean;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;)Lcom/squareup/server/SimpleStandardResponse;",
        "updateMerchantProfile",
        "published",
        "useGeneratedMenu",
        "profileImage",
        "Lokhttp3/MultipartBody$Part;",
        "featuredImageUri",
        "cardColor",
        "name",
        "phone",
        "email",
        "facebook",
        "twitter",
        "website",
        "bio",
        "(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokhttp3/MultipartBody$Part;Lokhttp3/MultipartBody$Part;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;)Lcom/squareup/server/SimpleStandardResponse;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getMerchantProfile()Lcom/squareup/server/SimpleStandardResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/1.0/merchant-profile"
    .end annotation
.end method

.method public abstract updateBusinessAddress(Ljava/lang/Boolean;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lretrofit2/http/Part;
            value = "mobile_business"
        .end annotation
    .end param
    .param p2    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "street1"
        .end annotation
    .end param
    .param p3    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "street2"
        .end annotation
    .end param
    .param p4    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "city"
        .end annotation
    .end param
    .param p5    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "state"
        .end annotation
    .end param
    .param p6    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "postal_code"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Multipart;
    .end annotation

    .annotation runtime Lretrofit2/http/PUT;
        value = "/1.0/merchant-profile"
    .end annotation
.end method

.method public abstract updateMerchantProfile(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokhttp3/MultipartBody$Part;Lokhttp3/MultipartBody$Part;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lretrofit2/http/Part;
            value = "published"
        .end annotation
    .end param
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lretrofit2/http/Part;
            value = "mobile_business"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lretrofit2/http/Part;
            value = "use_generated_menu"
        .end annotation
    .end param
    .param p4    # Lokhttp3/MultipartBody$Part;
        .annotation runtime Lretrofit2/http/Part;
        .end annotation
    .end param
    .param p5    # Lokhttp3/MultipartBody$Part;
        .annotation runtime Lretrofit2/http/Part;
        .end annotation
    .end param
    .param p6    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "card_color"
        .end annotation
    .end param
    .param p7    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "name"
        .end annotation
    .end param
    .param p8    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "street1"
        .end annotation
    .end param
    .param p9    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "street2"
        .end annotation
    .end param
    .param p10    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "city"
        .end annotation
    .end param
    .param p11    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "state"
        .end annotation
    .end param
    .param p12    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "postal_code"
        .end annotation
    .end param
    .param p13    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "phone"
        .end annotation
    .end param
    .param p14    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "email"
        .end annotation
    .end param
    .param p15    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "facebook"
        .end annotation
    .end param
    .param p16    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "twitter"
        .end annotation
    .end param
    .param p17    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "website"
        .end annotation
    .end param
    .param p18    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "bio"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lokhttp3/MultipartBody$Part;",
            "Lokhttp3/MultipartBody$Part;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Multipart;
    .end annotation

    .annotation runtime Lretrofit2/http/PUT;
        value = "/1.0/merchant-profile"
    .end annotation
.end method
