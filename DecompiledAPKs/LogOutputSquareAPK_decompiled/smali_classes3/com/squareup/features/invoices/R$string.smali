.class public final Lcom/squareup/features/invoices/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final add_additional_recipient_v2:I = 0x7f12006d

.field public static final add_additional_recipient_v2_plural:I = 0x7f12006e

.field public static final add_additional_recipient_v2_single:I = 0x7f12006f

.field public static final add_additional_recipients:I = 0x7f120070

.field public static final add_another_payment:I = 0x7f120078

.field public static final add_attachment:I = 0x7f120079

.field public static final add_line_item:I = 0x7f120083

.field public static final add_payment_schedule:I = 0x7f120091

.field public static final add_recipients_title:I = 0x7f120094

.field public static final additional_email_to_cc:I = 0x7f1200a0

.field public static final additional_recipients:I = 0x7f1200a4

.field public static final additional_recipients_helper:I = 0x7f1200a5

.field public static final after_number:I = 0x7f1200be

.field public static final allow_automatic_payments_label:I = 0x7f1200c6

.field public static final archive_invoice:I = 0x7f1200de

.field public static final balance_split_all_caps:I = 0x7f120137

.field public static final cancel_and_issue_refund:I = 0x7f120281

.field public static final cancel_invoice_confirmation_text:I = 0x7f120292

.field public static final cancel_invoice_confirmation_text_partial_payment:I = 0x7f120293

.field public static final cancel_invoice_notify_recipients:I = 0x7f120294

.field public static final charge_amount_now:I = 0x7f1203fa

.field public static final charge_amount_now_description:I = 0x7f1203fb

.field public static final charge_now:I = 0x7f1203fc

.field public static final charge_now_disabled_description:I = 0x7f1203fd

.field public static final content_description_search_invoices:I = 0x7f1204cd

.field public static final create_invoice:I = 0x7f1205be

.field public static final create_recurring_series:I = 0x7f1205e9

.field public static final day:I = 0x7f1207d7

.field public static final days:I = 0x7f1207db

.field public static final deposit_amount_greater_than_zero:I = 0x7f1207ef

.field public static final deposit_amount_larger_than_invoice:I = 0x7f1207f0

.field public static final deposit_amount_less_than_completed:I = 0x7f1207f1

.field public static final deposit_request_all_caps:I = 0x7f1207f6

.field public static final details_header:I = 0x7f120832

.field public static final download_invoice_pdf_path:I = 0x7f1208d8

.field public static final drafts:I = 0x7f1208d9

.field public static final edit_balance:I = 0x7f1208fa

.field public static final edit_balance_title:I = 0x7f1208fb

.field public static final edit_deposit_title:I = 0x7f1208fd

.field public static final edit_invoice_add_customer:I = 0x7f120904

.field public static final edit_invoice_continue:I = 0x7f120905

.field public static final edit_invoice_copy_link:I = 0x7f120906

.field public static final edit_invoice_save:I = 0x7f120907

.field public static final edit_payment_schedule:I = 0x7f120949

.field public static final edit_payment_title:I = 0x7f12094a

.field public static final email_address:I = 0x7f120991

.field public static final end:I = 0x7f120a76

.field public static final end_date:I = 0x7f120a78

.field public static final estimate_banner_download_invoices_app:I = 0x7f120a88

.field public static final estimate_banner_message_has_estimates:I = 0x7f120a89

.field public static final estimate_banner_message_no_estimates:I = 0x7f120a8a

.field public static final estimate_banner_title_has_estimates:I = 0x7f120a8b

.field public static final estimate_banner_title_no_estimates:I = 0x7f120a8c

.field public static final estimate_banner_view_in_invoices_app:I = 0x7f120a8d

.field public static final estimate_details_title:I = 0x7f120a8e

.field public static final external_file_viewing_failed:I = 0x7f120aa2

.field public static final image_attachment:I = 0x7f120bf0

.field public static final image_attachment_upload:I = 0x7f120bf1

.field public static final image_upload:I = 0x7f120bf2

.field public static final invalid_balance_date_message:I = 0x7f120c50

.field public static final invalid_balance_date_title:I = 0x7f120c51

.field public static final invalid_deposit_amount_title:I = 0x7f120c56

.field public static final invalid_reminder_in_past:I = 0x7f120c63

.field public static final invalid_reminder_on_invoice_send_date:I = 0x7f120c64

.field public static final invalid_reminder_prior_to_send_date:I = 0x7f120c65

.field public static final invoice_add_attachment:I = 0x7f120c73

.field public static final invoice_add_message:I = 0x7f120c74

.field public static final invoice_add_reminder:I = 0x7f120c75

.field public static final invoice_allow_automatic_payments:I = 0x7f120c76

.field public static final invoice_allow_buyer_save_cof:I = 0x7f120c77

.field public static final invoice_allow_tip:I = 0x7f120c78

.field public static final invoice_archived:I = 0x7f120c7a

.field public static final invoice_attachment_default_name:I = 0x7f120c7b

.field public static final invoice_attachment_legal_disclaimer:I = 0x7f120c7c

.field public static final invoice_automatic_payment_auto_enable_buyer_cof:I = 0x7f120c7d

.field public static final invoice_automatic_payment_off:I = 0x7f120c82

.field public static final invoice_automatic_payment_on:I = 0x7f120c83

.field public static final invoice_automatic_payments:I = 0x7f120c85

.field public static final invoice_automatic_payments_description_subtitle:I = 0x7f120c86

.field public static final invoice_automatic_payments_description_title:I = 0x7f120c87

.field public static final invoice_automatic_reminders:I = 0x7f120c88

.field public static final invoice_automatic_reminders_explanation:I = 0x7f120c89

.field public static final invoice_automatic_reminders_off:I = 0x7f120c8a

.field public static final invoice_automatic_reminders_on:I = 0x7f120c8b

.field public static final invoice_automatic_reminders_schedule_for:I = 0x7f120c8c

.field public static final invoice_automatic_reminders_sent_on:I = 0x7f120c8d

.field public static final invoice_automatic_reminders_wont_be_sent_after_paid:I = 0x7f120c8e

.field public static final invoice_cancel_title:I = 0x7f120c8f

.field public static final invoice_canceled:I = 0x7f120c90

.field public static final invoice_canceled_message:I = 0x7f120c91

.field public static final invoice_charge_cof:I = 0x7f120c92

.field public static final invoice_charge_cof_action:I = 0x7f120c93

.field public static final invoice_charge_cof_concise:I = 0x7f120c94

.field public static final invoice_charged_title:I = 0x7f120c97

.field public static final invoice_choose_charge_date:I = 0x7f120c98

.field public static final invoice_choose_due_date:I = 0x7f120c9a

.field public static final invoice_choose_send_date:I = 0x7f120c9b

.field public static final invoice_choose_start_date:I = 0x7f120c9c

.field public static final invoice_connection_error_cancel:I = 0x7f120c9e

.field public static final invoice_connection_error_charge:I = 0x7f120c9f

.field public static final invoice_connection_error_create:I = 0x7f120ca0

.field public static final invoice_connection_error_delete:I = 0x7f120ca1

.field public static final invoice_connection_error_delete_series:I = 0x7f120ca2

.field public static final invoice_connection_error_end_series:I = 0x7f120ca3

.field public static final invoice_connection_error_invoice_service_series_subtitle:I = 0x7f120ca4

.field public static final invoice_connection_error_invoice_service_subtitle:I = 0x7f120ca5

.field public static final invoice_connection_error_message:I = 0x7f120ca6

.field public static final invoice_connection_error_preview:I = 0x7f120ca7

.field public static final invoice_connection_error_resend:I = 0x7f120ca8

.field public static final invoice_connection_error_save:I = 0x7f120ca9

.field public static final invoice_connection_error_save_recurring_draft:I = 0x7f120caa

.field public static final invoice_connection_error_schedule:I = 0x7f120cab

.field public static final invoice_connection_error_schedule_recurring:I = 0x7f120cac

.field public static final invoice_connection_error_send:I = 0x7f120cad

.field public static final invoice_connection_error_send_reminder:I = 0x7f120cae

.field public static final invoice_connection_error_update:I = 0x7f120caf

.field public static final invoice_connection_error_update_series:I = 0x7f120cb0

.field public static final invoice_connection_error_upload:I = 0x7f120cb1

.field public static final invoice_create_discard:I = 0x7f120cb2

.field public static final invoice_create_discard_message:I = 0x7f120cb3

.field public static final invoice_delete_series_title:I = 0x7f120cba

.field public static final invoice_delete_title:I = 0x7f120cbb

.field public static final invoice_deleted:I = 0x7f120cbc

.field public static final invoice_deleted_series:I = 0x7f120cbd

.field public static final invoice_delivery_method:I = 0x7f120cbe

.field public static final invoice_delivery_method_cof_default:I = 0x7f120cbf

.field public static final invoice_delivery_method_manual_support:I = 0x7f120cc2

.field public static final invoice_delivery_method_select:I = 0x7f120cc3

.field public static final invoice_delivery_method_title:I = 0x7f120cc4

.field public static final invoice_detail_delete_draft:I = 0x7f120cd2

.field public static final invoice_detail_delete_draft_confirmation:I = 0x7f120cd3

.field public static final invoice_detail_delete_draft_recurring_confirmation:I = 0x7f120cd4

.field public static final invoice_detail_download_invoice:I = 0x7f120cd5

.field public static final invoice_detail_edit_button:I = 0x7f120cd8

.field public static final invoice_detail_end_series:I = 0x7f120cda

.field public static final invoice_detail_end_series_confirmation:I = 0x7f120cdb

.field public static final invoice_detail_series_title:I = 0x7f120ce2

.field public static final invoice_details_title:I = 0x7f120ce9

.field public static final invoice_display_state_active_series_null_state:I = 0x7f120cea

.field public static final invoice_display_state_canceled_null_state:I = 0x7f120ced

.field public static final invoice_display_state_draft_null_state:I = 0x7f120cef

.field public static final invoice_display_state_draft_series_null_state:I = 0x7f120cf0

.field public static final invoice_display_state_ended_series_null_state:I = 0x7f120cf1

.field public static final invoice_display_state_failed_null_state:I = 0x7f120cf3

.field public static final invoice_display_state_overdue_null_state:I = 0x7f120cf5

.field public static final invoice_display_state_paid_null_state:I = 0x7f120cf7

.field public static final invoice_display_state_recurring_null_state:I = 0x7f120cfa

.field public static final invoice_display_state_refunded_null_state:I = 0x7f120cfc

.field public static final invoice_display_state_scheduled_null_state:I = 0x7f120cfe

.field public static final invoice_display_state_undelivered_null_state:I = 0x7f120d00

.field public static final invoice_display_state_unknown_null_state:I = 0x7f120d02

.field public static final invoice_display_state_unpaid_null_state:I = 0x7f120d04

.field public static final invoice_duplicate_reminder_date:I = 0x7f120d05

.field public static final invoice_duplicate_reminder_date_explanation:I = 0x7f120d06

.field public static final invoice_each_invoice_due:I = 0x7f120d07

.field public static final invoice_edit_cancel:I = 0x7f120d08

.field public static final invoice_edit_cancel_confirm:I = 0x7f120d09

.field public static final invoice_edit_charge_invoice:I = 0x7f120d0a

.field public static final invoice_edit_create_invoice:I = 0x7f120d0b

.field public static final invoice_edit_delete_draft:I = 0x7f120d0c

.field public static final invoice_edit_delete_draft_confirm:I = 0x7f120d0d

.field public static final invoice_edit_delete_draft_recurring_confirm:I = 0x7f120d0e

.field public static final invoice_edit_discard:I = 0x7f120d0f

.field public static final invoice_edit_discard_message:I = 0x7f120d10

.field public static final invoice_edit_invoice:I = 0x7f120d11

.field public static final invoice_edit_new_invoice:I = 0x7f120d12

.field public static final invoice_edit_new_recurring_series:I = 0x7f120d13

.field public static final invoice_edit_recurring_series:I = 0x7f120d14

.field public static final invoice_edit_save_and_close:I = 0x7f120d15

.field public static final invoice_edit_save_as_draft:I = 0x7f120d16

.field public static final invoice_edit_schedule_invoice:I = 0x7f120d17

.field public static final invoice_edit_send_invoice:I = 0x7f120d18

.field public static final invoice_edit_update_invoice:I = 0x7f120d19

.field public static final invoice_end_series:I = 0x7f120d1b

.field public static final invoice_file_attachment_count_limit_description:I = 0x7f120d1c

.field public static final invoice_file_attachment_count_limit_title:I = 0x7f120d1d

.field public static final invoice_file_attachment_size_limit_description:I = 0x7f120d1e

.field public static final invoice_file_attachment_size_limit_title:I = 0x7f120d1f

.field public static final invoice_file_attachment_source_attach_pdf:I = 0x7f120d20

.field public static final invoice_frequency:I = 0x7f120d21

.field public static final invoice_frequency_end:I = 0x7f120d22

.field public static final invoice_frequency_one_time:I = 0x7f120d23

.field public static final invoice_frequency_repeat_every:I = 0x7f120d24

.field public static final invoice_gift_card_unspported_message:I = 0x7f120d25

.field public static final invoice_hint_id:I = 0x7f120d26

.field public static final invoice_hint_message:I = 0x7f120d27

.field public static final invoice_hint_recipient_additional_email:I = 0x7f120d28

.field public static final invoice_hint_recipient_email:I = 0x7f120d29

.field public static final invoice_hint_recipient_name:I = 0x7f120d2a

.field public static final invoice_hint_title:I = 0x7f120d2b

.field public static final invoice_history_title:I = 0x7f120d2c

.field public static final invoice_image_download_failed:I = 0x7f120d2e

.field public static final invoice_message:I = 0x7f120d33

.field public static final invoice_method:I = 0x7f120d34

.field public static final invoice_next_invoice:I = 0x7f120d35

.field public static final invoice_no_invoice_null_state_subtitle:I = 0x7f120d39

.field public static final invoice_no_invoice_null_state_title:I = 0x7f120d3a

.field public static final invoice_no_invoices_for_filter_message:I = 0x7f120d3b

.field public static final invoice_no_invoices_for_filter_title:I = 0x7f120d3c

.field public static final invoice_payment_schedule_title:I = 0x7f120d42

.field public static final invoice_preview_button:I = 0x7f120d43

.field public static final invoice_preview_title:I = 0x7f120d44

.field public static final invoice_recurring_start_date_eom_error_message:I = 0x7f120d63

.field public static final invoice_recurring_start_date_eom_error_title:I = 0x7f120d64

.field public static final invoice_reminder:I = 0x7f120d65

.field public static final invoice_reminder_confirmation_body:I = 0x7f120d66

.field public static final invoice_reminder_confirmation_title:I = 0x7f120d67

.field public static final invoice_reminder_error_message_default:I = 0x7f120d68

.field public static final invoice_reminder_send_after_due_date:I = 0x7f120d69

.field public static final invoice_reminder_send_after_due_date_option:I = 0x7f120d6a

.field public static final invoice_reminder_send_after_due_date_plural_option:I = 0x7f120d6b

.field public static final invoice_reminder_send_before_due_date:I = 0x7f120d6c

.field public static final invoice_reminder_send_before_due_date_option:I = 0x7f120d6d

.field public static final invoice_reminder_send_before_due_date_plural_option:I = 0x7f120d6e

.field public static final invoice_reminder_send_on_date:I = 0x7f120d6f

.field public static final invoice_reminder_sent:I = 0x7f120d70

.field public static final invoice_reminder_title:I = 0x7f120d71

.field public static final invoice_remove_reminder:I = 0x7f120d72

.field public static final invoice_request_shipping_address:I = 0x7f120d73

.field public static final invoice_request_tip:I = 0x7f120d74

.field public static final invoice_resend:I = 0x7f120d75

.field public static final invoice_resend_title:I = 0x7f120d76

.field public static final invoice_resent:I = 0x7f120d77

.field public static final invoice_save_cof_helper:I = 0x7f120d78

.field public static final invoice_save_cof_recurring_helper:I = 0x7f120d79

.field public static final invoice_save_recurring_title:I = 0x7f120d7a

.field public static final invoice_save_title:I = 0x7f120d7b

.field public static final invoice_saved_recurring:I = 0x7f120d7d

.field public static final invoice_schedule:I = 0x7f120d7e

.field public static final invoice_schedule_recurring_title:I = 0x7f120d7f

.field public static final invoice_scheduled_recurring_title:I = 0x7f120d81

.field public static final invoice_scheduled_title:I = 0x7f120d82

.field public static final invoice_search_active_series:I = 0x7f120d83

.field public static final invoice_search_all_invoices:I = 0x7f120d84

.field public static final invoice_search_all_outstanding:I = 0x7f120d85

.field public static final invoice_search_archived:I = 0x7f120d86

.field public static final invoice_search_draft_series:I = 0x7f120d87

.field public static final invoice_search_drafts:I = 0x7f120d88

.field public static final invoice_search_in_series:I = 0x7f120d89

.field public static final invoice_search_inactive_series:I = 0x7f120d8a

.field public static final invoice_search_paid:I = 0x7f120d8b

.field public static final invoice_search_scheduled:I = 0x7f120d8c

.field public static final invoice_send_action:I = 0x7f120d8e

.field public static final invoice_send_automatic_reminders:I = 0x7f120d8f

.field public static final invoice_send_reminder_button:I = 0x7f120d91

.field public static final invoice_send_reminder_confirmation_message:I = 0x7f120d92

.field public static final invoice_send_reminder_custom_message_placeholder:I = 0x7f120d93

.field public static final invoice_send_reminder_custom_message_title:I = 0x7f120d94

.field public static final invoice_send_reminder_title:I = 0x7f120d95

.field public static final invoice_send_title:I = 0x7f120d96

.field public static final invoice_series_ended:I = 0x7f120d98

.field public static final invoice_set_default_message:I = 0x7f120d99

.field public static final invoice_set_default_message_error:I = 0x7f120d9a

.field public static final invoice_set_default_message_helper:I = 0x7f120d9b

.field public static final invoice_set_default_message_saving:I = 0x7f120d9c

.field public static final invoice_set_default_message_set:I = 0x7f120d9d

.field public static final invoice_start:I = 0x7f120da2

.field public static final invoice_state_filter_all_outstanding:I = 0x7f120da3

.field public static final invoice_state_filter_all_outstanding_null_state:I = 0x7f120da4

.field public static final invoice_state_filter_all_sent:I = 0x7f120da5

.field public static final invoice_state_filter_all_sent_null_state:I = 0x7f120da6

.field public static final invoice_state_filter_unsuccessful:I = 0x7f120da7

.field public static final invoice_state_filter_unsuccessful_null_state:I = 0x7f120da8

.field public static final invoice_status_failed:I = 0x7f120daa

.field public static final invoice_timeline_title:I = 0x7f120db3

.field public static final invoice_unable_to_load_image:I = 0x7f120db4

.field public static final invoice_unarchived:I = 0x7f120db5

.field public static final invoice_unsupported_message_discounts:I = 0x7f120db7

.field public static final invoice_unsupported_message_modifiers:I = 0x7f120db8

.field public static final invoice_unsupported_title:I = 0x7f120dba

.field public static final invoice_update_settings_failure_title:I = 0x7f120dbb

.field public static final invoice_updated:I = 0x7f120dbc

.field public static final invoice_updated_recurring_title:I = 0x7f120dbd

.field public static final invoice_validation_error_deposit_after_balance:I = 0x7f120dbf

.field public static final invoice_validation_error_no_customer:I = 0x7f120dc0

.field public static final invoice_validation_error_no_customer_name:I = 0x7f120dc1

.field public static final invoice_validation_error_no_email:I = 0x7f120dc2

.field public static final invoice_view_message:I = 0x7f120dc3

.field public static final invoices_app_landing_page_url:I = 0x7f120dc4

.field public static final invoices_app_package_name:I = 0x7f120dc5

.field public static final issue_refund:I = 0x7f120dc6

.field public static final last_days:I = 0x7f120eb0

.field public static final make_this_invoice_recurring:I = 0x7f120f8a

.field public static final month:I = 0x7f121020

.field public static final months:I = 0x7f121023

.field public static final multiple_due_dates:I = 0x7f121047

.field public static final multiple_reminder_schedules:I = 0x7f121048

.field public static final network_error_failed:I = 0x7f12105c

.field public static final never:I = 0x7f121061

.field public static final no_custom_invoice_id:I = 0x7f121080

.field public static final no_invoice_title:I = 0x7f121089

.field public static final on_a_set_date:I = 0x7f1210e3

.field public static final one_time:I = 0x7f121126

.field public static final open_file:I = 0x7f121164

.field public static final open_menu:I = 0x7f121168

.field public static final payment_amount_option_next_payment:I = 0x7f1213af

.field public static final payment_amount_option_other_amount:I = 0x7f1213b0

.field public static final payment_amount_option_remaining_amount:I = 0x7f1213b1

.field public static final payment_recorded:I = 0x7f1213d9

.field public static final payment_recorded_failed:I = 0x7f1213da

.field public static final payment_recorded_tender_type_on_date:I = 0x7f1213db

.field public static final payment_request_balance_due_date:I = 0x7f1213dc

.field public static final payment_request_deposit_due_date:I = 0x7f1213dd

.field public static final payment_request_due_in:I = 0x7f1213de

.field public static final payment_request_due_in_no_due_text:I = 0x7f1213df

.field public static final payment_request_due_upon_receipt:I = 0x7f1213e1

.field public static final payment_request_due_upon_receipt_no_due_text:I = 0x7f1213e2

.field public static final payment_request_payment_due_date:I = 0x7f1213e8

.field public static final payment_request_summary:I = 0x7f1213e9

.field public static final payment_schedule:I = 0x7f1213ea

.field public static final payment_schedule_all_caps:I = 0x7f1213eb

.field public static final payment_schedule_balance_section_money:I = 0x7f1213ec

.field public static final payment_schedule_balance_section_percentage:I = 0x7f1213ed

.field public static final payment_schedule_deposit_section:I = 0x7f1213ee

.field public static final payment_schedule_null_state:I = 0x7f1213ef

.field public static final payment_schedule_validation_balance_high:I = 0x7f1213f0

.field public static final payment_schedule_validation_balance_high_money:I = 0x7f1213f1

.field public static final payment_schedule_validation_balance_low:I = 0x7f1213f2

.field public static final payment_schedule_validation_balance_low_body:I = 0x7f1213f3

.field public static final payment_schedule_validation_balance_one_hundred_percent:I = 0x7f1213f4

.field public static final payment_schedule_validation_conflicting_due_date:I = 0x7f1213f5

.field public static final payment_schedule_validation_conflicting_due_dates_body:I = 0x7f1213f6

.field public static final payment_schedule_validation_deposit_after_balance:I = 0x7f1213f7

.field public static final payment_schedule_validation_invalid_amount:I = 0x7f1213f8

.field public static final payment_schedule_validation_invalid_amount_body:I = 0x7f1213f9

.field public static final payment_type:I = 0x7f1213fa

.field public static final payment_type_above_maximum_invoice:I = 0x7f1213fe

.field public static final payment_type_below_minimum_invoice:I = 0x7f121402

.field public static final payment_type_need_to_charge_card:I = 0x7f121404

.field public static final payment_type_need_to_charge_card_subtitle:I = 0x7f121405

.field public static final payment_type_share_payment_link:I = 0x7f121409

.field public static final pdf_attachment:I = 0x7f121427

.field public static final percentage_of_invoice_balance:I = 0x7f121431

.field public static final percentage_of_invoice_total:I = 0x7f121432

.field public static final preview_and_send:I = 0x7f121497

.field public static final record_payment:I = 0x7f1215e3

.field public static final record_payment_date:I = 0x7f1215e6

.field public static final record_payment_description:I = 0x7f1215e7

.field public static final record_payment_method_label:I = 0x7f1215e8

.field public static final record_payment_method_title:I = 0x7f1215e9

.field public static final record_payment_note_hint:I = 0x7f1215ea

.field public static final record_payment_save:I = 0x7f1215eb

.field public static final record_payment_send_receipt:I = 0x7f1215ec

.field public static final recurring:I = 0x7f1215f0

.field public static final recurring_frequency:I = 0x7f1215f2

.field public static final recurring_series_updated:I = 0x7f1215f4

.field public static final recurring_state_filter_active:I = 0x7f1215f5

.field public static final recurring_state_filter_active_short:I = 0x7f1215f6

.field public static final recurring_state_filter_draft:I = 0x7f1215f7

.field public static final recurring_state_filter_draft_short:I = 0x7f1215f8

.field public static final recurring_state_filter_inactive:I = 0x7f1215f9

.field public static final recurring_state_filter_inactive_short:I = 0x7f1215fa

.field public static final recurring_state_filter_unknown:I = 0x7f1215fb

.field public static final reminder_save_error_duplicate_days_body:I = 0x7f121662

.field public static final reminder_save_error_duplicate_days_title:I = 0x7f121663

.field public static final reminder_scheduled:I = 0x7f121664

.field public static final reminder_scheduled_description:I = 0x7f121665

.field public static final reminders_off_description:I = 0x7f121666

.field public static final reminders_scheduled_description:I = 0x7f121667

.field public static final remove_deposit_request:I = 0x7f12166a

.field public static final remove_deposit_request_confirm_message:I = 0x7f12166b

.field public static final remove_deposit_request_confirm_title:I = 0x7f12166c

.field public static final remove_invoice_attachment:I = 0x7f12166e

.field public static final remove_invoice_attachment_confirm:I = 0x7f12166f

.field public static final remove_payment_request:I = 0x7f121670

.field public static final repeat_every:I = 0x7f121675

.field public static final request_deposit:I = 0x7f121696

.field public static final request_initial_deposit:I = 0x7f121697

.field public static final save_message_as_default:I = 0x7f121771

.field public static final split_balance_into_milestones:I = 0x7f121844

.field public static final standalone_percent_character:I = 0x7f1218ac

.field public static final tap_to_preview_file:I = 0x7f12191a

.field public static final titlecase_invoices:I = 0x7f1219dc

.field public static final titlecase_recurring_series:I = 0x7f1219df

.field public static final turn_off_reminders_body:I = 0x7f121a6b

.field public static final turn_off_reminders_title:I = 0x7f121a6c

.field public static final unarchive_invoice:I = 0x7f121ae3

.field public static final update_image:I = 0x7f121af2

.field public static final update_invoice:I = 0x7f121af3

.field public static final upload_failed_default:I = 0x7f121afb

.field public static final upload_file_invoice_error:I = 0x7f121afc

.field public static final upload_file_invoice_progress:I = 0x7f121afd

.field public static final upload_file_invoice_successful:I = 0x7f121afe

.field public static final upload_success:I = 0x7f121aff

.field public static final uploaded_at_time_stamp:I = 0x7f121b04

.field public static final uppercase_bill_to:I = 0x7f121b09

.field public static final uppercase_communication:I = 0x7f121b22

.field public static final uppercase_edit_invoice_details_title_id:I = 0x7f121b26

.field public static final uppercase_file_name:I = 0x7f121b29

.field public static final uppercase_invoice_file_name:I = 0x7f121b45

.field public static final uppercase_more_options:I = 0x7f121b51

.field public static final uppercase_overview:I = 0x7f121b59

.field public static final uppercase_payment_details:I = 0x7f121b5c

.field public static final uppercase_preview:I = 0x7f121b65

.field public static final uppercase_recurring_options:I = 0x7f121b71

.field public static final week:I = 0x7f121bd6

.field public static final weeks:I = 0x7f121bd9

.field public static final within_hundred_fifty_days:I = 0x7f121be4

.field public static final within_hundred_twenty_days:I = 0x7f121be5

.field public static final within_ninety_days:I = 0x7f121be6

.field public static final within_sixty_days:I = 0x7f121be7

.field public static final within_thirty_days:I = 0x7f121be8

.field public static final year:I = 0x7f121bed

.field public static final years:I = 0x7f121bef

.field public static final zero_amount_error:I = 0x7f121bf6

.field public static final zero_amount_error_body:I = 0x7f121bf7


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
