.class public final Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRendererKt;
.super Ljava/lang/Object;
.source "InvoiceV2SectionRenderer.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "enableLinksInDescription",
        "",
        "Lcom/squareup/noho/NohoRow;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final enableLinksInDescription(Lcom/squareup/noho/NohoRow;)V
    .locals 1

    const-string v0, "$this$enableLinksInDescription"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    check-cast p0, Landroid/view/View;

    .line 291
    sget v0, Lcom/squareup/noho/R$id;->description:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/squareup/noho/NohoLabel;

    .line 292
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoLabel;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    return-void
.end method
