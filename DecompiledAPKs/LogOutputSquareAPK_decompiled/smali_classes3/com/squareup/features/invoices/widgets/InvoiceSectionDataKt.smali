.class public final Lcom/squareup/features/invoices/widgets/InvoiceSectionDataKt;
.super Ljava/lang/Object;
.source "InvoiceSectionData.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceSectionData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceSectionData.kt\ncom/squareup/features/invoices/widgets/InvoiceSectionDataKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,245:1\n1370#2:246\n1401#2,4:247\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceSectionData.kt\ncom/squareup/features/invoices/widgets/InvoiceSectionDataKt\n*L\n237#1:246\n237#1,4:247\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u001a\u0016\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00020\u0001\u00a8\u0006\u0003"
    }
    d2 = {
        "addDividers",
        "",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final addDividers(Ljava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$addDividers"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    move-object v0, p0

    check-cast v0, Ljava/lang/Iterable;

    .line 246
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 248
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_0

    .line 249
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    move-object v5, v3

    check-cast v5, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    .line 238
    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->getLastIndex(Ljava/util/List;)I

    move-result v3

    if-eq v2, v3, :cond_1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x7

    const/4 v11, 0x0

    .line 239
    invoke-static/range {v5 .. v11}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->copy$default(Lcom/squareup/features/invoices/widgets/InvoiceSectionData;Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZILjava/lang/Object;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    move-result-object v5

    .line 238
    :cond_1
    invoke-interface {v1, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v2, v4

    goto :goto_0

    .line 250
    :cond_2
    check-cast v1, Ljava/util/List;

    return-object v1
.end method
