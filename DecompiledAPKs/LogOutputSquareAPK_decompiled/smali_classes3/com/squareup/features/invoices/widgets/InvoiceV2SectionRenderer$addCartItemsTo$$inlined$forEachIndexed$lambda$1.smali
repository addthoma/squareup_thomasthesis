.class public final Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$addCartItemsTo$$inlined$forEachIndexed$lambda$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->addCartItemsTo(Lcom/squareup/ui/cart/CartEntryViews;Landroid/widget/LinearLayout;Ljava/util/List;Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 InvoiceV2SectionRenderer.kt\ncom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer\n*L\n1#1,1322:1\n225#2:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0007"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release",
        "com/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$$special$$inlined$onClickDebounced$1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $event$inlined:Ljava/lang/Object;

.field final synthetic $i$inlined:I

.field final synthetic $itemizations$inlined:Ljava/util/List;

.field final synthetic $lineItemsContainer$inlined:Landroid/widget/LinearLayout;

.field final synthetic $onEvent$inlined:Lkotlin/jvm/functions/Function1;

.field final synthetic this$0:Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;


# direct methods
.method public constructor <init>(ILcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;Landroid/widget/LinearLayout;Ljava/util/List;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$addCartItemsTo$$inlined$forEachIndexed$lambda$1;->$i$inlined:I

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$addCartItemsTo$$inlined$forEachIndexed$lambda$1;->this$0:Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;

    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$addCartItemsTo$$inlined$forEachIndexed$lambda$1;->$lineItemsContainer$inlined:Landroid/widget/LinearLayout;

    iput-object p4, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$addCartItemsTo$$inlined$forEachIndexed$lambda$1;->$itemizations$inlined:Ljava/util/List;

    iput-object p5, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$addCartItemsTo$$inlined$forEachIndexed$lambda$1;->$event$inlined:Ljava/lang/Object;

    iput-object p6, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$addCartItemsTo$$inlined$forEachIndexed$lambda$1;->$onEvent$inlined:Lkotlin/jvm/functions/Function1;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Landroid/widget/LinearLayout;

    .line 1323
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$addCartItemsTo$$inlined$forEachIndexed$lambda$1;->$onEvent$inlined:Lkotlin/jvm/functions/Function1;

    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$LineItemClicked;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$addCartItemsTo$$inlined$forEachIndexed$lambda$1;->$event$inlined:Ljava/lang/Object;

    iget v2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$addCartItemsTo$$inlined$forEachIndexed$lambda$1;->$i$inlined:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$LineItemClicked;-><init>(Ljava/lang/Object;I)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
