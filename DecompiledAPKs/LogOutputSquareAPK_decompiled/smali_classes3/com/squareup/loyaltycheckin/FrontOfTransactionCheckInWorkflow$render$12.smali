.class final Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$12;
.super Lkotlin/jvm/internal/Lambda;
.source "FrontOfTransactionCheckInWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->render(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
        "+",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        "it",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$12;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    iget-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$12;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;

    invoke-static {p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$getExitAction$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$12;->invoke(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
