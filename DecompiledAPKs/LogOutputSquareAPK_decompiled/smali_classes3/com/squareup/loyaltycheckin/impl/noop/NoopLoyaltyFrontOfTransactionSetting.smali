.class public final Lcom/squareup/loyaltycheckin/impl/noop/NoopLoyaltyFrontOfTransactionSetting;
.super Ljava/lang/Object;
.source "NoopLoyaltyFrontOfTransactionSetting.kt"

# interfaces
.implements Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0008H\u0016J\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u000bH\u0016J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0008H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/impl/noop/NoopLoyaltyFrontOfTransactionSetting;",
        "Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/Features;)V",
        "getFeatures",
        "()Lcom/squareup/settings/server/Features;",
        "featureFlagOn",
        "",
        "getValue",
        "getValuesStream",
        "Lio/reactivex/Observable;",
        "setValueLocallyInternal",
        "",
        "enabledLocally",
        "impl-noop_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/impl/noop/NoopLoyaltyFrontOfTransactionSetting;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public featureFlagOn()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getFeatures()Lcom/squareup/settings/server/Features;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/impl/noop/NoopLoyaltyFrontOfTransactionSetting;->features:Lcom/squareup/settings/server/Features;

    return-object v0
.end method

.method public getValue()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getValuesStream()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 13
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.just(false)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public setValueLocallyInternal(Z)V
    .locals 0

    return-void
.end method
