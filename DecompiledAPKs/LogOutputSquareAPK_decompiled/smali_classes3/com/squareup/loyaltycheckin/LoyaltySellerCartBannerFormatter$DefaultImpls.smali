.class public final Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter$DefaultImpls;
.super Ljava/lang/Object;
.source "LoyaltySellerCartBannerFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static isCheckInFlowActive(Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 36
    invoke-interface {p0}, Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;->banner()Lio/reactivex/Observable;

    move-result-object p0

    .line 37
    sget-object v0, Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter$isCheckInFlowActive$1;->INSTANCE:Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter$isCheckInFlowActive$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    .line 38
    invoke-virtual {p0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "banner()\n      .map { it\u2026  .distinctUntilChanged()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
