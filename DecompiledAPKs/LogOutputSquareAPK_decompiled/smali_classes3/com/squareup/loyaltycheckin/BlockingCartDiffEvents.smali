.class public final Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents;
.super Ljava/lang/Object;
.source "BlockingCartDiffEvents.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\u000bB\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0005J\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\nR\u001c\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents;",
        "",
        "()V",
        "cartDiffs",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState;",
        "kotlin.jvm.PlatformType",
        "cartDiffReceived",
        "",
        "cartDiff",
        "Lio/reactivex/Observable;",
        "CartDiffState",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cartDiffs:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    const-string v1, "PublishRelay.create<CartDiffState>()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents;->cartDiffs:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method


# virtual methods
.method public final cartDiffReceived(Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState;)V
    .locals 1

    const-string v0, "cartDiff"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents;->cartDiffs:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final cartDiffs()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents$CartDiffState;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/BlockingCartDiffEvents;->cartDiffs:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
