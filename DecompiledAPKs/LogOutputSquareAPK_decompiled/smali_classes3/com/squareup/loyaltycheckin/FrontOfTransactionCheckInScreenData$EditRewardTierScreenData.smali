.class public final Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;
.super Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;
.source "FrontOfTransactionCheckInScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EditRewardTierScreenData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0014\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00c6\u0003J;\u0010\u0018\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00052\u0008\u0008\u0002\u0010\t\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u0019\u001a\u00020\u0007H\u00d6\u0001J\u0013\u0010\u001a\u001a\u00020\u00052\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u0007H\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\u0019\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u0007H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\t\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;",
        "tier",
        "Lcom/squareup/loyaltycheckin/Tier;",
        "confirmEnabled",
        "",
        "newQuantity",
        "",
        "minusIconEnabled",
        "plusIconEnabled",
        "(Lcom/squareup/loyaltycheckin/Tier;ZIZZ)V",
        "getConfirmEnabled",
        "()Z",
        "getMinusIconEnabled",
        "getNewQuantity",
        "()I",
        "getPlusIconEnabled",
        "getTier",
        "()Lcom/squareup/loyaltycheckin/Tier;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "describeContents",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final confirmEnabled:Z

.field private final minusIconEnabled:Z

.field private final newQuantity:I

.field private final plusIconEnabled:Z

.field private final tier:Lcom/squareup/loyaltycheckin/Tier;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData$Creator;

    invoke-direct {v0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData$Creator;-><init>()V

    sput-object v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/loyaltycheckin/Tier;ZIZZ)V
    .locals 1

    const-string/jumbo v0, "tier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0, v0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->tier:Lcom/squareup/loyaltycheckin/Tier;

    iput-boolean p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->confirmEnabled:Z

    iput p3, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->newQuantity:I

    iput-boolean p4, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->minusIconEnabled:Z

    iput-boolean p5, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->plusIconEnabled:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;Lcom/squareup/loyaltycheckin/Tier;ZIZZILjava/lang/Object;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->tier:Lcom/squareup/loyaltycheckin/Tier;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-boolean p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->confirmEnabled:Z

    :cond_1
    move p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->newQuantity:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->minusIconEnabled:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->plusIconEnabled:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move p4, p7

    move p5, v0

    move p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->copy(Lcom/squareup/loyaltycheckin/Tier;ZIZZ)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/loyaltycheckin/Tier;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->tier:Lcom/squareup/loyaltycheckin/Tier;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->confirmEnabled:Z

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->newQuantity:I

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->minusIconEnabled:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->plusIconEnabled:Z

    return v0
.end method

.method public final copy(Lcom/squareup/loyaltycheckin/Tier;ZIZZ)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;
    .locals 7

    const-string/jumbo v0, "tier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;

    move-object v1, v0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;-><init>(Lcom/squareup/loyaltycheckin/Tier;ZIZZ)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;

    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->tier:Lcom/squareup/loyaltycheckin/Tier;

    iget-object v1, p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->tier:Lcom/squareup/loyaltycheckin/Tier;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->confirmEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->confirmEnabled:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->newQuantity:I

    iget v1, p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->newQuantity:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->minusIconEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->minusIconEnabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->plusIconEnabled:Z

    iget-boolean p1, p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->plusIconEnabled:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getConfirmEnabled()Z
    .locals 1

    .line 41
    iget-boolean v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->confirmEnabled:Z

    return v0
.end method

.method public final getMinusIconEnabled()Z
    .locals 1

    .line 43
    iget-boolean v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->minusIconEnabled:Z

    return v0
.end method

.method public final getNewQuantity()I
    .locals 1

    .line 42
    iget v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->newQuantity:I

    return v0
.end method

.method public final getPlusIconEnabled()Z
    .locals 1

    .line 44
    iget-boolean v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->plusIconEnabled:Z

    return v0
.end method

.method public final getTier()Lcom/squareup/loyaltycheckin/Tier;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->tier:Lcom/squareup/loyaltycheckin/Tier;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->tier:Lcom/squareup/loyaltycheckin/Tier;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->confirmEnabled:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->newQuantity:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->minusIconEnabled:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->plusIconEnabled:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EditRewardTierScreenData(tier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->tier:Lcom/squareup/loyaltycheckin/Tier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", confirmEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->confirmEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", newQuantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->newQuantity:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", minusIconEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->minusIconEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", plusIconEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->plusIconEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->tier:Lcom/squareup/loyaltycheckin/Tier;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-boolean p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->confirmEnabled:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->newQuantity:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->minusIconEnabled:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;->plusIconEnabled:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
