.class public abstract Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;
.super Ljava/lang/Object;
.source "FrontOfTransactionCheckInState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;,
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$ErrorCheckingExistingContactAddedToSale;,
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$EnteringPhone;,
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;,
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$ErrorLookingUpPhone;,
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;,
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$RewardCarousel;,
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$NewLoyaltyAccount;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0008\u0003\u0004\u0005\u0006\u0007\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0008\u000b\u000c\r\u000e\u000f\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
        "",
        "()V",
        "CheckingExistingContactAddedToSale",
        "EnteringPhone",
        "ErrorCheckingExistingContactAddedToSale",
        "ErrorLookingUpPhone",
        "LookingUpPhone",
        "NewLoyaltyAccount",
        "RewardCarousel",
        "WelcomeScreen",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$ErrorCheckingExistingContactAddedToSale;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$EnteringPhone;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$ErrorLookingUpPhone;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$RewardCarousel;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$NewLoyaltyAccount;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;-><init>()V

    return-void
.end method
