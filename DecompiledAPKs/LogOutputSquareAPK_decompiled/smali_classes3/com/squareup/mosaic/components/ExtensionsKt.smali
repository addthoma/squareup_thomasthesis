.class public final Lcom/squareup/mosaic/components/ExtensionsKt;
.super Ljava/lang/Object;
.source "Extensions.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nExtensions.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Extensions.kt\ncom/squareup/mosaic/components/ExtensionsKt\n+ 2 MosaicBlock.kt\ncom/squareup/blueprint/mosaic/MosaicBlockKt\n*L\n1#1,18:1\n16#2,7:19\n20#2,4:26\n15#2,8:30\n20#2,4:38\n*E\n*S KotlinDebug\n*F\n+ 1 Extensions.kt\ncom/squareup/mosaic/components/ExtensionsKt\n*L\n8#1,7:19\n8#1,4:26\n14#1,8:30\n14#1,4:38\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a \u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u00020\u0004\u001a \u0010\u0006\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u00020\u0004\u00a8\u0006\u0007"
    }
    d2 = {
        "horizontalHairline",
        "",
        "P",
        "",
        "Lcom/squareup/blueprint/BlueprintContext;",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
        "verticalHairline",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final horizontalHairline(Lcom/squareup/blueprint/BlueprintContext;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "TP;>;)V"
        }
    .end annotation

    const-string v0, "$this$horizontalHairline"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 26
    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 22
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/blueprint/mosaic/MosaicBlock;-><init>(Ljava/lang/Object;ZZ)V

    .line 9
    move-object v1, v0

    check-cast v1, Lcom/squareup/mosaic/core/UiModelContext;

    invoke-static {v1}, Lcom/squareup/mosaic/components/HairlineUiModelKt;->horizontalHairline(Lcom/squareup/mosaic/core/UiModelContext;)V

    .line 10
    check-cast v0, Lcom/squareup/blueprint/Block;

    .line 22
    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public static final verticalHairline(Lcom/squareup/blueprint/BlueprintContext;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "TP;>;)V"
        }
    .end annotation

    const-string v0, "$this$verticalHairline"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 38
    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 34
    invoke-direct {v0, v1, v3, v2}, Lcom/squareup/blueprint/mosaic/MosaicBlock;-><init>(Ljava/lang/Object;ZZ)V

    .line 15
    move-object v1, v0

    check-cast v1, Lcom/squareup/mosaic/core/UiModelContext;

    invoke-static {v1}, Lcom/squareup/mosaic/components/HairlineUiModelKt;->verticalHairline(Lcom/squareup/mosaic/core/UiModelContext;)V

    .line 16
    check-cast v0, Lcom/squareup/blueprint/Block;

    .line 34
    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method
