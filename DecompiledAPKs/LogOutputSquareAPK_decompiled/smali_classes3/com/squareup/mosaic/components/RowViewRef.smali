.class public final Lcom/squareup/mosaic/components/RowViewRef;
.super Lcom/squareup/mosaic/core/StandardViewRef;
.source "RowViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/core/StandardViewRef<",
        "Lcom/squareup/mosaic/components/RowUiModel<",
        "*>;",
        "Lcom/squareup/noho/NohoRow;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRowViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RowViewRef.kt\ncom/squareup/mosaic/components/RowViewRef\n+ 2 ViewRefUtils.kt\ncom/squareup/mosaic/components/ViewRefUtilsKt\n*L\n1#1,57:1\n15#2,2:58\n15#2,2:60\n15#2,2:62\n15#2,2:64\n15#2,2:66\n*E\n*S KotlinDebug\n*F\n+ 1 RowViewRef.kt\ncom/squareup/mosaic/components/RowViewRef\n*L\n28#1,2:58\n28#1,2:60\n28#1,2:62\n28#1,2:64\n28#1,2:66\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J \u0010\u000b\u001a\u00020\u00082\n\u0010\u000c\u001a\u0006\u0012\u0002\u0008\u00030\u00022\n\u0010\r\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0014J\u001c\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\u0010\u000f\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\"\u0010\u0010\u001a\u00020\u00112\u000c\u0010\u0012\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00022\n\u0010\r\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016R\u001c\u0010\u0007\u001a\u00020\u0008*\u0006\u0012\u0002\u0008\u00030\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/RowViewRef;",
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "Lcom/squareup/mosaic/components/RowUiModel;",
        "Lcom/squareup/noho/NohoRow;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "hasCheck",
        "",
        "getHasCheck",
        "(Lcom/squareup/mosaic/components/RowUiModel;)Z",
        "canUpdateTo",
        "currentModel",
        "newModel",
        "createView",
        "model",
        "doBind",
        "",
        "oldModel",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/core/StandardViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public static final synthetic access$isBinding$p(Lcom/squareup/mosaic/components/RowViewRef;)Z
    .locals 0

    .line 11
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/RowViewRef;->isBinding()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$setBinding$p(Lcom/squareup/mosaic/components/RowViewRef;Z)V
    .locals 0

    .line 11
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/components/RowViewRef;->setBinding(Z)V

    return-void
.end method

.method private final getHasCheck(Lcom/squareup/mosaic/components/RowUiModel;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/RowUiModel<",
            "*>;)Z"
        }
    .end annotation

    .line 13
    invoke-virtual {p1}, Lcom/squareup/mosaic/components/RowUiModel;->getCheckType()Lcom/squareup/noho/CheckType;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method protected canUpdateTo(Lcom/squareup/mosaic/components/RowUiModel;Lcom/squareup/mosaic/components/RowUiModel;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/RowUiModel<",
            "*>;",
            "Lcom/squareup/mosaic/components/RowUiModel<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "currentModel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    move-object v0, p1

    check-cast v0, Lcom/squareup/mosaic/core/StandardUiModel;

    move-object v1, p2

    check-cast v1, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-super {p0, v0, v1}, Lcom/squareup/mosaic/core/StandardViewRef;->canUpdateTo(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/mosaic/components/RowViewRef;->getHasCheck(Lcom/squareup/mosaic/components/RowUiModel;)Z

    move-result p1

    invoke-direct {p0, p2}, Lcom/squareup/mosaic/components/RowViewRef;->getHasCheck(Lcom/squareup/mosaic/components/RowUiModel;)Z

    move-result p2

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic canUpdateTo(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)Z
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/mosaic/components/RowUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/RowUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/RowViewRef;->canUpdateTo(Lcom/squareup/mosaic/components/RowUiModel;Lcom/squareup/mosaic/components/RowUiModel;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic canUpdateTo(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)Z
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/mosaic/components/RowUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/RowUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/RowViewRef;->canUpdateTo(Lcom/squareup/mosaic/components/RowUiModel;Lcom/squareup/mosaic/components/RowUiModel;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .locals 0

    .line 11
    check-cast p2, Lcom/squareup/mosaic/components/RowUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/RowViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/components/RowUiModel;)Lcom/squareup/noho/NohoRow;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public createView(Landroid/content/Context;Lcom/squareup/mosaic/components/RowUiModel;)Lcom/squareup/noho/NohoRow;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/mosaic/components/RowUiModel<",
            "*>;)",
            "Lcom/squareup/noho/NohoRow;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0, p2}, Lcom/squareup/mosaic/components/RowViewRef;->getHasCheck(Lcom/squareup/mosaic/components/RowUiModel;)Z

    move-result p2

    if-eqz p2, :cond_0

    new-instance p2, Lcom/squareup/noho/NohoCheckableRow;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p2

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoCheckableRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p2, Lcom/squareup/noho/NohoRow;

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/squareup/noho/NohoRow;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p2

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    return-object p2
.end method

.method public doBind(Lcom/squareup/mosaic/components/RowUiModel;Lcom/squareup/mosaic/components/RowUiModel;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/RowUiModel<",
            "*>;",
            "Lcom/squareup/mosaic/components/RowUiModel<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    move-object v0, p1

    check-cast v0, Lcom/squareup/mosaic/core/StandardUiModel;

    move-object v1, p2

    check-cast v1, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-super {p0, v0, v1}, Lcom/squareup/mosaic/core/StandardViewRef;->doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V

    .line 28
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/RowViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 30
    invoke-virtual {p1}, Lcom/squareup/mosaic/components/RowUiModel;->getIcon()Lcom/squareup/mosaic/components/RowUiModel$IconModel;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/RowUiModel;->getIcon()Lcom/squareup/mosaic/components/RowUiModel$IconModel;

    move-result-object v3

    .line 58
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    .line 32
    instance-of v2, v3, Lcom/squareup/mosaic/components/RowUiModel$IconModel$SimpleIcon;

    if-eqz v2, :cond_1

    new-instance v2, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;

    check-cast v3, Lcom/squareup/mosaic/components/RowUiModel$IconModel$SimpleIcon;

    invoke-virtual {v3}, Lcom/squareup/mosaic/components/RowUiModel$IconModel$SimpleIcon;->getDrawableId()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;-><init>(I)V

    check-cast v2, Lcom/squareup/noho/NohoRow$Icon;

    goto :goto_1

    .line 33
    :cond_1
    instance-of v2, v3, Lcom/squareup/mosaic/components/RowUiModel$IconModel$SimpleText;

    if-eqz v2, :cond_2

    new-instance v2, Lcom/squareup/noho/NohoRow$Icon$TextIcon;

    check-cast v3, Lcom/squareup/mosaic/components/RowUiModel$IconModel$SimpleText;

    invoke-virtual {v3}, Lcom/squareup/mosaic/components/RowUiModel$IconModel$SimpleText;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/squareup/mosaic/components/RowUiModel$IconModel$SimpleText;->getColor()I

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, v2

    invoke-direct/range {v4 .. v9}, Lcom/squareup/noho/NohoRow$Icon$TextIcon;-><init>(Ljava/lang/String;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v2, Lcom/squareup/noho/NohoRow$Icon;

    goto :goto_1

    :cond_2
    move-object v2, v1

    .line 31
    :goto_1
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    :cond_3
    if-eqz p1, :cond_4

    .line 37
    invoke-virtual {p1}, Lcom/squareup/mosaic/components/RowUiModel;->getLabel()Lcom/squareup/resources/TextModel;

    move-result-object v2

    goto :goto_2

    :cond_4
    move-object v2, v1

    :goto_2
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/RowUiModel;->getLabel()Lcom/squareup/resources/TextModel;

    move-result-object v3

    .line 60
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    const-string v4, "context"

    if-eqz v2, :cond_6

    if-eqz v3, :cond_5

    .line 37
    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v2}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_3

    :cond_5
    move-object v2, v1

    :goto_3
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    :cond_6
    if-eqz p1, :cond_7

    .line 38
    invoke-virtual {p1}, Lcom/squareup/mosaic/components/RowUiModel;->getValue()Lcom/squareup/resources/TextModel;

    move-result-object v2

    goto :goto_4

    :cond_7
    move-object v2, v1

    :goto_4
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/RowUiModel;->getValue()Lcom/squareup/resources/TextModel;

    move-result-object v3

    .line 62
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_9

    if-eqz v3, :cond_8

    .line 38
    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v2}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_5

    :cond_8
    move-object v2, v1

    :goto_5
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    :cond_9
    if-eqz p1, :cond_a

    .line 39
    invoke-virtual {p1}, Lcom/squareup/mosaic/components/RowUiModel;->getDescription()Lcom/squareup/resources/TextModel;

    move-result-object v2

    goto :goto_6

    :cond_a
    move-object v2, v1

    :goto_6
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/RowUiModel;->getDescription()Lcom/squareup/resources/TextModel;

    move-result-object v3

    .line 64
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_c

    if-eqz v3, :cond_b

    .line 39
    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v2}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_7

    :cond_b
    move-object v2, v1

    :goto_7
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 40
    :cond_c
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/RowUiModel;->getAccessory()Lcom/squareup/noho/AccessoryType;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setAccessory(Lcom/squareup/noho/AccessoryType;)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/RowViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Lcom/squareup/noho/NohoCheckableRow;

    if-nez v3, :cond_d

    move-object v2, v1

    :cond_d
    check-cast v2, Lcom/squareup/noho/NohoCheckableRow;

    if-eqz v2, :cond_11

    .line 44
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/RowUiModel;->getCheckType()Lcom/squareup/noho/CheckType;

    move-result-object v0

    if-nez v0, :cond_e

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_e
    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoCheckableRow;->setType(Lcom/squareup/noho/CheckType;)V

    .line 45
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/RowUiModel;->isChecked()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    if-eqz p1, :cond_f

    .line 46
    invoke-virtual {p1}, Lcom/squareup/mosaic/components/RowUiModel;->getOnChechedChange()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    goto :goto_8

    :cond_f
    move-object v0, v1

    :goto_8
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/RowUiModel;->getOnChechedChange()Lkotlin/jvm/functions/Function1;

    move-result-object v4

    .line 66
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_13

    if-eqz v4, :cond_10

    .line 50
    new-instance v0, Lcom/squareup/mosaic/components/RowViewRef$doBind$$inlined$with$lambda$1;

    move-object v3, v0

    move-object v5, v2

    move-object v6, p0

    move-object v7, p1

    move-object v8, p2

    invoke-direct/range {v3 .. v8}, Lcom/squareup/mosaic/components/RowViewRef$doBind$$inlined$with$lambda$1;-><init>(Lkotlin/jvm/functions/Function1;Lcom/squareup/noho/NohoCheckableRow;Lcom/squareup/mosaic/components/RowViewRef;Lcom/squareup/mosaic/components/RowUiModel;Lcom/squareup/mosaic/components/RowUiModel;)V

    move-object v1, v0

    check-cast v1, Lkotlin/jvm/functions/Function2;

    .line 47
    :cond_10
    invoke-virtual {v2, v1}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    goto :goto_9

    .line 53
    :cond_11
    check-cast v0, Landroid/view/View;

    if-eqz p1, :cond_12

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/RowUiModel;->getOnClick()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    :cond_12
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/RowUiModel;->getOnClick()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/mosaic/components/ViewRefUtilsKt;->setOnClickDebouncedIfChanged(Landroid/view/View;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    :cond_13
    :goto_9
    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/mosaic/components/RowUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/RowUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/RowViewRef;->doBind(Lcom/squareup/mosaic/components/RowUiModel;Lcom/squareup/mosaic/components/RowUiModel;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/mosaic/components/RowUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/RowUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/RowViewRef;->doBind(Lcom/squareup/mosaic/components/RowUiModel;Lcom/squareup/mosaic/components/RowUiModel;)V

    return-void
.end method
