.class public final Lcom/squareup/mosaic/components/ViewRefUtilsKt;
.super Ljava/lang/Object;
.source "ViewRefUtils.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViewRefUtils.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ViewRefUtils.kt\ncom/squareup/mosaic/components/ViewRefUtilsKt\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,17:1\n15#1:18\n16#1:26\n1103#2,7:19\n*E\n*S KotlinDebug\n*F\n+ 1 ViewRefUtils.kt\ncom/squareup/mosaic/components/ViewRefUtilsKt\n*L\n9#1:18\n9#1:26\n9#1,7:19\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a:\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u00022\u0008\u0010\u0003\u001a\u0004\u0018\u0001H\u00022\u0006\u0010\u0004\u001a\u0002H\u00022\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00010\u0006H\u0086\u0008\u00a2\u0006\u0002\u0010\u0007\u001a6\u0010\u0008\u001a\u00020\u0001*\u00020\t2\u0014\u0010\n\u001a\u0010\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u000bj\u0004\u0018\u0001`\u000c2\u0014\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u000bj\u0004\u0018\u0001`\u000c*\u0016\u0010\u000e\"\u0008\u0012\u0004\u0012\u00020\u00010\u000b2\u0008\u0012\u0004\u0012\u00020\u00010\u000b\u00a8\u0006\u000f"
    }
    d2 = {
        "ifChanged",
        "",
        "T",
        "oldValue",
        "newValue",
        "block",
        "Lkotlin/Function1;",
        "(Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V",
        "setOnClickDebouncedIfChanged",
        "Landroid/view/View;",
        "oldHandler",
        "Lkotlin/Function0;",
        "Lcom/squareup/mosaic/components/Handler;",
        "newHandler",
        "Handler",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final ifChanged(Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    if-eqz p0, :cond_0

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public static final setOnClickDebouncedIfChanged(Landroid/view/View;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$setOnClickDebouncedIfChanged"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    .line 19
    new-instance p1, Lcom/squareup/mosaic/components/ViewRefUtilsKt$$special$$inlined$onClickDebounced$1;

    invoke-direct {p1, p2}, Lcom/squareup/mosaic/components/ViewRefUtilsKt$$special$$inlined$onClickDebounced$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {p0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 10
    invoke-virtual {p0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    :goto_0
    return-void
.end method
