.class public final Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;
.super Ljava/lang/Object;
.source "VerticalDividersStackUiModel.kt"

# interfaces
.implements Lcom/squareup/mosaic/lists/IdParams;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Params"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001a\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\u0011\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0008J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001\u00f8\u0001\u0000\u00a2\u0006\u0004\u0008\u0011\u0010\u0012J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u001f\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00f8\u0001\u0000\u00a2\u0006\u0010\n\u0002\u0010\u000b\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\nR\u001a\u0010\u0002\u001a\u00020\u0003X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\u0008\"\u0004\u0008\r\u0010\n\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
        "Lcom/squareup/mosaic/lists/IdParams;",
        "itemId",
        "",
        "edges",
        "Lcom/squareup/mosaic/components/DividerEdge;",
        "(IILkotlin/jvm/internal/DefaultConstructorMarker;)V",
        "getEdges",
        "()I",
        "setEdges-mqrhnls",
        "(I)V",
        "I",
        "getItemId",
        "setItemId",
        "component1",
        "component2",
        "copy",
        "copy-m4i1CII",
        "(II)Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private edges:I

.field private itemId:I


# direct methods
.method private constructor <init>(II)V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->itemId:I

    iput p2, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->edges:I

    return-void
.end method

.method public synthetic constructor <init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const/4 p1, 0x0

    .line 47
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;-><init>(II)V

    return-void
.end method

.method public synthetic constructor <init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;-><init>(II)V

    return-void
.end method

.method public static synthetic copy-m4i1CII$default(Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;IIILjava/lang/Object;)Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->getItemId()I

    move-result p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->edges:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->copy-m4i1CII(II)Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->getItemId()I

    move-result v0

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->edges:I

    return v0
.end method

.method public final copy-m4i1CII(II)Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;
    .locals 1

    new-instance v0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;

    invoke-direct {v0, p1, p2}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;-><init>(II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->getItemId()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->getItemId()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->edges:I

    iget p1, p1, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->edges:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getEdges()I
    .locals 1

    .line 48
    iget v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->edges:I

    return v0
.end method

.method public getItemId()I
    .locals 1

    .line 47
    iget v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->itemId:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->getItemId()I

    move-result v0

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->edges:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final setEdges-mqrhnls(I)V
    .locals 0

    .line 48
    iput p1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->edges:I

    return-void
.end method

.method public setItemId(I)V
    .locals 0

    .line 47
    iput p1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->itemId:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Params(itemId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", edges="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->edges:I

    invoke-static {v1}, Lcom/squareup/mosaic/components/DividerEdge;->toString-impl(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
