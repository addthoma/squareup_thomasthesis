.class public final Lcom/squareup/mosaic/components/EditViewRef$createView$$inlined$apply$lambda$1;
.super Ljava/lang/Object;
.source "EditViewRef.kt"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mosaic/components/EditViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/components/EditUiModel;)Lcom/squareup/noho/NohoEditRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000?\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J*\u0010\u0006\u001a\u00020\u00032\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\nH\u0016J*\u0010\r\u001a\u00020\u00032\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0016\u00a8\u0006\u0010\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/mosaic/components/EditViewRef$createView$1$1",
        "Landroid/text/TextWatcher;",
        "afterTextChanged",
        "",
        "text",
        "Landroid/text/Editable;",
        "beforeTextChanged",
        "charSequence",
        "",
        "start",
        "",
        "count",
        "after",
        "onTextChanged",
        "p0",
        "before",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/mosaic/components/EditViewRef;


# direct methods
.method constructor <init>(Lcom/squareup/mosaic/components/EditViewRef;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mosaic/components/EditViewRef$createView$$inlined$apply$lambda$1;->this$0:Lcom/squareup/mosaic/components/EditViewRef;

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lcom/squareup/mosaic/components/EditViewRef$createView$$inlined$apply$lambda$1;->this$0:Lcom/squareup/mosaic/components/EditViewRef;

    invoke-static {v0}, Lcom/squareup/mosaic/components/EditViewRef;->access$isBinding$p(Lcom/squareup/mosaic/components/EditViewRef;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/squareup/mosaic/components/EditViewRef$createView$$inlined$apply$lambda$1;->this$0:Lcom/squareup/mosaic/components/EditViewRef;

    invoke-virtual {v0}, Lcom/squareup/mosaic/components/EditViewRef;->getModel()Lcom/squareup/mosaic/core/UiModel;

    move-result-object v0

    check-cast v0, Lcom/squareup/mosaic/components/EditUiModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/mosaic/components/EditUiModel;->getOnChange()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
