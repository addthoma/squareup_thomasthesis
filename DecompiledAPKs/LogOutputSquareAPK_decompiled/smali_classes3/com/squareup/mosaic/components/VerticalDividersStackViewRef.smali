.class public final Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;
.super Lcom/squareup/mosaic/core/StandardViewRef;
.source "VerticalDividersStackViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$FixedLayoutParams;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/core/StandardViewRef<",
        "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel<",
        "*>;",
        "Lcom/squareup/noho/NohoLinearLayout;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nVerticalDividersStackViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 VerticalDividersStackViewRef.kt\ncom/squareup/mosaic/components/VerticalDividersStackViewRef\n*L\n1#1,109:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Q\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0011\u0008\u0000\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0001\u001eB\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\u0010\u0014\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\"\u0010\u0015\u001a\u00020\u00162\u000c\u0010\u0017\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00022\n\u0010\u0018\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\u0010\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0008\u0010\u001c\u001a\u00020\u001dH\u0016R\"\u0010\u0007\u001a\u0010\u0012\u000c\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\t0\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR \u0010\u000c\u001a\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u0004\u0012\u00020\u000f0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0012\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;",
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;",
        "Lcom/squareup/noho/NohoLinearLayout;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "children",
        "Lkotlin/sequences/Sequence;",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "getChildren",
        "()Lkotlin/sequences/Sequence;",
        "viewList",
        "Lcom/squareup/mosaic/lists/ModelsList;",
        "Lcom/squareup/mosaic/core/UiModel;",
        "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
        "viewListChanges",
        "com/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1",
        "Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;",
        "createView",
        "model",
        "doBind",
        "",
        "oldModel",
        "newModel",
        "restoreInstanceState",
        "parcelable",
        "Landroid/os/Parcelable;",
        "saveInstanceState",
        "Lcom/squareup/mosaic/lists/ModelsList$State;",
        "FixedLayoutParams",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final viewList:Lcom/squareup/mosaic/lists/ModelsList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/lists/ModelsList<",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
            ">;",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
            ">;"
        }
    .end annotation
.end field

.field private final viewListChanges:Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/core/StandardViewRef;-><init>(Landroid/content/Context;)V

    .line 43
    new-instance v0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;

    invoke-direct {v0, p0}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;-><init>(Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;)V

    iput-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->viewListChanges:Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;

    .line 85
    new-instance v0, Lcom/squareup/mosaic/lists/ModelsList;

    iget-object v1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->viewListChanges:Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;

    check-cast v1, Lcom/squareup/mosaic/lists/ModelsList$Changes;

    invoke-direct {v0, p1, v1}, Lcom/squareup/mosaic/lists/ModelsList;-><init>(Landroid/content/Context;Lcom/squareup/mosaic/lists/ModelsList$Changes;)V

    iput-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    return-void
.end method


# virtual methods
.method public bridge synthetic createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .locals 0

    .line 20
    check-cast p2, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;)Lcom/squareup/noho/NohoLinearLayout;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public createView(Landroid/content/Context;Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;)Lcom/squareup/noho/NohoLinearLayout;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel<",
            "*>;)",
            "Lcom/squareup/noho/NohoLinearLayout;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance p2, Lcom/squareup/noho/NohoLinearLayout;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lcom/squareup/noho/NohoLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x1

    .line 29
    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoLinearLayout;->setOrientation(I)V

    return-object p2
.end method

.method public doBind(Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel<",
            "*>;",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    check-cast p1, Lcom/squareup/mosaic/core/StandardUiModel;

    move-object v0, p2

    check-cast v0, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-super {p0, p1, v0}, Lcom/squareup/mosaic/core/StandardViewRef;->doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V

    .line 93
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLinearLayout;

    .line 95
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->getHorizontalPadding()Lcom/squareup/resources/DimenModel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/noho/NohoLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/resources/DimenModel;->toOffset(Landroid/content/Context;)I

    move-result v0

    .line 96
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->getVerticalPadding()Lcom/squareup/resources/DimenModel;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/noho/NohoLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Lcom/squareup/resources/DimenModel;->toOffset(Landroid/content/Context;)I

    move-result v1

    .line 97
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->getHorizontalPadding()Lcom/squareup/resources/DimenModel;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/noho/NohoLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Lcom/squareup/resources/DimenModel;->toOffset(Landroid/content/Context;)I

    move-result v3

    .line 98
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->getVerticalPadding()Lcom/squareup/resources/DimenModel;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/noho/NohoLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Lcom/squareup/resources/DimenModel;->toOffset(Landroid/content/Context;)I

    move-result v2

    .line 94
    invoke-virtual {p1, v0, v1, v3, v2}, Lcom/squareup/noho/NohoLinearLayout;->setPadding(IIII)V

    .line 102
    iget-object p1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {p2}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->getSubModels()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/mosaic/lists/ModelsList;->bind(Ljava/util/List;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->doBind(Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->doBind(Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;)V

    return-void
.end method

.method public getChildren()Lkotlin/sequences/Sequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/sequences/Sequence<",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;>;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {v0}, Lcom/squareup/mosaic/lists/ModelsList;->getSubViews()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    return-object v0
.end method

.method public restoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    const-string v0, "parcelable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {v0, p1}, Lcom/squareup/mosaic/lists/ModelsList;->restoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method public bridge synthetic saveInstanceState()Landroid/os/Parcelable;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->saveInstanceState()Lcom/squareup/mosaic/lists/ModelsList$State;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method public saveInstanceState()Lcom/squareup/mosaic/lists/ModelsList$State;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {v0}, Lcom/squareup/mosaic/lists/ModelsList;->saveInstanceState()Lcom/squareup/mosaic/lists/ModelsList$State;

    move-result-object v0

    return-object v0
.end method
