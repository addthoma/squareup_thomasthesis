.class public final Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;
.super Lcom/squareup/mosaic/core/StandardUiModel;
.source "VerticalDividersStackUiModel.kt"

# interfaces
.implements Lcom/squareup/mosaic/core/UiModelContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/mosaic/core/StandardUiModel<",
        "Lcom/squareup/noho/NohoLinearLayout;",
        "TP;>;",
        "Lcom/squareup/mosaic/core/UiModelContext<",
        "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nVerticalDividersStackUiModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 VerticalDividersStackUiModel.kt\ncom/squareup/mosaic/components/VerticalDividersStackUiModel\n+ 2 UiModel.kt\ncom/squareup/mosaic/core/UiModel\n*L\n1#1,101:1\n53#2:102\n*E\n*S KotlinDebug\n*F\n+ 1 VerticalDividersStackUiModel.kt\ncom/squareup/mosaic/components/VerticalDividersStackUiModel\n*L\n64#1:102\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u000c\n\u0002\u0010\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00010\u00032\u0008\u0012\u0004\u0012\u00020\u00060\u0005:\u00017B7\u0012\u0006\u0010\u0007\u001a\u00028\u0000\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\t\u0012\u0014\u0008\u0003\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\r0\u000c\u00a2\u0006\u0002\u0010\u000eJ\u0016\u0010\u001f\u001a\u00020 2\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00060\rH\u0016J\u000e\u0010\"\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0019J\t\u0010#\u001a\u00020\tH\u00c6\u0003J\t\u0010$\u001a\u00020\tH\u00c6\u0003J\u0015\u0010%\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\r0\u000cH\u00c6\u0003JH\u0010&\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0007\u001a\u00028\u00002\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\t2\u0014\u0008\u0003\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\r0\u000cH\u00c6\u0001\u00a2\u0006\u0002\u0010\'J\u0008\u0010(\u001a\u00020\u0006H\u0016J\u0018\u0010)\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030*2\u0006\u0010+\u001a\u00020,H\u0016J\u0018\u0010-\u001a\u00020 2\u0006\u0010.\u001a\u00020\u0010\u00f8\u0001\u0000\u00a2\u0006\u0004\u0008/\u00100J\u0013\u00101\u001a\u00020\u00132\u0008\u00102\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u00103\u001a\u000204H\u00d6\u0001J\t\u00105\u001a\u000206H\u00d6\u0001R\u0013\u0010\u000f\u001a\u00020\u0010X\u0082\u000e\u00f8\u0001\u0000\u00a2\u0006\u0004\n\u0002\u0010\u0011R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\"\u0004\u0008\u0016\u0010\u0017R\u0016\u0010\u0007\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u001a\u001a\u0004\u0008\u0018\u0010\u0019R\u001d\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\r0\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u001a\u0010\u0008\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001d\u0010\u0015\"\u0004\u0008\u001e\u0010\u0017\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u00068"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;",
        "P",
        "",
        "Lcom/squareup/mosaic/core/StandardUiModel;",
        "Lcom/squareup/noho/NohoLinearLayout;",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
        "params",
        "verticalPadding",
        "Lcom/squareup/resources/DimenModel;",
        "horizontalPadding",
        "subModels",
        "",
        "Lcom/squareup/mosaic/core/UiModel;",
        "(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Ljava/util/List;)V",
        "currentEdges",
        "Lcom/squareup/mosaic/components/DividerEdge;",
        "I",
        "hasEdgeBefore",
        "",
        "getHorizontalPadding",
        "()Lcom/squareup/resources/DimenModel;",
        "setHorizontalPadding",
        "(Lcom/squareup/resources/DimenModel;)V",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "getSubModels",
        "()Ljava/util/List;",
        "getVerticalPadding",
        "setVerticalPadding",
        "add",
        "",
        "model",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Ljava/util/List;)Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;",
        "createParams",
        "createViewRef",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "context",
        "Landroid/content/Context;",
        "edges",
        "value",
        "edges-mqrhnls",
        "(I)V",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Params",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private currentEdges:I

.field private hasEdgeBefore:Z

.field private horizontalPadding:Lcom/squareup/resources/DimenModel;

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private final subModels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
            ">;>;"
        }
    .end annotation
.end field

.field private verticalPadding:Lcom/squareup/resources/DimenModel;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/resources/DimenModel;",
            "Ljava/util/List<",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "verticalPadding"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "horizontalPadding"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subModels"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/squareup/mosaic/core/StandardUiModel;-><init>()V

    iput-object p1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->params:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->verticalPadding:Lcom/squareup/resources/DimenModel;

    iput-object p3, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->horizontalPadding:Lcom/squareup/resources/DimenModel;

    iput-object p4, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->subModels:Ljava/util/List;

    .line 53
    sget-object p1, Lcom/squareup/mosaic/components/DividerEdge;->Companion:Lcom/squareup/mosaic/components/DividerEdge$Companion;

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/DividerEdge$Companion;->getNONE()I

    move-result p1

    iput p1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->currentEdges:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    .line 39
    invoke-static {v0}, Lcom/squareup/resources/DimenModelsKt;->getDp(I)Lcom/squareup/resources/FixedDimen;

    move-result-object p2

    check-cast p2, Lcom/squareup/resources/DimenModel;

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    .line 40
    invoke-static {v0}, Lcom/squareup/resources/DimenModelsKt;->getDp(I)Lcom/squareup/resources/FixedDimen;

    move-result-object p3

    check-cast p3, Lcom/squareup/resources/DimenModel;

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    .line 42
    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    check-cast p4, Ljava/util/List;

    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;-><init>(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->verticalPadding:Lcom/squareup/resources/DimenModel;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->horizontalPadding:Lcom/squareup/resources/DimenModel;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->subModels:Ljava/util/List;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->copy(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Ljava/util/List;)Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public add(Lcom/squareup/mosaic/core/UiModel;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
            ">;)V"
        }
    .end annotation

    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p1}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;

    .line 65
    iget-boolean v1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->hasEdgeBefore:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->getEdges()I

    move-result v1

    sget-object v2, Lcom/squareup/mosaic/components/DividerEdge;->Companion:Lcom/squareup/mosaic/components/DividerEdge$Companion;

    invoke-virtual {v2}, Lcom/squareup/mosaic/components/DividerEdge$Companion;->getTOP()I

    move-result v2

    invoke-static {v1, v2}, Lcom/squareup/mosaic/components/DividerEdge;->minus-mqrhnls(II)I

    move-result v1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->getEdges()I

    move-result v1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->setEdges-mqrhnls(I)V

    .line 66
    invoke-virtual {v0}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->getEdges()I

    move-result v0

    sget-object v1, Lcom/squareup/mosaic/components/DividerEdge;->Companion:Lcom/squareup/mosaic/components/DividerEdge$Companion;

    invoke-virtual {v1}, Lcom/squareup/mosaic/components/DividerEdge$Companion;->getBOTTOM()I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/mosaic/components/DividerEdge;->contains-mqrhnls(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->hasEdgeBefore:Z

    .line 68
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->subModels:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/resources/DimenModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->verticalPadding:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final component3()Lcom/squareup/resources/DimenModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->horizontalPadding:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->subModels:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Ljava/util/List;)Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lcom/squareup/resources/DimenModel;",
            "Lcom/squareup/resources/DimenModel;",
            "Ljava/util/List<",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
            ">;>;)",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel<",
            "TP;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "verticalPadding"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "horizontalPadding"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subModels"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;-><init>(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Ljava/util/List;)V

    return-object v0
.end method

.method public createParams()Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;
    .locals 5

    .line 60
    new-instance v0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;

    iget v1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->currentEdges:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public bridge synthetic createParams()Ljava/lang/Object;
    .locals 1

    .line 37
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->createParams()Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;

    move-result-object v0

    return-object v0
.end method

.method public createViewRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    new-instance v0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/squareup/mosaic/core/ViewRef;

    return-object v0
.end method

.method public final edges-mqrhnls(I)V
    .locals 0

    .line 57
    iput p1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->currentEdges:I

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->verticalPadding:Lcom/squareup/resources/DimenModel;

    iget-object v1, p1, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->verticalPadding:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->horizontalPadding:Lcom/squareup/resources/DimenModel;

    iget-object v1, p1, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->horizontalPadding:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->subModels:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->subModels:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHorizontalPadding()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->horizontalPadding:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public final getSubModels()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
            ">;>;"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->subModels:Ljava/util/List;

    return-object v0
.end method

.method public final getVerticalPadding()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->verticalPadding:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->verticalPadding:Lcom/squareup/resources/DimenModel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->horizontalPadding:Lcom/squareup/resources/DimenModel;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->subModels:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final setHorizontalPadding(Lcom/squareup/resources/DimenModel;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iput-object p1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->horizontalPadding:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public final setVerticalPadding(Lcom/squareup/resources/DimenModel;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iput-object p1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->verticalPadding:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VerticalDividersStackUiModel(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", verticalPadding="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->verticalPadding:Lcom/squareup/resources/DimenModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", horizontalPadding="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->horizontalPadding:Lcom/squareup/resources/DimenModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subModels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel;->subModels:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
