.class public final Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;
.super Ljava/lang/Object;
.source "VerticalDividersStackViewRef.kt"

# interfaces
.implements Lcom/squareup/mosaic/lists/ModelsList$Changes;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/mosaic/lists/ModelsList$Changes<",
        "Lcom/squareup/mosaic/core/UiModel<",
        "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nVerticalDividersStackViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 VerticalDividersStackViewRef.kt\ncom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n*L\n1#1,109:1\n1651#2,3:110\n1104#3,2:113\n*E\n*S KotlinDebug\n*F\n+ 1 VerticalDividersStackViewRef.kt\ncom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1\n*L\n62#1,3:110\n74#1,2:113\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00009\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u0001J\u001e\u0010\u0004\u001a\u00020\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0007\u001a\u00020\u0008H\u0002J$\u0010\t\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u000b2\u0012\u0010\u000c\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\rH\u0016J\u0008\u0010\u000e\u001a\u00020\u0005H\u0016J2\u0010\u000f\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u000b2\u0012\u0010\u000c\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\r2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0011H\u0016J\u0018\u0010\u0012\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\u000bH\u0016J4\u0010\u0014\u001a\u00020\u00052\u0006\u0010\u0015\u001a\u00020\u000b2\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0017\u001a\u00020\u000b2\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\u0016\u00a8\u0006\u0019"
    }
    d2 = {
        "com/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1",
        "Lcom/squareup/mosaic/lists/ModelsList$Changes;",
        "Lcom/squareup/mosaic/core/UiModel;",
        "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
        "configure",
        "",
        "item",
        "androidView",
        "Landroid/view/View;",
        "onChanged",
        "position",
        "",
        "newItems",
        "",
        "onCleared",
        "onInserted",
        "newAndroidViews",
        "Lkotlin/sequences/Sequence;",
        "onRemoved",
        "count",
        "onUnchanged",
        "oldPosition",
        "oldItem",
        "newPosition",
        "newItem",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;


# direct methods
.method constructor <init>(Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 43
    iput-object p1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;->this$0:Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final configure(Lcom/squareup/mosaic/core/UiModel;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 58
    new-instance v0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$FixedLayoutParams;

    invoke-virtual {p1}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;->getEdges()I

    move-result p1

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$FixedLayoutParams;-><init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public onChanged(ILjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "newItems"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    check-cast p2, Ljava/lang/Iterable;

    .line 111
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v0, 0x1

    if-gez v0, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v1, Lcom/squareup/mosaic/core/UiModel;

    .line 63
    iget-object v3, p0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;->this$0:Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;

    invoke-virtual {v3}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/noho/NohoLinearLayout;

    add-int/2addr v0, p1

    invoke-virtual {v3, v0}, Lcom/squareup/noho/NohoLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const-string v3, "androidView"

    .line 64
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;->configure(Lcom/squareup/mosaic/core/UiModel;Landroid/view/View;)V

    move v0, v2

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onCleared()V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;->this$0:Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;

    invoke-virtual {v0}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLinearLayout;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoLinearLayout;->removeAllViews()V

    return-void
.end method

.method public onInserted(ILjava/util/List;Lkotlin/sequences/Sequence;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
            ">;>;",
            "Lkotlin/sequences/Sequence<",
            "+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newItems"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newAndroidViews"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    check-cast p2, Ljava/lang/Iterable;

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p2

    invoke-static {p2, p3}, Lkotlin/sequences/SequencesKt;->zip(Lkotlin/sequences/Sequence;Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object p2

    .line 113
    invoke-interface {p2}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lkotlin/Pair;

    invoke-virtual {p3}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mosaic/core/UiModel;

    invoke-virtual {p3}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/view/View;

    .line 75
    invoke-direct {p0, v0, p3}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;->configure(Lcom/squareup/mosaic/core/UiModel;Landroid/view/View;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;->this$0:Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;

    invoke-virtual {v0}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLinearLayout;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, p3, p1}, Lcom/squareup/noho/NohoLinearLayout;->addView(Landroid/view/View;I)V

    move p1, v1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onRemoved(II)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;->this$0:Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;

    invoke-virtual {v0}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLinearLayout;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/noho/NohoLinearLayout;->removeViews(II)V

    return-void
.end method

.method public onUnchanged(ILcom/squareup/mosaic/core/UiModel;ILcom/squareup/mosaic/core/UiModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
            ">;I",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/mosaic/components/VerticalDividersStackUiModel$Params;",
            ">;)V"
        }
    .end annotation

    const-string p1, "oldItem"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "newItem"

    invoke-static {p4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onUnchanged(ILjava/lang/Object;ILjava/lang/Object;)V
    .locals 0

    .line 43
    check-cast p2, Lcom/squareup/mosaic/core/UiModel;

    check-cast p4, Lcom/squareup/mosaic/core/UiModel;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$viewListChanges$1;->onUnchanged(ILcom/squareup/mosaic/core/UiModel;ILcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method
