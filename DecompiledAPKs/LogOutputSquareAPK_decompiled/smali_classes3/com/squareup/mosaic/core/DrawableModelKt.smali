.class public final Lcom/squareup/mosaic/core/DrawableModelKt;
.super Ljava/lang/Object;
.source "DrawableModel.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDrawableModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DrawableModel.kt\ncom/squareup/mosaic/core/DrawableModelKt\n*L\n1#1,13:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a+\u0010\u0000\u001a\u000c\u0012\u0004\u0012\u0002H\u0002\u0012\u0002\u0008\u00030\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0002H\u00022\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "toDrawableRef",
        "Lcom/squareup/mosaic/core/DrawableRef;",
        "M",
        "Lcom/squareup/mosaic/core/DrawableModel;",
        "context",
        "Landroid/content/Context;",
        "(Lcom/squareup/mosaic/core/DrawableModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/DrawableRef;",
        "mosaic-core_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toDrawableRef(Lcom/squareup/mosaic/core/DrawableModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/DrawableRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/squareup/mosaic/core/DrawableModel;",
            ">(TM;",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/DrawableRef<",
            "TM;*>;"
        }
    .end annotation

    const-string v0, "$this$toDrawableRef"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-virtual {p0, p1}, Lcom/squareup/mosaic/core/DrawableModel;->createDrawableRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/DrawableRef;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Lcom/squareup/mosaic/core/DrawableRef;->bind(Lcom/squareup/mosaic/core/DrawableModel;)V

    return-object p1

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type com.squareup.mosaic.core.DrawableRef<M, *>"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
