.class public final Lcom/squareup/mosaic/core/ViewRefKt;
.super Ljava/lang/Object;
.source "ViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ViewRef.kt\ncom/squareup/mosaic/core/ViewRefKt\n*L\n1#1,189:1\n161#1,4:190\n*E\n*S KotlinDebug\n*F\n+ 1 ViewRef.kt\ncom/squareup/mosaic/core/ViewRefKt\n*L\n186#1,4:190\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a.\u0010\u0000\u001a\u00020\u0001\"\u000c\u0008\u0000\u0010\u0002*\u0006\u0012\u0002\u0008\u00030\u0003*\u000c\u0012\u0004\u0012\u0002H\u0002\u0012\u0002\u0008\u00030\u00042\n\u0010\u0005\u001a\u0006\u0012\u0002\u0008\u00030\u0003\u001aU\u0010\u0006\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0007*\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00042\u0008\u0010\u0008\u001a\u0004\u0018\u0001H\u00072\u0006\u0010\t\u001a\u0002H\u00072!\u0010\n\u001a\u001d\u0012\u0013\u0012\u0011H\u0007\u00a2\u0006\u000c\u0008\u000c\u0012\u0008\u0008\r\u0012\u0004\u0008\u0008(\t\u0012\u0004\u0012\u00020\u00010\u000bH\u0086\u0008\u00a2\u0006\u0002\u0010\u000e\u001ai\u0010\u0006\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0007*\u00020\u000f*\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00042\u0008\u0010\u0008\u001a\u0004\u0018\u0001H\u00072\u0008\u0010\t\u001a\u0004\u0018\u0001H\u00072!\u0010\u0010\u001a\u001d\u0012\u0013\u0012\u0011H\u0007\u00a2\u0006\u000c\u0008\u000c\u0012\u0008\u0008\r\u0012\u0004\u0008\u0008(\t\u0012\u0004\u0012\u00020\u00010\u000b2\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00010\u0012H\u0086\u0008\u00a2\u0006\u0002\u0010\u0013\u001ax\u0010\u0014\u001a\u000c\u0012\u0004\u0012\u0002H\u0002\u0012\u0002\u0008\u00030\u0004\"\u0008\u0008\u0000\u0010\u0015*\u00020\u000f\"\u000e\u0008\u0001\u0010\u0002*\u0008\u0012\u0004\u0012\u0002H\u00150\u0003\"\u0008\u0008\u0002\u0010\u0016*\u00020\u0017*\u000e\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u0002H\u0016\u0018\u00010\u00042\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u0002H\u00022\u001a\u0010\u001b\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u00010\u0017\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00010\u001cH\u0086\u0008\u00a2\u0006\u0002\u0010\u001d\u00a8\u0006\u001e"
    }
    d2 = {
        "castAndBind",
        "",
        "M",
        "Lcom/squareup/mosaic/core/UiModel;",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "model",
        "ifChangedOrNew",
        "T",
        "oldValue",
        "newValue",
        "block",
        "Lkotlin/Function1;",
        "Lkotlin/ParameterName;",
        "name",
        "(Lcom/squareup/mosaic/core/ViewRef;Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V",
        "",
        "ifNotNull",
        "ifNull",
        "Lkotlin/Function0;",
        "(Lcom/squareup/mosaic/core/ViewRef;Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V",
        "updateOrReplace",
        "P",
        "V",
        "Landroid/view/View;",
        "context",
        "Landroid/content/Context;",
        "newModel",
        "replaceAndroidViewsBlock",
        "Lkotlin/Function2;",
        "(Lcom/squareup/mosaic/core/ViewRef;Landroid/content/Context;Lcom/squareup/mosaic/core/UiModel;Lkotlin/jvm/functions/Function2;)Lcom/squareup/mosaic/core/ViewRef;",
        "mosaic-core_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final castAndBind(Lcom/squareup/mosaic/core/ViewRef;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/squareup/mosaic/core/UiModel<",
            "*>;>(",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "TM;*>;",
            "Lcom/squareup/mosaic/core/UiModel<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "$this$castAndBind"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    invoke-virtual {p0, p1}, Lcom/squareup/mosaic/core/ViewRef;->bind(Lcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method

.method public static final ifChangedOrNew(Lcom/squareup/mosaic/core/ViewRef;Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;TT;TT;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$ifChangedOrNew"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mosaic/core/ViewRef;->getModel()Lcom/squareup/mosaic/core/UiModel;

    move-result-object p0

    if-nez p0, :cond_1

    .line 162
    :cond_0
    invoke-interface {p3, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public static final ifChangedOrNew(Lcom/squareup/mosaic/core/ViewRef;Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;TT;TT;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$ifChangedOrNew"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ifNotNull"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ifNull"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mosaic/core/ViewRef;->getModel()Lcom/squareup/mosaic/core/UiModel;

    move-result-object p0

    if-nez p0, :cond_2

    :cond_0
    if-eqz p2, :cond_1

    .line 186
    invoke-interface {p3, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lkotlin/Unit;

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    invoke-interface {p4}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lkotlin/Unit;

    :cond_2
    :goto_0
    return-void
.end method

.method public static final updateOrReplace(Lcom/squareup/mosaic/core/ViewRef;Landroid/content/Context;Lcom/squareup/mosaic/core/UiModel;Lkotlin/jvm/functions/Function2;)Lcom/squareup/mosaic/core/ViewRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            "M:",
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;V:",
            "Landroid/view/View;",
            ">(",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "*TV;>;",
            "Landroid/content/Context;",
            "TM;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Landroid/view/View;",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "TM;*>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "replaceAndroidViewsBlock"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_0

    .line 133
    invoke-virtual {p0, p2}, Lcom/squareup/mosaic/core/ViewRef;->canAccept(Lcom/squareup/mosaic/core/UiModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    invoke-static {p0, p2}, Lcom/squareup/mosaic/core/ViewRefKt;->castAndBind(Lcom/squareup/mosaic/core/ViewRef;Lcom/squareup/mosaic/core/UiModel;)V

    return-object p0

    .line 138
    :cond_0
    invoke-static {p2, p1}, Lcom/squareup/mosaic/core/UiModelKt;->toView(Lcom/squareup/mosaic/core/UiModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;

    move-result-object p1

    if-eqz p0, :cond_1

    .line 139
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/ViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/squareup/mosaic/core/ViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p2

    invoke-interface {p3, p0, p2}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p1
.end method
