.class public Lcom/squareup/messagebar/RealReaderMessageBar;
.super Ljava/lang/Object;
.source "RealReaderMessageBar.java"

# interfaces
.implements Lcom/squareup/messagebar/api/ReaderMessageBar;


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inflateInto(Landroid/view/ViewGroup;)V
    .locals 2

    .line 16
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/messagebar/R$layout;->reader_message_bar_view:I

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method
