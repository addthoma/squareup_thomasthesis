.class public final Lcom/squareup/location/AndroidGeoCountryCodeGuesser;
.super Ljava/lang/Object;
.source "AndroidGeoCountryCodeGuesser.kt"

# interfaces
.implements Lcom/squareup/location/CountryCodeGuesser;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u000bJ\u001e\u0010\u0015\u001a\u0010\u0012\u0004\u0012\u00020\u000f\u0012\u0006\u0012\u0004\u0018\u00010\u00100\u000e2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\n\u0010\u0018\u001a\u0004\u0018\u00010\u0010H\u0002J\n\u0010\u0019\u001a\u0004\u0018\u00010\u0010H\u0002J\n\u0010\u001a\u001a\u0004\u0018\u00010\u0010H\u0016R(\u0010\u000c\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\u000f\u0012\u0006\u0012\u0004\u0018\u00010\u00100\u000e0\rX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u000f0\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0012R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/location/AndroidGeoCountryCodeGuesser;",
        "Lcom/squareup/location/CountryCodeGuesser;",
        "addressProvider",
        "Lcom/squareup/core/location/providers/AddressProvider;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "locationProvider",
        "Ljavax/inject/Provider;",
        "Landroid/location/Location;",
        "tm",
        "Landroid/telephony/TelephonyManager;",
        "(Lcom/squareup/core/location/providers/AddressProvider;Lcom/squareup/util/Clock;Ljavax/inject/Provider;Landroid/telephony/TelephonyManager;)V",
        "countryCodeGuess",
        "Lio/reactivex/Single;",
        "Lkotlin/Pair;",
        "Lcom/squareup/location/CountryGuesser$Result;",
        "Lcom/squareup/CountryCode;",
        "getCountryCodeGuess",
        "()Lio/reactivex/Single;",
        "countryGuess",
        "getCountryGuess",
        "getResultFromString",
        "countryCodeString",
        "",
        "tryNetwork",
        "trySim",
        "tryTelephony",
        "impl-android-geo_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final countryCodeGuess:Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Single<",
            "Lkotlin/Pair<",
            "Lcom/squareup/location/CountryGuesser$Result;",
            "Lcom/squareup/CountryCode;",
            ">;>;"
        }
    .end annotation
.end field

.field private final tm:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Lcom/squareup/core/location/providers/AddressProvider;Lcom/squareup/util/Clock;Ljavax/inject/Provider;Landroid/telephony/TelephonyManager;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/core/location/providers/AddressProvider;",
            "Lcom/squareup/util/Clock;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Landroid/telephony/TelephonyManager;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "addressProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locationProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p4, p0, Lcom/squareup/location/AndroidGeoCountryCodeGuesser;->tm:Landroid/telephony/TelephonyManager;

    .line 39
    new-instance p4, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$guessFromTelephony$1;

    invoke-direct {p4, p0}, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$guessFromTelephony$1;-><init>(Lcom/squareup/location/AndroidGeoCountryCodeGuesser;)V

    check-cast p4, Ljava/util/concurrent/Callable;

    invoke-static {p4}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p4

    const-string v0, "Single.fromCallable {\n  \u2026e.toResult(), code)\n    }"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-interface {p3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/location/Location;

    const/4 v0, 0x0

    if-eqz p3, :cond_1

    const-wide v1, 0x202fbf000L

    .line 45
    invoke-interface {p2, p3, v1, v2}, Lcom/squareup/util/Clock;->withinPast(Landroid/location/Location;J)Z

    move-result p2

    if-nez p2, :cond_0

    goto :goto_0

    .line 48
    :cond_0
    invoke-interface {p1, p3}, Lcom/squareup/core/location/providers/AddressProvider;->getAddress(Landroid/location/Location;)Lio/reactivex/Single;

    move-result-object p1

    .line 49
    new-instance p2, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$guessFromLocation$1;

    invoke-direct {p2, p0}, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$guessFromLocation$1;-><init>(Lcom/squareup/location/AndroidGeoCountryCodeGuesser;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "addressProvider.getAddre\u2026            }\n          }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 46
    :cond_1
    :goto_0
    new-instance p1, Lkotlin/Pair;

    sget-object p2, Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;->INSTANCE:Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;

    invoke-direct {p1, p2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "just(Pair(NoSupportedCountryDetected, null))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    const/4 p2, 0x2

    new-array p2, p2, [Lio/reactivex/SingleSource;

    const/4 p3, 0x0

    .line 60
    check-cast p4, Lio/reactivex/SingleSource;

    aput-object p4, p2, p3

    const/4 p3, 0x1

    check-cast p1, Lio/reactivex/SingleSource;

    aput-object p1, p2, p3

    invoke-static {p2}, Lio/reactivex/Single;->concatArray([Lio/reactivex/SingleSource;)Lio/reactivex/Flowable;

    move-result-object p1

    .line 61
    sget-object p2, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$1;->INSTANCE:Lcom/squareup/location/AndroidGeoCountryCodeGuesser$1;

    check-cast p2, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, p2}, Lio/reactivex/Flowable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Flowable;

    move-result-object p1

    .line 62
    new-instance p2, Lkotlin/Pair;

    sget-object p3, Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;->INSTANCE:Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;

    invoke-direct {p2, p3, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p1, p2}, Lio/reactivex/Flowable;->first(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lio/reactivex/Single;->cache()Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.concatArray(guess\u2026, null))\n        .cache()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/location/AndroidGeoCountryCodeGuesser;->countryCodeGuess:Lio/reactivex/Single;

    return-void
.end method

.method public static final synthetic access$getResultFromString(Lcom/squareup/location/AndroidGeoCountryCodeGuesser;Ljava/lang/String;)Lkotlin/Pair;
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/location/AndroidGeoCountryCodeGuesser;->getResultFromString(Ljava/lang/String;)Lkotlin/Pair;

    move-result-object p0

    return-object p0
.end method

.method private final getResultFromString(Ljava/lang/String;)Lkotlin/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lkotlin/Pair<",
            "Lcom/squareup/location/CountryGuesser$Result;",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation

    .line 87
    invoke-static {p1}, Lcom/squareup/CountryCode;->parseCountryCode(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object p1

    .line 88
    new-instance v0, Lkotlin/Pair;

    invoke-static {p1}, Lcom/squareup/location/AndroidGeoCountryCodeGuesserKt;->access$toResult(Lcom/squareup/CountryCode;)Lcom/squareup/location/CountryGuesser$Result;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private final tryNetwork()Lcom/squareup/CountryCode;
    .locals 3

    .line 75
    iget-object v0, p0, Lcom/squareup/location/AndroidGeoCountryCodeGuesser;->tm:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v2, "Got country \'%s\' from network"

    .line 76
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    invoke-static {v0}, Lcom/squareup/CountryCode;->parseCountryCode(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object v0

    return-object v0
.end method

.method private final trySim()Lcom/squareup/CountryCode;
    .locals 3

    .line 81
    iget-object v0, p0, Lcom/squareup/location/AndroidGeoCountryCodeGuesser;->tm:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v2, "Got country \'%s\' from sim"

    .line 82
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    invoke-static {v0}, Lcom/squareup/CountryCode;->parseCountryCode(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCountryCodeGuess()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lkotlin/Pair<",
            "Lcom/squareup/location/CountryGuesser$Result;",
            "Lcom/squareup/CountryCode;",
            ">;>;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/location/AndroidGeoCountryCodeGuesser;->countryCodeGuess:Lio/reactivex/Single;

    return-object v0
.end method

.method public getCountryGuess()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/location/CountryGuesser$Result;",
            ">;"
        }
    .end annotation

    .line 36
    invoke-virtual {p0}, Lcom/squareup/location/AndroidGeoCountryCodeGuesser;->getCountryCodeGuess()Lio/reactivex/Single;

    move-result-object v0

    sget-object v1, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$countryGuess$1;->INSTANCE:Lcom/squareup/location/AndroidGeoCountryCodeGuesser$countryGuess$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "countryCodeGuess.map { (result, _) -> result }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public tryTelephony()Lcom/squareup/CountryCode;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/location/AndroidGeoCountryCodeGuesser;->tm:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 71
    :cond_0
    invoke-direct {p0}, Lcom/squareup/location/AndroidGeoCountryCodeGuesser;->trySim()Lcom/squareup/CountryCode;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/squareup/location/AndroidGeoCountryCodeGuesser;->tryNetwork()Lcom/squareup/CountryCode;

    move-result-object v0

    :goto_0
    return-object v0
.end method
