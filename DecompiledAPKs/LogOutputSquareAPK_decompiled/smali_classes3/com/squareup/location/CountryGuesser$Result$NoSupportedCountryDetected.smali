.class public final Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;
.super Lcom/squareup/location/CountryGuesser$Result;
.source "CountryGuesser.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/location/CountryGuesser$Result;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoSupportedCountryDetected"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;",
        "Lcom/squareup/location/CountryGuesser$Result;",
        "()V",
        "supportsPayments",
        "",
        "getSupportsPayments",
        "()Z",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;

    invoke-direct {v0}, Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;-><init>()V

    sput-object v0, Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;->INSTANCE:Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 10
    invoke-direct {p0, v0}, Lcom/squareup/location/CountryGuesser$Result;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public getSupportsPayments()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
