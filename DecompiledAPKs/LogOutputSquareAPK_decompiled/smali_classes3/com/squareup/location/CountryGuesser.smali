.class public interface abstract Lcom/squareup/location/CountryGuesser;
.super Ljava/lang/Object;
.source "CountryGuesser.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/location/CountryGuesser$Result;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001:\u0001\u0007R\u001a\u0010\u0002\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/location/CountryGuesser;",
        "",
        "countryGuess",
        "Lio/reactivex/Single;",
        "Lcom/squareup/location/CountryGuesser$Result;",
        "getCountryGuess",
        "()Lio/reactivex/Single;",
        "Result",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getCountryGuess()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/location/CountryGuesser$Result;",
            ">;"
        }
    .end annotation
.end method
