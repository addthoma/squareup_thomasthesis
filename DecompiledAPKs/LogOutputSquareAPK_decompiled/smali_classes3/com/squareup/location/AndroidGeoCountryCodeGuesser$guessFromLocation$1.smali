.class final Lcom/squareup/location/AndroidGeoCountryCodeGuesser$guessFromLocation$1;
.super Ljava/lang/Object;
.source "AndroidGeoCountryCodeGuesser.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/location/AndroidGeoCountryCodeGuesser;-><init>(Lcom/squareup/core/location/providers/AddressProvider;Lcom/squareup/util/Clock;Ljavax/inject/Provider;Landroid/telephony/TelephonyManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lkotlin/Pair;",
        "Lcom/squareup/location/CountryGuesser$Result;",
        "Lcom/squareup/CountryCode;",
        "addressResult",
        "Lcom/squareup/core/location/providers/AddressProvider$Result;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/location/AndroidGeoCountryCodeGuesser;


# direct methods
.method constructor <init>(Lcom/squareup/location/AndroidGeoCountryCodeGuesser;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$guessFromLocation$1;->this$0:Lcom/squareup/location/AndroidGeoCountryCodeGuesser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/core/location/providers/AddressProvider$Result;

    invoke-virtual {p0, p1}, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$guessFromLocation$1;->apply(Lcom/squareup/core/location/providers/AddressProvider$Result;)Lkotlin/Pair;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lcom/squareup/core/location/providers/AddressProvider$Result;)Lkotlin/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/core/location/providers/AddressProvider$Result;",
            ")",
            "Lkotlin/Pair<",
            "Lcom/squareup/location/CountryGuesser$Result;",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation

    const-string v0, "addressResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Got address %s from provider"

    .line 50
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    instance-of v0, p1, Lcom/squareup/core/location/providers/AddressProvider$Result$Resolved;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$guessFromLocation$1;->this$0:Lcom/squareup/location/AndroidGeoCountryCodeGuesser;

    check-cast p1, Lcom/squareup/core/location/providers/AddressProvider$Result$Resolved;

    invoke-virtual {p1}, Lcom/squareup/core/location/providers/AddressProvider$Result$Resolved;->getAddress()Landroid/location/Address;

    move-result-object p1

    invoke-virtual {p1}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object p1

    const-string v1, "addressResult.address.countryCode"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/location/AndroidGeoCountryCodeGuesser;->access$getResultFromString(Lcom/squareup/location/AndroidGeoCountryCodeGuesser;Ljava/lang/String;)Lkotlin/Pair;

    move-result-object p1

    goto :goto_0

    .line 55
    :cond_0
    new-instance p1, Lkotlin/Pair;

    sget-object v0, Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;->INSTANCE:Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-object p1
.end method
