.class public final Lcom/squareup/location/analytics/LocationAnalyticsUpdater;
.super Ljava/lang/Object;
.source "LocationAnalyticsUpdater.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLocationAnalyticsUpdater.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LocationAnalyticsUpdater.kt\ncom/squareup/location/analytics/LocationAnalyticsUpdater\n*L\n1#1,49:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\nH\u0016J\u000c\u0010\u000e\u001a\u00020\u000f*\u00020\u000fH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/location/analytics/LocationAnalyticsUpdater;",
        "Lmortar/Scoped;",
        "continuousLocationMonitor",
        "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "sanitize",
        "Landroid/location/Location;",
        "location-analytics_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final continuousLocationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "continuousLocationMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater;->continuousLocationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    iput-object p2, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p3, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/location/analytics/LocationAnalyticsUpdater;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$sanitize(Lcom/squareup/location/analytics/LocationAnalyticsUpdater;Landroid/location/Location;)Landroid/location/Location;
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/squareup/location/analytics/LocationAnalyticsUpdater;->sanitize(Landroid/location/Location;)Landroid/location/Location;

    move-result-object p0

    return-object p0
.end method

.method private final sanitize(Landroid/location/Location;)Landroid/location/Location;
    .locals 4

    .line 42
    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v0

    const/16 v2, 0x64

    int-to-double v2, v2

    mul-double v0, v0, v2

    invoke-static {v0, v1}, Lkotlin/math/MathKt;->truncate(D)D

    move-result-wide v0

    div-double/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setAltitude(D)V

    .line 43
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    mul-double v0, v0, v2

    invoke-static {v0, v1}, Lkotlin/math/MathKt;->truncate(D)D

    move-result-wide v0

    div-double/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 44
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    mul-double v0, v0, v2

    invoke-static {v0, v1}, Lkotlin/math/MathKt;->truncate(D)D

    move-result-wide v0

    div-double/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    return-object p1
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 30
    iget-object v1, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater;->continuousLocationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-interface {v1}, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;->onLocation()Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "continuousLocationMonitor.onLocation()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v2, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->SANITIZE_EVENTSTREAM_COORDINATES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "features.featureEnabled(\u2026_EVENTSTREAM_COORDINATES)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/squareup/location/analytics/LocationAnalyticsUpdater$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/location/analytics/LocationAnalyticsUpdater$onEnterScope$1;-><init>(Lcom/squareup/location/analytics/LocationAnalyticsUpdater;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "combineLatest(\n        c\u2026 else location)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
