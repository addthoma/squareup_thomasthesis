.class public abstract Lcom/squareup/compvoidcontroller/NoopCompVoidControllerModule;
.super Ljava/lang/Object;
.source "NoopCompVoidControllerModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/compvoidcontroller/NoopCompVoidControllerModule;",
        "",
        "()V",
        "bindCompController",
        "Lcom/squareup/compvoidcontroller/CompController;",
        "compController",
        "Lcom/squareup/compvoidcontroller/NoopCompController;",
        "bindVoidController",
        "Lcom/squareup/compvoidcontroller/VoidController;",
        "voidController",
        "Lcom/squareup/compvoidcontroller/NoopVoidController;",
        "impl-noop-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindCompController(Lcom/squareup/compvoidcontroller/NoopCompController;)Lcom/squareup/compvoidcontroller/CompController;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindVoidController(Lcom/squareup/compvoidcontroller/NoopVoidController;)Lcom/squareup/compvoidcontroller/VoidController;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
