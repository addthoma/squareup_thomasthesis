.class final Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectState$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DatabaseImpl.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->selectState(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;)Lcom/squareup/sqldelight/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/sqldelight/db/SqlCursor;",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "cursor",
        "Lcom/squareup/sqldelight/db/SqlCursor;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;


# direct methods
.method constructor <init>(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectState$1;->this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/sqldelight/db/SqlCursor;)Lcom/squareup/notificationcenterdata/Notification$State;
    .locals 2

    const-string v0, "cursor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectState$1;->this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;

    invoke-static {v0}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->access$getDatabase$p(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;)Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;->getNotification_statesAdapter$impl_release()Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;->getStateAdapter()Lcom/squareup/sqldelight/ColumnAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Lcom/squareup/sqldelight/db/SqlCursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-interface {v0, p1}, Lcom/squareup/sqldelight/ColumnAdapter;->decode(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/notificationcenterdata/Notification$State;

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/sqldelight/db/SqlCursor;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$selectState$1;->invoke(Lcom/squareup/sqldelight/db/SqlCursor;)Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object p1

    return-object p1
.end method
