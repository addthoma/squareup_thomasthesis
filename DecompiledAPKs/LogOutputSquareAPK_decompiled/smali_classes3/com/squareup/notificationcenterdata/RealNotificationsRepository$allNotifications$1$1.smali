.class final Lcom/squareup/notificationcenterdata/RealNotificationsRepository$allNotifications$1$1;
.super Ljava/lang/Object;
.source "RealNotificationsRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenterdata/RealNotificationsRepository;-><init>(Ljava/util/Set;Ljava/util/Comparator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
        "it",
        "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/notificationcenterdata/RealNotificationsRepository$allNotifications$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$allNotifications$1$1;

    invoke-direct {v0}, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$allNotifications$1$1;-><init>()V

    sput-object v0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$allNotifications$1$1;->INSTANCE:Lcom/squareup/notificationcenterdata/RealNotificationsRepository$allNotifications$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/notificationcenterdata/NotificationsSource$Result;)Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    instance-of v0, p1, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    .line 55
    check-cast p1, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;

    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;->getNotifications()Ljava/util/List;

    move-result-object p1

    const/4 v1, 0x0

    .line 54
    invoke-direct {v0, p1, v1}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;-><init>(Ljava/util/List;Z)V

    goto :goto_0

    .line 57
    :cond_0
    instance-of p1, p1, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Failure;

    if-eqz p1, :cond_1

    new-instance v0, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    .line 58
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    const/4 v1, 0x1

    .line 57
    invoke-direct {v0, p1, v1}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;-><init>(Ljava/util/List;Z)V

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/notificationcenterdata/NotificationsSource$Result;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$allNotifications$1$1;->apply(Lcom/squareup/notificationcenterdata/NotificationsSource$Result;)Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    move-result-object p1

    return-object p1
.end method
