.class public final Lcom/squareup/notificationcenterdata/utils/MessagesKt;
.super Ljava/lang/Object;
.source "Messages.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMessages.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Messages.kt\ncom/squareup/notificationcenterdata/utils/MessagesKt\n*L\n1#1,109:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0014\u0010\u0005\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0008H\u0002\u001a\u0016\u0010\t\u001a\u0004\u0018\u00010\n*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0008H\u0000\u001a\u000e\u0010\u000b\u001a\u0004\u0018\u00010\u000c*\u00020\rH\u0002\u001a\u000e\u0010\u000e\u001a\u0004\u0018\u00010\u000f*\u00020\u0010H\u0002\"\u001a\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0011"
    }
    d2 = {
        "notificationContent",
        "Lcom/squareup/communications/Message$Content$NotificationContent;",
        "Lcom/squareup/communications/Message;",
        "getNotificationContent",
        "(Lcom/squareup/communications/Message;)Lcom/squareup/communications/Message$Content$NotificationContent;",
        "destination",
        "Lcom/squareup/notificationcenterdata/Notification$Destination;",
        "clientActionTranslationDispatcher",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;",
        "toNotification",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "toNotificationState",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "Lcom/squareup/communications/Message$State;",
        "toPriority",
        "Lcom/squareup/notificationcenterdata/Notification$Priority;",
        "Lcom/squareup/communications/Message$Type;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final destination(Lcom/squareup/communications/Message;Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;)Lcom/squareup/notificationcenterdata/Notification$Destination;
    .locals 5

    .line 89
    invoke-static {p0}, Lcom/squareup/notificationcenterdata/utils/MessagesKt;->getNotificationContent(Lcom/squareup/communications/Message;)Lcom/squareup/communications/Message$Content$NotificationContent;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/communications/Message$Content$NotificationContent;->getClientAction()Lcom/squareup/protos/client/ClientAction;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 90
    :goto_0
    invoke-static {p0}, Lcom/squareup/notificationcenterdata/utils/MessagesKt;->getNotificationContent(Lcom/squareup/communications/Message;)Lcom/squareup/communications/Message$Content$NotificationContent;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/squareup/communications/Message$Content$NotificationContent;->getUrl()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, v1

    :goto_1
    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v0, :cond_5

    .line 93
    invoke-interface {p1, v0}, Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;->canHandleInternally(Lcom/squareup/protos/client/ClientAction;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 94
    move-object p0, v2

    check-cast p0, Ljava/lang/CharSequence;

    if-eqz p0, :cond_2

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result p0

    if-nez p0, :cond_3

    :cond_2
    const/4 v3, 0x1

    :cond_3
    if-eqz v3, :cond_4

    .line 95
    new-instance p0, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;

    invoke-direct {p0, v0}, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;-><init>(Lcom/squareup/protos/client/ClientAction;)V

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination;

    goto/16 :goto_7

    .line 97
    :cond_4
    new-instance p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedSupportedClientAction;

    invoke-direct {p0, v0, v2}, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedSupportedClientAction;-><init>(Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;)V

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination;

    goto :goto_7

    :cond_5
    if-eqz v0, :cond_8

    .line 100
    move-object p1, v2

    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_7

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_6

    goto :goto_2

    :cond_6
    const/4 p1, 0x0

    goto :goto_3

    :cond_7
    :goto_2
    const/4 p1, 0x1

    :goto_3
    if-nez p1, :cond_8

    new-instance p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedUnsupportedClientAction;

    invoke-direct {p0, v0, v2}, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedUnsupportedClientAction;-><init>(Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;)V

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination;

    goto :goto_7

    :cond_8
    if-eqz v0, :cond_b

    .line 102
    move-object p1, v2

    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_a

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_9

    goto :goto_4

    :cond_9
    const/4 p1, 0x0

    goto :goto_5

    :cond_a
    :goto_4
    const/4 p1, 0x1

    :goto_5
    if-eqz p1, :cond_b

    new-instance p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UnsupportedClientAction;

    invoke-direct {p0, v0}, Lcom/squareup/notificationcenterdata/Notification$Destination$UnsupportedClientAction;-><init>(Lcom/squareup/protos/client/ClientAction;)V

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination;

    goto :goto_7

    .line 104
    :cond_b
    move-object p1, v2

    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_c

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_d

    :cond_c
    const/4 v3, 0x1

    :cond_d
    if-nez v3, :cond_10

    invoke-static {p0}, Lcom/squareup/notificationcenterdata/utils/MessagesKt;->getNotificationContent(Lcom/squareup/communications/Message;)Lcom/squareup/communications/Message$Content$NotificationContent;

    move-result-object p0

    if-eqz p0, :cond_e

    invoke-virtual {p0}, Lcom/squareup/communications/Message$Content$NotificationContent;->getOpenInBrowserDialogBody()Ljava/lang/String;

    move-result-object v1

    :cond_e
    if-eqz v1, :cond_f

    goto :goto_6

    :cond_f
    const-string v1, ""

    :goto_6
    new-instance p0, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;

    invoke-direct {p0, v2, v1}, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination;

    goto :goto_7

    .line 106
    :cond_10
    sget-object p0, Lcom/squareup/notificationcenterdata/Notification$Destination$Nowhere;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Destination$Nowhere;

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination;

    :goto_7
    return-object p0
.end method

.method private static final getNotificationContent(Lcom/squareup/communications/Message;)Lcom/squareup/communications/Message$Content$NotificationContent;
    .locals 1

    .line 85
    invoke-virtual {p0}, Lcom/squareup/communications/Message;->getContent()Lcom/squareup/communications/Message$Content;

    move-result-object p0

    instance-of v0, p0, Lcom/squareup/communications/Message$Content$NotificationContent;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    :cond_0
    check-cast p0, Lcom/squareup/communications/Message$Content$NotificationContent;

    return-object p0
.end method

.method public static final toNotification(Lcom/squareup/communications/Message;Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;)Lcom/squareup/notificationcenterdata/Notification;
    .locals 17

    const-string v0, "$this$toNotification"

    move-object/from16 v1, p0

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clientActionTranslationDispatcher"

    move-object/from16 v2, p1

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-static/range {p0 .. p0}, Lcom/squareup/notificationcenterdata/utils/MessagesKt;->getNotificationContent(Lcom/squareup/communications/Message;)Lcom/squareup/communications/Message$Content$NotificationContent;

    move-result-object v0

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    .line 44
    new-instance v16, Lcom/squareup/notificationcenterdata/Notification;

    .line 45
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/communications/Message;->getId()Ljava/lang/String;

    move-result-object v5

    .line 46
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/communications/Message;->getRequestToken()Ljava/lang/String;

    move-result-object v6

    .line 47
    invoke-virtual {v0}, Lcom/squareup/communications/Message$Content$NotificationContent;->getTitle()Ljava/lang/String;

    move-result-object v7

    .line 48
    invoke-virtual {v0}, Lcom/squareup/communications/Message$Content$NotificationContent;->getBody()Ljava/lang/String;

    move-result-object v8

    .line 50
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/communications/Message;->getState()Lcom/squareup/communications/Message$State;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/notificationcenterdata/utils/MessagesKt;->toNotificationState(Lcom/squareup/communications/Message$State;)Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 52
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/communications/Message;->getType()Lcom/squareup/communications/Message$Type;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/notificationcenterdata/utils/MessagesKt;->toPriority(Lcom/squareup/communications/Message$Type;)Lcom/squareup/notificationcenterdata/Notification$Priority;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 53
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/communications/Message;->getType()Lcom/squareup/communications/Message$Type;

    move-result-object v0

    sget-object v3, Lcom/squareup/communications/Message$Type$AlertHighPriority;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertHighPriority;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;

    :goto_0
    check-cast v0, Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    move-object v11, v0

    .line 54
    invoke-static/range {p0 .. p1}, Lcom/squareup/notificationcenterdata/utils/MessagesKt;->destination(Lcom/squareup/communications/Message;Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;)Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v12

    .line 55
    sget-object v13, Lcom/squareup/notificationcenterdata/Notification$Source;->REMOTE:Lcom/squareup/notificationcenterdata/Notification$Source;

    .line 56
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/communications/Message;->getCreatedAt()Lorg/threeten/bp/Instant;

    move-result-object v14

    .line 57
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/communications/Message;->getType()Lcom/squareup/communications/Message$Type;

    move-result-object v15

    move-object/from16 v4, v16

    .line 44
    invoke-direct/range {v4 .. v15}, Lcom/squareup/notificationcenterdata/Notification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$State;Lcom/squareup/notificationcenterdata/Notification$Priority;Lcom/squareup/notificationcenterdata/Notification$DisplayType;Lcom/squareup/notificationcenterdata/Notification$Destination;Lcom/squareup/notificationcenterdata/Notification$Source;Lorg/threeten/bp/Instant;Lcom/squareup/communications/Message$Type;)V

    goto :goto_1

    :cond_1
    return-object v3

    :cond_2
    move-object/from16 v16, v3

    :goto_1
    return-object v16
.end method

.method private static final toNotificationState(Lcom/squareup/communications/Message$State;)Lcom/squareup/notificationcenterdata/Notification$State;
    .locals 1

    .line 62
    sget-object v0, Lcom/squareup/communications/Message$State$Unread;->INSTANCE:Lcom/squareup/communications/Message$State$Unread;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p0, Lcom/squareup/notificationcenterdata/Notification$State;->UNREAD:Lcom/squareup/notificationcenterdata/Notification$State;

    goto :goto_0

    .line 63
    :cond_0
    sget-object v0, Lcom/squareup/communications/Message$State$Read;->INSTANCE:Lcom/squareup/communications/Message$State$Read;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p0, Lcom/squareup/notificationcenterdata/Notification$State;->READ:Lcom/squareup/notificationcenterdata/Notification$State;

    goto :goto_0

    .line 64
    :cond_1
    sget-object v0, Lcom/squareup/communications/Message$State$Dismissed;->INSTANCE:Lcom/squareup/communications/Message$State$Dismissed;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    const/4 p0, 0x0

    :goto_0
    return-object p0

    :cond_2
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final toPriority(Lcom/squareup/communications/Message$Type;)Lcom/squareup/notificationcenterdata/Notification$Priority;
    .locals 1

    .line 73
    instance-of v0, p0, Lcom/squareup/communications/Message$Type$AlertToCompleteOrders;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 74
    :cond_0
    instance-of v0, p0, Lcom/squareup/communications/Message$Type$AlertCustomerComms;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 75
    :cond_1
    instance-of v0, p0, Lcom/squareup/communications/Message$Type$AlertHighPriority;

    if-eqz v0, :cond_2

    goto :goto_0

    .line 76
    :cond_2
    instance-of v0, p0, Lcom/squareup/communications/Message$Type$AlertResolveDispute;

    if-eqz v0, :cond_3

    goto :goto_0

    .line 77
    :cond_3
    instance-of v0, p0, Lcom/squareup/communications/Message$Type$AlertSetupPos;

    if-eqz v0, :cond_4

    goto :goto_0

    .line 78
    :cond_4
    instance-of v0, p0, Lcom/squareup/communications/Message$Type$AlertSupportCenter;

    if-eqz v0, :cond_5

    goto :goto_0

    .line 79
    :cond_5
    instance-of v0, p0, Lcom/squareup/communications/Message$Type$NonUrgentBusinessNoticesForSellers;

    if-eqz v0, :cond_6

    :goto_0
    sget-object p0, Lcom/squareup/notificationcenterdata/Notification$Priority$Important;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Priority$Important;

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Priority;

    goto :goto_1

    .line 80
    :cond_6
    instance-of p0, p0, Lcom/squareup/communications/Message$Type$Unsupported;

    if-eqz p0, :cond_7

    const/4 p0, 0x0

    goto :goto_1

    .line 81
    :cond_7
    sget-object p0, Lcom/squareup/notificationcenterdata/Notification$Priority$General;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Priority$General;

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Priority;

    :goto_1
    return-object p0
.end method
