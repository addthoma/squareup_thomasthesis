.class final Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$handleNotifications$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealNotificationCenterWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->handleNotifications(Lcom/squareup/workflow/RenderContext;Lcom/squareup/notificationcenter/NotificationCenterTab;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
        "+",
        "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
        ">;",
        "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealNotificationCenterWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealNotificationCenterWorkflow.kt\ncom/squareup/notificationcenter/RealNotificationCenterWorkflow$handleNotifications$1\n*L\n1#1,366:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

.field final synthetic this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;Lcom/squareup/notificationcenter/NotificationCenterTab;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$handleNotifications$1;->this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;

    iput-object p2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$handleNotifications$1;->$selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlin/Pair;)Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
            "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
            ">;)",
            "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    .line 188
    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$handleNotifications$1;->$selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    :goto_0
    move-object v6, v1

    goto :goto_1

    .line 189
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getNotifications()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getNotifications()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_1

    sget-object v1, Lcom/squareup/notificationcenter/NotificationCenterTab$WhatsNewTab;->INSTANCE:Lcom/squareup/notificationcenter/NotificationCenterTab$WhatsNewTab;

    check-cast v1, Lcom/squareup/notificationcenter/NotificationCenterTab;

    goto :goto_0

    .line 190
    :cond_1
    sget-object v1, Lcom/squareup/notificationcenter/NotificationCenterTab$AccountTab;->INSTANCE:Lcom/squareup/notificationcenter/NotificationCenterTab$AccountTab;

    check-cast v1, Lcom/squareup/notificationcenter/NotificationCenterTab;

    goto :goto_0

    .line 192
    :goto_1
    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$handleNotifications$1;->this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;

    invoke-static {v1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->access$getAnalytics$p(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;)Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    move-result-object v1

    .line 193
    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getNotifications()Ljava/util/List;

    move-result-object v3

    .line 194
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getNotifications()Ljava/util/List;

    move-result-object v4

    .line 192
    invoke-interface {v1, v3, v4, v6}, Lcom/squareup/notificationcenter/NotificationCenterAnalytics;->logNotificationsLoaded(Ljava/util/List;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;)V

    .line 197
    new-instance v1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;

    .line 198
    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getNotifications()Ljava/util/List;

    move-result-object v4

    .line 199
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getNotifications()Ljava/util/List;

    move-result-object v5

    .line 201
    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getHasError()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getHasError()Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    const/4 v7, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v7, 0x1

    .line 202
    :goto_3
    iget-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$handleNotifications$1;->this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;

    invoke-static {p1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->access$getAnalytics$p(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;)Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    move-result-object v8

    move-object v3, v1

    .line 197
    invoke-direct/range {v3 .. v8}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLcom/squareup/notificationcenter/NotificationCenterAnalytics;)V

    return-object v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$handleNotifications$1;->invoke(Lkotlin/Pair;)Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;

    move-result-object p1

    return-object p1
.end method
