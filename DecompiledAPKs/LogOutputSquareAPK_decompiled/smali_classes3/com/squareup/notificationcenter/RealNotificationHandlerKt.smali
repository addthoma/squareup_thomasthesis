.class public final Lcom/squareup/notificationcenter/RealNotificationHandlerKt;
.super Ljava/lang/Object;
.source "RealNotificationHandler.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\"\u001a\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u001a\u0010\u0005\u001a\u0004\u0018\u00010\u0006*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "clientAction",
        "Lcom/squareup/protos/client/ClientAction;",
        "Lcom/squareup/notificationcenterdata/Notification$Destination;",
        "getClientAction",
        "(Lcom/squareup/notificationcenterdata/Notification$Destination;)Lcom/squareup/protos/client/ClientAction;",
        "externalUrl",
        "",
        "getExternalUrl",
        "(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getClientAction$p(Lcom/squareup/notificationcenterdata/Notification$Destination;)Lcom/squareup/protos/client/ClientAction;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/RealNotificationHandlerKt;->getClientAction(Lcom/squareup/notificationcenterdata/Notification$Destination;)Lcom/squareup/protos/client/ClientAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getExternalUrl$p(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/RealNotificationHandlerKt;->getExternalUrl(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final getClientAction(Lcom/squareup/notificationcenterdata/Notification$Destination;)Lcom/squareup/protos/client/ClientAction;
    .locals 1

    .line 56
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedSupportedClientAction;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedSupportedClientAction;

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedSupportedClientAction;->getClientAction()Lcom/squareup/protos/client/ClientAction;

    move-result-object p0

    goto :goto_0

    .line 57
    :cond_0
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;->getClientAction()Lcom/squareup/protos/client/ClientAction;

    move-result-object p0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method private static final getExternalUrl(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;
    .locals 1

    .line 62
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedSupportedClientAction;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedSupportedClientAction;

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedSupportedClientAction;->getUrl()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 63
    :cond_0
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedUnsupportedClientAction;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedUnsupportedClientAction;

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedUnsupportedClientAction;->getUrl()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 64
    :cond_1
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;->getUrl()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method
