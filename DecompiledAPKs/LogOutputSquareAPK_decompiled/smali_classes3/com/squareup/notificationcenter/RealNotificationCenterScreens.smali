.class public final Lcom/squareup/notificationcenter/RealNotificationCenterScreens;
.super Ljava/lang/Object;
.source "RealNotificationCenterScreens.kt"

# interfaces
.implements Lcom/squareup/notificationcenter/NotificationCenterScreens;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenter/RealNotificationCenterScreens$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00082\u00020\u0001:\u0001\u0008B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u00020\u00042\u0012\u0010\u0005\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u0007H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/RealNotificationCenterScreens;",
        "Lcom/squareup/notificationcenter/NotificationCenterScreens;",
        "()V",
        "screenInListOfScreens",
        "",
        "key",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/notificationcenter/RealNotificationCenterScreens$Companion;

.field private static final workflowScreenKeys:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/legacy/Screen$Key;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/notificationcenter/RealNotificationCenterScreens$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/notificationcenter/RealNotificationCenterScreens$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/notificationcenter/RealNotificationCenterScreens;->Companion:Lcom/squareup/notificationcenter/RealNotificationCenterScreens$Companion;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/workflow/legacy/Screen$Key;

    .line 15
    sget-object v1, Lcom/squareup/notificationcenter/NotificationCenterScreen$LoadingScreen;->Companion:Lcom/squareup/notificationcenter/NotificationCenterScreen$LoadingScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/notificationcenter/NotificationCenterScreen$LoadingScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 16
    sget-object v1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->Companion:Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 17
    sget-object v1, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;->Companion:Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 18
    sget-object v1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->Companion:Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 14
    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/squareup/notificationcenter/RealNotificationCenterScreens;->workflowScreenKeys:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public screenInListOfScreens(Lcom/squareup/workflow/legacy/Screen$Key;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)Z"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    sget-object v0, Lcom/squareup/notificationcenter/RealNotificationCenterScreens;->workflowScreenKeys:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->contains(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method
