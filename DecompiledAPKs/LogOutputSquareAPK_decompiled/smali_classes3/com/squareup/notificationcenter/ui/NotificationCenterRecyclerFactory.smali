.class public final Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;
.super Ljava/lang/Object;
.source "NotificationCenterRecyclerFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationCenterRecyclerFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationCenterRecyclerFactory.kt\ncom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n*L\n1#1,81:1\n49#2:82\n50#2,3:88\n53#2:109\n599#3,4:83\n601#3:87\n310#4,6:91\n310#4,6:97\n310#4,6:103\n*E\n*S KotlinDebug\n*F\n+ 1 NotificationCenterRecyclerFactory.kt\ncom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory\n*L\n24#1:82\n24#1,3:88\n24#1:109\n24#1,4:83\n24#1:87\n24#1,6:91\n24#1,6:97\n24#1,6:103\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;",
        "",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Lcom/squareup/recycler/RecyclerFactory;)V",
        "create",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/notificationcenter/ui/NotificationCenterRow;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "recyclerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-void
.end method


# virtual methods
.method public final create(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            ")",
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/notificationcenter/ui/NotificationCenterRow;",
            ">;"
        }
    .end annotation

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 82
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 83
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 84
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 88
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 89
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 92
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory$$special$$inlined$row$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 25
    invoke-static {v0}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt;->access$createNotificationRow(Lcom/squareup/cycler/StandardRowSpec;)V

    .line 92
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 91
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 98
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory$$special$$inlined$row$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 26
    invoke-static {v0}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt;->access$createWarningBannerRow(Lcom/squareup/cycler/StandardRowSpec;)V

    .line 98
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 97
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 104
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory$$special$$inlined$row$3;->INSTANCE:Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory$$special$$inlined$row$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 27
    invoke-static {v0}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt;->access$createPaddingRow(Lcom/squareup/cycler/StandardRowSpec;)V

    .line 104
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 103
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 86
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    return-object p1

    .line 83
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
