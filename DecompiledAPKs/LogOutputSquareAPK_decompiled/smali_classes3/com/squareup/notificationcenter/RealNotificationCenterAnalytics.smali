.class public final Lcom/squareup/notificationcenter/RealNotificationCenterAnalytics;
.super Ljava/lang/Object;
.source "RealNotificationCenterAnalytics.kt"

# interfaces
.implements Lcom/squareup/notificationcenter/NotificationCenterAnalytics;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealNotificationCenterAnalytics.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealNotificationCenterAnalytics.kt\ncom/squareup/notificationcenter/RealNotificationCenterAnalytics\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n*L\n1#1,239:1\n1288#2:240\n1313#2,3:241\n1316#2,3:251\n1288#2:254\n1313#2,3:255\n1316#2,3:265\n1550#2,3:268\n1713#2,14:271\n347#3,7:244\n347#3,7:258\n*E\n*S KotlinDebug\n*F\n+ 1 RealNotificationCenterAnalytics.kt\ncom/squareup/notificationcenter/RealNotificationCenterAnalytics\n*L\n43#1:240\n43#1,3:241\n43#1,3:251\n44#1:254\n44#1,3:255\n44#1,3:265\n67#1,3:268\n68#1,14:271\n43#1,7:244\n44#1,7:258\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J \u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0016J\u0010\u0010\u000c\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J,\u0010\r\u001a\u00020\u00062\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u001e\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u00122\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u000fH\u0016J\u0010\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0010\u0010\u0016\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/RealNotificationCenterAnalytics;",
        "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "logNotificationClicked",
        "",
        "notification",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "openedInternally",
        "",
        "openedExternally",
        "logNotificationOpened",
        "logNotificationsLoaded",
        "importantNotifications",
        "",
        "generalNotifications",
        "selectedTab",
        "Lcom/squareup/notificationcenter/NotificationCenterTab;",
        "logTabToggled",
        "notifications",
        "logWebBrowserDialogCanceled",
        "logWebBrowserDialogOpened",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public logNotificationClicked(Lcom/squareup/notificationcenterdata/Notification;ZZ)V
    .locals 10

    const-string v0, "notification"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 111
    new-instance v9, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;

    .line 112
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getId()Ljava/lang/String;

    move-result-object v2

    .line 113
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getClientActionString(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object v3

    .line 114
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getUrlString(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object v4

    .line 115
    invoke-static {p1}, Lcom/squareup/notificationcenter/RealNotificationCenterAnalyticsKt;->access$getTabString$p(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;

    move-result-object v5

    .line 116
    invoke-static {p1}, Lcom/squareup/notificationcenter/RealNotificationCenterAnalyticsKt;->access$getSourceString$p(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;

    move-result-object v6

    move-object v1, v9

    move v7, p2

    move v8, p3

    .line 111
    invoke-direct/range {v1 .. v8}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    check-cast v9, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 110
    invoke-interface {v0, v9}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logNotificationOpened(Lcom/squareup/notificationcenterdata/Notification;)V
    .locals 8

    const-string v0, "notification"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 93
    new-instance v7, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationOpen;

    .line 94
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getId()Ljava/lang/String;

    move-result-object v2

    .line 95
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getClientActionString(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object v3

    .line 96
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getUrlString(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object v4

    .line 97
    invoke-static {p1}, Lcom/squareup/notificationcenter/RealNotificationCenterAnalyticsKt;->access$getTabString$p(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;

    move-result-object v5

    .line 98
    invoke-static {p1}, Lcom/squareup/notificationcenter/RealNotificationCenterAnalyticsKt;->access$getSourceString$p(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;

    move-result-object v6

    move-object v1, v7

    .line 93
    invoke-direct/range {v1 .. v6}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationOpen;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v7, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 92
    invoke-interface {v0, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logNotificationsLoaded(Ljava/util/List;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            ")V"
        }
    .end annotation

    const-string v0, "importantNotifications"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "generalNotifications"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedTab"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 240
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v1, Ljava/util/Map;

    .line 241
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 242
    move-object v3, v2

    check-cast v3, Lcom/squareup/notificationcenterdata/Notification;

    .line 43
    invoke-virtual {v3}, Lcom/squareup/notificationcenterdata/Notification;->getState()Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object v3

    .line 244
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    .line 243
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 247
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    :cond_0
    check-cast v4, Ljava/util/List;

    .line 251
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 44
    :cond_1
    move-object v0, p2

    check-cast v0, Ljava/lang/Iterable;

    .line 254
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v2, Ljava/util/Map;

    .line 255
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 256
    move-object v4, v3

    check-cast v4, Lcom/squareup/notificationcenterdata/Notification;

    .line 44
    invoke-virtual {v4}, Lcom/squareup/notificationcenterdata/Notification;->getState()Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object v4

    .line 258
    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_2

    .line 257
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 261
    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    :cond_2
    check-cast v5, Ljava/util/List;

    .line 265
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 45
    :cond_3
    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$State;->UNREAD:Lcom/squareup/notificationcenterdata/Notification$State;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_4

    goto :goto_2

    :cond_4
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    .line 47
    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$State;->UNREAD:Lcom/squareup/notificationcenterdata/Notification$State;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_5

    goto :goto_3

    :cond_5
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    .line 49
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    add-int v3, v9, v7

    if-lez v3, :cond_6

    const/4 v3, 0x1

    const/4 v4, 0x1

    goto :goto_4

    :cond_6
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 52
    :goto_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    .line 54
    sget-object p1, Lcom/squareup/notificationcenterdata/Notification$State;->READ:Lcom/squareup/notificationcenterdata/Notification$State;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-eqz p1, :cond_7

    goto :goto_5

    :cond_7
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_5
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    .line 55
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v8

    .line 57
    sget-object p1, Lcom/squareup/notificationcenterdata/Notification$State;->READ:Lcom/squareup/notificationcenterdata/Notification$State;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-eqz p1, :cond_8

    goto :goto_6

    :cond_8
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    .line 58
    invoke-static {p3}, Lcom/squareup/notificationcenter/RealNotificationCenterAnalyticsKt;->access$getString$p(Lcom/squareup/notificationcenter/NotificationCenterTab;)Ljava/lang/String;

    move-result-object v11

    .line 50
    new-instance p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;

    move-object v3, p1

    invoke-direct/range {v3 .. v11}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;-><init>(ZIIIIIILjava/lang/String;)V

    check-cast p1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 49
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logTabToggled(Lcom/squareup/notificationcenter/NotificationCenterTab;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;)V"
        }
    .end annotation

    const-string v0, "selectedTab"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notifications"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    move-object v1, p2

    check-cast v1, Ljava/lang/Iterable;

    .line 268
    instance-of v0, v1, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 269
    :cond_1
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/notificationcenterdata/Notification;

    .line 67
    invoke-virtual {v4}, Lcom/squareup/notificationcenterdata/Notification;->getState()Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object v4

    sget-object v5, Lcom/squareup/notificationcenterdata/Notification$State;->UNREAD:Lcom/squareup/notificationcenterdata/Notification$State;

    if-ne v4, v5, :cond_3

    const/4 v4, 0x1

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_2

    const/4 v0, 0x1

    .line 271
    :goto_1
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 272
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-nez v3, :cond_4

    move-object v3, v4

    goto :goto_2

    .line 273
    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 274
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_5

    goto :goto_2

    .line 275
    :cond_5
    move-object v5, v3

    check-cast v5, Lcom/squareup/notificationcenterdata/Notification;

    .line 68
    invoke-virtual {v5}, Lcom/squareup/notificationcenterdata/Notification;->getCreatedAt()Lorg/threeten/bp/Instant;

    move-result-object v5

    check-cast v5, Ljava/lang/Comparable;

    .line 277
    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 278
    move-object v7, v6

    check-cast v7, Lcom/squareup/notificationcenterdata/Notification;

    .line 68
    invoke-virtual {v7}, Lcom/squareup/notificationcenterdata/Notification;->getCreatedAt()Lorg/threeten/bp/Instant;

    move-result-object v7

    check-cast v7, Ljava/lang/Comparable;

    .line 279
    invoke-interface {v5, v7}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v8

    if-gez v8, :cond_7

    move-object v3, v6

    move-object v5, v7

    .line 283
    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_6

    .line 284
    :goto_2
    check-cast v3, Lcom/squareup/notificationcenterdata/Notification;

    if-eqz v3, :cond_8

    invoke-virtual {v3}, Lcom/squareup/notificationcenterdata/Notification;->getCreatedAt()Lorg/threeten/bp/Instant;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 69
    invoke-static {v2}, Lcom/squareup/utilities/threeten/InstantsKt;->asIso8601(Lorg/threeten/bp/Instant;)Ljava/lang/String;

    move-result-object v4

    :cond_8
    if-eqz v4, :cond_9

    goto :goto_3

    :cond_9
    const-string v4, ""

    :goto_3
    move-object v10, v4

    const-string v2, "["

    .line 71
    move-object v3, v2

    check-cast v3, Ljava/lang/CharSequence;

    const-string v2, "]"

    move-object v4, v2

    check-cast v4, Ljava/lang/CharSequence;

    const-string v2, ","

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lcom/squareup/notificationcenter/RealNotificationCenterAnalytics$logTabToggled$ids$1;->INSTANCE:Lcom/squareup/notificationcenter/RealNotificationCenterAnalytics$logTabToggled$ids$1;

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/16 v8, 0x18

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 72
    iget-object v2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 74
    instance-of v3, p1, Lcom/squareup/notificationcenter/NotificationCenterTab$AccountTab;

    if-eqz v3, :cond_a

    new-instance p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;

    .line 76
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    .line 74
    invoke-direct {p1, v0, p2, v1, v10}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;-><init>(ZILjava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;

    goto :goto_4

    .line 80
    :cond_a
    instance-of p1, p1, Lcom/squareup/notificationcenter/NotificationCenterTab$WhatsNewTab;

    if-eqz p1, :cond_b

    new-instance p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$ProductView;

    .line 82
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    .line 80
    invoke-direct {p1, v0, p2, v1, v10}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$ProductView;-><init>(ZILjava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;

    .line 73
    :goto_4
    check-cast p1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 72
    invoke-interface {v2, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void

    .line 80
    :cond_b
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public logWebBrowserDialogCanceled(Lcom/squareup/notificationcenterdata/Notification;)V
    .locals 8

    const-string v0, "notification"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 141
    new-instance v7, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$BrowserDialogCancel;

    .line 142
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getId()Ljava/lang/String;

    move-result-object v2

    .line 143
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getClientActionString(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object v3

    .line 144
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getUrlString(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object v4

    .line 145
    invoke-static {p1}, Lcom/squareup/notificationcenter/RealNotificationCenterAnalyticsKt;->access$getTabString$p(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;

    move-result-object v5

    .line 146
    invoke-static {p1}, Lcom/squareup/notificationcenter/RealNotificationCenterAnalyticsKt;->access$getSourceString$p(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;

    move-result-object v6

    move-object v1, v7

    .line 141
    invoke-direct/range {v1 .. v6}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$BrowserDialogCancel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v7, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 140
    invoke-interface {v0, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logWebBrowserDialogOpened(Lcom/squareup/notificationcenterdata/Notification;)V
    .locals 8

    const-string v0, "notification"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 127
    new-instance v7, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$BrowserDialogOpen;

    .line 128
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getId()Ljava/lang/String;

    move-result-object v2

    .line 129
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getClientActionString(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object v3

    .line 130
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getUrlString(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object v4

    .line 131
    invoke-static {p1}, Lcom/squareup/notificationcenter/RealNotificationCenterAnalyticsKt;->access$getTabString$p(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;

    move-result-object v5

    .line 132
    invoke-static {p1}, Lcom/squareup/notificationcenter/RealNotificationCenterAnalyticsKt;->access$getSourceString$p(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;

    move-result-object v6

    move-object v1, v7

    .line 127
    invoke-direct/range {v1 .. v6}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$BrowserDialogOpen;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v7, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 126
    invoke-interface {v0, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
