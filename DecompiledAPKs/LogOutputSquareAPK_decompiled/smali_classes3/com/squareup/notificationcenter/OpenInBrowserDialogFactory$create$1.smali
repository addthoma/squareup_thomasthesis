.class final Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1;
.super Ljava/lang/Object;
.source "OpenInBrowserDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOpenInBrowserDialogFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OpenInBrowserDialogFactory.kt\ncom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,93:1\n1103#2,7:94\n1103#2,7:101\n*E\n*S KotlinDebug\n*F\n+ 1 OpenInBrowserDialogFactory.kt\ncom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1\n*L\n46#1,7:94\n52#1,7:101\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u001c\u0010\u0003\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/Dialog;",
        "kotlin.jvm.PlatformType",
        "screenWrapper",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;

.field final synthetic this$0:Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory;


# direct methods
.method constructor <init>(Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1;->this$0:Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory;

    iput-object p2, p0, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/Dialog;
    .locals 5

    const-string v0, "screenWrapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    check-cast p1, Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;

    .line 41
    invoke-virtual {p1}, Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;->getNotification()Lcom/squareup/notificationcenterdata/Notification;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 44
    sget v2, Lcom/squareup/notificationcenter/impl/R$layout;->bottom_sheet_dialog_open_in_browser:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 45
    sget v2, Lcom/squareup/notificationcenter/impl/R$id;->open_in_browser_dialog_open_button:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/noho/NohoButton;

    .line 46
    check-cast v2, Landroid/view/View;

    .line 94
    new-instance v4, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1$$special$$inlined$onClickDebounced$1;

    invoke-direct {v4, p1, v0}, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1$$special$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;Lcom/squareup/notificationcenterdata/Notification;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    sget v2, Lcom/squareup/notificationcenter/impl/R$id;->open_in_browser_dialog_cancel_button:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 50
    check-cast v2, Lcom/squareup/noho/NohoButton;

    .line 52
    check-cast v2, Landroid/view/View;

    .line 101
    new-instance v4, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1$$special$$inlined$onClickDebounced$2;

    invoke-direct {v4, p1}, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1$$special$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    sget v2, Lcom/squareup/notificationcenter/impl/R$id;->open_in_browser_dialog_title:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/noho/NohoLabel;

    const-string/jumbo v4, "title"

    .line 57
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/Notification;->getTitle()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 59
    sget v2, Lcom/squareup/notificationcenter/impl/R$id;->open_in_browser_dialog_description:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/noho/NohoLabel;

    .line 60
    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v0

    .line 63
    invoke-static {v0}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getBrowserDialogBody(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_0

    const/4 v3, 0x1

    :cond_0
    if-eqz v3, :cond_1

    const-string v3, "description"

    .line 64
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getBrowserDialogBody(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 66
    :cond_1
    sget v0, Lcom/squareup/notificationcenter/impl/R$string;->open_in_browser_dialog_unhandled_body:I

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoLabel;->setText(I)V

    .line 69
    :goto_0
    iget-object v0, p0, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1;->this$0:Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory;

    invoke-static {v0}, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory;->access$getDevice$p(Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory;)Lcom/squareup/util/Device;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 70
    new-instance v0, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    iget-object v2, p0, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;-><init>(Landroid/content/Context;)V

    .line 71
    invoke-virtual {v0, v1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setContentView(Landroid/view/View;)V

    .line 72
    new-instance v2, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1$$special$$inlined$apply$lambda$1;

    invoke-direct {v2, v1, p1}, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1$$special$$inlined$apply$lambda$1;-><init>(Landroid/view/View;Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;)V

    check-cast v2, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v2}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 77
    sget p1, Lcom/google/android/material/R$id;->design_bottom_sheet:I

    invoke-virtual {v0, p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v1, "findViewById<ViewGroup>(\u2026id.design_bottom_sheet)!!"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    check-cast p1, Landroid/view/ViewGroup;

    .line 78
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->from(Landroid/view/View;)Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    move-result-object p1

    const/4 v1, 0x3

    .line 79
    invoke-virtual {p1, v1}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setState(I)V

    .line 70
    check-cast v0, Landroid/app/Dialog;

    goto :goto_1

    .line 83
    :cond_3
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v2, p0, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 84
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 85
    new-instance v1, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1$4;

    invoke-direct {v1, p1}, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1$4;-><init>(Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;)V

    check-cast v1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 88
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Landroid/app/Dialog;

    :goto_1
    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method
