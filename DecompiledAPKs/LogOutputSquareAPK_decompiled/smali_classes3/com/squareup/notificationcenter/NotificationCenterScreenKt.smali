.class public final Lcom/squareup/notificationcenter/NotificationCenterScreenKt;
.super Ljava/lang/Object;
.source "NotificationCenterScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0000\u001a\u000c\u0010\u0003\u001a\u00020\u0001*\u00020\u0002H\u0000\u00a8\u0006\u0004"
    }
    d2 = {
        "onBackPressed",
        "",
        "Lcom/squareup/notificationcenter/NotificationCenterScreen;",
        "onClosePressed",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final onBackPressed(Lcom/squareup/notificationcenter/NotificationCenterScreen;)V
    .locals 1

    const-string v0, "$this$onBackPressed"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    instance-of v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$LoadingScreen;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$LoadingScreen;

    invoke-virtual {p0}, Lcom/squareup/notificationcenter/NotificationCenterScreen$LoadingScreen;->getOnBackPressed()Lkotlin/jvm/functions/Function0;

    move-result-object p0

    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_0

    .line 67
    :cond_0
    instance-of v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;

    invoke-virtual {p0}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->getOnBackPressed()Lkotlin/jvm/functions/Function0;

    move-result-object p0

    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_0

    .line 68
    :cond_1
    instance-of v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;

    invoke-virtual {p0}, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;->getOnBackPressed()Lkotlin/jvm/functions/Function0;

    move-result-object p0

    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_0

    .line 69
    :cond_2
    instance-of p0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;

    if-eqz p0, :cond_3

    :goto_0
    return-void

    :cond_3
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final onClosePressed(Lcom/squareup/notificationcenter/NotificationCenterScreen;)V
    .locals 1

    const-string v0, "$this$onClosePressed"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    instance-of v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$LoadingScreen;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$LoadingScreen;

    invoke-virtual {p0}, Lcom/squareup/notificationcenter/NotificationCenterScreen$LoadingScreen;->getOnClosePressed()Lkotlin/jvm/functions/Function0;

    move-result-object p0

    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_0

    .line 60
    :cond_0
    instance-of v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;

    invoke-virtual {p0}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->getOnClosePressed()Lkotlin/jvm/functions/Function0;

    move-result-object p0

    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_0

    .line 61
    :cond_1
    instance-of v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;

    invoke-virtual {p0}, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;->getOnClosePressed()Lkotlin/jvm/functions/Function0;

    move-result-object p0

    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_0

    .line 62
    :cond_2
    instance-of p0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;

    if-eqz p0, :cond_3

    :goto_0
    return-void

    :cond_3
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
