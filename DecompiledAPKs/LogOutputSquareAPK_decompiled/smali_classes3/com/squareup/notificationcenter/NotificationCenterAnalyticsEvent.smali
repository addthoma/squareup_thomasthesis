.class abstract Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "RealNotificationCenterAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u00020\u0001:\u0001\u0007B\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u0082\u0001\u0001\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "name",
        "Lcom/squareup/eventstream/v1/EventStream$Name;",
        "value",
        "",
        "(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V",
        "Action",
        "Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V
    .locals 0

    .line 156
    invoke-direct {p0, p1, p2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 153
    invoke-direct {p0, p1, p2}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method
