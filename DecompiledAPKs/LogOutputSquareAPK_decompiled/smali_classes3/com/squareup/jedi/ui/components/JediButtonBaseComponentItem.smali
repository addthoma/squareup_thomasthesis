.class public Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;
.super Lcom/squareup/jedi/ui/JediComponentItem;
.source "JediButtonBaseComponentItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/jedi/service/Component;)V
    .locals 1

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/jedi/ui/JediComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    .line 37
    iget-object p1, p1, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    sget-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->BUTTON_BASE:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/jedi/service/ComponentKind;->equals(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    return-void
.end method


# virtual methods
.method public hasUrl()Z
    .locals 1

    .line 49
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;->url()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public label()Ljava/lang/String;
    .locals 1

    const-string v0, "label"

    .line 41
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;
    .locals 3

    .line 45
    sget-object v0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;->PRIMARY:Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    sget-object v1, Lcom/squareup/jedi/ui/components/-$$Lambda$SOaXbyf6XPXcoXdTczaOz-_V1tw;->INSTANCE:Lcom/squareup/jedi/ui/components/-$$Lambda$SOaXbyf6XPXcoXdTczaOz-_V1tw;

    const-string/jumbo v2, "type"

    invoke-virtual {p0, v2, v0, v1}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;->getParameterOrDefault(Ljava/lang/String;Ljava/lang/Object;Lrx/functions/Func1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    return-object v0
.end method

.method public url()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "url"

    .line 53
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public validate()Z
    .locals 2

    const/4 v0, 0x0

    .line 57
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string/jumbo v1, "validate"

    invoke-virtual {p0, v1, v0}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;->getBooleanParameterOrDefault(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
