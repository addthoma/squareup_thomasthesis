.class public final enum Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;
.super Ljava/lang/Enum;
.source "JediIconComponentItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/jedi/ui/components/JediIconComponentItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IconType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

.field public static final enum SUCCESSFUL_SUBMISSION:Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 13
    new-instance v0, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    const/4 v1, 0x0

    const-string v2, "SUCCESSFUL_SUBMISSION"

    const-string v3, "successful-submission"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;->SUCCESSFUL_SUBMISSION:Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    .line 12
    sget-object v2, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;->SUCCESSFUL_SUBMISSION:Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;->$VALUES:[Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput-object p3, p0, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;->value:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;
    .locals 0

    .line 12
    invoke-static {p0}, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;->fromValue(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    move-result-object p0

    return-object p0
.end method

.method private static fromValue(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;
    .locals 5

    .line 23
    invoke-static {}, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;->values()[Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 24
    iget-object v4, v3, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;
    .locals 1

    .line 12
    const-class v0, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;->$VALUES:[Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    invoke-virtual {v0}, [Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/jedi/ui/components/JediIconComponentItem$IconType;

    return-object v0
.end method
