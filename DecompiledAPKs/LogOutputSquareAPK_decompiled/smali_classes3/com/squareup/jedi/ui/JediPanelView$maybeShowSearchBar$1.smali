.class public final Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$1;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "JediPanelView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/jedi/ui/JediPanelView;->maybeShowSearchBar(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000#\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\"\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH\u0016\u00a8\u0006\n"
    }
    d2 = {
        "com/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$1",
        "Lcom/squareup/debounce/DebouncedOnEditorActionListener;",
        "doOnEditorAction",
        "",
        "view",
        "Landroid/widget/TextView;",
        "actionId",
        "",
        "keyEvent",
        "Landroid/view/KeyEvent;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/jedi/ui/JediPanelView;


# direct methods
.method constructor <init>(Lcom/squareup/jedi/ui/JediPanelView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 157
    iput-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x3

    if-eq p2, p1, :cond_1

    .line 163
    invoke-static {p3}, Lcom/squareup/text/KeyEvents;->isKeyboardEnterPress(Landroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    .line 164
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-static {p1}, Lcom/squareup/jedi/ui/JediPanelView;->access$getJediSearchBar$p(Lcom/squareup/jedi/ui/JediPanelView;)Lcom/squareup/ui/XableEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 165
    move-object p2, p1

    check-cast p2, Ljava/lang/CharSequence;

    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_2

    .line 166
    iget-object p2, p0, Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-static {p2}, Lcom/squareup/jedi/ui/JediPanelView;->access$getSearchTextRelay$p(Lcom/squareup/jedi/ui/JediPanelView;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 168
    :cond_2
    iget-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$1;->this$0:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-static {p1}, Lcom/squareup/jedi/ui/JediPanelView;->access$getJediSearchBar$p(Lcom/squareup/jedi/ui/JediPanelView;)Lcom/squareup/ui/XableEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/SelectableEditText;

    const-string p2, "jediSearchBar.editText"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    const/4 p1, 0x1

    return p1
.end method
