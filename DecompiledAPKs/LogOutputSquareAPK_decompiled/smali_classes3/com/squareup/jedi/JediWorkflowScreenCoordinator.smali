.class public final Lcom/squareup/jedi/JediWorkflowScreenCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "JediWorkflowScreenCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nJediWorkflowScreenCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 JediWorkflowScreenCoordinator.kt\ncom/squareup/jedi/JediWorkflowScreenCoordinator\n*L\n1#1,142:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001BA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\"\u0010\u0008\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\nj\u0008\u0012\u0004\u0012\u00020\u000b`\r0\t\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0018\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u000bH\u0002J\u0008\u0010\u001b\u001a\u00020\u001cH\u0002J\u0010\u0010\u001d\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u000bH\u0002J\u0010\u0010\u001e\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u000bH\u0002R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0008\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\nj\u0008\u0012\u0004\u0012\u00020\u000b`\r0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/jedi/JediWorkflowScreenCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "messagingController",
        "Lcom/squareup/ui/help/MessagingController;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "device",
        "Lcom/squareup/util/Device;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/jedi/JediWorkflowScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lcom/squareup/ui/help/MessagingController;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/Device;Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "jediPanelView",
        "Lcom/squareup/jedi/ui/JediPanelView;",
        "speechBubbleImageView",
        "Lcom/squareup/jedi/SpeechBubbleImageView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "onScreen",
        "screen",
        "openMessaging",
        "Lio/reactivex/disposables/Disposable;",
        "updateActionBar",
        "updateJediPanelView",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final device:Lcom/squareup/util/Device;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

.field private final messagingController:Lcom/squareup/ui/help/MessagingController;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private speechBubbleImageView:Lcom/squareup/jedi/SpeechBubbleImageView;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/MessagingController;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/Device;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/help/MessagingController;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            "Lcom/squareup/util/Device;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "messagingController"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->messagingController:Lcom/squareup/ui/help/MessagingController;

    iput-object p2, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p3, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->device:Lcom/squareup/util/Device;

    iput-object p4, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getGlassSpinner$p(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;)Lcom/squareup/register/widgets/GlassSpinner;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-object p0
.end method

.method public static final synthetic access$getMessagingController$p(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;)Lcom/squareup/ui/help/MessagingController;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->messagingController:Lcom/squareup/ui/help/MessagingController;

    return-object p0
.end method

.method public static final synthetic access$getSpeechBubbleImageView$p(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;)Lcom/squareup/jedi/SpeechBubbleImageView;
    .locals 1

    .line 30
    iget-object p0, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->speechBubbleImageView:Lcom/squareup/jedi/SpeechBubbleImageView;

    if-nez p0, :cond_0

    const-string v0, "speechBubbleImageView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;Landroid/view/View;Lcom/squareup/jedi/JediWorkflowScreen;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->onScreen(Landroid/view/View;Lcom/squareup/jedi/JediWorkflowScreen;)V

    return-void
.end method

.method public static final synthetic access$openMessaging(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;)Lio/reactivex/disposables/Disposable;
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->openMessaging()Lio/reactivex/disposables/Disposable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setSpeechBubbleImageView$p(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;Lcom/squareup/jedi/SpeechBubbleImageView;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->speechBubbleImageView:Lcom/squareup/jedi/SpeechBubbleImageView;

    return-void
.end method

.method private final onScreen(Landroid/view/View;Lcom/squareup/jedi/JediWorkflowScreen;)V
    .locals 1

    .line 82
    new-instance v0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$onScreen$1;

    invoke-direct {v0, p2}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$onScreen$1;-><init>(Lcom/squareup/jedi/JediWorkflowScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 83
    invoke-direct {p0, p2}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->updateActionBar(Lcom/squareup/jedi/JediWorkflowScreen;)V

    .line 84
    invoke-direct {p0, p2}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->updateJediPanelView(Lcom/squareup/jedi/JediWorkflowScreen;)V

    .line 87
    new-instance v0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$onScreen$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$onScreen$2;-><init>(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;Landroid/view/View;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p2, v0}, Lcom/squareup/jedi/JediWorkflowScreen;->setOnMessagingTapped(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final openMessaging()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 98
    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->messagingController:Lcom/squareup/ui/help/MessagingController;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/squareup/ui/help/MessagingController;->messagingViewState(Z)Lio/reactivex/Observable;

    move-result-object v0

    .line 99
    new-instance v1, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$openMessaging$1;

    invoke-direct {v1, p0}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$openMessaging$1;-><init>(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 102
    iget-object v1, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    sget-object v2, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$openMessaging$2;->INSTANCE:Lcom/squareup/jedi/JediWorkflowScreenCoordinator$openMessaging$2;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object v1

    const-string v2, "glassSpinner.spinnerTran\u2026nner(false)\n            }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    sget-object v2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Transformer(Lrx/Observable$Transformer;Lio/reactivex/BackpressureStrategy;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    .line 100
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 106
    new-instance v1, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$openMessaging$3;

    invoke-direct {v1, p0}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$openMessaging$3;-><init>(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "messagingController.mess\u2026enMessagingActivity(it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final updateActionBar(Lcom/squareup/jedi/JediWorkflowScreen;)V
    .locals 4

    .line 130
    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 110
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 114
    iget-object v2, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v2}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/squareup/jedi/JediWorkflowScreen;->getCanGoBack()Z

    move-result v2

    if-nez v2, :cond_1

    .line 115
    invoke-virtual {p1}, Lcom/squareup/jedi/JediWorkflowScreen;->getActionBarHeader()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setTitleNoUpButton(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    .line 117
    :cond_1
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1}, Lcom/squareup/jedi/JediWorkflowScreen;->getActionBarHeader()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    const/4 v3, 0x1

    .line 118
    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 119
    new-instance v3, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$updateActionBar$$inlined$apply$lambda$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$updateActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;Lcom/squareup/jedi/JediWorkflowScreen;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 124
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/jedi/JediWorkflowScreen;->getFirstScreen()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 123
    invoke-virtual {p1}, Lcom/squareup/jedi/JediWorkflowScreen;->getScreenData()Lcom/squareup/jedi/JediHelpScreenData;

    move-result-object v2

    instance-of v2, v2, Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;

    if-nez v2, :cond_2

    .line 124
    iget-object v2, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->messagingController:Lcom/squareup/ui/help/MessagingController;

    invoke-interface {v2}, Lcom/squareup/ui/help/MessagingController;->showMessagingIcon()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 126
    new-instance v2, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$updateActionBar$$inlined$apply$lambda$2;

    invoke-direct {v2, p0, p1}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$updateActionBar$$inlined$apply$lambda$2;-><init>(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;Lcom/squareup/jedi/JediWorkflowScreen;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showCustomView(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 127
    new-instance v3, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$updateActionBar$$inlined$apply$lambda$3;

    invoke-direct {v3, p0, p1}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$updateActionBar$$inlined$apply$lambda$3;-><init>(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;Lcom/squareup/jedi/JediWorkflowScreen;)V

    check-cast v3, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;

    const/4 p1, 0x0

    invoke-virtual {v2, v3, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setCustomView(Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 130
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final updateJediPanelView(Lcom/squareup/jedi/JediWorkflowScreen;)V
    .locals 3

    .line 134
    invoke-virtual {p1}, Lcom/squareup/jedi/JediWorkflowScreen;->getLoading()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    new-instance v0, Lcom/squareup/jedi/JediComponentInputHandler$None;

    invoke-direct {v0}, Lcom/squareup/jedi/JediComponentInputHandler$None;-><init>()V

    check-cast v0, Lcom/squareup/jedi/JediComponentInputHandler;

    goto :goto_0

    .line 137
    :cond_0
    new-instance v0, Lcom/squareup/jedi/RealJediComponentInputHandler;

    invoke-direct {v0, p1}, Lcom/squareup/jedi/RealJediComponentInputHandler;-><init>(Lcom/squareup/jedi/JediWorkflowScreen;)V

    check-cast v0, Lcom/squareup/jedi/JediComponentInputHandler;

    .line 139
    :goto_0
    iget-object v1, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

    if-nez v1, :cond_1

    const-string v2, "jediPanelView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/jedi/JediWorkflowScreen;->getScreenData()Lcom/squareup/jedi/JediHelpScreenData;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Lcom/squareup/jedi/ui/JediPanelView;->update(Lcom/squareup/jedi/JediHelpScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 42
    sget v0, Lcom/squareup/jedi/impl/R$id;->jedi_panel:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/jedi/ui/JediPanelView;

    iput-object v0, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

    .line 43
    new-instance v0, Lcom/squareup/jedi/SpeechBubbleImageView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "view.context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string/jumbo v3, "view.resources"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcom/squareup/jedi/SpeechBubbleImageView;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->speechBubbleImageView:Lcom/squareup/jedi/SpeechBubbleImageView;

    .line 45
    new-instance v0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$1;-><init>(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;Landroid/view/View;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->onDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->screens:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$latestScreen$1;->INSTANCE:Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$latestScreen$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "latestScreen"

    .line 49
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance v1, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$2;-><init>(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 53
    new-instance v1, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$3;-><init>(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 59
    iget-object v1, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

    const-string v2, "jediPanelView"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/jedi/ui/JediPanelView;->getSearchText()Lio/reactivex/Observable;

    move-result-object v1

    .line 60
    check-cast v0, Lio/reactivex/ObservableSource;

    invoke-static {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v1

    .line 61
    sget-object v3, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$4;->INSTANCE:Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$4;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, p1, v3}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 66
    iget-object v1, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

    if-nez v1, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Lcom/squareup/jedi/ui/JediPanelView;->getSearchClear()Lio/reactivex/Observable;

    move-result-object v1

    .line 67
    invoke-static {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 68
    sget-object v1, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$5;->INSTANCE:Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$5;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 73
    new-instance v0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$6;

    invoke-direct {v0, p0}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$6;-><init>(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
