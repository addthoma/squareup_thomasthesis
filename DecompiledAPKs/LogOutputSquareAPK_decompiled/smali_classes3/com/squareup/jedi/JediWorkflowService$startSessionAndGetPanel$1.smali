.class final Lcom/squareup/jedi/JediWorkflowService$startSessionAndGetPanel$1;
.super Ljava/lang/Object;
.source "JediWorkflowService.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/jedi/JediWorkflowService;->startSessionAndGetPanel(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nJediWorkflowService.kt\nKotlin\n*S Kotlin\n*F\n+ 1 JediWorkflowService.kt\ncom/squareup/jedi/JediWorkflowService$startSessionAndGetPanel$1\n*L\n1#1,208:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/protos/jedi/service/PanelResponse;",
        "sessionToken",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $panelToken:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/jedi/JediWorkflowService;


# direct methods
.method constructor <init>(Lcom/squareup/jedi/JediWorkflowService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/jedi/JediWorkflowService$startSessionAndGetPanel$1;->this$0:Lcom/squareup/jedi/JediWorkflowService;

    iput-object p2, p0, Lcom/squareup/jedi/JediWorkflowService$startSessionAndGetPanel$1;->$panelToken:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/protos/jedi/service/PanelResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowService$startSessionAndGetPanel$1;->this$0:Lcom/squareup/jedi/JediWorkflowService;

    .line 46
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 47
    iget-object v2, p0, Lcom/squareup/jedi/JediWorkflowService$startSessionAndGetPanel$1;->$panelToken:Ljava/lang/String;

    const/4 v3, 0x0

    .line 43
    invoke-static {v0, p1, v3, v1, v2}, Lcom/squareup/jedi/JediWorkflowService;->access$getPanelForSession(Lcom/squareup/jedi/JediWorkflowService;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/jedi/JediWorkflowService$startSessionAndGetPanel$1;->apply(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
