.class public final Lcom/squareup/money/MoneyLocaleHelper_Factory;
.super Ljava/lang/Object;
.source "MoneyLocaleHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/money/MoneyLocaleHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleDigitsKeyListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleScrubberProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/SelectableTextScrubber;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/SelectableTextScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/money/MoneyLocaleHelper_Factory;->moneyLocaleScrubberProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/money/MoneyLocaleHelper_Factory;->moneyLocaleDigitsKeyListenerProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/money/MoneyLocaleHelper_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/MoneyLocaleHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/SelectableTextScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/money/MoneyLocaleHelper_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/money/MoneyLocaleHelper_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/money/MoneyLocaleHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/money/MoneyLocaleHelper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/SelectableTextScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/squareup/money/MoneyLocaleHelper;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/money/MoneyLocaleHelper;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/money/MoneyLocaleHelper;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/money/MoneyLocaleHelper;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/money/MoneyLocaleHelper_Factory;->moneyLocaleScrubberProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/money/MoneyLocaleHelper_Factory;->moneyLocaleDigitsKeyListenerProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/money/MoneyLocaleHelper_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyLocaleHelper_Factory;->newInstance(Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/money/MoneyLocaleHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/money/MoneyLocaleHelper_Factory;->get()Lcom/squareup/money/MoneyLocaleHelper;

    move-result-object v0

    return-object v0
.end method
