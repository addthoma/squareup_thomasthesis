.class public final Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactory;
.super Ljava/lang/Object;
.source "CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Landroid/text/method/DigitsKeyListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideMoneyDigitsKeyListener(Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;)Landroid/text/method/DigitsKeyListener;
    .locals 0

    .line 42
    invoke-static {p0, p1}, Lcom/squareup/money/CurrencyMoneyModule;->provideMoneyDigitsKeyListener(Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;)Landroid/text/method/DigitsKeyListener;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/text/method/DigitsKeyListener;

    return-object p0
.end method


# virtual methods
.method public get()Landroid/text/method/DigitsKeyListener;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, p0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-static {v0, v1}, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactory;->provideMoneyDigitsKeyListener(Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;)Landroid/text/method/DigitsKeyListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactory;->get()Landroid/text/method/DigitsKeyListener;

    move-result-object v0

    return-object v0
.end method
