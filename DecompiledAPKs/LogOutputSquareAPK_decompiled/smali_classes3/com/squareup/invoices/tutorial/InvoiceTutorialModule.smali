.class public abstract Lcom/squareup/invoices/tutorial/InvoiceTutorialModule;
.super Ljava/lang/Object;
.source "InvoiceTutorialModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideNewInvoiceFeaturesTutorialRunner(Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;)Lcom/squareup/invoicesappletapi/NewInvoiceFeaturesTutorialRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
