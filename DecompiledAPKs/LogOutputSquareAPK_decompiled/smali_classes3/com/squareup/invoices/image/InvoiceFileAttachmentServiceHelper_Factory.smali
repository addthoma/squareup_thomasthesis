.class public final Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper_Factory;
.super Ljava/lang/Object;
.source "InvoiceFileAttachmentServiceHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final serviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/invoices/InvoiceFileAttachmentService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/invoices/InvoiceFileAttachmentService;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper_Factory;->serviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/invoices/InvoiceFileAttachmentService;",
            ">;)",
            "Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/invoices/InvoiceFileAttachmentService;)Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;-><init>(Lcom/squareup/server/invoices/InvoiceFileAttachmentService;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper_Factory;->serviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/invoices/InvoiceFileAttachmentService;

    invoke-static {v0}, Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper_Factory;->newInstance(Lcom/squareup/server/invoices/InvoiceFileAttachmentService;)Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper_Factory;->get()Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;

    move-result-object v0

    return-object v0
.end method
