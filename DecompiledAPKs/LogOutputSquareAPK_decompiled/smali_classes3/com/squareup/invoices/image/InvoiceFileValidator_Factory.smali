.class public final Lcom/squareup/invoices/image/InvoiceFileValidator_Factory;
.super Ljava/lang/Object;
.source "InvoiceFileValidator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/image/InvoiceFileValidator;",
        ">;"
    }
.end annotation


# instance fields
.field private final invoiceUnitCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/invoices/image/InvoiceFileValidator_Factory;->invoiceUnitCacheProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/invoices/image/InvoiceFileValidator_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/image/InvoiceFileValidator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/invoices/image/InvoiceFileValidator_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/invoices/image/InvoiceFileValidator_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/image/InvoiceFileValidator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/util/Res;)Lcom/squareup/invoices/image/InvoiceFileValidator;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/invoices/image/InvoiceFileValidator;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/image/InvoiceFileValidator;-><init>(Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/image/InvoiceFileValidator;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileValidator_Factory;->invoiceUnitCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    iget-object v1, p0, Lcom/squareup/invoices/image/InvoiceFileValidator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lcom/squareup/invoices/image/InvoiceFileValidator_Factory;->newInstance(Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/util/Res;)Lcom/squareup/invoices/image/InvoiceFileValidator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/invoices/image/InvoiceFileValidator_Factory;->get()Lcom/squareup/invoices/image/InvoiceFileValidator;

    move-result-object v0

    return-object v0
.end method
