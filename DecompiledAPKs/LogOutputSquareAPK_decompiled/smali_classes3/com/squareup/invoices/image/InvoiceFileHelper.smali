.class public final Lcom/squareup/invoices/image/InvoiceFileHelper;
.super Ljava/lang/Object;
.source "InvoiceFileHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/image/InvoiceFileHelper$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceFileHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceFileHelper.kt\ncom/squareup/invoices/image/InvoiceFileHelper\n*L\n1#1,222:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 /2\u00020\u0001:\u0001/B[\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0008\u0001\u0010\r\u001a\u00020\u000c\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\"\u0010\u0013\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00150\u00142\u0006\u0010\u0016\u001a\u00020\u00082\u0006\u0010\u0017\u001a\u00020\u0018J\"\u0010\u0019\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00150\u00142\u0006\u0010\u001a\u001a\u00020\u00082\u0006\u0010\u001b\u001a\u00020\u0018J,\u0010\u001c\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00150\u00142\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u00182\u0008\u0008\u0002\u0010 \u001a\u00020!J\u000e\u0010\"\u001a\u00020#2\u0006\u0010\u0016\u001a\u00020\u0008J \u0010$\u001a\u00020\u00182\u000e\u0010%\u001a\n\u0012\u0004\u0012\u00020\'\u0018\u00010&2\u0008\u0008\u0001\u0010(\u001a\u00020)J\u000e\u0010*\u001a\u00020\u00182\u0006\u0010+\u001a\u00020\'J\u0014\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00142\u0006\u0010-\u001a\u00020.R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/invoices/image/InvoiceFileHelper;",
        "",
        "fileThreadScheduler",
        "Lio/reactivex/Scheduler;",
        "fileExecutor",
        "Ljava/util/concurrent/Executor;",
        "mainScheduler",
        "tempPhotoDir",
        "Ljava/io/File;",
        "res",
        "Lcom/squareup/util/Res;",
        "dateFormatter",
        "Ljava/text/DateFormat;",
        "timeFormatter",
        "locale",
        "Ljava/util/Locale;",
        "application",
        "Landroid/app/Application;",
        "(Lio/reactivex/Scheduler;Ljava/util/concurrent/Executor;Lio/reactivex/Scheduler;Ljava/io/File;Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Landroid/app/Application;)V",
        "copy",
        "Lio/reactivex/Single;",
        "Lcom/squareup/util/Optional;",
        "file",
        "name",
        "",
        "copyToExternalFile",
        "fromFile",
        "targetFileName",
        "createFileFromUri",
        "uri",
        "Landroid/net/Uri;",
        "extension",
        "createFileInExternalDir",
        "",
        "delete",
        "",
        "nextFileAttachmentDefaultTitle",
        "attachmentList",
        "",
        "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
        "defaultPhraseId",
        "",
        "uploadedAtAsStringOrEmpty",
        "fileAttachmentMetadata",
        "writeResponseBodyToFile",
        "responseBody",
        "Lokhttp3/ResponseBody;",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/image/InvoiceFileHelper$Companion;


# instance fields
.field private final application:Landroid/app/Application;

.field private final dateFormatter:Ljava/text/DateFormat;

.field private final fileExecutor:Ljava/util/concurrent/Executor;

.field private final fileThreadScheduler:Lio/reactivex/Scheduler;

.field private final locale:Ljava/util/Locale;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final res:Lcom/squareup/util/Res;

.field private final tempPhotoDir:Ljava/io/File;

.field private final timeFormatter:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/image/InvoiceFileHelper$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/image/InvoiceFileHelper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/image/InvoiceFileHelper;->Companion:Lcom/squareup/invoices/image/InvoiceFileHelper$Companion;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/Scheduler;Ljava/util/concurrent/Executor;Lio/reactivex/Scheduler;Ljava/io/File;Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Landroid/app/Application;)V
    .locals 1
    .param p1    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p4    # Ljava/io/File;
        .annotation runtime Lcom/squareup/util/TempPhotoDir;
        .end annotation
    .end param
    .param p6    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/ShortForm;
        .end annotation
    .end param
    .param p7    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/TimeFormat;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "fileThreadScheduler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileExecutor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tempPhotoDir"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormatter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "timeFormatter"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "application"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->fileThreadScheduler:Lio/reactivex/Scheduler;

    iput-object p2, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->fileExecutor:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p4, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->tempPhotoDir:Ljava/io/File;

    iput-object p5, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->res:Lcom/squareup/util/Res;

    iput-object p6, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->dateFormatter:Ljava/text/DateFormat;

    iput-object p7, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->timeFormatter:Ljava/text/DateFormat;

    iput-object p8, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->locale:Ljava/util/Locale;

    iput-object p9, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->application:Landroid/app/Application;

    return-void
.end method

.method public static final synthetic access$getApplication$p(Lcom/squareup/invoices/image/InvoiceFileHelper;)Landroid/app/Application;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->application:Landroid/app/Application;

    return-object p0
.end method

.method public static final synthetic access$getTempPhotoDir$p(Lcom/squareup/invoices/image/InvoiceFileHelper;)Ljava/io/File;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->tempPhotoDir:Ljava/io/File;

    return-object p0
.end method

.method public static synthetic createFileFromUri$default(Lcom/squareup/invoices/image/InvoiceFileHelper;Landroid/net/Uri;Ljava/lang/String;ZILjava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 77
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/image/InvoiceFileHelper;->createFileFromUri(Landroid/net/Uri;Ljava/lang/String;Z)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Ljava/io/File;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation

    const-string v0, "file"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    new-instance v0, Lcom/squareup/invoices/image/InvoiceFileHelper$copy$1;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/invoices/image/InvoiceFileHelper$copy$1;-><init>(Lcom/squareup/invoices/image/InvoiceFileHelper;Ljava/lang/String;Ljava/io/File;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p1

    .line 60
    sget-object p2, Lcom/squareup/invoices/image/InvoiceFileHelper$copy$2;->INSTANCE:Lcom/squareup/invoices/image/InvoiceFileHelper$copy$2;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 61
    iget-object p2, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->fileThreadScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 62
    iget-object p2, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.fromCallable {\n  \u2026.observeOn(mainScheduler)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final copyToExternalFile(Ljava/io/File;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation

    const-string v0, "fromFile"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "targetFileName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    new-instance v0, Lcom/squareup/invoices/image/InvoiceFileHelper$copyToExternalFile$1;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/invoices/image/InvoiceFileHelper$copyToExternalFile$1;-><init>(Lcom/squareup/invoices/image/InvoiceFileHelper;Ljava/lang/String;Ljava/io/File;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p1

    .line 149
    sget-object p2, Lcom/squareup/invoices/image/InvoiceFileHelper$copyToExternalFile$2;->INSTANCE:Lcom/squareup/invoices/image/InvoiceFileHelper$copyToExternalFile$2;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 150
    iget-object p2, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->fileThreadScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 151
    iget-object p2, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.fromCallable {\n  \u2026.observeOn(mainScheduler)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final createFileFromUri(Landroid/net/Uri;Ljava/lang/String;Z)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "extension"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    new-instance v0, Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1;

    invoke-direct {v0, p0, p3, p2, p1}, Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1;-><init>(Lcom/squareup/invoices/image/InvoiceFileHelper;ZLjava/lang/String;Landroid/net/Uri;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p1

    .line 102
    sget-object p2, Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$2;->INSTANCE:Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$2;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 103
    iget-object p2, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->fileThreadScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 104
    iget-object p2, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.fromCallable {\n  \u2026.observeOn(mainScheduler)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final delete(Ljava/io/File;)V
    .locals 1

    const-string v0, "file"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->fileExecutor:Ljava/util/concurrent/Executor;

    invoke-static {p1, v0}, Lcom/squareup/util/Files;->deleteSilently(Ljava/io/File;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public final nextFileAttachmentDefaultTitle(Ljava/util/List;I)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 162
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    if-eqz p1, :cond_0

    .line 163
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    add-int/lit8 p1, p1, 0x1

    const-string v0, "number"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 164
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 165
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final uploadedAtAsStringOrEmpty(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)Ljava/lang/String;
    .locals 3

    const-string v0, "fileAttachmentMetadata"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->uploaded_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz p1, :cond_0

    .line 174
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->locale:Ljava/util/Locale;

    invoke-static {p1, v0}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    .line 175
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->uploaded_at_time_stamp:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 176
    iget-object v1, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->dateFormatter:Ljava/text/DateFormat;

    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "date"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 177
    iget-object v1, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->timeFormatter:Ljava/text/DateFormat;

    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string/jumbo v1, "time"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 178
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 179
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    return-object p1
.end method

.method public final writeResponseBodyToFile(Lokhttp3/ResponseBody;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/ResponseBody;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    const-string v0, "responseBody"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    new-instance v0, Lcom/squareup/invoices/image/InvoiceFileHelper$writeResponseBodyToFile$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/image/InvoiceFileHelper$writeResponseBodyToFile$1;-><init>(Lcom/squareup/invoices/image/InvoiceFileHelper;Lokhttp3/ResponseBody;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p1

    .line 124
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->fileThreadScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 125
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileHelper;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.fromCallable<File\u2026.observeOn(mainScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
