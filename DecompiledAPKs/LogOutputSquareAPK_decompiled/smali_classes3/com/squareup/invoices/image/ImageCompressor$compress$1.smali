.class final Lcom/squareup/invoices/image/ImageCompressor$compress$1;
.super Ljava/lang/Object;
.source "ImageCompressor.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/ImageCompressor;->compress(Landroid/net/Uri;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/invoices/image/CompressionResult$Success;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $uri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/squareup/invoices/image/ImageCompressor;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/ImageCompressor;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/ImageCompressor$compress$1;->this$0:Lcom/squareup/invoices/image/ImageCompressor;

    iput-object p2, p0, Lcom/squareup/invoices/image/ImageCompressor$compress$1;->$uri:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Lcom/squareup/invoices/image/CompressionResult$Success;
    .locals 3

    .line 39
    new-instance v0, Lcom/squareup/invoices/image/CompressionResult$Success;

    iget-object v1, p0, Lcom/squareup/invoices/image/ImageCompressor$compress$1;->this$0:Lcom/squareup/invoices/image/ImageCompressor;

    iget-object v2, p0, Lcom/squareup/invoices/image/ImageCompressor$compress$1;->$uri:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/squareup/invoices/image/ImageCompressor;->access$compressToFile(Lcom/squareup/invoices/image/ImageCompressor;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/invoices/image/CompressionResult$Success;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/invoices/image/ImageCompressor$compress$1;->call()Lcom/squareup/invoices/image/CompressionResult$Success;

    move-result-object v0

    return-object v0
.end method
