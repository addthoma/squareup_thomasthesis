.class final Lcom/squareup/invoices/image/ImageUploader$uploadUri$2;
.super Ljava/lang/Object;
.source "ImageUploader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/ImageUploader;->uploadUri(Landroid/net/Uri;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/invoices/image/FileUploadResult;",
        "kotlin.jvm.PlatformType",
        "it",
        "Lcom/squareup/util/Optional;",
        "Ljava/io/File;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $fileMetadata:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

.field final synthetic $invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

.field final synthetic $validationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

.field final synthetic this$0:Lcom/squareup/invoices/image/ImageUploader;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/ImageUploader$uploadUri$2;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    iput-object p2, p0, Lcom/squareup/invoices/image/ImageUploader$uploadUri$2;->$invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    iput-object p3, p0, Lcom/squareup/invoices/image/ImageUploader$uploadUri$2;->$fileMetadata:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    iput-object p4, p0, Lcom/squareup/invoices/image/ImageUploader$uploadUri$2;->$validationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/util/Optional;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "+",
            "Ljava/io/File;",
            ">;)",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/invoices/image/FileUploadResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader$uploadUri$2;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    .line 70
    iget-object v1, p0, Lcom/squareup/invoices/image/ImageUploader$uploadUri$2;->$invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    .line 71
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/io/File;

    .line 72
    iget-object v2, p0, Lcom/squareup/invoices/image/ImageUploader$uploadUri$2;->$fileMetadata:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    invoke-virtual {v2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 73
    iget-object v3, p0, Lcom/squareup/invoices/image/ImageUploader$uploadUri$2;->$validationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    .line 69
    invoke-virtual {v0, p1, v2, v1, v3}, Lcom/squareup/invoices/image/ImageUploader;->uploadFile(Ljava/io/File;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 75
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/image/ImageUploader$uploadUri$2;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    invoke-static {p1}, Lcom/squareup/invoices/image/ImageUploader;->access$defaultFailureResult(Lcom/squareup/invoices/image/ImageUploader;)Lcom/squareup/invoices/image/FileUploadResult$Failure;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(defaultFailureResult())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/util/Optional;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/image/ImageUploader$uploadUri$2;->apply(Lcom/squareup/util/Optional;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
