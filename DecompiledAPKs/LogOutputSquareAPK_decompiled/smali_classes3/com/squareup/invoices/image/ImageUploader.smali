.class public final Lcom/squareup/invoices/image/ImageUploader;
.super Ljava/lang/Object;
.source "ImageUploader.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0008\u0010\u000f\u001a\u00020\u0010H\u0002J,\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00122\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bJ\u001e\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00122\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J$\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00122\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001e2\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J&\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00122\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J,\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00122\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bJ\u000c\u0010$\u001a\u00020\u0010*\u00020%H\u0002J\u0012\u0010$\u001a\u00020\u0010*\u0008\u0012\u0004\u0012\u00020\'0&H\u0002J\"\u0010(\u001a\u00020)*\u0008\u0012\u0004\u0012\u00020\'0*2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0014\u001a\u00020\u0015H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/invoices/image/ImageUploader;",
        "",
        "invoiceFileHelper",
        "Lcom/squareup/invoices/image/InvoiceFileHelper;",
        "failureMessageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "imageCompressor",
        "Lcom/squareup/invoices/image/ImageCompressor;",
        "fileAttachmentServiceHelper",
        "Lcom/squareup/invoices/image/FileAttachmentServiceHelper;",
        "fileValidator",
        "Lcom/squareup/invoices/image/InvoiceFileValidator;",
        "(Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/Res;Lcom/squareup/invoices/image/ImageCompressor;Lcom/squareup/invoices/image/FileAttachmentServiceHelper;Lcom/squareup/invoices/image/InvoiceFileValidator;)V",
        "defaultFailureResult",
        "Lcom/squareup/invoices/image/FileUploadResult$Failure;",
        "uploadFile",
        "Lio/reactivex/Single;",
        "Lcom/squareup/invoices/image/FileUploadResult;",
        "file",
        "Ljava/io/File;",
        "fileName",
        "",
        "invoiceTokenType",
        "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
        "validationInfo",
        "Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;",
        "uploadFileToInvoice",
        "optionalFile",
        "Lcom/squareup/util/Optional;",
        "uploadUri",
        "uri",
        "Landroid/net/Uri;",
        "fileMetadata",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;",
        "toUploadFailureResult",
        "Lcom/squareup/invoices/image/FileValidationResult$Failure;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
        "toUploadSuccessResult",
        "Lcom/squareup/invoices/image/FileUploadResult$Success;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final fileAttachmentServiceHelper:Lcom/squareup/invoices/image/FileAttachmentServiceHelper;

.field private final fileValidator:Lcom/squareup/invoices/image/InvoiceFileValidator;

.field private final imageCompressor:Lcom/squareup/invoices/image/ImageCompressor;

.field private final invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/Res;Lcom/squareup/invoices/image/ImageCompressor;Lcom/squareup/invoices/image/FileAttachmentServiceHelper;Lcom/squareup/invoices/image/InvoiceFileValidator;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "invoiceFileHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessageFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "imageCompressor"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileAttachmentServiceHelper"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileValidator"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/image/ImageUploader;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    iput-object p2, p0, Lcom/squareup/invoices/image/ImageUploader;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p3, p0, Lcom/squareup/invoices/image/ImageUploader;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/invoices/image/ImageUploader;->imageCompressor:Lcom/squareup/invoices/image/ImageCompressor;

    iput-object p5, p0, Lcom/squareup/invoices/image/ImageUploader;->fileAttachmentServiceHelper:Lcom/squareup/invoices/image/FileAttachmentServiceHelper;

    iput-object p6, p0, Lcom/squareup/invoices/image/ImageUploader;->fileValidator:Lcom/squareup/invoices/image/InvoiceFileValidator;

    return-void
.end method

.method public static final synthetic access$defaultFailureResult(Lcom/squareup/invoices/image/ImageUploader;)Lcom/squareup/invoices/image/FileUploadResult$Failure;
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/invoices/image/ImageUploader;->defaultFailureResult()Lcom/squareup/invoices/image/FileUploadResult$Failure;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getInvoiceFileHelper$p(Lcom/squareup/invoices/image/ImageUploader;)Lcom/squareup/invoices/image/InvoiceFileHelper;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/invoices/image/ImageUploader;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/invoices/image/ImageUploader;)Lcom/squareup/util/Res;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/invoices/image/ImageUploader;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$toUploadFailureResult(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/invoices/image/FileValidationResult$Failure;)Lcom/squareup/invoices/image/FileUploadResult$Failure;
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/invoices/image/ImageUploader;->toUploadFailureResult(Lcom/squareup/invoices/image/FileValidationResult$Failure;)Lcom/squareup/invoices/image/FileUploadResult$Failure;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toUploadFailureResult(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/image/FileUploadResult$Failure;
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/invoices/image/ImageUploader;->toUploadFailureResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/image/FileUploadResult$Failure;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toUploadSuccessResult(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/io/File;)Lcom/squareup/invoices/image/FileUploadResult$Success;
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/image/ImageUploader;->toUploadSuccessResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/io/File;)Lcom/squareup/invoices/image/FileUploadResult$Success;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$uploadFileToInvoice(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/util/Optional;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)Lio/reactivex/Single;
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/image/ImageUploader;->uploadFileToInvoice(Lcom/squareup/util/Optional;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$uploadFileToInvoice(Lcom/squareup/invoices/image/ImageUploader;Ljava/io/File;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)Lio/reactivex/Single;
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/image/ImageUploader;->uploadFileToInvoice(Ljava/io/File;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final defaultFailureResult()Lcom/squareup/invoices/image/FileUploadResult$Failure;
    .locals 4

    .line 142
    new-instance v0, Lcom/squareup/invoices/image/FileUploadResult$Failure;

    .line 143
    iget-object v1, p0, Lcom/squareup/invoices/image/ImageUploader;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->upload_failed_default:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 142
    invoke-direct {v0, v1, v2, v3, v2}, Lcom/squareup/invoices/image/FileUploadResult$Failure;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method private final toUploadFailureResult(Lcom/squareup/invoices/image/FileValidationResult$Failure;)Lcom/squareup/invoices/image/FileUploadResult$Failure;
    .locals 2

    .line 148
    new-instance v0, Lcom/squareup/invoices/image/FileUploadResult$Failure;

    invoke-virtual {p1}, Lcom/squareup/invoices/image/FileValidationResult$Failure;->getErrorTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/invoices/image/FileValidationResult$Failure;->getErrorBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/image/FileUploadResult$Failure;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private final toUploadFailureResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/image/FileUploadResult$Failure;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+",
            "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
            ">;)",
            "Lcom/squareup/invoices/image/FileUploadResult$Failure;"
        }
    .end annotation

    .line 167
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v1, Lcom/squareup/common/strings/R$string;->error_default:I

    new-instance v2, Lcom/squareup/invoices/image/ImageUploader$toUploadFailureResult$failureMessage$1;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/image/ImageUploader$toUploadFailureResult$failureMessage$1;-><init>(Lcom/squareup/invoices/image/ImageUploader;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 174
    new-instance v0, Lcom/squareup/invoices/image/FileUploadResult$Failure;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/image/FileUploadResult$Failure;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private final toUploadSuccessResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/io/File;)Lcom/squareup/invoices/image/FileUploadResult$Success;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess<",
            "+",
            "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
            ">;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
            "Ljava/io/File;",
            ")",
            "Lcom/squareup/invoices/image/FileUploadResult$Success;"
        }
    .end annotation

    .line 155
    new-instance v0, Lcom/squareup/invoices/image/FileUploadResult$Success;

    .line 156
    sget-object v1, Lcom/squareup/invoices/image/InvoiceFileHelper;->Companion:Lcom/squareup/invoices/image/InvoiceFileHelper$Companion;

    .line 157
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;->getToken()Ljava/lang/String;

    move-result-object p2

    .line 159
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/invoices/InvoiceFileUploadResponse;

    iget-object p1, p1, Lcom/squareup/server/invoices/InvoiceFileUploadResponse;->token:Ljava/lang/String;

    const-string v2, "response.token"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    invoke-virtual {v1, p2, p3, p1}, Lcom/squareup/invoices/image/InvoiceFileHelper$Companion;->fileMetadata(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    move-result-object p1

    .line 155
    invoke-direct {v0, p1}, Lcom/squareup/invoices/image/FileUploadResult$Success;-><init>(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V

    return-object v0
.end method

.method private final uploadFileToInvoice(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/io/File;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
            "Ljava/io/File;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/image/FileUploadResult;",
            ">;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader;->fileAttachmentServiceHelper:Lcom/squareup/invoices/image/FileAttachmentServiceHelper;

    .line 129
    invoke-static {p2}, Lcom/squareup/invoices/image/InvoiceFileHelperKt;->getMimeType(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Lokhttp3/MediaType;->Companion:Lokhttp3/MediaType$Companion;

    invoke-virtual {v2, v1}, Lokhttp3/MediaType$Companion;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 128
    :goto_0
    invoke-virtual {v0, p1, p2, v1}, Lcom/squareup/invoices/image/FileAttachmentServiceHelper;->uploadFile(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/io/File;Lokhttp3/MediaType;)Lio/reactivex/Single;

    move-result-object v0

    .line 131
    new-instance v1, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$2;-><init>(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/io/File;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 138
    new-instance v0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$3;

    invoke-direct {v0, p0, p2}, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$3;-><init>(Lcom/squareup/invoices/image/ImageUploader;Ljava/io/File;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "fileAttachmentServiceHel\u2026FileHelper.delete(file) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final uploadFileToInvoice(Lcom/squareup/util/Optional;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "+",
            "Ljava/io/File;",
            ">;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/image/FileUploadResult;",
            ">;"
        }
    .end annotation

    .line 117
    sget-object v0, Lcom/squareup/util/Optional$Empty;->INSTANCE:Lcom/squareup/util/Optional$Empty;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/invoices/image/ImageUploader;->defaultFailureResult()Lcom/squareup/invoices/image/FileUploadResult$Failure;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(defaultFailureResult())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :cond_0
    instance-of v0, p1, Lcom/squareup/util/Optional$Present;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/io/File;

    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/image/ImageUploader;->uploadFileToInvoice(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/io/File;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final uploadFileToInvoice(Ljava/io/File;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/image/FileUploadResult;",
            ">;"
        }
    .end annotation

    .line 105
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/image/InvoiceFileHelper;->copy(Ljava/io/File;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 106
    new-instance p2, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$1;

    invoke-direct {p2, p0, p3}, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$1;-><init>(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "invoiceFileHelper.copy(f\u2026e(it, invoiceTokenType) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final uploadFile(Ljava/io/File;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
            "Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/image/FileUploadResult;",
            ">;"
        }
    .end annotation

    const-string v0, "file"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceTokenType"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "validationInfo"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader;->fileValidator:Lcom/squareup/invoices/image/InvoiceFileValidator;

    invoke-virtual {v0, p4, p1}, Lcom/squareup/invoices/image/InvoiceFileValidator;->validateFile(Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;Ljava/io/File;)Lio/reactivex/Single;

    move-result-object p4

    .line 88
    new-instance v0, Lcom/squareup/invoices/image/ImageUploader$uploadFile$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/image/ImageUploader$uploadFile$1;-><init>(Lcom/squareup/invoices/image/ImageUploader;Ljava/io/File;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p4, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "fileValidator.validateFi\u2026())\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final uploadUri(Landroid/net/Uri;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)Lio/reactivex/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
            "Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/image/FileUploadResult;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileMetadata"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceTokenType"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "validationInfo"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getMimeType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x58a7d764    # -2.9998036E-15f

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "image/jpeg"

    .line 54
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader;->imageCompressor:Lcom/squareup/invoices/image/ImageCompressor;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/image/ImageCompressor;->compress(Landroid/net/Uri;)Lio/reactivex/Single;

    move-result-object p1

    .line 55
    new-instance v0, Lcom/squareup/invoices/image/ImageUploader$uploadUri$1;

    invoke-direct {v0, p0, p3, p2, p4}, Lcom/squareup/invoices/image/ImageUploader$uploadUri$1;-><init>(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "imageCompressor.compress\u2026            }\n          }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 66
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getExtensionToPreserve()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/invoices/image/InvoiceFileHelper;->createFileFromUri$default(Lcom/squareup/invoices/image/InvoiceFileHelper;Landroid/net/Uri;Ljava/lang/String;ZILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    .line 67
    new-instance v0, Lcom/squareup/invoices/image/ImageUploader$uploadUri$2;

    invoke-direct {v0, p0, p3, p2, p4}, Lcom/squareup/invoices/image/ImageUploader$uploadUri$2;-><init>(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "invoiceFileHelper.create\u2026            }\n          }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method
