.class public final Lcom/squareup/invoices/image/FileViewer_Factory;
.super Ljava/lang/Object;
.source "FileViewer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/image/FileViewer;",
        ">;"
    }
.end annotation


# instance fields
.field private final fileViewingIntentCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ExternalFileViewingIntentCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final intentAvailabilityManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/IntentAvailabilityManager;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceFileHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/IntentAvailabilityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ExternalFileViewingIntentCreator;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/invoices/image/FileViewer_Factory;->intentAvailabilityManagerProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/invoices/image/FileViewer_Factory;->invoiceFileHelperProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/invoices/image/FileViewer_Factory;->fileViewingIntentCreatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/image/FileViewer_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/IntentAvailabilityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ExternalFileViewingIntentCreator;",
            ">;)",
            "Lcom/squareup/invoices/image/FileViewer_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/invoices/image/FileViewer_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/image/FileViewer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/IntentAvailabilityManager;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/image/ExternalFileViewingIntentCreator;)Lcom/squareup/invoices/image/FileViewer;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/invoices/image/FileViewer;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/image/FileViewer;-><init>(Lcom/squareup/util/IntentAvailabilityManager;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/image/ExternalFileViewingIntentCreator;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/image/FileViewer;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/invoices/image/FileViewer_Factory;->intentAvailabilityManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/IntentAvailabilityManager;

    iget-object v1, p0, Lcom/squareup/invoices/image/FileViewer_Factory;->invoiceFileHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/image/InvoiceFileHelper;

    iget-object v2, p0, Lcom/squareup/invoices/image/FileViewer_Factory;->fileViewingIntentCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/invoices/image/ExternalFileViewingIntentCreator;

    invoke-static {v0, v1, v2}, Lcom/squareup/invoices/image/FileViewer_Factory;->newInstance(Lcom/squareup/util/IntentAvailabilityManager;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/image/ExternalFileViewingIntentCreator;)Lcom/squareup/invoices/image/FileViewer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/invoices/image/FileViewer_Factory;->get()Lcom/squareup/invoices/image/FileViewer;

    move-result-object v0

    return-object v0
.end method
