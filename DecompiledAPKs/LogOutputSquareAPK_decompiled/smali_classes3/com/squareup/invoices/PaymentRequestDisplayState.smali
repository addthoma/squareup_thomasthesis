.class public abstract Lcom/squareup/invoices/PaymentRequestDisplayState;
.super Ljava/lang/Object;
.source "PaymentRequestDisplayState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/PaymentRequestDisplayState$Overdue;,
        Lcom/squareup/invoices/PaymentRequestDisplayState$PartiallyPaid;,
        Lcom/squareup/invoices/PaymentRequestDisplayState$Paid;,
        Lcom/squareup/invoices/PaymentRequestDisplayState$Unpaid;,
        Lcom/squareup/invoices/PaymentRequestDisplayState$Unknown;,
        Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0006\u000c\r\u000e\u000f\u0010\u0011B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u001c\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0082\u0001\u0005\u0012\u0013\u0014\u0015\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/invoices/PaymentRequestDisplayState;",
        "",
        "()V",
        "getColorId",
        "",
        "getDisplayString",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "Factory",
        "Overdue",
        "Paid",
        "PartiallyPaid",
        "Unknown",
        "Unpaid",
        "Lcom/squareup/invoices/PaymentRequestDisplayState$Overdue;",
        "Lcom/squareup/invoices/PaymentRequestDisplayState$PartiallyPaid;",
        "Lcom/squareup/invoices/PaymentRequestDisplayState$Paid;",
        "Lcom/squareup/invoices/PaymentRequestDisplayState$Unpaid;",
        "Lcom/squareup/invoices/PaymentRequestDisplayState$Unknown;",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/invoices/PaymentRequestDisplayState;-><init>()V

    return-void
.end method


# virtual methods
.method public final getColorId()I
    .locals 1

    .line 48
    sget-object v0, Lcom/squareup/invoices/PaymentRequestDisplayState$Paid;->INSTANCE:Lcom/squareup/invoices/PaymentRequestDisplayState$Paid;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/squareup/marin/R$color;->marin_green:I

    goto :goto_0

    .line 49
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/PaymentRequestDisplayState$Overdue;

    if-eqz v0, :cond_1

    sget v0, Lcom/squareup/marin/R$color;->marin_red:I

    goto :goto_0

    .line 50
    :cond_1
    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    :goto_0
    return v0
.end method

.method public final getDisplayString(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    sget-object v0, Lcom/squareup/invoices/PaymentRequestDisplayState$Paid;->INSTANCE:Lcom/squareup/invoices/PaymentRequestDisplayState$Paid;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget p2, Lcom/squareup/common/invoices/R$string;->partial_payments_paid:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_0

    .line 37
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/PaymentRequestDisplayState$Overdue;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/PaymentRequestDisplayState$Overdue;

    invoke-virtual {v0}, Lcom/squareup/invoices/PaymentRequestDisplayState$Overdue;->getCompletedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/invoices/PaymentRequestDisplayState$Overdue;->getOverdueAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v1, v0, p2, p1}, Lcom/squareup/invoices/PaymentRequestDisplayStateKt;->access$getOverdueText(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 38
    :cond_1
    instance-of v0, p0, Lcom/squareup/invoices/PaymentRequestDisplayState$Unpaid;

    if-eqz v0, :cond_2

    sget p2, Lcom/squareup/common/invoices/R$string;->partial_payments_unpaid:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_0

    .line 39
    :cond_2
    instance-of v0, p0, Lcom/squareup/invoices/PaymentRequestDisplayState$PartiallyPaid;

    if-eqz v0, :cond_3

    .line 40
    sget v0, Lcom/squareup/common/invoices/R$string;->partial_payments_partially_paid:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 41
    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/PaymentRequestDisplayState$PartiallyPaid;

    invoke-virtual {v0}, Lcom/squareup/invoices/PaymentRequestDisplayState$PartiallyPaid;->getCompletedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v0, "amount_paid"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 42
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "res.phrase(R.string.part\u2026unt))\n          .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 44
    :cond_3
    sget-object p2, Lcom/squareup/invoices/PaymentRequestDisplayState$Unknown;->INSTANCE:Lcom/squareup/invoices/PaymentRequestDisplayState$Unknown;

    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    sget p2, Lcom/squareup/common/invoices/R$string;->partial_payments_unknown:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
