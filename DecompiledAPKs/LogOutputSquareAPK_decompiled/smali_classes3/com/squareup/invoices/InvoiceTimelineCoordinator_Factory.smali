.class public final Lcom/squareup/invoices/InvoiceTimelineCoordinator_Factory;
.super Ljava/lang/Object;
.source "InvoiceTimelineCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/InvoiceTimelineCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final glassSpinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceTimelineViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator_Factory;->invoiceTimelineViewFactoryProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/InvoiceTimelineCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)",
            "Lcom/squareup/invoices/InvoiceTimelineCoordinator_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/invoices/InvoiceTimelineCoordinator_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/InvoiceTimelineCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/invoices/InvoiceTimelineCoordinator;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/invoices/InvoiceTimelineCoordinator;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/InvoiceTimelineCoordinator;-><init>(Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/InvoiceTimelineCoordinator;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;

    iget-object v1, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator_Factory;->invoiceTimelineViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

    iget-object v2, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/register/widgets/GlassSpinner;

    invoke-static {v0, v1, v2}, Lcom/squareup/invoices/InvoiceTimelineCoordinator_Factory;->newInstance(Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/invoices/InvoiceTimelineCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/invoices/InvoiceTimelineCoordinator_Factory;->get()Lcom/squareup/invoices/InvoiceTimelineCoordinator;

    move-result-object v0

    return-object v0
.end method
