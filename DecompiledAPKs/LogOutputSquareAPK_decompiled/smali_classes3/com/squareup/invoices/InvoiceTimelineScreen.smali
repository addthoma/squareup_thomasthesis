.class public final Lcom/squareup/invoices/InvoiceTimelineScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InvoiceTimelineScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/InvoiceTimelineScreen$ParentComponent;,
        Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;,
        Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;,
        Lcom/squareup/invoices/InvoiceTimelineScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceTimelineScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceTimelineScreen.kt\ncom/squareup/invoices/InvoiceTimelineScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,62:1\n43#2:63\n24#3,4:64\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceTimelineScreen.kt\ncom/squareup/invoices/InvoiceTimelineScreen\n*L\n39#1:63\n57#1,4:64\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u0000 \u00122\u00020\u00012\u00020\u00022\u00020\u0003:\u0004\u0012\u0013\u0014\u0015B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0014J\u0008\u0010\u000c\u001a\u00020\u0003H\u0016J\u0012\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u000bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/invoices/InvoiceTimelineScreen;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "parent",
        "(Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "getParentKey",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "Companion",
        "ParentComponent",
        "Runner",
        "ScreenData",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/InvoiceTimelineScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/invoices/InvoiceTimelineScreen$Companion;


# instance fields
.field private final parent:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/InvoiceTimelineScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/InvoiceTimelineScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/InvoiceTimelineScreen;->Companion:Lcom/squareup/invoices/InvoiceTimelineScreen$Companion;

    .line 64
    new-instance v0, Lcom/squareup/invoices/InvoiceTimelineScreen$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/invoices/InvoiceTimelineScreen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 67
    sput-object v0, Lcom/squareup/invoices/InvoiceTimelineScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/InvoiceTimelineScreen;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceTimelineScreen;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceTimelineScreen;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/invoices/InvoiceTimelineScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    const-class v0, Lcom/squareup/invoices/InvoiceTimelineScreen$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/InvoiceTimelineScreen$ParentComponent;

    .line 40
    invoke-interface {p1}, Lcom/squareup/invoices/InvoiceTimelineScreen$ParentComponent;->timelineCoordinator()Lcom/squareup/invoices/InvoiceTimelineCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 26
    sget v0, Lcom/squareup/features/invoices/R$layout;->invoice_timeline:I

    return v0
.end method
