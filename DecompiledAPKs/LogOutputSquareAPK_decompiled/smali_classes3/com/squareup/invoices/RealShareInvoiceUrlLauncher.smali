.class public final Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;
.super Ljava/lang/Object;
.source "RealShareInvoiceUrlLauncher.kt"

# interfaces
.implements Lcom/squareup/url/InvoiceShareUrlLauncher;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealShareInvoiceUrlLauncher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealShareInvoiceUrlLauncher.kt\ncom/squareup/invoices/RealShareInvoiceUrlLauncher\n*L\n1#1,79:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\t\u001a\u00020\nH\u0016J \u0010\r\u001a\u00020\u000c2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u000c2\u0006\u0010\t\u001a\u00020\nH\u0016R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;",
        "Lcom/squareup/url/InvoiceShareUrlLauncher;",
        "res",
        "Lcom/squareup/util/Res;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "invoiceUrlHelper",
        "Lcom/squareup/invoices/InvoiceUrlHelper;",
        "(Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/invoices/InvoiceUrlHelper;)V",
        "activity",
        "Landroid/app/Activity;",
        "dropActivity",
        "",
        "shareUrl",
        "invoiceIdPair",
        "Lcom/squareup/protos/client/IdPair;",
        "email",
        "",
        "urlType",
        "Lcom/squareup/url/UrlType;",
        "takeActivity",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private activity:Landroid/app/Activity;

.field private final invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/invoices/InvoiceUrlHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceUrlHelper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p3, p0, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    return-void
.end method


# virtual methods
.method public dropActivity(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;->activity:Landroid/app/Activity;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 32
    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;->activity:Landroid/app/Activity;

    :cond_0
    return-void
.end method

.method public shareUrl(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/url/UrlType;)V
    .locals 7

    const-string v0, "invoiceIdPair"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "email"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "urlType"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;->res:Lcom/squareup/util/Res;

    .line 42
    sget-object v1, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p3}, Lcom/squareup/url/UrlType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v3, 0x2

    if-eq v1, v3, :cond_1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    .line 45
    sget v1, Lcom/squareup/common/invoices/R$string;->recurring_series:I

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 44
    :cond_1
    sget v1, Lcom/squareup/common/invoices/R$string;->estimate:I

    goto :goto_0

    .line 43
    :cond_2
    sget v1, Lcom/squareup/common/invoices/R$string;->invoice:I

    .line 41
    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_delivery_share_subject:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 51
    iget-object v3, p0, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v3}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v3

    const-string v4, "settings.userSettings"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/settings/server/UserSettings;->getSubunitName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v3}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v3

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/settings/server/UserSettings;->getEmail()Ljava/lang/String;

    move-result-object v3

    :goto_1
    check-cast v3, Ljava/lang/CharSequence;

    const-string v4, "name"

    invoke-virtual {v1, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 52
    check-cast v0, Ljava/lang/CharSequence;

    const-string v3, "invoice_type"

    invoke-virtual {v1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 53
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 54
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 57
    iget-object v4, p0, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/common/invoices/R$string;->invoice_delivery_share_body:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    .line 58
    iget-object v5, p0, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const-string v6, "invoiceIdPair.server_id"

    invoke-static {p1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5, p1, p3}, Lcom/squareup/invoices/InvoiceUrlHelper;->invoiceUrl(Ljava/lang/String;Lcom/squareup/url/UrlType;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string/jumbo p3, "url"

    invoke-virtual {v4, p3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 59
    invoke-virtual {p1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 60
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 63
    sget-object p3, Lcom/squareup/invoices/InvoiceUrlHelper;->Companion:Lcom/squareup/invoices/InvoiceUrlHelper$Companion;

    invoke-virtual {p3}, Lcom/squareup/invoices/InvoiceUrlHelper$Companion;->shareInvoiceIntent()Landroid/content/Intent;

    move-result-object p3

    const-string v4, "android.intent.extra.TEXT"

    .line 65
    invoke-virtual {p3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "android.intent.extra.SUBJECT"

    .line 66
    invoke-virtual {p3, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-array p1, v2, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, p1, v1

    const-string p2, "android.intent.extra.EMAIL"

    .line 67
    invoke-virtual {p3, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    iget-object p1, p0, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/common/invoices/R$string;->invoice_delivery_share_using:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 72
    invoke-virtual {p1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 73
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 74
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 76
    iget-object p2, p0, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;->activity:Landroid/app/Activity;

    if-eqz p2, :cond_4

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p3, p1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_4
    return-void
.end method

.method public takeActivity(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iput-object p1, p0, Lcom/squareup/invoices/RealShareInvoiceUrlLauncher;->activity:Landroid/app/Activity;

    return-void
.end method
