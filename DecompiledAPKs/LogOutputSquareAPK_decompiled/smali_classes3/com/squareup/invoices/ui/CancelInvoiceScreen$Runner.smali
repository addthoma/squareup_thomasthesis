.class public interface abstract Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;
.super Ljava/lang/Object;
.source "CancelInvoiceScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/CancelInvoiceScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H&J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H&J\u0008\u0010\n\u001a\u00020\u0003H&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;",
        "",
        "cancelInvoice",
        "",
        "notifyRecipients",
        "",
        "issueRefund",
        "cancelInvoiceScreenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;",
        "goBackFromCancelScreen",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract cancelInvoice(ZZ)V
.end method

.method public abstract cancelInvoiceScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;",
            ">;"
        }
    .end annotation
.end method

.method public abstract goBackFromCancelScreen()V
.end method
