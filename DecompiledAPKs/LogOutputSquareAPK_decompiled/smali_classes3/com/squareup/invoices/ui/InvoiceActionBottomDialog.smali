.class public final Lcom/squareup/invoices/ui/InvoiceActionBottomDialog;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InvoiceActionBottomDialog.kt"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ParentComponent;,
        Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$Factory;,
        Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$Runner;,
        Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ScreenData;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceActionBottomDialog.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceActionBottomDialog.kt\ncom/squareup/invoices/ui/InvoiceActionBottomDialog\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,81:1\n52#2:82\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceActionBottomDialog.kt\ncom/squareup/invoices/ui/InvoiceActionBottomDialog\n*L\n40#1:82\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003:\u0004\u000b\u000c\r\u000eB\r\u0012\u0006\u0010\u0004\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0006\u001a\u00020\u0001H\u0016J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0004\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceActionBottomDialog;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/MaybePersistent;",
        "parentKey",
        "(Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "getParentKey",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "Factory",
        "ParentComponent",
        "Runner",
        "ScreenData",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "parentKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceActionBottomDialog;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceActionBottomDialog;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceActionBottomDialog;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    .line 29
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v0, "view.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    const-class v0, Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 40
    check-cast p1, Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ParentComponent;

    .line 41
    invoke-interface {p1}, Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ParentComponent;->invoiceActionBottomDialogCoordinator()Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method
