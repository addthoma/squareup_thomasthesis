.class public final Lcom/squareup/invoices/ui/InvoicePaymentTypeView$Factory;
.super Ljava/lang/Object;
.source "InvoicePaymentTypeView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoicePaymentTypeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002JH\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\u00062\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u0008\u0008\u0001\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoicePaymentTypeView$Factory;",
        "",
        "()V",
        "createView",
        "Lcom/squareup/invoices/ui/InvoicePaymentTypeView;",
        "title",
        "",
        "subtitle",
        "helperText",
        "buttonText",
        "buttonAction",
        "Lkotlin/Function0;",
        "",
        "drawableId",
        "",
        "context",
        "Landroid/content/Context;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView$Factory;-><init>()V

    return-void
.end method


# virtual methods
.method public final createView(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function0;ILandroid/content/Context;)Lcom/squareup/invoices/ui/InvoicePaymentTypeView;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;I",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/invoices/ui/InvoicePaymentTypeView;"
        }
    .end annotation

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subtitle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buttonText"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buttonAction"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    new-instance v0, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {v0, p7, v1, v2, v1}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 107
    invoke-virtual {v0, p6}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->setDrawable(I)V

    .line 108
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 109
    invoke-virtual {v0, p2}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 110
    invoke-virtual {v0, p4}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->setButtonText(Ljava/lang/CharSequence;)V

    .line 111
    invoke-virtual {v0, p5}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->setButtonOnClickListener(Lkotlin/jvm/functions/Function0;)V

    .line 112
    invoke-virtual {v0, p3}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->setHelperTextOrGone(Ljava/lang/CharSequence;)V

    return-object v0
.end method
