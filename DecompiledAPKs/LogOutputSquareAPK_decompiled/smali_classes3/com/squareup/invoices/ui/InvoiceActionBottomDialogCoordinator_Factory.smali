.class public final Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator_Factory;
.super Ljava/lang/Object;
.source "InvoiceActionBottomDialogCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final factoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator_Factory;->factoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;",
            ">;)",
            "Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$Runner;Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;)Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;-><init>(Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$Runner;Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$Runner;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator_Factory;->factoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;

    invoke-static {v0, v1}, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator_Factory;->newInstance(Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$Runner;Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;)Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator_Factory;->get()Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;

    move-result-object v0

    return-object v0
.end method
