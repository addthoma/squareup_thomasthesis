.class public final Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "InvoicesAppletConfirmationCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0010\u0010\r\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J \u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0012H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;",
        "(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "glyphMessageView",
        "Lcom/squareup/marin/widgets/MarinGlyphMessage;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "updateActionBar",
        "actionBarTitle",
        "",
        "success",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private final runner:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->runner:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;

    return-void
.end method

.method public static final synthetic access$getGlyphMessageView$p(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;)Lcom/squareup/marin/widgets/MarinGlyphMessage;
    .locals 1

    .line 14
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez p0, :cond_0

    const-string v0, "glyphMessageView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->runner:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$setGlyphMessageView$p(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;Lcom/squareup/marin/widgets/MarinGlyphMessage;)V
    .locals 0

    .line 14
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    return-void
.end method

.method public static final synthetic access$updateActionBar(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;Ljava/lang/String;Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;Z)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->updateActionBar(Ljava/lang/String;Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;Z)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 50
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 52
    sget v0, Lcom/squareup/features/invoices/R$id;->confirmation_glyph_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    return-void
.end method

.method private final updateActionBar(Ljava/lang/String;Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;Z)V
    .locals 3

    .line 46
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 42
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 43
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const/4 v1, 0x1

    .line 44
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 45
    new-instance v1, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$updateActionBar$1;

    invoke-direct {v1, p2, p3}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$updateActionBar$1;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;Z)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->bindViews(Landroid/view/View;)V

    .line 25
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->runner:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;->confirmationScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 26
    new-instance v1, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "runner.confirmationScree\u2026n(it.success) }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
