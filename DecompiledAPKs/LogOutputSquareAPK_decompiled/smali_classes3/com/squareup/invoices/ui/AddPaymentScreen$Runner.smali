.class public interface abstract Lcom/squareup/invoices/ui/AddPaymentScreen$Runner;
.super Ljava/lang/Object;
.source "AddPaymentScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/AddPaymentScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0006H&J\u0008\u0010\u0007\u001a\u00020\u0006H&J\u0008\u0010\u0008\u001a\u00020\u0006H&J\u0008\u0010\t\u001a\u00020\u0006H&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/AddPaymentScreen$Runner;",
        "",
        "addPaymentScreenData",
        "Lrx/Observable;",
        "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;",
        "chargeInvoice",
        "",
        "goBackFromAddPayment",
        "goToRecordPayment",
        "shareLink",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addPaymentScreenData()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract chargeInvoice()V
.end method

.method public abstract goBackFromAddPayment()V
.end method

.method public abstract goToRecordPayment()V
.end method

.method public abstract shareLink()V
.end method
