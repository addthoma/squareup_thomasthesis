.class Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;
.super Ljava/lang/Object;
.source "InvoicesAppletScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DisplayDetailsInput"
.end annotation


# static fields
.field private static final DELAY_GET_KEY:Ljava/lang/String; = "delay_get_key"

.field private static final INVOICE_ID_KEY:Ljava/lang/String; = "invoice_get_key"

.field private static final IS_RECURRING_KEY:Ljava/lang/String; = "invoice_recurring_key"


# instance fields
.field delayGet:Z

.field id:Ljava/lang/String;

.field isRecurring:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;ZZ)V
    .locals 0

    .line 1179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1180
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->id:Ljava/lang/String;

    .line 1181
    iput-boolean p2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->isRecurring:Z

    .line 1182
    iput-boolean p3, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->delayGet:Z

    return-void
.end method

.method static createSeriesInput(Ljava/lang/String;)Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;
    .locals 3

    .line 1204
    new-instance v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;-><init>(Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method static createSingleInvoiceInput(Ljava/lang/String;Z)Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;
    .locals 2

    .line 1200
    new-instance v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;-><init>(Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method static getFromBundle(Landroid/os/Bundle;)Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;
    .locals 3

    const-string v0, "invoice_get_key"

    .line 1192
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "invoice_recurring_key"

    .line 1193
    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "delay_get_key"

    .line 1194
    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p0

    .line 1196
    new-instance v2, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;

    invoke-direct {v2, v0, v1, p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;-><init>(Ljava/lang/String;ZZ)V

    return-object v2
.end method


# virtual methods
.method writeToBundle(Landroid/os/Bundle;)V
    .locals 2

    .line 1186
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->id:Ljava/lang/String;

    const-string v1, "invoice_get_key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1187
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->isRecurring:Z

    const-string v1, "invoice_recurring_key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1188
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->delayGet:Z

    const-string v1, "delay_get_key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
