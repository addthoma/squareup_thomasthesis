.class final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$onEnterScope$1;
.super Ljava/lang/Object;
.source "RecordPaymentScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/invoices/RecordPaymentInfo;",
        "displayDetails",
        "Lcom/squareup/invoices/DisplayDetails$Invoice;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/invoices/DisplayDetails$Invoice;)Lcom/squareup/invoices/RecordPaymentInfo;
    .locals 8

    .line 61
    new-instance v7, Lcom/squareup/invoices/RecordPaymentInfo;

    .line 62
    sget-object v1, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->CASH:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    .line 63
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    invoke-static {v0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->access$getDefaultRecordPaymentAmountCalculator$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;

    move-result-object v0

    .line 64
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v2

    .line 63
    invoke-virtual {v0, v2}, Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;->computeForInvoice(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 68
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getId()Ljava/lang/String;

    move-result-object v5

    .line 69
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    iget-object v6, p1, Lcom/squareup/protos/client/invoice/Invoice;->version:Ljava/lang/String;

    const-string p1, "displayDetails.invoice.version"

    invoke-static {v6, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, ""

    const/4 v4, 0x1

    move-object v0, v7

    .line 61
    invoke-direct/range {v0 .. v6}, Lcom/squareup/invoices/RecordPaymentInfo;-><init>(Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;Lcom/squareup/protos/common/Money;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v7
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$onEnterScope$1;->call(Lcom/squareup/invoices/DisplayDetails$Invoice;)Lcom/squareup/invoices/RecordPaymentInfo;

    move-result-object p1

    return-object p1
.end method
