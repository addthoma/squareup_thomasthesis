.class final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentScreenData$1;
.super Ljava/lang/Object;
.source "RecordPaymentScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->recordPaymentScreenData()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func3<",
        "TT1;TT2;TT3;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u00010\u00060\u00062\u000e\u0010\u0007\u001a\n \u0004*\u0004\u0018\u00010\u00080\u0008H\n\u00a2\u0006\u0004\u0008\t\u0010\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;",
        "info",
        "Lcom/squareup/invoices/RecordPaymentInfo;",
        "kotlin.jvm.PlatformType",
        "isBusy",
        "",
        "maxAmount",
        "Lcom/squareup/protos/common/Money;",
        "call",
        "(Lcom/squareup/invoices/RecordPaymentInfo;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentScreenData$1;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/invoices/RecordPaymentInfo;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentScreenData$1;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    invoke-static {v0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->access$getRecordPaymentScreenDataFactory$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;

    move-result-object v0

    const-string v1, "info"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "isBusy"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    const-string v1, "maxAmount"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;->create(Lcom/squareup/invoices/RecordPaymentInfo;ZLcom/squareup/protos/common/Money;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/invoices/RecordPaymentInfo;

    check-cast p2, Ljava/lang/Boolean;

    check-cast p3, Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentScreenData$1;->call(Lcom/squareup/invoices/RecordPaymentInfo;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method
