.class public final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;
.super Ljava/lang/Object;
.source "RecordPaymentMethodScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001b\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0006J\u000f\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0004H\u00c6\u0003J#\u0010\r\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0004H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;",
        "",
        "methods",
        "",
        "Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
        "currentPaymentMethod",
        "(Ljava/util/List;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;)V",
        "getCurrentPaymentMethod",
        "()Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
        "getMethods",
        "()Ljava/util/List;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentPaymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

.field private final methods:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
            ">;",
            "Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
            ")V"
        }
    .end annotation

    const-string v0, "methods"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentPaymentMethod"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->methods:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->currentPaymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;Ljava/util/List;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;ILjava/lang/Object;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->methods:Ljava/util/List;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->currentPaymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->copy(Ljava/util/List;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->methods:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->currentPaymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    return-object v0
.end method

.method public final copy(Ljava/util/List;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
            ">;",
            "Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
            ")",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;"
        }
    .end annotation

    const-string v0, "methods"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentPaymentMethod"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;-><init>(Ljava/util/List;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;

    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->methods:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->methods:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->currentPaymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    iget-object p1, p1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->currentPaymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCurrentPaymentMethod()Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->currentPaymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    return-object v0
.end method

.method public final getMethods()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->methods:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->methods:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->currentPaymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ScreenData(methods="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->methods:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currentPaymentMethod="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->currentPaymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
