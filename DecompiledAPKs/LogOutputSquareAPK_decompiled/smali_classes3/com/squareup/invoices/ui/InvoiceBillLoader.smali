.class public final Lcom/squareup/invoices/ui/InvoiceBillLoader;
.super Ljava/lang/Object;
.source "InvoiceBillLoader.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/invoices/ui/InvoicesAppletScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001BA\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0008\u0008\u0001\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0006\u0010\u0015\u001a\u00020\u0016J\n\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0007J\u000e\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u0018J\u0014\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u001c2\u0006\u0010\u001d\u001a\u00020\u000fJ\u0014\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u001f2\u0006\u0010 \u001a\u00020\u000fR\u001c\u0010\u0011\u001a\u0010\u0012\u000c\u0012\n \u0014*\u0004\u0018\u00010\u00130\u00130\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceBillLoader;",
        "",
        "billService",
        "Lcom/squareup/server/bills/BillListServiceHelper;",
        "standardReceiver",
        "Lcom/squareup/receiving/StandardReceiver;",
        "res",
        "Lcom/squareup/util/Res;",
        "voidCompSettings",
        "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
        "failureMessageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "userToken",
        "",
        "(Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/settings/server/Features;Ljava/lang/String;)V",
        "billHistoryLoadedState",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;",
        "kotlin.jvm.PlatformType",
        "clear",
        "",
        "getBillHistory",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "loadFromBill",
        "bill",
        "loadFromToken",
        "Lio/reactivex/Single;",
        "billToken",
        "loadedState",
        "Lio/reactivex/Observable;",
        "defaultBillToken",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final billHistoryLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;",
            ">;"
        }
    .end annotation
.end field

.field private final billService:Lcom/squareup/server/bills/BillListServiceHelper;

.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final res:Lcom/squareup/util/Res;

.field private final standardReceiver:Lcom/squareup/receiving/StandardReceiver;

.field private final userToken:Ljava/lang/String;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/settings/server/Features;Ljava/lang/String;)V
    .locals 1
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/user/UserToken;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "billService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "standardReceiver"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidCompSettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessageFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userToken"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->billService:Lcom/squareup/server/bills/BillListServiceHelper;

    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    iput-object p3, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iput-object p5, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p6, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->features:Lcom/squareup/settings/server/Features;

    iput-object p7, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->userToken:Ljava/lang/String;

    .line 48
    sget-object p1, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Empty;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Empty;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026istoryLoadedState>(Empty)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->billHistoryLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getBillHistoryLoadedState$p(Lcom/squareup/invoices/ui/InvoiceBillLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->billHistoryLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getFailureMessageFactory$p(Lcom/squareup/invoices/ui/InvoiceBillLoader;)Lcom/squareup/receiving/FailureMessageFactory;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    return-object p0
.end method

.method public static final synthetic access$getFeatures$p(Lcom/squareup/invoices/ui/InvoiceBillLoader;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/invoices/ui/InvoiceBillLoader;)Lcom/squareup/util/Res;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getVoidCompSettings$p(Lcom/squareup/invoices/ui/InvoiceBillLoader;)Lcom/squareup/tickets/voidcomp/VoidCompSettings;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    return-object p0
.end method


# virtual methods
.method public final clear()V
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->billHistoryLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Empty;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Empty;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final getBillHistory()Lcom/squareup/billhistory/model/BillHistory;
    .locals 2
    .annotation runtime Lkotlin/Deprecated;
        message = "Use the loadedState method to subscribe to updates."
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->billHistoryLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Kt;->requireValue(Lcom/jakewharton/rxrelay2/BehaviorRelay;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;

    .line 134
    sget-object v1, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Empty;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Empty;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    instance-of v1, v0, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;

    if-eqz v1, :cond_1

    :goto_0
    const/4 v0, 0x0

    goto :goto_1

    .line 135
    :cond_1
    instance-of v1, v0, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Loaded;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Loaded;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Loaded;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public final loadFromBill(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 2

    const-string v0, "bill"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->billHistoryLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Loaded;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Loaded;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final loadFromToken(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;",
            ">;"
        }
    .end annotation

    const-string v0, "billToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;-><init>()V

    .line 58
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;

    move-result-object p1

    .line 59
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    move-result-object v2

    .line 60
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->billService:Lcom/squareup/server/bills/BillListServiceHelper;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->userToken:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/server/bills/BillListServiceHelper;->getBillFamilies(Ljava/lang/String;Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;ILjava/lang/String;Z)Lio/reactivex/Single;

    move-result-object p1

    .line 61
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$1;-><init>(Lcom/squareup/invoices/ui/InvoiceBillLoader;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 89
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$2;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$2;-><init>(Lcom/squareup/invoices/ui/InvoiceBillLoader;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "billService.getBillFamil\u2026tate.accept(it)\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final loadedState(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;",
            ">;"
        }
    .end annotation

    const-string v0, "defaultBillToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader;->billHistoryLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadedState$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadedState$1;-><init>(Lcom/squareup/invoices/ui/InvoiceBillLoader;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "billHistoryLoadedState.s\u2026Observable.just(it)\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
