.class public final Lcom/squareup/invoices/ui/AddPaymentScreen;
.super Lcom/squareup/invoices/ui/InInvoicesAppletScope;
.source "AddPaymentScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;,
        Lcom/squareup/invoices/ui/AddPaymentScreen$SectionActionType;,
        Lcom/squareup/invoices/ui/AddPaymentScreen$Runner;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAddPaymentScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AddPaymentScreen.kt\ncom/squareup/invoices/ui/AddPaymentScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,144:1\n52#2:145\n24#3,4:146\n*E\n*S KotlinDebug\n*F\n+ 1 AddPaymentScreen.kt\ncom/squareup/invoices/ui/AddPaymentScreen\n*L\n34#1:145\n142#1,4:146\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003:\u0003\r\u000e\u000fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016R\u0016\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/AddPaymentScreen;",
        "Lcom/squareup/invoices/ui/InInvoicesAppletScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "Runner",
        "ScreenData",
        "SectionActionType",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/ui/AddPaymentScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/invoices/ui/AddPaymentScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/invoices/ui/AddPaymentScreen;

    invoke-direct {v0}, Lcom/squareup/invoices/ui/AddPaymentScreen;-><init>()V

    sput-object v0, Lcom/squareup/invoices/ui/AddPaymentScreen;->INSTANCE:Lcom/squareup/invoices/ui/AddPaymentScreen;

    .line 146
    new-instance v0, Lcom/squareup/invoices/ui/AddPaymentScreen$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/invoices/ui/AddPaymentScreen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 149
    sput-object v0, Lcom/squareup/invoices/ui/AddPaymentScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InInvoicesAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v0, "view.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    const-class v0, Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 34
    check-cast p1, Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;

    .line 35
    invoke-interface {p1}, Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;->addPaymentCoordinator()Lcom/squareup/invoices/ui/AddPaymentCoordinator;

    move-result-object p1

    const-string v0, "component.addPaymentCoordinator()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 31
    sget v0, Lcom/squareup/features/invoices/R$layout;->add_payment_view:I

    return v0
.end method
