.class public final Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;
.super Landroid/widget/LinearLayout;
.source "FloatingHeaderBodyRow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\n\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u000b\u001a\u0004\u0018\u00010\n2\u0008\u0010\t\u001a\u0004\u0018\u00010\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u0011\u001a\u0004\u0018\u00010\n2\u0008\u0010\t\u001a\u0004\u0018\u00010\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0012\u0010\r\"\u0004\u0008\u0013\u0010\u000f\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "body",
        "Lcom/squareup/marketfont/MarketTextView;",
        "text",
        "",
        "bodyText",
        "getBodyText",
        "()Ljava/lang/CharSequence;",
        "setBodyText",
        "(Ljava/lang/CharSequence;)V",
        "header",
        "headerText",
        "getHeaderText",
        "setHeaderText",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final body:Lcom/squareup/marketfont/MarketTextView;

.field private final header:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    .line 43
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->setOrientation(I)V

    .line 45
    sget v0, Lcom/squareup/common/invoices/R$layout;->header_body_row_layout:I

    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 46
    sget v0, Lcom/squareup/common/invoices/R$id;->header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->header:Lcom/squareup/marketfont/MarketTextView;

    .line 47
    sget v0, Lcom/squareup/common/invoices/R$id;->body:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->body:Lcom/squareup/marketfont/MarketTextView;

    .line 49
    iget-object v0, p0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->body:Lcom/squareup/marketfont/MarketTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 51
    sget-object v0, Lcom/squareup/common/invoices/R$styleable;->FloatingHeaderBodyRow:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 52
    iget-object p2, p0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->header:Lcom/squareup/marketfont/MarketTextView;

    sget v0, Lcom/squareup/common/invoices/R$styleable;->FloatingHeaderBodyRow_headerText:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iget-object p2, p0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->body:Lcom/squareup/marketfont/MarketTextView;

    sget v0, Lcom/squareup/common/invoices/R$styleable;->FloatingHeaderBodyRow_bodyText:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 24
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public final getBodyText()Ljava/lang/CharSequence;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->body:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getHeaderText()Ljava/lang/CharSequence;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->header:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final setBodyText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->body:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setHeaderText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->header:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
