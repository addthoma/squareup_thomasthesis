.class public final enum Lcom/squareup/invoices/ui/InvoiceStateFilter;
.super Ljava/lang/Enum;
.source "InvoiceStateFilter.java"

# interfaces
.implements Lcom/squareup/invoices/ui/GenericListFilter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/ui/InvoiceStateFilter;",
        ">;",
        "Lcom/squareup/invoices/ui/GenericListFilter;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/ui/InvoiceStateFilter;

.field public static final enum ALL_OUTSTANDING:Lcom/squareup/invoices/ui/InvoiceStateFilter;

.field public static final enum ALL_SENT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

.field public static final enum ALL_UNSUCCESSFUL:Lcom/squareup/invoices/ui/InvoiceStateFilter;

.field public static final enum ARCHIVED:Lcom/squareup/invoices/ui/InvoiceStateFilter;

.field public static final enum DRAFT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

.field public static final enum PAID:Lcom/squareup/invoices/ui/InvoiceStateFilter;

.field public static final enum SCHEDULED:Lcom/squareup/invoices/ui/InvoiceStateFilter;

.field public static final enum UNKNOWN:Lcom/squareup/invoices/ui/InvoiceStateFilter;


# instance fields
.field private stateFilter:Lcom/squareup/protos/client/invoice/StateFilter;

.field private titleId:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 16
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_display_state_unknown:I

    sget-object v2, Lcom/squareup/protos/client/invoice/StateFilter;->UNKNOWN:Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v3, 0x0

    const-string v4, "UNKNOWN"

    invoke-direct {v0, v4, v3, v1, v2}, Lcom/squareup/invoices/ui/InvoiceStateFilter;-><init>(Ljava/lang/String;IILcom/squareup/protos/client/invoice/StateFilter;)V

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->UNKNOWN:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    .line 18
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_state_filter_all_sent:I

    sget-object v2, Lcom/squareup/protos/client/invoice/StateFilter;->ALL_SENT:Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v4, 0x1

    const-string v5, "ALL_SENT"

    invoke-direct {v0, v5, v4, v1, v2}, Lcom/squareup/invoices/ui/InvoiceStateFilter;-><init>(Ljava/lang/String;IILcom/squareup/protos/client/invoice/StateFilter;)V

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_SENT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    .line 20
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_state_filter_all_outstanding:I

    sget-object v2, Lcom/squareup/protos/client/invoice/StateFilter;->ALL_OUTSTANDING:Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v5, 0x2

    const-string v6, "ALL_OUTSTANDING"

    invoke-direct {v0, v6, v5, v1, v2}, Lcom/squareup/invoices/ui/InvoiceStateFilter;-><init>(Ljava/lang/String;IILcom/squareup/protos/client/invoice/StateFilter;)V

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_OUTSTANDING:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    .line 22
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_display_state_draft:I

    sget-object v2, Lcom/squareup/protos/client/invoice/StateFilter;->DRAFT:Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v6, 0x3

    const-string v7, "DRAFT"

    invoke-direct {v0, v7, v6, v1, v2}, Lcom/squareup/invoices/ui/InvoiceStateFilter;-><init>(Ljava/lang/String;IILcom/squareup/protos/client/invoice/StateFilter;)V

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->DRAFT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    .line 24
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_display_state_paid:I

    sget-object v2, Lcom/squareup/protos/client/invoice/StateFilter;->PAID:Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v7, 0x4

    const-string v8, "PAID"

    invoke-direct {v0, v8, v7, v1, v2}, Lcom/squareup/invoices/ui/InvoiceStateFilter;-><init>(Ljava/lang/String;IILcom/squareup/protos/client/invoice/StateFilter;)V

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->PAID:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    .line 26
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_display_state_scheduled:I

    sget-object v2, Lcom/squareup/protos/client/invoice/StateFilter;->SCHEDULED:Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v8, 0x5

    const-string v9, "SCHEDULED"

    invoke-direct {v0, v9, v8, v1, v2}, Lcom/squareup/invoices/ui/InvoiceStateFilter;-><init>(Ljava/lang/String;IILcom/squareup/protos/client/invoice/StateFilter;)V

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->SCHEDULED:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    .line 28
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_state_filter_unsuccessful:I

    sget-object v2, Lcom/squareup/protos/client/invoice/StateFilter;->ALL_UNSUCCESSFUL:Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v9, 0x6

    const-string v10, "ALL_UNSUCCESSFUL"

    invoke-direct {v0, v10, v9, v1, v2}, Lcom/squareup/invoices/ui/InvoiceStateFilter;-><init>(Ljava/lang/String;IILcom/squareup/protos/client/invoice/StateFilter;)V

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_UNSUCCESSFUL:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    .line 30
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_display_state_archived:I

    sget-object v2, Lcom/squareup/protos/client/invoice/StateFilter;->ARCHIVED:Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v10, 0x7

    const-string v11, "ARCHIVED"

    invoke-direct {v0, v11, v10, v1, v2}, Lcom/squareup/invoices/ui/InvoiceStateFilter;-><init>(Ljava/lang/String;IILcom/squareup/protos/client/invoice/StateFilter;)V

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ARCHIVED:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/invoices/ui/InvoiceStateFilter;

    .line 15
    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->UNKNOWN:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_SENT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_OUTSTANDING:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->DRAFT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->PAID:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->SCHEDULED:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_UNSUCCESSFUL:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ARCHIVED:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->$VALUES:[Lcom/squareup/invoices/ui/InvoiceStateFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILcom/squareup/protos/client/invoice/StateFilter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/protos/client/invoice/StateFilter;",
            ")V"
        }
    .end annotation

    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 81
    iput p3, p0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->titleId:I

    .line 82
    iput-object p4, p0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->stateFilter:Lcom/squareup/protos/client/invoice/StateFilter;

    return-void
.end method

.method public static getDefaultInvoiceStateFilterForDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/ui/InvoiceStateFilter;
    .locals 1

    .line 104
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceDisplayDetails$DisplayState:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 125
    sget-object p0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->UNKNOWN:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    return-object p0

    .line 123
    :pswitch_0
    sget-object p0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_OUTSTANDING:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    return-object p0

    .line 119
    :pswitch_1
    sget-object p0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->SCHEDULED:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    return-object p0

    .line 115
    :pswitch_2
    sget-object p0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->DRAFT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    return-object p0

    .line 112
    :pswitch_3
    sget-object p0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->PAID:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    return-object p0

    .line 108
    :pswitch_4
    sget-object p0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_SENT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getFilterValues(Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/ui/GenericListFilter;",
            ">;"
        }
    .end annotation

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x6

    new-array v1, v1, [Lcom/squareup/invoices/ui/InvoiceStateFilter;

    sget-object v2, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_SENT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_OUTSTANDING:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/invoices/ui/InvoiceStateFilter;->DRAFT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/invoices/ui/InvoiceStateFilter;->PAID:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/invoices/ui/InvoiceStateFilter;->SCHEDULED:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_UNSUCCESSFUL:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    if-eqz p0, :cond_0

    .line 96
    sget-object p0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ARCHIVED:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/ui/InvoiceStateFilter;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/ui/InvoiceStateFilter;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/ui/InvoiceStateFilter;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->$VALUES:[Lcom/squareup/invoices/ui/InvoiceStateFilter;

    invoke-virtual {v0}, [Lcom/squareup/invoices/ui/InvoiceStateFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/ui/InvoiceStateFilter;

    return-object v0
.end method


# virtual methods
.method public formatTitle(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 64
    iget v0, p0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->titleId:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getLabel()I
    .locals 1

    .line 41
    iget v0, p0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->titleId:I

    return v0
.end method

.method public getSearchBarHint()I
    .locals 2

    .line 45
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceStateFilter:[I

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 59
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_search_all_invoices:I

    return v0

    .line 55
    :cond_0
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_search_archived:I

    return v0

    .line 53
    :cond_1
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_search_scheduled:I

    return v0

    .line 51
    :cond_2
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_search_paid:I

    return v0

    .line 49
    :cond_3
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_search_drafts:I

    return v0

    .line 47
    :cond_4
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_search_all_outstanding:I

    return v0
.end method

.method public getStateFilter()Lcom/squareup/protos/client/invoice/StateFilter;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->stateFilter:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object v0
.end method

.method public getTitle()I
    .locals 1

    .line 37
    iget v0, p0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->titleId:I

    return v0
.end method

.method public isRecurringListFilter()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isStateFilter()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
