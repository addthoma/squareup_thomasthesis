.class public abstract Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;
.super Landroid/widget/LinearLayout;
.source "AbstractInvoiceDetailView.java"


# instance fields
.field protected actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private additionalEmailsContainer:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

.field protected animator:Lcom/squareup/widgets/SquareViewAnimator;

.field protected billToRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

.field private displayedState:Landroid/widget/TextView;

.field private dueRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

.field private errorMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field protected fileAttachmentsContainer:Landroid/widget/LinearLayout;

.field private fileAttachmentsHeader:Lcom/squareup/marketfont/MarketTextView;

.field private header:Lcom/squareup/invoices/ui/InvoiceDetailHeader;

.field private idRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

.field private invoiceDateRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

.field private invoiceMessage:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

.field private legacyStatus:Landroid/view/View;

.field private lineItemsContainer:Landroid/widget/LinearLayout;

.field private lineItemsDivider:Landroid/view/View;

.field protected loadingInstrumentsProgressBar:Landroid/widget/ProgressBar;

.field protected recurringRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

.field private shareLink:Landroid/widget/TextView;

.field private shippingAddressRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

.field private timeline:Landroid/view/ViewGroup;

.field private titleRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

.field protected viewTimelineButton:Lcom/squareup/marketfont/MarketButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public addLineItemRow(Lcom/squareup/ui/cart/CartEntryView;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->lineItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public addLineItemRows(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ">;)V"
        }
    .end annotation

    .line 196
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartEntryView;

    .line 197
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->addLineItemRow(Lcom/squareup/ui/cart/CartEntryView;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public changeListState(Lcom/squareup/invoices/ListState;)V
    .locals 1

    .line 252
    sget-object v0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView$1;->$SwitchMap$com$squareup$invoices$ListState:[I

    invoke-virtual {p1}, Lcom/squareup/invoices/ListState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    .line 260
    :cond_0
    sget p1, Lcom/squareup/common/invoices/R$id;->invoice_detail_error_message:I

    goto :goto_0

    .line 257
    :cond_1
    sget p1, Lcom/squareup/common/invoices/R$id;->invoice_detail_content:I

    goto :goto_0

    .line 254
    :cond_2
    sget p1, Lcom/squareup/common/invoices/R$id;->progress_loading_invoice:I

    .line 263
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method public clearLineItems()V
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->lineItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-lez v0, :cond_0

    .line 187
    iget-object v1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->lineItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method createFileAttachmentRow(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;Z)Lcom/squareup/ui/account/view/LineRow;
    .locals 3

    .line 171
    new-instance v0, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 172
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->size_bytes:Ljava/lang/Integer;

    .line 173
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v1, p1

    invoke-static {v1, v2}, Lcom/squareup/util/Files;->readableFileSizeFromBytes(J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setValue(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_when_pressed:I

    .line 174
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;->setBackground(I)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    if-eqz p2, :cond_0

    sget-object p2, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    .line 175
    :goto_0
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/LineRow$Builder;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 176
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p1

    return-object p1
.end method

.method public hideSentRow()V
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->invoiceDateRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->setVisibility(I)V

    return-void
.end method

.method public hideShippingAddress()V
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->shippingAddressRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->setVisibility(I)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 63
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 64
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 65
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_detail_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceDetailHeader;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->header:Lcom/squareup/invoices/ui/InvoiceDetailHeader;

    .line 66
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_detail_display_state:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->displayedState:Landroid/widget/TextView;

    .line 67
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_detail_share_link:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->shareLink:Landroid/widget/TextView;

    .line 68
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_timeline_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->viewTimelineButton:Lcom/squareup/marketfont/MarketButton;

    .line 69
    sget v0, Lcom/squareup/common/invoices/R$id;->shipping_address:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->shippingAddressRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    .line 70
    sget v0, Lcom/squareup/common/invoices/R$id;->bill_to_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->billToRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    .line 71
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_additional_emails_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->additionalEmailsContainer:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    .line 72
    sget v0, Lcom/squareup/common/invoices/R$id;->title_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->titleRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    .line 73
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_id_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->idRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    .line 74
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_recurring:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->recurringRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    .line 75
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_date_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->invoiceDateRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    .line 76
    sget v0, Lcom/squareup/common/invoices/R$id;->due_date_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->dueRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    .line 77
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_detail_line_items_divider:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->lineItemsDivider:Landroid/view/View;

    .line 78
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_detail_line_items_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->lineItemsContainer:Landroid/widget/LinearLayout;

    .line 79
    sget v0, Lcom/squareup/common/invoices/R$id;->file_attachment_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->fileAttachmentsHeader:Lcom/squareup/marketfont/MarketTextView;

    .line 80
    sget v0, Lcom/squareup/common/invoices/R$id;->detail_invoice_file_attachments_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->fileAttachmentsContainer:Landroid/widget/LinearLayout;

    .line 81
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->invoiceMessage:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    .line 82
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_detail_animator:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 83
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_detail_legacy_status:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->legacyStatus:Landroid/view/View;

    .line 84
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_detail_timeline_content:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->timeline:Landroid/view/ViewGroup;

    .line 85
    sget v0, Lcom/squareup/common/invoices/R$id;->loading_instruments:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->loadingInstrumentsProgressBar:Landroid/widget/ProgressBar;

    .line 86
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_detail_error_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->errorMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    return-void
.end method

.method resetHeader()V
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->header:Lcom/squareup/invoices/ui/InvoiceDetailHeader;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->reset()V

    return-void
.end method

.method public setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setAdditionalRecipients(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 130
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->additionalEmailsContainer:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    invoke-static {v0, p1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRowsKt;->setBodyAndVisibility(Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;Ljava/lang/Iterable;)V

    return-void
.end method

.method setBillToRow(Ljava/lang/CharSequence;)V
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->billToRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->billToRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->setBodyText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setDueRowTitle(Ljava/lang/String;)V
    .locals 1

    .line 238
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->dueRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->setHeaderText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setDueRowValue(Ljava/lang/String;)V
    .locals 3

    .line 234
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->dueRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRowsKt;->setBodyAndVisibility(Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;[Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setDueRowVisibility(Z)V
    .locals 1

    .line 230
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->dueRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->errorMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 247
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->errorMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setIdRow(Ljava/lang/String;)V
    .locals 3

    .line 126
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->idRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRowsKt;->setBodyAndVisibility(Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;[Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setInvoiceDateRowTitle(Ljava/lang/String;)V
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->invoiceDateRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->setHeaderText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setInvoiceDateRowValue(Ljava/lang/String;)V
    .locals 3

    .line 222
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->invoiceDateRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRowsKt;->setBodyAndVisibility(Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;[Ljava/lang/CharSequence;)V

    return-void
.end method

.method setInvoiceDisplayStateText(ILjava/lang/String;)V
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->displayedState:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object p2, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->displayedState:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method setInvoiceMessage(Ljava/lang/String;)V
    .locals 3

    .line 134
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->invoiceMessage:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRowsKt;->setBodyAndVisibility(Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;[Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setLineItemVisibility(Z)V
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->lineItemsDivider:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 181
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->lineItemsContainer:Landroid/widget/LinearLayout;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setPrimaryAmount(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->header:Lcom/squareup/invoices/ui/InvoiceDetailHeader;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->setPrimaryValue(Ljava/lang/CharSequence;)V

    .line 95
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->header:Lcom/squareup/invoices/ui/InvoiceDetailHeader;

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->setPrimaryHeaderText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setSecondaryAmount(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->header:Lcom/squareup/invoices/ui/InvoiceDetailHeader;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->setSecondaryValue(Ljava/lang/CharSequence;)V

    .line 100
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->header:Lcom/squareup/invoices/ui/InvoiceDetailHeader;

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->setSecondaryHeaderText(Ljava/lang/CharSequence;)V

    .line 101
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->header:Lcom/squareup/invoices/ui/InvoiceDetailHeader;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->setSecondaryVisible()V

    return-void
.end method

.method public setShareLinkTextVisibility(Z)V
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->shareLink:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setSingleInvoiceInRecurringSeries(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 145
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->recurringRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->setVisibility(I)V

    .line 146
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 147
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    invoke-interface {p1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    iget-object p2, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->recurringRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    invoke-static {p2, p1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRowsKt;->setBodyAndVisibility(Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;Ljava/lang/Iterable;)V

    goto :goto_0

    .line 152
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->recurringRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method setTertiaryAmount(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->header:Lcom/squareup/invoices/ui/InvoiceDetailHeader;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->setTertiaryValue(Ljava/lang/CharSequence;)V

    .line 106
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->header:Lcom/squareup/invoices/ui/InvoiceDetailHeader;

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->setTertiaryHeaderText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->header:Lcom/squareup/invoices/ui/InvoiceDetailHeader;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->setTertiaryVisible()V

    return-void
.end method

.method setTitleRow(Ljava/lang/String;)V
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->titleRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRowsKt;->setBodyAndVisibility(Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;[Ljava/lang/CharSequence;)V

    return-void
.end method

.method showFileAttachmentHeader()V
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->fileAttachmentsHeader:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    return-void
.end method

.method abstract showFileAttachmentRows(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;)V"
        }
    .end annotation
.end method

.method public showShippingAddress(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 161
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->shippingAddressRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 p2, 0x1

    aput-object p1, v1, p2

    invoke-static {v0, v1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRowsKt;->setBodyAndVisibility(Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;[Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showTimeline(I)Landroid/view/ViewGroup;
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->legacyStatus:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->timeline:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->header:Lcom/squareup/invoices/ui/InvoiceDetailHeader;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->setDividerVisible()V

    .line 207
    iget-object v0, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->header:Lcom/squareup/invoices/ui/InvoiceDetailHeader;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailHeader;->setPrimaryValueColor(I)V

    .line 210
    iget-object p1, p0, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->timeline:Landroid/view/ViewGroup;

    return-object p1
.end method
