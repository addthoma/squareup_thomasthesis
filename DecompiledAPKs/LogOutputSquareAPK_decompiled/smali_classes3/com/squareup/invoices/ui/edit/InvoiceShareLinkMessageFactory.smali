.class public final Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;
.super Ljava/lang/Object;
.source "InvoiceShareLinkMessageFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0019\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;",
        "",
        "application",
        "Landroid/app/Application;",
        "(Landroid/app/Application;)V",
        "shareLinkMessage",
        "",
        "kotlin.jvm.PlatformType",
        "getShareLinkMessage",
        "()Ljava/lang/CharSequence;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final shareLinkMessage:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    check-cast p1, Landroid/content/Context;

    invoke-direct {v0, p1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 13
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_delivery_method_manual_support:I

    const-string v1, "support_center"

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 14
    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_share_link_support_url:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 15
    sget v0, Lcom/squareup/checkout/R$string;->support_center:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 16
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;->shareLinkMessage:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public final getShareLinkMessage()Ljava/lang/CharSequence;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;->shareLinkMessage:Ljava/lang/CharSequence;

    return-object v0
.end method
