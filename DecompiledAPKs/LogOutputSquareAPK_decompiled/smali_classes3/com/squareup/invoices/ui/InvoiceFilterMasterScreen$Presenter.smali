.class Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "InvoiceFilterMasterScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/invoices/ui/InvoiceFilterMasterView;",
        ">;"
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

.field visibleFilters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/invoices/ui/GenericListFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 41
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    .line 43
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    .line 44
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ARCHIVE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, p1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    invoke-static {p1}, Lcom/squareup/invoices/ui/InvoiceStateFilter;->getFilterValues(Z)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->visibleFilters:Ljava/util/List;

    .line 45
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->isReccuringEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 46
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->visibleFilters:Ljava/util/List;

    invoke-static {}, Lcom/squareup/invoices/ui/SeriesStateFilter;->getVisibleFilters()Ljava/util/List;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/invoices/ui/GenericListFilter;Ljava/lang/String;)Lcom/squareup/invoices/ui/GenericListFilter;
    .locals 0

    .line 60
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p0, 0x0

    :cond_0
    return-object p0
.end method

.method static synthetic lambda$null$1(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;Lcom/squareup/invoices/ui/GenericListFilter;)V
    .locals 0

    .line 62
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->refreshAdapter()V

    return-void
.end method


# virtual methods
.method isFilterSelected(I)Z
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getTrimmedSearchTermString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    .line 68
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->hasListFilter()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    .line 69
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentListFilter()Lcom/squareup/invoices/ui/GenericListFilter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->visibleFilters:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method isReccuringEnabled()Z
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RECURRING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onLoad$2$InvoiceFilterMasterScreen$Presenter(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;)Lrx/Subscription;
    .locals 3

    .line 57
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    .line 58
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentListFilter()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    .line 59
    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getSearchTerm()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceFilterMasterScreen$Presenter$64ALX-k3MU-CMcP3cKeV-kAMp7I;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoiceFilterMasterScreen$Presenter$64ALX-k3MU-CMcP3cKeV-kAMp7I;

    .line 57
    invoke-static {v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceFilterMasterScreen$Presenter$aNouLAm_--sVw5_HQ6rGadkX97A;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceFilterMasterScreen$Presenter$aNouLAm_--sVw5_HQ6rGadkX97A;-><init>(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 51
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;

    .line 53
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->createAdapter()V

    .line 54
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->refreshAdapter()V

    .line 56
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceFilterMasterScreen$Presenter$juq7Cx5KL-G5AymWlJ41iwffCCQ;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceFilterMasterScreen$Presenter$juq7Cx5KL-G5AymWlJ41iwffCCQ;-><init>(Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;Lcom/squareup/invoices/ui/InvoiceFilterMasterView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onSectionClicked(I)V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->visibleFilters:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/GenericListFilter;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->setCurrentStateAndGoToHistoryIfNecessary(Lcom/squareup/invoices/ui/GenericListFilter;)V

    .line 78
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->refreshAdapter()V

    return-void
.end method
