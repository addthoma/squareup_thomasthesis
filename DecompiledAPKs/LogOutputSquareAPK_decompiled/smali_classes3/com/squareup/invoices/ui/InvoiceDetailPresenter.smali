.class public Lcom/squareup/invoices/ui/InvoiceDetailPresenter;
.super Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;
.source "InvoiceDetailPresenter.java"

# interfaces
.implements Lcom/squareup/features/invoices/widgets/EventHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter<",
        "Lcom/squareup/invoices/ui/InvoiceDetailView;",
        ">;",
        "Lcom/squareup/features/invoices/widgets/EventHandler;"
    }
.end annotation


# instance fields
.field private final buttonDataFactory:Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;

.field private defaultCurrencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final invoiceDetailTimelineDataFactory:Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;

.field private final invoiceTimelineViewFactory:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

.field private partialTransactionDataFactory:Lcom/squareup/invoices/PartialTransactionData$Factory;

.field private final paymentRequestDataFactory:Lcom/squareup/invoices/PaymentRequestData$Factory;

.field private final paymentRequestReadOnlyInfoFactory:Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;

.field private final scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;Lflow/Flow;Ljava/text/DateFormat;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/invoices/PartialTransactionData$Factory;Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;Lcom/squareup/invoices/PaymentRequestData$Factory;Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/settings/server/Features;",
            "Lflow/Flow;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/invoices/PartialTransactionData$Factory;",
            "Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;",
            "Lcom/squareup/invoices/PaymentRequestData$Factory;",
            "Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;",
            "Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v9, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object/from16 v3, p8

    move-object v4, p5

    move-object/from16 v5, p9

    move-object v6, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    .line 104
    invoke-direct/range {v0 .. v8}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;-><init>(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/util/Res;Lflow/Flow;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)V

    move-object v0, p2

    .line 106
    iput-object v0, v9, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    move-object/from16 v0, p10

    .line 107
    iput-object v0, v9, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->defaultCurrencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object/from16 v0, p11

    .line 108
    iput-object v0, v9, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->partialTransactionDataFactory:Lcom/squareup/invoices/PartialTransactionData$Factory;

    move-object/from16 v0, p12

    .line 109
    iput-object v0, v9, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->paymentRequestReadOnlyInfoFactory:Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;

    move-object/from16 v0, p13

    .line 110
    iput-object v0, v9, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->paymentRequestDataFactory:Lcom/squareup/invoices/PaymentRequestData$Factory;

    move-object/from16 v0, p15

    .line 111
    iput-object v0, v9, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->invoiceDetailTimelineDataFactory:Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;

    move-object/from16 v0, p16

    .line 112
    iput-object v0, v9, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->invoiceTimelineViewFactory:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

    move-object/from16 v0, p14

    .line 113
    iput-object v0, v9, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->buttonDataFactory:Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;

    return-void
.end method

.method private initializeDatesForSeries()V
    .locals 2

    .line 240
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceDetailView;

    .line 241
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->hideSentRow()V

    const/4 v1, 0x0

    .line 242
    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setDueRowVisibility(Z)V

    return-void
.end method

.method private isEditable(Lcom/squareup/invoices/DisplayDetails;)Z
    .locals 4

    .line 325
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->isRecurring()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 326
    check-cast p1, Lcom/squareup/invoices/DisplayDetails$Recurring;

    .line 327
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Recurring;->getDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p1

    .line 328
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    sget-object v3, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ACTIVE:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    if-eq v0, v3, :cond_1

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    sget-object v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1

    .line 332
    :cond_2
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_REQUEST_EDITING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 333
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_3

    return v2

    .line 337
    :cond_3
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 338
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsConfigKt;->containsInstallments(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_4

    return v2

    .line 342
    :cond_4
    check-cast p1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    .line 343
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 344
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->PAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->CANCELLED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 345
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->RECURRING:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 346
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->REFUNDED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 347
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->FAILED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 348
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method static synthetic lambda$null$0(Lcom/squareup/invoices/ui/InvoiceDetailView;Ljava/lang/Boolean;)V
    .locals 0

    .line 124
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 125
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setLoadingContactProgressVisible(Z)V

    return-void
.end method

.method static synthetic lambda$null$2(Lcom/squareup/invoices/ui/InvoiceDetailView;Ljava/lang/Boolean;)V
    .locals 1

    .line 131
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 132
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setEnabled(Z)V

    .line 133
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 134
    sget-object p1, Lcom/squareup/invoices/ListState;->SHOW_LOADING:Lcom/squareup/invoices/ListState;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->changeListState(Lcom/squareup/invoices/ListState;)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$null$4(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/util/Optional;)V
    .locals 1

    .line 142
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/receiving/FailureMessage;

    invoke-virtual {v0}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/FailureMessage;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    .line 143
    invoke-virtual {p0, v0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    sget-object p1, Lcom/squareup/invoices/ListState;->SHOW_ERROR:Lcom/squareup/invoices/ListState;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->changeListState(Lcom/squareup/invoices/ListState;)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$null$6(Lcom/squareup/invoices/DisplayDetails;)Ljava/lang/Boolean;
    .locals 0

    if-eqz p0, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private populateHeader(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 5

    .line 385
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->resetHeader()V

    .line 387
    iget-object v0, p2, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    .line 388
    invoke-static {p2}, Lcom/squareup/invoices/Invoices;->getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 390
    iget-object v1, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz v1, :cond_0

    .line 391
    iget-object p4, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 396
    :cond_0
    iget-object v1, p3, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payments:Ljava/util/List;

    .line 397
    iget-object v2, p3, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refund:Ljava/util/List;

    .line 398
    iget-object p3, p3, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 399
    invoke-static {v1, p4}, Lcom/squareup/invoices/PartialTransactionsKt;->getPaidAmount(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 400
    invoke-static {p2, v3}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 403
    invoke-static {v1, v2, p2, p4}, Lcom/squareup/invoices/PartialTransactionsKt;->isPartiallyPaid(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Z

    move-result p2

    .line 405
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_1

    if-nez p2, :cond_1

    .line 408
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->formatter:Lcom/squareup/text/Formatter;

    invoke-static {p2, v0}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->formatWithSmallDollar(Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget p4, Lcom/squareup/common/invoices/R$string;->invoice_detail_invoice_total:I

    .line 409
    invoke-interface {p3, p4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 408
    invoke-virtual {p1, p2, p3}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setPrimaryAmount(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    if-eqz v1, :cond_2

    .line 414
    invoke-static {v2, p4}, Lcom/squareup/invoices/PartialTransactionsKt;->getRefundedAmount(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 415
    invoke-static {v3, p2}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p3

    .line 417
    invoke-direct {p0, p1, p3, v0, p2}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->populateHeaderForRefunded(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-void

    :cond_2
    if-eqz p2, :cond_3

    .line 421
    sget-object p4, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->FAILED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    if-ne p3, p4, :cond_3

    .line 422
    invoke-direct {p0, p1, v3, v0, v4}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->populateHeaderForPartiallyPaidAndFailed(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-void

    :cond_3
    if-eqz p2, :cond_4

    .line 428
    invoke-direct {p0, p1, v4, v0, v3}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->populateHeaderForPartiallyPaid(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    :cond_4
    return-void
.end method

.method private populateHeaderForPartiallyPaid(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 455
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->formatter:Lcom/squareup/text/Formatter;

    invoke-static {v0, p2}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->formatWithSmallDollar(Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_detail_amount_remaining:I

    .line 456
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 455
    invoke-virtual {p1, p2, v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setPrimaryAmount(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 457
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->formatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_detail_invoice_total:I

    .line 458
    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 457
    invoke-virtual {p1, p2, p3}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setSecondaryAmount(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 459
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->formatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget p4, Lcom/squareup/common/invoices/R$string;->invoice_detail_amount_paid:I

    .line 460
    invoke-interface {p3, p4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 459
    invoke-virtual {p1, p2, p3}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setTertiaryAmount(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private populateHeaderForPartiallyPaidAndFailed(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 445
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->formatter:Lcom/squareup/text/Formatter;

    invoke-static {v0, p2}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->formatWithSmallDollar(Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_detail_amount_paid:I

    .line 446
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 445
    invoke-virtual {p1, p2, v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setPrimaryAmount(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 447
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->formatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_detail_invoice_total:I

    .line 448
    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 447
    invoke-virtual {p1, p2, p3}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setSecondaryAmount(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 449
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->formatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget p4, Lcom/squareup/common/invoices/R$string;->invoice_detail_amount_remaining:I

    .line 450
    invoke-interface {p3, p4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 449
    invoke-virtual {p1, p2, p3}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setTertiaryAmount(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private populateHeaderForRefunded(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 434
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->formatter:Lcom/squareup/text/Formatter;

    invoke-static {v0, p2}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->formatWithSmallDollar(Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_detail_amount_paid_after_refund:I

    .line 435
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 434
    invoke-virtual {p1, p2, v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setPrimaryAmount(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 437
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->formatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_detail_invoice_total:I

    .line 438
    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 437
    invoke-virtual {p1, p2, p3}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setSecondaryAmount(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 439
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->formatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget p4, Lcom/squareup/common/invoices/R$string;->invoice_detail_amount_refunded:I

    .line 440
    invoke-interface {p3, p4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 439
    invoke-virtual {p1, p2, p3}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setTertiaryAmount(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private populatePaymentSchedule(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 2

    .line 282
    iget-object p2, p2, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {p2}, Lcom/squareup/invoices/PaymentRequestsKt;->sortedPaymentRequests(Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    .line 285
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 286
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->hidePaymentRequests()V

    .line 287
    invoke-virtual {p1, v1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setDueRowVisibility(Z)V

    return-void

    .line 291
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(I)V

    .line 292
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->paymentRequestReadOnlyInfoFactory:Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;

    .line 293
    invoke-virtual {p2, p3}, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;->fromInvoiceDisplayDetails(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/util/List;

    move-result-object p2

    .line 295
    iget-object p3, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->paymentRequestDataFactory:Lcom/squareup/invoices/PaymentRequestData$Factory;

    invoke-virtual {p3, p2}, Lcom/squareup/invoices/PaymentRequestData$Factory;->createForDetails(Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const/4 p2, 0x0

    .line 296
    invoke-virtual {p1, p2}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setDueRowVisibility(Z)V

    .line 297
    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->showPaymentRequests(Ljava/util/List;)V

    return-void
.end method

.method private populatePaymentsSection(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 6

    .line 358
    iget-object v0, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payments:Ljava/util/List;

    .line 359
    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refund:Ljava/util/List;

    .line 360
    iget-object p2, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-static {p2}, Lcom/squareup/invoices/InvoiceDisplayState;->forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object p2

    .line 362
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 363
    :goto_1
    sget-object v5, Lcom/squareup/invoices/InvoiceDisplayState;->UNPAID:Lcom/squareup/invoices/InvoiceDisplayState;

    if-eq p2, v5, :cond_3

    sget-object v5, Lcom/squareup/invoices/InvoiceDisplayState;->OVERDUE:Lcom/squareup/invoices/InvoiceDisplayState;

    if-eq p2, v5, :cond_3

    sget-object v5, Lcom/squareup/invoices/InvoiceDisplayState;->PARTIALLY_PAID:Lcom/squareup/invoices/InvoiceDisplayState;

    if-ne p2, v5, :cond_2

    goto :goto_2

    :cond_2
    const/4 p2, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 p2, 0x1

    :goto_3
    if-nez v2, :cond_4

    if-eqz p2, :cond_5

    :cond_4
    const/4 v3, 0x1

    .line 367
    :cond_5
    invoke-virtual {p1, v3}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setPaymentsContainerVisibility(Z)V

    if-eqz v2, :cond_6

    .line 370
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->partialTransactionDataFactory:Lcom/squareup/invoices/PartialTransactionData$Factory;

    .line 371
    invoke-virtual {v2, v0, v1}, Lcom/squareup/invoices/PartialTransactionData$Factory;->create(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 373
    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->showPartialTransactions(Ljava/util/List;)V

    goto :goto_4

    .line 375
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->hidePartialTransactions()V

    .line 378
    :goto_4
    invoke-virtual {p1, p2}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setVisibilityAddPaymentButton(Z)V

    return-void
.end method

.method private showBottomButtons(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 2

    .line 272
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceDetailView;

    .line 274
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->buttonDataFactory:Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->createForDetailScreen(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->addButtons(Ljava/util/List;)V

    .line 276
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->buttonDataFactory:Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;

    .line 277
    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->createForBottomDialog(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/util/List;

    move-result-object p1

    .line 276
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->setInvoiceActionBottomDialogScreenData(Ljava/util/List;)V

    return-void
.end method

.method private showBottomButtons(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V
    .locals 2

    .line 265
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceDetailView;

    .line 266
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->buttonDataFactory:Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->createForDetailScreen(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->addButtons(Ljava/util/List;)V

    .line 267
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->buttonDataFactory:Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;

    .line 268
    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->createForBottomDialog(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Ljava/util/List;

    move-result-object p1

    .line 267
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->setInvoiceActionBottomDialogScreenData(Ljava/util/List;)V

    return-void
.end method

.method private showDisplayDetails(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/invoices/DisplayDetails;)V
    .locals 2

    .line 159
    sget-object v0, Lcom/squareup/invoices/ListState;->SHOW_INVOICES:Lcom/squareup/invoices/ListState;

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->changeListState(Lcom/squareup/invoices/ListState;)V

    .line 160
    invoke-direct {p0, p2}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->updateActionBar(Lcom/squareup/invoices/DisplayDetails;)V

    .line 161
    invoke-virtual {p2}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    .line 163
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->updateSharedInvoiceTemplateSections(Lcom/squareup/protos/client/invoice/Invoice;)V

    .line 165
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {p0, v1}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->populateItems(Lcom/squareup/protos/client/bills/Cart;)V

    .line 168
    invoke-virtual {p2}, Lcom/squareup/invoices/DisplayDetails;->isRecurring()Z

    move-result v1

    if-nez v1, :cond_0

    .line 169
    check-cast p2, Lcom/squareup/invoices/DisplayDetails$Invoice;

    .line 170
    invoke-virtual {p2}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p2

    .line 173
    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->populatePaymentSchedule(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    .line 174
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->populatePaymentsSection(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    .line 177
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->defaultCurrencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->populateHeader(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 179
    invoke-virtual {p0, p2}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->updateInvoiceDisplayDetailsSections(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    goto :goto_0

    .line 181
    :cond_0
    check-cast p2, Lcom/squareup/invoices/DisplayDetails$Recurring;

    .line 182
    invoke-virtual {p2}, Lcom/squareup/invoices/DisplayDetails$Recurring;->getDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p1

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->updateRecurringDisplayDetailSections(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    :goto_0
    return-void
.end method

.method private showTimelineForInvoiceDisplayDetails(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 5

    .line 246
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceDetailView;

    .line 248
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EVENT_TIMELINE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 249
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->invoiceTimelineViewFactory:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

    .line 251
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->invoiceDetailTimelineDataFactory:Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;

    const/4 v4, 0x1

    .line 252
    invoke-virtual {v3, p1, v4}, Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;->create(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Z)Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;

    move-result-object v3

    .line 250
    invoke-virtual {v1, v2, v3, p0}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;->create(Landroid/content/Context;Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;Lcom/squareup/features/invoices/widgets/EventHandler;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;

    move-result-object v1

    .line 256
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    .line 257
    invoke-static {p1}, Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactoryKt;->timelineBackgroundColor(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)I

    move-result p1

    invoke-interface {v2, p1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->showTimeline(I)Landroid/view/ViewGroup;

    move-result-object p1

    .line 259
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 260
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private updateActionBar(Lcom/squareup/invoices/DisplayDetails;)V
    .locals 3

    .line 189
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    .line 188
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->createActionBarConfigForInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 190
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->flow:Lflow/Flow;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v2, v1}, Lcom/squareup/invoices/ui/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 192
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->isEditable(Lcom/squareup/invoices/DisplayDetails;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 193
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_detail_edit_button:I

    .line 194
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$997-NR8qk0D6jZ77Kh0gGfJqGlE;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$997-NR8qk0D6jZ77Kh0gGfJqGlE;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailPresenter;)V

    .line 195
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 198
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->isRecurring()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 199
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_detail_series_title:I

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    .line 200
    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v1

    .line 199
    invoke-virtual {p0, p1, v1}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->getDetailTitle(ILcom/squareup/protos/client/invoice/Invoice;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 202
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceDetailView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private updateRecurringDisplayDetailSections(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V
    .locals 4

    .line 226
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceDetailView;

    .line 227
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    invoke-static {v1}, Lcom/squareup/invoices/ui/SeriesDisplayState;->forSeriesDisplayState(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;)Lcom/squareup/invoices/ui/SeriesDisplayState;

    move-result-object v1

    .line 228
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/SeriesDisplayState;->getDisplayColor()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v2

    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->res:Lcom/squareup/util/Res;

    .line 229
    invoke-virtual {v1}, Lcom/squareup/invoices/ui/SeriesDisplayState;->getTitle()I

    move-result v1

    invoke-interface {v3, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 228
    invoke-virtual {v0, v2, v1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setInvoiceDisplayStateText(ILjava/lang/String;)V

    .line 230
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->hideRecurringRow()V

    .line 232
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    .line 234
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/RecurringSeries;->recurrence_schedule:Lcom/squareup/protos/client/invoice/RecurringSchedule;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->withYearFormat:Ljava/text/DateFormat;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setFrequencyRow(Lcom/squareup/protos/client/invoice/RecurringSchedule;Ljava/text/DateFormat;)V

    .line 235
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->initializeDatesForSeries()V

    .line 236
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->showBottomButtons(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$7$InvoiceDetailPresenter(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/invoices/DisplayDetails;)V
    .locals 0

    .line 154
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->showDisplayDetails(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/invoices/DisplayDetails;)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$InvoiceDetailPresenter(Lcom/squareup/invoices/ui/InvoiceDetailView;)Lrx/Subscription;
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loadingOverInvoiceDetail()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$7VCNBSJdvCgFz9AEYV1tTZ9Bar8;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$7VCNBSJdvCgFz9AEYV1tTZ9Bar8;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailView;)V

    .line 123
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$InvoiceDetailPresenter(Lcom/squareup/invoices/ui/InvoiceDetailView;)Lrx/Subscription;
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->isFetchingInvoice()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$R3w9YebNDLw_Ff3c5m6wFeAQaVw;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$R3w9YebNDLw_Ff3c5m6wFeAQaVw;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailView;)V

    .line 130
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$InvoiceDetailPresenter(Lcom/squareup/invoices/ui/InvoiceDetailView;)Lrx/Subscription;
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getErrorGettingInvoice()Lrx/Observable;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$oqxb4oBTJlIBn5xQPW_eDTlwims;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$oqxb4oBTJlIBn5xQPW_eDTlwims;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailView;)V

    .line 141
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$8$InvoiceDetailPresenter(Lcom/squareup/invoices/ui/InvoiceDetailView;)Lrx/Subscription;
    .locals 2

    .line 152
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentDisplayDetails()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$JC3WfgXLQLEwZ4OndCg_0ouq8lM;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$JC3WfgXLQLEwZ4OndCg_0ouq8lM;

    .line 153
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$o1AZTDzsBLVM07FOAvRJ-d_bAYw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$o1AZTDzsBLVM07FOAvRJ-d_bAYw;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailPresenter;Lcom/squareup/invoices/ui/InvoiceDetailView;)V

    .line 154
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$updateSharedInvoiceTemplateSections$9$InvoiceDetailPresenter()Lkotlin/Unit;
    .locals 1

    .line 217
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->onCustomerClicked()V

    .line 218
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method onAddPaymentClicked()V
    .locals 1

    .line 309
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->addPayment()V

    return-void
.end method

.method onAttachmentClicked(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V
    .locals 1

    .line 317
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->onAttachmentClicked(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V

    return-void
.end method

.method onCustomerClicked()V
    .locals 1

    .line 321
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->goToCustomerFromDetail()V

    return-void
.end method

.method onDuplicateClicked()V
    .locals 1

    .line 305
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->duplicateInvoice()V

    return-void
.end method

.method onEditClicked()V
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->editCurrentInvoice()V

    return-void
.end method

.method public onEvent(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;)V
    .locals 1

    .line 464
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->onEvent(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 117
    invoke-super {p0, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 119
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceDetailView;

    .line 121
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$7HXxBwA0TgedXp9e7Cqe7BzYwTc;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$7HXxBwA0TgedXp9e7Cqe7BzYwTc;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailPresenter;Lcom/squareup/invoices/ui/InvoiceDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 128
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$jJH-6c2lOVMTos7lqM038_jtILc;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$jJH-6c2lOVMTos7lqM038_jtILc;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailPresenter;Lcom/squareup/invoices/ui/InvoiceDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 138
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$Lut5k1hL4MSp4KmEgoD44Ck5tgU;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$Lut5k1hL4MSp4KmEgoD44Ck5tgU;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailPresenter;Lcom/squareup/invoices/ui/InvoiceDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 151
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$1XYMhWa4iuS78l-i1AFdJU3nucI;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$1XYMhWa4iuS78l-i1AFdJU3nucI;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailPresenter;Lcom/squareup/invoices/ui/InvoiceDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method onSendReminderClicked()V
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->displaySendReminderConfirmation()V

    return-void
.end method

.method public updateInvoiceDisplayDetailsSections(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 0

    .line 207
    invoke-super {p0, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->updateInvoiceDisplayDetailsSections(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    .line 208
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->showBottomButtons(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    .line 209
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->showTimelineForInvoiceDisplayDetails(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    return-void
.end method

.method public updateSharedInvoiceTemplateSections(Lcom/squareup/protos/client/invoice/Invoice;)V
    .locals 4

    .line 213
    invoke-super {p0, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->updateSharedInvoiceTemplateSections(Lcom/squareup/protos/client/invoice/Invoice;)V

    .line 214
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceDetailView;

    .line 215
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_INVOICE_LINKING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/NameClickableState$Clickable;

    new-instance v3, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$NinFM_byls3Crzdj2mMFegPYkhk;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailPresenter$NinFM_byls3Crzdj2mMFegPYkhk;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailPresenter;)V

    invoke-direct {v2, v3}, Lcom/squareup/invoices/NameClickableState$Clickable;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-static {p1, v1, v2}, Lcom/squareup/invoices/InvoiceContactFormatter;->formatForDisplay(Lcom/squareup/protos/client/invoice/InvoiceContact;Landroid/content/Context;Lcom/squareup/invoices/NameClickableState;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 220
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->setBillToRow(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
