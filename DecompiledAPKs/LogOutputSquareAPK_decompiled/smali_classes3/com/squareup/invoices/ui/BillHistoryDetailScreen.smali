.class public Lcom/squareup/invoices/ui/BillHistoryDetailScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "BillHistoryDetailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;,
        Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;,
        Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/ui/BillHistoryDetailScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final billToken:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 199
    sget-object v0, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$n0DlCe0Tjl_Z4_MXchKY0meLaHE;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$n0DlCe0Tjl_Z4_MXchKY0meLaHE;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;->billToken:Ljava/lang/String;

    .line 45
    iput-object p2, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;->title:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/invoices/ui/BillHistoryDetailScreen;)Ljava/lang/String;
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;->getTitle()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/invoices/ui/BillHistoryDetailScreen;)Ljava/lang/String;
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;->getBillToken()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private getBillToken()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;->billToken:Ljava/lang/String;

    return-object v0
.end method

.method private getTitle()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;->title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/invoices/ui/BillHistoryDetailScreen;
    .locals 2

    .line 200
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 201
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 202
    new-instance v1, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 194
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 195
    iget-object p2, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;->billToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 196
    iget-object p2, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->INVOICES_BILL_HISTORY:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 49
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 57
    sget v0, Lcom/squareup/features/invoices/R$layout;->invoice_bill_history_view:I

    return v0
.end method
