.class public final Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;
.super Ljava/lang/Object;
.source "InvoiceIssueRefundStarter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceBillLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceBillLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceBillLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;->invoiceBillLoaderProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceBillLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;)",
            "Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/invoices/ui/InvoiceBillLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PermissionGatekeeper;)Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;-><init>(Lflow/Flow;Lcom/squareup/invoices/ui/InvoiceBillLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PermissionGatekeeper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;->invoiceBillLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/ui/InvoiceBillLoader;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;->newInstance(Lflow/Flow;Lcom/squareup/invoices/ui/InvoiceBillLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PermissionGatekeeper;)Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter_Factory;->get()Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;

    move-result-object v0

    return-object v0
.end method
