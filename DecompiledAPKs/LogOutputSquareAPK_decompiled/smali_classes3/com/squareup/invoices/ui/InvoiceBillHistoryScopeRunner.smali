.class public Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;
.super Ljava/lang/Object;
.source "InvoiceBillHistoryScopeRunner.java"

# interfaces
.implements Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;
.implements Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;


# instance fields
.field private final flow:Lflow/Flow;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

.field private final invoiceIssueRefundStarter:Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/invoices/ui/InvoiceBillLoader;Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->flow:Lflow/Flow;

    .line 44
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->res:Lcom/squareup/util/Res;

    .line 45
    iput-object p3, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    .line 46
    iput-object p4, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 47
    iput-object p5, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    .line 48
    iput-object p6, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->invoiceIssueRefundStarter:Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;

    return-void
.end method


# virtual methods
.method public billFromBillToken(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;",
            ">;"
        }
    .end annotation

    .line 115
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->loadedState(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public closeBillHistoryInvoice()V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeIssueReceiptScreen()V
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/activity/IssueReceiptScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeIssueRefundScreen()V
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/activity/InIssueRefundScope;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeIssueRefundScreen(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->loadFromBill(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 93
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->closeIssueRefundScreen()V

    return-void
.end method

.method public getBill()Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->getBillHistory()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentIdForReceipt()Ljava/lang/String;
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->getBillHistory()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->getTender()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/BillHistoryId;->forTender(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->getPaymentIdForReceipt()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTender()Lcom/squareup/billhistory/model/TenderHistory;
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->getBillHistory()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    .line 57
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->getBillHistory()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    :goto_0
    return-object v0
.end method

.method public goBack()V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public goBackBecauseBillIdChanged()V
    .locals 1

    .line 101
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onBillIdChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 61
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onTransactionsHistoryChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 65
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public printGiftReceiptForSelectedTender(Ljava/lang/String;)V
    .locals 4

    .line 73
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_PRINTER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->printing_hud:I

    .line 74
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 73
    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 75
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/BillHistory;->getTender(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p1

    .line 76
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    sget-object v2, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->GIFT_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-virtual {v0, v1, p1, v2}, Lcom/squareup/print/OrderPrintingDispatcher;->printReceipt(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V

    return-void
.end method

.method public reprintTicket()V
    .locals 4

    .line 143
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_PRINTER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->printing_hud:I

    .line 144
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 143
    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 145
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/print/OrderPrintingDispatcher;->reprintTicket(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method

.method public showIssueReceiptScreen()V
    .locals 4

    .line 127
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->getBillHistory()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    const-string v1, "bill"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 128
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/IssueReceiptScreen;

    sget-object v2, Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/activity/IssueReceiptScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Z)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showIssueReceiptScreen(Ljava/lang/String;)V
    .locals 0

    .line 80
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public showIssueRefundScreen()V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->invoiceIssueRefundStarter:Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;->showIssueRefundScreen()V

    return-void
.end method

.method public showSelectBuyerLanguageScreen()V
    .locals 1

    .line 109
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public showStartRefundScreen(Ljava/lang/String;)V
    .locals 0

    .line 88
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public startPrintGiftReceiptFlow()V
    .locals 3

    .line 132
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->getBillHistory()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    const-string v1, "bill"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 133
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->getBillHistory()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    const-string v1, "tenderHistoryList"

    .line 134
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    .line 135
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 136
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen;

    sget-object v2, Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceBillHistoryScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 138
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    iget-object v0, v0, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoiceBillHistoryScopeRunner;->printGiftReceiptForSelectedTender(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
