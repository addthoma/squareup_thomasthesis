.class public Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;
.super Lcom/squareup/ui/DropDownContainer;
.source "InvoiceFilterDropDownView.java"


# instance fields
.field presenter:Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/DropDownContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected inject()V
    .locals 2

    .line 19
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/invoices/ui/InvoiceHistoryScreen$Component;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceHistoryScreen$Component;

    invoke-interface {v0, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryScreen$Component;->inject(Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;->presenter:Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->dropView(Ljava/lang/Object;)V

    .line 29
    invoke-super {p0}, Lcom/squareup/ui/DropDownContainer;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 23
    invoke-super {p0}, Lcom/squareup/ui/DropDownContainer;->onFinishInflate()V

    .line 24
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;->presenter:Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
