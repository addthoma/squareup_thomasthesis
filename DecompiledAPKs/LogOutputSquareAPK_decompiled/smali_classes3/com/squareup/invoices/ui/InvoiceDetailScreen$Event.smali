.class public final enum Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;
.super Ljava/lang/Enum;
.source "InvoiceDetailScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0011\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;",
        "",
        "(Ljava/lang/String;I)V",
        "ISSUE_REFUND",
        "SHARE",
        "SEND_REMINDER",
        "DUPLICATE",
        "VIEW_TRANSACTION",
        "CANCEL",
        "MORE",
        "VIEW_IN_SERIES",
        "END_SERIES",
        "TIMELINE_CTA",
        "TIMELINE_VIEW_ALL_ACTIVITY",
        "DOWNLOAD_INVOICE",
        "DELETE_DRAFT",
        "ARCHIVE",
        "UNARCHIVE",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum ARCHIVE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum CANCEL:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum DELETE_DRAFT:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum DOWNLOAD_INVOICE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum DUPLICATE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum END_SERIES:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum ISSUE_REFUND:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum MORE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum SEND_REMINDER:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum SHARE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum TIMELINE_CTA:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum TIMELINE_VIEW_ALL_ACTIVITY:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum UNARCHIVE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum VIEW_IN_SERIES:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

.field public static final enum VIEW_TRANSACTION:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/4 v2, 0x0

    const-string v3, "ISSUE_REFUND"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ISSUE_REFUND:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/4 v2, 0x1

    const-string v3, "SHARE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->SHARE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/4 v2, 0x2

    const-string v3, "SEND_REMINDER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->SEND_REMINDER:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/4 v2, 0x3

    const-string v3, "DUPLICATE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->DUPLICATE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/4 v2, 0x4

    const-string v3, "VIEW_TRANSACTION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->VIEW_TRANSACTION:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/4 v2, 0x5

    const-string v3, "CANCEL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->CANCEL:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/4 v2, 0x6

    const-string v3, "MORE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->MORE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/4 v2, 0x7

    const-string v3, "VIEW_IN_SERIES"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->VIEW_IN_SERIES:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/16 v2, 0x8

    const-string v3, "END_SERIES"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->END_SERIES:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/16 v2, 0x9

    const-string v3, "TIMELINE_CTA"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->TIMELINE_CTA:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/16 v2, 0xa

    const-string v3, "TIMELINE_VIEW_ALL_ACTIVITY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->TIMELINE_VIEW_ALL_ACTIVITY:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/16 v2, 0xb

    const-string v3, "DOWNLOAD_INVOICE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->DOWNLOAD_INVOICE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/16 v2, 0xc

    const-string v3, "DELETE_DRAFT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->DELETE_DRAFT:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/16 v2, 0xd

    const-string v3, "ARCHIVE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ARCHIVE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/16 v2, 0xe

    const-string v3, "UNARCHIVE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->UNARCHIVE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->$VALUES:[Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;
    .locals 1

    const-class v0, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;
    .locals 1

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->$VALUES:[Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    invoke-virtual {v0}, [Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    return-object v0
.end method
