.class public final Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;
.super Ljava/lang/Object;
.source "OtherAmountDialogFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tJ*\u0010\n\u001a\u00020\u000b2\"\u0010\u000c\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000ej\u0008\u0012\u0004\u0012\u00020\u000f`\u00110\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;",
        "",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;)V",
        "build",
        "Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iput-object p3, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public final build(Lio/reactivex/Observable;)Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;

    iget-object v2, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v3, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v4, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    const/4 v6, 0x0

    move-object v1, v0

    move-object v5, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lio/reactivex/Observable;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
