.class public final Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunnerKt;
.super Ljava/lang/Object;
.source "InvoicePaymentAmountWorkflowRunner.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "getWorkflowRunner",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;",
        "scope",
        "Lmortar/MortarScope;",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getWorkflowRunner(Lmortar/MortarScope;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;
    .locals 2

    const-string v0, "scope"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    const-class v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InvoicePaymentAmountWork\u2026owRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/squareup/container/PosWorkflowRunnerKt;->getWorkflowRunner(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/PosWorkflowRunner;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;

    return-object p0
.end method
