.class final Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner$onEnterScope$1;
.super Ljava/lang/Object;
.source "EditInvoiceLoadingScreenRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "status",
        "Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen;

.field final synthetic this$0:Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner$onEnterScope$1;->this$0:Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;

    iput-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner$onEnterScope$1;->$screen:Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;)V
    .locals 5

    .line 30
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner$onEnterScope$1;->this$0:Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;

    invoke-static {v0}, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;->access$getFlow$p(Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;)Lflow/Flow;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner$onEnterScope$1;->$screen:Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen;->getNextScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 31
    instance-of v0, p1, Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus$Failed;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner$onEnterScope$1;->this$0:Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;

    invoke-static {v0}, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;->access$getFlow$p(Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;)Lflow/Flow;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    .line 34
    new-instance v2, Lcom/squareup/widgets/warning/WarningStrings;

    .line 35
    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner$onEnterScope$1;->this$0:Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;

    invoke-static {v3}, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;->access$getRes$p(Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;)Lcom/squareup/util/Res;

    move-result-object v3

    sget v4, Lcom/squareup/features/invoices/R$string;->invoice_update_settings_failure_title:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 36
    check-cast p1, Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus$Failed;

    invoke-virtual {p1}, Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus$Failed;->getFailureMessage()Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    .line 34
    invoke-direct {v2, v3, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/widgets/warning/Warning;

    .line 33
    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    .line 32
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner$onEnterScope$1;->accept(Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;)V

    return-void
.end method
