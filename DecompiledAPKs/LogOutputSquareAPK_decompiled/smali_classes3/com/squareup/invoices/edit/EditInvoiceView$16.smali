.class Lcom/squareup/invoices/edit/EditInvoiceView$16;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "EditInvoiceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/edit/EditInvoiceView;->addAdditionalFileAttachmentRowView(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/edit/EditInvoiceView;

.field final synthetic val$fileAttachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V
    .locals 0

    .line 634
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView$16;->this$0:Lcom/squareup/invoices/edit/EditInvoiceView;

    iput-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceView$16;->val$fileAttachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 636
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView$16;->this$0:Lcom/squareup/invoices/edit/EditInvoiceView;

    iget-object p1, p1, Lcom/squareup/invoices/edit/EditInvoiceView;->presenter:Lcom/squareup/invoices/edit/EditInvoicePresenter;

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView$16;->val$fileAttachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->goToEditFileAttachment(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V

    return-void
.end method
