.class public final synthetic Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$LlSjTXE0GFfJWUFx3q2vCchBhgM;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/BiConsumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$LlSjTXE0GFfJWUFx3q2vCchBhgM;->f$0:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$LlSjTXE0GFfJWUFx3q2vCchBhgM;->f$0:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    check-cast p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    check-cast p2, Ljava/util/List;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->lambda$goToAutomaticRemindersScreen$12$EditInvoiceScopeRunner(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;Ljava/util/List;)V

    return-void
.end method
