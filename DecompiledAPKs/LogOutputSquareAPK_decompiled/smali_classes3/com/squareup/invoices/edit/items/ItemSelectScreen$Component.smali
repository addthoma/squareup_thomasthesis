.class public interface abstract Lcom/squareup/invoices/edit/items/ItemSelectScreen$Component;
.super Ljava/lang/Object;
.source "ItemSelectScreen.kt"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListView$Component;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/invoices/edit/items/ItemSelectScreen;
.end annotation

.annotation runtime Lcom/squareup/librarylist/SimpleLibraryListModule$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/librarylist/SimpleLibraryListModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/items/ItemSelectScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/invoices/edit/items/ItemSelectScreen$Component;",
        "Lcom/squareup/librarylist/LibraryListView$Component;",
        "inject",
        "",
        "view",
        "Lcom/squareup/invoices/edit/items/ItemSelectCardView;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/invoices/edit/items/ItemSelectCardView;)V
.end method
