.class public final Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$1;
.super Landroid/webkit/WebChromeClient;
.source "InvoicePreviewCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "com/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$1",
        "Landroid/webkit/WebChromeClient;",
        "onProgressChanged",
        "",
        "view",
        "Landroid/webkit/WebView;",
        "newProgress",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 44
    iput-object p1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$1;->this$0:Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$1;->this$0:Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->access$getWebProgressBar$p(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 50
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$1;->this$0:Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->access$getWebProgressBar$p(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;)Landroid/widget/ProgressBar;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$1;->this$0:Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;

    invoke-static {v1}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->access$getWebProgressBar$p(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getMax()I

    move-result v1

    if-eq p2, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 51
    invoke-super {p0, p1, p2}, Landroid/webkit/WebChromeClient;->onProgressChanged(Landroid/webkit/WebView;I)V

    return-void
.end method
