.class public Lcom/squareup/invoices/edit/EditInvoiceScreen;
.super Lcom/squareup/invoices/edit/InEditInvoiceScope;
.source "EditInvoiceScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/invoices/edit/EditInvoiceScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/edit/EditInvoiceScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/edit/EditInvoiceScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final SHOWN:Ljava/lang/String; = "Shown EditInvoiceScreen"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 47
    sget-object v0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScreen$dD_XIiCDQgmPaMS5h_rvlMiLLy4;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScreen$dD_XIiCDQgmPaMS5h_rvlMiLLy4;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/edit/EditInvoiceScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/InEditInvoiceScope;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/invoices/edit/EditInvoiceScreen;
    .locals 1

    .line 48
    const-class v0, Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/edit/EditInvoiceScope;

    .line 49
    new-instance v0, Lcom/squareup/invoices/edit/EditInvoiceScreen;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/EditInvoiceScreen;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 39
    invoke-super {p0, p1, p2}, Lcom/squareup/invoices/edit/InEditInvoiceScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 40
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScreen;->editInvoicePath:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScreen;->editInvoicePath:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScope;->getEditInvoiceContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->INVOICES_CREATION_FORM:Lcom/squareup/analytics/RegisterViewName;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->UNKNOWN:Lcom/squareup/analytics/RegisterViewName;

    :goto_0
    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 35
    sget v0, Lcom/squareup/features/invoices/R$layout;->edit_invoice_view:I

    return v0
.end method
