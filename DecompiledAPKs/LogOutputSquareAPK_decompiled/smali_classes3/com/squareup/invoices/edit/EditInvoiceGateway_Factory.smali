.class public final Lcom/squareup/invoices/edit/EditInvoiceGateway_Factory;
.super Ljava/lang/Object;
.source "EditInvoiceGateway_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/edit/EditInvoiceGateway;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/edit/EditInvoiceGateway_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/invoices/edit/EditInvoiceGateway_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/invoices/edit/EditInvoiceGateway_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceGateway_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Ldagger/Lazy;)Lcom/squareup/invoices/edit/EditInvoiceGateway;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/invoices/edit/EditInvoiceGateway;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/invoices/edit/EditInvoiceGateway;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceGateway;-><init>(Lcom/squareup/settings/server/Features;Ldagger/Lazy;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/edit/EditInvoiceGateway;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/invoices/edit/EditInvoiceGateway_Factory;->newInstance(Lcom/squareup/settings/server/Features;Ldagger/Lazy;)Lcom/squareup/invoices/edit/EditInvoiceGateway;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceGateway_Factory;->get()Lcom/squareup/invoices/edit/EditInvoiceGateway;

    move-result-object v0

    return-object v0
.end method
