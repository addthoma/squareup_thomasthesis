.class public final Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;
.super Ljava/lang/Object;
.source "EditInvoicePresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/edit/EditInvoicePresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final cartEntryViewsFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final currentTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final editPaymentRequestRowDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final errorBarPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final formatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final invoiceTutorialRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentRequestDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PaymentRequestData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field

.field private final updateCustomerFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PaymentRequestData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 84
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->cartEntryViewsFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 85
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 86
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->errorBarPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 87
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 88
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 89
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 90
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->formatterProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 91
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 92
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 93
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 94
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 95
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 96
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 97
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->invoiceTutorialRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 98
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 99
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->updateCustomerFlowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 100
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->paymentRequestDataFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 101
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->editPaymentRequestRowDataFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PaymentRequestData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;",
            ">;)",
            "Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    .line 124
    new-instance v19, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;

    move-object/from16 v0, v19

    invoke-direct/range {v0 .. v18}, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v19
.end method

.method public static newInstance(Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/time/CurrentTime;Lcom/squareup/crm/CustomerManagementSettings;Lflow/Flow;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/invoices/PaymentRequestData$Factory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;)Lcom/squareup/invoices/edit/EditInvoicePresenter;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;",
            "Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            "Lflow/Flow;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            "Lcom/squareup/invoices/PaymentRequestData$Factory;",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;",
            ")",
            "Lcom/squareup/invoices/edit/EditInvoicePresenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    .line 136
    new-instance v19, Lcom/squareup/invoices/edit/EditInvoicePresenter;

    move-object/from16 v0, v19

    invoke-direct/range {v0 .. v18}, Lcom/squareup/invoices/edit/EditInvoicePresenter;-><init>(Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/time/CurrentTime;Lcom/squareup/crm/CustomerManagementSettings;Lflow/Flow;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/invoices/PaymentRequestData$Factory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;)V

    return-object v19
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/edit/EditInvoicePresenter;
    .locals 20

    move-object/from16 v0, p0

    .line 106
    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->cartEntryViewsFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->errorBarPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Ljava/text/DateFormat;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->formatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/time/CurrentTime;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->invoiceTutorialRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->updateCustomerFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->paymentRequestDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/invoices/PaymentRequestData$Factory;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->editPaymentRequestRowDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;

    invoke-static/range {v2 .. v19}, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->newInstance(Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/time/CurrentTime;Lcom/squareup/crm/CustomerManagementSettings;Lflow/Flow;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/invoices/PaymentRequestData$Factory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;)Lcom/squareup/invoices/edit/EditInvoicePresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter_Factory;->get()Lcom/squareup/invoices/edit/EditInvoicePresenter;

    move-result-object v0

    return-object v0
.end method
