.class Lcom/squareup/invoices/edit/EditInvoiceView$InvoiceTextWatcher;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditInvoiceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/EditInvoiceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InvoiceTextWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/edit/EditInvoiceView;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V
    .locals 0

    .line 708
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView$InvoiceTextWatcher;->this$0:Lcom/squareup/invoices/edit/EditInvoiceView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .line 710
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView$InvoiceTextWatcher;->this$0:Lcom/squareup/invoices/edit/EditInvoiceView;

    iget-object p1, p1, Lcom/squareup/invoices/edit/EditInvoiceView;->presenter:Lcom/squareup/invoices/edit/EditInvoicePresenter;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->textChanged()V

    return-void
.end method
