.class public final Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;
.super Ljava/lang/Object;
.source "InvoiceChooseDateInfoFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceChooseDateInfoFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceChooseDateInfoFactory.kt\ncom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory\n*L\n1#1,160:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010 \n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J(\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0015\u001a\u00020\u0016J\"\u0010\u0017\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0018\u0010\u0018\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0016\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\nJ,\u0010\u001d\u001a\u00020\u000e2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010\u001c\u001a\u00020\n2\u0006\u0010 \u001a\u00020!2\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u001f0#R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;",
        "",
        "invoiceDateOptions",
        "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;",
        "res",
        "Lcom/squareup/util/Res;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "(Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;)V",
        "today",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "getToday",
        "()Lcom/squareup/protos/common/time/YearMonthDay;",
        "createForInvoice",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;",
        "type",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        "displayDetails",
        "Lcom/squareup/invoices/DisplayDetails;",
        "isRecurring",
        "",
        "createForInvoiceDue",
        "createForInvoiceScheduled",
        "createForPaymentRequest",
        "paymentRequestType",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;",
        "invoiceFirstSentDate",
        "createForPaymentRequestV2",
        "paymentRequest",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "index",
        "",
        "paymentRequests",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final invoiceDateOptions:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "invoiceDateOptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->invoiceDateOptions:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->currentTime:Lcom/squareup/time/CurrentTime;

    return-void
.end method

.method private final createForInvoiceDue(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/invoices/DisplayDetails;Z)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;
    .locals 9

    .line 117
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_choose_due_date:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 118
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->getToday()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    invoke-static {p2, p1, v0}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    .line 119
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->getToday()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/util/YearMonthDaysKt;->isBefore(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->getToday()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    move-object v5, v0

    goto :goto_0

    :cond_0
    move-object v5, p2

    .line 120
    :goto_0
    invoke-static {p1, p2}, Lcom/squareup/invoices/Invoices;->getRemainderDueOn(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v6

    .line 121
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->invoiceDateOptions:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;

    invoke-virtual {p1, v5}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->forInvoiceDue(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;

    move-result-object v4

    .line 123
    new-instance p1, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    const/4 v7, 0x0

    xor-int/lit8 v8, p3, 0x1

    move-object v2, p1

    invoke-direct/range {v2 .. v8}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Z)V

    return-object p1
.end method

.method private final createForInvoiceScheduled(Lcom/squareup/protos/client/invoice/Invoice$Builder;Z)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;
    .locals 8

    if-eqz p2, :cond_0

    .line 138
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_choose_start_date:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v2, v0

    goto :goto_1

    .line 140
    :cond_0
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v0, v1, :cond_1

    .line 141
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_choose_charge_date:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_choose_send_date:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 146
    :goto_1
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v4

    .line 147
    iget-object v5, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 148
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->invoiceDateOptions:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;

    invoke-virtual {p1, v4}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->forInvoiceScheduled(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;

    move-result-object v3

    .line 150
    new-instance p1, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    const/4 v6, 0x0

    xor-int/lit8 v7, p2, 0x1

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Z)V

    return-object p1
.end method

.method private final getToday()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final createForInvoice(Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/invoices/DisplayDetails;Z)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoice"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    sget-object v0, Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Scheduled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Scheduled;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2, p4}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->createForInvoiceScheduled(Lcom/squareup/protos/client/invoice/Invoice$Builder;Z)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-result-object p1

    goto :goto_0

    .line 49
    :cond_0
    sget-object v0, Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Due;->INSTANCE:Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Due;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0, p2, p3, p4}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->createForInvoiceDue(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/invoices/DisplayDetails;Z)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final createForPaymentRequest(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;
    .locals 10

    const-string v0, "paymentRequestType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceFirstSentDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    .line 91
    instance-of v1, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->payment_request_balance_due_date:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 92
    :cond_0
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->payment_request_deposit_due_date:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object v4, v2

    .line 95
    instance-of v2, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    if-eqz v2, :cond_2

    .line 96
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->getToday()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/squareup/util/YearMonthDaysKt;->isBefore(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->getToday()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    :goto_1
    move-object v6, p1

    goto :goto_2

    :cond_1
    move-object v6, p2

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_4

    .line 99
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->getDepositDueDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->getToday()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/util/YearMonthDaysKt;->isBefore(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->getToday()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    goto :goto_1

    .line 100
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->getDepositDueDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    goto :goto_1

    .line 102
    :goto_2
    new-instance p1, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    .line 104
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->invoiceDateOptions:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;

    iget-object v2, v0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    const-string v3, "paymentRequest.amount_type"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v6, v2}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->forPaymentRequest(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)Ljava/util/List;

    move-result-object v5

    .line 106
    invoke-static {v0, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object v3, p1

    .line 102
    invoke-direct/range {v3 .. v9}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Z)V

    return-object p1

    .line 99
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final createForPaymentRequestV2(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;ILjava/util/List;)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "I",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;"
        }
    .end annotation

    const-string v0, "paymentRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceFirstSentDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentRequests"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eqz v0, :cond_4

    sget-object v1, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 p3, 0x5

    if-ne v0, p3, :cond_0

    .line 68
    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->res:Lcom/squareup/util/Res;

    sget p4, Lcom/squareup/features/invoices/R$string;->payment_request_balance_due_date:I

    invoke-interface {p3, p4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    .line 69
    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->payment_request_payment_due_date:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 64
    invoke-static {p4, p3}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateInstallmentNumber(Ljava/util/List;I)I

    move-result p3

    const-string p4, "number"

    invoke-virtual {v0, p4, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 65
    invoke-virtual {p3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p3

    .line 66
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    .line 61
    :cond_2
    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->res:Lcom/squareup/util/Res;

    sget p4, Lcom/squareup/features/invoices/R$string;->payment_request_deposit_due_date:I

    invoke-interface {p3, p4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    :goto_0
    move-object v1, p3

    .line 72
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->getToday()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p3

    invoke-static {p2, p3}, Lcom/squareup/util/YearMonthDaysKt;->isBefore(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result p3

    if-eqz p3, :cond_3

    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->getToday()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p3

    move-object v3, p3

    goto :goto_1

    :cond_3
    move-object v3, p2

    .line 73
    :goto_1
    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->invoiceDateOptions:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;

    iget-object p4, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    const-string v0, "paymentRequest.amount_type"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3, v3, p4}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceDateOptions;->forPaymentRequestV2(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)Ljava/util/List;

    move-result-object v2

    .line 74
    new-instance p3, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    .line 78
    invoke-static {p1, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p3

    .line 74
    invoke-direct/range {v0 .. v6}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Z)V

    return-object p3

    .line 69
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "payment request must have amount type"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
