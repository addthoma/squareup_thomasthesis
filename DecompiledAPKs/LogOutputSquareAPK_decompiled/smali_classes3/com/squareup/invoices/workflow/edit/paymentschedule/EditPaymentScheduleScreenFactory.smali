.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;
.super Ljava/lang/Object;
.source "EditPaymentScheduleScreenFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentScheduleScreenFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentScheduleScreenFactory.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,334:1\n1360#2:335\n1429#2,3:336\n1370#2:339\n1401#2,4:340\n1550#2,3:344\n215#2,2:347\n1529#2,3:349\n1370#2:352\n1401#2,4:353\n1577#2,4:357\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentScheduleScreenFactory.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory\n*L\n75#1:335\n75#1,3:336\n106#1:339\n106#1,4:340\n202#1,3:344\n205#1,2:347\n236#1,3:349\n241#1:352\n241#1,4:353\n295#1,4:357\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u0000 ;2\u00020\u0001:\u0002;<B9\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u00b8\u0001\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u00112\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u001f0\u001d2\u0012\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u001f0\u001d2\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\u001f0\u001d2\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020$\u0012\u0004\u0012\u00020\u001f0\u001d2\u0018\u0010%\u001a\u0014\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u001f0&2\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u001f0(2\u000c\u0010)\u001a\u0008\u0012\u0004\u0012\u00020\u001f0(2\u000c\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\u001f0(J$\u0010+\u001a\u0008\u0012\u0004\u0012\u00020,0\u00112\u000c\u0010-\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J&\u0010.\u001a\u00020/2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011H\u0002J&\u00100\u001a\u00020/2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011H\u0002J&\u00101\u001a\u00020/2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011H\u0002J\u0008\u00102\u001a\u00020/H\u0002J\u0014\u00103\u001a\u000204*\u00020\u00122\u0006\u00105\u001a\u00020\u001aH\u0002J \u00106\u001a\u0008\u0012\u0004\u0012\u0002070\u0011*\u0008\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u000e\u00108\u001a\u0004\u0018\u000104*\u00020\u0012H\u0002J\u001c\u00109\u001a\u00020,*\u00020\u00122\u0006\u0010:\u001a\u0002072\u0006\u0010\u0019\u001a\u00020\u001aH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000f\u001a\u00020\u0010*\u0008\u0012\u0004\u0012\u00020\u00120\u00118BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006="
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;",
        "",
        "numberAmountFactory",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;",
        "balanceSplitHelperTextFactory",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;",
        "depositHelperTextFactory",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;",
        "res",
        "Lcom/squareup/util/Res;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/settings/server/Features;)V",
        "installmentsCount",
        "",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "getInstallmentsCount",
        "(Ljava/util/List;)I",
        "fromPaymentRequests",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;",
        "invoiceAmount",
        "Lcom/squareup/protos/common/Money;",
        "invoiceFirstSentDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "paymentRequests",
        "toggleRequestDeposit",
        "Lkotlin/Function1;",
        "",
        "",
        "toggleBalanceSplit",
        "textUpdated",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent;",
        "paymentRequestClicked",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked;",
        "amountTypeChanged",
        "Lkotlin/Function2;",
        "addInstallment",
        "Lkotlin/Function0;",
        "cancelClicked",
        "saveClicked",
        "installmentsListToRowData",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;",
        "installmentRows",
        "screenForBalance",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;",
        "screenForDepositInstallments",
        "screenForDepositRemainder",
        "screenForFullInvoice",
        "dueString",
        "",
        "firstSentDate",
        "numberAmountDepositBalance",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;",
        "reminderText",
        "toRowData",
        "numberAmount",
        "Companion",
        "ScreenData",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final INSTALLMENTS_LIMIT:I = 0x8


# instance fields
.field private final balanceSplitHelperTextFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;

.field private final dateFormat:Ljava/text/DateFormat;

.field private final depositHelperTextFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final numberAmountFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->Companion:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .param p5    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "numberAmountFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceSplitHelperTextFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositHelperTextFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->numberAmountFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->balanceSplitHelperTextFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->depositHelperTextFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->dateFormat:Ljava/text/DateFormat;

    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private final dueString(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;
    .locals 5

    .line 281
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    .line 282
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->payment_request_due_upon_receipt:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 284
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->payment_request_due_in:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 285
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "number_of_days"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 286
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 287
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 289
    :goto_1
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->dateFormat:Ljava/text/DateFormat;

    invoke-static {p1, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/ProtoDates;->calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Calendar;

    move-result-object p1

    const-string p2, "ProtoDates.calendarFromY\u2026teDueDate(firstSentDate))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    .line 291
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " ("

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x29

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final getInstallmentsCount(Ljava/util/List;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)I"
        }
    .end annotation

    .line 295
    check-cast p1, Ljava/lang/Iterable;

    .line 357
    instance-of v0, p1, Ljava/util/Collection;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 359
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 295
    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v1, v1, 0x1

    if-gez v1, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_0

    :cond_2
    :goto_1
    return v1
.end method

.method private final installmentsListToRowData(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 236
    move-object v2, v1

    check-cast v2, Ljava/lang/Iterable;

    .line 349
    instance-of v3, v2, Ljava/util/Collection;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_1

    move-object v3, v2

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v3, 0x1

    goto :goto_0

    .line 350
    :cond_1
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 236
    invoke-static {v6}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_7

    .line 239
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v3, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->FIXED_INSTALLMENT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne v1, v3, :cond_3

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    .line 352
    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 354
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    add-int/lit8 v7, v3, 0x1

    if-gez v3, :cond_4

    .line 355
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_4
    check-cast v6, Lcom/squareup/protos/client/invoice/PaymentRequest;

    const/4 v3, 0x0

    const/4 v8, 0x2

    if-eqz v4, :cond_5

    .line 243
    iget-object v9, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->numberAmountFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;

    iget-object v10, v6, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    const-string v11, "paymentRequest.fixed_amount"

    invoke-static {v10, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v9, v10, v5, v8, v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;->from$default(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;Lcom/squareup/protos/common/Money;ZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    move-result-object v3

    goto :goto_3

    .line 245
    :cond_5
    iget-object v9, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->numberAmountFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;

    iget-object v10, v6, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    long-to-int v11, v10

    invoke-static {v9, v11, v5, v8, v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;->from$default(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;IZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    move-result-object v3

    :goto_3
    move-object v9, v3

    .line 248
    iget-object v3, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->res:Lcom/squareup/util/Res;

    sget v8, Lcom/squareup/common/invoices/R$string;->payment_request_number:I

    invoke-interface {v3, v8}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    const-string v8, "number"

    .line 249
    invoke-virtual {v3, v8, v7}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 250
    invoke-virtual {v3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v3

    .line 251
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 253
    new-instance v3, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;

    move-object/from16 v15, p2

    .line 256
    invoke-direct {v0, v6, v15}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->dueString(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;

    move-result-object v11

    .line 257
    invoke-direct {v0, v6}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->reminderText(Lcom/squareup/protos/client/invoice/PaymentRequest;)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    const/16 v14, 0x10

    const/4 v6, 0x0

    move-object v8, v3

    move-object v15, v6

    .line 253
    invoke-direct/range {v8 .. v15}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 258
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v3, v7

    goto :goto_2

    .line 356
    :cond_6
    check-cast v1, Ljava/util/List;

    return-object v1

    .line 237
    :cond_7
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "installment rows must only contain installments"

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method private final numberAmountDepositBalance(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;",
            ">;"
        }
    .end annotation

    .line 202
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 344
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 345
    :cond_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 202
    invoke-static {v4}, Lcom/squareup/invoices/PaymentRequestsKt;->isDepositRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_6

    .line 347
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 205
    invoke-static {v1}, Lcom/squareup/invoices/PaymentRequestsKt;->isDepositRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 207
    iget-object v0, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v4, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->FIXED_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    const/4 v5, 0x0

    const/4 v6, 0x2

    if-ne v0, v4, :cond_4

    new-array v0, v6, [Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    .line 209
    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->numberAmountFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    const-string v7, "deposit.fixed_amount"

    invoke-static {v1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, v1, v3, v6, v5}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;->from$default(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;Lcom/squareup/protos/common/Money;ZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    move-result-object v1

    aput-object v1, v0, v3

    .line 210
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->numberAmountFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;

    invoke-static {p1, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateBalanceAmount(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v1, p1, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;->from(Lcom/squareup/protos/common/Money;Z)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    move-result-object p1

    aput-object p1, v0, v2

    .line 208
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_1

    :cond_4
    new-array p1, v6, [Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    .line 214
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->numberAmountFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;

    iget-object v0, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    long-to-int v0, v7

    invoke-static {p2, v0, v3, v6, v5}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;->from$default(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;IZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    move-result-object p2

    aput-object p2, p1, v3

    .line 215
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->numberAmountFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;

    iget-object v0, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v1, v0

    rsub-int/lit8 v0, v1, 0x64

    invoke-virtual {p2, v0, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;->from(IZ)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    move-result-object p2

    aput-object p2, p1, v2

    .line 213
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    :goto_1
    return-object p1

    .line 348
    :cond_5
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string p2, "Collection contains no element matching the predicate."

    invoke-direct {p1, p2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 203
    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Need a deposit request."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final reminderText(Lcom/squareup/protos/client/invoice/PaymentRequest;)Ljava/lang/String;
    .locals 2

    .line 263
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_SHOW_PAYMENT_SCHEDULE_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 264
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    const-string v0, "reminders"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->reminders_scheduled_description:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "count"

    .line 270
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 271
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 272
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 268
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->reminder_scheduled_description:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 267
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->reminders_off_description:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final screenForBalance(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;"
        }
    .end annotation

    .line 179
    invoke-direct {p0, p3, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->installmentsListToRowData(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;

    move-result-object p2

    .line 180
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;->getNumberAmount()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$PercentageAmount;

    .line 182
    new-instance v9, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;

    .line 186
    new-instance v5, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;

    .line 188
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->balanceSplitHelperTextFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;

    invoke-virtual {v1, p3, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;->from(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;

    move-result-object p1

    .line 189
    invoke-direct {p0, p3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->getInstallmentsCount(Ljava/util/List;)I

    move-result p3

    const/16 v1, 0x8

    if-ge p3, v1, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    .line 186
    :goto_0
    invoke-direct {v5, v0, p2, p1, p3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;-><init>(ZLjava/util/List;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;Z)V

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object v1, v9

    .line 182
    invoke-direct/range {v1 .. v8}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;-><init>(ZZLcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositSection;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v9
.end method

.method private final screenForDepositInstallments(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    const/4 v4, 0x0

    .line 131
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 132
    iget-object v6, v5, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v7, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    const/4 v8, 0x1

    if-ne v6, v7, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    .line 134
    :goto_0
    invoke-direct {v0, v3, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->numberAmountDepositBalance(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v7

    .line 135
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    .line 136
    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    move-object v11, v7

    check-cast v11, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    .line 137
    iget-object v7, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v10, Lcom/squareup/settings/server/Features$Feature;->INVOICES_SHOW_PAYMENT_SCHEDULE_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v7, v10}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 138
    iget-object v7, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->res:Lcom/squareup/util/Res;

    sget v10, Lcom/squareup/features/invoices/R$string;->multiple_reminder_schedules:I

    invoke-interface {v7, v10}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    :goto_1
    move-object v14, v7

    .line 142
    invoke-direct {v0, v5, v9, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->toRowData(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;

    move-result-object v5

    .line 143
    new-instance v7, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;

    .line 145
    iget-object v9, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->res:Lcom/squareup/util/Res;

    .line 146
    sget v10, Lcom/squareup/common/invoices/R$string;->partial_payments_balance:I

    .line 145
    invoke-interface {v9, v10}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 148
    iget-object v9, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->res:Lcom/squareup/util/Res;

    sget v10, Lcom/squareup/features/invoices/R$string;->multiple_due_dates:I

    invoke-interface {v9, v10}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v15, 0x0

    move-object v10, v7

    .line 143
    invoke-direct/range {v10 .. v15}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 154
    move-object v9, v3

    check-cast v9, Ljava/lang/Iterable;

    invoke-static {v9, v8}, Lkotlin/collections/CollectionsKt;->drop(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v9

    .line 155
    invoke-direct {v0, v9, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->installmentsListToRowData(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;

    move-result-object v2

    .line 156
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;

    invoke-virtual {v9}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;->getNumberAmount()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    move-result-object v9

    instance-of v9, v9, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$PercentageAmount;

    .line 158
    new-instance v18, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;

    const/4 v11, 0x1

    const/4 v12, 0x1

    .line 161
    new-instance v13, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositSection;

    .line 163
    iget-object v10, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->depositHelperTextFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;

    invoke-virtual {v10, v3, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;->from(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;

    move-result-object v10

    .line 161
    invoke-direct {v13, v6, v5, v7, v10}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositSection;-><init>(ZLcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;)V

    .line 165
    new-instance v14, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;

    .line 167
    iget-object v5, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->balanceSplitHelperTextFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;

    invoke-virtual {v5, v3, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;->from(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;

    move-result-object v1

    .line 168
    invoke-direct {v0, v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->getInstallmentsCount(Ljava/util/List;)I

    move-result v3

    const/16 v5, 0x8

    if-ge v3, v5, :cond_2

    const/4 v4, 0x1

    .line 165
    :cond_2
    invoke-direct {v14, v9, v2, v1, v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;-><init>(ZLjava/util/List;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;Z)V

    const/4 v15, 0x0

    const/16 v16, 0x10

    const/16 v17, 0x0

    move-object/from16 v10, v18

    .line 158
    invoke-direct/range {v10 .. v17}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;-><init>(ZZLcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositSection;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v18
.end method

.method private final screenForDepositRemainder(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;"
        }
    .end annotation

    .line 105
    invoke-direct {p0, p3, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->numberAmountDepositBalance(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v0

    .line 106
    move-object v1, p3

    check-cast v1, Ljava/lang/Iterable;

    .line 339
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 341
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    add-int/lit8 v6, v4, 0x1

    if-gez v4, :cond_0

    .line 342
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v5, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 107
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    invoke-direct {p0, v5, v4, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->toRowData(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v4, v6

    goto :goto_0

    .line 343
    :cond_1
    check-cast v2, Ljava/util/List;

    .line 110
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;

    const/4 v0, 0x1

    .line 111
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;

    .line 112
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;->getNumberAmount()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$PercentageAmount;

    .line 114
    new-instance v10, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 117
    new-instance v5, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositSection;

    .line 119
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->depositHelperTextFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;

    invoke-virtual {v2, p3, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;->from(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;

    move-result-object p1

    .line 117
    invoke-direct {v5, v1, p2, v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositSection;-><init>(ZLcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x10

    const/4 v9, 0x0

    move-object v2, v10

    .line 114
    invoke-direct/range {v2 .. v9}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;-><init>(ZZLcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositSection;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v10
.end method

.method private final screenForFullInvoice()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;
    .locals 7

    .line 90
    new-instance v6, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;-><init>(ZZLcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositSection;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;Z)V

    return-object v6
.end method

.method private final toRowData(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;
    .locals 9

    .line 224
    new-instance v8, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;

    .line 226
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/invoices/PaymentRequestsKt;->getPaymentRequestType(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 227
    invoke-direct {p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->dueString(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;

    move-result-object v3

    .line 228
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->reminderText(Lcom/squareup/protos/client/invoice/PaymentRequest;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    move-object v1, p2

    .line 224
    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v8
.end method


# virtual methods
.method public final fromPaymentRequests(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Boolean;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;"
        }
    .end annotation

    move-object/from16 v0, p3

    const-string v1, "invoiceAmount"

    move-object v2, p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "invoiceFirstSentDate"

    move-object v3, p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "paymentRequests"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "toggleRequestDeposit"

    move-object/from16 v4, p4

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "toggleBalanceSplit"

    move-object/from16 v5, p5

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "textUpdated"

    move-object/from16 v6, p6

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "paymentRequestClicked"

    move-object/from16 v7, p7

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "amountTypeChanged"

    move-object/from16 v8, p8

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "addInstallment"

    move-object/from16 v9, p9

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "cancelClicked"

    move-object/from16 v10, p10

    invoke-static {v10, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "saveClicked"

    move-object/from16 v11, p11

    invoke-static {v11, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    sget-object v1, Lcom/squareup/invoices/PaymentRequestsConfig;->Companion:Lcom/squareup/invoices/PaymentRequestsConfig$Companion;

    invoke-virtual {v1, v0}, Lcom/squareup/invoices/PaymentRequestsConfig$Companion;->fromPaymentRequests(Ljava/util/List;)Lcom/squareup/invoices/PaymentRequestsConfig;

    move-result-object v1

    .line 61
    sget-object v12, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/invoices/PaymentRequestsConfig;->ordinal()I

    move-result v1

    aget v1, v12, v1

    const/4 v12, 0x1

    if-eq v1, v12, :cond_4

    const/4 v12, 0x2

    if-eq v1, v12, :cond_3

    const/4 v12, 0x3

    if-eq v1, v12, :cond_2

    const/4 v12, 0x4

    if-eq v1, v12, :cond_1

    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid payment request config: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast v0, Ljava/lang/Iterable;

    .line 335
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 336
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 337
    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 75
    iget-object v3, v3, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 338
    :cond_0
    check-cast v2, Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 74
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 72
    :cond_1
    invoke-direct/range {p0 .. p3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->screenForBalance(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;

    move-result-object v0

    goto :goto_1

    .line 69
    :cond_2
    invoke-direct/range {p0 .. p3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->screenForDepositInstallments(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;

    move-result-object v0

    goto :goto_1

    .line 66
    :cond_3
    invoke-direct/range {p0 .. p3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->screenForDepositRemainder(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;

    move-result-object v0

    goto :goto_1

    .line 63
    :cond_4
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->screenForFullInvoice()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;

    move-result-object v0

    :goto_1
    move-object v2, v0

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    .line 77
    invoke-virtual/range {v2 .. v10}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory$ScreenData;->toWorkflowScreen(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;

    move-result-object v0

    return-object v0
.end method
