.class final Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$takeSnapshot$1;
.super Lkotlin/jvm/internal/Lambda;
.source "InvoiceMessageState.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;->takeSnapshot$invoices_hairball_release()Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "sink",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$takeSnapshot$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "this::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 45
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    .line 46
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;

    if-eqz v1, :cond_0

    .line 47
    check-cast v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;->getUpdatedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 48
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;->isDefaultMessageEnabled()Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabledKt;->writeDefaultMessageEnabled(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)V

    goto :goto_0

    .line 50
    :cond_0
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;

    if-eqz v1, :cond_1

    .line 51
    check-cast v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 52
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;->isDefaultMessageEnabled()Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabledKt;->writeDefaultMessageEnabled(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)V

    goto :goto_0

    .line 54
    :cond_1
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;

    if-eqz v1, :cond_2

    .line 55
    check-cast v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;->getErrorTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 56
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;->getErrorBody()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 57
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;->getUpdatedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;->isDefaultMessageEnabled()Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabledKt;->writeDefaultMessageEnabled(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)V

    :cond_2
    :goto_0
    return-void
.end method
