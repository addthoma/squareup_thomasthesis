.class public final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;
.super Ljava/lang/Object;
.source "InvoiceFileAttachmentReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/rx2/Reactor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor<",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0082\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 T2\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0001TB_\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u00a2\u0006\u0002\u0010\u001bJ\u0006\u0010\u001c\u001a\u00020\u001dJ\u001c\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020!0\u001f2\u0006\u0010\"\u001a\u00020#H\u0002J$\u0010$\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020%0\u001f2\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)H\u0002J*\u0010*\u001a\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040+2\u0006\u0010,\u001a\u00020\u00022\u0006\u0010-\u001a\u00020.H\u0016J\u001c\u0010/\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u0002000\u001f2\u0006\u00101\u001a\u000202H\u0002J-\u00103\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u0002040\u001f2\u0012\u00105\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020)06\"\u00020)H\u0002\u00a2\u0006\u0002\u00107J:\u00108\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040:092\u0006\u0010;\u001a\u00020\u00022\u000c\u0010<\u001a\u0008\u0012\u0004\u0012\u00020\u00030=2\u0006\u0010-\u001a\u00020.H\u0016J4\u0010>\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020?0\u001f2\u0006\u0010&\u001a\u00020\'2\u0006\u0010@\u001a\u00020A2\u0006\u0010B\u001a\u00020C2\u0006\u0010D\u001a\u00020EH\u0002J\u000c\u0010(\u001a\u00020)*\u00020FH\u0002J\u000c\u0010G\u001a\u00020 *\u00020AH\u0002J \u0010H\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040:*\u00020%2\u0006\u0010;\u001a\u00020IH\u0002J\u0014\u0010J\u001a\u00020F*\u00020K2\u0006\u0010(\u001a\u00020)H\u0002J\u0018\u0010L\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020M0\u001f*\u00020NH\u0002J \u0010O\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020M0\u001f*\u00020P2\u0006\u0010Q\u001a\u00020)H\u0002J(\u0010O\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020M0\u001f*\u00020R2\u0006\u0010Q\u001a\u00020)2\u0006\u0010S\u001a\u00020)H\u0002R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006U"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
        "invoiceImageChooser",
        "Lcom/squareup/invoices/image/InvoiceImageChooser;",
        "cameraHelper",
        "Lcom/squareup/camerahelper/CameraHelper;",
        "imageCompressor",
        "Lcom/squareup/invoices/image/ImageCompressor;",
        "fileHelper",
        "Lcom/squareup/invoices/image/InvoiceFileHelper;",
        "imageUploader",
        "Lcom/squareup/invoices/image/ImageUploader;",
        "imageDownloader",
        "Lcom/squareup/invoices/image/ImageDownloader;",
        "filePicker",
        "Lcom/squareup/filepicker/FilePicker;",
        "fileViewer",
        "Lcom/squareup/invoices/image/FileViewer;",
        "toastFactory",
        "Lcom/squareup/util/ToastFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "attachmentFileViewer",
        "Lcom/squareup/invoices/image/AttachmentFileViewer;",
        "(Lcom/squareup/invoices/image/InvoiceImageChooser;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/invoices/image/ImageCompressor;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/invoices/image/ImageDownloader;Lcom/squareup/filepicker/FilePicker;Lcom/squareup/invoices/image/FileViewer;Lcom/squareup/util/ToastFactory;Lcom/squareup/util/Res;Lcom/squareup/invoices/image/AttachmentFileViewer;)V",
        "asWorkflow",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileAttachmentsWorkflow;",
        "compressImage",
        "Lcom/squareup/workflow/legacy/Worker;",
        "",
        "Lcom/squareup/invoices/image/CompressionResult;",
        "uri",
        "Landroid/net/Uri;",
        "downloadFile",
        "Lcom/squareup/invoices/image/FileDownloadResult;",
        "invoiceTokenType",
        "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
        "attachmentToken",
        "",
        "launch",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "initialState",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "launchCameraHelper",
        "Lcom/squareup/invoices/image/ImageChooserResult;",
        "photoSource",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/PhotoSource;",
        "launchFilePicker",
        "Lcom/squareup/filepicker/FilePickerResult;",
        "mimeTypes",
        "",
        "([Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;",
        "onReact",
        "Lio/reactivex/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "state",
        "events",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel;",
        "uploadFile",
        "Lcom/squareup/invoices/image/FileUploadResult;",
        "attachmentStatus",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;",
        "fileMetadata",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;",
        "uploadValidationInfo",
        "Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;",
        "attemptDeleteFile",
        "toReaction",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;",
        "toViewingImageType",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType;",
        "viewExternalFile",
        "",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;",
        "viewExternally",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;",
        "mimeType",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;",
        "extension",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$Companion;


# instance fields
.field private final attachmentFileViewer:Lcom/squareup/invoices/image/AttachmentFileViewer;

.field private final cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

.field private final fileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

.field private final filePicker:Lcom/squareup/filepicker/FilePicker;

.field private final fileViewer:Lcom/squareup/invoices/image/FileViewer;

.field private final imageCompressor:Lcom/squareup/invoices/image/ImageCompressor;

.field private final imageDownloader:Lcom/squareup/invoices/image/ImageDownloader;

.field private final imageUploader:Lcom/squareup/invoices/image/ImageUploader;

.field private final invoiceImageChooser:Lcom/squareup/invoices/image/InvoiceImageChooser;

.field private final res:Lcom/squareup/util/Res;

.field private final toastFactory:Lcom/squareup/util/ToastFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->Companion:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/image/InvoiceImageChooser;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/invoices/image/ImageCompressor;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/invoices/image/ImageDownloader;Lcom/squareup/filepicker/FilePicker;Lcom/squareup/invoices/image/FileViewer;Lcom/squareup/util/ToastFactory;Lcom/squareup/util/Res;Lcom/squareup/invoices/image/AttachmentFileViewer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "invoiceImageChooser"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cameraHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "imageCompressor"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileHelper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "imageUploader"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "imageDownloader"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "filePicker"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileViewer"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "toastFactory"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attachmentFileViewer"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->invoiceImageChooser:Lcom/squareup/invoices/image/InvoiceImageChooser;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->imageCompressor:Lcom/squareup/invoices/image/ImageCompressor;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->fileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->imageUploader:Lcom/squareup/invoices/image/ImageUploader;

    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->imageDownloader:Lcom/squareup/invoices/image/ImageDownloader;

    iput-object p7, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->filePicker:Lcom/squareup/filepicker/FilePicker;

    iput-object p8, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->fileViewer:Lcom/squareup/invoices/image/FileViewer;

    iput-object p9, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->toastFactory:Lcom/squareup/util/ToastFactory;

    iput-object p10, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->res:Lcom/squareup/util/Res;

    iput-object p11, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->attachmentFileViewer:Lcom/squareup/invoices/image/AttachmentFileViewer;

    return-void
.end method

.method public static final synthetic access$attemptDeleteFile(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;)V
    .locals 0

    .line 97
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->attemptDeleteFile(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;)V

    return-void
.end method

.method public static final synthetic access$compressImage(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Landroid/net/Uri;)Lcom/squareup/workflow/legacy/Worker;
    .locals 0

    .line 97
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->compressImage(Landroid/net/Uri;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$downloadFile(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;
    .locals 0

    .line 97
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->downloadFile(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCameraHelper$p(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;)Lcom/squareup/camerahelper/CameraHelper;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    return-object p0
.end method

.method public static final synthetic access$getFilePicker$p(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;)Lcom/squareup/filepicker/FilePicker;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->filePicker:Lcom/squareup/filepicker/FilePicker;

    return-object p0
.end method

.method public static final synthetic access$getInvoiceImageChooser$p(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;)Lcom/squareup/invoices/image/InvoiceImageChooser;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->invoiceImageChooser:Lcom/squareup/invoices/image/InvoiceImageChooser;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;)Lcom/squareup/util/Res;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getToastFactory$p(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;)Lcom/squareup/util/ToastFactory;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->toastFactory:Lcom/squareup/util/ToastFactory;

    return-object p0
.end method

.method public static final synthetic access$launchCameraHelper(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/PhotoSource;)Lcom/squareup/workflow/legacy/Worker;
    .locals 0

    .line 97
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->launchCameraHelper(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/PhotoSource;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p0

    return-object p0
.end method

.method public static final varargs synthetic access$launchFilePicker(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;[Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;
    .locals 0

    .line 97
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->launchFilePicker([Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toReaction(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/invoices/image/FileDownloadResult;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 0

    .line 97
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->toReaction(Lcom/squareup/invoices/image/FileDownloadResult;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toViewingImageType(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType;Ljava/lang/String;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;
    .locals 0

    .line 97
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->toViewingImageType(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType;Ljava/lang/String;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$uploadFile(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)Lcom/squareup/workflow/legacy/Worker;
    .locals 0

    .line 97
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->uploadFile(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$viewExternalFile(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;)Lcom/squareup/workflow/legacy/Worker;
    .locals 0

    .line 97
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->viewExternalFile(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p0

    return-object p0
.end method

.method private final attachmentToken(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;)Ljava/lang/String;
    .locals 1

    .line 548
    instance-of v0, p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Update;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Update;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Update;->getAttachmentToken()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 549
    :cond_0
    instance-of v0, p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$ReadOnly;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$ReadOnly;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$ReadOnly;->getAttachmentToken()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1

    .line 550
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "ViewingImageType must have attachment token."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final attemptDeleteFile(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;)V
    .locals 1

    .line 502
    instance-of v0, p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->fileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->getFile()Ljava/io/File;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/image/InvoiceFileHelper;->delete(Ljava/io/File;)V

    :cond_0
    return-void
.end method

.method private final compressImage(Landroid/net/Uri;)Lcom/squareup/workflow/legacy/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/squareup/workflow/legacy/Worker<",
            "Lkotlin/Unit;",
            "Lcom/squareup/invoices/image/CompressionResult;",
            ">;"
        }
    .end annotation

    .line 470
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->imageCompressor:Lcom/squareup/invoices/image/ImageCompressor;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/image/ImageCompressor;->compress(Landroid/net/Uri;)Lio/reactivex/Single;

    move-result-object p1

    .line 471
    sget-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$compressImage$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$compressImage$1;

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "imageCompressor.compress\u2026ession failed\")\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 475
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkersKt;->asWorker(Lio/reactivex/Single;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p1

    return-object p1
.end method

.method private final downloadFile(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/legacy/Worker<",
            "Lkotlin/Unit;",
            "Lcom/squareup/invoices/image/FileDownloadResult;",
            ">;"
        }
    .end annotation

    .line 439
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->imageDownloader:Lcom/squareup/invoices/image/ImageDownloader;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/image/ImageDownloader;->download(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 440
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkersKt;->asWorker(Lio/reactivex/Single;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p1

    return-object p1
.end method

.method private final launchCameraHelper(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/PhotoSource;)Lcom/squareup/workflow/legacy/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/PhotoSource;",
            ")",
            "Lcom/squareup/workflow/legacy/Worker<",
            "Lkotlin/Unit;",
            "Lcom/squareup/invoices/image/ImageChooserResult;",
            ">;"
        }
    .end annotation

    .line 479
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->invoiceImageChooser:Lcom/squareup/invoices/image/InvoiceImageChooser;

    invoke-virtual {v0}, Lcom/squareup/invoices/image/InvoiceImageChooser;->image()Lio/reactivex/Single;

    move-result-object v0

    .line 480
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchCameraHelper$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchCameraHelper$1;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/PhotoSource;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    .line 487
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchCameraHelper$2;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchCameraHelper$2;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;)V

    check-cast v0, Lio/reactivex/functions/Action;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnDispose(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "invoiceImageChooser.imag\u2026er(invoiceImageChooser) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 488
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkersKt;->asWorker(Lio/reactivex/Single;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p1

    return-object p1
.end method

.method private final varargs launchFilePicker([Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/legacy/Worker<",
            "Lkotlin/Unit;",
            "Lcom/squareup/filepicker/FilePickerResult;",
            ">;"
        }
    .end annotation

    .line 492
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->filePicker:Lcom/squareup/filepicker/FilePicker;

    invoke-interface {v0}, Lcom/squareup/filepicker/FilePicker;->results()Lio/reactivex/Observable;

    move-result-object v0

    .line 493
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 494
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchFilePicker$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchFilePicker$1;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;[Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "filePicker.results()\n   \u2026nch(*mimeTypes)\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 497
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkersKt;->asWorker(Lio/reactivex/Single;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p1

    return-object p1
.end method

.method private final toReaction(Lcom/squareup/invoices/image/FileDownloadResult;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/image/FileDownloadResult;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
            ">;"
        }
    .end annotation

    .line 510
    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;->getDownloadingImageType()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType;

    move-result-object v2

    .line 513
    instance-of v0, p1, Lcom/squareup/invoices/image/FileDownloadResult$Failed;

    if-eqz v0, :cond_0

    .line 514
    new-instance v8, Lcom/squareup/workflow/legacy/EnterState;

    .line 515
    new-instance v9, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorDownloading;

    .line 516
    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;->getAttachmentToken()Ljava/lang/String;

    move-result-object v1

    .line 518
    sget-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Error;->INSTANCE:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Error;

    move-object v3, v0

    check-cast v3, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    .line 519
    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object v4

    .line 520
    check-cast p1, Lcom/squareup/invoices/image/FileDownloadResult$Failed;

    invoke-virtual {p1}, Lcom/squareup/invoices/image/FileDownloadResult$Failed;->getErrorTitle()Ljava/lang/String;

    move-result-object v5

    .line 521
    invoke-virtual {p1}, Lcom/squareup/invoices/image/FileDownloadResult$Failed;->getErrorBody()Ljava/lang/String;

    move-result-object v6

    .line 522
    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object v7

    move-object v0, v9

    .line 515
    invoke-direct/range {v0 .. v7}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorDownloading;-><init>(Ljava/lang/String;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)V

    .line 514
    invoke-direct {v8, v9}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast v8, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 526
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/image/FileDownloadResult$Success;

    if-eqz v0, :cond_1

    .line 527
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 528
    new-instance v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;

    .line 529
    new-instance v3, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;

    check-cast p1, Lcom/squareup/invoices/image/FileDownloadResult$Success;

    invoke-virtual {p1}, Lcom/squareup/invoices/image/FileDownloadResult$Success;->getFile()Ljava/io/File;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;-><init>(Ljava/io/File;)V

    check-cast v3, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    .line 530
    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p1

    .line 531
    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;->getAttachmentToken()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v4}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->toViewingImageType(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType;Ljava/lang/String;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;

    move-result-object v2

    .line 532
    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object p2

    .line 528
    invoke-direct {v1, v3, p1, v2, p2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;-><init>(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)V

    .line 527
    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    move-object v8, v0

    check-cast v8, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object v8

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final toViewingImageType(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType;Ljava/lang/String;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;
    .locals 1

    .line 543
    sget-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType$Update;->INSTANCE:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType$Update;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Update;

    invoke-direct {p1, p2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Update;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;

    goto :goto_0

    .line 544
    :cond_0
    instance-of v0, p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType$ReadOnly;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$ReadOnly;

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType$ReadOnly;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType$ReadOnly;->getUploadedAt()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1, p2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$ReadOnly;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final uploadFile(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)Lcom/squareup/workflow/legacy/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;",
            "Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;",
            ")",
            "Lcom/squareup/workflow/legacy/Worker<",
            "Lkotlin/Unit;",
            "Lcom/squareup/invoices/image/FileUploadResult;",
            ">;"
        }
    .end annotation

    .line 450
    instance-of v0, p2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->imageUploader:Lcom/squareup/invoices/image/ImageUploader;

    .line 452
    check-cast p2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;

    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->getFile()Ljava/io/File;

    move-result-object p2

    invoke-virtual {p3}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getTitle()Ljava/lang/String;

    move-result-object p3

    .line 451
    invoke-virtual {v0, p2, p3, p1, p4}, Lcom/squareup/invoices/image/ImageUploader;->uploadFile(Ljava/io/File;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)Lio/reactivex/Single;

    move-result-object p1

    .line 455
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkersKt;->asWorker(Lio/reactivex/Single;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p1

    goto :goto_0

    .line 457
    :cond_0
    instance-of v0, p2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;

    if-eqz v0, :cond_1

    .line 458
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->imageUploader:Lcom/squareup/invoices/image/ImageUploader;

    .line 459
    check-cast p2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;

    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;->getUri()Landroid/net/Uri;

    move-result-object p2

    .line 458
    invoke-virtual {v0, p2, p3, p1, p4}, Lcom/squareup/invoices/image/ImageUploader;->uploadUri(Landroid/net/Uri;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)Lio/reactivex/Single;

    move-result-object p1

    .line 462
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkersKt;->asWorker(Lio/reactivex/Single;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p1

    :goto_0
    return-object p1

    .line 465
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "attachmentStatus must have a displayed image."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final viewExternalFile(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;)Lcom/squareup/workflow/legacy/Worker;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;",
            ")",
            "Lcom/squareup/workflow/legacy/Worker<",
            "Lkotlin/Unit;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 420
    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;->getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    move-result-object v0

    .line 421
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getMimeType()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->viewExternally(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p1

    goto :goto_0

    .line 422
    :cond_0
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;

    .line 423
    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getExtensionToPreserve()Ljava/lang/String;

    move-result-object p1

    .line 422
    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->viewExternally(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p1

    goto :goto_0

    .line 425
    :cond_1
    instance-of v0, v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Remote;

    if-eqz v0, :cond_2

    .line 426
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->attachmentFileViewer:Lcom/squareup/invoices/image/AttachmentFileViewer;

    .line 427
    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;->getViewingImageType()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->attachmentToken(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;->getMimeType()Ljava/lang/String;

    move-result-object p1

    .line 426
    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/invoices/image/AttachmentFileViewer;->viewAttachment(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 429
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkersKt;->asWorker(Lio/reactivex/Single;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p1

    goto :goto_0

    :cond_2
    const/4 p1, 0x1

    .line 431
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(true)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkersKt;->asWorker(Lio/reactivex/Single;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final viewExternally(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/legacy/Worker<",
            "Lkotlin/Unit;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 558
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->fileViewer:Lcom/squareup/invoices/image/FileViewer;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->getFile()Ljava/io/File;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/image/FileViewer;->viewFileExternally(Ljava/io/File;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 559
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkersKt;->asWorker(Lio/reactivex/Single;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p1

    return-object p1
.end method

.method private final viewExternally(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/legacy/Worker<",
            "Lkotlin/Unit;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 566
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->fileViewer:Lcom/squareup/invoices/image/FileViewer;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalUri;->getUri()Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/invoices/image/FileViewer;->viewUriExternally(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 567
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkersKt;->asWorker(Lio/reactivex/Single;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final asWorkflow()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileAttachmentsWorkflow;
    .locals 1

    .line 113
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$asWorkflow$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$asWorkflow$1;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;)V

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileAttachmentsWorkflow;

    return-object v0
.end method

.method public launch(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
            ">;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    .line 574
    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/legacy/rx2/ReactorKt;->doLaunch$default(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    sget-object p2, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launch$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launch$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, p2}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->switchMapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->launch(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public onReact(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    invoke-direct {v0, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;Lcom/squareup/workflow/legacy/WorkflowPool;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->onReact(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
