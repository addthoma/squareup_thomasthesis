.class final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "InvoiceFileAttachmentWorkflowRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter;->start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lcom/squareup/workflow/ScreenState<",
        "+",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;>;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceFileAttachmentWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceFileAttachmentWorkflowRunner.kt\ncom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1\n*L\n1#1,114:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a*\u0012&\u0012$\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u00050\u0002j\u0008\u0012\u0004\u0012\u00020\u0003`\u00060\u00012\u0006\u0010\u0007\u001a\u00020\u0008H\u008a@\u00a2\u0006\u0004\u0008\t\u0010\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "state",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.invoices.workflow.edit.fileattachments.InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1"
    f = "InvoiceFileAttachmentWorkflowRunner.kt"
    i = {}
    l = {}
    m = "invokeSuspend"
    n = {}
    s = {}
.end annotation


# instance fields
.field final synthetic $pool:Lcom/squareup/workflow/legacy/WorkflowPool;

.field final synthetic $workflow:Lcom/squareup/workflow/legacy/Workflow;

.field label:I

.field private p$0:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Workflow;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;->$workflow:Lcom/squareup/workflow/legacy/Workflow;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;->$pool:Lcom/squareup/workflow/legacy/WorkflowPool;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;->$workflow:Lcom/squareup/workflow/legacy/Workflow;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;->$pool:Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-direct {v0, v1, v2, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    iput-object p1, v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;->p$0:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    .line 99
    iget v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;->label:I

    if-nez v0, :cond_0

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;->p$0:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    .line 100
    sget-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentRenderer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentRenderer;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;->$workflow:Lcom/squareup/workflow/legacy/Workflow;

    check-cast v1, Lcom/squareup/workflow/legacy/WorkflowInput;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$InvoiceFileAttachmentWorkflowStarter$start$1;->$pool:Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentRenderer;->render(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object v0

    .line 101
    new-instance v1, Lcom/squareup/workflow/ScreenState;

    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/ScreenState;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V

    return-object v1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
