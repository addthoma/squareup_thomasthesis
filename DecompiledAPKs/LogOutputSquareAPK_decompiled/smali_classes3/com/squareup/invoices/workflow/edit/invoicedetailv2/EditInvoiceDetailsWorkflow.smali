.class public final Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "EditInvoiceDetailsWorkflow.kt"

# interfaces
.implements Lcom/squareup/features/invoices/shared/edit/workflow/details/EditDetailsWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/details/EditDetailsWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceDetailsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceDetailsWorkflow.kt\ncom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,158:1\n149#2,5:159\n149#2,5:167\n149#2,5:172\n149#2,5:177\n85#3:164\n240#4:165\n276#5:166\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoiceDetailsWorkflow.kt\ncom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow\n*L\n82#1,5:159\n107#1,5:167\n118#1,5:172\n130#1,5:177\n87#1:164\n87#1:165\n87#1:166\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u001d2\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0001\u001dB\u0019\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0008\u0001\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJt\u0010\u0010\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0003\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u00060\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00050\u0011j6\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n`\u0013J\u001a\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u00032\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016JN\u0010\u0018\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u00042\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u0004H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/details/EditDetailsWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "defaultMessageUpdater",
        "Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "(Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;Lkotlinx/coroutines/CoroutineDispatcher;)V",
        "asLegacyLauncher",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/workflow/legacyintegration/LegacyLauncher;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$Companion;


# instance fields
.field private final defaultMessageUpdater:Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;

.field private final mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;->Companion:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;Lkotlinx/coroutines/CoroutineDispatcher;)V
    .locals 1
    .param p2    # Lkotlinx/coroutines/CoroutineDispatcher;
        .annotation runtime Lcom/squareup/thread/Main$Immediate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "defaultMessageUpdater"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;->defaultMessageUpdater:Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    return-void
.end method


# virtual methods
.method public final asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;",
            ">;"
        }
    .end annotation

    .line 143
    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacyintegration/LegacyLauncherKt;->createLegacyLauncher(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object v0

    return-object v0
.end method

.method public initialState(Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;
    .locals 1

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 54
    sget-object v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;->Companion:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$Companion;

    invoke-virtual {v0, p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$Companion;->restoreSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;

    move-result-object p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ShowDetails;

    .line 55
    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->getInvoiceDetailsInfo()Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->getDisableId()Z

    move-result p1

    .line 54
    invoke-direct {p2, v0, p1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ShowDetails;-><init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;Z)V

    check-cast p2, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;

    :goto_0
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;->initialState(Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;

    check-cast p2, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;->render(Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    const-string v2, "props"

    move-object/from16 v7, p1

    invoke-static {v7, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "state"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    instance-of v2, v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ShowDetails;

    const-string v8, ""

    if-eqz v2, :cond_0

    sget-object v9, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->Companion:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;

    .line 66
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;->getInvoiceDetailsInfo()Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    move-result-object v10

    const/4 v11, 0x0

    .line 67
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;->getDisableId()Z

    move-result v12

    .line 68
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->getForEstimate()Z

    move-result v13

    .line 69
    new-instance v2, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$render$1;

    invoke-direct {v2, v0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$render$1;-><init>(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {v1, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v14

    const/4 v15, 0x2

    const/16 v16, 0x0

    .line 65
    invoke-static/range {v9 .. v16}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;->fromDetailsInfo$default(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;ZZZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 160
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 161
    const-class v2, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 162
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 160
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 83
    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v0

    move-object/from16 v9, p0

    goto/16 :goto_0

    .line 85
    :cond_0
    instance-of v2, v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$SavingMessage;

    if-eqz v2, :cond_1

    move-object/from16 v9, p0

    .line 87
    iget-object v2, v9, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;->defaultMessageUpdater:Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;->getInvoiceDetailsInfo()Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;->update(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v2

    .line 164
    sget-object v3, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v3, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$render$$inlined$asWorker$1;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 165
    invoke-static {v3}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v2

    .line 166
    const-class v3, Lcom/squareup/invoices/DefaultMessageUpdatedResult;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v3, v2}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v2, v4

    check-cast v2, Lcom/squareup/workflow/Worker;

    const/4 v3, 0x0

    .line 88
    new-instance v4, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$render$3;

    invoke-direct {v4, v0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$render$3;-><init>(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object/from16 v1, p3

    .line 86
    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 101
    sget-object v10, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->Companion:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;

    .line 102
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;->getInvoiceDetailsInfo()Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    move-result-object v11

    .line 103
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;->getDisableId()Z

    move-result v13

    .line 104
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->getForEstimate()Z

    move-result v14

    const/4 v12, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x10

    const/16 v17, 0x0

    .line 101
    invoke-static/range {v10 .. v17}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;->fromDetailsInfo$default(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;ZZZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 168
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 169
    const-class v2, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 170
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 168
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 108
    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object/from16 v9, p0

    .line 111
    instance-of v2, v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ErrorSavingMessage;

    if-eqz v2, :cond_2

    .line 112
    sget-object v2, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 113
    sget-object v10, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->Companion:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;

    .line 114
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;->getInvoiceDetailsInfo()Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    move-result-object v11

    const/4 v12, 0x0

    .line 115
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;->getDisableId()Z

    move-result v13

    .line 116
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;->getForEstimate()Z

    move-result v14

    const/4 v15, 0x0

    const/16 v16, 0x12

    const/16 v17, 0x0

    .line 113
    invoke-static/range {v10 .. v17}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;->fromDetailsInfo$default(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;ZZZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;

    move-result-object v3

    check-cast v3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 173
    new-instance v4, Lcom/squareup/workflow/legacy/Screen;

    .line 174
    const-class v5, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    invoke-static {v5, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 175
    sget-object v6, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v6}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v6

    .line 173
    invoke-direct {v4, v5, v3, v6}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 120
    new-instance v3, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/ErrorSavingMessageDialogScreen;

    .line 121
    move-object v5, v0

    check-cast v5, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ErrorSavingMessage;

    invoke-virtual {v5}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ErrorSavingMessage;->getErrorTitle()Ljava/lang/String;

    move-result-object v6

    .line 122
    invoke-virtual {v5}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ErrorSavingMessage;->getErrorBody()Ljava/lang/String;

    move-result-object v5

    .line 123
    new-instance v7, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$render$5;

    invoke-direct {v7, v0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$render$5;-><init>(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    invoke-interface {v1, v7}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v0

    .line 120
    invoke-direct {v3, v6, v5, v0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/ErrorSavingMessageDialogScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 178
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    .line 179
    const-class v1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/ErrorSavingMessageDialogScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 180
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 178
    invoke-direct {v0, v1, v3, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 112
    invoke-virtual {v2, v4, v0}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v0

    .line 133
    :goto_0
    sget-object v1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    sget-object v2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/LayeredScreensKt;->toPosScreen(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    return-object v0

    .line 112
    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public snapshotState(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;->takeSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;->snapshotState(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
