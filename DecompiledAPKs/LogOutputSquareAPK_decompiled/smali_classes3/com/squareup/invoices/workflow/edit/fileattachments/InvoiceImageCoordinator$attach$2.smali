.class final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$attach$2;
.super Ljava/lang/Object;
.source "InvoiceImageCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        ">;+",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012V\u0010\u0002\u001aR\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007\u0012\u000c\u0012\n \t*\u0004\u0018\u00010\u00080\u0008 \t*(\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007\u0012\u000c\u0012\n \t*\u0004\u0018\u00010\u00080\u0008\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreen;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$attach$2;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 48
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$attach$2;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;+",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;

    .line 85
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$attach$2;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;

    .line 86
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$attach$2;->$view:Landroid/view/View;

    const-string v3, "photoState"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v3, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;

    iget-object v0, v0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 85
    invoke-static {v1, v2, p1, v3, v0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->access$update(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;Landroid/view/View;Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method
