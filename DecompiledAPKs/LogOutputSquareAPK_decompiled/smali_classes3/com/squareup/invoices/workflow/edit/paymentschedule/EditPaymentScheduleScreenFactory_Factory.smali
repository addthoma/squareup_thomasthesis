.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;
.super Ljava/lang/Object;
.source "EditPaymentScheduleScreenFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final balanceSplitHelperTextFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final depositHelperTextFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final numberAmountFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;->numberAmountFactoryProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;->balanceSplitHelperTextFactoryProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;->depositHelperTextFactoryProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/settings/server/Features;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;
    .locals 8

    .line 64
    new-instance v7, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/settings/server/Features;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;->numberAmountFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;->balanceSplitHelperTextFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;->depositHelperTextFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v6}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;->newInstance(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Factory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData$Factory;Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/settings/server/Features;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory_Factory;->get()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;

    move-result-object v0

    return-object v0
.end method
