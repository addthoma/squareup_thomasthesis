.class public final Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "AutomaticRemindersWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;",
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAutomaticRemindersWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AutomaticRemindersWorkflow.kt\ncom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,573:1\n32#2,12:574\n149#3,5:586\n149#3,5:591\n149#3,5:596\n149#3,5:601\n149#3,5:606\n149#3,5:611\n*E\n*S KotlinDebug\n*F\n+ 1 AutomaticRemindersWorkflow.kt\ncom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow\n*L\n318#1,12:574\n334#1,5:586\n334#1,5:591\n334#1,5:596\n334#1,5:601\n334#1,5:606\n334#1,5:611\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00c2\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u0000 F2<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0002EFB)\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012Jt\u0010\u0018\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0002\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u00050\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0019j6\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t`\u001bJ\u001a\u0010\u001c\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u00022\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016JN\u0010 \u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u001d\u001a\u00020\u00022\u0006\u0010!\u001a\u00020\u00032\u0012\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040#H\u0016J\u0010\u0010$\u001a\u00020\u001f2\u0006\u0010!\u001a\u00020\u0003H\u0016J\u0016\u0010%\u001a\u0004\u0018\u00010&*\u00020\u00032\u0006\u0010\'\u001a\u00020(H\u0002J\u001c\u0010)\u001a\u00020**\u00020+2\u0006\u0010,\u001a\u00020-2\u0006\u0010\'\u001a\u00020(H\u0002J\u001c\u0010)\u001a\u00020**\u00020.2\u0006\u0010,\u001a\u00020-2\u0006\u0010\'\u001a\u00020(H\u0002J\u001a\u0010/\u001a\u000200*\u00020+2\u000c\u00101\u001a\u0008\u0012\u0004\u0012\u00020302H\u0002J(\u0010/\u001a\u000200*\u00020.2\u000c\u00101\u001a\u0008\u0012\u0004\u0012\u000203022\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020302H\u0002Jr\u00105\u001a\u000206*\u0002072\u000c\u00108\u001a\u0008\u0012\u0004\u0012\u000203022\u000c\u00109\u001a\u0008\u0012\u0004\u0012\u000203022\u000c\u0010:\u001a\u0008\u0012\u0004\u0012\u000203022\u0012\u0010;\u001a\u000e\u0012\u0004\u0012\u00020=\u0012\u0004\u0012\u0002030<2\u0012\u0010>\u001a\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u0002030<2\u0012\u0010?\u001a\u000e\u0012\u0004\u0012\u00020&\u0012\u0004\u0012\u0002030<H\u0002J`\u00105\u001a\u00020**\u00020@2\u0006\u0010,\u001a\u00020-2\u0006\u0010\'\u001a\u00020(2\u000c\u0010A\u001a\u0008\u0012\u0004\u0012\u000203022\u0012\u0010B\u001a\u000e\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u0002030<2\u0012\u0010C\u001a\u000e\u0012\u0004\u0012\u00020=\u0012\u0004\u0012\u0002030<2\u000c\u0010D\u001a\u0008\u0012\u0004\u0012\u00020302H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0013\u001a\u00020\u0014*\u00020\u00158BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006G"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;",
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "reminderRowFactory",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/analytics/Analytics;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;Lcom/squareup/util/Res;)V",
        "offsetType",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceReminder;",
        "getOffsetType",
        "(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;)Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;",
        "asLegacyLauncher",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/workflow/legacyintegration/LegacyLauncher;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "helperText",
        "",
        "isPaymentRequestInPaymentSchedule",
        "",
        "toBodyScreen",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;",
        "info",
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;",
        "toDialogScreen",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;",
        "positiveClick",
        "Lkotlin/Function0;",
        "",
        "negativeClick",
        "toScreen",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;",
        "onCancel",
        "savePressed",
        "removeReminder",
        "updateReminderOffset",
        "Lkotlin/Function1;",
        "",
        "updateReminderOffsetType",
        "messageChanged",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;",
        "backPressed",
        "remindersToggled",
        "reminderPressed",
        "addReminder",
        "Action",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Companion;

.field private static final EMPTY_INPUT:Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

.field private final reminderRowFactory:Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->Companion:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Companion;

    .line 556
    new-instance v0, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    .line 557
    new-instance v1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;-><init>(Ljava/util/List;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    .line 558
    new-instance v1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;-><init>(Ljava/util/List;)V

    move-object v4, v1

    check-cast v4, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    .line 559
    new-instance v1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v5

    const-string v1, "InvoiceAutomaticReminderSettings.Builder().build()"

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v2, v0

    .line 556
    invoke-direct/range {v2 .. v8}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->EMPTY_INPUT:Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;Lcom/squareup/util/Res;)V
    .locals 1
    .param p2    # Lkotlinx/coroutines/CoroutineDispatcher;
        .annotation runtime Lcom/squareup/thread/Main$Immediate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reminderRowFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->reminderRowFactory:Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getEMPTY_INPUT$cp()Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;
    .locals 1

    .line 69
    sget-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->EMPTY_INPUT:Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    return-object v0
.end method

.method private final getOffsetType(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;)Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;
    .locals 1

    .line 548
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getRelativeDays()I

    move-result v0

    if-gez v0, :cond_0

    sget-object p1, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->BEFORE:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    goto :goto_0

    .line 549
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getRelativeDays()I

    move-result p1

    if-lez p1, :cond_1

    sget-object p1, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->AFTER:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    goto :goto_0

    .line 550
    :cond_1
    sget-object p1, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->ON:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    :goto_0
    return-object p1
.end method

.method private final helperText(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;Z)Ljava/lang/String;
    .locals 0

    if-eqz p2, :cond_1

    .line 536
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;->getRemindersEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->res:Lcom/squareup/util/Res;

    .line 537
    sget p2, Lcom/squareup/features/invoices/R$string;->invoice_automatic_reminders_wont_be_sent_after_paid:I

    .line 536
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 539
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/features/invoices/R$string;->invoice_automatic_reminders_explanation:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final toBodyScreen(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Z)Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;
    .locals 10

    .line 470
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->reminderRowFactory:Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;

    invoke-virtual {v0, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->fromInfo(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;)Ljava/util/List;

    move-result-object v3

    .line 471
    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    invoke-direct {p0, v0, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->helperText(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;Z)Ljava/lang/String;

    move-result-object v5

    .line 472
    new-instance p3, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;

    .line 473
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;->getRemindersEnabled()Z

    move-result v2

    .line 475
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->getReminders()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;->automatic_reminder_count_limit:Ljava/lang/Integer;

    const-string v1, "reminderSettings.automatic_reminder_count_limit"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->compare(II)I

    move-result p2

    if-gez p2, :cond_0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;->getRemindersEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v4, 0x0

    .line 477
    :goto_0
    sget-object p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$1;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function0;

    sget-object p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$2;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$2;

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$3;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$3;

    move-object v8, p1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$4;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$4;

    move-object v9, p1

    check-cast v9, Lkotlin/jvm/functions/Function0;

    move-object v1, p3

    .line 472
    invoke-direct/range {v1 .. v9}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;-><init>(ZLjava/util/List;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    return-object p3
.end method

.method private final toBodyScreen(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Z)Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;
    .locals 10

    .line 500
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->reminderRowFactory:Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;

    invoke-virtual {v0, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->fromInfo(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;)Ljava/util/List;

    move-result-object v3

    .line 501
    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    invoke-direct {p0, v0, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->helperText(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;Z)Ljava/lang/String;

    move-result-object v5

    .line 502
    new-instance p3, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;

    .line 503
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;->getRemindersEnabled()Z

    move-result v2

    .line 505
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->getReminders()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;->automatic_reminder_count_limit:Ljava/lang/Integer;

    const-string v1, "reminderSettings.automatic_reminder_count_limit"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->compare(II)I

    move-result p2

    if-gez p2, :cond_0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;->getRemindersEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v4, 0x0

    .line 507
    :goto_0
    sget-object p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$5;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$5;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function0;

    sget-object p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$6;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$6;

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$7;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$7;

    move-object v8, p1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$8;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$toBodyScreen$8;

    move-object v9, p1

    check-cast v9, Lkotlin/jvm/functions/Function0;

    move-object v1, p3

    .line 502
    invoke-direct/range {v1 .. v9}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;-><init>(ZLjava/util/List;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    return-object p3
.end method

.method private final toDialogScreen(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;Lkotlin/jvm/functions/Function0;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;"
        }
    .end annotation

    .line 484
    new-instance v3, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    .line 485
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/strings/R$string;->ok:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 484
    invoke-direct {v3, p1, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 489
    new-instance p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;

    .line 490
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->reminder_save_error_duplicate_days_title:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 491
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->reminder_save_error_duplicate_days_body:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p1

    .line 489
    invoke-direct/range {v0 .. v6}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method private final toDialogScreen(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;"
        }
    .end annotation

    .line 515
    new-instance p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    .line 516
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->ok:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 515
    invoke-direct {p1, v0, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 520
    new-instance p2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    .line 521
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 520
    invoke-direct {p2, v0, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 525
    new-instance p3, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;

    .line 526
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->turn_off_reminders_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 527
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->turn_off_reminders_body:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 525
    invoke-direct {p3, v0, v1, p1, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;)V

    return-object p3
.end method

.method private final toScreen(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;"
        }
    .end annotation

    .line 430
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getTempReminderToEdit()Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getRelativeDays()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 431
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getTempReminderToEdit()Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    move-result-object v0

    move-object v13, p0

    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->getOffsetType(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;)Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    move-result-object v3

    .line 432
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getTempReminderToEdit()Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getCustomMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    move-object v4, v0

    .line 433
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;->custom_message_char_limit:Ljava/lang/Integer;

    const-string v1, "reminderSettings.custom_message_char_limit"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 434
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getEditingIndex()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    const/4 v6, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 429
    :goto_1
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;

    move-object v1, v0

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    invoke-direct/range {v1 .. v12}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;-><init>(ILcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;Ljava/lang/String;IZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method private final toScreen(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
            "Z",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;"
        }
    .end annotation

    move-object v0, p0

    .line 452
    iget-object v1, v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->reminderRowFactory:Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;

    move-object v2, p2

    invoke-virtual {v1, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->fromInfo(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;)Ljava/util/List;

    move-result-object v4

    .line 453
    move-object v1, p1

    check-cast v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    move v3, p3

    invoke-direct {p0, v1, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->helperText(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;Z)Ljava/lang/String;

    move-result-object v6

    .line 454
    new-instance v1, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;

    .line 455
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersEnabled()Z

    move-result v3

    .line 457
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->getReminders()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;->automatic_reminder_count_limit:Ljava/lang/Integer;

    const-string v7, "reminderSettings.automatic_reminder_count_limit"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->compare(II)I

    move-result v2

    if-gez v2, :cond_0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    const/4 v5, 0x0

    :goto_0
    move-object v2, v1

    move-object v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    .line 454
    invoke-direct/range {v2 .. v10}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;-><init>(ZLjava/util/List;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    return-object v1
.end method


# virtual methods
.method public final asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
            ">;"
        }
    .end annotation

    .line 418
    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacyintegration/LegacyLauncherKt;->createLegacyLauncher(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object v0

    return-object v0
.end method

.method public initialState(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;
    .locals 4

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    if-eqz p2, :cond_4

    .line 574
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v1

    const/4 v2, 0x0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x0

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v3

    :goto_1
    if-eqz p2, :cond_3

    .line 579
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const-string v3, "Parcel.obtain()"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 580
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 581
    array-length v3, p2

    invoke-virtual {v1, p2, v2, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 582
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 583
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    if-nez v3, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 584
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 585
    :cond_3
    check-cast v3, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    if-eqz v3, :cond_4

    goto :goto_2

    .line 320
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->getReminders()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/util/Collection;

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    xor-int/2addr p2, v0

    .line 321
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    .line 322
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->getDefaultList()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v1

    .line 323
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object p1

    .line 319
    new-instance v2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    invoke-direct {v2, p2, v0, v1, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;-><init>(ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V

    .line 318
    move-object v3, v2

    check-cast v3, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    :goto_2
    return-object v3
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 69
    check-cast p1, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->initialState(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 69
    check-cast p1, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    check-cast p2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->render(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 332
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p3

    .line 336
    instance-of v0, p2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    const-string v1, ""

    if-eqz v0, :cond_0

    move-object v3, p2

    check-cast v3, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    .line 337
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v4

    .line 338
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->isPaymentRequestInSchedule()Z

    move-result v5

    .line 339
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$1;

    invoke-direct {v0, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$1;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 340
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$2;

    invoke-direct {v0, p2, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$2;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 343
    new-instance p2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$3;

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$3;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V

    move-object v8, p2

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 344
    new-instance p2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$4;

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$4;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V

    move-object v9, p2

    check-cast v9, Lkotlin/jvm/functions/Function0;

    move-object v2, p0

    .line 336
    invoke-direct/range {v2 .. v9}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->toScreen(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 587
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 588
    const-class p3, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 589
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 587
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 347
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 349
    :cond_0
    instance-of v0, p2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;

    if-eqz v0, :cond_1

    .line 350
    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 351
    move-object v2, p2

    check-cast v2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;

    .line 352
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->isPaymentRequestInSchedule()Z

    move-result v4

    .line 351
    invoke-direct {p0, v2, v3, v4}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->toBodyScreen(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Z)Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;

    move-result-object v3

    check-cast v3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 592
    new-instance v4, Lcom/squareup/workflow/legacy/Screen;

    .line 593
    const-class v5, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 594
    sget-object v6, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v6}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v6

    .line 592
    invoke-direct {v4, v5, v3, v6}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 355
    new-instance v3, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$5;

    invoke-direct {v3, p2, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$5;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 354
    invoke-direct {p0, v2, v3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->toDialogScreen(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;Lkotlin/jvm/functions/Function0;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 597
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 598
    const-class p3, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 599
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 597
    invoke-direct {p2, p3, p1, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 350
    invoke-virtual {v0, v4, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    .line 366
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    sget-object p3, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2, p3}, Lcom/squareup/container/LayeredScreensKt;->toPosScreen(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 369
    :cond_1
    instance-of v0, p2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;

    if-eqz v0, :cond_2

    .line 370
    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 371
    move-object v2, p2

    check-cast v2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;

    .line 372
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;->isPaymentRequestInSchedule()Z

    move-result v4

    .line 371
    invoke-direct {p0, v2, v3, v4}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->toBodyScreen(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Z)Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;

    move-result-object v3

    check-cast v3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 602
    new-instance v4, Lcom/squareup/workflow/legacy/Screen;

    .line 603
    const-class v5, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 604
    sget-object v6, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v6}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v6

    .line 602
    invoke-direct {v4, v5, v3, v6}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 375
    new-instance v3, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$6;

    invoke-direct {v3, p2, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$6;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 385
    new-instance v5, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$7;

    invoke-direct {v5, p2, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$7;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 374
    invoke-direct {p0, v2, v3, v5}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->toDialogScreen(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 607
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 608
    const-class p3, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 609
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 607
    invoke-direct {p2, p3, p1, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 370
    invoke-virtual {v0, v4, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    .line 396
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    sget-object p3, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2, p3}, Lcom/squareup/container/LayeredScreensKt;->toPosScreen(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 399
    :cond_2
    instance-of v0, p2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    if-eqz v0, :cond_3

    move-object v3, p2

    check-cast v3, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    .line 400
    new-instance p2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$8;

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$8;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V

    move-object v4, p2

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 401
    new-instance p2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$9;

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$9;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V

    move-object v5, p2

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 402
    new-instance p2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$10;

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$10;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V

    move-object v6, p2

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 403
    new-instance p2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$11;

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$11;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V

    move-object v7, p2

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 404
    new-instance p2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$12;

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$12;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V

    move-object v8, p2

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 405
    new-instance p2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$13;

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$13;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V

    move-object v9, p2

    check-cast v9, Lkotlin/jvm/functions/Function1;

    move-object v2, p0

    .line 399
    invoke-direct/range {v2 .. v9}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->toScreen(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 612
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 613
    const-class p3, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 614
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 612
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 408
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 413
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 69
    check-cast p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->snapshotState(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
