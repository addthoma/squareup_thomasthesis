.class public final Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;
.super Ljava/lang/Object;
.source "ChooseDateRenderer.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/Renderer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/Renderer<",
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\tJH\u0010\n\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\u000b\u001a\u00020\u00022\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00030\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;",
        "Lcom/squareup/workflow/legacy/Renderer;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "()V",
        "render",
        "state",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;->render(Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    instance-of p3, p1, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;

    if-eqz p3, :cond_0

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;->getData()Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateScreenKt;->ChooseDateScreen(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 23
    :cond_0
    instance-of p3, p1, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingCustomDate;

    if-eqz p3, :cond_1

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingCustomDate;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingCustomDate;->getData()Lcom/squareup/invoices/workflow/edit/CustomDateInfo;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateScreenKt;->CustomDateScreen(Lcom/squareup/invoices/workflow/edit/CustomDateInfo;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 25
    :goto_0
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 23
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
