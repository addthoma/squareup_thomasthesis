.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;
.super Ljava/lang/Object;
.source "EditPaymentScheduleScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BalanceSplitHelperTextData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0016\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001\u001eB5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\tH\u00c6\u0003J\t\u0010\u0018\u001a\u00020\tH\u00c6\u0003JE\u0010\u0019\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u001a\u001a\u00020\t2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\rR\u0011\u0010\n\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0010R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;",
        "",
        "number",
        "",
        "balanceSplitAmount",
        "",
        "balanceAmount",
        "balanceSplitString",
        "isValid",
        "",
        "isPercentage",
        "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V",
        "getBalanceAmount",
        "()Ljava/lang/String;",
        "getBalanceSplitAmount",
        "getBalanceSplitString",
        "()Z",
        "getNumber",
        "()I",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final balanceAmount:Ljava/lang/String;

.field private final balanceSplitAmount:Ljava/lang/String;

.field private final balanceSplitString:Ljava/lang/String;

.field private final isPercentage:Z

.field private final isValid:Z

.field private final number:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1

    const-string v0, "balanceSplitAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceSplitString"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->number:I

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitAmount:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceAmount:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitString:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isValid:Z

    iput-boolean p6, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isPercentage:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->number:I

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitAmount:Ljava/lang/String;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceAmount:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitString:Ljava/lang/String;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isValid:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isPercentage:Z

    :cond_5
    move v3, p6

    move-object p2, p0

    move p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->copy(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->number:I

    return v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitAmount:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceAmount:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitString:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isValid:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isPercentage:Z

    return v0
.end method

.method public final copy(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;
    .locals 8

    const-string v0, "balanceSplitAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceSplitString"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;

    move-object v1, v0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->number:I

    iget v1, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->number:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitAmount:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitAmount:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceAmount:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceAmount:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitString:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitString:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isValid:Z

    iget-boolean v1, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isValid:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isPercentage:Z

    iget-boolean p1, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isPercentage:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBalanceAmount()Ljava/lang/String;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceAmount:Ljava/lang/String;

    return-object v0
.end method

.method public final getBalanceSplitAmount()Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitAmount:Ljava/lang/String;

    return-object v0
.end method

.method public final getBalanceSplitString()Ljava/lang/String;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitString:Ljava/lang/String;

    return-object v0
.end method

.method public final getNumber()I
    .locals 1

    .line 66
    iget v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->number:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->number:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitAmount:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceAmount:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitString:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isValid:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isPercentage:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public final isPercentage()Z
    .locals 1

    .line 71
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isPercentage:Z

    return v0
.end method

.method public final isValid()Z
    .locals 1

    .line 70
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isValid:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BalanceSplitHelperTextData(number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->number:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", balanceSplitAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitAmount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", balanceAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceAmount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", balanceSplitString="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->balanceSplitString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", isValid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isValid:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isPercentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isPercentage:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
