.class final Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogFactory$create$2;
.super Ljava/lang/Object;
.source "ConfirmRemovePaymentRequestDialog.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "screen",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogFactory$create$2;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;)Landroid/app/AlertDialog;
    .locals 3

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogFactory$create$2;->$context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 40
    sget v1, Lcom/squareup/features/invoices/R$string;->remove_deposit_request_confirm_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 41
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;->getData()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 43
    sget v1, Lcom/squareup/common/strings/R$string;->confirm:I

    .line 44
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogFactory$create$2$1;

    invoke-direct {v2, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogFactory$create$2$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    .line 42
    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 45
    sget v1, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogFactory$create$2$2;

    invoke-direct {v2, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogFactory$create$2$2;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 48
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogFactory$create$2$3;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogFactory$create$2$3;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;)V

    check-cast v1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogFactory$create$2;->apply(Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
