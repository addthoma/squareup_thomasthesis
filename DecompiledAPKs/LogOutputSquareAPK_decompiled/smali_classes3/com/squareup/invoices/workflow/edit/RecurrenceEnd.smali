.class public abstract Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;
.super Ljava/lang/Object;
.source "RecurrenceRule.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;,
        Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;,
        Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\t\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008\u0082\u0001\u0003\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;",
        "",
        "()V",
        "prettyString",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "AfterCount",
        "AtDate",
        "Never",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 214
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;-><init>()V

    return-void
.end method


# virtual methods
.method public final prettyString(Lcom/squareup/util/Res;Ljava/text/DateFormat;)Ljava/lang/String;
    .locals 2

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 224
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;

    if-eqz v0, :cond_0

    sget p2, Lcom/squareup/invoicesappletapi/R$string;->never:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 225
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    if-eqz v0, :cond_1

    sget v0, Lcom/squareup/invoicesappletapi/R$string;->on_date:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 226
    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;->getDate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string v0, "date"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 227
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 228
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 229
    :cond_1
    instance-of p2, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    if-eqz p2, :cond_3

    .line 230
    move-object p2, p0

    check-cast p2, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 231
    sget p2, Lcom/squareup/invoicesappletapi/R$string;->after_one_invoice:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 233
    :cond_2
    sget v0, Lcom/squareup/invoicesappletapi/R$string;->after_plural_invoices:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 234
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;->getCount()I

    move-result p2

    const-string v0, "count"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 235
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 236
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1

    .line 230
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
