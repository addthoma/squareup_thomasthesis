.class public final Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Companion;
.super Ljava/lang/Object;
.source "AutomaticRemindersWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAutomaticRemindersWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AutomaticRemindersWorkflow.kt\ncom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Companion\n+ 2 LegacyState.kt\ncom/squareup/workflow/legacyintegration/LegacyStateKt\n+ 3 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPool$Companion\n+ 4 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPoolKt\n*L\n1#1,573:1\n74#2,6:574\n74#2,6:582\n335#3:580\n335#3:588\n412#4:581\n412#4:589\n*E\n*S KotlinDebug\n*F\n+ 1 AutomaticRemindersWorkflow.kt\ncom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Companion\n*L\n565#1,6:574\n570#1,6:582\n565#1:580\n570#1:588\n565#1:581\n570#1:589\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J|\u0010\u0008\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0004\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u000c\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\rj\u0002`\u000e0\u000b0\n\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000f0\tj6\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000f\u0012&\u0012$\u0012\u0004\u0012\u00020\u000c\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\rj\u0002`\u000e0\u000bj\u0008\u0012\u0004\u0012\u00020\u000c`\u0011`\u00102\u0006\u0010\u0012\u001a\u00020\u0004J|\u0010\u0013\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0004\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u000c\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\rj\u0002`\u000e0\u000b0\n\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000f0\tj6\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000f\u0012&\u0012$\u0012\u0004\u0012\u00020\u000c\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\rj\u0002`\u000e0\u000bj\u0008\u0012\u0004\u0012\u00020\u000c`\u0011`\u00102\u0006\u0010\u0014\u001a\u00020\u0015R\u001c\u0010\u0003\u001a\u00020\u00048\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0005\u0010\u0002\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Companion;",
        "",
        "()V",
        "EMPTY_INPUT",
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
        "EMPTY_INPUT$annotations",
        "getEMPTY_INPUT$invoices_hairball_release",
        "()Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
        "legacyHandleFromInput",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
        "Lcom/squareup/workflow/legacyintegration/LegacyHandle;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "input",
        "legacyHandleFromSnapshot",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 554
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Companion;-><init>()V

    return-void
.end method

.method public static synthetic EMPTY_INPUT$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final getEMPTY_INPUT$invoices_hairball_release()Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;
    .locals 1

    .line 555
    invoke-static {}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->access$getEMPTY_INPUT$cp()Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    move-result-object v0

    return-object v0
.end method

.method public final legacyHandleFromInput(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 576
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowPool;->Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    .line 577
    new-instance v0, Lcom/squareup/workflow/legacyintegration/FakeKClass;

    invoke-direct {v0}, Lcom/squareup/workflow/legacyintegration/FakeKClass;-><init>()V

    check-cast v0, Lkotlin/reflect/KClass;

    .line 578
    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/workflow/legacyintegration/LegacyState;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 581
    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-class v2, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-direct {p1, v1, v2, v3}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    const-string v1, ""

    .line 580
    invoke-virtual {p1, v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p1

    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {v1, p1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object v1
.end method

.method public final legacyHandleFromSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 570
    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Companion;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Companion;->getEMPTY_INPUT$invoices_hairball_release()Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    move-result-object v2

    .line 584
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowPool;->Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    .line 585
    new-instance v0, Lcom/squareup/workflow/legacyintegration/FakeKClass;

    invoke-direct {v0}, Lcom/squareup/workflow/legacyintegration/FakeKClass;-><init>()V

    check-cast v0, Lkotlin/reflect/KClass;

    .line 586
    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, v0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/workflow/legacyintegration/LegacyState;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 589
    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-class v2, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-direct {p1, v1, v2, v3}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    const-string v1, ""

    .line 588
    invoke-virtual {p1, v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p1

    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {v1, p1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object v1
.end method
