.class public final Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ChooseDateCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;,
        Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseDateCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseDateCoordinator.kt\ncom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator\n+ 2 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,255:1\n17#2,2:256\n1651#3,3:258\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseDateCoordinator.kt\ncom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator\n*L\n135#1,2:256\n170#1,3:258\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0018\u0000 42\u00020\u0001:\u000245B-\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u001e\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001bH\u0002J\u0010\u0010\u001d\u001a\u00020\u00172\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0010\u0010 \u001a\u00020\u00172\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0010\u0010!\u001a\u00020\u000e2\u0006\u0010\"\u001a\u00020\u001cH\u0002J\u0018\u0010#\u001a\u00020$2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\"\u001a\u00020\u001cH\u0002J,\u0010%\u001a\u00020\u00172\u0006\u0010&\u001a\u00020\'2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001b2\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u00060)H\u0002J\u0017\u0010*\u001a\u00020\u00172\u0008\u0010&\u001a\u0004\u0018\u00010\'H\u0002\u00a2\u0006\u0002\u0010+J$\u0010,\u001a\u00020\u00172\u000c\u0010-\u001a\u0008\u0012\u0004\u0012\u00020\u00060)2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001bH\u0002J\u001e\u0010.\u001a\u00020\u00172\u0006\u0010/\u001a\u0002002\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u00060)H\u0002J\u001e\u00101\u001a\u00020\u00172\u0006\u0010\u001e\u001a\u00020\u001f2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001bH\u0002J(\u00102\u001a\u00020\u00172\u0016\u00103\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010\u001e\u001a\u00020\u001fH\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateScreen;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "(Lio/reactivex/Observable;Ljava/text/DateFormat;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "customDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "dateOptionsContainer",
        "Lcom/squareup/widgets/CheckableGroup;",
        "selectedDate",
        "shouldShowActualDate",
        "",
        "startDate",
        "suppressCheckChangeEvent",
        "addDateOptions",
        "",
        "context",
        "Landroid/content/Context;",
        "dateOptions",
        "",
        "Lcom/squareup/invoices/workflow/edit/DateOption;",
        "attach",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "getDateFromOption",
        "option",
        "getOptionText",
        "",
        "onOptionSelected",
        "index",
        "",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "setCheckedOption",
        "(Ljava/lang/Integer;)V",
        "setListeners",
        "workflowInput",
        "setTitle",
        "title",
        "",
        "showDateOptions",
        "update",
        "screen",
        "Companion",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private customDate:Lcom/squareup/protos/common/time/YearMonthDay;

.field private final dateFormat:Ljava/text/DateFormat;

.field private dateOptionsContainer:Lcom/squareup/widgets/CheckableGroup;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private selectedDate:Lcom/squareup/protos/common/time/YearMonthDay;

.field private shouldShowActualDate:Z

.field private startDate:Lcom/squareup/protos/common/time/YearMonthDay;

.field private suppressCheckChangeEvent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->Companion:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;

    return-void
.end method

.method private constructor <init>(Lio/reactivex/Observable;Ljava/text/DateFormat;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            ">;>;",
            "Ljava/text/DateFormat;",
            ")V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->dateFormat:Ljava/text/DateFormat;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Ljava/text/DateFormat;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;-><init>(Lio/reactivex/Observable;Ljava/text/DateFormat;)V

    return-void
.end method

.method public static final synthetic access$getSuppressCheckChangeEvent$p(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;)Z
    .locals 0

    .line 42
    iget-boolean p0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->suppressCheckChangeEvent:Z

    return p0
.end method

.method public static final synthetic access$onOptionSelected(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;ILjava/util/List;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->onOptionSelected(ILjava/util/List;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$setSuppressCheckChangeEvent$p(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;Z)V
    .locals 0

    .line 42
    iput-boolean p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->suppressCheckChangeEvent:Z

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->update(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    return-void
.end method

.method private final addDateOptions(Landroid/content/Context;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;)V"
        }
    .end annotation

    .line 167
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->dateOptionsContainer:Lcom/squareup/widgets/CheckableGroup;

    const-string v1, "dateOptionsContainer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->removeAllViews()V

    .line 169
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 170
    check-cast p2, Ljava/lang/Iterable;

    .line 259
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v5, v3, 0x1

    if-gez v3, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_1
    check-cast v4, Lcom/squareup/invoices/workflow/edit/DateOption;

    .line 172
    iget-object v6, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->dateOptionsContainer:Lcom/squareup/widgets/CheckableGroup;

    if-nez v6, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0, p1, v4}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->getOptionText(Landroid/content/Context;Lcom/squareup/invoices/workflow/edit/DateOption;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 171
    invoke-static {v0, v6, v3, v4, v2}, Lcom/squareup/ui/CheckableGroups;->addCheckableRow(Landroid/view/LayoutInflater;Lcom/squareup/widgets/CheckableGroup;ILjava/lang/CharSequence;Z)V

    move v3, v5

    goto :goto_0

    :cond_3
    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 197
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 199
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_date_options:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/CheckableGroup;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->dateOptionsContainer:Lcom/squareup/widgets/CheckableGroup;

    return-void
.end method

.method private final getDateFromOption(Lcom/squareup/invoices/workflow/edit/DateOption;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 4

    .line 204
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->Companion:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->startDate:Lcom/squareup/protos/common/time/YearMonthDay;

    if-nez v1, :cond_0

    const-string v2, "startDate"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;->getRelativeDays()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;->access$plus(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;Lcom/squareup/protos/common/time/YearMonthDay;J)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    goto :goto_0

    .line 205
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;->getDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    :goto_0
    return-object p1

    .line 206
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get date from DateOption type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final getOptionText(Landroid/content/Context;Lcom/squareup/invoices/workflow/edit/DateOption;)Ljava/lang/CharSequence;
    .locals 5

    .line 123
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/DateOption;->getTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 126
    instance-of v1, p2, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->Companion:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->startDate:Lcom/squareup/protos/common/time/YearMonthDay;

    if-nez v2, :cond_0

    const-string v3, "startDate"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p2, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;->getRelativeDays()J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;->access$plus(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;Lcom/squareup/protos/common/time/YearMonthDay;J)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    goto :goto_0

    .line 127
    :cond_1
    instance-of v1, p2, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;

    if-eqz v1, :cond_2

    check-cast p2, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;->getDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    goto :goto_0

    .line 128
    :cond_2
    instance-of v1, p2, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;

    if-eqz v1, :cond_3

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->customDate:Lcom/squareup/protos/common/time/YearMonthDay;

    goto :goto_0

    .line 129
    :cond_3
    instance-of p2, p2, Lcom/squareup/invoices/workflow/edit/DateOption$Never;

    if-eqz p2, :cond_5

    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_4

    .line 132
    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->shouldShowActualDate:Z

    if-eqz v1, :cond_4

    .line 133
    invoke-static {p2}, Lcom/squareup/util/ProtoDates;->calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Calendar;

    move-result-object p2

    .line 135
    new-instance v1, Landroid/text/SpannableString;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->dateFormat:Ljava/text/DateFormat;

    const-string v4, "calendar"

    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p2

    invoke-virtual {v3, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ")"

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-direct {v1, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 136
    check-cast v1, Landroid/text/Spannable;

    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v2, 0x0

    .line 257
    new-instance v3, Lcom/squareup/fonts/FontSpan;

    invoke-static {p2, v2}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result p2

    invoke-direct {v3, p1, p2}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast v3, Landroid/text/style/CharacterStyle;

    .line 136
    invoke-static {v1, v3}, Lcom/squareup/text/Spannables;->span(Landroid/text/Spannable;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    .line 138
    new-instance p2, Landroid/text/style/ForegroundColorSpan;

    .line 139
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v2, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 138
    invoke-direct {p2, p1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    check-cast p2, Landroid/text/style/CharacterStyle;

    .line 137
    invoke-static {v1, p2}, Lcom/squareup/text/Spannables;->span(Landroid/text/Spannable;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    .line 144
    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 147
    :cond_4
    check-cast v0, Ljava/lang/CharSequence;

    return-object v0

    .line 129
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final onOptionSelected(ILjava/util/List;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            ">;)V"
        }
    .end annotation

    .line 155
    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/DateOption;

    .line 157
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;

    if-eqz p2, :cond_0

    sget-object p1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent$CustomDateClicked;->INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent$CustomDateClicked;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    goto :goto_0

    .line 158
    :cond_0
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/DateOption$Never;

    if-eqz p2, :cond_1

    sget-object p1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent$NeverClicked;->INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent$NeverClicked;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    goto :goto_0

    .line 159
    :cond_1
    new-instance p2, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent$DateSelected;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->getDateFromOption(Lcom/squareup/invoices/workflow/edit/DateOption;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent$DateSelected;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;)V

    invoke-interface {p3, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private final setCheckedOption(Ljava/lang/Integer;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 180
    iput-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->suppressCheckChangeEvent:Z

    .line 181
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->dateOptionsContainer:Lcom/squareup/widgets/CheckableGroup;

    if-nez v0, :cond_1

    const-string v1, "dateOptionsContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    const/4 p1, 0x0

    .line 182
    iput-boolean p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->suppressCheckChangeEvent:Z

    return-void
.end method

.method private final setListeners(Lcom/squareup/workflow/legacy/WorkflowInput;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;)V"
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->dateOptionsContainer:Lcom/squareup/widgets/CheckableGroup;

    const-string v1, "dateOptionsContainer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$setListeners$1;

    invoke-direct {v2, p0, p2, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$setListeners$1;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;Ljava/util/List;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->dateOptionsContainer:Lcom/squareup/widgets/CheckableGroup;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$setListeners$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$setListeners$2;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;Ljava/util/List;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedClickListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;)V

    return-void
.end method

.method private final setTitle(Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            ">;)V"
        }
    .end annotation

    .line 189
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 190
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 191
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$setTitle$configBuilder$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$setTitle$configBuilder$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 193
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez p2, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final showDateOptions(Landroid/view/View;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;)V"
        }
    .end annotation

    .line 106
    sget-object v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->Companion:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->startDate:Lcom/squareup/protos/common/time/YearMonthDay;

    if-nez v1, :cond_0

    const-string v2, "startDate"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->selectedDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v0, p2, v1, v2}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;->access$getSelectedOptionIndexFromDate(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Companion;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 107
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    .line 110
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->customDate:Lcom/squareup/protos/common/time/YearMonthDay;

    if-nez v1, :cond_2

    .line 111
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->selectedDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->customDate:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 114
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v1, "view.context"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->addDateOptions(Landroid/content/Context;Ljava/util/List;)V

    .line 116
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->setCheckedOption(Ljava/lang/Integer;)V

    return-void
.end method

.method private final update(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 73
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->setTitle(Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 74
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getStartDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->startDate:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 75
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getSelectedDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->selectedDate:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 76
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getCustomDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->customDate:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 77
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getShouldShowActualDate()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->shouldShowActualDate:Z

    .line 79
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getDateOptions()Ljava/util/List;

    move-result-object v0

    .line 80
    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0, v1, v0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->setListeners(Lcom/squareup/workflow/legacy/WorkflowInput;Ljava/util/List;)V

    .line 81
    invoke-direct {p0, p2, v0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->showDateOptions(Landroid/view/View;Ljava/util/List;)V

    .line 82
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$update$1;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$update$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->bindViews(Landroid/view/View;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
