.class final Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$update$4;
.super Ljava/lang/Object;
.source "RecurringEndsCoordinator.kt"

# interfaces
.implements Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->update(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "onQuantityChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/workflow/legacy/Screen;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$update$4;->$screen:Lcom/squareup/workflow/legacy/Screen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onQuantityChanged(I)V
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$update$4;->$screen:Lcom/squareup/workflow/legacy/Screen;

    iget-object v0, v0, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getEndCount()I

    move-result v0

    if-eq p1, v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$update$4;->$screen:Lcom/squareup/workflow/legacy/Screen;

    iget-object v0, v0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$NumberSelected;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$NumberSelected;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method
