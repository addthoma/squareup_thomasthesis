.class public abstract Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;
.super Ljava/lang/Object;
.source "EditPaymentRequestState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Editing;,
        Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ConfirmingRemoval;,
        Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Validating;,
        Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ShowingValidationError;,
        Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ChoosingDate;,
        Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u000b2\u00020\u0001:\u0006\n\u000b\u000c\r\u000e\u000fB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\r\u0010\u0007\u001a\u00020\u0008H\u0000\u00a2\u0006\u0002\u0008\tR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0005\u0010\u0011\u0012\u0013\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;",
        "",
        "paymentRequestInfo",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
        "(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)V",
        "getPaymentRequestInfo",
        "()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
        "takeSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "takeSnapshot$invoices_hairball_release",
        "ChoosingDate",
        "Companion",
        "ConfirmingRemoval",
        "Editing",
        "ShowingValidationError",
        "Validating",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Editing;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ConfirmingRemoval;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Validating;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ShowingValidationError;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ChoosingDate;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;


# instance fields
.field private final paymentRequestInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->paymentRequestInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)V

    return-void
.end method

.method public static final createInfo(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;ZLcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;
    .locals 6
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;->createInfo(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;ZLcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->paymentRequestInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    return-object v0
.end method

.method public final takeSnapshot$invoices_hairball_release()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 65
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$takeSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$takeSnapshot$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
