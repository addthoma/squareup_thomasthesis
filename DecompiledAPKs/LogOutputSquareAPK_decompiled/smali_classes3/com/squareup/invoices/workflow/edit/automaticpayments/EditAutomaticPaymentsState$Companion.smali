.class public final Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState$Companion;
.super Ljava/lang/Object;
.source "EditAutomaticPaymentsState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditAutomaticPaymentsState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditAutomaticPaymentsState.kt\ncom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,21:1\n180#2:22\n*E\n*S KotlinDebug\n*F\n+ 1 EditAutomaticPaymentsState.kt\ncom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState$Companion\n*L\n15#1:22\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState$Companion;",
        "",
        "()V",
        "restoreSnapshot",
        "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final restoreSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;
    .locals 1

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    .line 22
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 16
    new-instance v0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;-><init>(Z)V

    return-object v0
.end method
