.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory_Factory;
.super Ljava/lang/Object;
.source "EditPaymentScheduleViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final editPaymentRequestV2ViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2ViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final editPaymentScheduleViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2ViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory_Factory;->editPaymentScheduleViewFactoryProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory_Factory;->editPaymentRequestV2ViewFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2ViewFactory;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenViewFactory;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2ViewFactory;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenViewFactory;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2ViewFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory_Factory;->editPaymentScheduleViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenViewFactory;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory_Factory;->editPaymentRequestV2ViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2ViewFactory;

    invoke-static {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory_Factory;->newInstance(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenViewFactory;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2ViewFactory;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory_Factory;->get()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;

    move-result-object v0

    return-object v0
.end method
