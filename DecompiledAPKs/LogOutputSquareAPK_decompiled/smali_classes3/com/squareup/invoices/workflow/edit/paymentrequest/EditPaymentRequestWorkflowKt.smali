.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflowKt;
.super Ljava/lang/Object;
.source "EditPaymentRequestWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentRequestWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentRequestWorkflow.kt\ncom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflowKt\n+ 2 ProtosPure.kt\ncom/squareup/util/ProtosPure\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,325:1\n132#2,3:326\n132#2,3:329\n149#3,5:332\n149#3,5:337\n149#3,5:342\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentRequestWorkflow.kt\ncom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflowKt\n*L\n300#1,3:326\n303#1,3:329\n317#1,5:332\n318#1,5:337\n324#1,5:342\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u001aX\u0010\u0000\u001a$\u0012\u0004\u0012\u00020\u0002\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0005\"\n\u0008\u0000\u0010\u0006\u0018\u0001*\u00020\u0007\"\n\u0008\u0001\u0010\u0008\u0018\u0001*\u00020\u00072\u0006\u0010\t\u001a\u0002H\u00062\u0006\u0010\n\u001a\u0002H\u0008H\u0082\u0008\u00a2\u0006\u0002\u0010\u000b\u001a@\u0010\u000c\u001a$\u0012\u0004\u0012\u00020\u0002\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0005\"\n\u0008\u0000\u0010\u0006\u0018\u0001*\u00020\u0007*\u0002H\u0006H\u0082\u0008\u00a2\u0006\u0002\u0010\r\u001a\u000e\u0010\u000e\u001a\u0004\u0018\u00010\u000f*\u00020\u0010H\u0002\u001a%\u0010\u0011\u001a\u00020\u0010*\u00020\u00102\u0017\u0010\u0012\u001a\u0013\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00150\u0013\u00a2\u0006\u0002\u0008\u0016H\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "cardDialogStack",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "ScreenT",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "ScreenV",
        "card",
        "dialog",
        "(Lcom/squareup/workflow/legacy/V2Screen;Lcom/squareup/workflow/legacy/V2Screen;)Ljava/util/Map;",
        "asCardLayering",
        "(Lcom/squareup/workflow/legacy/V2Screen;)Ljava/util/Map;",
        "getInitialRequestedAmount",
        "Lcom/squareup/protos/common/Money;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
        "updatePaymentRequest",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$asCardLayering(Lcom/squareup/workflow/legacy/V2Screen;)Ljava/util/Map;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflowKt;->asCardLayering(Lcom/squareup/workflow/legacy/V2Screen;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$cardDialogStack(Lcom/squareup/workflow/legacy/V2Screen;Lcom/squareup/workflow/legacy/V2Screen;)Ljava/util/Map;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflowKt;->cardDialogStack(Lcom/squareup/workflow/legacy/V2Screen;Lcom/squareup/workflow/legacy/V2Screen;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getInitialRequestedAmount(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflowKt;->getInitialRequestedAmount(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method private static final synthetic asCardLayering(Lcom/squareup/workflow/legacy/V2Screen;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ScreenT::",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            ">(TScreenT;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 343
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    const/4 v1, 0x4

    const-string v2, "ScreenT"

    .line 344
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Lcom/squareup/workflow/legacy/V2Screen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 345
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 343
    invoke-direct {v0, v1, p0, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 324
    sget-object p0, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {v0, p0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method private static final synthetic cardDialogStack(Lcom/squareup/workflow/legacy/V2Screen;Lcom/squareup/workflow/legacy/V2Screen;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ScreenT::",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            "ScreenV::",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            ">(TScreenT;TScreenV;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 316
    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 333
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    const/4 v2, 0x4

    const-string v3, "ScreenT"

    .line 334
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v3, Lcom/squareup/workflow/legacy/V2Screen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, ""

    invoke-static {v3, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 335
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 333
    invoke-direct {v1, v3, p0, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 338
    new-instance p0, Lcom/squareup/workflow/legacy/Screen;

    const-string v3, "ScreenV"

    .line 339
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v2, Lcom/squareup/workflow/legacy/V2Screen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 340
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 338
    invoke-direct {p0, v2, p1, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 316
    invoke-virtual {v0, v1, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    .line 320
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    sget-object v0, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p0, p1, v0}, Lcom/squareup/container/LayeredScreensKt;->toPosScreen(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method private static final getInitialRequestedAmount(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)Lcom/squareup/protos/common/Money;
    .locals 2

    .line 310
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getPaymentRequestType()Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    move-result-object p0

    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p0, v1

    :cond_0
    check-cast p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->getRemovalInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    move-result-object p0

    goto :goto_0

    :cond_1
    move-object p0, v1

    :goto_0
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$Removable;

    if-nez v0, :cond_2

    move-object p0, v1

    :cond_2
    check-cast p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$Removable;

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$Removable;->getInitialRequestedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    :cond_3
    return-object v1
.end method

.method public static final updatePaymentRequest(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Lkotlin/jvm/functions/Function1;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;"
        }
    .end annotation

    const-string v0, "$this$updatePaymentRequest"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 296
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getPaymentRequestType()Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    move-result-object v0

    .line 299
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    const-string v2, "null cannot be cast to non-null type B"

    const-string v3, "paymentRequestType.paymentRequest.buildUpon(block)"

    if-eqz v1, :cond_1

    move-object v4, v0

    check-cast v4, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    .line 300
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    .line 327
    invoke-virtual {v0}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    invoke-virtual {v0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, p1

    check-cast v5, Lcom/squareup/protos/client/invoice/PaymentRequest;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xe

    const/4 v10, 0x0

    .line 299
    invoke-static/range {v4 .. v10}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->copy$default(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    :goto_0
    move-object v1, p1

    goto :goto_1

    .line 327
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    invoke-direct {p0, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 302
    :cond_1
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    if-eqz v1, :cond_3

    move-object v4, v0

    check-cast v4, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    .line 303
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    .line 330
    invoke-virtual {v0}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    invoke-virtual {v0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, p1

    check-cast v5, Lcom/squareup/protos/client/invoice/PaymentRequest;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x6

    const/4 v9, 0x0

    .line 302
    invoke-static/range {v4 .. v9}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->copy$default(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    goto :goto_0

    :goto_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    .line 297
    invoke-static/range {v0 .. v5}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->copy$default(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object p0

    return-object p0

    .line 330
    :cond_2
    new-instance p0, Lkotlin/TypeCastException;

    invoke-direct {p0, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 302
    :cond_3
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
