.class public final Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;
.super Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;
.source "AutomaticRemindersState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EditReminder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder$Creator;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAutomaticRemindersState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AutomaticRemindersState.kt\ncom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder\n*L\n1#1,58:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0019\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0087\u0008\u0018\u0000 /2\u00020\u0001:\u0001/B?\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0008\u0002\u0010\r\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u001b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001f\u001a\u00020\tH\u00c6\u0003J\t\u0010 \u001a\u00020\u000cH\u00c6\u0003J\t\u0010!\u001a\u00020\u0005H\u00c6\u0003JO\u0010\"\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\t2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u0005H\u00c6\u0001J\t\u0010#\u001a\u00020\u0005H\u00d6\u0001J\u0013\u0010$\u001a\u00020\u00072\u0008\u0010%\u001a\u0004\u0018\u00010&H\u00d6\u0003J\t\u0010\'\u001a\u00020\u0005H\u00d6\u0001J\t\u0010(\u001a\u00020)H\u00d6\u0001J\u0019\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\r\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0014\u0010\n\u001a\u00020\tX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0010R\u0014\u0010\u000b\u001a\u00020\u000cX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0014\u0010\u0008\u001a\u00020\tX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;",
        "tempReminderToEdit",
        "Lcom/squareup/invoices/workflow/edit/InvoiceReminder;",
        "editingIndex",
        "",
        "remindersEnabled",
        "",
        "remindersListInfo",
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "defaultListInfo",
        "reminderSettings",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "cachedDays",
        "(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;IZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;I)V",
        "getCachedDays",
        "()I",
        "getDefaultListInfo",
        "()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "getEditingIndex",
        "getReminderSettings",
        "()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "getRemindersEnabled",
        "()Z",
        "getRemindersListInfo",
        "getTempReminderToEdit",
        "()Lcom/squareup/invoices/workflow/edit/InvoiceReminder;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "describeContents",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final Companion:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder$Companion;

.field public static final NEW_REMINDER:I = -0x1


# instance fields
.field private final cachedDays:I

.field private final defaultListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

.field private final editingIndex:I

.field private final reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

.field private final remindersEnabled:Z

.field private final remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

.field private final tempReminderToEdit:Lcom/squareup/invoices/workflow/edit/InvoiceReminder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->Companion:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder$Companion;

    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder$Creator;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder$Creator;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;IZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;I)V
    .locals 1

    const-string v0, "tempReminderToEdit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "remindersListInfo"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultListInfo"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reminderSettings"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->tempReminderToEdit:Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    iput p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->editingIndex:I

    iput-boolean p3, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->remindersEnabled:Z

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->defaultListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    iput p7, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->cachedDays:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;IZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getRelativeDays()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    move v8, v0

    goto :goto_0

    :cond_0
    move/from16 v8, p7

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;IZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;I)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;Lcom/squareup/invoices/workflow/edit/InvoiceReminder;IZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;IILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->tempReminderToEdit:Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->editingIndex:I

    :cond_1
    move p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersEnabled()Z

    move-result p3

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object p4

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object p5

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object p6

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget p7, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->cachedDays:I

    :cond_6
    move v4, p7

    move-object p2, p0

    move-object p3, p1

    move p4, p9

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->copy(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;IZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;I)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/invoices/workflow/edit/InvoiceReminder;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->tempReminderToEdit:Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->editingIndex:I

    return v0
.end method

.method public final component3()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersEnabled()Z

    move-result v0

    return v0
.end method

.method public final component4()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    return-object v0
.end method

.method public final component5()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    return-object v0
.end method

.method public final component6()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v0

    return-object v0
.end method

.method public final component7()I
    .locals 1

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->cachedDays:I

    return v0
.end method

.method public final copy(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;IZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;I)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;
    .locals 9

    const-string v0, "tempReminderToEdit"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "remindersListInfo"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultListInfo"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reminderSettings"

    move-object v7, p6

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    move-object v1, v0

    move v3, p2

    move v4, p3

    move/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;IZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;I)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->tempReminderToEdit:Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->tempReminderToEdit:Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->editingIndex:I

    iget v1, p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->editingIndex:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersEnabled()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersEnabled()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->cachedDays:I

    iget p1, p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->cachedDays:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCachedDays()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->cachedDays:I

    return v0
.end method

.method public getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->defaultListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    return-object v0
.end method

.method public final getEditingIndex()I
    .locals 1

    .line 30
    iget v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->editingIndex:I

    return v0
.end method

.method public getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    return-object v0
.end method

.method public getRemindersEnabled()Z
    .locals 1

    .line 31
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->remindersEnabled:Z

    return v0
.end method

.method public getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    return-object v0
.end method

.method public final getTempReminderToEdit()Lcom/squareup/invoices/workflow/edit/InvoiceReminder;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->tempReminderToEdit:Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->tempReminderToEdit:Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->editingIndex:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->cachedDays:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EditReminder(tempReminderToEdit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->tempReminderToEdit:Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", editingIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->editingIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", remindersEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", remindersListInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", defaultListInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", reminderSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cachedDays="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->cachedDays:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->tempReminderToEdit:Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->editingIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->remindersEnabled:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->defaultListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->cachedDays:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
