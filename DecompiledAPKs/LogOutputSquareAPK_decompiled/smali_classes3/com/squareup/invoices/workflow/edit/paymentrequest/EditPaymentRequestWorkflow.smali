.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "EditPaymentRequestWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentRequestWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentRequestWorkflow.kt\ncom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow\n+ 2 EditPaymentRequestWorkflow.kt\ncom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflowKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 6 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,325:1\n324#2:326\n316#2,5:332\n317#2:337\n318#2:343\n324#2:352\n316#2,5:358\n317#2:363\n318#2:369\n149#3,5:327\n149#3,5:338\n149#3,5:344\n149#3,5:353\n149#3,5:364\n149#3,5:370\n85#4:349\n240#5:350\n276#6:351\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentRequestWorkflow.kt\ncom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow\n*L\n183#1:326\n186#1,5:332\n186#1:337\n186#1:343\n228#1:352\n232#1,5:358\n232#1:363\n232#1:369\n183#1,5:327\n186#1,5:338\n186#1,5:344\n228#1,5:353\n232#1,5:364\n232#1,5:370\n212#1:349\n212#1:350\n212#1:351\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0094\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u0000 .2<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0001.B?\u0008\u0017\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0002\u0010\u0017BU\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0014\u0012\u001c\u0010\u0015\u001a\u0018\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001b0\u0018j\u0002`\u001c\u00a2\u0006\u0002\u0010\u001dJt\u0010 \u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0002\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u00050!\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0018j6\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t`\"J\u001a\u0010#\u001a\u00020\u00032\u0006\u0010$\u001a\u00020\u00022\u0008\u0010%\u001a\u0004\u0018\u00010&H\u0016JN\u0010\'\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010$\u001a\u00020\u00022\u0006\u0010(\u001a\u00020\u00032\u0012\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040*H\u0016J\u0010\u0010+\u001a\u00020&2\u0006\u0010(\u001a\u00020\u0003H\u0016J\u000c\u0010,\u001a\u00020-*\u00020\u0002H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000RN\u0010\u001e\u001aB\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001b\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "chooseDateInfoFactory",
        "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;",
        "validator",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "res",
        "Lcom/squareup/util/Res;",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "dateReactor",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;",
        "(Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;)V",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateLauncher;",
        "(Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;)V",
        "chooseDateWorkflow",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;",
        "asLegacyLauncher",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/workflow/legacyintegration/LegacyLauncher;",
        "initialState",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "removalMessage",
        "",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$Companion;


# instance fields
.field private final chooseDateInfoFactory:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

.field private final chooseDateWorkflow:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation
.end field

.field private final mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final validator:Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;)V
    .locals 8
    .param p5    # Lkotlinx/coroutines/CoroutineDispatcher;
        .annotation runtime Lcom/squareup/thread/Main$Immediate;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "chooseDateInfoFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "validator"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateReactor"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    move-object v7, p6

    check-cast v7, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 90
    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;)V
    .locals 1
    .param p5    # Lkotlinx/coroutines/CoroutineDispatcher;
        .annotation runtime Lcom/squareup/thread/Main$Immediate;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            "+",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "chooseDateInfoFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "validator"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateReactor"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->chooseDateInfoFactory:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->validator:Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    .line 96
    sget-object p1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;

    check-cast p1, Lcom/squareup/workflow/legacy/Renderer;

    .line 97
    sget-object p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$chooseDateWorkflow$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$chooseDateWorkflow$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 98
    sget-object p3, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$chooseDateWorkflow$2;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$chooseDateWorkflow$2;

    check-cast p3, Lkotlin/jvm/functions/Function1;

    .line 95
    invoke-static {p6, p1, p2, p3}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt;->asV2Workflow(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/Renderer;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->chooseDateWorkflow:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    return-void
.end method

.method public static final synthetic access$removalMessage(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)Ljava/lang/String;
    .locals 0

    .line 69
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->removalMessage(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final removalMessage(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)Ljava/lang/String;
    .locals 2

    .line 264
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflowKt;->access$getInitialRequestedAmount(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 268
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->remove_deposit_request_confirm_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 269
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v1, "amount"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 270
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 271
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 264
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Can\'t remove a PaymentRequest if the RemovalInfo isn\'t of type Removable."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public final asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;",
            ">;"
        }
    .end annotation

    .line 260
    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacyintegration/LegacyLauncherKt;->createLegacyLauncher(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object v0

    return-object v0
.end method

.method public initialState(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;
    .locals 1

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 105
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;

    invoke-virtual {v0, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;->restoreSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    move-result-object p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Editing;

    invoke-direct {p2, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Editing;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)V

    check-cast p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    :goto_0
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 69
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->initialState(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 69
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    check-cast p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->render(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    instance-of p1, p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ChoosingDate;

    if-eqz p1, :cond_1

    .line 115
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->chooseDateWorkflow:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 116
    sget-object p1, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;->Companion:Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;

    .line 117
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->chooseDateInfoFactory:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

    .line 118
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->getPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getPaymentRequestType()Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    move-result-object v2

    .line 119
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->getPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getInvoiceFirstSentDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    .line 117
    invoke-virtual {v0, v2, v3}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->createForPaymentRequest(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-result-object v0

    .line 116
    invoke-virtual {p1, v0}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;->startState(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;)Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;

    move-result-object v2

    const/4 v3, 0x0

    .line 122
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$1;

    invoke-direct {p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 114
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;->getRendering()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    if-eqz p1, :cond_0

    goto/16 :goto_0

    .line 132
    :cond_0
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 134
    :cond_1
    instance-of p1, p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Editing;

    const-string v0, ""

    if-eqz p1, :cond_2

    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;

    .line 135
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->getPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v1

    .line 136
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2;

    invoke-direct {v2, p0, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 134
    invoke-direct {p1, v1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 328
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 329
    const-class p3, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 330
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 328
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 326
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 185
    :cond_2
    instance-of p1, p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ConfirmingRemoval;

    if-eqz p1, :cond_3

    .line 187
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;

    .line 188
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->getPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v1

    .line 189
    sget-object v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$3;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 187
    invoke-direct {p1, v1, v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 192
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;

    .line 193
    move-object v2, p2

    check-cast v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ConfirmingRemoval;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ConfirmingRemoval;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 194
    new-instance v3, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$4;

    invoke-direct {v3, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$4;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 192
    invoke-direct {v1, v2, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 332
    sget-object p2, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 339
    new-instance p3, Lcom/squareup/workflow/legacy/Screen;

    .line 340
    const-class v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 341
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 339
    invoke-direct {p3, v2, p1, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 345
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 346
    const-class v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/ConfirmRemovePaymentRequestDialogScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 347
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 345
    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 332
    invoke-virtual {p2, p3, p1}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    .line 336
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    sget-object p3, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2, p3}, Lcom/squareup/container/LayeredScreensKt;->toPosScreen(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 206
    :cond_3
    instance-of p1, p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Validating;

    if-eqz p1, :cond_4

    .line 208
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->validator:Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;

    .line 209
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->getPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getPaymentRequestType()Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    move-result-object v1

    .line 210
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->getPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 211
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->getPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getInvoiceFirstSentDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    .line 208
    invoke-virtual {p1, v1, v2, v3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;->validate(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)Lio/reactivex/Single;

    move-result-object p1

    .line 349
    sget-object v1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$$inlined$asWorker$1;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 350
    invoke-static {v1}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 351
    const-class v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 213
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$5;

    invoke-direct {p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$5;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p3

    .line 207
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContextKt;->onWorkerOutput$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 225
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;

    .line 226
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->getPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object p2

    .line 227
    sget-object p3, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$6;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$6;

    check-cast p3, Lkotlin/jvm/functions/Function1;

    .line 225
    invoke-direct {p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 354
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 355
    const-class p3, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 356
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 354
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 352
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 231
    :cond_4
    instance-of p1, p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ShowingValidationError;

    if-eqz p1, :cond_5

    .line 233
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;

    .line 234
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->getPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v1

    .line 235
    sget-object v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$7;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$7;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 233
    invoke-direct {p1, v1, v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 238
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogScreen;

    .line 239
    move-object v2, p2

    check-cast v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ShowingValidationError;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ShowingValidationError;->getValidationErrorInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;

    move-result-object v2

    .line 240
    new-instance v3, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$8;

    invoke-direct {v3, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$8;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 238
    invoke-direct {v1, v2, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogScreen;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 358
    sget-object p2, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 365
    new-instance p3, Lcom/squareup/workflow/legacy/Screen;

    .line 366
    const-class v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 367
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 365
    invoke-direct {p3, v2, p1, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 371
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 372
    const-class v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorDialogScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 373
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 371
    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 358
    invoke-virtual {p2, p3, p1}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    .line 362
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    sget-object p3, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2, p3}, Lcom/squareup/container/LayeredScreensKt;->toPosScreen(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->takeSnapshot$invoices_hairball_release()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 69
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->snapshotState(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
