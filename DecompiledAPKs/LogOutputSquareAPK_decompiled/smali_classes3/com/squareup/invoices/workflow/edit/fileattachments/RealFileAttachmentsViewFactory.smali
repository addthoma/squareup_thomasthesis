.class public final Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealFileAttachmentsViewFactory.kt"

# interfaces
.implements Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileAttachmentsViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileAttachmentsViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "addInvoiceImageAttachmentDialogFactoryFactory",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;",
        "invoiceImageCoordinatorFactory",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;",
        "(Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;)V",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "addInvoiceImageAttachmentDialogFactoryFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceImageCoordinatorFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 11
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 12
    sget-object v2, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialog;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialog;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    new-instance v3, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$1;

    invoke-direct {v3, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$1;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 11
    invoke-virtual {v1, v2, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 15
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 16
    sget-object p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImage;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImage;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImage;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    sget v4, Lcom/squareup/features/invoices/R$layout;->invoice_attachment_view:I

    .line 17
    new-instance p1, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$2;

    invoke-direct {p1, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$2;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    .line 15
    invoke-static/range {v2 .. v9}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    .line 20
    sget-object p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    sget-object p2, Lcom/squareup/invoices/workflow/edit/fileattachments/ErrorDialog;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/ErrorDialog;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/ErrorDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$3;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2, v1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 p2, 0x2

    aput-object p1, v0, p2

    .line 22
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 23
    sget-object p1, Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmation;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmation;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmation;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    sget v3, Lcom/squareup/features/invoices/R$layout;->confirmation_view:I

    .line 24
    sget-object p1, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$4;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$4;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 22
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 p2, 0x3

    aput-object p1, v0, p2

    .line 10
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
