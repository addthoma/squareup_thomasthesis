.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditPaymentScheduleCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$Factory;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentScheduleCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentScheduleCoordinator.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator\n+ 2 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n+ 3 Views.kt\ncom/squareup/util/Views\n+ 4 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n*L\n1#1,306:1\n17#2,2:307\n1163#3:309\n1163#3:312\n118#4,2:310\n1046#4,2:313\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentScheduleCoordinator.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator\n*L\n85#1,2:307\n285#1:309\n291#1:312\n286#1,2:310\n291#1,2:313\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a2\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0002?@B/\u0008\u0002\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0018\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0002J\u0010\u0010\"\u001a\u00020\u001d2\u0006\u0010#\u001a\u00020$H\u0016J\u0010\u0010%\u001a\u00020\u001d2\u0006\u0010#\u001a\u00020$H\u0002J\u0018\u0010&\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020!2\u0006\u0010\'\u001a\u00020\u0004H\u0002J\u0018\u0010(\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020!2\u0006\u0010\'\u001a\u00020\u0004H\u0002J\u0010\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,H\u0002J\u0010\u0010-\u001a\u00020.2\u0006\u0010+\u001a\u00020,H\u0002J\u0018\u0010/\u001a\u00020*2\u0006\u00100\u001a\u0002012\u0006\u00102\u001a\u00020\u000fH\u0002J\u0010\u00103\u001a\u00020\u001d2\u0006\u0010+\u001a\u00020,H\u0002J\u0018\u00104\u001a\u00020\u001d2\u0006\u0010\'\u001a\u00020\u00042\u0006\u0010#\u001a\u00020$H\u0002J,\u00105\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020!2\u000c\u00106\u001a\u0008\u0012\u0004\u0012\u00020\u001d072\u000c\u00108\u001a\u0008\u0012\u0004\u0012\u00020\u001d07H\u0002J\u0014\u00109\u001a\u00020\u001d*\u00020$2\u0006\u0010:\u001a\u00020,H\u0002J\u0014\u0010;\u001a\u00020<*\u00020=2\u0006\u0010 \u001a\u00020!H\u0002J\u0014\u0010;\u001a\u00020<*\u00020>2\u0006\u0010 \u001a\u00020!H\u0002R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u00020\u000f8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006A"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "moneyLocaleDigitsKeyListenerFactory",
        "Lcom/squareup/money/MoneyDigitsKeyListenerFactory;",
        "unitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "(Lio/reactivex/Observable;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "currencySymbol",
        "",
        "getCurrencySymbol",
        "()Ljava/lang/String;",
        "mediumWeightStyle",
        "Landroid/text/style/CharacterStyle;",
        "nullStateMessageView",
        "Lcom/squareup/widgets/MessageView;",
        "redCharacterStyle",
        "sectionContainer",
        "Landroid/widget/LinearLayout;",
        "toggleRequestDepositRow",
        "Lcom/squareup/widgets/list/ToggleButtonRow;",
        "toggleSplitBalanceRow",
        "addBalanceSection",
        "",
        "balanceSection",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;",
        "resources",
        "Landroid/content/res/Resources;",
        "attach",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "displayBalanceSection",
        "screen",
        "displayDepositSection",
        "getSection",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;",
        "tag",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;",
        "hasSection",
        "",
        "paymentPlanView",
        "rowCount",
        "",
        "headerText",
        "removeSection",
        "update",
        "updateActionBar",
        "cancelClicked",
        "Lkotlin/Function0;",
        "saveClicked",
        "setSectionTag",
        "sectionTag",
        "toCharSequence",
        "",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;",
        "Factory",
        "SectionTag",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private mediumWeightStyle:Landroid/text/style/CharacterStyle;

.field private final moneyLocaleDigitsKeyListenerFactory:Lcom/squareup/money/MoneyDigitsKeyListenerFactory;

.field private nullStateMessageView:Lcom/squareup/widgets/MessageView;

.field private redCharacterStyle:Landroid/text/style/CharacterStyle;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;",
            ">;"
        }
    .end annotation
.end field

.field private sectionContainer:Landroid/widget/LinearLayout;

.field private toggleRequestDepositRow:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private toggleSplitBalanceRow:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private final unitFormatter:Lcom/squareup/quantity/PerUnitFormatter;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;)V
    .locals 0
    .param p3    # Lcom/squareup/money/MoneyDigitsKeyListenerFactory;
        .annotation runtime Lcom/squareup/money/ForMoney;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/money/MoneyDigitsKeyListenerFactory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ")V"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->moneyLocaleDigitsKeyListenerFactory:Lcom/squareup/money/MoneyDigitsKeyListenerFactory;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->unitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;Landroid/view/View;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->update(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;Landroid/view/View;)V

    return-void
.end method

.method private final addBalanceSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;Landroid/content/res/Resources;)V
    .locals 1

    .line 205
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;->getInstallmentRows()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    .line 206
    sget v0, Lcom/squareup/features/invoices/R$string;->balance_split_all_caps:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string v0, "resources.getString(R.st\u2026g.balance_split_all_caps)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->paymentPlanView(ILjava/lang/String;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;

    move-result-object p1

    .line 208
    check-cast p1, Landroid/view/View;

    sget-object p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->BALANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->setSectionTag(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)V

    .line 209
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->sectionContainer:Landroid/widget/LinearLayout;

    if-nez p2, :cond_0

    const-string v0, "sectionContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p2, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 272
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 274
    sget v0, Lcom/squareup/features/invoices/R$id;->request_deposit_toggle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->toggleRequestDepositRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 275
    sget v0, Lcom/squareup/features/invoices/R$id;->split_balance_toggle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->toggleSplitBalanceRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 276
    sget v0, Lcom/squareup/features/invoices/R$id;->section_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->sectionContainer:Landroid/widget/LinearLayout;

    .line 277
    sget v0, Lcom/squareup/features/invoices/R$id;->null_state_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->nullStateMessageView:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final displayBalanceSection(Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V
    .locals 11

    .line 158
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;->getBalanceSplitSection()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 160
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->BALANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    invoke-direct {p0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->hasSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->addBalanceSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;Landroid/content/res/Resources;)V

    goto :goto_0

    .line 167
    :cond_0
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->BALANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    invoke-direct {p0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->getSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->getRowCount()I

    move-result v1

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;->getInstallmentRows()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 168
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->BALANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    invoke-direct {p0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->removeSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)V

    .line 169
    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->addBalanceSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;Landroid/content/res/Resources;)V

    .line 174
    :cond_1
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;->getCanAddPayment()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton$Show;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;->getAddAnotherPaymentClicked()Lkotlin/jvm/functions/Function0;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton$Show;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;

    goto :goto_1

    .line 175
    :cond_2
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton$Disabled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton$Disabled;

    check-cast v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;

    :goto_1
    move-object v4, v1

    .line 177
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->BALANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    invoke-direct {p0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->getSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;

    move-result-object v2

    .line 178
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;->isPercentage()Z

    move-result v3

    .line 180
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;->getInstallmentRows()Ljava/util/List;

    move-result-object v5

    .line 181
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitSection;->getHelperText()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->toCharSequence(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 182
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$$inlined$also$lambda$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$$inlined$also$lambda$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V

    move-object v7, v1

    check-cast v7, Lkotlin/jvm/functions/Function2;

    .line 187
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$$inlined$also$lambda$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$$inlined$also$lambda$2;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function2;

    .line 192
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$$inlined$also$lambda$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$$inlined$also$lambda$3;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V

    move-object v9, v1

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 195
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$$inlined$also$lambda$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$$inlined$also$lambda$4;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V

    move-object v10, v1

    check-cast v10, Lkotlin/jvm/functions/Function1;

    .line 177
    invoke-virtual/range {v2 .. v10}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->setData(ZLcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;Ljava/util/List;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    if-eqz v0, :cond_3

    goto :goto_2

    .line 197
    :cond_3
    sget-object p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->BALANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->removeSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    :goto_2
    return-void
.end method

.method private final displayDepositSection(Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    .line 123
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;->getDepositSection()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositSection;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 124
    sget-object v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->DEPOSIT:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    invoke-direct {v0, v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->hasSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x2

    if-nez v4, :cond_1

    .line 127
    sget v4, Lcom/squareup/features/invoices/R$string;->deposit_request_all_caps:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v7, "resources.getString(R.st\u2026deposit_request_all_caps)"

    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-direct {v0, v6, v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->paymentPlanView(ILjava/lang/String;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;

    move-result-object v4

    .line 129
    check-cast v4, Landroid/view/View;

    sget-object v7, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->DEPOSIT:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    invoke-direct {v0, v4, v7}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->setSectionTag(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)V

    .line 130
    iget-object v7, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->sectionContainer:Landroid/widget/LinearLayout;

    if-nez v7, :cond_0

    const-string v8, "sectionContainer"

    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v7, v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 133
    :cond_1
    sget-object v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->DEPOSIT:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    invoke-direct {v0, v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->getSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;

    move-result-object v7

    .line 134
    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositSection;->isPercentage()Z

    move-result v8

    const/4 v9, 0x0

    new-array v4, v6, [Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;

    .line 135
    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositSection;->getDepositRow()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositSection;->getBalanceRow()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    .line 136
    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositSection;->getHelperText()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;

    move-result-object v4

    invoke-direct {v0, v4, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->toCharSequence(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v11

    .line 137
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayDepositSection$$inlined$also$lambda$1;

    invoke-direct {v4, v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayDepositSection$$inlined$also$lambda$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V

    move-object v12, v4

    check-cast v12, Lkotlin/jvm/functions/Function2;

    .line 140
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayDepositSection$$inlined$also$lambda$2;

    invoke-direct {v4, v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayDepositSection$$inlined$also$lambda$2;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V

    move-object v13, v4

    check-cast v13, Lkotlin/jvm/functions/Function2;

    .line 143
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayDepositSection$$inlined$also$lambda$3;

    invoke-direct {v4, v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayDepositSection$$inlined$also$lambda$3;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V

    move-object v14, v4

    check-cast v14, Lkotlin/jvm/functions/Function1;

    .line 147
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayDepositSection$$inlined$also$lambda$4;

    invoke-direct {v4, v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayDepositSection$$inlined$also$lambda$4;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V

    move-object v15, v4

    check-cast v15, Lkotlin/jvm/functions/Function1;

    const/16 v16, 0x2

    const/16 v17, 0x0

    .line 133
    invoke-static/range {v7 .. v17}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->setData$default(Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;ZLcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;Ljava/util/List;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    if-eqz v3, :cond_2

    goto :goto_0

    .line 151
    :cond_2
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->DEPOSIT:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->removeSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)V

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    :goto_0
    return-void
.end method

.method private final getCurrencySymbol()Ljava/lang/String;
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0}, Lcom/squareup/currency_db/Currencies;->getCurrencySymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "getCurrencySymbol(currencyCode)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;
    .locals 4

    .line 285
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->sectionContainer:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    const-string v1, "sectionContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    .line 309
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    invoke-static {v1, v2}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$getSection$$inlined$getChildren$1;

    invoke-direct {v2, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$getSection$$inlined$getChildren$1;-><init>(Landroid/view/ViewGroup;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 310
    invoke-interface {v0}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 286
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    .line 287
    check-cast v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;

    return-object v1

    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.paymentschedule.PaymentPlanSection"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 311
    :cond_3
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string v0, "Sequence contains no element matching the predicate."

    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final hasSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)Z
    .locals 4

    .line 291
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->sectionContainer:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    const-string v1, "sectionContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    .line 312
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v2, v1}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v1

    new-instance v3, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$hasSection$$inlined$getChildren$1;

    invoke-direct {v3, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$hasSection$$inlined$getChildren$1;-><init>(Landroid/view/ViewGroup;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v3}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 313
    invoke-interface {v0}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 291
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :cond_2
    return v2
.end method

.method private final paymentPlanView(ILjava/lang/String;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;
    .locals 7

    .line 216
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->Companion:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$Companion;

    .line 217
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->sectionContainer:Landroid/widget/LinearLayout;

    if-nez v1, :cond_0

    const-string v2, "sectionContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v1, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->moneyLocaleDigitsKeyListenerFactory:Lcom/squareup/money/MoneyDigitsKeyListenerFactory;

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->unitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 220
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->getCurrencySymbol()Ljava/lang/String;

    move-result-object v6

    move v4, p1

    move-object v5, p2

    .line 216
    invoke-virtual/range {v0 .. v6}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$Companion;->inflateFromParent(Landroid/view/ViewGroup;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;ILjava/lang/String;Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;

    move-result-object p1

    return-object p1
.end method

.method private final removeSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)V
    .locals 2

    .line 295
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->hasSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 296
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->sectionContainer:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    const-string v1, "sectionContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->getSection(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method private final setSectionTag(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;)V
    .locals 0

    .line 281
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->getTag()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method private final toCharSequence(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 9

    .line 245
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isPercentage()Z

    move-result v0

    const-string v1, "number"

    const-string v2, "balance_amount_money"

    const-string v3, "redCharacterStyle"

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x2

    const-string v7, "mediumWeightStyle"

    if-eqz v0, :cond_4

    .line 247
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isValid()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->getBalanceSplitAmount()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->mediumWeightStyle:Landroid/text/style/CharacterStyle;

    if-nez v3, :cond_0

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, v3}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0

    .line 248
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->getBalanceSplitAmount()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    new-array v6, v6, [Landroid/text/style/CharacterStyle;

    iget-object v8, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->redCharacterStyle:Landroid/text/style/CharacterStyle;

    if-nez v8, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    aput-object v8, v6, v5

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->mediumWeightStyle:Landroid/text/style/CharacterStyle;

    if-nez v3, :cond_3

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    aput-object v3, v6, v4

    invoke-static {v0, v6}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;[Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    .line 250
    :goto_0
    sget v3, Lcom/squareup/features/invoices/R$string;->payment_schedule_balance_section_percentage:I

    invoke-static {p2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 251
    check-cast v0, Ljava/lang/CharSequence;

    const-string v3, "percentage"

    invoke-virtual {p2, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 252
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->getBalanceAmount()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 253
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->getNumber()I

    move-result v0

    invoke-virtual {p2, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 254
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    goto :goto_2

    .line 257
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->isValid()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->getBalanceSplitAmount()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->mediumWeightStyle:Landroid/text/style/CharacterStyle;

    if-nez v3, :cond_5

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-static {v0, v3}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_1

    .line 258
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->getBalanceSplitAmount()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    new-array v6, v6, [Landroid/text/style/CharacterStyle;

    iget-object v8, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->redCharacterStyle:Landroid/text/style/CharacterStyle;

    if-nez v8, :cond_7

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    aput-object v8, v6, v5

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->mediumWeightStyle:Landroid/text/style/CharacterStyle;

    if-nez v3, :cond_8

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    aput-object v3, v6, v4

    invoke-static {v0, v6}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;[Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    .line 260
    :goto_1
    sget v3, Lcom/squareup/features/invoices/R$string;->payment_schedule_balance_section_money:I

    invoke-static {p2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 261
    check-cast v0, Ljava/lang/CharSequence;

    const-string v3, "money"

    invoke-virtual {p2, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 262
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->getBalanceAmount()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 263
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->getNumber()I

    move-result v0

    invoke-virtual {p2, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 264
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 267
    :goto_2
    invoke-static {p2}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object p2

    .line 268
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;->getBalanceSplitString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x29

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object p1

    const-string p2, "SpannableStringBuilder.v\u2026\" ($balanceSplitString)\")"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method private final toCharSequence(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 3

    .line 238
    sget v0, Lcom/squareup/features/invoices/R$string;->payment_schedule_deposit_section:I

    invoke-static {p2, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 239
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;->getDepositAmount()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->mediumWeightStyle:Landroid/text/style/CharacterStyle;

    if-nez v1, :cond_0

    const-string v2, "mediumWeightStyle"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, v1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "deposit_amount"

    invoke-virtual {p2, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 240
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$DepositHelperTextData;->getInvoiceAmount()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v0, "invoice_amount_money"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 241
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "Phrase.from(resources, R\u2026Amount)\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final update(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;Landroid/view/View;)V
    .locals 5

    .line 99
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v1, "view.resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;->getCancelClicked()Lkotlin/jvm/functions/Function0;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;->getSaveClicked()Lkotlin/jvm/functions/Function0;

    move-result-object v3

    invoke-direct {p0, v0, v2, v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->updateActionBar(Landroid/content/res/Resources;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    .line 100
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$update$1;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$update$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->toggleRequestDepositRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    const-string/jumbo v2, "toggleRequestDepositRow"

    if-nez v0, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;->getRequestInitialDeposit()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 103
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->toggleSplitBalanceRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    const-string/jumbo v3, "toggleSplitBalanceRow"

    if-nez v0, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;->getSplitBalance()Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 105
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->toggleRequestDepositRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$update$2;

    invoke-direct {v2, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$update$2;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V

    check-cast v2, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->toggleSplitBalanceRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$update$3;

    invoke-direct {v2, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$update$3;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V

    check-cast v2, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->nullStateMessageView:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_4

    const-string v2, "nullStateMessageView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;->getShowNullState()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 115
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->displayDepositSection(Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V

    .line 116
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->displayBalanceSection(Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V

    return-void
.end method

.method private final updateActionBar(Landroid/content/res/Resources;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 234
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 229
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 230
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v3, Lcom/squareup/features/invoices/R$string;->payment_schedule:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    if-eqz p2, :cond_1

    .line 231
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v2, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    move-object p2, v2

    :cond_1
    check-cast p2, Ljava/lang/Runnable;

    invoke-virtual {v1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 232
    sget v1, Lcom/squareup/common/strings/R$string;->save:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    if-eqz p3, :cond_2

    .line 233
    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {p2, p3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_2
    move-object p2, p3

    :goto_0
    check-cast p2, Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 234
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 83
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->bindViews(Landroid/view/View;)V

    .line 85
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 308
    new-instance v2, Lcom/squareup/fonts/FontSpan;

    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result v1

    invoke-direct {v2, v0, v1}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast v2, Landroid/text/style/CharacterStyle;

    iput-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->mediumWeightStyle:Landroid/text/style/CharacterStyle;

    .line 86
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    .line 87
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/features/invoices/R$color;->payment_request_amount_invalid:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 86
    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    check-cast v0, Landroid/text/style/CharacterStyle;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->redCharacterStyle:Landroid/text/style/CharacterStyle;

    .line 90
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->screens:Lio/reactivex/Observable;

    .line 91
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens\n        .subscri\u2026-> update(screen, view) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
