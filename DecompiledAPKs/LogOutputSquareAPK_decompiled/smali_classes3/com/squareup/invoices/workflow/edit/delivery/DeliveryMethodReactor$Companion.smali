.class public final Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$Companion;
.super Ljava/lang/Object;
.source "DeliveryMethodReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDeliveryMethodReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DeliveryMethodReactor.kt\ncom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$Companion\n+ 2 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPool$Companion\n+ 3 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPoolKt\n*L\n1#1,138:1\n334#2,2:139\n412#3:141\n*E\n*S KotlinDebug\n*F\n+ 1 DeliveryMethodReactor.kt\ncom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$Companion\n*L\n135#1,2:139\n135#1:141\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00042\u0006\u0010\u0008\u001a\u00020\u0005\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$Companion;",
        "",
        "()V",
        "handle",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
        "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
        "state",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 132
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final handle(Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
            "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
            ">;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowPool;->Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    const-class v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;

    .line 141
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-class v2, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    const-string v1, ""

    .line 140
    invoke-virtual {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object v1
.end method
