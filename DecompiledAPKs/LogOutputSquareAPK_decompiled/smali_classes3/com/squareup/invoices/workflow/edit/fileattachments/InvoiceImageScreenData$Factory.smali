.class public final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData$Factory;
.super Ljava/lang/Object;
.source "InvoiceImageScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData$Factory;",
        "",
        "()V",
        "fromState",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;",
        "state",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData$Factory;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromState(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;
    .locals 7

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;

    .line 41
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt;->access$toConfig(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    move-result-object v2

    .line 42
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt;->access$title(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Ljava/lang/String;

    move-result-object v3

    .line 43
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt;->access$extension(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Ljava/lang/String;

    move-result-object v4

    .line 44
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt;->access$previewImage(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    move-result-object v5

    .line 45
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenDataKt;->access$showProgress(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Z

    move-result v6

    move-object v1, v0

    .line 40
    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;Z)V

    return-object v0
.end method
