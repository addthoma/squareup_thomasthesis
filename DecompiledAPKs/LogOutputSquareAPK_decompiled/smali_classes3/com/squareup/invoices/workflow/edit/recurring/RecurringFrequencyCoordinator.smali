.class public final Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "RecurringFrequencyCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0001,B5\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0010\u0010!\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0010\u0010\"\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020$H\u0002J\u001e\u0010%\u001a\u00020&2\u0006\u0010\n\u001a\u00020\'2\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u00060)H\u0002J&\u0010*\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010+\u001a\u00020\u00052\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u00060)H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyScreen;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Ljava/text/DateFormat;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "frequencyHelper",
        "Lcom/squareup/widgets/MessageView;",
        "frequencyOptions",
        "Lcom/squareup/widgets/CheckableGroup;",
        "oneTimeOption",
        "Lcom/squareup/marketfont/MarketCheckedTextView;",
        "recurrenceEnd",
        "Lcom/squareup/ui/account/view/LineRow;",
        "recurrenceOptions",
        "Landroid/view/ViewGroup;",
        "recurringOption",
        "repeatEvery",
        "startDate",
        "Ljava/util/Date;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "displayFrequencyOptions",
        "visible",
        "",
        "getActionBarConfig",
        "Lcom/squareup/marin/widgets/MarinActionBar$Config;",
        "Landroid/content/res/Resources;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "update",
        "recurrenceInfo",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final dateFormat:Ljava/text/DateFormat;

.field private frequencyHelper:Lcom/squareup/widgets/MessageView;

.field private frequencyOptions:Lcom/squareup/widgets/CheckableGroup;

.field private oneTimeOption:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private recurrenceEnd:Lcom/squareup/ui/account/view/LineRow;

.field private recurrenceOptions:Landroid/view/ViewGroup;

.field private recurringOption:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private repeatEvery:Lcom/squareup/ui/account/view/LineRow;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private startDate:Ljava/util/Date;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Ljava/text/DateFormat;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;>;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->dateFormat:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Ljava/text/DateFormat;Lcom/squareup/util/Res;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;-><init>(Lio/reactivex/Observable;Ljava/text/DateFormat;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public static final synthetic access$getRecurringOption$p(Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;)Lcom/squareup/marketfont/MarketCheckedTextView;
    .locals 1

    .line 34
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->recurringOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p0, :cond_0

    const-string v0, "recurringOption"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setRecurringOption$p(Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;Lcom/squareup/marketfont/MarketCheckedTextView;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->recurringOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;Landroid/view/View;Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 131
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 133
    sget v0, Lcom/squareup/features/invoices/R$id;->frequency_options:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CheckableGroup;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->frequencyOptions:Lcom/squareup/widgets/CheckableGroup;

    .line 134
    sget v0, Lcom/squareup/features/invoices/R$id;->one_time:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->oneTimeOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 135
    sget v0, Lcom/squareup/features/invoices/R$id;->recurring:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->recurringOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 136
    sget v0, Lcom/squareup/features/invoices/R$id;->recurrence_options:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->recurrenceOptions:Landroid/view/ViewGroup;

    .line 137
    sget v0, Lcom/squareup/features/invoices/R$id;->repeat_every:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->repeatEvery:Lcom/squareup/ui/account/view/LineRow;

    .line 138
    sget v0, Lcom/squareup/features/invoices/R$id;->recurrence_end:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->recurrenceEnd:Lcom/squareup/ui/account/view/LineRow;

    .line 139
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_frequency_helper:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->frequencyHelper:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final displayFrequencyOptions(Z)V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->frequencyOptions:Lcom/squareup/widgets/CheckableGroup;

    if-nez v0, :cond_0

    const-string v1, "frequencyOptions"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    if-eqz p1, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->setVisibility(I)V

    return-void
.end method

.method private final getActionBarConfig(Landroid/content/res/Resources;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;)",
            "Lcom/squareup/marin/widgets/MarinActionBar$Config;"
        }
    .end annotation

    .line 123
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 124
    sget v1, Lcom/squareup/features/invoices/R$string;->recurring_frequency:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 125
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 126
    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$getActionBarConfig$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$getActionBarConfig$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 127
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    const-string p2, "Builder()\n        .setUp\u2026essed) }\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;)V"
        }
    .end annotation

    .line 78
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->isSeriesActive()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->displayFrequencyOptions(Z)V

    .line 80
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getStartDate()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->startDate:Ljava/util/Date;

    .line 82
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string/jumbo v3, "view.resources"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2, p3}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->getActionBarConfig(Landroid/content/res/Resources;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 84
    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$update$1;

    invoke-direct {v0, p3}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$update$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 86
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->frequencyOptions:Lcom/squareup/widgets/CheckableGroup;

    const-string v0, "frequencyOptions"

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v2, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$update$2;

    invoke-direct {v2, p0, p3}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$update$2;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

    invoke-virtual {p1, v2}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 96
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->repeatEvery:Lcom/squareup/ui/account/view/LineRow;

    const-string v2, "repeatEvery"

    if-nez p1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v3, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$update$3;

    invoke-direct {v3, p3}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$update$3;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v3}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->recurrenceEnd:Lcom/squareup/ui/account/view/LineRow;

    const-string v3, "recurrenceEnd"

    if-nez p1, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v4, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$update$4;

    invoke-direct {v4, p3}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$update$4;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v4}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p1

    const-string p2, "recurrenceOptions"

    const-string p3, "frequencyHelper"

    if-nez p1, :cond_8

    .line 102
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->frequencyOptions:Lcom/squareup/widgets/CheckableGroup;

    if-nez p1, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->oneTimeOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez v0, :cond_5

    const-string v1, "oneTimeOption"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    .line 103
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->recurrenceOptions:Landroid/view/ViewGroup;

    if-nez p1, :cond_6

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 104
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->frequencyHelper:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_7

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    goto :goto_0

    .line 106
    :cond_8
    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->frequencyOptions:Lcom/squareup/widgets/CheckableGroup;

    if-nez v4, :cond_9

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->recurringOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez v0, :cond_a

    const-string v5, "recurringOption"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    .line 107
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->recurrenceOptions:Landroid/view/ViewGroup;

    if-nez v0, :cond_b

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    const/4 p2, 0x0

    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->repeatEvery:Lcom/squareup/ui/account/view/LineRow;

    if-nez v0, :cond_c

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getFrequency()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object v2

    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {v2, v4, v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->prettyString(Lcom/squareup/util/Res;Z)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->recurrenceEnd:Lcom/squareup/ui/account/view/LineRow;

    if-nez v0, :cond_d

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getRecurrenceEnd()Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;->prettyString(Lcom/squareup/util/Res;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->frequencyHelper:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_e

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->startDate:Ljava/util/Date;

    if-nez v3, :cond_f

    const-string v4, "startDate"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    invoke-static {p1, v2, v3, v1}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->getRecurringPeriodShortText(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/util/Res;Ljava/util/Date;Z)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->frequencyHelper:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_10

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->bindViews(Landroid/view/View;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
