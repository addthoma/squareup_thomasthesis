.class public abstract Lcom/squareup/invoices/workflow/edit/EditInvoiceState;
.super Ljava/lang/Object;
.source "EditInvoiceState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Initializing;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Recurring;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceAttachment;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u000c2\u00020\u0001:\u000f\u0006\u0007\u0008\t\n\u000b\u000c\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0003\u001a\u00020\u0004H\u0000\u00a2\u0006\u0002\u0008\u0005\u0082\u0001\u000e\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f !\"\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "",
        "()V",
        "takeSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "takeSnapshot$invoices_hairball_release",
        "AdditionalRecipients",
        "AutoReminders",
        "AutomaticPayments",
        "ChoosingDueDate",
        "ChoosingExpiryDate",
        "ChoosingSendDate",
        "Companion",
        "Delivery",
        "Initializing",
        "InvoiceAttachment",
        "InvoiceDetails",
        "InvoiceMessage",
        "PaymentRequest",
        "PaymentSchedule",
        "Recurring",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Initializing;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Recurring;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceAttachment;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;->Companion:Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;-><init>()V

    return-void
.end method


# virtual methods
.method public final takeSnapshot$invoices_hairball_release()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 211
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$takeSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$takeSnapshot$1;-><init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
