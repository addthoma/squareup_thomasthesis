.class final Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$3;
.super Lkotlin/jvm/internal/Lambda;
.source "EditInvoiceReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDateChooser;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDateChooser;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$3;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$3;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$3;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$3;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDateChooser;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDateChooser;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDateChooser;->getType()Lcom/squareup/invoices/workflow/edit/ChooseDateType;

    move-result-object v0

    .line 126
    sget-object v1, Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Scheduled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Scheduled;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/invoices/workflow/edit/ChooseDateType$EstimateDateType$Scheduled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/ChooseDateType$EstimateDateType$Scheduled;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 127
    :goto_0
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDateChooser;->getChooseDateInfo()Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;-><init>(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    goto :goto_1

    .line 128
    :cond_1
    sget-object v1, Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Due;->INSTANCE:Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Due;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDateChooser;->getChooseDateInfo()Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;-><init>(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    goto :goto_1

    .line 129
    :cond_2
    sget-object v1, Lcom/squareup/invoices/workflow/edit/ChooseDateType$EstimateDateType$Expiry;->INSTANCE:Lcom/squareup/invoices/workflow/edit/ChooseDateType$EstimateDateType$Expiry;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDateChooser;->getChooseDateInfo()Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;-><init>(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    .line 131
    :goto_1
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p1

    .line 129
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDateChooser;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$3;->invoke(Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDateChooser;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
