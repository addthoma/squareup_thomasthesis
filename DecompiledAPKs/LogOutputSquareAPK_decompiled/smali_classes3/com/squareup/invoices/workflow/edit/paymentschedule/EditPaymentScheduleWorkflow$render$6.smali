.class final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$6;
.super Lkotlin/jvm/internal/Lambda;
.source "EditPaymentScheduleWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->render(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$6;->this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$6;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$6;->$props:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 73
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$6;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 427
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$6;->this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;

    invoke-static {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->access$getAnalytics$p(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/analytics/AddAnotherPaymentClicked;->INSTANCE:Lcom/squareup/invoices/analytics/AddAnotherPaymentClicked;

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 428
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$6;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AddInstallment;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$6;->$props:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;->getDefaultConfigs()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AddInstallment;-><init>(Ljava/util/List;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
