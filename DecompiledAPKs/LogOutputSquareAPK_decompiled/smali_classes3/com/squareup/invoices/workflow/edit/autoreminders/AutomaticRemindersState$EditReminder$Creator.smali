.class public final Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder$Creator;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Creator"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 9

    const-string v0, "in"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    const-class v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v4, 0x0

    :goto_0
    const-class v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    const-class v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;IZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;I)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 0

    new-array p1, p1, [Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    return-object p1
.end method
