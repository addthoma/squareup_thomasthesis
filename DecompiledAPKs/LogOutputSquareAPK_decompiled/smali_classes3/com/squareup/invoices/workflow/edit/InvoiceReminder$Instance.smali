.class public final Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;
.super Lcom/squareup/invoices/workflow/edit/InvoiceReminder;
.source "InvoiceReminder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/InvoiceReminder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Instance"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B?\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\n\u0012\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u000cJ\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0008H\u00c6\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\nH\u00c6\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003JO\u0010\u001d\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\t\u0010\u001e\u001a\u00020\u0005H\u00d6\u0001J\u0013\u0010\u001f\u001a\u00020 2\u0008\u0010!\u001a\u0004\u0018\u00010\"H\u00d6\u0003J\t\u0010#\u001a\u00020\u0005H\u00d6\u0001J\u000e\u0010$\u001a\u00020\u00082\u0006\u0010%\u001a\u00020\u0008J\t\u0010&\u001a\u00020\u0003H\u00d6\u0001J\u0019\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020\u0005H\u00d6\u0001R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000eR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0013\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u000e\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceReminder;",
        "configToken",
        "",
        "relativeDays",
        "",
        "customMessage",
        "sentOn",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "serverState",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;",
        "token",
        "(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;Ljava/lang/String;)V",
        "getConfigToken",
        "()Ljava/lang/String;",
        "getCustomMessage",
        "getRelativeDays",
        "()I",
        "getSentOn",
        "()Lcom/squareup/protos/common/time/YearMonthDay;",
        "getServerState",
        "()Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;",
        "getToken",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "describeContents",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "sendDate",
        "paymentRequestDueDate",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final configToken:Ljava/lang/String;

.field private final customMessage:Ljava/lang/String;

.field private final relativeDays:I

.field private final sentOn:Lcom/squareup/protos/common/time/YearMonthDay;

.field private final serverState:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

.field private final token:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance$Creator;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance$Creator;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->configToken:Ljava/lang/String;

    iput p2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->relativeDays:I

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->customMessage:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->sentOn:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->serverState:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->token:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;Ljava/lang/String;ILjava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getConfigToken()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getRelativeDays()I

    move-result p2

    :cond_1
    move p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getCustomMessage()Ljava/lang/String;

    move-result-object p3

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->sentOn:Lcom/squareup/protos/common/time/YearMonthDay;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->serverState:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->token:Ljava/lang/String;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->copy(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getConfigToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getRelativeDays()I

    move-result v0

    return v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getCustomMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component4()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->sentOn:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->serverState:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    return-object v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->token:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;
    .locals 8

    new-instance v7, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    move-object v0, v7

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;Ljava/lang/String;)V

    return-object v7
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getConfigToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getConfigToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getRelativeDays()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getRelativeDays()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getCustomMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getCustomMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->sentOn:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->sentOn:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->serverState:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->serverState:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->token:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getConfigToken()Ljava/lang/String;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->configToken:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomMessage()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->customMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getRelativeDays()I
    .locals 1

    .line 32
    iget v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->relativeDays:I

    return v0
.end method

.method public final getSentOn()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->sentOn:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final getServerState()Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->serverState:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    return-object v0
.end method

.method public final getToken()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->token:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getConfigToken()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getRelativeDays()I

    move-result v2

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getCustomMessage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->sentOn:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->serverState:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->token:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public final sendDate(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    const-string v0, "paymentRequestDueDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getRelativeDays()I

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/util/ProtoDates;->addDays(Lcom/squareup/protos/common/time/YearMonthDay;I)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    const-string v0, "ProtoDates.addDays(payme\u2026estDueDate, relativeDays)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Instance(configToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getConfigToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", relativeDays="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getRelativeDays()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", customMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getCustomMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", sentOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->sentOn:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", serverState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->serverState:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->configToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget p2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->relativeDays:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->customMessage:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->sentOn:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->serverState:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    :goto_0
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->token:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
