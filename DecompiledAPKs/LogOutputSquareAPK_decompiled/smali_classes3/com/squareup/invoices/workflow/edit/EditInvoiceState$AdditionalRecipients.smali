.class public final Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;
.super Lcom/squareup/invoices/workflow/edit/EditInvoiceState;
.source "EditInvoiceState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/EditInvoiceState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AdditionalRecipients"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0008\u0016\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005B{\u0012t\u0010\u0006\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\t\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\n0\u0008\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u000e0\u0007j6\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u000e\u0012&\u0012$\u0012\u0004\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\nj\u0008\u0012\u0004\u0012\u00020\u000b`\u0010`\u000f\u00a2\u0006\u0002\u0010\u0011Jw\u0010\u0014\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\t\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\n0\u0008\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u000e0\u0007j6\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u000e\u0012&\u0012$\u0012\u0004\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\nj\u0008\u0012\u0004\u0012\u00020\u000b`\u0010`\u000fH\u00c6\u0003J\u0081\u0001\u0010\u0015\u001a\u00020\u00002v\u0008\u0002\u0010\u0006\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\t\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\n0\u0008\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u000e0\u0007j6\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u000e\u0012&\u0012$\u0012\u0004\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\nj\u0008\u0012\u0004\u0012\u00020\u000b`\u0010`\u000fH\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0004H\u00d6\u0001R\u007f\u0010\u0006\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\t\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\n0\u0008\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u000e0\u0007j6\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u000e\u0012&\u0012$\u0012\u0004\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\nj\u0008\u0012\u0004\u0012\u00020\u000b`\u0010`\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "recipients",
        "",
        "",
        "(Ljava/util/List;)V",
        "additionalRecipientsWorkflow",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
        "Lcom/squareup/workflow/legacyintegration/LegacyHandle;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V",
        "getAdditionalRecipientsWorkflow",
        "()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final additionalRecipientsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;-",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "+",
            "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "additionalRecipientsWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 168
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;->additionalRecipientsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "recipients"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    sget-object v0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;->Companion:Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$Companion;

    .line 171
    new-instance v1, Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;

    invoke-direct {v1, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;-><init>(Ljava/util/List;)V

    .line 170
    invoke-virtual {v0, v1}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$Companion;->legacyHandleFromInput(Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 169
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;->additionalRecipientsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;->copy(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;->additionalRecipientsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-object v0
.end method

.method public final copy(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;-",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "+",
            "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;"
        }
    .end annotation

    const-string v0, "additionalRecipientsWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;->additionalRecipientsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;->additionalRecipientsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAdditionalRecipientsWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
            ">;"
        }
    .end annotation

    .line 166
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;->additionalRecipientsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;->additionalRecipientsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AdditionalRecipients(additionalRecipientsWorkflow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;->additionalRecipientsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
