.class public final Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;
.super Ljava/lang/Object;
.source "PaymentRequestReadOnlyInfo.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0018\u00002\u00020\u0001:\u0001\u0013B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nR\u0011\u0010\u000b\u001a\u00020\u00078F\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\r\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;",
        "",
        "paymentRequest",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "paymentRequestState",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;",
        "requestedAmount",
        "Lcom/squareup/protos/common/Money;",
        "dueDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)V",
        "completedAmount",
        "getCompletedAmount",
        "()Lcom/squareup/protos/common/Money;",
        "getDueDate",
        "()Lcom/squareup/protos/common/time/YearMonthDay;",
        "getPaymentRequest",
        "()Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "getRequestedAmount",
        "Factory",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dueDate:Lcom/squareup/protos/common/time/YearMonthDay;

.field private final paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

.field private final paymentRequestState:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;

.field private final requestedAmount:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)V
    .locals 1

    const-string v0, "paymentRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentRequestState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requestedAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dueDate"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;->paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

    iput-object p2, p0, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;->paymentRequestState:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;

    iput-object p3, p0, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;->requestedAmount:Lcom/squareup/protos/common/Money;

    iput-object p4, p0, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;->dueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-void
.end method


# virtual methods
.method public final getCompletedAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;->paymentRequestState:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->completed_amount:Lcom/squareup/protos/common/Money;

    const-string v1, "paymentRequestState.completed_amount"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getDueDate()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;->dueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;->paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

    return-object v0
.end method

.method public final getRequestedAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;->requestedAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method
