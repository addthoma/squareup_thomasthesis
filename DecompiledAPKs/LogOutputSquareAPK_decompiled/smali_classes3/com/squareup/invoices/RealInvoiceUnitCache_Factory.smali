.class public final Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;
.super Ljava/lang/Object;
.source "RealInvoiceUnitCache_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/RealInvoiceUnitCache;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final failureMessageFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceDefaultListPreferenceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final invoiceMetricsPreferenceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final invoiceServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceUnitMetadataPreferenceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final pushMessagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final receiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->invoiceUnitMetadataPreferenceProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p2, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->invoiceMetricsPreferenceProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p3, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->invoiceDefaultListPreferenceProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p4, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->invoiceServiceProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p5, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p6, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->resProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p7, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p8, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->receiverProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p9, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->failureMessageFactoryProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p10, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->pushMessagesProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p11, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)",
            "Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;"
        }
    .end annotation

    .line 88
    new-instance v12, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/pushmessages/PushMessageDelegate;Lio/reactivex/Scheduler;)Lcom/squareup/invoices/RealInvoiceUnitCache;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;>;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;>;",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/receiving/StandardReceiver;",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            "Lio/reactivex/Scheduler;",
            ")",
            "Lcom/squareup/invoices/RealInvoiceUnitCache;"
        }
    .end annotation

    .line 98
    new-instance v12, Lcom/squareup/invoices/RealInvoiceUnitCache;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/invoices/RealInvoiceUnitCache;-><init>(Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/pushmessages/PushMessageDelegate;Lio/reactivex/Scheduler;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/RealInvoiceUnitCache;
    .locals 12

    .line 75
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->invoiceUnitMetadataPreferenceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->invoiceMetricsPreferenceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->invoiceDefaultListPreferenceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->invoiceServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->receiverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/receiving/StandardReceiver;

    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->failureMessageFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/receiving/FailureMessageFactory;

    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->pushMessagesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/pushmessages/PushMessageDelegate;

    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lio/reactivex/Scheduler;

    invoke-static/range {v1 .. v11}, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->newInstance(Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/pushmessages/PushMessageDelegate;Lio/reactivex/Scheduler;)Lcom/squareup/invoices/RealInvoiceUnitCache;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/invoices/RealInvoiceUnitCache_Factory;->get()Lcom/squareup/invoices/RealInvoiceUnitCache;

    move-result-object v0

    return-object v0
.end method
