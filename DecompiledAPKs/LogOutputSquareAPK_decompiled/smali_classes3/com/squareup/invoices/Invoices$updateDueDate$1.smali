.class final Lcom/squareup/invoices/Invoices$updateDueDate$1;
.super Lkotlin/jvm/internal/Lambda;
.source "Invoices.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/Invoices;->updateDueDate(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $firstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

.field final synthetic $newDueDate:Lcom/squareup/protos/common/time/YearMonthDay;


# direct methods
.method constructor <init>(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/Invoices$updateDueDate$1;->$firstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object p2, p0, Lcom/squareup/invoices/Invoices$updateDueDate$1;->$newDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/Invoices$updateDueDate$1;->invoke(Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;)V
    .locals 2

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/invoices/Invoices$updateDueDate$1;->$firstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v1, p0, Lcom/squareup/invoices/Invoices$updateDueDate$1;->$newDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v0, v1}, Lcom/squareup/util/ProtoDates;->countDaysBetweenAsUTC(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on(Ljava/lang/Long;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    return-void
.end method
