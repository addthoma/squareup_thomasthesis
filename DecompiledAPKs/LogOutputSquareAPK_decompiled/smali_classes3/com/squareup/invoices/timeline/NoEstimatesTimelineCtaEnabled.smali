.class public final Lcom/squareup/invoices/timeline/NoEstimatesTimelineCtaEnabled;
.super Ljava/lang/Object;
.source "NoEstimatesTimelineCtaEnabled.kt"

# interfaces
.implements Lcom/squareup/TimelineCtaEnabled;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/timeline/NoEstimatesTimelineCtaEnabled;",
        "Lcom/squareup/TimelineCtaEnabled;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/Features;)V",
        "isEnabled",
        "",
        "callToAction",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/timeline/NoEstimatesTimelineCtaEnabled;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public isEnabled(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;)Z
    .locals 3

    const-string v0, "callToAction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    goto :goto_0

    .line 18
    :cond_0
    sget-object v2, Lcom/squareup/invoices/timeline/NoEstimatesTimelineCtaEnabled$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->ordinal()I

    move-result v0

    aget v0, v2, v0

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 20
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/timeline/NoEstimatesTimelineCtaEnabled;->features:Lcom/squareup/settings/server/Features;

    .line 21
    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->INVOICES_TIMELINE_CTA_LINKING:Lcom/squareup/settings/server/Features$Feature;

    .line 20
    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_type:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;->ESTIMATE:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    if-eq p1, v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method
