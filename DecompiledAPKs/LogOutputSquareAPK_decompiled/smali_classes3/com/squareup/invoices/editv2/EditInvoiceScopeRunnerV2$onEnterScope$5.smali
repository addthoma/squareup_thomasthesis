.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$5;
.super Lkotlin/jvm/internal/Lambda;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$5;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$5;->invoke(Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 324
    sget-object v0, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event$CloseKeypad;->INSTANCE:Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event$CloseKeypad;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$5;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getFlow$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lflow/Flow;

    move-result-object p1

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    goto :goto_0

    .line 325
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event$ItemPriceEntered;

    if-eqz v0, :cond_1

    .line 326
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$5;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getWorkingOrderEditor$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/order/WorkingOrderEditor;

    move-result-object v0

    check-cast p1, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event$ItemPriceEntered;

    invoke-virtual {p1}, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event$ItemPriceEntered;->getItem()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/order/WorkingOrderEditor;->addItemToCart(Lcom/squareup/checkout/CartItem;)V

    .line 327
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$5;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    goto :goto_0

    .line 329
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event$DiscountAmountEntered;

    if-eqz v0, :cond_2

    .line 330
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$5;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getWorkingOrderEditor$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/order/WorkingOrderEditor;

    move-result-object v0

    check-cast p1, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event$DiscountAmountEntered;

    invoke-virtual {p1}, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner$Event$DiscountAmountEntered;->getDiscount()Lcom/squareup/checkout/Discount;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/order/WorkingOrderEditor;->addDiscountToCart(Lcom/squareup/checkout/Discount;)V

    .line 331
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$5;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    :cond_2
    :goto_0
    return-void
.end method
