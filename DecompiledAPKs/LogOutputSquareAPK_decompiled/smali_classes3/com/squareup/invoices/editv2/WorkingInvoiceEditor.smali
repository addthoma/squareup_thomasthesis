.class public final Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;
.super Ljava/lang/Object;
.source "WorkingInvoiceEditor.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkingInvoiceEditor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkingInvoiceEditor.kt\ncom/squareup/invoices/editv2/WorkingInvoiceEditor\n+ 2 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,357:1\n132#2,3:358\n*E\n*S KotlinDebug\n*F\n+ 1 WorkingInvoiceEditor.kt\ncom/squareup/invoices/editv2/WorkingInvoiceEditor\n*L\n345#1,3:358\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00e0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0012\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 g2\u00020\u0001:\u0001gBO\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u000e\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cJ\u000e\u0010\u001d\u001a\u00020\u001a2\u0006\u0010\u001e\u001a\u00020\u001fJ\u000e\u0010 \u001a\u00020\u001a2\u0006\u0010!\u001a\u00020\"J\u000e\u0010#\u001a\u00020\u001a2\u0006\u0010$\u001a\u00020%J\u0010\u0010&\u001a\u00020\u001a2\u0006\u0010\'\u001a\u00020(H\u0016J\u0008\u0010)\u001a\u00020\u001aH\u0016J\u000e\u0010*\u001a\u00020\u001a2\u0006\u0010+\u001a\u00020,J\u000e\u0010-\u001a\u00020\u001a2\u0006\u0010.\u001a\u00020/J\u0008\u00100\u001a\u00020\u001aH\u0002J\u0016\u00101\u001a\u00020\u001a2\u0006\u00102\u001a\u00020\u00172\u0006\u0010!\u001a\u00020\"J\u0014\u00103\u001a\u00020\u001a2\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001f05J\u000e\u00106\u001a\u00020\u001a2\u0006\u00107\u001a\u000208J\u0014\u00109\u001a\u00020\u001a2\u000c\u0010:\u001a\u0008\u0012\u0004\u0012\u00020;05J\u0014\u0010<\u001a\u00020\u001a2\u000c\u0010=\u001a\u0008\u0012\u0004\u0012\u00020>05J\u000e\u0010?\u001a\u00020\u001a2\u0006\u0010@\u001a\u000208J\u0016\u0010A\u001a\u00020\u001a2\u0006\u0010B\u001a\u00020C2\u0006\u0010D\u001a\u00020CJ\u0016\u0010E\u001a\u00020\u001a2\u0006\u0010+\u001a\u00020,2\u0006\u0010F\u001a\u00020,J\u0010\u0010G\u001a\u00020\u001a2\u0008\u0010H\u001a\u0004\u0018\u00010,J\u000e\u0010I\u001a\u00020\u001a2\u0006\u0010J\u001a\u00020,J\u000e\u0010K\u001a\u00020\u001a2\u0006\u0010L\u001a\u00020,J\u000e\u0010M\u001a\u00020\u001a2\u0006\u0010N\u001a\u00020,J\u000e\u0010O\u001a\u00020\u001a2\u0006\u0010P\u001a\u00020QJ\u0016\u0010R\u001a\u00020\u001a2\u0006\u0010.\u001a\u00020/2\u0006\u0010\u001e\u001a\u00020\u001fJ\u0014\u0010S\u001a\u00020\u001a2\u000c\u0010T\u001a\u0008\u0012\u0004\u0012\u00020,05J\u000e\u0010U\u001a\u00020\u001a2\u0006\u0010V\u001a\u000208J\u000e\u0010W\u001a\u00020\u001a2\u0006\u0010X\u001a\u000208J\u0010\u0010Y\u001a\u00020\u001a2\u0008\u0010Z\u001a\u0004\u0018\u00010CJ\u000c\u0010[\u001a\u0008\u0012\u0004\u0012\u00020\u00170\\J\u0016\u0010]\u001a\u00020\u001a2\u0006\u0010^\u001a\u00020_2\u0006\u0010!\u001a\u00020\"J\u0006\u0010`\u001a\u00020_J\u000c\u0010a\u001a\u00020b*\u00020\"H\u0002J+\u0010c\u001a\u00020\u001a*\u0008\u0012\u0004\u0012\u00020\u00170\u00162\u0017\u0010d\u001a\u0013\u0012\u0004\u0012\u00020b\u0012\u0004\u0012\u00020\u001a0e\u00a2\u0006\u0002\u0008fH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0015\u001a\u0010\u0012\u000c\u0012\n \u0018*\u0004\u0018\u00010\u00170\u00170\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006h"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;",
        "Lmortar/Scoped;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "invoiceUnitCache",
        "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "ruleEditor",
        "Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;",
        "orderEditor",
        "Lcom/squareup/invoices/order/WorkingOrderEditor;",
        "orderEditorFactory",
        "Lcom/squareup/invoices/editv2/EditInvoiceOrderFactory;",
        "instrumentsStore",
        "Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;",
        "dateSanitizer",
        "Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;",
        "(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/time/CurrentTime;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/settings/server/Features;Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;Lcom/squareup/invoices/order/WorkingOrderEditor;Lcom/squareup/invoices/editv2/EditInvoiceOrderFactory;Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;)V",
        "invoiceRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "kotlin.jvm.PlatformType",
        "addFileAttachment",
        "",
        "attachmentMetadata",
        "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
        "addPaymentRequest",
        "paymentRequest",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "init",
        "editInvoiceContext",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "loadFromTransaction",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "removeFileAttachment",
        "attachmentToken",
        "",
        "removePaymentRequest",
        "index",
        "",
        "resetInstruments",
        "set",
        "invoice",
        "setPaymentRequests",
        "paymentRequests",
        "",
        "updateAutomaticPayments",
        "allowAutoPayments",
        "",
        "updateAutomaticRemindersConfig",
        "reminders",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "updateAutomaticRemindersInstances",
        "instances",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
        "updateBuyerCofEnabled",
        "buyerCofEnabled",
        "updateDueDate",
        "dueDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "firstSentDate",
        "updateFileAttachment",
        "newTitle",
        "updateInstrumentToken",
        "token",
        "updateInvoiceId",
        "id",
        "updateInvoiceMessage",
        "message",
        "updateInvoiceTitle",
        "title",
        "updateMethod",
        "paymentMethod",
        "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
        "updatePaymentRequest",
        "updateRecipients",
        "recipients",
        "updateRequestShippingAddress",
        "requestShippingAddress",
        "updateRequestTipping",
        "requestTipping",
        "updateScheduledAt",
        "scheduledAt",
        "workingInvoice",
        "Lio/reactivex/Observable;",
        "workingInvoiceFromByteArray",
        "byteArray",
        "",
        "workingInvoiceToByteArray",
        "toTemplateBuilder",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        "update",
        "block",
        "Lkotlin/Function1;",
        "Lkotlin/ExtensionFunctionType;",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final DEFAULT_REMAINDER_DUE:J = 0x1eL


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final dateSanitizer:Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final instrumentsStore:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

.field private final invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

.field private final orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

.field private final orderEditorFactory:Lcom/squareup/invoices/editv2/EditInvoiceOrderFactory;

.field private final ruleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->Companion:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/time/CurrentTime;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/settings/server/Features;Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;Lcom/squareup/invoices/order/WorkingOrderEditor;Lcom/squareup/invoices/editv2/EditInvoiceOrderFactory;Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceUnitCache"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ruleEditor"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEditor"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEditorFactory"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instrumentsStore"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateSanitizer"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p3, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    iput-object p4, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->features:Lcom/squareup/settings/server/Features;

    iput-object p5, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->ruleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    iput-object p6, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    iput-object p7, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->orderEditorFactory:Lcom/squareup/invoices/editv2/EditInvoiceOrderFactory;

    iput-object p8, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->instrumentsStore:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    iput-object p9, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->dateSanitizer:Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;

    .line 59
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<Invoice>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$Companion()Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$Companion;
    .locals 1

    sget-object v0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->Companion:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$Companion;

    return-object v0
.end method

.method public static final synthetic access$getInvoiceRelay$p(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$resetInstruments(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->resetInstruments()V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final resetInstruments()V
    .locals 2

    .line 324
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->instrumentsStore:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->clear()V

    .line 325
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$resetInstruments$1;->INSTANCE:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$resetInstruments$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final toTemplateBuilder(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 4

    .line 335
    new-instance v0, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-direct {v0, v1, v2}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/time/CurrentTime;)V

    .line 336
    iget-object v1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v1}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->getCurrentDefaultMessage()Ljava/lang/String;

    move-result-object v1

    .line 337
    iget-object v2, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v2}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->getInvoiceDefaults()Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    move-result-object v2

    .line 338
    iget-object v3, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->features:Lcom/squareup/settings/server/Features;

    .line 334
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->constructInitialTemplateToEdit(Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceDefaults;Lcom/squareup/settings/server/Features;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    return-object p1
.end method

.method private final declared-synchronized update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    .line 345
    :try_start_0
    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/Invoice;

    if-eqz v0, :cond_1

    check-cast v0, Lcom/squareup/wire/Message;

    .line 359
    invoke-virtual {v0}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    invoke-virtual {v0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/invoice/Invoice;

    if-eqz p2, :cond_1

    .line 344
    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    monitor-exit p0

    return-void

    .line 359
    :cond_0
    :try_start_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type B"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 345
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Need to initialize the WorkingInvoiceEditor first "

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public final addFileAttachment(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V
    .locals 2

    const-string v0, "attachmentMetadata"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$addFileAttachment$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$addFileAttachment$1;-><init>(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final addPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;)V
    .locals 2

    const-string v0, "paymentRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$addPaymentRequest$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$addPaymentRequest$1;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final init(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V
    .locals 2

    const-string v0, "editInvoiceContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->toTemplateBuilder(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    const-string v1, "template"

    .line 116
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->set(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    return-void
.end method

.method public final loadFromTransaction(Lcom/squareup/payment/Transaction;)V
    .locals 4

    const-string/jumbo v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    sget-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;->INSTANCE:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    check-cast v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-direct {p0, v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->toTemplateBuilder(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 103
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/squareup/payment/Order;->getCartProtoForInvoice(Lcom/squareup/protos/common/Money;Z)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 105
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/invoices/Invoices;->setPayerFromContact(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/client/rolodex/Contact;)Z

    .line 107
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    const-string v0, "template"

    .line 109
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;->INSTANCE:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    check-cast v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->set(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    .line 63
    invoke-virtual {v0}, Lcom/squareup/invoices/order/WorkingOrderEditor;->workingOrder()Lio/reactivex/Observable;

    move-result-object v0

    .line 64
    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1;-><init>(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "orderEditor\n        .wor\u2026s()\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->ruleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->rule()Lio/reactivex/Observable;

    move-result-object v0

    .line 78
    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$2;-><init>(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "ruleEditor.rule()\n      \u2026  }\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public final removeFileAttachment(Ljava/lang/String;)V
    .locals 2

    const-string v0, "attachmentToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$removeFileAttachment$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$removeFileAttachment$1;-><init>(Ljava/lang/String;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final removePaymentRequest(I)V
    .locals 2

    .line 296
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$removePaymentRequest$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$removePaymentRequest$1;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final set(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V
    .locals 2

    const-string v0, "invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInvoiceContext"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->dateSanitizer:Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;->sanitize(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 126
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->orderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->orderEditorFactory:Lcom/squareup/invoices/editv2/EditInvoiceOrderFactory;

    invoke-virtual {p2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    invoke-virtual {v1, p1, p2}, Lcom/squareup/invoices/editv2/EditInvoiceOrderFactory;->createOrder(Lcom/squareup/protos/client/invoice/Invoice;Z)Lcom/squareup/payment/Order;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/order/WorkingOrderEditor;->init(Lcom/squareup/payment/Order;)V

    return-void
.end method

.method public final setPaymentRequests(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)V"
        }
    .end annotation

    const-string v0, "paymentRequests"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 278
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$setPaymentRequests$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$setPaymentRequests$1;-><init>(Ljava/util/List;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateAutomaticPayments(Z)V
    .locals 2

    .line 219
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateAutomaticPayments$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateAutomaticPayments$1;-><init>(Z)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateAutomaticRemindersConfig(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;)V"
        }
    .end annotation

    const-string v0, "reminders"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateAutomaticRemindersConfig$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateAutomaticRemindersConfig$1;-><init>(Ljava/util/List;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateAutomaticRemindersInstances(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
            ">;)V"
        }
    .end annotation

    const-string v0, "instances"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateAutomaticRemindersInstances$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateAutomaticRemindersInstances$1;-><init>(Ljava/util/List;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateBuyerCofEnabled(Z)V
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateBuyerCofEnabled$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateBuyerCofEnabled$1;-><init>(Z)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateDueDate(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V
    .locals 2

    const-string v0, "dueDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firstSentDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateDueDate$1;

    invoke-direct {v1, p2, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateDueDate$1;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateFileAttachment(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const-string v0, "attachmentToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newTitle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateFileAttachment$1;

    invoke-direct {v1, p1, p2}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateFileAttachment$1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateInstrumentToken(Ljava/lang/String;)V
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateInstrumentToken$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateInstrumentToken$1;-><init>(Ljava/lang/String;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateInvoiceId(Ljava/lang/String;)V
    .locals 2

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateInvoiceId$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateInvoiceId$1;-><init>(Ljava/lang/String;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateInvoiceMessage(Ljava/lang/String;)V
    .locals 2

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateInvoiceMessage$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateInvoiceMessage$1;-><init>(Ljava/lang/String;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateInvoiceTitle(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateInvoiceTitle$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateInvoiceTitle$1;-><init>(Ljava/lang/String;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateMethod(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)V
    .locals 2

    const-string v0, "paymentMethod"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateMethod$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateMethod$1;-><init>(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updatePaymentRequest(ILcom/squareup/protos/client/invoice/PaymentRequest;)V
    .locals 2

    const-string v0, "paymentRequest"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updatePaymentRequest$1;

    invoke-direct {v1, p2, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updatePaymentRequest$1;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;I)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateRecipients(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "recipients"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateRecipients$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateRecipients$1;-><init>(Ljava/util/List;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateRequestShippingAddress(Z)V
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateRequestShippingAddress$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateRequestShippingAddress$1;-><init>(Z)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateRequestTipping(Z)V
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateRequestTipping$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateRequestTipping$1;-><init>(Z)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final updateScheduledAt(Lcom/squareup/protos/common/time/YearMonthDay;)V
    .locals 2

    .line 180
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateScheduledAt$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$updateScheduledAt$1;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->update(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final workingInvoice()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ">;"
        }
    .end annotation

    .line 129
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "invoiceRelay.distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final workingInvoiceFromByteArray([BLcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V
    .locals 2

    const-string v0, "byteArray"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInvoiceContext"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 312
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v1, "Invoice.ADAPTER"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/util/ProtosPure;->decodeOrNull(Lcom/squareup/wire/ProtoAdapter;[B)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/Invoice;

    if-eqz p1, :cond_0

    .line 314
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->set(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    :cond_0
    return-void
.end method

.method public final workingInvoiceToByteArray()[B
    .locals 2

    .line 304
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->invoiceRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object v0

    return-object v0
.end method
