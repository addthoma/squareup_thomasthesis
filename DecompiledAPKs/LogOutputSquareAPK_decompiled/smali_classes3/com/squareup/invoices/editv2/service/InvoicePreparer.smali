.class public final Lcom/squareup/invoices/editv2/service/InvoicePreparer;
.super Ljava/lang/Object;
.source "InvoicePreparer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoicePreparer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoicePreparer.kt\ncom/squareup/invoices/editv2/service/InvoicePreparer\n+ 2 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,65:1\n132#2,3:66\n*E\n*S KotlinDebug\n*F\n+ 1 InvoicePreparer.kt\ncom/squareup/invoices/editv2/service/InvoicePreparer\n*L\n33#1,3:66\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u00082\u0006\u0010\n\u001a\u00020\u000bJ\u000c\u0010\u000c\u001a\u00020\u000b*\u00020\rH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/service/InvoicePreparer;",
        "",
        "clock",
        "Lcom/squareup/util/Clock;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)V",
        "prepareForSendOrSave",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "invoice",
        "isRecurring",
        "",
        "shouldClearScheduledAt",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/service/InvoicePreparer;->clock:Lcom/squareup/util/Clock;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/service/InvoicePreparer;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private final shouldClearScheduledAt(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Z
    .locals 2

    .line 62
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/service/InvoicePreparer;->clock:Lcom/squareup/util/Clock;

    invoke-static {v0, v1}, Lcom/squareup/invoices/InvoiceDateUtility;->isToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p1

    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public final prepareForSendOrSave(Lcom/squareup/protos/client/invoice/Invoice;Z)Lcom/squareup/protos/client/invoice/Invoice;
    .locals 6

    const-string v0, "invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    move-object v0, p1

    check-cast v0, Lcom/squareup/wire/Message;

    .line 67
    invoke-virtual {v0}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v0

    if-eqz v0, :cond_5

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 35
    invoke-direct {p0, v1}, Lcom/squareup/invoices/editv2/service/InvoicePreparer;->shouldClearScheduledAt(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 36
    move-object v2, v3

    check-cast v2, Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v2, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 39
    :cond_0
    iget-object v2, p1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v2, :cond_1

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    if-nez p1, :cond_2

    .line 40
    :cond_1
    new-instance p1, Ljava/util/Random;

    invoke-direct {p1}, Ljava/util/Random;-><init>()V

    invoke-static {p1, v5, v4, v3}, Lcom/squareup/util/HexStrings;->randomHexString$default(Ljava/util/Random;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 41
    new-instance v2, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 42
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object p1

    .line 43
    invoke-virtual {p1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    iput-object p1, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 46
    :cond_2
    invoke-static {v1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p1

    sget-object v2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne p1, v2, :cond_3

    .line 47
    iget-object p1, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->additional_recipient_email:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 50
    :cond_3
    iget-object p1, p0, Lcom/squareup/invoices/editv2/service/InvoicePreparer;->features:Lcom/squareup/settings/server/Features;

    invoke-static {v1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v2

    sget-object v3, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v2, v3, :cond_4

    goto :goto_0

    :cond_4
    const/4 v4, 0x0

    :goto_0
    invoke-static {v1, p1, p2, v4}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->sanitizeReminders(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/settings/server/Features;ZZ)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 68
    invoke-virtual {v0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p1

    const-string p2, "invoice.buildUpon {\n\n   \u2026() == CARD_ON_FILE)\n    }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/protos/client/invoice/Invoice;

    return-object p1

    .line 67
    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type B"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
