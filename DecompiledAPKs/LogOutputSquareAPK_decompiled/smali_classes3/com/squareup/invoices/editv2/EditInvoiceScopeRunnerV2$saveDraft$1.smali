.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraft$1;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->saveDraft()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/editv2/service/SaveDraftResponse;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "saveDraftResponse",
        "Lcom/squareup/invoices/editv2/service/SaveDraftResponse;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraft$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V
    .locals 2

    .line 593
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraft$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$shouldCompleteOnboardingSetUp(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraft$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getSetupGuideDialogScreenDataFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

    move-result-object p1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;->SAVE_DRAFT:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;

    invoke-virtual {p1, v0}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;->forSetupPaymentsReminder(Ljava/lang/Object;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    move-result-object p1

    .line 596
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraft$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getSetupGuideDialogScreenData$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 597
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraft$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getFlow$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lflow/Flow;

    move-result-object p1

    new-instance v0, Lcom/squareup/setupguide/SetupPaymentsDialog;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraft$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceScopeV2$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/setupguide/SetupPaymentsDialog;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 599
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraft$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    const-string v1, "saveDraftResponse"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$goToSaveDraftConfirmation(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/invoices/editv2/service/SaveDraftResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraft$1;->accept(Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V

    return-void
.end method
