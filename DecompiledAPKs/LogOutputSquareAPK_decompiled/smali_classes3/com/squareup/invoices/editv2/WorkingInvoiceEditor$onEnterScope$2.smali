.class final Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$2;
.super Ljava/lang/Object;
.source "WorkingInvoiceEditor.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/util/Optional<",
        "+",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$2;->this$0:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/util/Optional;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ">;)V"
        }
    .end annotation

    .line 79
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 80
    iget-object p1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$2;->this$0:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->access$getInvoiceRelay$p(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$2$1;->INSTANCE:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$2$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->access$update(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/util/Optional;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$2;->accept(Lcom/squareup/util/Optional;)V

    return-void
.end method
