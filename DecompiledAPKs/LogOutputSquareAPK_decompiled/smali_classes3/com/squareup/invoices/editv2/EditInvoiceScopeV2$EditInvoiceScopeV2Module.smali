.class public abstract Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$EditInvoiceScopeV2Module;
.super Ljava/lang/Object;
.source "EditInvoiceScopeV2.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/features/invoices/widgets/V2WidgetsModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "EditInvoiceScopeV2Module"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\'J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\n\u001a\u00020\u000bH\'J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\n\u001a\u00020\u000bH\'J\u0015\u0010\u0010\u001a\u00020\u00112\u0006\u0010\n\u001a\u00020\u0012H!\u00a2\u0006\u0002\u0008\u0013J\u0015\u0010\u0014\u001a\u00020\u00152\u0006\u0010\n\u001a\u00020\u0016H!\u00a2\u0006\u0002\u0008\u0017J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\n\u001a\u00020\u000bH\'J\u0015\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH!\u00a2\u0006\u0002\u0008\u001eJ\u0015\u0010\u001f\u001a\u00020 2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008!J\u0015\u0010\"\u001a\u00020#2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008$J\u0010\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020(H\'\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$EditInvoiceScopeV2Module;",
        "",
        "()V",
        "configureItemHost",
        "Lcom/squareup/configure/item/ConfigureItemHost;",
        "host",
        "Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;",
        "configureItemHost$invoices_hairball_release",
        "editConfirmationRunner",
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;",
        "runner",
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;",
        "editInvoice1ScreenRunner",
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Runner;",
        "editInvoice2ScreenRunner",
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;",
        "itemSelectTutorialRunner",
        "Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;",
        "Lcom/squareup/register/tutorial/NoOpItemSelectScreenTutorialRunner;",
        "itemSelectTutorialRunner$invoices_hairball_release",
        "keypadEntryRunner",
        "Lcom/squareup/orderentry/KeypadEntryScreen$Runner;",
        "Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;",
        "keypadEntryRunner$invoices_hairball_release",
        "overflowDialogRunner",
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;",
        "providesItemSelectOrderEditor",
        "Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;",
        "editor",
        "Lcom/squareup/invoices/order/WorkingOrderEditor;",
        "providesItemSelectOrderEditor$invoices_hairball_release",
        "providesItemSelectScreenRunner",
        "Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;",
        "providesItemSelectScreenRunner$invoices_hairball_release",
        "setupGuideDialogRunner",
        "Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;",
        "setupGuideDialogRunner$invoices_hairball_release",
        "workingOrderLogger",
        "Lcom/squareup/invoices/order/WorkingOrderLogger;",
        "logger",
        "Lcom/squareup/invoices/editv2/service/InvoicesWorkingOrderLogger;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract configureItemHost$invoices_hairball_release(Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;)Lcom/squareup/configure/item/ConfigureItemHost;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract editConfirmationRunner(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract editInvoice1ScreenRunner(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract editInvoice2ScreenRunner(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract itemSelectTutorialRunner$invoices_hairball_release(Lcom/squareup/register/tutorial/NoOpItemSelectScreenTutorialRunner;)Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract keypadEntryRunner$invoices_hairball_release(Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;)Lcom/squareup/orderentry/KeypadEntryScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract overflowDialogRunner(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract providesItemSelectOrderEditor$invoices_hairball_release(Lcom/squareup/invoices/order/WorkingOrderEditor;)Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract providesItemSelectScreenRunner$invoices_hairball_release(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract setupGuideDialogRunner$invoices_hairball_release(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract workingOrderLogger(Lcom/squareup/invoices/editv2/service/InvoicesWorkingOrderLogger;)Lcom/squareup/invoices/order/WorkingOrderLogger;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
