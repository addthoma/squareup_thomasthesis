.class public final Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Companion;
.super Ljava/lang/Object;
.source "EditInvoiceScopeV2.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nR\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Companion;",
        "",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;",
        "fromType",
        "type",
        "Lcom/squareup/invoices/edit/EditInvoiceScope$Type;",
        "editInvoiceContext",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 163
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromType(Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInvoiceContext"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceScope$Type;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 177
    new-instance p1, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    const/4 v0, 0x0

    invoke-direct {p1, p2, v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;-><init>(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)V

    goto :goto_0

    .line 176
    :cond_0
    new-instance p1, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    sget-object p2, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;->INSTANCE:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    check-cast p2, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-direct {p1, p2, v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;-><init>(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)V

    :goto_0
    return-object p1
.end method
