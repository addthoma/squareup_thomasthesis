.class public final Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditInvoiceV2ConfirmationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceV2ConfirmationCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceV2ConfirmationCoordinator.kt\ncom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,102:1\n1103#2,7:103\n1103#2,7:110\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoiceV2ConfirmationCoordinator.kt\ncom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator\n*L\n55#1,7:103\n68#1,7:110\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J \u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u0017H\u0002J\u0014\u0010\u001c\u001a\u00020\u0011*\u00020\u000c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;",
        "hudToaster",
        "Lcom/squareup/hudtoaster/HudToaster;",
        "(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;Lcom/squareup/hudtoaster/HudToaster;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "copyLinkButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "glyphMessageView",
        "Lcom/squareup/marin/widgets/MarinGlyphMessage;",
        "moreOptions",
        "shareLinkHelperText",
        "Lcom/squareup/widgets/MessageView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "setShareLinkSection",
        "visible",
        "",
        "updateActionBar",
        "actionBarTitle",
        "",
        "success",
        "set",
        "screenData",
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private copyLinkButton:Lcom/squareup/marketfont/MarketButton;

.field private glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private moreOptions:Lcom/squareup/marketfont/MarketButton;

.field private final runner:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;

.field private shareLinkHelperText:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;Lcom/squareup/hudtoaster/HudToaster;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hudToaster"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->runner:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    return-void
.end method

.method public static final synthetic access$getGlyphMessageView$p(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;)Lcom/squareup/marin/widgets/MarinGlyphMessage;
    .locals 1

    .line 22
    iget-object p0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez p0, :cond_0

    const-string v0, "glyphMessageView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getHudToaster$p(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;)Lcom/squareup/hudtoaster/HudToaster;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->runner:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$set(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;Lcom/squareup/marin/widgets/MarinGlyphMessage;Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->set(Lcom/squareup/marin/widgets/MarinGlyphMessage;Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;)V

    return-void
.end method

.method public static final synthetic access$setGlyphMessageView$p(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;Lcom/squareup/marin/widgets/MarinGlyphMessage;)V
    .locals 0

    .line 22
    iput-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    return-void
.end method

.method public static final synthetic access$setShareLinkSection(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;Z)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->setShareLinkSection(Z)V

    return-void
.end method

.method public static final synthetic access$updateActionBar(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;Ljava/lang/String;Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;Z)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->updateActionBar(Ljava/lang/String;Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;Z)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 93
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 95
    sget v0, Lcom/squareup/features/invoices/R$id;->confirmation_glyph_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 96
    sget v0, Lcom/squareup/features/invoices/R$id;->copy_link:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->copyLinkButton:Lcom/squareup/marketfont/MarketButton;

    .line 97
    sget v0, Lcom/squareup/features/invoices/R$id;->more_options:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->moreOptions:Lcom/squareup/marketfont/MarketButton;

    .line 99
    sget v0, Lcom/squareup/features/invoices/R$id;->share_link_email_sent_helper:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->shareLinkHelperText:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final set(Lcom/squareup/marin/widgets/MarinGlyphMessage;Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;)V
    .locals 2

    .line 49
    invoke-virtual {p2}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;->getSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 50
    iget-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const-string v0, "glyphMessageView"

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p2}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;->getTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p2}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;->getSubtitle()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final setShareLinkSection(Z)V
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->copyLinkButton:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_0

    const-string v1, "copyLinkButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 56
    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p1, :cond_1

    .line 103
    new-instance v1, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$setShareLinkSection$$inlined$apply$lambda$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$setShareLinkSection$$inlined$apply$lambda$1;-><init>(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;Z)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->moreOptions:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_2

    const-string v1, "moreOptions"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 69
    :cond_2
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p1, :cond_3

    .line 110
    new-instance v1, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$setShareLinkSection$$inlined$apply$lambda$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$setShareLinkSection$$inlined$apply$lambda$2;-><init>(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;Z)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    :cond_3
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->shareLinkHelperText:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_4

    const-string v1, "shareLinkHelperText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final updateActionBar(Ljava/lang/String;Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;Z)V
    .locals 3

    .line 89
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 85
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 86
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const/4 v1, 0x1

    .line 87
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 88
    new-instance v1, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$updateActionBar$1;

    invoke-direct {v1, p2, p3}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$updateActionBar$1;-><init>(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;Z)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 89
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->bindViews(Landroid/view/View;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;->runner:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;->confirmationScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 38
    new-instance v1, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator$attach$1;-><init>(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "runner.confirmationScree\u2026areLinkButtons)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
