.class public final Lcom/squareup/invoices/InvoiceContactFormatter$showClickable$1;
.super Lcom/squareup/ui/LinkSpan;
.source "InvoiceContactFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/InvoiceContactFormatter;->showClickable(Ljava/lang/CharSequence;Landroid/content/Context;Lcom/squareup/invoices/NameClickableState;)Ljava/lang/CharSequence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/invoices/InvoiceContactFormatter$showClickable$1",
        "Lcom/squareup/ui/LinkSpan;",
        "onClick",
        "",
        "widget",
        "Landroid/view/View;",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;

.field final synthetic $nameClickableState:Lcom/squareup/invoices/NameClickableState;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/NameClickableState;Landroid/content/Context;I)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/squareup/invoices/InvoiceContactFormatter$showClickable$1;->$nameClickableState:Lcom/squareup/invoices/NameClickableState;

    iput-object p2, p0, Lcom/squareup/invoices/InvoiceContactFormatter$showClickable$1;->$context:Landroid/content/Context;

    invoke-direct {p0, p3}, Lcom/squareup/ui/LinkSpan;-><init>(I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "widget"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/invoices/InvoiceContactFormatter$showClickable$1;->$nameClickableState:Lcom/squareup/invoices/NameClickableState;

    check-cast p1, Lcom/squareup/invoices/NameClickableState$Clickable;

    invoke-virtual {p1}, Lcom/squareup/invoices/NameClickableState$Clickable;->getOnClicked()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method
