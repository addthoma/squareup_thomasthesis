.class final Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardSuccessScreen$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealLinkDebitCardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->linkDebitCardSuccessScreen(Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardSuccessScreen$3;->this$0:Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 53
    invoke-virtual {p0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardSuccessScreen$3;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    .line 197
    iget-object v0, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardSuccessScreen$3;->this$0:Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;

    invoke-static {v0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->access$getBrowserLauncher$p(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)Lcom/squareup/util/BrowserLauncher;

    move-result-object v0

    sget v1, Lcom/squareup/debitcard/R$string;->instant_deposits_link_card_url:I

    invoke-interface {v0, v1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(I)V

    return-void
.end method
