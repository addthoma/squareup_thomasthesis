.class public final Lcom/squareup/debitcard/VerifyCardChangeScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "VerifyCardChangeScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/VerifyCardChangeScope$ParentComponent;,
        Lcom/squareup/debitcard/VerifyCardChangeScope$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nVerifyCardChangeScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 VerifyCardChangeScope.kt\ncom/squareup/debitcard/VerifyCardChangeScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,40:1\n35#2:41\n*E\n*S KotlinDebug\n*F\n+ 1 VerifyCardChangeScope.kt\ncom/squareup/debitcard/VerifyCardChangeScope\n*L\n15#1:41\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0018\u0000 \u00102\u00020\u0001:\u0002\u0010\u0011B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0018\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0014J\u0008\u0010\u000e\u001a\u00020\u000fH\u0016R\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/debitcard/VerifyCardChangeScope;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "parentKey",
        "(Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "getParentKey",
        "",
        "Companion",
        "ParentComponent",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
            "Lcom/squareup/debitcard/VerifyCardChangeScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/debitcard/VerifyCardChangeScope$Companion;


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/debitcard/VerifyCardChangeScope$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/debitcard/VerifyCardChangeScope$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/debitcard/VerifyCardChangeScope;->Companion:Lcom/squareup/debitcard/VerifyCardChangeScope$Companion;

    .line 35
    sget-object v0, Lcom/squareup/debitcard/VerifyCardChangeScope$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/debitcard/VerifyCardChangeScope$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.fromParcel {\u2026ava.classLoader)!!)\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/debitcard/VerifyCardChangeScope;->CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "parentKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/debitcard/VerifyCardChangeScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    const-class v0, Lcom/squareup/debitcard/VerifyCardChangeScope$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 15
    check-cast v0, Lcom/squareup/debitcard/VerifyCardChangeScope$ParentComponent;

    .line 16
    invoke-interface {v0}, Lcom/squareup/debitcard/VerifyCardChangeScope$ParentComponent;->verifyCardChangeWorkflowRunner()Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner;

    move-result-object v0

    .line 18
    invoke-super {p0, p1}, Lcom/squareup/ui/main/RegisterTreeKey;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object p1

    const-string v1, "it"

    .line 19
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string v0, "super.buildScope(parentS\u2026er.registerServices(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 27
    iget-object v0, p0, Lcom/squareup/debitcard/VerifyCardChangeScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/debitcard/VerifyCardChangeScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method
