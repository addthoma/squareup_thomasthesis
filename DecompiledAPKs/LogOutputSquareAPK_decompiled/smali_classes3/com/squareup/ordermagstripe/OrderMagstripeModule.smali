.class public abstract Lcom/squareup/ordermagstripe/OrderMagstripeModule;
.super Ljava/lang/Object;
.source "OrderMagstripeModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\'\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008\u000cJ\u0015\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H!\u00a2\u0006\u0002\u0008\u0011J\u0015\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H!\u00a2\u0006\u0002\u0008\u0016\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ordermagstripe/OrderMagstripeModule;",
        "",
        "()V",
        "bindMailOrderAnalytics",
        "Lcom/squareup/mailorder/MailOrderAnalytics;",
        "analytics",
        "Lcom/squareup/ordermagstripe/RealMailOrderAnalytics;",
        "bindMailOrderAnalytics$impl_wiring_release",
        "bindOrderMagstripeWorkflowRunner",
        "Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner;",
        "workflowRunner",
        "Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;",
        "bindOrderMagstripeWorkflowRunner$impl_wiring_release",
        "bindOrderScreenWorkflowStarter",
        "Lcom/squareup/mailorder/OrderScreenWorkflowStarter;",
        "orderScreenWorkflowStarter",
        "Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;",
        "bindOrderScreenWorkflowStarter$impl_wiring_release",
        "bindOrderServiceHelperEndpoints",
        "Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;",
        "serviceHelper",
        "Lcom/squareup/ordermagstripe/OrderMagstripeServiceHelper;",
        "bindOrderServiceHelperEndpoints$impl_wiring_release",
        "Companion",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ordermagstripe/OrderMagstripeModule;->Companion:Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideCorrectedAddressConfiguration()Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ordermagstripe/OrderMagstripeModule;->Companion:Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;->provideCorrectedAddressConfiguration()Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public static final provideOrderConfirmationConfiguration()Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ordermagstripe/OrderMagstripeModule;->Companion:Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;->provideOrderConfirmationConfiguration()Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public static final provideOrderCoodinatorConfiguration()Lcom/squareup/mailorder/OrderCoordinator$Configuration;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ordermagstripe/OrderMagstripeModule;->Companion:Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;->provideOrderCoodinatorConfiguration()Lcom/squareup/mailorder/OrderCoordinator$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public static final provideOrderReactorConfiguration(Lcom/squareup/settings/server/Features;)Lcom/squareup/mailorder/OrderReactor$Configuration;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ordermagstripe/OrderMagstripeModule;->Companion:Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;->provideOrderReactorConfiguration(Lcom/squareup/settings/server/Features;)Lcom/squareup/mailorder/OrderReactor$Configuration;

    move-result-object p0

    return-object p0
.end method

.method public static final provideUnverifiedAddressConfiguration()Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ordermagstripe/OrderMagstripeModule;->Companion:Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;->provideUnverifiedAddressConfiguration()Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract bindMailOrderAnalytics$impl_wiring_release(Lcom/squareup/ordermagstripe/RealMailOrderAnalytics;)Lcom/squareup/mailorder/MailOrderAnalytics;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindOrderMagstripeWorkflowRunner$impl_wiring_release(Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;)Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindOrderScreenWorkflowStarter$impl_wiring_release(Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;)Lcom/squareup/mailorder/OrderScreenWorkflowStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindOrderServiceHelperEndpoints$impl_wiring_release(Lcom/squareup/ordermagstripe/OrderMagstripeServiceHelper;)Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
