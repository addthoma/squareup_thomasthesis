.class final Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$StarterAdapter;
.super Ljava/lang/Object;
.source "RealOrderMagstripeWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowStarter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "StarterAdapter"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderMagstripeWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderMagstripeWorkflowRunner.kt\ncom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$StarterAdapter\n*L\n1#1,108:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J`\u0010\u0007\u001aP\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\nj\n\u0012\u0006\u0008\u0001\u0012\u00020\u000b`\u000e0\t\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0008j\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003`\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016J\u008e\u0001\u0010\u0012\u001aP\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u000b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\nj\n\u0012\u0006\u0008\u0001\u0012\u00020\u000b`\u000e0\t\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0008j\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003`\u000f*6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r0\n0\t\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0013j\u0002`\u0015H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$StarterAdapter;",
        "Lcom/squareup/container/PosWorkflowStarter;",
        "",
        "Lcom/squareup/mailorder/OrderWorkflowResult;",
        "orderWorkflowStarter",
        "Lcom/squareup/mailorder/OrderScreenWorkflowStarter;",
        "(Lcom/squareup/mailorder/OrderScreenWorkflowStarter;)V",
        "start",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/container/PosWorkflow;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "toPosWorkflow",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/mailorder/OrderScreenWorkflow;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final orderWorkflowStarter:Lcom/squareup/mailorder/OrderScreenWorkflowStarter;


# direct methods
.method public constructor <init>(Lcom/squareup/mailorder/OrderScreenWorkflowStarter;)V
    .locals 1

    const-string v0, "orderWorkflowStarter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$StarterAdapter;->orderWorkflowStarter:Lcom/squareup/mailorder/OrderScreenWorkflowStarter;

    return-void
.end method

.method private final toPosWorkflow(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+",
            "Lcom/squareup/workflow/ScreenState<",
            "+",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*+",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;"
        }
    .end annotation

    .line 103
    sget-object v0, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$StarterAdapter$toPosWorkflow$1;->INSTANCE:Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$StarterAdapter$toPosWorkflow$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/rx1/ScreensKt;->mapScreen(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    .line 104
    invoke-static {p1}, Lcom/squareup/workflow/rx1/WorkflowKt;->toCoreWorkflow(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 96
    iget-object v0, p0, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$StarterAdapter;->orderWorkflowStarter:Lcom/squareup/mailorder/OrderScreenWorkflowStarter;

    invoke-interface {v0, p1}, Lcom/squareup/mailorder/OrderScreenWorkflowStarter;->start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 97
    :cond_0
    iget-object p1, p0, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$StarterAdapter;->orderWorkflowStarter:Lcom/squareup/mailorder/OrderScreenWorkflowStarter;

    new-instance v6, Lcom/squareup/mailorder/OrderWorkflowStartArgument;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/mailorder/OrderWorkflowStartArgument;-><init>(Lcom/squareup/mailorder/ContactInfo;Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p1, v6}, Lcom/squareup/mailorder/OrderScreenWorkflowStarter;->start(Lcom/squareup/mailorder/OrderWorkflowStartArgument;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    .line 99
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$StarterAdapter;->toPosWorkflow(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method
