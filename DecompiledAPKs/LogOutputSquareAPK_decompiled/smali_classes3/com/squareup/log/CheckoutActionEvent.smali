.class public final Lcom/squareup/log/CheckoutActionEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "CheckoutActionEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0017\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0007\u0010\u0008\u001a\u0004\u0008\u0006\u0010\tR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/log/CheckoutActionEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "isUnitPriced",
        "",
        "isVariablePriced",
        "(ZZ)V",
        "is_unit_priced",
        "is_unit_priced$annotations",
        "()V",
        "()Z",
        "pricing_type",
        "",
        "getPricing_type",
        "()Ljava/lang/String;",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final is_unit_priced:Z

.field private final pricing_type:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZZ)V
    .locals 2

    .line 12
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Checkout: Add Item to Cart"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 14
    iput-boolean p1, p0, Lcom/squareup/log/CheckoutActionEvent;->is_unit_priced:Z

    if-eqz p2, :cond_0

    const-string/jumbo p1, "variable"

    goto :goto_0

    :cond_0
    const-string p1, "fixed"

    .line 15
    :goto_0
    iput-object p1, p0, Lcom/squareup/log/CheckoutActionEvent;->pricing_type:Ljava/lang/String;

    return-void
.end method

.method public static synthetic is_unit_priced$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final getPricing_type()Ljava/lang/String;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/log/CheckoutActionEvent;->pricing_type:Ljava/lang/String;

    return-object v0
.end method

.method public final is_unit_priced()Z
    .locals 1

    .line 14
    iget-boolean v0, p0, Lcom/squareup/log/CheckoutActionEvent;->is_unit_priced:Z

    return v0
.end method
