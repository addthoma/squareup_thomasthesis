.class public Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent$Builder;
.super Lcom/squareup/log/ReaderEvent$Builder;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public error:Lcom/squareup/cardreader/lcr/CrsReaderError;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1149
    invoke-direct {p0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildReaderEvent()Lcom/squareup/log/ReaderEvent;
    .locals 1

    .line 1149
    invoke-virtual {p0}, Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent$Builder;->buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent;

    move-result-object v0

    return-object v0
.end method

.method public buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent;
    .locals 1

    .line 1158
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent;

    invoke-direct {v0, p0}, Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent;-><init>(Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent$Builder;)V

    return-object v0
.end method

.method public error(Lcom/squareup/cardreader/lcr/CrsReaderError;)Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent$Builder;
    .locals 0

    .line 1153
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent$Builder;->error:Lcom/squareup/cardreader/lcr/CrsReaderError;

    return-object p0
.end method
