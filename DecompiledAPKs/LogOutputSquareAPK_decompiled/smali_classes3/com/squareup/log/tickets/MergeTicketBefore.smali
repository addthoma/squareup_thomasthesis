.class public Lcom/squareup/log/tickets/MergeTicketBefore;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "MergeTicketBefore.java"


# instance fields
.field private final number_of_items_in_parent_transaction:I

.field private final number_of_items_in_transactions_to_merge:I

.field private final number_of_transactions_to_merge:I

.field private final parent_transaction_client_id:Ljava/lang/String;

.field private final parent_transaction_total_amount:Ljava/lang/String;

.field private final transactions_to_merge_total_amount:Ljava/lang/String;


# direct methods
.method public constructor <init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MERGE_TICKETS_BEFORE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 21
    iput p1, p0, Lcom/squareup/log/tickets/MergeTicketBefore;->number_of_transactions_to_merge:I

    .line 22
    iput p2, p0, Lcom/squareup/log/tickets/MergeTicketBefore;->number_of_items_in_transactions_to_merge:I

    .line 23
    iput p3, p0, Lcom/squareup/log/tickets/MergeTicketBefore;->number_of_items_in_parent_transaction:I

    .line 24
    iput-object p4, p0, Lcom/squareup/log/tickets/MergeTicketBefore;->parent_transaction_client_id:Ljava/lang/String;

    .line 25
    iput-object p5, p0, Lcom/squareup/log/tickets/MergeTicketBefore;->transactions_to_merge_total_amount:Ljava/lang/String;

    .line 26
    iput-object p6, p0, Lcom/squareup/log/tickets/MergeTicketBefore;->parent_transaction_total_amount:Ljava/lang/String;

    return-void
.end method
