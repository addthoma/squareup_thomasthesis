.class public Lcom/squareup/log/ReaderEvent$Builder;
.super Ljava/lang/Object;
.source "ReaderEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field public chargeCycleCount:I

.field public connectionType:Ljava/lang/String;

.field public firmwareVersion:Ljava/lang/String;

.field public hardwareSerialNumber:Ljava/lang/String;

.field public inCommsRate:Ljava/lang/String;

.field public outCommsRate:Ljava/lang/String;

.field public overrideName:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public overrideRawBytes:Lokio/ByteString;

.field public overrideTimestamp:Lcom/squareup/protos/common/time/DateTime;

.field public overrideValue:Ljava/lang/String;

.field public paymentTimings:Lcom/squareup/cardreader/PaymentTimings;

.field public readerTypePrefix:Ljava/lang/String;

.field public sessionId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    sget-object v0, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEvent$Builder;->paymentTimings(Lcom/squareup/cardreader/PaymentTimings;)Lcom/squareup/log/ReaderEvent$Builder;

    .line 83
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->READER:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEvent$Builder;->overrideName(Lcom/squareup/eventstream/v1/EventStream$Name;)Lcom/squareup/log/ReaderEvent$Builder;

    return-void
.end method


# virtual methods
.method public buildReaderEvent()Lcom/squareup/log/ReaderEvent;
    .locals 1

    .line 209
    new-instance v0, Lcom/squareup/log/ReaderEvent;

    invoke-direct {v0, p0}, Lcom/squareup/log/ReaderEvent;-><init>(Lcom/squareup/log/ReaderEvent$Builder;)V

    return-object v0
.end method

.method public cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object p0
.end method

.method public chargeCycleCount(I)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 0

    .line 154
    iput p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->chargeCycleCount:I

    return-object p0
.end method

.method public connectionType(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->connectionType:Ljava/lang/String;

    return-object p0
.end method

.method public firmwareVersion(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->firmwareVersion:Ljava/lang/String;

    return-object p0
.end method

.method public hardwareSerialNumber(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->hardwareSerialNumber:Ljava/lang/String;

    return-object p0
.end method

.method public inCommsRate(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->inCommsRate:Ljava/lang/String;

    return-object p0
.end method

.method public outCommsRate(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->outCommsRate:Ljava/lang/String;

    return-object p0
.end method

.method public overrideName(Lcom/squareup/eventstream/v1/EventStream$Name;)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->overrideName:Lcom/squareup/eventstream/v1/EventStream$Name;

    return-object p0
.end method

.method public overrideRawBytes([B)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 0

    .line 102
    invoke-static {p1}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->overrideRawBytes:Lokio/ByteString;

    return-object p0
.end method

.method public overrideTimestamp(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->overrideTimestamp:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public overrideTimestampIfNotSet(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/log/ReaderEvent$Builder;->overrideTimestamp:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_0

    return-object p0

    .line 115
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/log/ReaderEvent$Builder;->overrideTimestamp(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/log/ReaderEvent$Builder;

    move-result-object p1

    return-object p1
.end method

.method public overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->overrideValue:Ljava/lang/String;

    return-object p0
.end method

.method public paymentTimings(Lcom/squareup/cardreader/PaymentTimings;)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->paymentTimings:Lcom/squareup/cardreader/PaymentTimings;

    return-object p0
.end method

.method public readerTypePrefix(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->readerTypePrefix:Ljava/lang/String;

    return-object p0
.end method

.method public sessionId(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->sessionId:Ljava/lang/String;

    return-object p0
.end method

.method public setReaderTypePrefix(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/log/ReaderEvent$Builder;
    .locals 3

    .line 164
    sget-object v0, Lcom/squareup/log/ReaderEvent$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 199
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid readerType provided: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-string p1, "T2 "

    .line 196
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->readerTypePrefix:Ljava/lang/String;

    return-object p0

    :pswitch_1
    const-string p1, "X2 "

    .line 193
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->readerTypePrefix:Ljava/lang/String;

    return-object p0

    :pswitch_2
    const-string p1, "R12 "

    .line 190
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->readerTypePrefix:Ljava/lang/String;

    return-object p0

    :pswitch_3
    const-string p1, "ENCRYPTED_KEYED "

    .line 187
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->readerTypePrefix:Ljava/lang/String;

    return-object p0

    :pswitch_4
    const-string p1, "R6 "

    .line 184
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->readerTypePrefix:Ljava/lang/String;

    return-object p0

    :pswitch_5
    const-string p1, "R4 "

    .line 181
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->readerTypePrefix:Ljava/lang/String;

    return-object p0

    :pswitch_6
    const-string p1, "S1 "

    .line 178
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->readerTypePrefix:Ljava/lang/String;

    return-object p0

    :pswitch_7
    const-string p1, "O1 "

    .line 175
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->readerTypePrefix:Ljava/lang/String;

    return-object p0

    :pswitch_8
    const-string p1, "UNENCRYPTED "

    .line 172
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->readerTypePrefix:Ljava/lang/String;

    return-object p0

    :pswitch_9
    const-string p1, "KEYED "

    .line 169
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->readerTypePrefix:Ljava/lang/String;

    return-object p0

    :pswitch_a
    const-string p1, "UNKNOWN "

    .line 166
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent$Builder;->readerTypePrefix:Ljava/lang/String;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
