.class public final Lcom/squareup/log/CrashReportingLogger_Factory;
.super Ljava/lang/Object;
.source "CrashReportingLogger_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/log/CrashReportingLogger;",
        ">;"
    }
.end annotation


# instance fields
.field private final appUpgradeDetectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/AppUpgradeDetector;",
            ">;"
        }
    .end annotation
.end field

.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceSerialNumberProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final featureFlagsForLogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            ">;"
        }
    .end annotation
.end field

.field private final installationIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mortarScopeHierarchyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final playServicesVersionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/versions/PlayServicesVersions;",
            ">;"
        }
    .end annotation
.end field

.field private final posBuildProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;"
        }
    .end annotation
.end field

.field private final processUniqueIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/ProcessUniqueId;",
            ">;"
        }
    .end annotation
.end field

.field private final reporterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CrashReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final startUptimeMsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final userAgentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final viewHierarchyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CrashReporter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/AppUpgradeDetector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Long;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/versions/PlayServicesVersions;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/ProcessUniqueId;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 66
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->resourcesProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 67
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->reporterProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 68
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->mortarScopeHierarchyProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 69
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->viewHierarchyProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 70
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->featureFlagsForLogsProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 71
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 72
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->applicationProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 73
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->installationIdProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 74
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->userAgentProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 75
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->appUpgradeDetectorProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 76
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->startUptimeMsProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 77
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->deviceSerialNumberProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 78
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->posBuildProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 79
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->playServicesVersionsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 80
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->processUniqueIdProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 81
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/log/CrashReportingLogger_Factory;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CrashReporter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/AppUpgradeDetector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Long;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/versions/PlayServicesVersions;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/ProcessUniqueId;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Lcom/squareup/log/CrashReportingLogger_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 100
    new-instance v17, Lcom/squareup/log/CrashReportingLogger_Factory;

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/log/CrashReportingLogger_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v17
.end method

.method public static newInstance(Landroid/content/res/Resources;Lcom/squareup/log/CrashReporter;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/log/FeatureFlagsForLogs;Lcom/squareup/util/Device;Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/log/AppUpgradeDetector;JLjava/lang/String;Lcom/squareup/util/PosBuild;Lcom/squareup/firebase/versions/PlayServicesVersions;Lcom/squareup/analytics/ProcessUniqueId;Ljavax/inject/Provider;)Lcom/squareup/log/CrashReportingLogger;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/log/CrashReporter;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            "Lcom/squareup/util/Device;",
            "Landroid/app/Application;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/log/AppUpgradeDetector;",
            "J",
            "Ljava/lang/String;",
            "Lcom/squareup/util/PosBuild;",
            "Lcom/squareup/firebase/versions/PlayServicesVersions;",
            "Lcom/squareup/analytics/ProcessUniqueId;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Lcom/squareup/log/CrashReportingLogger;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-wide/from16 v11, p10

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 110
    new-instance v18, Lcom/squareup/log/CrashReportingLogger;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/log/CrashReportingLogger;-><init>(Landroid/content/res/Resources;Lcom/squareup/log/CrashReporter;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/log/FeatureFlagsForLogs;Lcom/squareup/util/Device;Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/log/AppUpgradeDetector;JLjava/lang/String;Lcom/squareup/util/PosBuild;Lcom/squareup/firebase/versions/PlayServicesVersions;Lcom/squareup/analytics/ProcessUniqueId;Ljavax/inject/Provider;)V

    return-object v18
.end method


# virtual methods
.method public get()Lcom/squareup/log/CrashReportingLogger;
    .locals 19

    move-object/from16 v0, p0

    .line 86
    iget-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/content/res/Resources;

    iget-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->reporterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/log/CrashReporter;

    iget-object v4, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->mortarScopeHierarchyProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->viewHierarchyProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->featureFlagsForLogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/log/FeatureFlagsForLogs;

    iget-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/app/Application;

    iget-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->installationIdProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->userAgentProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->appUpgradeDetectorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/log/AppUpgradeDetector;

    iget-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->startUptimeMsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    iget-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->deviceSerialNumberProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->posBuildProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/util/PosBuild;

    iget-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->playServicesVersionsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/firebase/versions/PlayServicesVersions;

    iget-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->processUniqueIdProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/analytics/ProcessUniqueId;

    iget-object v1, v0, Lcom/squareup/log/CrashReportingLogger_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    move-object/from16 v18, v1

    invoke-static/range {v2 .. v18}, Lcom/squareup/log/CrashReportingLogger_Factory;->newInstance(Landroid/content/res/Resources;Lcom/squareup/log/CrashReporter;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/log/FeatureFlagsForLogs;Lcom/squareup/util/Device;Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/log/AppUpgradeDetector;JLjava/lang/String;Lcom/squareup/util/PosBuild;Lcom/squareup/firebase/versions/PlayServicesVersions;Lcom/squareup/analytics/ProcessUniqueId;Ljavax/inject/Provider;)Lcom/squareup/log/CrashReportingLogger;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/log/CrashReportingLogger_Factory;->get()Lcom/squareup/log/CrashReportingLogger;

    move-result-object v0

    return-object v0
.end method
