.class synthetic Lcom/squareup/log/ReaderEventLogger$1;
.super Ljava/lang/Object;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$cardreader$lcr$CrTamperStatus:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 784
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrTamperStatus;->values()[Lcom/squareup/cardreader/lcr/CrTamperStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/log/ReaderEventLogger$1;->$SwitchMap$com$squareup$cardreader$lcr$CrTamperStatus:[I

    :try_start_0
    sget-object v0, Lcom/squareup/log/ReaderEventLogger$1;->$SwitchMap$com$squareup$cardreader$lcr$CrTamperStatus:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrTamperStatus;->CR_TAMPER_STATUS_NORMAL:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrTamperStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/log/ReaderEventLogger$1;->$SwitchMap$com$squareup$cardreader$lcr$CrTamperStatus:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrTamperStatus;->CR_TAMPER_STATUS_FLAGGED:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrTamperStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/log/ReaderEventLogger$1;->$SwitchMap$com$squareup$cardreader$lcr$CrTamperStatus:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrTamperStatus;->CR_TAMPER_STATUS_TAMPERED:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrTamperStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/log/ReaderEventLogger$1;->$SwitchMap$com$squareup$cardreader$lcr$CrTamperStatus:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrTamperStatus;->CR_TAMPER_STATUS_UNKNOWN:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrTamperStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    return-void
.end method
