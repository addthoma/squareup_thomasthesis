.class public abstract Lcom/squareup/log/accuracy/LocalTenderEvent;
.super Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;
.source "LocalTenderEvent.java"


# instance fields
.field public final _payer_tendered_amount:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseLocalTender;Lcom/squareup/payment/BillPayment;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/payment/BillPayment;)V

    .line 15
    invoke-virtual {p3}, Lcom/squareup/payment/tender/BaseLocalTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/log/accuracy/LocalTenderEvent;->optionalMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    iput-object p1, p0, Lcom/squareup/log/accuracy/LocalTenderEvent;->_payer_tendered_amount:Ljava/lang/Long;

    return-void
.end method
