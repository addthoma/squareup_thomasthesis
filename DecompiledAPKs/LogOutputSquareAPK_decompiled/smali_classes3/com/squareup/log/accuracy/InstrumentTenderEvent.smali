.class public Lcom/squareup/log/accuracy/InstrumentTenderEvent;
.super Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;
.source "InstrumentTenderEvent.java"


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/payment/BillPayment;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/payment/BillPayment;)V

    return-void
.end method


# virtual methods
.method protected paymentType()Ljava/lang/String;
    .locals 1

    const-string v0, "instrument"

    return-object v0
.end method
