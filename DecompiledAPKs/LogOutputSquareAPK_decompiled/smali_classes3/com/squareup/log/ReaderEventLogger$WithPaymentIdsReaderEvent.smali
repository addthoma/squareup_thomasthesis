.class public Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent;
.super Lcom/squareup/log/ReaderEvent;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WithPaymentIdsReaderEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;
    }
.end annotation


# instance fields
.field public final cardPresenceRequired:Ljava/lang/Boolean;

.field public final clientTenderId:Ljava/lang/String;

.field public final paymentSessionId:Ljava/lang/String;

.field public final serverTenderId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;)V
    .locals 1

    .line 1319
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEvent;-><init>(Lcom/squareup/log/ReaderEvent$Builder;)V

    .line 1320
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->clientTenderId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent;->clientTenderId:Ljava/lang/String;

    .line 1321
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->paymentSessionId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent;->paymentSessionId:Ljava/lang/String;

    .line 1322
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->serverTenderId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent;->serverTenderId:Ljava/lang/String;

    .line 1323
    iget-object p1, p1, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->cardPresenceRequired:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent;->cardPresenceRequired:Ljava/lang/Boolean;

    return-void
.end method
