.class public Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;
.super Lcom/squareup/log/ReaderEvent$Builder;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1581
    invoke-direct {p0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildReaderEvent()Lcom/squareup/log/ReaderEvent;
    .locals 1

    .line 1581
    invoke-virtual {p0}, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;

    move-result-object v0

    return-object v0
.end method

.method public buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;
    .locals 2

    .line 1591
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;-><init>(Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;Lcom/squareup/log/ReaderEventLogger$1;)V

    return-object v0
.end method

.method public event(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;
    .locals 2

    .line 1585
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_EVENT:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v1}, Lcom/squareup/analytics/ReaderEventName;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->overrideValue(Ljava/lang/String;)Lcom/squareup/log/ReaderEvent$Builder;

    .line 1586
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    return-object p0
.end method
