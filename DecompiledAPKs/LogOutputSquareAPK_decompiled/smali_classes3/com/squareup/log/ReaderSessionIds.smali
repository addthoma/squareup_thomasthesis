.class public Lcom/squareup/log/ReaderSessionIds;
.super Ljava/lang/Object;
.source "ReaderSessionIds.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;


# instance fields
.field private final paymentSessionIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final uuidGenerator:Lcom/squareup/log/UUIDGenerator;


# direct methods
.method public constructor <init>(Lcom/squareup/log/UUIDGenerator;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/log/ReaderSessionIds;->sessionIds:Ljava/util/Map;

    .line 25
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/log/ReaderSessionIds;->paymentSessionIds:Ljava/util/Map;

    .line 28
    iput-object p1, p0, Lcom/squareup/log/ReaderSessionIds;->uuidGenerator:Lcom/squareup/log/UUIDGenerator;

    return-void
.end method


# virtual methods
.method public getPaymentSessionId(Lcom/squareup/cardreader/CardReaderId;)Ljava/lang/String;
    .locals 3

    .line 67
    invoke-virtual {p0, p1}, Lcom/squareup/log/ReaderSessionIds;->hasPaymentSessionId(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v2, "No PaymentSessionId for CardReader: %s"

    .line 68
    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 67
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/log/ReaderSessionIds;->paymentSessionIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public getSessionId(Lcom/squareup/cardreader/CardReaderId;)Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/log/ReaderSessionIds;->sessionIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public hasPaymentSessionId(Lcom/squareup/cardreader/CardReaderId;)Z
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/log/ReaderSessionIds;->paymentSessionIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/log/ReaderSessionIds;->sessionIds:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/log/ReaderSessionIds;->uuidGenerator:Lcom/squareup/log/UUIDGenerator;

    invoke-interface {v1}, Lcom/squareup/log/UUIDGenerator;->randomUUID()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/squareup/log/ReaderSessionIds;->sessionIds:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "ReaderSessionIds::onCardReaderRemoved session ids doesn\'t contain card reader id"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/log/ReaderSessionIds;->sessionIds:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onEmvPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 2

    .line 49
    invoke-virtual {p0, p1}, Lcom/squareup/log/ReaderSessionIds;->hasPaymentSessionId(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/squareup/log/ReaderSessionIds;->paymentSessionIds:Ljava/util/Map;

    iget-object v1, p0, Lcom/squareup/log/ReaderSessionIds;->uuidGenerator:Lcom/squareup/log/UUIDGenerator;

    invoke-interface {v1}, Lcom/squareup/log/UUIDGenerator;->randomUUID()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onEmvPaymentTerminated(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 2

    .line 57
    invoke-virtual {p0, p1}, Lcom/squareup/log/ReaderSessionIds;->hasPaymentSessionId(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    const-string v1, "ReaderSessionIds::onEmvPaymentTerminated payment session ids doesn\'t contain reader id"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/log/ReaderSessionIds;->paymentSessionIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
