.class public Lcom/squareup/log/advancedmodifiers/HideModifierFromCustomerEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "HideModifierFromCustomerEvent.java"


# instance fields
.field final number_of_items_with_mod_hidden_from_buyer:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_HIDE_MODIFIER_FROM_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 14
    iput p1, p0, Lcom/squareup/log/advancedmodifiers/HideModifierFromCustomerEvent;->number_of_items_with_mod_hidden_from_buyer:I

    return-void
.end method
