.class public Lcom/squareup/log/advancedmodifiers/PreSelectedModifierUsedEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "PreSelectedModifierUsedEvent.java"


# instance fields
.field final number_of_items_with_pre_selected:I

.field final number_of_modifier_options_pre_selected:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_PRE_SELECTED_MODIFIER_USED_IN_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 16
    iput p1, p0, Lcom/squareup/log/advancedmodifiers/PreSelectedModifierUsedEvent;->number_of_items_with_pre_selected:I

    .line 17
    iput p2, p0, Lcom/squareup/log/advancedmodifiers/PreSelectedModifierUsedEvent;->number_of_modifier_options_pre_selected:I

    return-void
.end method
