.class public Lcom/squareup/log/advancedmodifiers/SkipItemModalEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "SkipItemModalEvent.java"


# instance fields
.field final number_of_items_that_skip_item_modal:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_SKIP_ITEM_MODAL:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 14
    iput p1, p0, Lcom/squareup/log/advancedmodifiers/SkipItemModalEvent;->number_of_items_that_skip_item_modal:I

    return-void
.end method
