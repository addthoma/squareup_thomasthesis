.class public Lcom/squareup/eventstream/DroppedEventCounter;
.super Ljava/lang/Object;
.source "DroppedEventCounter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/eventstream/DroppedEventCounter$DropType;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDroppedEventCounter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DroppedEventCounter.kt\ncom/squareup/eventstream/DroppedEventCounter\n*L\n1#1,92:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008\u0016\u0018\u00002\u00020\u0001:\u0001\u0013B\u0017\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\n2\u0006\u0010\u0011\u001a\u00020\tH\u0016J\u001c\u0010\u0012\u001a\u00020\u000f2\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\"\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0018\u00010\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u000e\u0010\r\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/eventstream/DroppedEventCounter;",
        "",
        "countStorage",
        "Landroid/content/SharedPreferences;",
        "logger",
        "Lcom/squareup/eventstream/EventStreamLog;",
        "(Landroid/content/SharedPreferences;Lcom/squareup/eventstream/EventStreamLog;)V",
        "counts",
        "",
        "Lcom/squareup/eventstream/DroppedEventCounter$DropType;",
        "",
        "getCounts",
        "()Ljava/util/Map;",
        "lock",
        "add",
        "",
        "delta",
        "dropType",
        "remove",
        "DropType",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final countStorage:Landroid/content/SharedPreferences;

.field private final lock:Ljava/lang/Object;

.field private final logger:Lcom/squareup/eventstream/EventStreamLog;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/squareup/eventstream/EventStreamLog;)V
    .locals 1

    const-string v0, "countStorage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "logger"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/eventstream/DroppedEventCounter;->countStorage:Landroid/content/SharedPreferences;

    iput-object p2, p0, Lcom/squareup/eventstream/DroppedEventCounter;->logger:Lcom/squareup/eventstream/EventStreamLog;

    .line 40
    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/eventstream/DroppedEventCounter;->lock:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public add(ILcom/squareup/eventstream/DroppedEventCounter$DropType;)V
    .locals 5

    const-string v0, "dropType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/eventstream/DroppedEventCounter;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 47
    :try_start_0
    iget-object v1, p0, Lcom/squareup/eventstream/DroppedEventCounter;->countStorage:Landroid/content/SharedPreferences;

    iget-object v2, p2, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->key:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 48
    iget-object v2, p0, Lcom/squareup/eventstream/DroppedEventCounter;->countStorage:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 49
    iget-object v4, p2, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->key:Ljava/lang/String;

    add-int/2addr v1, p1

    invoke-interface {v2, v4, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 51
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result p1

    if-nez p1, :cond_0

    .line 53
    iget-object p1, p0, Lcom/squareup/eventstream/DroppedEventCounter;->logger:Lcom/squareup/eventstream/EventStreamLog;

    const-string v1, "Could not add event %s to disk"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-interface {p1, v1, v2}, Lcom/squareup/eventstream/EventStreamLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    :cond_0
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public getCounts()Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/eventstream/DroppedEventCounter$DropType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/squareup/eventstream/DroppedEventCounter;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 65
    :try_start_0
    new-instance v1, Ljava/util/EnumMap;

    const-class v2, Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    invoke-direct {v1, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    check-cast v1, Ljava/util/Map;

    .line 66
    invoke-static {}, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->values()[Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    if-ge v5, v3, :cond_1

    aget-object v7, v2, v5

    .line 67
    iget-object v8, p0, Lcom/squareup/eventstream/DroppedEventCounter;->countStorage:Landroid/content/SharedPreferences;

    iget-object v9, v7, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->key:Ljava/lang/String;

    invoke-interface {v8, v9, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    if-lez v8, :cond_0

    const/4 v6, 0x1

    .line 71
    :cond_0
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v1, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    if-eqz v6, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    .line 73
    :goto_1
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 63
    monitor-exit v0

    throw v1
.end method

.method public remove(Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/eventstream/DroppedEventCounter$DropType;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-string v0, "counts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/eventstream/DroppedEventCounter;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 79
    :try_start_0
    iget-object v1, p0, Lcom/squareup/eventstream/DroppedEventCounter;->countStorage:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 80
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    .line 81
    iget-object v6, p0, Lcom/squareup/eventstream/DroppedEventCounter;->countStorage:Landroid/content/SharedPreferences;

    iget-object v7, v5, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->key:Ljava/lang/String;

    invoke-interface {v6, v7, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 82
    iget-object v5, v5, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->key:Ljava/lang/String;

    sub-int/2addr v6, v3

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-interface {v1, v5, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 85
    :cond_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    if-nez v1, :cond_1

    .line 87
    iget-object v1, p0, Lcom/squareup/eventstream/DroppedEventCounter;->logger:Lcom/squareup/eventstream/EventStreamLog;

    const-string v2, "Could not remove events %s to disk"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/squareup/eventstream/EventStreamLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    :cond_1
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method
