.class public interface abstract Lcom/squareup/eventstream/EventBatchUploader;
.super Ljava/lang/Object;
.source "EventBatchUploader.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/eventstream/EventBatchUploader$Result;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\u0008f\u0018\u0000*\u0004\u0008\u0000\u0010\u00012\u00020\u0002:\u0001\u0007J\u0016\u0010\u0003\u001a\u00020\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/eventstream/EventBatchUploader;",
        "T",
        "",
        "upload",
        "Lcom/squareup/eventstream/EventBatchUploader$Result;",
        "events",
        "",
        "Result",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract upload(Ljava/util/List;)Lcom/squareup/eventstream/EventBatchUploader$Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+TT;>;)",
            "Lcom/squareup/eventstream/EventBatchUploader$Result;"
        }
    .end annotation
.end method
