.class public final Lcom/squareup/eventstream/v2/EventstreamV2$Builder;
.super Lcom/squareup/eventstream/BaseEventstream$Builder;
.source "EventstreamV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/eventstream/v2/EventstreamV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/eventstream/BaseEventstream$Builder<",
        "Lcom/squareup/eventstream/v2/AppEvent;",
        "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
        "Lcom/squareup/eventstream/v2/EventstreamV2$AppState;",
        "Lcom/squareup/eventstream/v2/EventstreamV2;",
        "Lcom/squareup/eventstream/v2/Es2JsonSerializer;",
        "Lcom/squareup/eventstream/v2/EventstreamV2$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Lcom/squareup/eventstream/v2/Es2JsonSerializer;Lcom/squareup/eventstream/EventBatchUploader;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/squareup/eventstream/BaseEventstream$BuildType;",
            "Lcom/squareup/eventstream/v2/Es2JsonSerializer;",
            "Lcom/squareup/eventstream/EventBatchUploader<",
            "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
            ">;)V"
        }
    .end annotation

    const-string v6, "eventstreamv2.queue"

    const-string v7, "ES2"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 44
    invoke-direct/range {v0 .. v7}, Lcom/squareup/eventstream/BaseEventstream$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Ljava/lang/Object;Lcom/squareup/eventstream/EventBatchUploader;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected createConverter()Lcom/squareup/tape/FileObjectQueue$Converter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
            ">;"
        }
    .end annotation

    .line 55
    new-instance v0, Lcom/squareup/eventstream/v2/WireEventConverter;

    invoke-direct {v0}, Lcom/squareup/eventstream/v2/WireEventConverter;-><init>()V

    return-object v0
.end method

.method protected createDroppedEventsFactory(Lcom/squareup/eventstream/EventFactory;)Lcom/squareup/eventstream/DroppedEventsFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/eventstream/EventFactory<",
            "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
            "Lcom/squareup/eventstream/v2/AppEvent;",
            "Lcom/squareup/eventstream/v2/EventstreamV2$AppState;",
            ">;)",
            "Lcom/squareup/eventstream/DroppedEventsFactory<",
            "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
            ">;"
        }
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/eventstream/v2/ES2DroppedEventsFactory;

    invoke-direct {v0, p1}, Lcom/squareup/eventstream/v2/ES2DroppedEventsFactory;-><init>(Lcom/squareup/eventstream/EventFactory;)V

    return-object v0
.end method

.method protected createEventFactory(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Lcom/squareup/eventstream/v2/Es2JsonSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/EventFactory;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/squareup/eventstream/BaseEventstream$BuildType;",
            "Lcom/squareup/eventstream/v2/Es2JsonSerializer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/eventstream/EventFactory<",
            "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
            "Lcom/squareup/eventstream/v2/AppEvent;",
            "Lcom/squareup/eventstream/v2/EventstreamV2$AppState;",
            ">;"
        }
    .end annotation

    .line 69
    invoke-static/range {p1 .. p10}, Lcom/squareup/eventstream/v2/Es2EventFactory;->create(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Lcom/squareup/eventstream/v2/Es2JsonSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/v2/Es2EventFactory;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic createEventFactory(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/EventFactory;
    .locals 0

    .line 33
    check-cast p4, Lcom/squareup/eventstream/v2/Es2JsonSerializer;

    invoke-virtual/range {p0 .. p10}, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;->createEventFactory(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Lcom/squareup/eventstream/v2/Es2JsonSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/EventFactory;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic createEventStream(Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/EventFactory;Lcom/squareup/eventstream/EventStore;)Lcom/squareup/eventstream/BaseEventstream;
    .locals 0

    .line 33
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;->createEventStream(Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/EventFactory;Lcom/squareup/eventstream/EventStore;)Lcom/squareup/eventstream/v2/EventstreamV2;

    move-result-object p1

    return-object p1
.end method

.method protected createEventStream(Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/EventFactory;Lcom/squareup/eventstream/EventStore;)Lcom/squareup/eventstream/v2/EventstreamV2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/squareup/eventstream/EventFactory<",
            "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
            "Lcom/squareup/eventstream/v2/AppEvent;",
            "Lcom/squareup/eventstream/v2/EventstreamV2$AppState;",
            ">;",
            "Lcom/squareup/eventstream/EventStore<",
            "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
            ">;)",
            "Lcom/squareup/eventstream/v2/EventstreamV2;"
        }
    .end annotation

    .line 88
    new-instance v0, Lcom/squareup/eventstream/v2/EventstreamV2;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/eventstream/v2/EventstreamV2;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/EventFactory;Lcom/squareup/eventstream/EventStore;)V

    return-object v0
.end method
