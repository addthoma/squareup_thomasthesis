.class public Lcom/squareup/eventstream/v2/catalog/SessionCatalog;
.super Ljava/lang/Object;
.source "SessionCatalog.java"


# instance fields
.field private session_start_time_msec:Ljava/lang/Long;

.field private session_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setStartTimeMillis(J)Lcom/squareup/eventstream/v2/catalog/SessionCatalog;
    .locals 0

    .line 16
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/SessionCatalog;->session_start_time_msec:Ljava/lang/Long;

    return-object p0
.end method

.method public setToken(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/SessionCatalog;
    .locals 0

    .line 11
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/SessionCatalog;->session_token:Ljava/lang/String;

    return-object p0
.end method
