.class public final Lcom/squareup/glyph/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/glyph/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final SquareGlyphView:[I

.field public static final SquareGlyphView_android_textColor:I = 0x0

.field public static final SquareGlyphView_glyph:I = 0x1

.field public static final SquareGlyphView_glyphFontSizeOverride:I = 0x2

.field public static final SquareGlyphView_glyphShadowColor:I = 0x3

.field public static final SquareGlyphView_glyphShadowDx:I = 0x4

.field public static final SquareGlyphView_glyphShadowDy:I = 0x5

.field public static final SquareGlyphView_glyphShadowRadius:I = 0x6


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x7

    new-array v0, v0, [I

    .line 302
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/glyph/R$styleable;->SquareGlyphView:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1010098
        0x7f0401a0
        0x7f0401a2
        0x7f0401a5
        0x7f0401a6
        0x7f0401a7
        0x7f0401a8
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
