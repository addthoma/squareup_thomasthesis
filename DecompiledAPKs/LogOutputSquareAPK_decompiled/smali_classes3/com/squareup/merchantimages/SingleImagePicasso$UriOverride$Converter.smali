.class final Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride$Converter;
.super Ljava/lang/Object;
.source "SingleImagePicasso.java"

# interfaces
.implements Lcom/f2prateek/rx/preferences2/Preference$Converter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Converter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/f2prateek/rx/preferences2/Preference$Converter<",
        "Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;",
        ">;"
    }
.end annotation


# instance fields
.field private final gson:Lcom/google/gson/Gson;


# direct methods
.method constructor <init>(Lcom/google/gson/Gson;)V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride$Converter;->gson:Lcom/google/gson/Gson;

    return-void
.end method


# virtual methods
.method public deserialize(Ljava/lang/String;)Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride$Converter;->gson:Lcom/google/gson/Gson;

    const-class v1, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;

    return-object p1
.end method

.method public bridge synthetic deserialize(Ljava/lang/String;)Ljava/lang/Object;
    .locals 0

    .line 60
    invoke-virtual {p0, p1}, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride$Converter;->deserialize(Ljava/lang/String;)Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;

    move-result-object p1

    return-object p1
.end method

.method public serialize(Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;)Ljava/lang/String;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride$Converter;->gson:Lcom/google/gson/Gson;

    invoke-virtual {v0, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;

    invoke-virtual {p0, p1}, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride$Converter;->serialize(Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
