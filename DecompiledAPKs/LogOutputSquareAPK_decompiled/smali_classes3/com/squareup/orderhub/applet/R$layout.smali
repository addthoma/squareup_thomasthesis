.class public final Lcom/squareup/orderhub/applet/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderhub/applet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final orderhub_add_note_view:I = 0x7f0d03f2

.field public static final orderhub_add_tracking_view:I = 0x7f0d03f3

.field public static final orderhub_bill_history_view:I = 0x7f0d03f5

.field public static final orderhub_detail_filter_header_row:I = 0x7f0d03f6

.field public static final orderhub_detail_filter_row:I = 0x7f0d03f7

.field public static final orderhub_detail_header_row:I = 0x7f0d03f8

.field public static final orderhub_detail_order_row:I = 0x7f0d03f9

.field public static final orderhub_detail_view:I = 0x7f0d03fa

.field public static final orderhub_item_selection_view:I = 0x7f0d03fb

.field public static final orderhub_mark_shipped_view:I = 0x7f0d03fc

.field public static final orderhub_master_header_row:I = 0x7f0d03fd

.field public static final orderhub_master_row:I = 0x7f0d03fe

.field public static final orderhub_master_view:I = 0x7f0d03ff

.field public static final orderhub_order_adjust_pickup_time:I = 0x7f0d0400

.field public static final orderhub_order_buttons_view:I = 0x7f0d0401

.field public static final orderhub_order_cancelation_processing_view:I = 0x7f0d0402

.field public static final orderhub_order_cancelation_reason_view:I = 0x7f0d0403

.field public static final orderhub_order_customer_view:I = 0x7f0d0404

.field public static final orderhub_order_fulfillment_header_view:I = 0x7f0d0405

.field public static final orderhub_order_fulfillment_recipient_address_view:I = 0x7f0d0406

.field public static final orderhub_order_fulfillment_time_view:I = 0x7f0d0407

.field public static final orderhub_order_fulfillment_view:I = 0x7f0d0408

.field public static final orderhub_order_fulfillments_section_view:I = 0x7f0d0409

.field public static final orderhub_order_id_view:I = 0x7f0d040a

.field public static final orderhub_order_item_view:I = 0x7f0d040b

.field public static final orderhub_order_note_view:I = 0x7f0d040c

.field public static final orderhub_order_price_view:I = 0x7f0d040d

.field public static final orderhub_order_secondary_button:I = 0x7f0d040e

.field public static final orderhub_order_shipment_method_view:I = 0x7f0d040f

.field public static final orderhub_order_status_view:I = 0x7f0d0410

.field public static final orderhub_order_tender_row_view:I = 0x7f0d0411

.field public static final orderhub_order_tender_view:I = 0x7f0d0412

.field public static final orderhub_order_tracking_view:I = 0x7f0d0413

.field public static final orderhub_order_view:I = 0x7f0d0414

.field public static final orderhub_recycler_title_row:I = 0x7f0d0417


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
