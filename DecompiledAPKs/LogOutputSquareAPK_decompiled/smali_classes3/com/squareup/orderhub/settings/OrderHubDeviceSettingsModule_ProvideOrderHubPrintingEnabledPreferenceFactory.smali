.class public final Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule_ProvideOrderHubPrintingEnabledPreferenceFactory;
.super Ljava/lang/Object;
.source "OrderHubDeviceSettingsModule_ProvideOrderHubPrintingEnabledPreferenceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/f2prateek/rx/preferences2/Preference<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final prefsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule_ProvideOrderHubPrintingEnabledPreferenceFactory;->prefsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule_ProvideOrderHubPrintingEnabledPreferenceFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;)",
            "Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule_ProvideOrderHubPrintingEnabledPreferenceFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule_ProvideOrderHubPrintingEnabledPreferenceFactory;

    invoke-direct {v0, p0}, Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule_ProvideOrderHubPrintingEnabledPreferenceFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideOrderHubPrintingEnabledPreference(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 38
    invoke-static {p0}, Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule;->provideOrderHubPrintingEnabledPreference(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule_ProvideOrderHubPrintingEnabledPreferenceFactory;->prefsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    invoke-static {v0}, Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule_ProvideOrderHubPrintingEnabledPreferenceFactory;->provideOrderHubPrintingEnabledPreference(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule_ProvideOrderHubPrintingEnabledPreferenceFactory;->get()Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method
