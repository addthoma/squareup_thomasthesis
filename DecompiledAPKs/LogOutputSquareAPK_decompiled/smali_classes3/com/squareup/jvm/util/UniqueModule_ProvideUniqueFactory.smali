.class public final Lcom/squareup/jvm/util/UniqueModule_ProvideUniqueFactory;
.super Ljava/lang/Object;
.source "UniqueModule_ProvideUniqueFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jvm/util/UniqueModule_ProvideUniqueFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/util/Unique;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/jvm/util/UniqueModule_ProvideUniqueFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/jvm/util/UniqueModule_ProvideUniqueFactory$InstanceHolder;->access$000()Lcom/squareup/jvm/util/UniqueModule_ProvideUniqueFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideUnique()Lcom/squareup/util/Unique;
    .locals 2

    .line 27
    sget-object v0, Lcom/squareup/jvm/util/UniqueModule;->INSTANCE:Lcom/squareup/jvm/util/UniqueModule;

    invoke-virtual {v0}, Lcom/squareup/jvm/util/UniqueModule;->provideUnique()Lcom/squareup/util/Unique;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Unique;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/util/Unique;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/jvm/util/UniqueModule_ProvideUniqueFactory;->provideUnique()Lcom/squareup/util/Unique;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/jvm/util/UniqueModule_ProvideUniqueFactory;->get()Lcom/squareup/util/Unique;

    move-result-object v0

    return-object v0
.end method
