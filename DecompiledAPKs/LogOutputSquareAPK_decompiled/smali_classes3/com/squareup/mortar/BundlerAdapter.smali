.class public Lcom/squareup/mortar/BundlerAdapter;
.super Ljava/lang/Object;
.source "BundlerAdapter.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# instance fields
.field private final bundleKey:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/mortar/BundlerAdapter;->bundleKey:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/mortar/BundlerAdapter;->bundleKey:Ljava/lang/String;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method
