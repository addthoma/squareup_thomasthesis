.class Lcom/squareup/mortar/AppContextWrapper$ExtraAppContextWrapper;
.super Landroid/content/ContextWrapper;
.source "AppContextWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mortar/AppContextWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExtraAppContextWrapper"
.end annotation


# instance fields
.field private final application:Landroid/app/Application;

.field final synthetic this$0:Lcom/squareup/mortar/AppContextWrapper;


# direct methods
.method constructor <init>(Lcom/squareup/mortar/AppContextWrapper;Landroid/app/Application;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/mortar/AppContextWrapper$ExtraAppContextWrapper;->this$0:Lcom/squareup/mortar/AppContextWrapper;

    .line 119
    invoke-direct {p0, p2}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 120
    iput-object p2, p0, Lcom/squareup/mortar/AppContextWrapper$ExtraAppContextWrapper;->application:Landroid/app/Application;

    return-void
.end method


# virtual methods
.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/mortar/AppContextWrapper$ExtraAppContextWrapper;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/mortar/AppContextWrapper$ExtraAppContextWrapper;->this$0:Lcom/squareup/mortar/AppContextWrapper;

    invoke-virtual {v0, p1}, Lcom/squareup/mortar/AppContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
