.class public final Lcom/squareup/loggedout/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loggedout/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accept_legal:I = 0x7f0a00fe

.field public static final accept_legal_parent:I = 0x7f0a00ff

.field public static final call_to_action:I = 0x7f0a0297

.field public static final confirm_email:I = 0x7f0a038c

.field public static final country_picker:I = 0x7f0a03b4

.field public static final create_account:I = 0x7f0a03bf

.field public static final create_account_account:I = 0x7f0a03c0

.field public static final create_account_action_bar:I = 0x7f0a03c1

.field public static final create_account_container:I = 0x7f0a03c2

.field public static final email:I = 0x7f0a06b2

.field public static final email_suggestion_box:I = 0x7f0a06cd

.field public static final header:I = 0x7f0a07c8

.field public static final learn_container:I = 0x7f0a0918

.field public static final learn_parent:I = 0x7f0a091e

.field public static final learn_space:I = 0x7f0a091f

.field public static final legal:I = 0x7f0a0926

.field public static final logo_image:I = 0x7f0a0962

.field public static final page_indicator:I = 0x7f0a0b83

.field public static final password:I = 0x7f0a0bd2

.field public static final reader:I = 0x7f0a0ce4

.field public static final sign_in:I = 0x7f0a0e84

.field public static final splash_image_accept_payments:I = 0x7f0a0ec2

.field public static final splash_image_build_trust:I = 0x7f0a0ec3

.field public static final splash_image_reports:I = 0x7f0a0ec4

.field public static final splash_image_sell_in_minutes:I = 0x7f0a0ec5

.field public static final splash_image_square_logo:I = 0x7f0a0ec6

.field public static final splash_page_icon:I = 0x7f0a0ec7

.field public static final splash_page_image:I = 0x7f0a0ec8

.field public static final splash_page_text:I = 0x7f0a0ec9

.field public static final splash_pager:I = 0x7f0a0eca

.field public static final splash_screen_gradient:I = 0x7f0a0ecb

.field public static final splash_view:I = 0x7f0a0ecc

.field public static final sticky_bar:I = 0x7f0a0f3a

.field public static final tour_call_to_action:I = 0x7f0a105b

.field public static final tour_close_button:I = 0x7f0a105c

.field public static final tour_next_button:I = 0x7f0a105f

.field public static final tour_pager:I = 0x7f0a1060


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
