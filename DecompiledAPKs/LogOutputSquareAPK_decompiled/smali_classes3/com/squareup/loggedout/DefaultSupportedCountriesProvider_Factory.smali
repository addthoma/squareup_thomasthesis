.class public final Lcom/squareup/loggedout/DefaultSupportedCountriesProvider_Factory;
.super Ljava/lang/Object;
.source "DefaultSupportedCountriesProvider_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;",
        ">;"
    }
.end annotation


# instance fields
.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/loggedout/DefaultSupportedCountriesProvider_Factory;->resourcesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/loggedout/DefaultSupportedCountriesProvider_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;)",
            "Lcom/squareup/loggedout/DefaultSupportedCountriesProvider_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/loggedout/DefaultSupportedCountriesProvider_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/loggedout/DefaultSupportedCountriesProvider_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/content/res/Resources;)Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;

    invoke-direct {v0, p0}, Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;-><init>(Landroid/content/res/Resources;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/loggedout/DefaultSupportedCountriesProvider_Factory;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/squareup/loggedout/DefaultSupportedCountriesProvider_Factory;->newInstance(Landroid/content/res/Resources;)Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/loggedout/DefaultSupportedCountriesProvider_Factory;->get()Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;

    move-result-object v0

    return-object v0
.end method
