.class public final Lcom/squareup/deeplinks/DeepLinkResult;
.super Ljava/lang/Object;
.source "DeepLinkResult.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u000f\u0008\u0016\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0008J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u000e\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u001f\u0010\u000f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u0006\u0010\u0013\u001a\u00020\u0011J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/deeplinks/DeepLinkResult;",
        "",
        "status",
        "Lcom/squareup/deeplinks/DeepLinkStatus;",
        "(Lcom/squareup/deeplinks/DeepLinkStatus;)V",
        "factory",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "(Lcom/squareup/ui/main/HistoryFactory;)V",
        "(Lcom/squareup/deeplinks/DeepLinkStatus;Lcom/squareup/ui/main/HistoryFactory;)V",
        "getFactory",
        "()Lcom/squareup/ui/main/HistoryFactory;",
        "getStatus",
        "()Lcom/squareup/deeplinks/DeepLinkStatus;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hasTarget",
        "hashCode",
        "",
        "toString",
        "",
        "deeplinks_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final factory:Lcom/squareup/ui/main/HistoryFactory;

.field private final status:Lcom/squareup/deeplinks/DeepLinkStatus;


# direct methods
.method public constructor <init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V
    .locals 1

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 11
    invoke-direct {p0, p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;Lcom/squareup/ui/main/HistoryFactory;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/deeplinks/DeepLinkStatus;Lcom/squareup/ui/main/HistoryFactory;)V
    .locals 1

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/deeplinks/DeepLinkResult;->status:Lcom/squareup/deeplinks/DeepLinkStatus;

    iput-object p2, p0, Lcom/squareup/deeplinks/DeepLinkResult;->factory:Lcom/squareup/ui/main/HistoryFactory;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/HistoryFactory;)V
    .locals 1

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->KNOWN:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p0, v0, p1}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;Lcom/squareup/ui/main/HistoryFactory;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/deeplinks/DeepLinkResult;Lcom/squareup/deeplinks/DeepLinkStatus;Lcom/squareup/ui/main/HistoryFactory;ILjava/lang/Object;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/deeplinks/DeepLinkResult;->status:Lcom/squareup/deeplinks/DeepLinkStatus;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/deeplinks/DeepLinkResult;->factory:Lcom/squareup/ui/main/HistoryFactory;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/deeplinks/DeepLinkResult;->copy(Lcom/squareup/deeplinks/DeepLinkStatus;Lcom/squareup/ui/main/HistoryFactory;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/deeplinks/DeepLinkStatus;
    .locals 1

    iget-object v0, p0, Lcom/squareup/deeplinks/DeepLinkResult;->status:Lcom/squareup/deeplinks/DeepLinkStatus;

    return-object v0
.end method

.method public final component2()Lcom/squareup/ui/main/HistoryFactory;
    .locals 1

    iget-object v0, p0, Lcom/squareup/deeplinks/DeepLinkResult;->factory:Lcom/squareup/ui/main/HistoryFactory;

    return-object v0
.end method

.method public final copy(Lcom/squareup/deeplinks/DeepLinkStatus;Lcom/squareup/ui/main/HistoryFactory;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 1

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/deeplinks/DeepLinkResult;

    invoke-direct {v0, p1, p2}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;Lcom/squareup/ui/main/HistoryFactory;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/deeplinks/DeepLinkResult;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/deeplinks/DeepLinkResult;

    iget-object v0, p0, Lcom/squareup/deeplinks/DeepLinkResult;->status:Lcom/squareup/deeplinks/DeepLinkStatus;

    iget-object v1, p1, Lcom/squareup/deeplinks/DeepLinkResult;->status:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/deeplinks/DeepLinkResult;->factory:Lcom/squareup/ui/main/HistoryFactory;

    iget-object p1, p1, Lcom/squareup/deeplinks/DeepLinkResult;->factory:Lcom/squareup/ui/main/HistoryFactory;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFactory()Lcom/squareup/ui/main/HistoryFactory;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/deeplinks/DeepLinkResult;->factory:Lcom/squareup/ui/main/HistoryFactory;

    return-object v0
.end method

.method public final getStatus()Lcom/squareup/deeplinks/DeepLinkStatus;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/deeplinks/DeepLinkResult;->status:Lcom/squareup/deeplinks/DeepLinkStatus;

    return-object v0
.end method

.method public final hasTarget()Z
    .locals 2

    .line 17
    iget-object v0, p0, Lcom/squareup/deeplinks/DeepLinkResult;->status:Lcom/squareup/deeplinks/DeepLinkStatus;

    sget-object v1, Lcom/squareup/deeplinks/DeepLinkStatus;->KNOWN:Lcom/squareup/deeplinks/DeepLinkStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/deeplinks/DeepLinkResult;->status:Lcom/squareup/deeplinks/DeepLinkStatus;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/deeplinks/DeepLinkResult;->factory:Lcom/squareup/ui/main/HistoryFactory;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DeepLinkResult(status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/deeplinks/DeepLinkResult;->status:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", factory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/deeplinks/DeepLinkResult;->factory:Lcom/squareup/ui/main/HistoryFactory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
