.class public abstract Lcom/squareup/opentickets/TicketsScheduler;
.super Ljava/lang/Object;
.source "TicketsScheduler.java"


# instance fields
.field protected callback:Lcom/squareup/ui/ticket/TicketsListSchedulerListener;

.field protected final localTickets:Lcom/squareup/tickets/Tickets$InternalTickets;

.field final mainThread:Lcom/squareup/thread/executor/MainThread;

.field protected running:Z

.field private final syncOpenTickets:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/Tickets$InternalTickets;)V
    .locals 1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 19
    iput-boolean v0, p0, Lcom/squareup/opentickets/TicketsScheduler;->running:Z

    .line 22
    new-instance v0, Lcom/squareup/opentickets/-$$Lambda$TicketsScheduler$odhoINPWE-Z71RQzTeE_0PT5LJs;

    invoke-direct {v0, p0}, Lcom/squareup/opentickets/-$$Lambda$TicketsScheduler$odhoINPWE-Z71RQzTeE_0PT5LJs;-><init>(Lcom/squareup/opentickets/TicketsScheduler;)V

    iput-object v0, p0, Lcom/squareup/opentickets/TicketsScheduler;->syncOpenTickets:Ljava/lang/Runnable;

    .line 29
    iput-object p1, p0, Lcom/squareup/opentickets/TicketsScheduler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 30
    iput-object p2, p0, Lcom/squareup/opentickets/TicketsScheduler;->localTickets:Lcom/squareup/tickets/Tickets$InternalTickets;

    return-void
.end method


# virtual methods
.method protected abstract doSync()V
.end method

.method protected abstract getInterval()J
.end method

.method public synthetic lambda$new$0$TicketsScheduler()V
    .locals 2

    const-string v0, "Sync not scheduled on the main thread!"

    .line 23
    invoke-static {v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread(Ljava/lang/String;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "doSync starting..."

    .line 24
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    invoke-virtual {p0}, Lcom/squareup/opentickets/TicketsScheduler;->doSync()V

    return-void
.end method

.method protected onSyncFinished(Z)V
    .locals 3

    .line 86
    iget-object v0, p0, Lcom/squareup/opentickets/TicketsScheduler;->callback:Lcom/squareup/ui/ticket/TicketsListSchedulerListener;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "Firing callback."

    .line 87
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/opentickets/TicketsScheduler;->callback:Lcom/squareup/ui/ticket/TicketsListSchedulerListener;

    invoke-interface {v0, p1}, Lcom/squareup/ui/ticket/TicketsListSchedulerListener;->onUpdate(Z)V

    .line 92
    :cond_0
    iget-boolean p1, p0, Lcom/squareup/opentickets/TicketsScheduler;->running:Z

    if-eqz p1, :cond_1

    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "Scheduling next sync..."

    .line 93
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    invoke-virtual {p0}, Lcom/squareup/opentickets/TicketsScheduler;->schedulePeriodicSync()V

    :cond_1
    return-void
.end method

.method public schedulePeriodicSync()V
    .locals 5

    const/4 v0, 0x1

    .line 65
    iput-boolean v0, p0, Lcom/squareup/opentickets/TicketsScheduler;->running:Z

    .line 66
    invoke-virtual {p0}, Lcom/squareup/opentickets/TicketsScheduler;->getInterval()J

    move-result-wide v1

    new-array v0, v0, [Ljava/lang/Object;

    const-wide/16 v3, 0x3e8

    .line 67
    div-long v3, v1, v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v0, v4

    const-string v3, "Sync scheduled in %d seconds"

    invoke-static {v3, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/opentickets/TicketsScheduler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v3, p0, Lcom/squareup/opentickets/TicketsScheduler;->syncOpenTickets:Ljava/lang/Runnable;

    invoke-interface {v0, v3}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/opentickets/TicketsScheduler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v3, p0, Lcom/squareup/opentickets/TicketsScheduler;->syncOpenTickets:Ljava/lang/Runnable;

    invoke-interface {v0, v3, v1, v2}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public setCallback(Lcom/squareup/ui/ticket/TicketsListSchedulerListener;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Callback is %s"

    .line 38
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    iput-object p1, p0, Lcom/squareup/opentickets/TicketsScheduler;->callback:Lcom/squareup/ui/ticket/TicketsListSchedulerListener;

    return-void
.end method

.method public startPeriodicSync()V
    .locals 2

    const/4 v0, 0x1

    .line 54
    iput-boolean v0, p0, Lcom/squareup/opentickets/TicketsScheduler;->running:Z

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Starting syncing with immediate sync."

    .line 55
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/opentickets/TicketsScheduler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/opentickets/TicketsScheduler;->syncOpenTickets:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/opentickets/TicketsScheduler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/opentickets/TicketsScheduler;->syncOpenTickets:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public stopSyncing()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 73
    iget-boolean v1, p0, Lcom/squareup/opentickets/TicketsScheduler;->running:Z

    if-eqz v1, :cond_0

    const-string v1, "running"

    goto :goto_0

    :cond_0
    const-string v1, "not running"

    :goto_0
    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Stopping ticket syncing that was currently %s."

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    iput-boolean v2, p0, Lcom/squareup/opentickets/TicketsScheduler;->running:Z

    const/4 v0, 0x0

    .line 76
    iput-object v0, p0, Lcom/squareup/opentickets/TicketsScheduler;->callback:Lcom/squareup/ui/ticket/TicketsListSchedulerListener;

    .line 77
    iget-object v0, p0, Lcom/squareup/opentickets/TicketsScheduler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/opentickets/TicketsScheduler;->syncOpenTickets:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method public syncOnce()V
    .locals 2

    const-string v0, "Sync not performed on the main thread!"

    .line 44
    invoke-static {v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread(Ljava/lang/String;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Starting to syncOnce..."

    .line 45
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    invoke-virtual {p0}, Lcom/squareup/opentickets/TicketsScheduler;->doSync()V

    return-void
.end method
