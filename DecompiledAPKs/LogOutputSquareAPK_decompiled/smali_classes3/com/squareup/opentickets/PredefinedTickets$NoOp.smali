.class public final Lcom/squareup/opentickets/PredefinedTickets$NoOp;
.super Ljava/lang/Object;
.source "PredefinedTickets.kt"

# interfaces
.implements Lcom/squareup/opentickets/PredefinedTickets;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/opentickets/PredefinedTickets;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoOp"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPredefinedTickets.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PredefinedTickets.kt\ncom/squareup/opentickets/PredefinedTickets$NoOp\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,46:1\n151#2,2:47\n*E\n*S KotlinDebug\n*F\n+ 1 PredefinedTickets.kt\ncom/squareup/opentickets/PredefinedTickets$NoOp\n*L\n44#1,2:47\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u00050\u0004H\u0096\u0001J\u0015\u0010\u0007\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00050\u0004H\u0096\u0001J\u001d\u0010\t\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u00050\u00042\u0006\u0010\n\u001a\u00020\u000bH\u0096\u0001J\u0019\u0010\u000c\u001a\u00020\r2\u000e\u0010\u000e\u001a\n \u0010*\u0004\u0018\u00010\u000f0\u000fH\u0096\u0001J\t\u0010\u0011\u001a\u00020\rH\u0096\u0001J\u0011\u0010\u0012\u001a\u00020\r2\u0006\u0010\n\u001a\u00020\u000bH\u0096\u0001\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/opentickets/PredefinedTickets$NoOp;",
        "Lcom/squareup/opentickets/PredefinedTickets;",
        "()V",
        "getAllAvailableTicketTemplates",
        "Lio/reactivex/Single;",
        "",
        "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
        "getAllTicketGroups",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "getAvailableTicketTemplatesForGroup",
        "ticketGroupId",
        "",
        "onEnterScope",
        "",
        "p0",
        "Lmortar/MortarScope;",
        "kotlin.jvm.PlatformType",
        "onExitScope",
        "updateAvailableTemplateCountForGroup",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/opentickets/PredefinedTickets$NoOp;


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/opentickets/PredefinedTickets;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/opentickets/PredefinedTickets$NoOp;

    invoke-direct {v0}, Lcom/squareup/opentickets/PredefinedTickets$NoOp;-><init>()V

    sput-object v0, Lcom/squareup/opentickets/PredefinedTickets$NoOp;->INSTANCE:Lcom/squareup/opentickets/PredefinedTickets$NoOp;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 47
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 48
    const-class v2, Lcom/squareup/opentickets/PredefinedTickets;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/opentickets/PredefinedTickets;

    iput-object v0, p0, Lcom/squareup/opentickets/PredefinedTickets$NoOp;->$$delegate_0:Lcom/squareup/opentickets/PredefinedTickets;

    return-void
.end method


# virtual methods
.method public getAllAvailableTicketTemplates()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/opentickets/PredefinedTickets$NoOp;->$$delegate_0:Lcom/squareup/opentickets/PredefinedTickets;

    invoke-interface {v0}, Lcom/squareup/opentickets/PredefinedTickets;->getAllAvailableTicketTemplates()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public getAllTicketGroups()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/opentickets/PredefinedTickets$NoOp;->$$delegate_0:Lcom/squareup/opentickets/PredefinedTickets;

    invoke-interface {v0}, Lcom/squareup/opentickets/PredefinedTickets;->getAllTicketGroups()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public getAvailableTicketTemplatesForGroup(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;>;"
        }
    .end annotation

    const-string/jumbo v0, "ticketGroupId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/opentickets/PredefinedTickets$NoOp;->$$delegate_0:Lcom/squareup/opentickets/PredefinedTickets;

    invoke-interface {v0, p1}, Lcom/squareup/opentickets/PredefinedTickets;->getAvailableTicketTemplatesForGroup(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/opentickets/PredefinedTickets$NoOp;->$$delegate_0:Lcom/squareup/opentickets/PredefinedTickets;

    invoke-interface {v0, p1}, Lcom/squareup/opentickets/PredefinedTickets;->onEnterScope(Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/opentickets/PredefinedTickets$NoOp;->$$delegate_0:Lcom/squareup/opentickets/PredefinedTickets;

    invoke-interface {v0}, Lcom/squareup/opentickets/PredefinedTickets;->onExitScope()V

    return-void
.end method

.method public updateAvailableTemplateCountForGroup(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "ticketGroupId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/opentickets/PredefinedTickets$NoOp;->$$delegate_0:Lcom/squareup/opentickets/PredefinedTickets;

    invoke-interface {v0, p1}, Lcom/squareup/opentickets/PredefinedTickets;->updateAvailableTemplateCountForGroup(Ljava/lang/String;)V

    return-void
.end method
