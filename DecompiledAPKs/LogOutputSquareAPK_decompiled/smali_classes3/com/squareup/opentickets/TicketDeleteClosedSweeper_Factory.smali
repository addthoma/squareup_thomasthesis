.class public final Lcom/squareup/opentickets/TicketDeleteClosedSweeper_Factory;
.super Ljava/lang/Object;
.source "TicketDeleteClosedSweeper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/opentickets/TicketDeleteClosedSweeper;",
        ">;"
    }
.end annotation


# instance fields
.field private final localTicketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets$InternalTickets;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets$InternalTickets;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/opentickets/TicketDeleteClosedSweeper_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/opentickets/TicketDeleteClosedSweeper_Factory;->localTicketsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/opentickets/TicketDeleteClosedSweeper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets$InternalTickets;",
            ">;)",
            "Lcom/squareup/opentickets/TicketDeleteClosedSweeper_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/opentickets/TicketDeleteClosedSweeper_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/opentickets/TicketDeleteClosedSweeper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/Tickets$InternalTickets;)Lcom/squareup/opentickets/TicketDeleteClosedSweeper;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/opentickets/TicketDeleteClosedSweeper;

    invoke-direct {v0, p0, p1}, Lcom/squareup/opentickets/TicketDeleteClosedSweeper;-><init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/Tickets$InternalTickets;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/opentickets/TicketDeleteClosedSweeper;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/opentickets/TicketDeleteClosedSweeper_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/opentickets/TicketDeleteClosedSweeper_Factory;->localTicketsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tickets/Tickets$InternalTickets;

    invoke-static {v0, v1}, Lcom/squareup/opentickets/TicketDeleteClosedSweeper_Factory;->newInstance(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/Tickets$InternalTickets;)Lcom/squareup/opentickets/TicketDeleteClosedSweeper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/opentickets/TicketDeleteClosedSweeper_Factory;->get()Lcom/squareup/opentickets/TicketDeleteClosedSweeper;

    move-result-object v0

    return-object v0
.end method
