.class public Lcom/squareup/opentickets/TicketsSyncSweeper;
.super Lcom/squareup/opentickets/TicketsScheduler;
.source "TicketsSyncSweeper.java"


# static fields
.field private static final TICKET_SWEEP_INTERVAL_MS:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 16
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x5

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/opentickets/TicketsSyncSweeper;->TICKET_SWEEP_INTERVAL_MS:J

    return-void
.end method

.method public constructor <init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/Tickets$InternalTickets;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/opentickets/TicketsScheduler;-><init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/Tickets$InternalTickets;)V

    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Hitting local store for non-zero tickets for update."

    .line 27
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    iget-object v0, p0, Lcom/squareup/opentickets/TicketsSyncSweeper;->localTickets:Lcom/squareup/tickets/Tickets$InternalTickets;

    invoke-interface {v0}, Lcom/squareup/tickets/Tickets$InternalTickets;->getBroker()Lcom/squareup/tickets/TicketsBroker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketsBroker;->updateUnsyncedTickets()V

    const/4 v0, 0x1

    .line 30
    invoke-virtual {p0, v0}, Lcom/squareup/opentickets/TicketsSyncSweeper;->onSyncFinished(Z)V

    return-void
.end method

.method protected getInterval()J
    .locals 2

    .line 23
    sget-wide v0, Lcom/squareup/opentickets/TicketsSyncSweeper;->TICKET_SWEEP_INTERVAL_MS:J

    return-wide v0
.end method
