.class final Lcom/squareup/experiments/SquareCardUpsellExperiment$squareCardUpsellBehavior$1;
.super Ljava/lang/Object;
.source "SquareCardUpsellExperiment.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/experiments/SquareCardUpsellExperiment;->squareCardUpsellBehavior()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardUpsellExperiment.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardUpsellExperiment.kt\ncom/squareup/experiments/SquareCardUpsellExperiment$squareCardUpsellBehavior$1\n*L\n1#1,48:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/experiments/SquareCardUpsellExperiment$Behavior;",
        "it",
        "Lcom/squareup/server/ExperimentsResponse$Bucket;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/experiments/SquareCardUpsellExperiment$squareCardUpsellBehavior$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/experiments/SquareCardUpsellExperiment$squareCardUpsellBehavior$1;

    invoke-direct {v0}, Lcom/squareup/experiments/SquareCardUpsellExperiment$squareCardUpsellBehavior$1;-><init>()V

    sput-object v0, Lcom/squareup/experiments/SquareCardUpsellExperiment$squareCardUpsellBehavior$1;->INSTANCE:Lcom/squareup/experiments/SquareCardUpsellExperiment$squareCardUpsellBehavior$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/server/ExperimentsResponse$Bucket;)Lcom/squareup/experiments/SquareCardUpsellExperiment$Behavior;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object p1, p1, Lcom/squareup/server/ExperimentsResponse$Bucket;->name:Ljava/lang/String;

    const-string v0, "it.name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    const-string v0, "(this as java.lang.String).toUpperCase()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/experiments/SquareCardUpsellExperiment$Behavior;->valueOf(Ljava/lang/String;)Lcom/squareup/experiments/SquareCardUpsellExperiment$Behavior;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/server/ExperimentsResponse$Bucket;

    invoke-virtual {p0, p1}, Lcom/squareup/experiments/SquareCardUpsellExperiment$squareCardUpsellBehavior$1;->apply(Lcom/squareup/server/ExperimentsResponse$Bucket;)Lcom/squareup/experiments/SquareCardUpsellExperiment$Behavior;

    move-result-object p1

    return-object p1
.end method
