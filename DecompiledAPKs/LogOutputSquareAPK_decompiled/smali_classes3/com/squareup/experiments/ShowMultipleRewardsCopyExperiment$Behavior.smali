.class public final enum Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;
.super Ljava/lang/Enum;
.source "ShowMultipleRewardsCopyExperiment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Behavior"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

.field public static final enum CONTROL:Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

.field public static final enum SHOW:Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 29
    new-instance v0, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    const/4 v1, 0x0

    const-string v2, "CONTROL"

    invoke-direct {v0, v2, v1}, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;->CONTROL:Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    new-instance v0, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    const/4 v2, 0x1

    const-string v3, "SHOW"

    invoke-direct {v0, v3, v2}, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;->SHOW:Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    .line 28
    sget-object v3, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;->CONTROL:Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;->SHOW:Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;->$VALUES:[Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;
    .locals 1

    .line 28
    const-class v0, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    return-object p0
.end method

.method public static values()[Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;->$VALUES:[Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    invoke-virtual {v0}, [Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    return-object v0
.end method


# virtual methods
.method public shouldShow()Z
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;->SHOW:Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
