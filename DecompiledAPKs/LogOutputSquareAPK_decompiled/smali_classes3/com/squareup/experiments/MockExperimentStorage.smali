.class public final Lcom/squareup/experiments/MockExperimentStorage;
.super Ljava/lang/Object;
.source "MockExperimentStorage.kt"

# interfaces
.implements Lcom/squareup/experiments/ExperimentStorage;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J \u0010\u000b\u001a\u0010\u0012\u000c\u0012\n \r*\u0004\u0018\u00010\u00060\u00060\u000c2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\n\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/experiments/MockExperimentStorage;",
        "Lcom/squareup/experiments/ExperimentStorage;",
        "controlBucketName",
        "",
        "(Ljava/lang/String;)V",
        "bucket",
        "Lcom/squareup/server/ExperimentsResponse$Bucket;",
        "getBucket",
        "()Lcom/squareup/server/ExperimentsResponse$Bucket;",
        "setBucket",
        "(Lcom/squareup/server/ExperimentsResponse$Bucket;)V",
        "assignedBucket",
        "Lio/reactivex/Observable;",
        "kotlin.jvm.PlatformType",
        "experiment",
        "Lcom/squareup/experiments/ExperimentProfile;",
        "experiments_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private bucket:Lcom/squareup/server/ExperimentsResponse$Bucket;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    const-string v0, "controlBucketName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Lcom/squareup/server/ExperimentsResponse$Bucket;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, p1, v1}, Lcom/squareup/server/ExperimentsResponse$Bucket;-><init>(IILjava/lang/String;I)V

    iput-object v0, p0, Lcom/squareup/experiments/MockExperimentStorage;->bucket:Lcom/squareup/server/ExperimentsResponse$Bucket;

    return-void
.end method


# virtual methods
.method public assignedBucket(Lcom/squareup/experiments/ExperimentProfile;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/server/ExperimentsResponse$Bucket;",
            ">;"
        }
    .end annotation

    .line 13
    iget-object p1, p0, Lcom/squareup/experiments/MockExperimentStorage;->bucket:Lcom/squareup/server/ExperimentsResponse$Bucket;

    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "Observable.just(bucket)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getBucket()Lcom/squareup/server/ExperimentsResponse$Bucket;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/experiments/MockExperimentStorage;->bucket:Lcom/squareup/server/ExperimentsResponse$Bucket;

    return-object v0
.end method

.method public final setBucket(Lcom/squareup/server/ExperimentsResponse$Bucket;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    iput-object p1, p0, Lcom/squareup/experiments/MockExperimentStorage;->bucket:Lcom/squareup/server/ExperimentsResponse$Bucket;

    return-void
.end method
