.class public final enum Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;
.super Ljava/lang/Enum;
.source "IposSkipOnboardingProductionExperiment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Behavior"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;",
        "",
        "(Ljava/lang/String;I)V",
        "shouldSkip",
        "",
        "CONTROL",
        "TEST",
        "experiments_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;

.field public static final enum CONTROL:Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;

.field public static final enum TEST:Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;

    new-instance v1, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;

    const/4 v2, 0x0

    const-string v3, "CONTROL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;->CONTROL:Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;

    const/4 v2, 0x1

    const-string v3, "TEST"

    invoke-direct {v1, v3, v2}, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;->TEST:Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;->$VALUES:[Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;
    .locals 1

    const-class v0, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;

    return-object p0
.end method

.method public static values()[Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;
    .locals 1

    sget-object v0, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;->$VALUES:[Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;

    invoke-virtual {v0}, [Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;

    return-object v0
.end method


# virtual methods
.method public final shouldSkip()Z
    .locals 2

    .line 26
    move-object v0, p0

    check-cast v0, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;

    sget-object v1, Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;->CONTROL:Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment$Behavior;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
