.class public Lcom/squareup/marketfont/MarketEditText;
.super Landroidx/appcompat/widget/AppCompatEditText;
.source "MarketEditText.java"


# instance fields
.field private weight:Lcom/squareup/marketfont/MarketFont$Weight;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 13
    invoke-direct {p0, p1, v0}, Lcom/squareup/marketfont/MarketEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x101006e

    .line 17
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/marketfont/MarketEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    sget-object v0, Lcom/squareup/marketfont/R$styleable;->MarketEditText:[I

    sget v1, Lcom/squareup/marketfont/R$styleable;->MarketEditText_weight:I

    invoke-static {p1, p2, v0, v1, p3}, Lcom/squareup/marketfont/MarketUtils;->getWeight(Landroid/content/Context;Landroid/util/AttributeSet;[III)Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/marketfont/MarketEditText;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 29
    iget-object p1, p0, Lcom/squareup/marketfont/MarketEditText;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p0, p1}, Lcom/squareup/marketfont/MarketUtils;->setTextViewTypeface(Landroid/widget/TextView;Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method


# virtual methods
.method public getWeight()Lcom/squareup/marketfont/MarketFont$Weight;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/marketfont/MarketEditText;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/marketfont/MarketEditText;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 48
    invoke-super {p0}, Landroidx/appcompat/widget/AppCompatEditText;->onDetachedFromWindow()V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 0

    .line 42
    invoke-static {p1, p2}, Lcom/squareup/marketfont/MarketUtils;->ensureSpannableTextType(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)Landroid/widget/TextView$BufferType;

    move-result-object p2

    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatEditText;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 0

    .line 33
    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatEditText;->setTextAppearance(Landroid/content/Context;I)V

    .line 34
    iget-object p1, p0, Lcom/squareup/marketfont/MarketEditText;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p0, p1}, Lcom/squareup/marketfont/MarketUtils;->setTextViewTypeface(Landroid/widget/TextView;Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method
