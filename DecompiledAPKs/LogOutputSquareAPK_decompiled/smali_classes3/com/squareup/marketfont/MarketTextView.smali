.class public Lcom/squareup/marketfont/MarketTextView;
.super Landroidx/appcompat/widget/AppCompatTextView;
.source "MarketTextView.java"


# instance fields
.field private weight:Lcom/squareup/marketfont/MarketFont$Weight;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 14
    invoke-direct {p0, p1, v0}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x1010084

    .line 18
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    sget-object v0, Lcom/squareup/marketfont/R$styleable;->MarketTextView:[I

    sget v1, Lcom/squareup/marketfont/R$styleable;->MarketTextView_weight:I

    invoke-static {p1, p2, v0, v1, p3}, Lcom/squareup/marketfont/MarketUtils;->getWeight(Landroid/content/Context;Landroid/util/AttributeSet;[III)Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object p1

    .line 26
    invoke-virtual {p0, p1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method


# virtual methods
.method public getWeight()Lcom/squareup/marketfont/MarketFont$Weight;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/marketfont/MarketTextView;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/marketfont/MarketTextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 52
    invoke-super {p0}, Landroidx/appcompat/widget/AppCompatTextView;->onDetachedFromWindow()V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 0

    .line 46
    invoke-static {p1, p2}, Lcom/squareup/marketfont/MarketUtils;->ensureSpannableTextType(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)Landroid/widget/TextView$BufferType;

    move-result-object p2

    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 0

    .line 37
    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 38
    iget-object p1, p0, Lcom/squareup/marketfont/MarketTextView;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p0, p1}, Lcom/squareup/marketfont/MarketUtils;->setTextViewTypeface(Landroid/widget/TextView;Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method

.method public setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/marketfont/MarketTextView;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    if-eq p1, v0, :cond_0

    .line 31
    iput-object p1, p0, Lcom/squareup/marketfont/MarketTextView;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 32
    invoke-static {p0, p1}, Lcom/squareup/marketfont/MarketUtils;->setTextViewTypeface(Landroid/widget/TextView;Lcom/squareup/marketfont/MarketFont$Weight;)V

    :cond_0
    return-void
.end method
