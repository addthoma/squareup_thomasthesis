.class public abstract Lcom/squareup/disputes/DisputesState;
.super Ljava/lang/Object;
.source "DisputesState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/DisputesState$WaitingToStart;,
        Lcom/squareup/disputes/DisputesState$OverviewState;,
        Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;,
        Lcom/squareup/disputes/DisputesState$DetailState;,
        Lcom/squareup/disputes/DisputesState$ActionDialogState;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0005\u0005\u0006\u0007\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u0082\u0001\u0005\n\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesState;",
        "",
        "()V",
        "toSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "ActionDialogState",
        "ChallengeSummaryState",
        "DetailState",
        "OverviewState",
        "WaitingToStart",
        "Lcom/squareup/disputes/DisputesState$WaitingToStart;",
        "Lcom/squareup/disputes/DisputesState$OverviewState;",
        "Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;",
        "Lcom/squareup/disputes/DisputesState$DetailState;",
        "Lcom/squareup/disputes/DisputesState$ActionDialogState;",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/disputes/DisputesState;-><init>()V

    return-void
.end method


# virtual methods
.method public final toSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/disputes/DisputesSerializer;->INSTANCE:Lcom/squareup/disputes/DisputesSerializer;

    invoke-virtual {v0, p0}, Lcom/squareup/disputes/DisputesSerializer;->snapshotState(Lcom/squareup/disputes/DisputesState;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
