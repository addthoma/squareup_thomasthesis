.class public final Lcom/squareup/disputes/AllDisputesCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "AllDisputesCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/disputes/AllDisputesCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final allDisputesAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/AllDisputesAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/AllDisputesAdapter;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/disputes/AllDisputesCoordinator_Factory_Factory;->allDisputesAdapterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/disputes/AllDisputesCoordinator_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/AllDisputesAdapter;",
            ">;)",
            "Lcom/squareup/disputes/AllDisputesCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/disputes/AllDisputesCoordinator_Factory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/disputes/AllDisputesCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;)Lcom/squareup/disputes/AllDisputesCoordinator$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/AllDisputesAdapter;",
            ">;)",
            "Lcom/squareup/disputes/AllDisputesCoordinator$Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/disputes/AllDisputesCoordinator$Factory;

    invoke-direct {v0, p0}, Lcom/squareup/disputes/AllDisputesCoordinator$Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/disputes/AllDisputesCoordinator$Factory;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator_Factory_Factory;->allDisputesAdapterProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/disputes/AllDisputesCoordinator_Factory_Factory;->newInstance(Ljavax/inject/Provider;)Lcom/squareup/disputes/AllDisputesCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/disputes/AllDisputesCoordinator_Factory_Factory;->get()Lcom/squareup/disputes/AllDisputesCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
