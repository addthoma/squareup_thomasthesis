.class public final Lcom/squareup/disputes/DisputesTutorialRunner;
.super Ljava/lang/Object;
.source "DisputesTutorialRunner.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u0007\u001a\u00020\u0008J\u0006\u0010\t\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesTutorialRunner;",
        "",
        "flow",
        "Lflow/Flow;",
        "reportsAppletGateway",
        "Lcom/squareup/reports/applet/ReportsAppletGateway;",
        "(Lflow/Flow;Lcom/squareup/reports/applet/ReportsAppletGateway;)V",
        "jumpToDisputes",
        "",
        "showPrompt",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;

.field private final reportsAppletGateway:Lcom/squareup/reports/applet/ReportsAppletGateway;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/reports/applet/ReportsAppletGateway;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reportsAppletGateway"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/DisputesTutorialRunner;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/disputes/DisputesTutorialRunner;->reportsAppletGateway:Lcom/squareup/reports/applet/ReportsAppletGateway;

    return-void
.end method


# virtual methods
.method public final jumpToDisputes()V
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/disputes/DisputesTutorialRunner;->reportsAppletGateway:Lcom/squareup/reports/applet/ReportsAppletGateway;

    invoke-interface {v0}, Lcom/squareup/reports/applet/ReportsAppletGateway;->activateDisputesReport()V

    return-void
.end method

.method public final showPrompt()V
    .locals 2

    .line 13
    iget-object v0, p0, Lcom/squareup/disputes/DisputesTutorialRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/disputes/DisputesTutorialDialogScreen;->INSTANCE:Lcom/squareup/disputes/DisputesTutorialDialogScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
