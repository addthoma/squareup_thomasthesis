.class public abstract Lcom/squareup/disputes/Field;
.super Ljava/lang/Object;
.source "Field.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/Field$Factory;,
        Lcom/squareup/disputes/Field$SelectField;,
        Lcom/squareup/disputes/Field$TextField;,
        Lcom/squareup/disputes/Field$FileField;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nField.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Field.kt\ncom/squareup/disputes/Field\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,144:1\n1529#2,3:145\n*E\n*S KotlinDebug\n*F\n+ 1 Field.kt\ncom/squareup/disputes/Field\n*L\n67#1,3:145\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0010$\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00080\u0018\u00002\u00020\u0001:\u0004#$%&BO\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u0007\u0012\u000e\u0008\u0002\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u00a2\u0006\u0002\u0010\u000eJ\u001a\u0010\u001b\u001a\u00020\u00142\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00000\u001dJ\u0008\u0010\u001e\u001a\u00020\u0014H&J\u0010\u0010\u001f\u001a\u00020\u00142\u0006\u0010 \u001a\u00020\u0007H&J\u0006\u0010!\u001a\u00020\"R\u0017\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u001a\u0010\u0013\u001a\u00020\u0014X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0015\"\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\n\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0012R\u0011\u0010\t\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0012\u0082\u0001\u0003\'()\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/disputes/Field;",
        "",
        "section",
        "Lcom/squareup/protos/client/irf/Section;",
        "fieldProto",
        "Lcom/squareup/protos/client/irf/Field;",
        "sectionHeader",
        "",
        "fieldName",
        "prompt",
        "label",
        "criteria",
        "",
        "Lcom/squareup/protos/client/irf/Criteria;",
        "(Lcom/squareup/protos/client/irf/Section;Lcom/squareup/protos/client/irf/Field;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V",
        "getCriteria",
        "()Ljava/util/List;",
        "getFieldName",
        "()Ljava/lang/String;",
        "isSkipped",
        "",
        "()Z",
        "setSkipped",
        "(Z)V",
        "getLabel",
        "getPrompt",
        "getSectionHeader",
        "canShow",
        "fieldsMap",
        "",
        "hasAnswer",
        "meetsCriteria",
        "value",
        "skip",
        "",
        "Factory",
        "FileField",
        "SelectField",
        "TextField",
        "Lcom/squareup/disputes/Field$SelectField;",
        "Lcom/squareup/disputes/Field$TextField;",
        "Lcom/squareup/disputes/Field$FileField;",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final criteria:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Criteria;",
            ">;"
        }
    .end annotation
.end field

.field private final fieldName:Ljava/lang/String;

.field private isSkipped:Z

.field private final label:Ljava/lang/String;

.field private final prompt:Ljava/lang/String;

.field private final sectionHeader:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/protos/client/irf/Section;Lcom/squareup/protos/client/irf/Field;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/irf/Section;",
            "Lcom/squareup/protos/client/irf/Field;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Criteria;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/squareup/disputes/Field;->sectionHeader:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/disputes/Field;->fieldName:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/disputes/Field;->prompt:Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/disputes/Field;->label:Ljava/lang/String;

    iput-object p7, p0, Lcom/squareup/disputes/Field;->criteria:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/protos/client/irf/Section;Lcom/squareup/protos/client/irf/Field;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 8

    move-object v2, p2

    and-int/lit8 v0, p8, 0x4

    move-object v1, p1

    if-eqz v0, :cond_0

    .line 26
    iget-object v0, v1, Lcom/squareup/protos/client/irf/Section;->header:Ljava/lang/String;

    const-string v3, "section.header"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v0

    goto :goto_0

    :cond_0
    move-object v3, p3

    :goto_0
    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_1

    .line 27
    iget-object v0, v2, Lcom/squareup/protos/client/irf/Field;->field_name:Ljava/lang/String;

    const-string v4, "fieldProto.field_name"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_1

    :cond_1
    move-object v4, p4

    :goto_1
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_2

    .line 28
    iget-object v0, v2, Lcom/squareup/protos/client/irf/Field;->prompt:Ljava/lang/String;

    const-string v5, "fieldProto.prompt"

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v0

    goto :goto_2

    :cond_2
    move-object v5, p5

    :goto_2
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_3

    .line 29
    iget-object v0, v2, Lcom/squareup/protos/client/irf/Field;->field_label:Ljava/lang/String;

    const-string v6, "fieldProto.field_label"

    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, v0

    goto :goto_3

    :cond_3
    move-object v6, p6

    :goto_3
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_4

    .line 30
    iget-object v0, v2, Lcom/squareup/protos/client/irf/Field;->visibility:Lcom/squareup/protos/client/irf/Visibility;

    iget-object v0, v0, Lcom/squareup/protos/client/irf/Visibility;->criteria:Ljava/util/List;

    const-string v7, "fieldProto.visibility.criteria"

    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v7, v0

    goto :goto_4

    :cond_4
    move-object v7, p7

    :goto_4
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/squareup/disputes/Field;-><init>(Lcom/squareup/protos/client/irf/Section;Lcom/squareup/protos/client/irf/Field;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public final canShow(Ljava/util/Map;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/disputes/Field;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "fieldsMap"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/disputes/Field;->criteria:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/squareup/disputes/Field;->criteria:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 145
    instance-of v2, v0, Ljava/util/Collection;

    if-eqz v2, :cond_1

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 146
    :cond_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/irf/Criteria;

    .line 68
    iget-object v3, v2, Lcom/squareup/protos/client/irf/Criteria;->conditional_target:Ljava/lang/String;

    const-string v4, "it.conditional_target"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v3}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/disputes/Field;

    iget-object v2, v2, Lcom/squareup/protos/client/irf/Criteria;->value:Ljava/lang/String;

    const-string v4, "it.value"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Lcom/squareup/disputes/Field;->meetsCriteria(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v1, 0x0

    :cond_3
    :goto_0
    return v1
.end method

.method public final getCriteria()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Criteria;",
            ">;"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/squareup/disputes/Field;->criteria:Ljava/util/List;

    return-object v0
.end method

.method public final getFieldName()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/disputes/Field;->fieldName:Ljava/lang/String;

    return-object v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/disputes/Field;->label:Ljava/lang/String;

    return-object v0
.end method

.method public final getPrompt()Ljava/lang/String;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/disputes/Field;->prompt:Ljava/lang/String;

    return-object v0
.end method

.method public final getSectionHeader()Ljava/lang/String;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/disputes/Field;->sectionHeader:Ljava/lang/String;

    return-object v0
.end method

.method public abstract hasAnswer()Z
.end method

.method public final isSkipped()Z
    .locals 1

    .line 54
    iget-boolean v0, p0, Lcom/squareup/disputes/Field;->isSkipped:Z

    return v0
.end method

.method public abstract meetsCriteria(Ljava/lang/String;)Z
.end method

.method public final setSkipped(Z)V
    .locals 0

    .line 54
    iput-boolean p1, p0, Lcom/squareup/disputes/Field;->isSkipped:Z

    return-void
.end method

.method public final skip()V
    .locals 1

    const/4 v0, 0x1

    .line 57
    iput-boolean v0, p0, Lcom/squareup/disputes/Field;->isSkipped:Z

    return-void
.end method
