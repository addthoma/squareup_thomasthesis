.class public final Lcom/squareup/disputes/DisputesTutorialDialogScreen$Factory;
.super Ljava/lang/Object;
.source "DisputesTutorialDialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/DisputesTutorialDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisputesTutorialDialogScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisputesTutorialDialogScreen.kt\ncom/squareup/disputes/DisputesTutorialDialogScreen$Factory\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,56:1\n52#2:57\n*E\n*S KotlinDebug\n*F\n+ 1 DisputesTutorialDialogScreen.kt\ncom/squareup/disputes/DisputesTutorialDialogScreen$Factory\n*L\n21#1:57\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0018\u0010\u0008\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\nH\u0002\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesTutorialDialogScreen$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "core",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/tutorialv2/TutorialCore;)Landroid/app/Dialog;
    .locals 2

    .line 31
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 32
    sget p1, Lcom/squareup/disputes/R$string;->disputes_notification_dialog_title:I

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 33
    sget v0, Lcom/squareup/disputes/R$string;->disputes_notification_dialog_content:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 35
    sget v0, Lcom/squareup/disputes/R$string;->disputes_notification_dialog_primary:I

    .line 36
    new-instance v1, Lcom/squareup/disputes/DisputesTutorialDialogScreen$Factory$createDialog$1;

    invoke-direct {v1, p2}, Lcom/squareup/disputes/DisputesTutorialDialogScreen$Factory$createDialog$1;-><init>(Lcom/squareup/tutorialv2/TutorialCore;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    .line 34
    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 38
    sget v0, Lcom/squareup/noho/R$drawable;->noho_selector_primary_button_background:I

    .line 37
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 41
    sget v0, Lcom/squareup/noho/R$color;->noho_color_selector_primary_button_text:I

    .line 40
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 43
    sget v0, Lcom/squareup/common/strings/R$string;->dismiss:I

    new-instance v1, Lcom/squareup/disputes/DisputesTutorialDialogScreen$Factory$createDialog$2;

    invoke-direct {v1, p2}, Lcom/squareup/disputes/DisputesTutorialDialogScreen$Factory$createDialog$2;-><init>(Lcom/squareup/tutorialv2/TutorialCore;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 48
    new-instance v0, Lcom/squareup/disputes/DisputesTutorialDialogScreen$Factory$createDialog$3;

    invoke-direct {v0, p2}, Lcom/squareup/disputes/DisputesTutorialDialogScreen$Factory$createDialog$3;-><init>(Lcom/squareup/tutorialv2/TutorialCore;)V

    check-cast v0, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026EL) }\n          .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/app/Dialog;

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    const-class v0, Lcom/squareup/register/tutorial/TutorialsComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/tutorial/TutorialsComponent;

    .line 22
    invoke-interface {v0}, Lcom/squareup/register/tutorial/TutorialsComponent;->tutorialCore()Lcom/squareup/tutorialv2/TutorialCore;

    move-result-object v0

    const-string v1, "core"

    .line 24
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/squareup/disputes/DisputesTutorialDialogScreen$Factory;->createDialog(Landroid/content/Context;Lcom/squareup/tutorialv2/TutorialCore;)Landroid/app/Dialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(createDialog(context, core))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
