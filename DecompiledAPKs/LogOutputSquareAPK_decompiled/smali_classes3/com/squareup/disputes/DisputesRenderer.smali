.class public final Lcom/squareup/disputes/DisputesRenderer;
.super Ljava/lang/Object;
.source "DisputesRenderer.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/Renderer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/Renderer<",
        "Lcom/squareup/disputes/DisputesState;",
        "Lcom/squareup/disputes/DisputesEvent;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJH\u0010\u0018\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\u0019\u001a\u00020\u00022\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J<\u0010\u001e\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0008*\u00020\u001f2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u001bH\u0002J<\u0010 \u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0008*\u00020!2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u001bH\u0002J\u0014\u0010\"\u001a\u00020#*\u00020\u00102\u0006\u0010$\u001a\u00020%H\u0002R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0018\u0010\u000e\u001a\u00020\u000f*\u00020\u00108BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0012R \u0010\u0013\u001a\n \u0015*\u0004\u0018\u00010\u00140\u0014*\u00020\u00108BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesRenderer;",
        "Lcom/squareup/workflow/legacy/Renderer;",
        "Lcom/squareup/disputes/DisputesState;",
        "Lcom/squareup/disputes/DisputesEvent;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/util/Device;)V",
        "getDevice",
        "()Lcom/squareup/util/Device;",
        "hasMore",
        "",
        "Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;",
        "getHasMore",
        "(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)Z",
        "nextCursor",
        "",
        "kotlin.jvm.PlatformType",
        "getNextCursor",
        "(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)Ljava/lang/Integer;",
        "render",
        "state",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "toAllDisputesScreen",
        "Lcom/squareup/disputes/DisputesState$OverviewState;",
        "toSummaryScreen",
        "Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;",
        "toWithData",
        "Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;",
        "moreType",
        "Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/DisputesRenderer;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method private final getHasMore(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)Z
    .locals 1

    .line 125
    invoke-direct {p0, p1}, Lcom/squareup/disputes/DisputesRenderer;->getNextCursor(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->compare(II)I

    move-result p1

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private final getNextCursor(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)Ljava/lang/Integer;
    .locals 0

    .line 128
    iget-object p1, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute_cursor:Ljava/lang/Integer;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->DEFAULT_RESOLVED_DISPUTE_CURSOR:Ljava/lang/Integer;

    :goto_0
    return-object p1
.end method

.method private final toAllDisputesScreen(Lcom/squareup/disputes/DisputesState$OverviewState;Lcom/squareup/workflow/legacy/WorkflowInput;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/DisputesState$OverviewState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/DisputesEvent;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 70
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputes;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 71
    sget-object v0, Lcom/squareup/disputes/AllDisputes$ScreenData$Loading;->INSTANCE:Lcom/squareup/disputes/AllDisputes$ScreenData$Loading;

    check-cast v0, Lcom/squareup/disputes/AllDisputes$ScreenData;

    invoke-static {v0, p2}, Lcom/squareup/disputes/AllDisputesScreenKt;->AllDisputesScreen(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    .line 70
    invoke-virtual {p1, p2}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 73
    :cond_0
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 75
    check-cast p1, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->getResponse()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object v1

    .line 76
    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->getLoadingMoreError()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 77
    sget-object p1, Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;->LOAD_MORE_ERROR:Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;

    goto :goto_0

    .line 78
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->getResponse()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/disputes/DisputesRenderer;->getHasMore(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 79
    sget-object p1, Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;->HAS_MORE:Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;

    goto :goto_0

    .line 81
    :cond_2
    sget-object p1, Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;->NO_MORE:Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;

    .line 75
    :goto_0
    invoke-direct {p0, v1, p1}, Lcom/squareup/disputes/DisputesRenderer;->toWithData(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;)Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;

    move-result-object p1

    check-cast p1, Lcom/squareup/disputes/AllDisputes$ScreenData;

    .line 74
    invoke-static {p1, p2}, Lcom/squareup/disputes/AllDisputesScreenKt;->AllDisputesScreen(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 73
    invoke-virtual {v0, p1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 87
    :cond_3
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputesError;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 89
    new-instance v1, Lcom/squareup/disputes/AllDisputes$ScreenData$DisputesError;

    check-cast p1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputesError;

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputesError;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputesError;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$DisputesError;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/disputes/AllDisputes$ScreenData;

    .line 88
    invoke-static {v1, p2}, Lcom/squareup/disputes/AllDisputesScreenKt;->AllDisputesScreen(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 87
    invoke-virtual {v0, p1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 92
    :cond_4
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreDisputes;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 94
    check-cast p1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreDisputes;

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreDisputes;->getLastResponse()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object p1

    sget-object v1, Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;->LOADING_MORE:Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;

    invoke-direct {p0, p1, v1}, Lcom/squareup/disputes/DisputesRenderer;->toWithData(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;)Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;

    move-result-object p1

    check-cast p1, Lcom/squareup/disputes/AllDisputes$ScreenData;

    .line 93
    invoke-static {p1, p2}, Lcom/squareup/disputes/AllDisputesScreenKt;->AllDisputesScreen(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 92
    invoke-virtual {v0, p1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 97
    :cond_5
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreError;

    if-eqz v0, :cond_6

    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 99
    check-cast p1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreError;

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreError;->getPreviousResponse()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object p1

    sget-object v0, Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;->LOAD_MORE_ERROR:Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;

    invoke-direct {p0, p1, v0}, Lcom/squareup/disputes/DisputesRenderer;->toWithData(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;)Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;

    move-result-object p1

    check-cast p1, Lcom/squareup/disputes/AllDisputes$ScreenData;

    .line 98
    invoke-static {p1, p2}, Lcom/squareup/disputes/AllDisputesScreenKt;->AllDisputesScreen(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 103
    new-instance p1, Lcom/squareup/disputes/DisputesErrorDialog$ScreenData;

    const/4 v0, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {p1, v6, v6, v0, v5}, Lcom/squareup/disputes/DisputesErrorDialog$ScreenData;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 102
    invoke-static {p1, p2}, Lcom/squareup/disputes/DisputesErrorDialogScreenKt;->DisputesErrorDialogScreen(Lcom/squareup/disputes/DisputesErrorDialog$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v5

    const/4 v6, 0x6

    const/4 v7, 0x0

    .line 97
    invoke-static/range {v1 .. v7}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final toSummaryScreen(Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;Lcom/squareup/workflow/legacy/WorkflowInput;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/DisputesEvent;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 113
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingForm;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    sget-object v0, Lcom/squareup/disputes/Summary$ScreenData$Loading;->INSTANCE:Lcom/squareup/disputes/Summary$ScreenData$Loading;

    check-cast v0, Lcom/squareup/disputes/Summary$ScreenData;

    invoke-static {v0, p2}, Lcom/squareup/disputes/SummaryScreenKt;->SummaryScreen(Lcom/squareup/disputes/Summary$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 114
    :cond_0
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$FormLoaded;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 116
    new-instance v1, Lcom/squareup/disputes/Summary$ScreenData$WithData;

    new-instance v2, Lcom/squareup/disputes/ChallengeEngine;

    check-cast p1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$FormLoaded;

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$FormLoaded;->getResponse()Lcom/squareup/protos/client/irf/GetFormResponse;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/irf/GetFormResponse;->information_request:Lcom/squareup/protos/client/irf/InformationRequestDetail;

    const-string/jumbo v3, "this.response.information_request"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, p1}, Lcom/squareup/disputes/ChallengeEngine;-><init>(Lcom/squareup/protos/client/irf/InformationRequestDetail;)V

    invoke-direct {v1, v2}, Lcom/squareup/disputes/Summary$ScreenData$WithData;-><init>(Lcom/squareup/disputes/ChallengeEngine;)V

    check-cast v1, Lcom/squareup/disputes/Summary$ScreenData;

    .line 115
    invoke-static {v1, p2}, Lcom/squareup/disputes/SummaryScreenKt;->SummaryScreen(Lcom/squareup/disputes/Summary$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 114
    invoke-virtual {v0, p1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 120
    :cond_1
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 121
    new-instance v1, Lcom/squareup/disputes/Summary$ScreenData$SummaryError;

    check-cast p1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/disputes/Summary$ScreenData$SummaryError;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/disputes/Summary$ScreenData;

    invoke-static {v1, p2}, Lcom/squareup/disputes/SummaryScreenKt;->SummaryScreen(Lcom/squareup/disputes/Summary$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 120
    invoke-virtual {v0, p1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final toWithData(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;)Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;
    .locals 10

    .line 131
    new-instance v9, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;

    .line 132
    iget-object v3, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_covered_amount:Lcom/squareup/protos/common/Money;

    const-string/jumbo v0, "total_covered_amount"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputes_count:Ljava/lang/Long;

    const-string/jumbo v1, "total_disputes_count"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 134
    iget-object v6, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    const-string/jumbo v0, "total_disputed_amount"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    invoke-direct {p0, p1}, Lcom/squareup/disputes/DisputesRenderer;->getNextCursor(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "nextCursor"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 137
    iget-object v8, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute:Ljava/util/List;

    const-string v0, "resolved_dispute"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    iget-object v7, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unresolved_dispute:Ljava/util/List;

    const-string/jumbo p1, "unresolved_dispute"

    invoke-static {v7, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v9

    move-object v1, p2

    .line 131
    invoke-direct/range {v0 .. v8}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;-><init>(Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;ILcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;)V

    return-object v9
.end method


# virtual methods
.method public final getDevice()Lcom/squareup/util/Device;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/disputes/DisputesRenderer;->device:Lcom/squareup/util/Device;

    return-object v0
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/disputes/DisputesState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/disputes/DisputesRenderer;->render(Lcom/squareup/disputes/DisputesState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/disputes/DisputesState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/DisputesState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/DisputesEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    instance-of p3, p1, Lcom/squareup/disputes/DisputesState$WaitingToStart;

    if-nez p3, :cond_4

    .line 46
    instance-of p3, p1, Lcom/squareup/disputes/DisputesState$OverviewState;

    if-eqz p3, :cond_0

    check-cast p1, Lcom/squareup/disputes/DisputesState$OverviewState;

    invoke-direct {p0, p1, p2}, Lcom/squareup/disputes/DisputesRenderer;->toAllDisputesScreen(Lcom/squareup/disputes/DisputesState$OverviewState;Lcom/squareup/workflow/legacy/WorkflowInput;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 47
    :cond_0
    instance-of p3, p1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;

    if-eqz p3, :cond_1

    check-cast p1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;

    invoke-direct {p0, p1, p2}, Lcom/squareup/disputes/DisputesRenderer;->toSummaryScreen(Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;Lcom/squareup/workflow/legacy/WorkflowInput;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 48
    :cond_1
    instance-of p3, p1, Lcom/squareup/disputes/DisputesState$DetailState;

    if-eqz p3, :cond_2

    sget-object p3, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 50
    new-instance v0, Lcom/squareup/disputes/DisputesDetail$ScreenData;

    check-cast p1, Lcom/squareup/disputes/DisputesState$DetailState;

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesState$DetailState;->getDispute()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/disputes/DisputesDetail$ScreenData;-><init>(Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    .line 49
    invoke-static {v0, p2}, Lcom/squareup/disputes/DisputesDetailScreenKt;->DisputesDetailScreen(Lcom/squareup/disputes/DisputesDetail$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 48
    invoke-virtual {p3, p1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 54
    :cond_2
    instance-of p3, p1, Lcom/squareup/disputes/DisputesState$ActionDialogState;

    if-eqz p3, :cond_3

    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 56
    new-instance p3, Lcom/squareup/disputes/DisputesDetail$ScreenData;

    check-cast p1, Lcom/squareup/disputes/DisputesState$ActionDialogState;

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesState$ActionDialogState;->getDispute()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object v1

    invoke-direct {p3, v1}, Lcom/squareup/disputes/DisputesDetail$ScreenData;-><init>(Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    .line 55
    invoke-static {p3, p2}, Lcom/squareup/disputes/DisputesDetailScreenKt;->DisputesDetailScreen(Lcom/squareup/disputes/DisputesDetail$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 60
    new-instance p3, Lcom/squareup/disputes/DisputeActionDialog$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesState$ActionDialogState;->getDispute()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object p1

    invoke-direct {p3, p1}, Lcom/squareup/disputes/DisputeActionDialog$ScreenData;-><init>(Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    .line 59
    invoke-static {p3, p2}, Lcom/squareup/disputes/DisputeActionDialogScreenKt;->DisputeActionDialogScreen(Lcom/squareup/disputes/DisputeActionDialog$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v4

    const/4 v5, 0x6

    const/4 v6, 0x0

    .line 54
    invoke-static/range {v0 .. v6}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 45
    :cond_4
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Cannot render screen for "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method
