.class final Lcom/squareup/disputes/DisputesReactor$onReact$3;
.super Lkotlin/jvm/internal/Lambda;
.source "DisputesReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/DisputesReactor;->onReact(Lcom/squareup/disputes/DisputesState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
        "Lcom/squareup/disputes/DisputesEvent;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/disputes/DisputesState;",
        "+",
        "Lkotlin/Unit;",
        ">;>;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisputesReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisputesReactor.kt\ncom/squareup/disputes/DisputesReactor$onReact$3\n+ 2 EventSelectBuilder.kt\ncom/squareup/workflow/legacy/rx2/EventSelectBuilder\n*L\n1#1,232:1\n85#2,2:233\n85#2,2:235\n85#2,2:237\n85#2,2:239\n*E\n*S KotlinDebug\n*F\n+ 1 DisputesReactor.kt\ncom/squareup/disputes/DisputesReactor$onReact$3\n*L\n94#1,2:233\n95#1,2:235\n100#1,2:237\n103#1,2:239\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u001a\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;",
        "Lcom/squareup/disputes/DisputesEvent;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/disputes/DisputesState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/disputes/DisputesState;

.field final synthetic this$0:Lcom/squareup/disputes/DisputesReactor;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/disputes/DisputesState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$3;->this$0:Lcom/squareup/disputes/DisputesReactor;

    iput-object p2, p0, Lcom/squareup/disputes/DisputesReactor$onReact$3;->$state:Lcom/squareup/disputes/DisputesState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/DisputesReactor$onReact$3;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
            "Lcom/squareup/disputes/DisputesEvent;",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/disputes/DisputesState;",
            "Lkotlin/Unit;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    sget-object v0, Lcom/squareup/disputes/DisputesReactor$onReact$3$1;->INSTANCE:Lcom/squareup/disputes/DisputesReactor$onReact$3$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 233
    sget-object v1, Lcom/squareup/disputes/DisputesReactor$onReact$3$$special$$inlined$onEvent$1;->INSTANCE:Lcom/squareup/disputes/DisputesReactor$onReact$3$$special$$inlined$onEvent$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 95
    new-instance v0, Lcom/squareup/disputes/DisputesReactor$onReact$3$2;

    invoke-direct {v0, p0}, Lcom/squareup/disputes/DisputesReactor$onReact$3$2;-><init>(Lcom/squareup/disputes/DisputesReactor$onReact$3;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 235
    sget-object v1, Lcom/squareup/disputes/DisputesReactor$onReact$3$$special$$inlined$onEvent$2;->INSTANCE:Lcom/squareup/disputes/DisputesReactor$onReact$3$$special$$inlined$onEvent$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 100
    new-instance v0, Lcom/squareup/disputes/DisputesReactor$onReact$3$3;

    invoke-direct {v0, p0}, Lcom/squareup/disputes/DisputesReactor$onReact$3$3;-><init>(Lcom/squareup/disputes/DisputesReactor$onReact$3;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 237
    sget-object v1, Lcom/squareup/disputes/DisputesReactor$onReact$3$$special$$inlined$onEvent$3;->INSTANCE:Lcom/squareup/disputes/DisputesReactor$onReact$3$$special$$inlined$onEvent$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 103
    new-instance v0, Lcom/squareup/disputes/DisputesReactor$onReact$3$4;

    invoke-direct {v0, p0}, Lcom/squareup/disputes/DisputesReactor$onReact$3$4;-><init>(Lcom/squareup/disputes/DisputesReactor$onReact$3;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 239
    sget-object v1, Lcom/squareup/disputes/DisputesReactor$onReact$3$$special$$inlined$onEvent$4;->INSTANCE:Lcom/squareup/disputes/DisputesReactor$onReact$3$$special$$inlined$onEvent$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
