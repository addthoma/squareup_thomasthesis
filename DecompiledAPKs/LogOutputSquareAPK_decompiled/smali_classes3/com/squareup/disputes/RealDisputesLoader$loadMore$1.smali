.class final Lcom/squareup/disputes/RealDisputesLoader$loadMore$1;
.super Ljava/lang/Object;
.source "DisputesLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/RealDisputesLoader;->loadMore(I)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/disputes/DisputesState$OverviewState;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/cbms/ListDisputesResponse;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/disputes/RealDisputesLoader;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/RealDisputesLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/RealDisputesLoader$loadMore$1;->this$0:Lcom/squareup/disputes/RealDisputesLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/disputes/DisputesState$OverviewState;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/cbms/ListDisputesResponse;",
            ">;)",
            "Lcom/squareup/disputes/DisputesState$OverviewState;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/squareup/disputes/RealDisputesLoader$loadMore$1;->this$0:Lcom/squareup/disputes/RealDisputesLoader;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    const-string v1, "successOrFailure.response"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/protos/client/cbms/ListDisputesResponse;

    invoke-static {v0, p1}, Lcom/squareup/disputes/RealDisputesLoader;->access$addResolved(Lcom/squareup/disputes/RealDisputesLoader;Lcom/squareup/protos/client/cbms/ListDisputesResponse;)V

    goto :goto_0

    .line 77
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    .line 78
    new-instance p1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreError;

    .line 79
    iget-object v0, p0, Lcom/squareup/disputes/RealDisputesLoader$loadMore$1;->this$0:Lcom/squareup/disputes/RealDisputesLoader;

    invoke-static {v0}, Lcom/squareup/disputes/RealDisputesLoader;->access$getBaseResponse$p(Lcom/squareup/disputes/RealDisputesLoader;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object v0

    .line 78
    invoke-direct {p1, v0}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreError;-><init>(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)V

    check-cast p1, Lcom/squareup/disputes/DisputesState$OverviewState;

    return-object p1

    .line 83
    :cond_1
    :goto_0
    new-instance p1, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    iget-object v0, p0, Lcom/squareup/disputes/RealDisputesLoader$loadMore$1;->this$0:Lcom/squareup/disputes/RealDisputesLoader;

    invoke-static {v0}, Lcom/squareup/disputes/RealDisputesLoader;->access$getBaseResponse$p(Lcom/squareup/disputes/RealDisputesLoader;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;-><init>(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/disputes/DisputesState$OverviewState;

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/RealDisputesLoader$loadMore$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/disputes/DisputesState$OverviewState;

    move-result-object p1

    return-object p1
.end method
