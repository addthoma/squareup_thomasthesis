.class public abstract Lcom/squareup/disputes/DisputesEvent;
.super Ljava/lang/Object;
.source "DisputesEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0006\u0003\u0004\u0005\u0006\u0007\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesEvent;",
        "",
        "()V",
        "Lcom/squareup/disputes/InitializeWorkflowEvent;",
        "Lcom/squareup/disputes/AllDisputesEvent;",
        "Lcom/squareup/disputes/ErrorDialogEvent;",
        "Lcom/squareup/disputes/SummaryEvent;",
        "Lcom/squareup/disputes/DetailEvent;",
        "Lcom/squareup/disputes/DisputeActionDialogEvent;",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/disputes/DisputesEvent;-><init>()V

    return-void
.end method
