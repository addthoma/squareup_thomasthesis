.class public final Lcom/squareup/fonts/FontSpan;
.super Landroid/text/style/TypefaceSpan;
.source "FontSpan.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFontSpan.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FontSpan.kt\ncom/squareup/fonts/FontSpan\n*L\n1#1,44:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0015H\u0016R\u0016\u0010\u0007\u001a\n \u0008*\u0004\u0018\u00010\u00030\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000b\u0010\u000cR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/fonts/FontSpan;",
        "Landroid/text/style/TypefaceSpan;",
        "context",
        "Landroid/content/Context;",
        "fontId",
        "",
        "(Landroid/content/Context;I)V",
        "appContext",
        "kotlin.jvm.PlatformType",
        "font",
        "Landroid/graphics/Typeface;",
        "getFont",
        "()Landroid/graphics/Typeface;",
        "font$delegate",
        "Lkotlin/Lazy;",
        "applyFont",
        "",
        "paint",
        "Landroid/graphics/Paint;",
        "updateDrawState",
        "ds",
        "Landroid/text/TextPaint;",
        "updateMeasureState",
        "fonts-utils_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final appContext:Landroid/content/Context;

.field private final font$delegate:Lkotlin/Lazy;

.field private final fontId:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/fonts/FontSpan;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "font"

    const-string v4, "getFont()Landroid/graphics/Typeface;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/fonts/FontSpan;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sans-serif"

    .line 25
    invoke-direct {p0, v0}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    iput p2, p0, Lcom/squareup/fonts/FontSpan;->fontId:I

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/fonts/FontSpan;->appContext:Landroid/content/Context;

    .line 34
    new-instance p1, Lcom/squareup/fonts/FontSpan$font$2;

    invoke-direct {p1, p0}, Lcom/squareup/fonts/FontSpan$font$2;-><init>(Lcom/squareup/fonts/FontSpan;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/fonts/FontSpan;->font$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$getAppContext$p(Lcom/squareup/fonts/FontSpan;)Landroid/content/Context;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/fonts/FontSpan;->appContext:Landroid/content/Context;

    return-object p0
.end method

.method public static final synthetic access$getFontId$p(Lcom/squareup/fonts/FontSpan;)I
    .locals 0

    .line 22
    iget p0, p0, Lcom/squareup/fonts/FontSpan;->fontId:I

    return p0
.end method

.method private final applyFont(Landroid/graphics/Paint;)V
    .locals 1

    .line 40
    invoke-direct {p0}, Lcom/squareup/fonts/FontSpan;->getFont()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 41
    invoke-static {p1}, Lcom/squareup/fonts/FontUtilsKt;->configureOptimalTextFlags(Landroid/graphics/Paint;)V

    return-void
.end method

.method private final getFont()Landroid/graphics/Typeface;
    .locals 3

    iget-object v0, p0, Lcom/squareup/fonts/FontSpan;->font$delegate:Lkotlin/Lazy;

    sget-object v1, Lcom/squareup/fonts/FontSpan;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    return-object v0
.end method


# virtual methods
.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    const-string v0, "ds"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    check-cast p1, Landroid/graphics/Paint;

    invoke-direct {p0, p1}, Lcom/squareup/fonts/FontSpan;->applyFont(Landroid/graphics/Paint;)V

    return-void
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .locals 1

    const-string v0, "paint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    check-cast p1, Landroid/graphics/Paint;

    invoke-direct {p0, p1}, Lcom/squareup/fonts/FontSpan;->applyFont(Landroid/graphics/Paint;)V

    return-void
.end method
