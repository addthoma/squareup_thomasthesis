.class public abstract Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule;
.super Ljava/lang/Object;
.source "ReleaseDevelopmentDrawerModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\'\u0018\u0000 \u00082\u00020\u0001:\u0001\u0008B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule;",
        "",
        "()V",
        "bindContentViewInitializer",
        "Lcom/squareup/development/drawer/ContentViewInitializer;",
        "initializer",
        "Lcom/squareup/development/drawer/ReleaseContentViewInitializer;",
        "bindContentViewInitializer$impl_wiring_release",
        "Companion",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule;->Companion:Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final contributeAppInfoSection()Ljava/util/Set;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldagger/multibindings/ElementsIntoSet;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/development/drawer/DevelopmentDrawerSection;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule;->Companion:Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule$Companion;->contributeAppInfoSection()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract bindContentViewInitializer$impl_wiring_release(Lcom/squareup/development/drawer/ReleaseContentViewInitializer;)Lcom/squareup/development/drawer/ContentViewInitializer;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
