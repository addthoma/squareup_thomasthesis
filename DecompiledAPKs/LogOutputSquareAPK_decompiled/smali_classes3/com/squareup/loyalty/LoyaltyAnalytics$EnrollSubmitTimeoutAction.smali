.class Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitTimeoutAction;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "LoyaltyAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/LoyaltyAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EnrollSubmitTimeoutAction"
.end annotation


# instance fields
.field final buyerToken:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "buyer_token"
    .end annotation
.end field

.field final clientPaymentToken:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "client_payment_token"
    .end annotation
.end field

.field final unitToken:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "unit_token"
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/analytics/RegisterActionName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V
    .locals 1

    const/4 v0, 0x0

    .line 152
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitTimeoutAction;-><init>(Lcom/squareup/analytics/RegisterActionName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/protos/eventstream/v1/Subject;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/analytics/RegisterActionName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/protos/eventstream/v1/Subject;)V
    .locals 0

    .line 157
    invoke-direct {p0, p1, p4}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;Lcom/squareup/protos/eventstream/v1/Subject;)V

    .line 158
    invoke-virtual {p3}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitTimeoutAction;->clientPaymentToken:Ljava/lang/String;

    const/4 p1, 0x0

    if-eqz p2, :cond_0

    move-object p4, p1

    goto :goto_0

    .line 159
    :cond_0
    iget-object p4, p3, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->unitToken:Ljava/lang/String;

    :goto_0
    iput-object p4, p0, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitTimeoutAction;->unitToken:Ljava/lang/String;

    if-eqz p2, :cond_1

    .line 160
    invoke-static {p3}, Lcom/squareup/loyalty/LoyaltyAnalytics;->access$000(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Ljava/lang/String;

    move-result-object p1

    :cond_1
    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitTimeoutAction;->buyerToken:Ljava/lang/String;

    return-void
.end method
