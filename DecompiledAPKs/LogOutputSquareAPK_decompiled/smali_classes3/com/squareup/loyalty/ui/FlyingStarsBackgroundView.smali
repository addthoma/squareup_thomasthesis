.class public Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;
.super Landroid/widget/FrameLayout;
.source "FlyingStarsBackgroundView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;
    }
.end annotation


# instance fields
.field private final animator:Landroid/animation/ValueAnimator;

.field private final numberOfStars:I

.field private final screenDensity:F

.field private final stars:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;",
            ">;"
        }
    .end annotation
.end field

.field private startedAnimation:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->stars:Ljava/util/List;

    const/4 v0, 0x0

    .line 36
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->startedAnimation:Ljava/lang/Boolean;

    .line 41
    sget-object v0, Lcom/squareup/loyalty/R$styleable;->FlyingStarsBackgroundView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 42
    sget v0, Lcom/squareup/loyalty/R$styleable;->FlyingStarsBackgroundView_numberOfStars:I

    const/16 v1, 0x8

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->numberOfStars:I

    .line 43
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 45
    sget p2, Lcom/squareup/loyalty/R$layout;->flying_stars_background_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    iput p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->screenDensity:F

    const/4 p1, 0x2

    new-array p1, p1, [F

    .line 48
    fill-array-data p1, :array_0

    invoke-static {p1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->animator:Landroid/animation/ValueAnimator;

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic access$100(Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;F)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->updateStars(F)V

    return-void
.end method

.method static synthetic access$200(FF)F
    .locals 0

    .line 30
    invoke-static {p0, p1}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->random(FF)F

    move-result p0

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;)F
    .locals 0

    .line 30
    iget p0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->screenDensity:F

    return p0
.end method

.method private static random(FF)F
    .locals 2

    sub-float/2addr p1, p0

    .line 217
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    double-to-float v0, v0

    mul-float p1, p1, v0

    add-float/2addr p0, p1

    return p0
.end method

.method private startAnimation(II)V
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->stars:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;

    .line 97
    invoke-virtual {v1, p1, p2}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->initialize(II)V

    goto :goto_0

    .line 100
    :cond_0
    iget-object p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->animator:Landroid/animation/ValueAnimator;

    new-instance p2, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$1;

    invoke-direct {p2, p0}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$1;-><init>(Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;)V

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->animator:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 118
    iget-object p1, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->animator:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method private updateStars(F)V
    .locals 4

    .line 122
    invoke-virtual {p0}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->getWidth()I

    move-result v0

    .line 123
    invoke-virtual {p0}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->getHeight()I

    move-result v1

    const/4 v2, 0x0

    .line 125
    :goto_0
    iget v3, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->numberOfStars:I

    if-ge v2, v3, :cond_0

    .line 126
    iget-object v3, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->stars:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;

    invoke-virtual {v3, p1, v0, v1}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->update(FII)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public isAnimating()Z
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->startedAnimation:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$startAnimation$0$FlyingStarsBackgroundView(Landroid/view/View;II)V
    .locals 0

    .line 79
    invoke-direct {p0, p2, p3}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->startAnimation(II)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 91
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 92
    iget-object v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->animator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 87
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public startAnimation(Landroid/content/Context;)V
    .locals 4

    .line 56
    iget-object v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->startedAnimation:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 59
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->startedAnimation:Ljava/lang/Boolean;

    .line 63
    iget-object v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->animator:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 64
    iget-object v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->animator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 68
    invoke-static {}, Lcom/squareup/util/Views;->noAnimationsForInstrumentation()Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->animator:Landroid/animation/ValueAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 72
    :cond_1
    sget v0, Lcom/squareup/loyalty/R$id;->background_stars:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    .line 73
    :goto_0
    iget v2, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->numberOfStars:I

    if-ge v1, v2, :cond_2

    .line 74
    new-instance v2, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;

    invoke-direct {v2, p0, p1}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;-><init>(Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;Landroid/content/Context;)V

    .line 75
    invoke-static {v2}, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;->access$000(Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView$Star;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 76
    iget-object v3, p0, Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;->stars:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 79
    :cond_2
    new-instance p1, Lcom/squareup/loyalty/ui/-$$Lambda$FlyingStarsBackgroundView$E0YuM5kAnYv-Gd1NCGQ0avl-B80;

    invoke-direct {p1, p0}, Lcom/squareup/loyalty/ui/-$$Lambda$FlyingStarsBackgroundView$E0YuM5kAnYv-Gd1NCGQ0avl-B80;-><init>(Lcom/squareup/loyalty/ui/FlyingStarsBackgroundView;)V

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method
