.class final Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;
.super Ljava/lang/Object;
.source "RewardAdapterHelper.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyalty/ui/RewardAdapterHelper;->createRewardWrapperList(Lrx/Observable;Lcom/squareup/checkout/HoldsCoupons;Lkotlin/jvm/functions/Function1;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/loyalty/ui/RewardWrapper;",
        "contactPointTotal1",
        "",
        "kotlin.jvm.PlatformType",
        "call",
        "(Ljava/lang/Integer;)Lcom/squareup/loyalty/ui/RewardWrapper;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $appliedToCart:Z

.field final synthetic $applyEvent:Lkotlin/jvm/functions/Function1;

.field final synthetic $rewardTier:Lcom/squareup/server/account/protos/RewardTier;

.field final synthetic this$0:Lcom/squareup/loyalty/ui/RewardAdapterHelper;


# direct methods
.method constructor <init>(Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/server/account/protos/RewardTier;ZLkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;->this$0:Lcom/squareup/loyalty/ui/RewardAdapterHelper;

    iput-object p2, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;->$rewardTier:Lcom/squareup/server/account/protos/RewardTier;

    iput-boolean p3, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;->$appliedToCart:Z

    iput-object p4, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;->$applyEvent:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Integer;)Lcom/squareup/loyalty/ui/RewardWrapper;
    .locals 4

    .line 104
    iget-object v0, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;->this$0:Lcom/squareup/loyalty/ui/RewardAdapterHelper;

    .line 105
    iget-object v1, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;->$rewardTier:Lcom/squareup/server/account/protos/RewardTier;

    const-string v2, "rewardTier"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/loyalty/ui/RewardAdapterKt;->toRewardTier(Lcom/squareup/server/account/protos/RewardTier;)Lcom/squareup/protos/client/loyalty/RewardTier;

    move-result-object v1

    const-string v2, "rewardTier.toRewardTier()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-boolean v2, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;->$appliedToCart:Z

    const-string v3, "contactPointTotal1"

    .line 107
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 108
    new-instance v3, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1$1;

    invoke-direct {v3, p0}, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1$1;-><init>(Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 104
    invoke-static {v0, v1, v2, p1, v3}, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->access$createRewardWrapper(Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/protos/client/loyalty/RewardTier;ZILkotlin/jvm/functions/Function0;)Lcom/squareup/loyalty/ui/RewardWrapper;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;->call(Ljava/lang/Integer;)Lcom/squareup/loyalty/ui/RewardWrapper;

    move-result-object p1

    return-object p1
.end method
