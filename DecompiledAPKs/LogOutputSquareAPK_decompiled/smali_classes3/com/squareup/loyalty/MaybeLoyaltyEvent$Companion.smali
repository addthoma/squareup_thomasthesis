.class public final Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion;
.super Ljava/lang/Object;
.source "MaybeLoyaltyEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/MaybeLoyaltyEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMaybeLoyaltyEvent.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MaybeLoyaltyEvent.kt\ncom/squareup/loyalty/MaybeLoyaltyEvent$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,225:1\n1360#2:226\n1429#2,3:227\n*E\n*S KotlinDebug\n*F\n+ 1 MaybeLoyaltyEvent.kt\ncom/squareup/loyalty/MaybeLoyaltyEvent$Companion\n*L\n194#1:226\n194#1,3:227\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002JX\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\r2\u0006\u0010\u0015\u001a\u00020\rH\u0007\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion;",
        "",
        "()V",
        "createLoyaltyEventFromLoyaltyStatus",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
        "tenderIdPair",
        "Lcom/squareup/protos/client/IdPair;",
        "billIdPair",
        "tenderType",
        "Lcom/squareup/protos/client/bills/Tender$Type;",
        "cart",
        "Lcom/squareup/protos/client/bills/Cart;",
        "isEnrolled",
        "",
        "punchStatus",
        "Lcom/squareup/protos/client/loyalty/LoyaltyStatus;",
        "eventContext",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$EventContext;",
        "unitToken",
        "",
        "isNewlyEnrolled",
        "didLinkNewCardToExistingLoyalty",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 154
    invoke-direct {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final createLoyaltyEventFromLoyaltyStatus(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;ZLcom/squareup/protos/client/loyalty/LoyaltyStatus;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$EventContext;Ljava/lang/String;ZZ)Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;
    .locals 19
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object/from16 v0, p6

    const-string v1, "tenderIdPair"

    move-object/from16 v3, p1

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "billIdPair"

    move-object/from16 v4, p2

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "tenderType"

    move-object/from16 v5, p3

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "cart"

    move-object/from16 v6, p4

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "punchStatus"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "eventContext"

    move-object/from16 v2, p7

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "unitToken"

    move-object/from16 v12, p8

    invoke-static {v12, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    if-eqz p5, :cond_0

    .line 170
    iget-object v7, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->phone_token:Ljava/lang/String;

    if-eqz v7, :cond_0

    invoke-static {v7}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    move-object v15, v1

    const/4 v1, 0x1

    if-eqz v15, :cond_1

    const/4 v8, 0x1

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 175
    :goto_0
    iget-object v7, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_points:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    long-to-int v7, v9

    .line 178
    sget-object v9, Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual/range {p7 .. p7}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$EventContext;->ordinal()I

    move-result v2

    aget v2, v9, v2

    const-string v9, "punchStatus.balance"

    if-eq v2, v1, :cond_3

    const/4 v1, 0x2

    if-ne v2, v1, :cond_2

    .line 189
    iget-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    invoke-static {v1, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 190
    iget-object v2, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, v7

    goto :goto_1

    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 182
    :cond_3
    iget-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v1, v7

    .line 183
    iget-object v2, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    invoke-static {v2, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_1
    move v11, v2

    .line 194
    iget-object v2, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_tiers:Ljava/util/List;

    const-string v7, "punchStatus.reward_tiers"

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    .line 226
    new-instance v7, Ljava/util/ArrayList;

    const/16 v9, 0xa

    invoke-static {v2, v9}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v9

    invoke-direct {v7, v9}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v7, Ljava/util/Collection;

    .line 227
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    .line 228
    check-cast v9, Lcom/squareup/protos/client/loyalty/RewardTier;

    const-string v10, "it"

    .line 194
    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v9}, Lcom/squareup/loyalty/ui/RewardAdapterKt;->toRewardTier(Lcom/squareup/protos/client/loyalty/RewardTier;)Lcom/squareup/server/account/protos/RewardTier;

    move-result-object v9

    invoke-interface {v7, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 229
    :cond_4
    check-cast v7, Ljava/util/List;

    .line 195
    sget-object v2, Lcom/squareup/loyalty/LoyaltyServiceHelper;->Companion:Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;

    invoke-virtual {v2, v11, v7}, Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;->getAvailableRewards(ILjava/util/List;)I

    move-result v17

    .line 196
    sget-object v2, Lcom/squareup/loyalty/LoyaltyServiceHelper;->Companion:Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;

    invoke-virtual {v2, v1, v7}, Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;->getAvailableRewards(ILjava/util/List;)I

    move-result v1

    sub-int v16, v17, v1

    .line 199
    iget-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_points:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v9, 0x0

    cmp-long v7, v1, v9

    if-lez v7, :cond_5

    sget-object v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->EARN_POINTS:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    :goto_3
    move-object v7, v1

    goto :goto_4

    :cond_5
    if-eqz v8, :cond_6

    .line 200
    sget-object v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->SHOW_STATUS_WITHOUT_EARNING_POINTS:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    goto :goto_3

    .line 201
    :cond_6
    sget-object v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->ENROLL_WITHOUT_EARNING_POINTS:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    goto :goto_3

    .line 204
    :goto_4
    new-instance v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    move-object v2, v1

    .line 212
    iget-object v9, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_points:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    long-to-int v10, v9

    .line 216
    iget-object v14, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->obfuscated_phone_number:Ljava/lang/String;

    .line 220
    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    move-object/from16 v18, v0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v9, p9

    move-object/from16 v12, p8

    move/from16 v13, p10

    .line 204
    invoke-direct/range {v2 .. v18}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;ZZIILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;IILcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;)V

    return-object v1
.end method
