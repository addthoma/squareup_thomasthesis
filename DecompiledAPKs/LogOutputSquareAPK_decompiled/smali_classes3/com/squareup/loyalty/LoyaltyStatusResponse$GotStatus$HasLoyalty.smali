.class public final Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;
.super Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;
.source "LoyaltyStatusResponse.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HasLoyalty"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B+\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u000f\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\nH\u00c6\u0003J7\u0010\u0018\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\t\u0010\u0019\u001a\u00020\u0006H\u00d6\u0001J\u0013\u0010\u001a\u001a\u00020\u001b2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u0006H\u00d6\u0001J\r\u0010\u001f\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010 J\t\u0010!\u001a\u00020\u0008H\u00d6\u0001J\u0019\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\u0006H\u00d6\u0001R\u001a\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;",
        "coupons",
        "",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "currentPoints",
        "",
        "phoneToken",
        "",
        "response",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
        "(Ljava/util/List;ILjava/lang/String;Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)V",
        "getCoupons",
        "()Ljava/util/List;",
        "getCurrentPoints",
        "()I",
        "getPhoneToken",
        "()Ljava/lang/String;",
        "getResponse",
        "()Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "lifetimePoints",
        "()Ljava/lang/Integer;",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final coupons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;"
        }
    .end annotation
.end field

.field private final currentPoints:I

.field private final phoneToken:Ljava/lang/String;

.field private final response:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty$Creator;

    invoke-direct {v0}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty$Creator;-><init>()V

    sput-object v0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;ILjava/lang/String;Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;I",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
            ")V"
        }
    .end annotation

    const-string v0, "coupons"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "response"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1, v0}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;-><init>(Ljava/util/List;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->coupons:Ljava/util/List;

    iput p2, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->currentPoints:I

    iput-object p3, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->phoneToken:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->response:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;Ljava/util/List;ILjava/lang/String;Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;ILjava/lang/Object;)Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getCoupons()Ljava/util/List;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget p2, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->currentPoints:I

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->phoneToken:Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->response:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->copy(Ljava/util/List;ILjava/lang/String;Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getCoupons()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->currentPoints:I

    return v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->phoneToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->response:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    return-object v0
.end method

.method public final copy(Ljava/util/List;ILjava/lang/String;Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;I",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
            ")",
            "Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;"
        }
    .end annotation

    const-string v0, "coupons"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "response"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;-><init>(Ljava/util/List;ILjava/lang/String;Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    invoke-virtual {p0}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getCoupons()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getCoupons()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->currentPoints:I

    iget v1, p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->currentPoints:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->phoneToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->phoneToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->response:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    iget-object p1, p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->response:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getCoupons()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->coupons:Ljava/util/List;

    return-object v0
.end method

.method public final getCurrentPoints()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->currentPoints:I

    return v0
.end method

.method public final getPhoneToken()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->phoneToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getResponse()Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->response:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getCoupons()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->currentPoints:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->phoneToken:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->response:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final lifetimePoints()Ljava/lang/Integer;
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->response:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_status:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->lifetime_points:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v1, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HasLoyalty(coupons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getCoupons()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currentPoints="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->currentPoints:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", phoneToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->phoneToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->response:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->coupons:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    goto :goto_0

    :cond_0
    iget p2, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->currentPoints:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object p2, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->phoneToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->response:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
