.class public final Lcom/squareup/loyalty/AnimateProgressRewardText;
.super Lcom/squareup/loyalty/LoyaltyRewardText;
.source "LoyaltyRewardText.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u00002\u00020\u0001B1\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00030\u0008\u00a2\u0006\u0002\u0010\tJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0005H\u00c6\u0003J\u0015\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00030\u0008H\u00c2\u0003J=\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0014\u0008\u0002\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00030\u0008H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\u000e\u0010\u0018\u001a\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u0005J\t\u0010\u001a\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u0003H\u00d6\u0001R\u001a\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00030\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\r\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/loyalty/AnimateProgressRewardText;",
        "Lcom/squareup/loyalty/LoyaltyRewardText;",
        "initialText",
        "",
        "newPointsEarned",
        "",
        "totalCurrentBalance",
        "balanceFormatter",
        "Lkotlin/Function1;",
        "(Ljava/lang/String;IILkotlin/jvm/functions/Function1;)V",
        "getInitialText",
        "()Ljava/lang/String;",
        "getNewPointsEarned",
        "()I",
        "getTotalCurrentBalance",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "getInterpolatedBalance",
        "progressPercent",
        "hashCode",
        "toString",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final balanceFormatter:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final initialText:Ljava/lang/String;

.field private final newPointsEarned:I

.field private final totalCurrentBalance:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IILkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "initialText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, v0}, Lcom/squareup/loyalty/LoyaltyRewardText;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->initialText:Ljava/lang/String;

    iput p2, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->newPointsEarned:I

    iput p3, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->totalCurrentBalance:I

    iput-object p4, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->balanceFormatter:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method private final component4()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->balanceFormatter:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/loyalty/AnimateProgressRewardText;Ljava/lang/String;IILkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/loyalty/AnimateProgressRewardText;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->initialText:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget p2, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->newPointsEarned:I

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->totalCurrentBalance:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->balanceFormatter:Lkotlin/jvm/functions/Function1;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/loyalty/AnimateProgressRewardText;->copy(Ljava/lang/String;IILkotlin/jvm/functions/Function1;)Lcom/squareup/loyalty/AnimateProgressRewardText;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->initialText:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->newPointsEarned:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->totalCurrentBalance:I

    return v0
.end method

.method public final copy(Ljava/lang/String;IILkotlin/jvm/functions/Function1;)Lcom/squareup/loyalty/AnimateProgressRewardText;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/loyalty/AnimateProgressRewardText;"
        }
    .end annotation

    const-string v0, "initialText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/loyalty/AnimateProgressRewardText;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/loyalty/AnimateProgressRewardText;-><init>(Ljava/lang/String;IILkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/loyalty/AnimateProgressRewardText;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/loyalty/AnimateProgressRewardText;

    iget-object v0, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->initialText:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/loyalty/AnimateProgressRewardText;->initialText:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->newPointsEarned:I

    iget v1, p1, Lcom/squareup/loyalty/AnimateProgressRewardText;->newPointsEarned:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->totalCurrentBalance:I

    iget v1, p1, Lcom/squareup/loyalty/AnimateProgressRewardText;->totalCurrentBalance:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->balanceFormatter:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/loyalty/AnimateProgressRewardText;->balanceFormatter:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getInitialText()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->initialText:Ljava/lang/String;

    return-object v0
.end method

.method public final getInterpolatedBalance(I)Ljava/lang/String;
    .locals 4

    const/16 v0, 0x64

    if-lt p1, v0, :cond_0

    .line 43
    iget-object p1, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->balanceFormatter:Lkotlin/jvm/functions/Function1;

    iget v0, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->totalCurrentBalance:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1

    .line 46
    :cond_0
    iget v1, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->totalCurrentBalance:I

    iget v2, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->newPointsEarned:I

    sub-int/2addr v1, v2

    .line 47
    iget-object v3, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->balanceFormatter:Lkotlin/jvm/functions/Function1;

    mul-int p1, p1, v2

    div-int/2addr p1, v0

    add-int/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v3, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public final getNewPointsEarned()I
    .locals 1

    .line 30
    iget v0, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->newPointsEarned:I

    return v0
.end method

.method public final getTotalCurrentBalance()I
    .locals 1

    .line 31
    iget v0, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->totalCurrentBalance:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->initialText:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->newPointsEarned:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->totalCurrentBalance:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->balanceFormatter:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AnimateProgressRewardText(initialText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->initialText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", newPointsEarned="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->newPointsEarned:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", totalCurrentBalance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->totalCurrentBalance:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", balanceFormatter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyalty/AnimateProgressRewardText;->balanceFormatter:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
