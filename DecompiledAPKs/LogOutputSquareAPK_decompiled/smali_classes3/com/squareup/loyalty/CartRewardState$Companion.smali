.class public final Lcom/squareup/loyalty/CartRewardState$Companion;
.super Ljava/lang/Object;
.source "CartRewardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/CartRewardState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCartRewardState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CartRewardState.kt\ncom/squareup/loyalty/CartRewardState$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,48:1\n1360#2:49\n1429#2,2:50\n1412#2,9:52\n1642#2,2:61\n1421#2,11:63\n*E\n*S KotlinDebug\n*F\n+ 1 CartRewardState.kt\ncom/squareup/loyalty/CartRewardState$Companion\n*L\n34#1:49\n34#1,2:50\n34#1,9:52\n34#1,2:61\n34#1,11:63\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/loyalty/CartRewardState$Companion;",
        "",
        "()V",
        "createCartRewardState",
        "Lcom/squareup/loyalty/CartRewardState;",
        "holdsCoupons",
        "Lcom/squareup/checkout/HoldsCoupons;",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/loyalty/CartRewardState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final createCartRewardState(Lcom/squareup/checkout/HoldsCoupons;Lcom/squareup/loyalty/LoyaltySettings;)Lcom/squareup/loyalty/CartRewardState;
    .locals 5

    const-string v0, "holdsCoupons"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-interface {p2}, Lcom/squareup/loyalty/LoyaltySettings;->rewardTiers()Ljava/util/List;

    move-result-object p2

    if-eqz p2, :cond_4

    check-cast p2, Ljava/lang/Iterable;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 50
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 51
    check-cast v1, Lcom/squareup/server/account/protos/RewardTier;

    const-string v2, "rewardTier"

    .line 36
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v2, v1, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-interface {p1, v2}, Lcom/squareup/checkout/HoldsCoupons;->getAllAddedCoupons(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    const-string v3, "holdsCoupons\n           \u2026oupon_definition_token!!)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    .line 52
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 61
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 60
    check-cast v4, Lcom/squareup/checkout/Discount;

    .line 39
    invoke-virtual {v4}, Lcom/squareup/checkout/Discount;->getCouponToken()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 60
    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 63
    :cond_2
    check-cast v3, Ljava/util/List;

    .line 35
    new-instance v2, Lcom/squareup/loyalty/CartRewardState$RewardTierState;

    invoke-direct {v2, v1, v3}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;-><init>(Lcom/squareup/server/account/protos/RewardTier;Ljava/util/List;)V

    .line 40
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 73
    :cond_3
    check-cast v0, Ljava/util/List;

    goto :goto_2

    .line 42
    :cond_4
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 44
    :goto_2
    new-instance p1, Lcom/squareup/loyalty/CartRewardState;

    invoke-direct {p1, v0}, Lcom/squareup/loyalty/CartRewardState;-><init>(Ljava/util/List;)V

    return-object p1
.end method
