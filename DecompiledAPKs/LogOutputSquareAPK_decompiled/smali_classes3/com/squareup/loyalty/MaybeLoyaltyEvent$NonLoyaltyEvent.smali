.class public final Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;
.super Lcom/squareup/loyalty/MaybeLoyaltyEvent;
.source "MaybeLoyaltyEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/MaybeLoyaltyEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NonLoyaltyEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0005J\u000b\u0010\t\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\n\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J!\u0010\u000b\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent;",
        "tenderIdPair",
        "Lcom/squareup/protos/client/IdPair;",
        "billIdPair",
        "(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V",
        "getBillIdPair",
        "()Lcom/squareup/protos/client/IdPair;",
        "getTenderIdPair",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final billIdPair:Lcom/squareup/protos/client/IdPair;

.field private final tenderIdPair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v0, v1, v0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V
    .locals 1

    const/4 v0, 0x0

    .line 149
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->tenderIdPair:Lcom/squareup/protos/client/IdPair;

    iput-object p2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p4, p3, 0x1

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    .line 147
    move-object p1, v0

    check-cast p1, Lcom/squareup/protos/client/IdPair;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    .line 148
    move-object p2, v0

    check-cast p2, Lcom/squareup/protos/client/IdPair;

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;ILjava/lang/Object;)Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p2

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->copy(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/IdPair;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/client/IdPair;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;
    .locals 1

    new-instance v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;

    invoke-direct {v0, p1, p2}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getBillIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public getTenderIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->tenderIdPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NonLoyaltyEvent(tenderIdPair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", billIdPair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
