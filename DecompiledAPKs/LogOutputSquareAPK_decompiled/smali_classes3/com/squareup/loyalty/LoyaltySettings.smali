.class public interface abstract Lcom/squareup/loyalty/LoyaltySettings;
.super Ljava/lang/Object;
.source "LoyaltySettings.java"


# virtual methods
.method public abstract accrualRules()Lcom/squareup/server/account/protos/AccrualRules;
.end method

.method public abstract canRedeemPoints()Z
.end method

.method public abstract getExpirationPolicy()Lcom/squareup/server/account/protos/ExpirationPolicy;
.end method

.method public abstract getLoyaltyScreenTimeoutMillis()I
.end method

.method public abstract getLoyaltyScreenTimeoutSeconds()J
.end method

.method public abstract getSubunitName()Ljava/lang/String;
.end method

.method public abstract hasLoyaltyProgram()Z
.end method

.method public abstract isLoyaltyProgramActive()Z
.end method

.method public abstract isLoyaltyScreensEnabled()Z
.end method

.method public abstract isLoyaltyScreensEnabledObservable()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract pointsTerms()Lcom/squareup/loyalty/PointsTerms;
.end method

.method public abstract qualifyingPurchaseDescription()Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract rewardTiers()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setLoyaltyScreenTimeoutSeconds(J)V
.end method

.method public abstract setLoyaltyScreensEnabled(Z)V
.end method

.method public abstract setShowNonQualifyingLoyaltyEvents(Z)V
.end method

.method public abstract showNonQualifyingLoyaltyEvents()Z
.end method
