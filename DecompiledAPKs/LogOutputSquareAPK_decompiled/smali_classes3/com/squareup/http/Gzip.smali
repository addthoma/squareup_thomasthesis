.class public final Lcom/squareup/http/Gzip;
.super Ljava/lang/Object;
.source "Gzip.java"


# static fields
.field public static final GZIP_BODY_ENABLED:Ljava/lang/String; = "X-Square-Gzip: true"

.field public static final GZIP_BODY_HEADER:Ljava/lang/String; = "X-Square-Gzip"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
