.class public Lcom/squareup/http/Endpoints;
.super Ljava/lang/Object;
.source "Endpoints.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/http/Endpoints$Server;
    }
.end annotation


# static fields
.field public static final ENDPOINT_MOCK:Ljava/lang/String; = "https://localhost.mock.test/"

.field private static final MOCK_URL:Ljava/lang/String; = "localhost.mock.test"

.field public static final PROD_BASE_URL:Ljava/lang/String; = "squareup.com"

.field public static final STAGING_BASE_URL:Ljava/lang/String; = "squareupstaging.com"

.field public static final UNIVERSE_URL:Ljava/lang/String; = "universe"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static endpointsEqual(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 0

    .line 20
    invoke-static {p0}, Lcom/squareup/http/Endpoints;->getServerFromUrl(Ljava/lang/String;)Lcom/squareup/http/Endpoints$Server;

    move-result-object p0

    invoke-static {p1}, Lcom/squareup/http/Endpoints;->getServerFromUrl(Ljava/lang/String;)Lcom/squareup/http/Endpoints$Server;

    move-result-object p1

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static getServerFromUrl(Ljava/lang/String;)Lcom/squareup/http/Endpoints$Server;
    .locals 1

    const-string/jumbo v0, "universe"

    .line 12
    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p0, Lcom/squareup/http/Endpoints$Server;->CUSTOM:Lcom/squareup/http/Endpoints$Server;

    return-object p0

    :cond_0
    const-string v0, "squareupstaging.com"

    .line 13
    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p0, Lcom/squareup/http/Endpoints$Server;->STAGING:Lcom/squareup/http/Endpoints$Server;

    return-object p0

    :cond_1
    const-string v0, "localhost.mock.test"

    .line 14
    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p0, Lcom/squareup/http/Endpoints$Server;->MOCK_MODE:Lcom/squareup/http/Endpoints$Server;

    return-object p0

    :cond_2
    const-string v0, "squareup.com"

    .line 15
    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_3

    sget-object p0, Lcom/squareup/http/Endpoints$Server;->PRODUCTION:Lcom/squareup/http/Endpoints$Server;

    return-object p0

    .line 16
    :cond_3
    sget-object p0, Lcom/squareup/http/Endpoints$Server;->CUSTOM:Lcom/squareup/http/Endpoints$Server;

    return-object p0
.end method
