.class public Lcom/squareup/http/interceptor/UrlRedirectInterceptor;
.super Ljava/lang/Object;
.source "UrlRedirectInterceptor.java"

# interfaces
.implements Lokhttp3/Interceptor;


# instance fields
.field private final redirectUrlSelector:Lcom/squareup/http/RedirectUrlSelector;

.field private final redirectingServer:Lcom/squareup/http/Server;

.field private final setting:Lcom/squareup/http/UrlRedirectSetting;


# direct methods
.method public constructor <init>(Lcom/squareup/http/Server;Lcom/squareup/http/RedirectUrlSelector;Lcom/squareup/http/UrlRedirectSetting;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor;->redirectingServer:Lcom/squareup/http/Server;

    .line 36
    iput-object p2, p0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor;->redirectUrlSelector:Lcom/squareup/http/RedirectUrlSelector;

    .line 37
    iput-object p3, p0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor;->setting:Lcom/squareup/http/UrlRedirectSetting;

    return-void
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor;->setting:Lcom/squareup/http/UrlRedirectSetting;

    invoke-interface {v0}, Lcom/squareup/http/UrlRedirectSetting;->isUrlRedirectEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->request()Lokhttp3/Request;

    move-result-object v0

    invoke-interface {p1, v0}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object p1

    return-object p1

    .line 45
    :cond_0
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->request()Lokhttp3/Request;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v1

    .line 48
    invoke-virtual {v1}, Lokhttp3/HttpUrl;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor;->redirectingServer:Lcom/squareup/http/Server;

    invoke-virtual {v3}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 50
    iget-object v3, p0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor;->redirectUrlSelector:Lcom/squareup/http/RedirectUrlSelector;

    invoke-interface {v3}, Lcom/squareup/http/RedirectUrlSelector;->getBaseUrl()Lokhttp3/HttpUrl;

    move-result-object v3

    .line 51
    invoke-virtual {v1}, Lokhttp3/HttpUrl;->newBuilder()Lokhttp3/HttpUrl$Builder;

    move-result-object v1

    .line 52
    invoke-virtual {v3}, Lokhttp3/HttpUrl;->scheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lokhttp3/HttpUrl$Builder;->scheme(Ljava/lang/String;)Lokhttp3/HttpUrl$Builder;

    move-result-object v1

    .line 53
    invoke-virtual {v3}, Lokhttp3/HttpUrl;->host()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokhttp3/HttpUrl$Builder;->host(Ljava/lang/String;)Lokhttp3/HttpUrl$Builder;

    move-result-object v1

    .line 54
    invoke-virtual {v1}, Lokhttp3/HttpUrl$Builder;->build()Lokhttp3/HttpUrl;

    move-result-object v1

    .line 55
    invoke-virtual {v0}, Lokhttp3/Request;->newBuilder()Lokhttp3/Request$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->url(Lokhttp3/HttpUrl;)Lokhttp3/Request$Builder;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v0

    .line 61
    :cond_1
    :try_start_0
    invoke-interface {p1, v0}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object p1
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    if-eqz v2, :cond_2

    .line 65
    iget-object v1, p0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor;->redirectUrlSelector:Lcom/squareup/http/RedirectUrlSelector;

    invoke-virtual {v0}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/squareup/http/RedirectUrlSelector;->onUnknownHostError(Lokhttp3/HttpUrl;)V

    .line 67
    :cond_2
    throw p1
.end method
