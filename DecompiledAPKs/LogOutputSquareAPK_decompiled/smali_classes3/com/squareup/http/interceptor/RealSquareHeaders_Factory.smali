.class public final Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;
.super Ljava/lang/Object;
.source "RealSquareHeaders_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/http/interceptor/RealSquareHeaders;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final arg10Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;"
        }
    .end annotation
.end field

.field private final arg11Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final arg12Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final arg13Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final arg14Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg15Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg16Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/SpeleoIdGenerator;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/MacAddressProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/Telephony;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/MacAddressProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/Telephony;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/SpeleoIdGenerator;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 63
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg0Provider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 64
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg1Provider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 65
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg2Provider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 66
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg3Provider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 67
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg4Provider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 68
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg5Provider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 69
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg6Provider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 70
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg7Provider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 71
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg8Provider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 72
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg9Provider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 73
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg10Provider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 74
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg11Provider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 75
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg12Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 76
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg13Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 77
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg14Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 78
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg15Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 79
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg16Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/MacAddressProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/Telephony;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/SpeleoIdGenerator;",
            ">;)",
            "Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 96
    new-instance v18, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v18
.end method

.method public static newInstance(Landroid/app/Application;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/http/interceptor/MacAddressProvider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Landroid/telephony/TelephonyManager;Ljava/lang/Object;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/settings/DeviceIdProvider;Lcom/squareup/util/Clock;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Ljava/lang/Object;)Lcom/squareup/http/interceptor/RealSquareHeaders;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/http/interceptor/MacAddressProvider;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Landroid/telephony/TelephonyManager;",
            "Ljava/lang/Object;",
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/settings/DeviceIdProvider;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/squareup/http/interceptor/RealSquareHeaders;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 104
    new-instance v18, Lcom/squareup/http/interceptor/RealSquareHeaders;

    move-object/from16 v0, v18

    move-object/from16 v10, p9

    check-cast v10, Lcom/squareup/http/interceptor/Telephony;

    move-object/from16 v17, p16

    check-cast v17, Lcom/squareup/http/interceptor/SpeleoIdGenerator;

    invoke-direct/range {v0 .. v17}, Lcom/squareup/http/interceptor/RealSquareHeaders;-><init>(Landroid/app/Application;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/http/interceptor/MacAddressProvider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Landroid/telephony/TelephonyManager;Lcom/squareup/http/interceptor/Telephony;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/settings/DeviceIdProvider;Lcom/squareup/util/Clock;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/http/interceptor/SpeleoIdGenerator;)V

    return-object v18
.end method


# virtual methods
.method public get()Lcom/squareup/http/interceptor/RealSquareHeaders;
    .locals 19

    move-object/from16 v0, p0

    .line 84
    iget-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/app/Application;

    iget-object v3, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg1Provider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg2Provider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg3Provider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/http/interceptor/MacAddressProvider;

    iget-object v7, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg5Provider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg6Provider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg7Provider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Landroid/telephony/TelephonyManager;

    iget-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg9Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v11

    iget-object v12, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg10Provider:Ljavax/inject/Provider;

    iget-object v13, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg11Provider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg12Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/settings/DeviceIdProvider;

    iget-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg13Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/util/Clock;

    iget-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg14Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/settings/LocalSetting;

    iget-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg15Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/settings/LocalSetting;

    iget-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->arg16Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v18

    invoke-static/range {v2 .. v18}, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->newInstance(Landroid/app/Application;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/http/interceptor/MacAddressProvider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Landroid/telephony/TelephonyManager;Ljava/lang/Object;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/settings/DeviceIdProvider;Lcom/squareup/util/Clock;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Ljava/lang/Object;)Lcom/squareup/http/interceptor/RealSquareHeaders;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/http/interceptor/RealSquareHeaders_Factory;->get()Lcom/squareup/http/interceptor/RealSquareHeaders;

    move-result-object v0

    return-object v0
.end method
