.class public abstract Lcom/squareup/http/RealDeviceInformationModule;
.super Ljava/lang/Object;
.source "RealDeviceInformationModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideDeviceInformation()Ljava/lang/String;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 11
    sget-object v0, Lcom/squareup/http/UserAgentBuilder;->REAL_DEVICE_INFORMATION:Ljava/lang/String;

    return-object v0
.end method
