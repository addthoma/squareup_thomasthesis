.class public interface abstract Lcom/squareup/itemsorter/SortableItem;
.super Ljava/lang/Object;
.source "SortableItem.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I::",
        "Lcom/squareup/itemsorter/SortableItem<",
        "TI;TD;>;D:",
        "Lcom/squareup/itemsorter/SortableDiningOption;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0006\u0008f\u0018\u0000*\u0014\u0008\u0000\u0010\u0001*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0000*\u0008\u0008\u0001\u0010\u0002*\u00020\u00032\u00020\u0004J\u0015\u0010\u0008\u001a\u00028\u00002\u0006\u0010\u0005\u001a\u00028\u0001H&\u00a2\u0006\u0002\u0010\tR\u0014\u0010\u0005\u001a\u0004\u0018\u00018\u0001X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/itemsorter/SortableItem;",
        "I",
        "D",
        "Lcom/squareup/itemsorter/SortableDiningOption;",
        "",
        "selectedDiningOption",
        "getSelectedDiningOption",
        "()Lcom/squareup/itemsorter/SortableDiningOption;",
        "setSelectedDiningOption",
        "(Lcom/squareup/itemsorter/SortableDiningOption;)Lcom/squareup/itemsorter/SortableItem;",
        "itemsorter"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getSelectedDiningOption()Lcom/squareup/itemsorter/SortableDiningOption;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TD;"
        }
    .end annotation
.end method

.method public abstract setSelectedDiningOption(Lcom/squareup/itemsorter/SortableDiningOption;)Lcom/squareup/itemsorter/SortableItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)TI;"
        }
    .end annotation
.end method
