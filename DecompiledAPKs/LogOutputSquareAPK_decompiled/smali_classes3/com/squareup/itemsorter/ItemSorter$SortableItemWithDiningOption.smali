.class final Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;
.super Ljava/lang/Object;
.source "ItemSorter.kt"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/itemsorter/ItemSorter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SortableItemWithDiningOption"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I::",
        "Lcom/squareup/itemsorter/SortableItem<",
        "TI;TD;>;D:",
        "Lcom/squareup/itemsorter/SortableDiningOption;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption<",
        "TI;TD;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemSorter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemSorter.kt\ncom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption\n*L\n1#1,128:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0002\u0008\u000c\u0008\u0002\u0018\u0000*\u0014\u0008\u0000\u0010\u0001*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u00000\u0005B\u001f\u0008\u0000\u0012\u0006\u0010\u0006\u001a\u00028\u0000\u0012\u0006\u0010\u0007\u001a\u00028\u0001\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u001d\u0010\u001c\u001a\u00020\t2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0000H\u0096\u0002R\u001c\u0010\u000b\u001a\u00028\u0001X\u0080\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0010\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR\u001a\u0010\u0011\u001a\u00020\u0012X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R\u0014\u0010\u0008\u001a\u00020\tX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0016\u0010\u0006\u001a\u00028\u0000X\u0080\u0004\u00a2\u0006\n\n\u0002\u0010\u001b\u001a\u0004\u0008\u0019\u0010\u001a\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;",
        "I",
        "Lcom/squareup/itemsorter/SortableItem;",
        "D",
        "Lcom/squareup/itemsorter/SortableDiningOption;",
        "",
        "item",
        "cartDiningOption",
        "index",
        "",
        "(Lcom/squareup/itemsorter/SortableItem;Lcom/squareup/itemsorter/SortableDiningOption;I)V",
        "diningOption",
        "getDiningOption$itemsorter",
        "()Lcom/squareup/itemsorter/SortableDiningOption;",
        "setDiningOption$itemsorter",
        "(Lcom/squareup/itemsorter/SortableDiningOption;)V",
        "Lcom/squareup/itemsorter/SortableDiningOption;",
        "hasCartLevelDiningOption",
        "",
        "getHasCartLevelDiningOption$itemsorter",
        "()Z",
        "setHasCartLevelDiningOption$itemsorter",
        "(Z)V",
        "getIndex$itemsorter",
        "()I",
        "getItem$itemsorter",
        "()Lcom/squareup/itemsorter/SortableItem;",
        "Lcom/squareup/itemsorter/SortableItem;",
        "compareTo",
        "other",
        "itemsorter"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private diningOption:Lcom/squareup/itemsorter/SortableDiningOption;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field private hasCartLevelDiningOption:Z

.field private final index:I

.field private final item:Lcom/squareup/itemsorter/SortableItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TI;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/itemsorter/SortableItem;Lcom/squareup/itemsorter/SortableDiningOption;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TI;TD;I)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cartDiningOption"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->item:Lcom/squareup/itemsorter/SortableItem;

    iput p3, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->index:I

    .line 103
    iget-object p1, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->item:Lcom/squareup/itemsorter/SortableItem;

    invoke-interface {p1}, Lcom/squareup/itemsorter/SortableItem;->getSelectedDiningOption()Lcom/squareup/itemsorter/SortableDiningOption;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 104
    iget-object p1, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->item:Lcom/squareup/itemsorter/SortableItem;

    invoke-interface {p1}, Lcom/squareup/itemsorter/SortableItem;->getSelectedDiningOption()Lcom/squareup/itemsorter/SortableDiningOption;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->hasCartLevelDiningOption:Z

    .line 105
    iget-object p1, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->item:Lcom/squareup/itemsorter/SortableItem;

    invoke-interface {p1}, Lcom/squareup/itemsorter/SortableItem;->getSelectedDiningOption()Lcom/squareup/itemsorter/SortableDiningOption;

    move-result-object p1

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->diningOption:Lcom/squareup/itemsorter/SortableDiningOption;

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Required value was null."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_1
    const/4 p1, 0x1

    .line 107
    iput-boolean p1, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->hasCartLevelDiningOption:Z

    .line 108
    iput-object p2, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->diningOption:Lcom/squareup/itemsorter/SortableDiningOption;

    :goto_0
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption<",
            "TI;TD;>;)I"
        }
    .end annotation

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    invoke-static {}, Lcom/squareup/util/ComparisonChain;->start()Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 119
    iget-boolean v1, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->hasCartLevelDiningOption:Z

    const/4 v2, -0x1

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    const/4 v1, -0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    .line 120
    :goto_0
    iget-boolean v4, p1, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->hasCartLevelDiningOption:Z

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x1

    .line 118
    :goto_1
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/ComparisonChain;->compare(II)Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 122
    iget-object v1, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->diningOption:Lcom/squareup/itemsorter/SortableDiningOption;

    check-cast v1, Ljava/lang/Comparable;

    iget-object v2, p1, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->diningOption:Lcom/squareup/itemsorter/SortableDiningOption;

    check-cast v2, Ljava/lang/Comparable;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/ComparisonChain;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 123
    iget v1, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->index:I

    iget p1, p1, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->index:I

    invoke-virtual {v0, v1, p1}, Lcom/squareup/util/ComparisonChain;->compare(II)Lcom/squareup/util/ComparisonChain;

    move-result-object p1

    .line 124
    invoke-virtual {p1}, Lcom/squareup/util/ComparisonChain;->result()I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 91
    check-cast p1, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;

    invoke-virtual {p0, p1}, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->compareTo(Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;)I

    move-result p1

    return p1
.end method

.method public final getDiningOption$itemsorter()Lcom/squareup/itemsorter/SortableDiningOption;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TD;"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->diningOption:Lcom/squareup/itemsorter/SortableDiningOption;

    return-object v0
.end method

.method public final getHasCartLevelDiningOption$itemsorter()Z
    .locals 1

    .line 99
    iget-boolean v0, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->hasCartLevelDiningOption:Z

    return v0
.end method

.method public final getIndex$itemsorter()I
    .locals 1

    .line 95
    iget v0, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->index:I

    return v0
.end method

.method public final getItem$itemsorter()Lcom/squareup/itemsorter/SortableItem;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TI;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->item:Lcom/squareup/itemsorter/SortableItem;

    return-object v0
.end method

.method public final setDiningOption$itemsorter(Lcom/squareup/itemsorter/SortableDiningOption;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    iput-object p1, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->diningOption:Lcom/squareup/itemsorter/SortableDiningOption;

    return-void
.end method

.method public final setHasCartLevelDiningOption$itemsorter(Z)V
    .locals 0

    .line 99
    iput-boolean p1, p0, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->hasCartLevelDiningOption:Z

    return-void
.end method
