.class public final Lcom/squareup/crmviewcustomer/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crmviewcustomer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final book_new_appointment_with_customer:I = 0x7f12018e

.field public static final book_new_appointment_with_customer_shortened:I = 0x7f12018f

.field public static final confirm:I = 0x7f12047c

.field public static final crm_activity_list_header_uppercase_v2:I = 0x7f1205f2

.field public static final crm_activity_list_view_all_v2:I = 0x7f1205f4

.field public static final crm_activity_summary_customer_since:I = 0x7f1205f5

.field public static final crm_activity_summary_last_visited:I = 0x7f1205f6

.field public static final crm_activity_summary_title_avg_spend:I = 0x7f1205f7

.field public static final crm_activity_summary_visit_frequency:I = 0x7f1205f8

.field public static final crm_add_customer_title:I = 0x7f1205ff

.field public static final crm_add_customer_to_invoice_title:I = 0x7f120600

.field public static final crm_add_note_label:I = 0x7f120605

.field public static final crm_add_to_sale:I = 0x7f120608

.field public static final crm_additional_info_title:I = 0x7f12060e

.field public static final crm_all_past_appointments_title:I = 0x7f120615

.field public static final crm_all_upcoming_appointments_title:I = 0x7f120616

.field public static final crm_book_appointment:I = 0x7f12061a

.field public static final crm_born_on_format:I = 0x7f12061b

.field public static final crm_buyer_summary_title:I = 0x7f12061c

.field public static final crm_cardonfile_addcard_button:I = 0x7f12061e

.field public static final crm_cardonfile_addcard_dropdown:I = 0x7f12061f

.field public static final crm_cardonfile_expiry:I = 0x7f120626

.field public static final crm_cardonfile_expiry_phone:I = 0x7f120627

.field public static final crm_cardonfile_expiry_tablet:I = 0x7f120628

.field public static final crm_cardonfile_savecard_new_customer:I = 0x7f12062d

.field public static final crm_cardonfile_unlink_confirm:I = 0x7f120634

.field public static final crm_cardonfile_unlink_confirm_body:I = 0x7f120635

.field public static final crm_contact_deleting_error:I = 0x7f120659

.field public static final crm_contact_loading_error:I = 0x7f12065c

.field public static final crm_coupons_and_rewards_manage_action:I = 0x7f120667

.field public static final crm_coupons_and_rewards_void_confirm_plural:I = 0x7f12066b

.field public static final crm_coupons_and_rewards_void_confirm_single:I = 0x7f12066c

.field public static final crm_coupons_and_rewards_void_confirm_title:I = 0x7f12066d

.field public static final crm_coupons_uppercase:I = 0x7f120672

.field public static final crm_coupons_view_all:I = 0x7f120673

.field public static final crm_create_customer_label:I = 0x7f120674

.field public static final crm_create_new_customer_label_format:I = 0x7f12067b

.field public static final crm_created:I = 0x7f12067e

.field public static final crm_creation_source_title:I = 0x7f12067f

.field public static final crm_creation_source_unknown:I = 0x7f120680

.field public static final crm_delete_customer:I = 0x7f12069c

.field public static final crm_display_name_label:I = 0x7f1206a2

.field public static final crm_edit_personal_information_label:I = 0x7f1206ae

.field public static final crm_frequent_items_heading_uppercase:I = 0x7f1206d0

.field public static final crm_frequent_items_section_all_items:I = 0x7f1206d2

.field public static final crm_grouped_in_format:I = 0x7f1206da

.field public static final crm_invoice_details_all:I = 0x7f1206e4

.field public static final crm_invoice_list_header:I = 0x7f1206e5

.field public static final crm_invoice_row:I = 0x7f1206e6

.field public static final crm_invoices_uppercase:I = 0x7f1206e7

.field public static final crm_loyalty_delete_account_overflow:I = 0x7f1206f5

.field public static final crm_loyalty_lifetime_points:I = 0x7f1206fa

.field public static final crm_loyalty_member_since:I = 0x7f1206fb

.field public static final crm_loyalty_program_null_state:I = 0x7f1206fc

.field public static final crm_loyalty_program_phone:I = 0x7f1206fd

.field public static final crm_loyalty_program_section_adjust_status:I = 0x7f1206fe

.field public static final crm_loyalty_program_section_copy_phone:I = 0x7f120700

.field public static final crm_loyalty_program_section_edit_phone:I = 0x7f120701

.field public static final crm_loyalty_program_section_header_uppercase:I = 0x7f120702

.field public static final crm_loyalty_program_section_see_all_reward_tiers:I = 0x7f120703

.field public static final crm_loyalty_program_section_send_status:I = 0x7f120704

.field public static final crm_loyalty_program_section_send_status_body:I = 0x7f120705

.field public static final crm_loyalty_program_section_send_status_confirm:I = 0x7f120706

.field public static final crm_loyalty_program_section_view_expiring_points:I = 0x7f120707

.field public static final crm_loyalty_program_section_view_expiring_points_empty:I = 0x7f120708

.field public static final crm_loyalty_program_section_view_expiring_points_error:I = 0x7f120709

.field public static final crm_loyalty_transfer_account:I = 0x7f12070d

.field public static final crm_loyalty_transfer_account_overflow:I = 0x7f12070e

.field public static final crm_loyalty_transfer_account_short:I = 0x7f12070f

.field public static final crm_loyalty_transferring_account:I = 0x7f120710

.field public static final crm_loyalty_transferring_failure:I = 0x7f120711

.field public static final crm_loyalty_transferring_success:I = 0x7f120712

.field public static final crm_merge_customer_profile_label:I = 0x7f120714

.field public static final crm_merge_customers_label:I = 0x7f120716

.field public static final crm_merge_label:I = 0x7f120717

.field public static final crm_merged_customers_format:I = 0x7f120718

.field public static final crm_merging_customers_error:I = 0x7f120719

.field public static final crm_merging_customers_format:I = 0x7f12071a

.field public static final crm_notes_section_all_notes:I = 0x7f120724

.field public static final crm_notes_section_header_uppercase:I = 0x7f120725

.field public static final crm_profile_attachments_add_files:I = 0x7f120730

.field public static final crm_profile_attachments_header_uppercase:I = 0x7f12073b

.field public static final crm_profile_attachments_upload_files:I = 0x7f120744

.field public static final crm_remove_customer_from_estimate:I = 0x7f120754

.field public static final crm_remove_customer_from_estimate_shortened:I = 0x7f120755

.field public static final crm_remove_customer_from_invoice:I = 0x7f120756

.field public static final crm_remove_customer_from_invoice_shortened:I = 0x7f120757

.field public static final crm_remove_customer_from_sale:I = 0x7f120758

.field public static final crm_remove_customer_from_sale_shortened:I = 0x7f120759

.field public static final crm_section_view_all:I = 0x7f120769

.field public static final crm_send_message_label:I = 0x7f120775

.field public static final crm_update_loyalty_conflict_message:I = 0x7f12077a

.field public static final crm_update_loyalty_conflict_negative:I = 0x7f12077b

.field public static final crm_update_loyalty_conflict_neutral:I = 0x7f12077c

.field public static final crm_update_loyalty_conflict_positive:I = 0x7f12077d

.field public static final crm_update_loyalty_conflict_title:I = 0x7f12077e

.field public static final crm_update_loyalty_phone_error:I = 0x7f12077f

.field public static final crm_update_loyalty_phone_hint:I = 0x7f120780

.field public static final crm_update_loyalty_phone_title:I = 0x7f120781

.field public static final edit:I = 0x7f1208f9

.field public static final new_sale_with_customer:I = 0x7f12106c

.field public static final new_sale_with_customer_shortened:I = 0x7f12106d

.field public static final no:I = 0x7f12107a

.field public static final open_menu:I = 0x7f121168

.field public static final outstanding:I = 0x7f1212f9

.field public static final uppercase_card_on_file:I = 0x7f121b0c

.field public static final yes:I = 0x7f121bf0


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
