.class public final Lcom/squareup/crmscreens/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crmscreens/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final compact_loyalty_reward_row:I = 0x7f0d00ed

.field public static final crm_activity_list_row:I = 0x7f0d010d

.field public static final crm_add_coupon_view:I = 0x7f0d010f

.field public static final crm_adjust_points:I = 0x7f0d0111

.field public static final crm_all_frequent_items_view:I = 0x7f0d0112

.field public static final crm_all_notes_view:I = 0x7f0d0113

.field public static final crm_birthday_picker_view:I = 0x7f0d0116

.field public static final crm_birthday_popup:I = 0x7f0d0117

.field public static final crm_conversation_card_view:I = 0x7f0d0120

.field public static final crm_conversation_view:I = 0x7f0d0124

.field public static final crm_create_group_view:I = 0x7f0d0125

.field public static final crm_create_note_view:I = 0x7f0d0126

.field public static final crm_customer_activity_view:I = 0x7f0d0128

.field public static final crm_frequent_item_row:I = 0x7f0d0143

.field public static final crm_reminder_view:I = 0x7f0d0158

.field public static final crm_send_message_view:I = 0x7f0d0160

.field public static final crm_v2_expiring_points_row:I = 0x7f0d017e

.field public static final crm_v2_expiring_points_view:I = 0x7f0d017f

.field public static final crm_v2_manage_coupons_and_rewards_row_checkable:I = 0x7f0d0187

.field public static final crm_v2_manage_coupons_and_rewards_row_uncheckable:I = 0x7f0d0188

.field public static final crm_v2_manage_coupons_and_rewards_screen:I = 0x7f0d0189

.field public static final crm_v2_see_all_reward_tiers:I = 0x7f0d0195

.field public static final crm_v2_view_all_coupons_and_rewards_row:I = 0x7f0d0199

.field public static final crm_v2_view_all_coupons_and_rewards_screen:I = 0x7f0d019a

.field public static final crm_view_loyalty_balance:I = 0x7f0d01a1

.field public static final crm_view_note_view:I = 0x7f0d01a2

.field public static final hud_progress_view:I = 0x7f0d02cb


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
