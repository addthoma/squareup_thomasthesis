.class public Lcom/squareup/configure/item/WorkingDiscount;
.super Ljava/lang/Object;
.source "WorkingDiscount.java"


# static fields
.field private static final ZERO:Ljava/lang/String; = "0"


# instance fields
.field public amountMoney:Lcom/squareup/protos/common/Money;

.field private final catalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

.field public percentageString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 2

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/configure/item/WorkingDiscount;->catalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    const-string p1, "0"

    .line 27
    iput-object p1, p0, Lcom/squareup/configure/item/WorkingDiscount;->percentageString:Ljava/lang/String;

    const-wide/16 v0, 0x0

    .line 28
    invoke-static {v0, v1, p2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/WorkingDiscount;->amountMoney:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public finish(Z)Lcom/squareup/checkout/Discount;
    .locals 3

    .line 45
    new-instance v0, Lcom/squareup/checkout/Discount$Builder;

    iget-object v1, p0, Lcom/squareup/configure/item/WorkingDiscount;->catalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-direct {v0, v1}, Lcom/squareup/checkout/Discount$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    .line 46
    invoke-virtual {p0}, Lcom/squareup/configure/item/WorkingDiscount;->isPercentage()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    iget-object v1, p0, Lcom/squareup/configure/item/WorkingDiscount;->percentageString:Ljava/lang/String;

    sget-object v2, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-static {v1, v2}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v1

    .line 48
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Discount$Builder;->percentage(Lcom/squareup/util/Percentage;)Lcom/squareup/checkout/Adjustment$Builder;

    goto :goto_0

    .line 50
    :cond_0
    iget-object v1, p0, Lcom/squareup/configure/item/WorkingDiscount;->amountMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Discount$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 53
    :goto_0
    sget-object v1, Lcom/squareup/checkout/Discount$Scope;->CART:Lcom/squareup/checkout/Discount$Scope;

    invoke-static {v0, v1, p1}, Lcom/squareup/checkout/Discounts;->toAppliedDiscount(Lcom/squareup/checkout/Discount$Builder;Lcom/squareup/checkout/Discount$Scope;Z)Lcom/squareup/checkout/Discount;

    move-result-object p1

    return-object p1
.end method

.method public getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingDiscount;->catalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingDiscount;->catalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isPercentage()Z
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingDiscount;->catalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/Discount$DiscountType;->VARIABLE_PERCENTAGE:Lcom/squareup/api/items/Discount$DiscountType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
