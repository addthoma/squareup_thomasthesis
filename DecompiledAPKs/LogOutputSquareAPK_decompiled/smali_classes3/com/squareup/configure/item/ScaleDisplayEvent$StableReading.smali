.class public final Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;
.super Lcom/squareup/configure/item/ScaleDisplayEvent;
.source "ScaleDataHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ScaleDisplayEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StableReading"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\'\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J3\u0010\u0017\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u0007H\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;",
        "Lcom/squareup/configure/item/ScaleDisplayEvent;",
        "hardwareScale",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "measurementUnit",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        "precision",
        "",
        "weight",
        "Ljava/math/BigDecimal;",
        "(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;ILjava/math/BigDecimal;)V",
        "getHardwareScale",
        "()Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "getMeasurementUnit",
        "()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        "getPrecision",
        "()I",
        "getWeight",
        "()Ljava/math/BigDecimal;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

.field private final measurementUnit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

.field private final precision:I

.field private final weight:Ljava/math/BigDecimal;


# direct methods
.method public constructor <init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;ILjava/math/BigDecimal;)V
    .locals 1

    const-string v0, "measurementUnit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "weight"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 57
    invoke-direct {p0, v0}, Lcom/squareup/configure/item/ScaleDisplayEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    iput-object p2, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->measurementUnit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iput p3, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->precision:I

    iput-object p4, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->weight:Ljava/math/BigDecimal;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;ILjava/math/BigDecimal;ILjava/lang/Object;)Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->measurementUnit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->precision:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->weight:Ljava/math/BigDecimal;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->copy(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;ILjava/math/BigDecimal;)Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/scales/ScaleTracker$HardwareScale;
    .locals 1

    iget-object v0, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .locals 1

    iget-object v0, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->measurementUnit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->precision:I

    return v0
.end method

.method public final component4()Ljava/math/BigDecimal;
    .locals 1

    iget-object v0, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->weight:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public final copy(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;ILjava/math/BigDecimal;)Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;
    .locals 1

    const-string v0, "measurementUnit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "weight"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;ILjava/math/BigDecimal;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;

    iget-object v0, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    iget-object v1, p1, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->measurementUnit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iget-object v1, p1, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->measurementUnit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->precision:I

    iget v1, p1, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->precision:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->weight:Ljava/math/BigDecimal;

    iget-object p1, p1, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->weight:Ljava/math/BigDecimal;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHardwareScale()Lcom/squareup/scales/ScaleTracker$HardwareScale;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    return-object v0
.end method

.method public final getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->measurementUnit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    return-object v0
.end method

.method public final getPrecision()I
    .locals 1

    .line 55
    iget v0, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->precision:I

    return v0
.end method

.method public final getWeight()Ljava/math/BigDecimal;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->weight:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->measurementUnit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->precision:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->weight:Ljava/math/BigDecimal;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StableReading(hardwareScale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->hardwareScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", measurementUnit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->measurementUnit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", precision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->precision:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", weight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->weight:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
