.class public final synthetic Lcom/squareup/configure/item/-$$Lambda$cM990Qe-GXnf5uqAPJ7khs9o8cM;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher$OnUnitQuantityChangedListener;


# instance fields
.field private final synthetic f$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/configure/item/-$$Lambda$cM990Qe-GXnf5uqAPJ7khs9o8cM;->f$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    return-void
.end method


# virtual methods
.method public final onUnitQuantityChanged(Ljava/math/BigDecimal;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/configure/item/-$$Lambda$cM990Qe-GXnf5uqAPJ7khs9o8cM;->f$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onUnitQuantityChanged(Ljava/math/BigDecimal;)V

    return-void
.end method
