.class public final Lcom/squareup/configure/item/ConfigureItemScreensKt;
.super Ljava/lang/Object;
.source "ConfigureItemScreens.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0000\u001a\u001a\u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u001a\u000c\u0010\u0005\u001a\u00020\u0006*\u00020\u0007H\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "shouldClearQuantity",
        "",
        "previousVariation",
        "Lcom/squareup/checkout/OrderVariation;",
        "selectedVariation",
        "headerResourceId",
        "",
        "Lcom/squareup/orders/model/Order$QuantityUnit;",
        "configure-item_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final headerResourceId(Lcom/squareup/orders/model/Order$QuantityUnit;)I
    .locals 1

    const-string v0, "$this$headerResourceId"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    if-eqz v0, :cond_0

    sget p0, Lcom/squareup/configure/item/R$string;->uppercase_cart_quantity_header_weight:I

    goto :goto_0

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    if-eqz v0, :cond_1

    sget p0, Lcom/squareup/configure/item/R$string;->uppercase_cart_quantity_header_volume:I

    goto :goto_0

    .line 38
    :cond_1
    iget-object v0, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    if-eqz v0, :cond_2

    sget p0, Lcom/squareup/configure/item/R$string;->uppercase_cart_quantity_header_length:I

    goto :goto_0

    .line 39
    :cond_2
    iget-object p0, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    if-eqz p0, :cond_3

    sget p0, Lcom/squareup/configure/item/R$string;->uppercase_cart_quantity_header_area:I

    goto :goto_0

    .line 40
    :cond_3
    sget p0, Lcom/squareup/configure/item/R$string;->uppercase_cart_quantity_header:I

    :goto_0
    return p0
.end method

.method public static final shouldClearQuantity(Lcom/squareup/checkout/OrderVariation;Lcom/squareup/checkout/OrderVariation;)Z
    .locals 4

    const/4 v0, 0x1

    if-eqz p0, :cond_4

    if-nez p1, :cond_0

    goto :goto_1

    .line 22
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/checkout/OrderVariation;->getQuantityUnit()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/checkout/OrderVariation;->getQuantityUnit()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getQuantityUnit()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 27
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result p0

    if-nez p0, :cond_2

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result p0

    if-eqz p0, :cond_3

    :cond_2
    const/4 v2, 0x1

    :cond_3
    :goto_0
    return v2

    :cond_4
    :goto_1
    return v0
.end method
