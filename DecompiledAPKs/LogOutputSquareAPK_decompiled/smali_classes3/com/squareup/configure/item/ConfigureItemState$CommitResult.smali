.class public final enum Lcom/squareup/configure/item/ConfigureItemState$CommitResult;
.super Ljava/lang/Enum;
.source "ConfigureItemState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CommitResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/configure/item/ConfigureItemState$CommitResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

.field public static final enum EXCEEDS_GIFT_CARD_MAX:Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

.field public static final enum NO_SELECTED_VARIATION:Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

.field public static final enum SUCCESS:Lcom/squareup/configure/item/ConfigureItemState$CommitResult;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 57
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    const/4 v1, 0x0

    const-string v2, "SUCCESS"

    invoke-direct {v0, v2, v1}, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->SUCCESS:Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    .line 58
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    const/4 v2, 0x1

    const-string v3, "NO_SELECTED_VARIATION"

    invoke-direct {v0, v3, v2}, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->NO_SELECTED_VARIATION:Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    .line 59
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    const/4 v3, 0x2

    const-string v4, "EXCEEDS_GIFT_CARD_MAX"

    invoke-direct {v0, v4, v3}, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->EXCEEDS_GIFT_CARD_MAX:Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    .line 56
    sget-object v4, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->SUCCESS:Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->NO_SELECTED_VARIATION:Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->EXCEEDS_GIFT_CARD_MAX:Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->$VALUES:[Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/configure/item/ConfigureItemState$CommitResult;
    .locals 1

    .line 56
    const-class v0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/configure/item/ConfigureItemState$CommitResult;
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->$VALUES:[Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    invoke-virtual {v0}, [Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    return-object v0
.end method
