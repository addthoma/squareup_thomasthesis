.class public abstract Lcom/squareup/configure/item/ConfigureItemCompScreen$Module;
.super Ljava/lang/Object;
.source "ConfigureItemCompScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemCompScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideVoidCompPresenter(Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;)Lcom/squareup/configure/item/VoidCompPresenter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
