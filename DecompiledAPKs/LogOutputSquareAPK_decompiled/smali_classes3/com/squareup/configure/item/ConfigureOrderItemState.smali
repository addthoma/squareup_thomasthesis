.class public Lcom/squareup/configure/item/ConfigureOrderItemState;
.super Lcom/squareup/configure/item/ConfigureItemState;
.source "ConfigureOrderItemState.java"


# static fields
.field private static final INDEX_TO_CONFIGURE_KEY:Ljava/lang/String; = "INDEX_TO_CONFIGURE_KEY"

.field private static final IS_CURRENT_VARIATION_NULL_KEY:Ljava/lang/String; = "IS_CURRENT_VARIATION_NULL_KEY"


# instance fields
.field private final currency:Lcom/squareup/protos/common/CurrencyCode;

.field private currentVariation:Lcom/squareup/checkout/OrderVariation;

.field private final host:Lcom/squareup/configure/item/ConfigureItemHost;

.field private indexToConfigure:I

.field private orderItem:Lcom/squareup/checkout/CartItem;

.field private final orderItemBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method private constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)V"
        }
    .end annotation

    .line 86
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemState;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 88
    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 89
    iput-object p3, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    .line 90
    iput-object p4, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 91
    iput-object p5, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItemBundleKey:Lcom/squareup/BundleKey;

    return-void
.end method

.method public static emptyOrderItemState(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;)Lcom/squareup/configure/item/ConfigureOrderItemState;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Lcom/squareup/configure/item/ConfigureOrderItemState;"
        }
    .end annotation

    .line 72
    new-instance v6, Lcom/squareup/configure/item/ConfigureOrderItemState;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/configure/item/ConfigureOrderItemState;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;)V

    return-object v6
.end method

.method private getExistingSeats()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;"
        }
    .end annotation

    .line 452
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderDestination;->getSeats()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private loadItem(I)V
    .locals 1

    .line 96
    iput p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->indexToConfigure:I

    .line 99
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-interface {v0, p1}, Lcom/squareup/configure/item/ConfigureItemHost;->getOrderItemCopy(I)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    .line 100
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->currentVariation:Lcom/squareup/checkout/OrderVariation;

    return-void
.end method

.method public static loadedOrderItemState(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/protos/common/CurrencyCode;ILcom/squareup/BundleKey;)Lcom/squareup/configure/item/ConfigureOrderItemState;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "I",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Lcom/squareup/configure/item/ConfigureOrderItemState;"
        }
    .end annotation

    .line 79
    new-instance v6, Lcom/squareup/configure/item/ConfigureOrderItemState;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/configure/item/ConfigureOrderItemState;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;)V

    .line 81
    invoke-direct {v6, p4}, Lcom/squareup/configure/item/ConfigureOrderItemState;->loadItem(I)V

    return-object v6
.end method


# virtual methods
.method public cacheHasHiddenModifier(Z)V
    .locals 1

    .line 318
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 319
    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->hasHiddenModifier(Z)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 320
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    return-void
.end method

.method public clearQuantityPrecisionOverride()V
    .locals 0

    return-void
.end method

.method public commit()Lcom/squareup/configure/item/ConfigureItemState$CommitResult;
    .locals 7

    .line 152
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-nez v0, :cond_0

    .line 153
    sget-object v0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->NO_SELECTED_VARIATION:Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    return-object v0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardTotalPurchaseMaximum()Ljava/lang/Long;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 158
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    iget v2, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->indexToConfigure:I

    invoke-interface {v1, v2}, Lcom/squareup/configure/item/ConfigureItemHost;->getOrderItem(I)Lcom/squareup/checkout/CartItem;

    move-result-object v1

    .line 159
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->baseAmount()J

    move-result-wide v1

    .line 160
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->baseAmount()J

    move-result-wide v3

    .line 161
    iget-object v5, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-interface {v5}, Lcom/squareup/configure/item/ConfigureItemHost;->getGiftCardTotal()Lcom/squareup/protos/common/Money;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    sub-long/2addr v5, v1

    add-long/2addr v5, v3

    .line 162
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v2, v5, v0

    if-lez v2, :cond_1

    .line 163
    sget-object v0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->EXCEEDS_GIFT_CARD_MAX:Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    return-object v0

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    iget v1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->indexToConfigure:I

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-interface {v0, v1, v2}, Lcom/squareup/configure/item/ConfigureItemHost;->replaceItem(ILcom/squareup/checkout/CartItem;)V

    .line 168
    sget-object v0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->SUCCESS:Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    return-object v0
.end method

.method public compItem(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/client/Employee;)V
    .locals 3

    .line 135
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    iget v1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->indexToConfigure:I

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-interface {v0, v1, p1, v2, p2}, Lcom/squareup/configure/item/ConfigureItemHost;->compItem(ILcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)V

    return-void
.end method

.method public delete()V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    iget v1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->indexToConfigure:I

    invoke-interface {v0, v1}, Lcom/squareup/configure/item/ConfigureItemHost;->removeItem(I)V

    return-void
.end method

.method public getAppliedDiscounts()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 341
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    return-object v0
.end method

.method public getAppliedTaxes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 324
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    return-object v0
.end method

.method public getBackingType()Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    return-object v0
.end method

.method protected getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 444
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->currency:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method

.method public getCurrentVariablePrice()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 288
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->variablePrice:Lcom/squareup/protos/common/Money;

    :goto_0
    return-object v0
.end method

.method public getDuration()Lorg/threeten/bp/Duration;
    .locals 2

    .line 399
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->service_duration:Ljava/lang/Long;

    .line 400
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 399
    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v0

    return-object v0
.end method

.method public getEvents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;"
        }
    .end annotation

    .line 448
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->events:Ljava/util/List;

    return-object v0
.end method

.method public getIntermissions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;"
        }
    .end annotation

    .line 420
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->intermissions:Ljava/util/List;

    return-object v0
.end method

.method public getItemDescription()Ljava/lang/String;
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->itemDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getItemOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation

    .line 440
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->itemOptions:Ljava/util/List;

    return-object v0
.end method

.method public getItemType()Lcom/squareup/api/items/Item$Type;
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v0

    return-object v0
.end method

.method public getModifierLists()Ljava/util/SortedMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifierList;",
            ">;"
        }
    .end annotation

    .line 310
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->modifierLists:Ljava/util/SortedMap;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 365
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    return-object v0
.end method

.method public getNote()Ljava/lang/String;
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    return-object v0
.end method

.method public getOverridePrice()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 296
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->overridePrice:Lcom/squareup/protos/common/Money;

    :goto_0
    return-object v0
.end method

.method public getQuantity()Ljava/math/BigDecimal;
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getQuantityEntryType()Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;
    .locals 1

    .line 202
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    return-object v0
.end method

.method public getQuantityPrecision()I
    .locals 1

    .line 196
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 198
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getQuantityPrecision()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedModifiers()Ljava/util/SortedMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;>;"
        }
    .end annotation

    .line 314
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    return-object v0
.end method

.method public getSelectedVariation()Lcom/squareup/checkout/OrderVariation;
    .locals 1

    .line 239
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->currentVariation:Lcom/squareup/checkout/OrderVariation;

    return-object v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 306
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getUnitAbbreviation()Ljava/lang/String;
    .locals 1

    .line 222
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    .line 223
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getVariations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/OrderVariation;",
            ">;"
        }
    .end annotation

    .line 235
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->variations:Ljava/util/List;

    return-object v0
.end method

.method public isCompable()Z
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isConfigurationLockedFromScale()Z
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->READ_FROM_SCALE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isConfigurationLockedFromTicket()Z
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-boolean v0, v0, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    return v0
.end method

.method public isDeletable()Z
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->isVoidable()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isGiftCard()Z
    .locals 1

    .line 386
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v0

    return v0
.end method

.method public isService()Z
    .locals 1

    .line 394
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isService()Z

    move-result v0

    return v0
.end method

.method public isUncompable()Z
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isUnitPriced()Z
    .locals 1

    .line 218
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isVoidable()Z
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    .line 125
    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemHost;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-boolean v0, v0, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 369
    invoke-super {p0, p1}, Lcom/squareup/configure/item/ConfigureItemState;->onLoad(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    .line 371
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItemBundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    const-string v0, "INDEX_TO_CONFIGURE_KEY"

    .line 372
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->indexToConfigure:I

    const-string v0, "IS_CURRENT_VARIATION_NULL_KEY"

    .line 374
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    :goto_0
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->currentVariation:Lcom/squareup/checkout/OrderVariation;

    :cond_1
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 379
    invoke-super {p0, p1}, Lcom/squareup/configure/item/ConfigureItemState;->onSave(Landroid/os/Bundle;)V

    .line 380
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItemBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 381
    iget v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->indexToConfigure:I

    const-string v1, "INDEX_TO_CONFIGURE_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 382
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->currentVariation:Lcom/squareup/checkout/OrderVariation;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "IS_CURRENT_VARIATION_NULL_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public orderDestination()Lcom/squareup/checkout/OrderDestination;
    .locals 1

    .line 390
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    return-object v0
.end method

.method public setCurrentVariablePrice(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 292
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    return-void
.end method

.method public setDiscountApplied(Lcom/squareup/checkout/Discount;Z)V
    .locals 1

    const/4 v0, 0x0

    .line 345
    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->setDiscountApplied(Lcom/squareup/checkout/Discount;ZLcom/squareup/protos/client/Employee;)V

    return-void
.end method

.method public setDiscountApplied(Lcom/squareup/checkout/Discount;ZLcom/squareup/protos/client/Employee;)V
    .locals 1

    .line 349
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    if-eqz p2, :cond_0

    .line 351
    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->addAppliedDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/checkout/CartItem$Builder;

    if-eqz p3, :cond_1

    .line 353
    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object p1

    invoke-static {p3, p1}, Lcom/squareup/checkout/util/ItemizationEvents;->discountAddEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    goto :goto_0

    .line 356
    :cond_0
    iget-object p2, p1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/squareup/checkout/CartItem$Builder;->removeAppliedDiscount(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    if-eqz p3, :cond_1

    .line 358
    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object p1

    invoke-static {p3, p1}, Lcom/squareup/checkout/util/ItemizationEvents;->discountRemoveEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    .line 361
    :cond_1
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    return-void
.end method

.method public setDuration(Lorg/threeten/bp/Duration;)V
    .locals 3

    .line 404
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    .line 406
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    move-result-object v0

    .line 407
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->service_duration(Ljava/lang/Long;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    move-result-object p1

    .line 408
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    move-result-object p1

    .line 410
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    move-result-object v0

    .line 411
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->appointments_service_details(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    move-result-object p1

    .line 412
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    move-result-object p1

    .line 414
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 415
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getExistingSeats()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/checkout/CartItem$Builder;->featureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 416
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    return-void
.end method

.method public setIntermissions(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;)V"
        }
    .end annotation

    .line 424
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    .line 426
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    move-result-object v0

    .line 427
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->intermissions(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    move-result-object p1

    .line 428
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    move-result-object p1

    .line 430
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    move-result-object v0

    .line 431
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->appointments_service_details(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    move-result-object p1

    .line 432
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    move-result-object p1

    .line 434
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 435
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getExistingSeats()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/checkout/CartItem$Builder;->featureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 436
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    return-void
.end method

.method public setNote(Ljava/lang/String;)V
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->notes(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    return-void
.end method

.method public setOverridePrice(Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 300
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isFixedPriced()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Override price can only be set for fixed-price variations."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->overridePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    return-void
.end method

.method public setQuantity(Ljava/math/BigDecimal;)V
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    return-void
.end method

.method public setQuantityEntryType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)V
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->quantityEntryType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    return-void
.end method

.method public setQuantityPrecisionOverride(I)V
    .locals 0

    return-void
.end method

.method public setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)V
    .locals 3

    .line 272
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    .line 273
    invoke-interface {v1}, Lcom/squareup/configure/item/ConfigureItemHost;->getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem;->getDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/DiningOption;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 274
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-interface {v1}, Lcom/squareup/configure/item/ConfigureItemHost;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v1

    .line 276
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-static {v0, v1, p1}, Lcom/squareup/util/ConditionalTaxesHelper;->updateTaxesUpon(Lcom/squareup/checkout/CartItem;Ljava/util/List;Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    goto :goto_0

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    :goto_0
    if-nez p1, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    .line 281
    :cond_1
    sget-object v1, Lcom/squareup/protos/client/bills/ApplicationScope;->ITEMIZATION_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 282
    invoke-virtual {p1, v1}, Lcom/squareup/checkout/DiningOption;->copy(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/checkout/DiningOption;

    move-result-object p1

    .line 281
    :goto_1
    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->selectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem$Builder;

    .line 284
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    return-void
.end method

.method public setSelectedVariation(Lcom/squareup/checkout/OrderVariation;)V
    .locals 5

    .line 243
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getPreviousVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureOrderItemState;->getPreviousVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->clearQuantityUnitOverride()V

    .line 247
    :cond_0
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->currentVariation:Lcom/squareup/checkout/OrderVariation;

    if-eqz p1, :cond_2

    .line 250
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getQuantityUnit()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/OrderVariation;->setQuantityUnitOverride(Lcom/squareup/orders/model/Order$QuantityUnit;)V

    .line 252
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->variablePrice:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_1

    .line 253
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    const-wide/16 v2, 0x0

    iget-object v4, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 254
    invoke-static {v2, v3, v4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 255
    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation(Lcom/squareup/checkout/OrderVariation;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 256
    invoke-virtual {p1, v1}, Lcom/squareup/checkout/CartItem$Builder;->overridePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 257
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    goto :goto_0

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 260
    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation(Lcom/squareup/checkout/OrderVariation;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 261
    invoke-virtual {p1, v1}, Lcom/squareup/checkout/CartItem$Builder;->overridePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 262
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    :cond_2
    :goto_0
    return-void
.end method

.method public setTaxApplied(Lcom/squareup/checkout/Tax;Z)V
    .locals 2

    .line 328
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    .line 329
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->addUserEditedTaxIds(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    if-eqz p2, :cond_0

    .line 332
    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->addAppliedTax(Lcom/squareup/checkout/Tax;)Lcom/squareup/checkout/CartItem$Builder;

    goto :goto_0

    .line 334
    :cond_0
    iget-object p1, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->removeAppliedTax(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 337
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    return-void
.end method

.method public uncompItem()V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    iget v1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->indexToConfigure:I

    invoke-interface {v0, v1}, Lcom/squareup/configure/item/ConfigureItemHost;->uncompItem(I)V

    return-void
.end method

.method public voidItem(Ljava/lang/String;Lcom/squareup/protos/client/Employee;)V
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->orderItem:Lcom/squareup/checkout/CartItem;

    iget-boolean v0, v0, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    const-string v1, "Cannot void an unsaved item."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    iget v1, p0, Lcom/squareup/configure/item/ConfigureOrderItemState;->indexToConfigure:I

    invoke-interface {v0, v1, p1, p2}, Lcom/squareup/configure/item/ConfigureItemHost;->voidItem(ILjava/lang/String;Lcom/squareup/protos/client/Employee;)V

    return-void
.end method
