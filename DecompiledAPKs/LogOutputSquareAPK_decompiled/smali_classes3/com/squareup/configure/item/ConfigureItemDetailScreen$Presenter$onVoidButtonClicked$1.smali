.class public final Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVoidButtonClicked$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "ConfigureItemDetailScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onVoidButtonClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"
    }
    d2 = {
        "com/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVoidButtonClicked$1",
        "Lcom/squareup/permissions/PermissionGatekeeper$When;",
        "success",
        "",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1041
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVoidButtonClicked$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 1043
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVoidButtonClicked$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getScopeRunner$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->cacheCurrentVariationAndVariablePrice()V

    .line 1044
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVoidButtonClicked$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getView(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemDetailView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideKeyboard()V

    .line 1045
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVoidButtonClicked$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getAnalytics$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->VOID_ITEMIZATION_STARTED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 1046
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVoidButtonClicked$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getNavigator$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemNavigator;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVoidButtonClicked$1;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/configure/item/ConfigureItemNavigator;->goToVoidScreen(Ljava/lang/String;)V

    return-void
.end method
