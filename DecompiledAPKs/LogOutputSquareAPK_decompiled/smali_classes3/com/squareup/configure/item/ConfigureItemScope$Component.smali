.class public interface abstract Lcom/squareup/configure/item/ConfigureItemScope$Component;
.super Ljava/lang/Object;
.source "ConfigureItemScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/configure/item/ConfigureItemScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract appointmentConfigureItemDetail()Lcom/squareup/configure/item/AppointmentConfigureItemDetailScreen$Component;
.end method

.method public abstract configureItemComp()Lcom/squareup/configure/item/ConfigureItemCompScreen$Component;
.end method

.method public abstract configureItemDetail()Lcom/squareup/configure/item/ConfigureItemDetailScreen$Component;
.end method

.method public abstract configureItemPrice()Lcom/squareup/configure/item/ConfigureItemPriceScreen$Component;
.end method

.method public abstract configureItemVoid()Lcom/squareup/configure/item/ConfigureItemVoidScreen$Component;
.end method

.method public abstract host()Lcom/squareup/configure/item/ConfigureItemHost;
.end method

.method public abstract invoiceConfigureItemDetail()Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen$Component;
.end method

.method public abstract navigator()Lcom/squareup/configure/item/ConfigureItemNavigator;
.end method

.method public abstract orderEntryScreenState()Lcom/squareup/orderentry/OrderEntryScreenState;
.end method

.method public abstract scopeRunner()Lcom/squareup/configure/item/ConfigureItemScopeRunner;
.end method
