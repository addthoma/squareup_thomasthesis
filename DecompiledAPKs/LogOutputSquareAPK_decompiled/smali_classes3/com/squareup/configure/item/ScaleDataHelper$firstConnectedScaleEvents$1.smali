.class final Lcom/squareup/configure/item/ScaleDataHelper$firstConnectedScaleEvents$1;
.super Ljava/lang/Object;
.source "ScaleDataHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ScaleDataHelper;->firstConnectedScaleEvents(Lcom/squareup/scales/ScaleTracker;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a*\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0014\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/configure/item/ScaleDisplayEvent;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "Lcom/squareup/scales/Scale;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/configure/item/ScaleDataHelper$firstConnectedScaleEvents$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/configure/item/ScaleDataHelper$firstConnectedScaleEvents$1;

    invoke-direct {v0}, Lcom/squareup/configure/item/ScaleDataHelper$firstConnectedScaleEvents$1;-><init>()V

    sput-object v0, Lcom/squareup/configure/item/ScaleDataHelper$firstConnectedScaleEvents$1;->INSTANCE:Lcom/squareup/configure/item/ScaleDataHelper$firstConnectedScaleEvents$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/util/Map;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
            "+",
            "Lcom/squareup/scales/Scale;",
            ">;)",
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/configure/item/ScaleDisplayEvent;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    sget-object p1, Lcom/squareup/configure/item/ScaleDisplayEvent$NoConnectedScale;->INSTANCE:Lcom/squareup/configure/item/ScaleDisplayEvent$NoConnectedScale;

    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    .line 73
    :cond_0
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/scales/Scale;

    invoke-interface {p1}, Lcom/squareup/scales/Scale;->getScaleEvents()Lio/reactivex/Observable;

    move-result-object p1

    .line 75
    sget-object v0, Lcom/squareup/configure/item/ScaleDataHelper$firstConnectedScaleEvents$1$1;->INSTANCE:Lcom/squareup/configure/item/ScaleDataHelper$firstConnectedScaleEvents$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    if-eqz v0, :cond_1

    new-instance v1, Lcom/squareup/configure/item/ScaleDataHelper$sam$io_reactivex_functions_Function$0;

    invoke-direct {v1, v0}, Lcom/squareup/configure/item/ScaleDataHelper$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v0, v1

    :cond_1
    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ScaleDataHelper$firstConnectedScaleEvents$1;->apply(Ljava/util/Map;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
