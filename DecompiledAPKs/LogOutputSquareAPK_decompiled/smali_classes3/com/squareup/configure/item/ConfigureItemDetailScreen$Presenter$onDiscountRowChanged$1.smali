.class public final Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "ConfigureItemDetailScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onDiscountRowChanged(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "com/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1",
        "Lcom/squareup/permissions/PermissionGatekeeper$When;",
        "failure",
        "",
        "success",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $applied:Z

.field final synthetic $discount:Lcom/squareup/checkout/Discount;

.field final synthetic $discountId:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;Lcom/squareup/checkout/Discount;ZLjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/Discount;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1355
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->$discount:Lcom/squareup/checkout/Discount;

    iput-boolean p3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->$applied:Z

    iput-object p4, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->$discountId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public failure()V
    .locals 3

    .line 1373
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$hasView(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1374
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getView(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemDetailView;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->$discountId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->updateDiscountCheckedState(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public success()V
    .locals 3

    .line 1358
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getEmployeeManagement$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/permissions/EmployeeManagement;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->oneEmployeeProtoByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 1359
    sget-object v1, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$1;->INSTANCE:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 1360
    sget-object v1, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v1}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "employeeManagement.oneEm\u2026oSingle(Optional.empty())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1361
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getView(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemDetailView;

    move-result-object v1

    const-string/jumbo v2, "view"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/view/View;

    new-instance v2, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$2;

    invoke-direct {v2, p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$2;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1, v2}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Single;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
