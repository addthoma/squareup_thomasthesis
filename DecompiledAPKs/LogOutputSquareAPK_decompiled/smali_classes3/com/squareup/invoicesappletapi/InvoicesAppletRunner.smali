.class public interface abstract Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;
.super Ljava/lang/Object;
.source "InvoicesAppletRunner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoicesappletapi/InvoicesAppletRunner$ParentComponent;
    }
.end annotation


# static fields
.field public static final NOT_SUPPORTED:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    .line 14
    invoke-static {v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    sput-object v0, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->NOT_SUPPORTED:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    return-void
.end method


# virtual methods
.method public abstract cancelInvoiceConfirmation()V
.end method

.method public abstract getInstanceOfHistoryScreen()Lcom/squareup/container/ContainerTreeKey;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract historyBehindInvoiceTenderScreen(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
.end method

.method public abstract invoicePaymentCanceled(Ljava/lang/String;)V
.end method

.method public abstract invoicePaymentSucceeded()V
.end method

.method public abstract isInvoiceHistoryScreen(Lcom/squareup/container/ContainerTreeKey;)Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract isTakingInvoicePayment()Z
.end method

.method public abstract onInvoiceConfirmationCanceled()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract start()V
.end method

.method public abstract viewFullInvoiceDetail(Ljava/lang/String;)V
.end method
