.class public Lcom/squareup/currency_db/NumberFormats;
.super Ljava/lang/Object;
.source "NumberFormats.java"


# static fields
.field private static final STANDARD_DIGITS:Ljava/lang/String; = "0123456789\u00a0\u2008"


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Told you not to instantiate it!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getAllowedDigits(Ljava/util/Locale;Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;
    .locals 1

    .line 78
    invoke-static {p0, p1}, Lcom/squareup/currency_db/NumberFormats;->getCurrencyFormat(Ljava/util/Locale;Lcom/squareup/protos/common/CurrencyCode;)Ljava/text/DecimalFormat;

    move-result-object p0

    invoke-virtual {p0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object p0

    .line 79
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "0123456789\u00a0\u2008"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {p0}, Ljava/text/DecimalFormatSymbols;->getCurrencySymbol()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    invoke-virtual {p0}, Ljava/text/DecimalFormatSymbols;->getGroupingSeparator()C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 82
    invoke-virtual {p0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static getBestLocaleInfo(Ljava/util/Locale;)Lcom/squareup/currency_db/LocaleInfo;
    .locals 3

    .line 23
    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 24
    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object p0

    .line 25
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string p0, "%s_%s"

    .line 26
    invoke-static {p0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 27
    invoke-static {p0}, Lcom/squareup/currency_db/NumberFormats;->getLocaleInfoByKey(Ljava/lang/String;)Lcom/squareup/currency_db/LocaleInfo;

    move-result-object p0

    if-eqz p0, :cond_0

    return-object p0

    .line 30
    :cond_0
    invoke-static {v0}, Lcom/squareup/currency_db/NumberFormats;->getLocaleInfoByKey(Ljava/lang/String;)Lcom/squareup/currency_db/LocaleInfo;

    move-result-object p0

    if-eqz p0, :cond_1

    return-object p0

    .line 32
    :cond_1
    sget-object p0, Lcom/squareup/currency_db/LocaleInfo;->EN_US:Lcom/squareup/currency_db/LocaleInfo;

    return-object p0
.end method

.method public static getCurrencyFormat(Ljava/util/Locale;Lcom/squareup/protos/common/CurrencyCode;)Ljava/text/DecimalFormat;
    .locals 3

    .line 62
    invoke-static {p0}, Lcom/squareup/currency_db/NumberFormats;->getBestLocaleInfo(Ljava/util/Locale;)Lcom/squareup/currency_db/LocaleInfo;

    move-result-object v0

    .line 63
    invoke-static {p1}, Lcom/squareup/currency_db/Currencies;->getCurrencyInfo(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/currency_db/CurrencyInfo;

    move-result-object v1

    .line 64
    new-instance v2, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v2, p0}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 65
    invoke-virtual {p1}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/text/DecimalFormatSymbols;->setCurrency(Ljava/util/Currency;)V

    .line 66
    iget-object p0, v1, Lcom/squareup/currency_db/CurrencyInfo;->symbol:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/text/DecimalFormatSymbols;->setCurrencySymbol(Ljava/lang/String;)V

    .line 67
    iget-char p0, v0, Lcom/squareup/currency_db/LocaleInfo;->groupingSeparator:C

    invoke-virtual {v2, p0}, Ljava/text/DecimalFormatSymbols;->setGroupingSeparator(C)V

    .line 68
    iget-char p0, v0, Lcom/squareup/currency_db/LocaleInfo;->decimalSeparator:C

    invoke-virtual {v2, p0}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 69
    new-instance p0, Ljava/text/DecimalFormat;

    iget-object p1, v0, Lcom/squareup/currency_db/LocaleInfo;->currencyPattern:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 71
    iget p1, v1, Lcom/squareup/currency_db/CurrencyInfo;->fractionalDigits:I

    invoke-virtual {p0, p1}, Ljava/text/DecimalFormat;->setMinimumFractionDigits(I)V

    .line 72
    iget p1, v1, Lcom/squareup/currency_db/CurrencyInfo;->fractionalDigits:I

    invoke-virtual {p0, p1}, Ljava/text/DecimalFormat;->setMaximumFractionDigits(I)V

    return-object p0
.end method

.method public static getInstance()Ljava/text/NumberFormat;
    .locals 1

    .line 44
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/currency_db/NumberFormats;->getNumberFormat(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;
    .locals 0

    .line 48
    invoke-static {p0}, Lcom/squareup/currency_db/NumberFormats;->getNumberFormat(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object p0

    return-object p0
.end method

.method public static getIntegerInstance()Ljava/text/NumberFormat;
    .locals 1

    .line 58
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/currency_db/NumberFormats;->getIntegerInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getIntegerInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;
    .locals 1

    .line 52
    invoke-static {p0}, Lcom/squareup/currency_db/NumberFormats;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object p0

    const/4 v0, 0x0

    .line 53
    invoke-virtual {p0, v0}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    return-object p0
.end method

.method private static getLocaleInfoByKey(Ljava/lang/String;)Lcom/squareup/currency_db/LocaleInfo;
    .locals 0

    .line 16
    :try_start_0
    invoke-static {p0}, Lcom/squareup/currency_db/LocaleInfo;->valueOf(Ljava/lang/String;)Lcom/squareup/currency_db/LocaleInfo;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static getNumberFormat(Ljava/util/Locale;)Ljava/text/NumberFormat;
    .locals 2

    .line 36
    invoke-static {p0}, Lcom/squareup/currency_db/NumberFormats;->getBestLocaleInfo(Ljava/util/Locale;)Lcom/squareup/currency_db/LocaleInfo;

    move-result-object p0

    .line 37
    new-instance v0, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v0}, Ljava/text/DecimalFormatSymbols;-><init>()V

    .line 38
    iget-char v1, p0, Lcom/squareup/currency_db/LocaleInfo;->groupingSeparator:C

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormatSymbols;->setGroupingSeparator(C)V

    .line 39
    iget-char v1, p0, Lcom/squareup/currency_db/LocaleInfo;->decimalSeparator:C

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 40
    new-instance v1, Ljava/text/DecimalFormat;

    iget-object p0, p0, Lcom/squareup/currency_db/LocaleInfo;->numberPattern:Ljava/lang/String;

    invoke-direct {v1, p0, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    return-object v1
.end method
