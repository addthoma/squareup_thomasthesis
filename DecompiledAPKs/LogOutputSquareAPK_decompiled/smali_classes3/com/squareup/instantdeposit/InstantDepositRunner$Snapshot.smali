.class public final Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;
.super Ljava/lang/Object;
.source "InstantDepositRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/instantdeposit/InstantDepositRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Snapshot"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInstantDepositRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InstantDepositRunner.kt\ncom/squareup/instantdeposit/InstantDepositRunner$Snapshot\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,163:1\n250#2,2:164\n*E\n*S KotlinDebug\n*F\n+ 1 InstantDepositRunner.kt\ncom/squareup/instantdeposit/InstantDepositRunner$Snapshot\n*L\n98#1,2:164\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000p\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u00002\u00020\u0001B1\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u000b\u001a\u00020\tJ\u0006\u0010\u000c\u001a\u00020\tJ\u0006\u0010\r\u001a\u00020\u000eJ\u0006\u0010\u000f\u001a\u00020\tJ\u0008\u0010\u0010\u001a\u00020\tH\u0002J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\tH\u00c6\u0003J5\u0010\u0015\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0006\u0010\u0016\u001a\u00020\tJ\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018J\u0006\u0010\u0019\u001a\u00020\u000eJ\u0006\u0010\u001a\u001a\u00020\u001bJ\u0006\u0010\u001c\u001a\u00020\u001bJ\u0006\u0010\u001d\u001a\u00020\u001eJ\u0006\u0010\u001f\u001a\u00020\u001bJ\u0006\u0010 \u001a\u00020\u001bJ\u0013\u0010!\u001a\u00020\t2\u0008\u0010\"\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u0008\u0010#\u001a\u00020\tH\u0002J\t\u0010$\u001a\u00020%H\u00d6\u0001J\u0008\u0010&\u001a\u00020\'H\u0002J\u0008\u0010(\u001a\u0004\u0018\u00010)J\u0008\u0010*\u001a\u0004\u0018\u00010+J\u0006\u0010,\u001a\u00020\tJ\u0006\u0010-\u001a\u00020\tJ\u0006\u0010.\u001a\u00020\tJ\u0008\u0010/\u001a\u00020\tH\u0002J\u000c\u00100\u001a\u0008\u0012\u0004\u0012\u00020201J\u0008\u00103\u001a\u0004\u0018\u000104J\u0006\u00105\u001a\u00020\u000eJ\u0006\u00106\u001a\u00020\tJ\t\u00107\u001a\u00020\u001bH\u00d6\u0001J\u000e\u00108\u001a\u00020\t*\u0004\u0018\u00010\u001eH\u0002R\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0008\u001a\u00020\t8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00069"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
        "",
        "state",
        "Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;",
        "getBalanceSummaryResponse",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;",
        "createTransferResponse",
        "Lcom/squareup/protos/deposits/CreateTransferResponse;",
        "isConfirmingInstantTransfer",
        "",
        "(Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;Z)V",
        "allowAddMoney",
        "allowPartialDeposit",
        "balance",
        "Lcom/squareup/protos/common/Money;",
        "balanceSummaryLoaded",
        "canMakeDeposit",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "couldNotLoadBalance",
        "debitCardPayInEligibility",
        "Lcom/squareup/protos/client/deposits/EligibilityDetails;",
        "depositAmount",
        "depositFailedDescription",
        "",
        "depositFailedTitle",
        "eligibilityBlocker",
        "Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;",
        "eligibilityDescription",
        "eligibilityTitle",
        "equals",
        "other",
        "hasUnknownEligibilityBlocker",
        "hashCode",
        "",
        "instantDepositDetails",
        "Lcom/squareup/protos/client/deposits/InstantDepositDetails;",
        "linkedBankAccount",
        "Lcom/squareup/protos/client/bankaccount/BankAccount;",
        "linkedCard",
        "Lcom/squareup/protos/client/deposits/CardInfo;",
        "loading",
        "noLinkedBankAccount",
        "noLinkedDebitCard",
        "notEligible",
        "recentCardActivity",
        "",
        "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
        "recentDepositActivity",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;",
        "sentDepositAmount",
        "showPriceChangeModal",
        "toString",
        "isAllowedBlockerForAddMoney",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final createTransferResponse:Lcom/squareup/protos/deposits/CreateTransferResponse;

.field public final getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

.field public final isConfirmingInstantTransfer:Z

.field public final state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xf

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;Z)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    iput-object p2, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    iput-object p3, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->createTransferResponse:Lcom/squareup/protos/deposits/CreateTransferResponse;

    iput-boolean p4, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->isConfirmingInstantTransfer:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    .line 72
    sget-object p1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->LOADING:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    const/4 v0, 0x0

    if-eqz p6, :cond_1

    .line 73
    move-object p2, v0

    check-cast p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    .line 74
    move-object p3, v0

    check-cast p3, Lcom/squareup/protos/deposits/CreateTransferResponse;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    const/4 p4, 0x0

    .line 75
    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;Z)V

    return-void
.end method

.method private final canMakeDeposit()Z
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    sget-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->CAN_MAKE_DEPOSIT:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic copy$default(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;ZILjava/lang/Object;)Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->createTransferResponse:Lcom/squareup/protos/deposits/CreateTransferResponse;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->isConfirmingInstantTransfer:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->copy(Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;Z)Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    move-result-object p0

    return-object p0
.end method

.method private final hasUnknownEligibilityBlocker()Z
    .locals 2

    .line 144
    invoke-direct {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->instantDepositDetails()Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->UNKNOWN:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final instantDepositDetails()Lcom/squareup/protos/client/deposits/InstantDepositDetails;
    .locals 2

    .line 148
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    const-string v1, "getBalanceSummaryRespons\u2026!.instant_deposit_details"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final isAllowedBlockerForAddMoney(Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;)Z
    .locals 1

    if-nez p1, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->linkedCard()Lcom/squareup/protos/client/deposits/CardInfo;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    sget-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->UNVERIFIED_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    if-eq p1, v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NO_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    if-ne p1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private final notEligible()Z
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    sget-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->NOT_ELIGIBLE:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public final allowAddMoney()Z
    .locals 2

    .line 88
    invoke-virtual {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->debitCardPayInEligibility()Lcom/squareup/protos/client/deposits/EligibilityDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v1, v0, Lcom/squareup/protos/client/deposits/EligibilityDetails;->blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    invoke-direct {p0, v1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->isAllowedBlockerForAddMoney(Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/protos/client/deposits/EligibilityDetails;->min_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/deposits/EligibilityDetails;->max_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final allowPartialDeposit()Z
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->allow_partial_deposit:Ljava/lang/Boolean;

    const-string v1, "getBalanceSummaryResponse!!.allow_partial_deposit"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final balance()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance:Lcom/squareup/protos/common/Money;

    const-string v1, "getBalanceSummaryResponse!!.balance"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final balanceSummaryLoaded()Z
    .locals 1

    .line 106
    invoke-direct {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->canMakeDeposit()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->notEligible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->hasUnknownEligibilityBlocker()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public final component1()Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/deposits/CreateTransferResponse;
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->createTransferResponse:Lcom/squareup/protos/deposits/CreateTransferResponse;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->isConfirmingInstantTransfer:Z

    return v0
.end method

.method public final copy(Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;Z)Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;Lcom/squareup/protos/deposits/CreateTransferResponse;Z)V

    return-object v0
.end method

.method public final couldNotLoadBalance()Z
    .locals 1

    .line 109
    invoke-direct {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->notEligible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->hasUnknownEligibilityBlocker()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final debitCardPayInEligibility()Lcom/squareup/protos/client/deposits/EligibilityDetails;
    .locals 4

    .line 97
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->eligibility_details:Ljava/util/List;

    const-string v1, "getBalanceSummaryResponse!!.eligibility_details"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 164
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/deposits/EligibilityDetails;

    .line 98
    iget-object v2, v2, Lcom/squareup/protos/client/deposits/EligibilityDetails;->balance_activity_type:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    sget-object v3, Lcom/squareup/protos/client/deposits/BalanceActivityType;->DEBIT_CARD_PAY_IN:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    .line 165
    :goto_1
    check-cast v1, Lcom/squareup/protos/client/deposits/EligibilityDetails;

    return-object v1
.end method

.method public final depositAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 77
    invoke-direct {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->instantDepositDetails()Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->max_deposit_amount:Lcom/squareup/protos/common/Money;

    const-string v1, "instantDepositDetails().max_deposit_amount"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final depositFailedDescription()Ljava/lang/String;
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->createTransferResponse:Lcom/squareup/protos/deposits/CreateTransferResponse;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/deposits/CreateTransferResponse;->status_message:Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    iget-object v0, v0, Lcom/squareup/protos/deposits/LocalizedStatusMessage;->description:Ljava/lang/String;

    const-string v1, "createTransferResponse!!\u2026tatus_message.description"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final depositFailedTitle()Ljava/lang/String;
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->createTransferResponse:Lcom/squareup/protos/deposits/CreateTransferResponse;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/deposits/CreateTransferResponse;->status_message:Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    iget-object v0, v0, Lcom/squareup/protos/deposits/LocalizedStatusMessage;->title:Ljava/lang/String;

    const-string v1, "createTransferResponse!!.status_message.title"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final eligibilityBlocker()Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;
    .locals 2

    .line 117
    invoke-direct {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->instantDepositDetails()Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const-string v1, "instantDepositDetails().eligibility_blocker"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final eligibilityDescription()Ljava/lang/String;
    .locals 2

    .line 114
    invoke-direct {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->instantDepositDetails()Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    const-string v1, "instantDepositDetails().\u2026tus.localized_description"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final eligibilityTitle()Ljava/lang/String;
    .locals 2

    .line 111
    invoke-direct {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->instantDepositDetails()Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    const-string v1, "instantDepositDetails().\u2026ty_status.localized_title"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    iget-object v1, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    iget-object v1, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->createTransferResponse:Lcom/squareup/protos/deposits/CreateTransferResponse;

    iget-object v1, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->createTransferResponse:Lcom/squareup/protos/deposits/CreateTransferResponse;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->isConfirmingInstantTransfer:Z

    iget-boolean p1, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->isConfirmingInstantTransfer:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->createTransferResponse:Lcom/squareup/protos/deposits/CreateTransferResponse;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->isConfirmingInstantTransfer:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final linkedBankAccount()Lcom/squareup/protos/client/bankaccount/BankAccount;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    return-object v0
.end method

.method public final linkedCard()Lcom/squareup/protos/client/deposits/CardInfo;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    return-object v0
.end method

.method public final loading()Z
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    sget-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->LOADING:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final noLinkedBankAccount()Z
    .locals 2

    .line 120
    invoke-direct {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->notEligible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->eligibilityBlocker()Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NO_LINKED_BANK_ACCOUNT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final noLinkedDebitCard()Z
    .locals 2

    .line 124
    invoke-direct {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->notEligible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->eligibilityBlocker()Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NO_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final recentCardActivity()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;"
        }
    .end annotation

    .line 101
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->card_activity:Ljava/util/List;

    const-string v1, "getBalanceSummaryResponse!!.card_activity"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final recentDepositActivity()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    return-object v0
.end method

.method public final sentDepositAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->createTransferResponse:Lcom/squareup/protos/deposits/CreateTransferResponse;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/deposits/CreateTransferResponse;->balance_activity:Lcom/squareup/protos/deposits/BalanceActivity;

    iget-object v0, v0, Lcom/squareup/protos/deposits/BalanceActivity;->amount:Lcom/squareup/protos/common/Money;

    const-string v1, "createTransferResponse!!.balance_activity.amount"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final showPriceChangeModal()Z
    .locals 1

    .line 135
    invoke-direct {p0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->instantDepositDetails()Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->show_price_change_modal:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Snapshot(state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getBalanceSummaryResponse="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", createTransferResponse="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->createTransferResponse:Lcom/squareup/protos/deposits/CreateTransferResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isConfirmingInstantTransfer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->isConfirmingInstantTransfer:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
