.class public final Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;
.super Ljava/lang/Object;
.source "RealInstantDepositAnalytics.kt"

# interfaces
.implements Lcom/squareup/instantdeposit/InstantDepositAnalytics;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0019\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J \u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u000bH\u0016J\u0010\u0010\r\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\u000e\u001a\u00020\u0006H\u0016J\u0008\u0010\u000f\u001a\u00020\u0006H\u0016J \u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u000bH\u0016J\u0010\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u0008H\u0016J\u0010\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u0019\u001a\u00020\u0008H\u0016J\u0010\u0010\u001a\u001a\u00020\u00062\u0006\u0010\u001b\u001a\u00020\u000bH\u0016J\u0008\u0010\u001c\u001a\u00020\u0006H\u0016J\u0008\u0010\u001d\u001a\u00020\u0006H\u0016J\u0008\u0010\u001e\u001a\u00020\u0006H\u0016J\u0008\u0010\u001f\u001a\u00020\u0006H\u0016J\u0008\u0010 \u001a\u00020\u0006H\u0016J\u0008\u0010!\u001a\u00020\u0006H\u0016J\u0008\u0010\"\u001a\u00020\u0006H\u0016J\u0008\u0010#\u001a\u00020\u0006H\u0016J\u0008\u0010$\u001a\u00020\u0006H\u0016J\u0008\u0010%\u001a\u00020\u0006H\u0016J\u0008\u0010&\u001a\u00020\u0006H\u0016J\u0008\u0010\'\u001a\u00020\u0006H\u0016J\u0008\u0010(\u001a\u00020\u0006H\u0016J\u0008\u0010)\u001a\u00020\u0006H\u0016J\u0008\u0010*\u001a\u00020\u0006H\u0016J\u0018\u0010+\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u000bH\u0016J\u0008\u0010,\u001a\u00020\u0006H\u0016J \u0010-\u001a\u00020\u00062\u0006\u0010.\u001a\u00020/2\u0006\u00100\u001a\u0002012\u0006\u00102\u001a\u00020\u0008H\u0016J \u00103\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u000bH\u0016J\u0008\u00104\u001a\u00020\u0006H\u0016J\u0008\u00105\u001a\u00020\u0006H\u0016J\u0008\u00106\u001a\u00020\u0006H\u0016J\u0018\u00107\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0019\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00068"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;",
        "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "cancelCardLinking",
        "",
        "hasLinkedCard",
        "",
        "cardLinkFailure",
        "title",
        "",
        "message",
        "cardLinkSuccess",
        "headerDisplayedShowingAvailableBalance",
        "headerDisplayedShowingError",
        "instantDepositFailed",
        "registerErrorName",
        "Lcom/squareup/analytics/RegisterErrorName;",
        "instantDepositSucceeded",
        "registerViewName",
        "Lcom/squareup/analytics/RegisterViewName;",
        "instantDepositToggled",
        "checked",
        "logBalanceAppletAddDebitCardAttempt",
        "isSuccessful",
        "logBalanceAppletAddDebitCardFailure",
        "failureMessage",
        "logBalanceAppletAddDebitCardSuccess",
        "logBalanceAppletDisplayedAvailableBalance",
        "logBalanceAppletInstantTransferClick",
        "logBalanceAppletInstantTransferSuccess",
        "logBalanceAppletLinkDebitCardCancel",
        "logBalanceAppletSetUpInstantTransfer",
        "logBalanceAppletTransferEstimatedFeesClick",
        "logBalanceAppletTransferEstimatedFeesLearnMoreClick",
        "logBalanceAppletTransferReportsDisplayedAvailableBalance",
        "logBalanceAppletTransferReportsInstantTransferClick",
        "logBalanceAppletTransferReportsInstantTransferSuccess",
        "logTransferReportsActiveSalesClick",
        "logTransferReportsPendingDepositClick",
        "logTransferReportsSentDepositClick",
        "logTransferReportsSummaryView",
        "resendEmailFailure",
        "resendEmailSuccess",
        "tapDepositAvailableFunds",
        "registerTapName",
        "Lcom/squareup/analytics/RegisterTapName;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "isTransferReports",
        "tapLearnMore",
        "tapLinkDifferentDebitCard",
        "transferReportsDisplayedAvailableBalance",
        "transferReportsDisplayedError",
        "tryToLinkCard",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public cancelCardLinking(Z)V
    .locals 2

    if-eqz p1, :cond_0

    const-string p1, "Settings Instant Deposit: Change Debit Card Cancel"

    goto :goto_0

    :cond_0
    const-string p1, "Settings Instant Deposit: Add Debit Card Cancel"

    .line 156
    :goto_0
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    invoke-direct {v1, p1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public cardLinkFailure(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 4

    const-string/jumbo v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const-string p1, "Settings Instant Deposit: Change Debit Card Result"

    goto :goto_0

    :cond_0
    const-string p1, "Settings Instant Deposit: Add Debit Card Result"

    .line 130
    :goto_0
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/instantdeposit/InstantDepositAnalytics$LinkCardFailedErrorEvent;

    invoke-direct {v1, p2, p3}, Lcom/squareup/instantdeposit/InstantDepositAnalytics$LinkCardFailedErrorEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 132
    new-instance v1, Lcom/squareup/log/deposits/CardLinkingResultEvent;

    const/4 v2, 0x0

    .line 135
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ": "

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 132
    invoke-direct {v1, p1, v2, p2}, Lcom/squareup/log/deposits/CardLinkingResultEvent;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 131
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public cardLinkSuccess(Z)V
    .locals 7

    if-eqz p1, :cond_0

    const-string p1, "Settings Instant Deposit: Change Debit Card Result"

    goto :goto_0

    :cond_0
    const-string p1, "Settings Instant Deposit: Add Debit Card Result"

    :goto_0
    move-object v1, p1

    .line 115
    iget-object p1, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_LINK_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 116
    iget-object p1, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 117
    new-instance v6, Lcom/squareup/log/deposits/CardLinkingResultEvent;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/log/deposits/CardLinkingResultEvent;-><init>(Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v6, Lcom/squareup/eventstream/v2/AppEvent;

    .line 116
    invoke-interface {p1, v6}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public headerDisplayedShowingAvailableBalance()V
    .locals 3

    .line 197
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_HEADER_DISPLAYED_SHOWING_AVAILABLE_BALANCE:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 198
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 199
    new-instance v1, Lcom/squareup/log/deposits/DepositsImpressionsEvent;

    const-string v2, "Transactions: Showing Available Balance"

    invoke-direct {v1, v2}, Lcom/squareup/log/deposits/DepositsImpressionsEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 198
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public headerDisplayedShowingError()V
    .locals 3

    .line 192
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_HEADER_DISPLAYED_SHOWING_ERROR:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 193
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/deposits/DepositsImpressionsEvent;

    const-string v2, "Transactions: Showing Error"

    invoke-direct {v1, v2}, Lcom/squareup/log/deposits/DepositsImpressionsEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public instantDepositFailed(Lcom/squareup/analytics/RegisterErrorName;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const-string v0, "registerErrorName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/instantdeposit/InstantDepositAnalytics$DepositingErrorEvent;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/instantdeposit/InstantDepositAnalytics$DepositingErrorEvent;-><init>(Lcom/squareup/analytics/RegisterErrorName;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public instantDepositSucceeded(Lcom/squareup/analytics/RegisterViewName;)V
    .locals 1

    const-string v0, "registerViewName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    return-void
.end method

.method public instantDepositToggled(Z)V
    .locals 3

    .line 160
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/instantdeposit/InstantDepositAnalytics$InstantDepositToggleAction;

    if-eqz p1, :cond_0

    const-string v2, "ON"

    goto :goto_0

    :cond_0
    const-string v2, "OFF"

    :goto_0
    invoke-direct {v1, v2}, Lcom/squareup/instantdeposit/InstantDepositAnalytics$InstantDepositToggleAction;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 161
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    if-eqz p1, :cond_1

    const-string p1, "Settings Instant Deposit: Allow Instant Deposit"

    goto :goto_1

    :cond_1
    const-string p1, "Settings Instant Deposit: Disallow Instant Deposit"

    :goto_1
    invoke-direct {v1, p1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logBalanceAppletAddDebitCardAttempt(Z)V
    .locals 3

    .line 325
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 326
    new-instance v1, Lcom/squareup/log/deposits/CardLinkingAttemptEvent;

    const-string v2, "Deposits: Add Debit Card Attempt"

    invoke-direct {v1, v2, p1}, Lcom/squareup/log/deposits/CardLinkingAttemptEvent;-><init>(Ljava/lang/String;Z)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 325
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logBalanceAppletAddDebitCardFailure(Ljava/lang/String;)V
    .locals 4

    const-string v0, "failureMessage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 343
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 344
    new-instance v1, Lcom/squareup/log/deposits/CardLinkingResultEvent;

    const-string v2, "Deposits: Add Debit Card Attempt"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/log/deposits/CardLinkingResultEvent;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 343
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logBalanceAppletAddDebitCardSuccess()V
    .locals 8

    .line 334
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 335
    new-instance v7, Lcom/squareup/log/deposits/CardLinkingResultEvent;

    const-string v2, "Deposits: Add Debit Card Result"

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/log/deposits/CardLinkingResultEvent;-><init>(Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v7, Lcom/squareup/eventstream/v2/AppEvent;

    .line 334
    invoke-interface {v0, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logBalanceAppletDisplayedAvailableBalance()V
    .locals 3

    .line 240
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 241
    new-instance v1, Lcom/squareup/log/deposits/DepositsImpressionsEvent;

    const-string v2, "Deposits: Showing Available Balance"

    invoke-direct {v1, v2}, Lcom/squareup/log/deposits/DepositsImpressionsEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 240
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logBalanceAppletInstantTransferClick()V
    .locals 3

    .line 256
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 257
    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Deposits: Deposit Available Funds Click"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 256
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logBalanceAppletInstantTransferSuccess()V
    .locals 3

    .line 272
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 273
    new-instance v1, Lcom/squareup/log/deposits/DepositsImpressionsEvent;

    const-string v2, "Deposits: Deposit Successful"

    invoke-direct {v1, v2}, Lcom/squareup/log/deposits/DepositsImpressionsEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 272
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logBalanceAppletLinkDebitCardCancel()V
    .locals 3

    .line 317
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 318
    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Deposits: Link Debit Card Cancel"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 317
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logBalanceAppletSetUpInstantTransfer()V
    .locals 3

    .line 304
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 305
    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Deposits: Set Up Instant Deposit"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 304
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 309
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 310
    new-instance v1, Lcom/squareup/log/deposits/DepositsImpressionsEvent;

    const-string v2, "Deposits: Link Debit Card"

    invoke-direct {v1, v2}, Lcom/squareup/log/deposits/DepositsImpressionsEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 309
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logBalanceAppletTransferEstimatedFeesClick()V
    .locals 3

    .line 288
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 289
    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Deposits: Deposit Reports Estimated Fees Info"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 288
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logBalanceAppletTransferEstimatedFeesLearnMoreClick()V
    .locals 3

    .line 296
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 297
    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Deposits: Deposit Reports Learn More From Estimated Fees Info"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 296
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logBalanceAppletTransferReportsDisplayedAvailableBalance()V
    .locals 3

    .line 248
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 249
    new-instance v1, Lcom/squareup/log/deposits/DepositsImpressionsEvent;

    const-string v2, "Deposits: Deposit Reports Showing Available Balance"

    invoke-direct {v1, v2}, Lcom/squareup/log/deposits/DepositsImpressionsEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 248
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logBalanceAppletTransferReportsInstantTransferClick()V
    .locals 3

    .line 264
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 265
    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Deposits: Deposit Reports Deposit Available Funds Click"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 264
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logBalanceAppletTransferReportsInstantTransferSuccess()V
    .locals 3

    .line 280
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 281
    new-instance v1, Lcom/squareup/log/deposits/DepositsImpressionsEvent;

    const-string v2, "Deposits: Deposit Reports Deposit Successful"

    invoke-direct {v1, v2}, Lcom/squareup/log/deposits/DepositsImpressionsEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 280
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logTransferReportsActiveSalesClick()V
    .locals 3

    .line 228
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Deposits: Deposit Reports Active Sales"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logTransferReportsPendingDepositClick()V
    .locals 3

    .line 232
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Deposits: Deposit Reports Pending Deposit"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logTransferReportsSentDepositClick()V
    .locals 3

    .line 236
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Deposits: Deposits Reports Sent Deposit"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logTransferReportsSummaryView()V
    .locals 3

    .line 224
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/deposits/DepositsImpressionsEvent;

    const-string v2, "Deposits: Deposit Reports Summary"

    invoke-direct {v1, v2}, Lcom/squareup/log/deposits/DepositsImpressionsEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public resendEmailFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_RESEND_EMAIL_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logError(Lcom/squareup/analytics/RegisterErrorName;)V

    .line 183
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 184
    new-instance v7, Lcom/squareup/log/deposits/DebitVerificationEmailEvent;

    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ": "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, v7

    .line 184
    invoke-direct/range {v1 .. v6}, Lcom/squareup/log/deposits/DebitVerificationEmailEvent;-><init>(Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v7, Lcom/squareup/eventstream/v2/AppEvent;

    .line 183
    invoke-interface {v0, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public resendEmailSuccess()V
    .locals 8

    .line 170
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_RESEND_EMAIL_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 172
    new-instance v7, Lcom/squareup/log/deposits/DebitVerificationEmailEvent;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/log/deposits/DebitVerificationEmailEvent;-><init>(Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v7, Lcom/squareup/eventstream/v2/AppEvent;

    .line 171
    invoke-interface {v0, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public tapDepositAvailableFunds(Lcom/squareup/analytics/RegisterTapName;Lcom/squareup/protos/common/Money;Z)V
    .locals 2

    const-string v0, "registerTapName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p3, :cond_0

    const-string p3, "Deposits Report: Deposit Available Funds Click"

    goto :goto_0

    :cond_0
    const-string p3, "Transactions: Deposit Available Funds Click"

    .line 89
    :goto_0
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/instantdeposit/InstantDepositAnalytics$DepositAvailableFundsTapEvent;

    invoke-direct {v1, p1, p2}, Lcom/squareup/instantdeposit/InstantDepositAnalytics$DepositAvailableFundsTapEvent;-><init>(Lcom/squareup/analytics/RegisterTapName;Lcom/squareup/protos/common/Money;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p2, Lcom/squareup/analytics/event/ClickEvent;

    invoke-direct {p2, p3}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public tapLearnMore(Lcom/squareup/analytics/RegisterErrorName;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const-string v0, "registerErrorName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/instantdeposit/InstantDepositAnalytics$LearnMoreErrorEvent;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/instantdeposit/InstantDepositAnalytics$LearnMoreErrorEvent;-><init>(Lcom/squareup/analytics/RegisterErrorName;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public tapLinkDifferentDebitCard()V
    .locals 3

    .line 165
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_LINK_DIFFERENT_DEBIT_CARD:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Settings Instant Deposit: Change Debit Card"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public transferReportsDisplayedAvailableBalance()V
    .locals 3

    .line 215
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_DEPOSITS_REPORT_DISPLAYED_AVAILABLE_BALANCE:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 216
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 217
    new-instance v1, Lcom/squareup/log/deposits/DepositsImpressionsEvent;

    const-string v2, "Deposits Report: Showing Available Balance"

    invoke-direct {v1, v2}, Lcom/squareup/log/deposits/DepositsImpressionsEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 216
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public transferReportsDisplayedError()V
    .locals 3

    .line 206
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_DEPOSITS_REPORT_DISPLAYED_ERROR:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 207
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 208
    new-instance v1, Lcom/squareup/log/deposits/DepositsImpressionsEvent;

    const-string v2, "Deposits Report: Showing Error"

    invoke-direct {v1, v2}, Lcom/squareup/log/deposits/DepositsImpressionsEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 207
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public tryToLinkCard(ZZ)V
    .locals 2

    if-eqz p1, :cond_0

    const-string p1, "Settings Instant Deposit: Change Debit Card Attempt"

    goto :goto_0

    :cond_0
    const-string p1, "Settings Instant Deposit: Add Debit Card Attempt"

    .line 145
    :goto_0
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTANT_DEPOSIT_TRY_TO_LINK_CARD:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 147
    new-instance v1, Lcom/squareup/log/deposits/CardLinkingAttemptEvent;

    invoke-direct {v1, p1, p2}, Lcom/squareup/log/deposits/CardLinkingAttemptEvent;-><init>(Ljava/lang/String;Z)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 146
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method
