.class public final Lcom/squareup/instantdeposit/InstantDepositAnalytics$LearnMoreErrorEvent;
.super Lcom/squareup/analytics/event/v1/ErrorEvent;
.source "InstantDepositAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/instantdeposit/InstantDepositAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LearnMoreErrorEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\t\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/InstantDepositAnalytics$LearnMoreErrorEvent;",
        "Lcom/squareup/analytics/event/v1/ErrorEvent;",
        "registerErrorName",
        "Lcom/squareup/analytics/RegisterErrorName;",
        "title",
        "",
        "message",
        "(Lcom/squareup/analytics/RegisterErrorName;Ljava/lang/String;Ljava/lang/String;)V",
        "getMessage",
        "()Ljava/lang/String;",
        "getTitle",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final message:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterErrorName;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "registerErrorName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ErrorEvent;-><init>(Lcom/squareup/analytics/RegisterErrorName;)V

    iput-object p2, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$LearnMoreErrorEvent;->title:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$LearnMoreErrorEvent;->message:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getMessage()Ljava/lang/String;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$LearnMoreErrorEvent;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$LearnMoreErrorEvent;->title:Ljava/lang/String;

    return-object v0
.end method
