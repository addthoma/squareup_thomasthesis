.class public final Lcom/squareup/instantdeposit/InstantDepositAnalytics$DepositAvailableFundsTapEvent;
.super Lcom/squareup/analytics/event/v1/TapEvent;
.source "InstantDepositAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/instantdeposit/InstantDepositAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DepositAvailableFundsTapEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/InstantDepositAnalytics$DepositAvailableFundsTapEvent;",
        "Lcom/squareup/analytics/event/v1/TapEvent;",
        "registerTapName",
        "Lcom/squareup/analytics/RegisterTapName;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/analytics/RegisterTapName;Lcom/squareup/protos/common/Money;)V",
        "getAmount",
        "()Lcom/squareup/protos/common/Money;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterTapName;Lcom/squareup/protos/common/Money;)V
    .locals 1

    const-string v0, "registerTapName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    check-cast p1, Lcom/squareup/analytics/EventNamedTap;

    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/TapEvent;-><init>(Lcom/squareup/analytics/EventNamedTap;)V

    iput-object p2, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$DepositAvailableFundsTapEvent;->amount:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public final getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$DepositAvailableFundsTapEvent;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method
