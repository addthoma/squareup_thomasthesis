.class public final Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;
.super Ljava/lang/Object;
.source "LoyaltyReportDateTimeFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltyReportDateTimeFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltyReportDateTimeFormatter.kt\ncom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter\n*L\n1#1,20:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fR#\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u00068BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\n\u0010\u000b\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "dateTimeFormatter",
        "Lorg/threeten/bp/format/DateTimeFormatter;",
        "kotlin.jvm.PlatformType",
        "getDateTimeFormatter",
        "()Lorg/threeten/bp/format/DateTimeFormatter;",
        "dateTimeFormatter$delegate",
        "Lkotlin/Lazy;",
        "format",
        "",
        "localDateTime",
        "Lorg/threeten/bp/LocalDateTime;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dateTimeFormatter$delegate:Lkotlin/Lazy;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter$dateTimeFormatter$2;

    invoke-direct {v0, p1}, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter$dateTimeFormatter$2;-><init>(Lcom/squareup/util/Res;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {v0}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;->dateTimeFormatter$delegate:Lkotlin/Lazy;

    return-void
.end method

.method private final getDateTimeFormatter()Lorg/threeten/bp/format/DateTimeFormatter;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;->dateTimeFormatter$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/format/DateTimeFormatter;

    return-object v0
.end method


# virtual methods
.method public final format(Lorg/threeten/bp/LocalDateTime;)Ljava/lang/String;
    .locals 1

    const-string v0, "localDateTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;->getDateTimeFormatter()Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    check-cast p1, Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-virtual {v0, p1}, Lorg/threeten/bp/format/DateTimeFormatter;->format(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "dateTimeFormatter.format(localDateTime)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
