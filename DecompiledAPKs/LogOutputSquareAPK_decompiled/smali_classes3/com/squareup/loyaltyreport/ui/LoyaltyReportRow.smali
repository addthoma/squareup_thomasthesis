.class public Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;
.super Ljava/lang/Object;
.source "LoyaltyReportModels.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$Creator;,
        Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$DateRangeTitle;,
        Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$SectionHeader;,
        Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$Summary;,
        Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$ViewInDashboard;,
        Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;,
        Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$IconInitialsTitleValue;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0008\u0017\u0018\u00002\u00020\u0001:\u0006\n\u000b\u000c\r\u000e\u000fB\u0005\u00a2\u0006\u0002\u0010\u0002J\t\u0010\u0003\u001a\u00020\u0004H\u00d6\u0001J\u0019\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0004H\u00d6\u0001\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;",
        "Landroid/os/Parcelable;",
        "()V",
        "describeContents",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "DateRangeTitle",
        "IconInitialsTitleValue",
        "SectionHeader",
        "Summary",
        "TitleValue",
        "ViewInDashboard",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$Creator;

    invoke-direct {v0}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$Creator;-><init>()V

    sput-object v0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
