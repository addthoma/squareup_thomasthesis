.class public final Lcom/squareup/loyaltyreport/RealLoyaltyReportViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealLoyaltyReportViewFactory.kt"

# interfaces
.implements Lcom/squareup/loyaltyreport/LoyaltyReportViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/RealLoyaltyReportViewFactory;",
        "Lcom/squareup/loyaltyreport/LoyaltyReportViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "loyaltyReportRunnerFactory",
        "Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$Factory;",
        "(Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$Factory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loyaltyReportRunnerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 14
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 15
    const-class v2, Lcom/squareup/loyaltyreport/LoyaltyReportScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    .line 16
    sget v3, Lcom/squareup/loyaltyreport/impl/R$layout;->loyalty_report:I

    .line 17
    new-instance v4, Lcom/squareup/loyaltyreport/RealLoyaltyReportViewFactory$1;

    invoke-direct {v4, p1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportViewFactory$1;-><init>(Lcom/squareup/loyaltyreport/ui/LoyaltyReportLayoutRunner$Factory;)V

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 14
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 19
    sget-object p1, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->Companion:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$Companion;

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 20
    sget-object p1, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner;->Companion:Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner$Companion;

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x2

    aput-object p1, v0, v1

    .line 13
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
