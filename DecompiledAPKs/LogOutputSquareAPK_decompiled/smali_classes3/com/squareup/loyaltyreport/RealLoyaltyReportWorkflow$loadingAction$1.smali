.class final Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1;
.super Ljava/lang/Object;
.source "RealLoyaltyReportWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->loadingAction(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
        "",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $dateRange:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

.field final synthetic this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1;->this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    iput-object p2, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1;->$dateRange:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1;->this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    invoke-static {v0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->access$getLoyaltyReportDateRangeService$p(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;)Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1;->$dateRange:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    invoke-virtual {v0, v1}, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;->dateRangeNow(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/protos/common/time/DateTimeInterval;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1;->this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    invoke-static {v1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->access$getLoyaltyReportTransformer$p(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;)Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;

    move-result-object v1

    .line 107
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;

    .line 106
    invoke-virtual {v1, v0, p1}, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;->transform(Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;)Lcom/squareup/loyaltyreport/ui/LoyaltyReport;

    move-result-object p1

    .line 109
    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1;->this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    iget-object v1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1;->$dateRange:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    invoke-static {v0, v1, p1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->access$renderReportAction(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;Lcom/squareup/loyaltyreport/ui/LoyaltyReport;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 111
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    .line 112
    iget-object p1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1;->this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    new-instance v0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1$1;-><init>(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
