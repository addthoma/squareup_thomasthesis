.class public abstract Lcom/squareup/datafetch/Rx1AbstractLoader;
.super Ljava/lang/Object;
.source "Rx1AbstractLoader.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;,
        Lcom/squareup/datafetch/Rx1AbstractLoader$Response;,
        Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;,
        Lcom/squareup/datafetch/Rx1AbstractLoader$Failure;,
        Lcom/squareup/datafetch/Rx1AbstractLoader$Results;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TInput:",
        "Ljava/lang/Object;",
        "TItem:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lmortar/Scoped;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final MIN_REFRESH_SECONDS:I = 0x4


# instance fields
.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private defaultPageSize:Ljava/lang/Integer;

.field private final failure:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Failure<",
            "TTInput;>;>;>;"
        }
    .end annotation
.end field

.field protected final mainScheduler:Lrx/Scheduler;

.field private final onRecover:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onRefresh:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final pageSizes:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final pagingKeys:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final progress:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Progress<",
            "TTInput;>;>;>;"
        }
    .end annotation
.end field

.field private final results:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Results<",
            "TTInput;TTItem;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;)V
    .locals 1

    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->onRefresh:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 82
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->onRecover:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 91
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->pagingKeys:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 92
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->pageSizes:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 94
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->results:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 98
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->failure:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 102
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->progress:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 263
    iput-object p1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 264
    iput-object p2, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->mainScheduler:Lrx/Scheduler;

    return-void
.end method

.method private doRequest(Lmortar/MortarScope;Ljava/lang/Object;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmortar/MortarScope;",
            "TTInput;)",
            "Lrx/Observable<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Response<",
            "TTInput;TTItem;>;>;"
        }
    .end annotation

    .line 346
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 349
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->pagingKeys:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->create()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 350
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->pageSizes:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 352
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->pagingKeys:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 353
    invoke-static {v0}, Lrx/Observable;->switchOnNext(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->pageSizes:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 354
    invoke-static {v1}, Lrx/Observable;->switchOnNext(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/datafetch/-$$Lambda$l7W7Ka72iTZQI6wmFC0dkUBGIWA;->INSTANCE:Lcom/squareup/datafetch/-$$Lambda$l7W7Ka72iTZQI6wmFC0dkUBGIWA;

    .line 352
    invoke-static {v0, v1, v2}, Lrx/Observable;->zip(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x1

    .line 356
    invoke-virtual {v0, v1}, Lrx/Observable;->replay(I)Lrx/observables/ConnectableObservable;

    move-result-object v0

    new-instance v2, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$oesE-gogtY70p_ywVfFd2xIMbY4;

    invoke-direct {v2, p1}, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$oesE-gogtY70p_ywVfFd2xIMbY4;-><init>(Lmortar/MortarScope;)V

    .line 357
    invoke-virtual {v0, v1, v2}, Lrx/observables/ConnectableObservable;->autoConnect(ILrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->onRecover:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 358
    invoke-static {v0}, Lcom/squareup/util/rx/RxTransformers;->resubscribeWhen(Lrx/Observable;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    .line 359
    invoke-static {}, Lrx/schedulers/Schedulers;->trampoline()Lrx/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->subscribeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$KIUy3ojfCJPQGr8YLAFzRhMmxmk;

    invoke-direct {v0, p0}, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$KIUy3ojfCJPQGr8YLAFzRhMmxmk;-><init>(Lcom/squareup/datafetch/Rx1AbstractLoader;)V

    .line 360
    invoke-virtual {p1, v0}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$mUiEIRnfNLWfwKey5sU0auNfO7U;

    invoke-direct {v0, p0, p2}, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$mUiEIRnfNLWfwKey5sU0auNfO7U;-><init>(Lcom/squareup/datafetch/Rx1AbstractLoader;Ljava/lang/Object;)V

    .line 365
    invoke-virtual {p1, v0}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method private hasFailure()Z
    .locals 1

    .line 334
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->failure:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Optional;

    invoke-virtual {v0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    return v0
.end method

.method public static synthetic lambda$_Ltg36VovvhYhFXQemKz0IF7zL4(Lcom/squareup/datafetch/Rx1AbstractLoader;Lcom/squareup/datafetch/Rx1AbstractLoader$Response;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/datafetch/Rx1AbstractLoader;->onResponse(Lcom/squareup/datafetch/Rx1AbstractLoader$Response;)V

    return-void
.end method

.method static synthetic lambda$doRequest$3(Lmortar/MortarScope;Lrx/Subscription;)V
    .locals 0

    .line 357
    invoke-static {p0, p1}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method static synthetic lambda$null$6(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Ljava/lang/Throwable;)Lcom/squareup/datafetch/Rx1AbstractLoader$Response;
    .locals 2

    .line 368
    instance-of v0, p2, Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    .line 369
    new-instance v0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;

    new-instance v1, Lcom/squareup/datafetch/LoaderError$ThrowableError;

    invoke-direct {v1, p2}, Lcom/squareup/datafetch/LoaderError$ThrowableError;-><init>(Ljava/lang/Throwable;)V

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lcom/squareup/datafetch/LoaderError;)V

    return-object v0

    .line 371
    :cond_0
    new-instance p0, Lrx/exceptions/OnErrorNotImplementedException;

    invoke-direct {p0, p2}, Lrx/exceptions/OnErrorNotImplementedException;-><init>(Ljava/lang/Throwable;)V

    throw p0
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/connectivity/InternetState;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 283
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private onResponse(Lcom/squareup/datafetch/Rx1AbstractLoader$Response;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Response<",
            "TTInput;TTItem;>;)V"
        }
    .end annotation

    .line 377
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 379
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->progress:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Optional;

    invoke-virtual {v0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->progress:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 383
    :cond_0
    iget-object v0, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->error:Lcom/squareup/datafetch/LoaderError;

    if-eqz v0, :cond_1

    .line 384
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->failure:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/datafetch/Rx1AbstractLoader$Failure;

    iget-object v2, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->input:Ljava/lang/Object;

    iget-object v3, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->pagingParams:Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;

    iget-object p1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->error:Lcom/squareup/datafetch/LoaderError;

    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/datafetch/Rx1AbstractLoader$Failure;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lcom/squareup/datafetch/LoaderError;)V

    .line 385
    invoke-static {v1}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    .line 384
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_2

    .line 387
    :cond_1
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->failure:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 390
    iget-object v0, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->pagingParams:Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;

    iget-object v0, v0, Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->results:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 391
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;

    iget-object v1, v1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 394
    :goto_0
    iget-object v1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->fetchedItems:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->fetchedItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 395
    iget-object v1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->fetchedItems:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 398
    :cond_3
    iget-object v1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->nextPagingKey:Ljava/lang/String;

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    .line 399
    :goto_1
    iget-object v2, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->results:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v3, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;

    iget-object v4, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->input:Ljava/lang/Object;

    invoke-direct {v3, v4, v0, v1}, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;-><init>(Ljava/lang/Object;Ljava/util/List;Z)V

    invoke-virtual {v2, v3}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    if-eqz v1, :cond_5

    .line 402
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->pagingKeys:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/subjects/BehaviorSubject;

    iget-object p1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->nextPagingKey:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_2

    .line 404
    :cond_5
    iget-object p1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->pagingKeys:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lrx/subjects/BehaviorSubject;

    invoke-virtual {p1}, Lrx/subjects/BehaviorSubject;->onCompleted()V

    :goto_2
    return-void
.end method

.method private retryIfLastError()V
    .locals 2

    .line 338
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 340
    invoke-direct {p0}, Lcom/squareup/datafetch/Rx1AbstractLoader;->hasFailure()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->onRecover:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public failure()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Failure<",
            "TTInput;>;>;>;"
        }
    .end annotation

    .line 311
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->failure:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method protected abstract fetch(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lrx/functions/Action0;)Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;",
            "Lrx/functions/Action0;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Response<",
            "TTInput;TTItem;>;>;"
        }
    .end annotation
.end method

.method protected abstract input()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "TTInput;>;"
        }
    .end annotation
.end method

.method public synthetic lambda$doRequest$4$Rx1AbstractLoader()V
    .locals 2

    .line 362
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->pagingKeys:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/subjects/BehaviorSubject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 363
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->defaultPageSize:Ljava/lang/Integer;

    invoke-virtual {p0, v0}, Lcom/squareup/datafetch/Rx1AbstractLoader;->loadMore(Ljava/lang/Integer;)V

    return-void
.end method

.method public synthetic lambda$doRequest$7$Rx1AbstractLoader(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;)Lrx/Observable;
    .locals 2

    .line 365
    new-instance v0, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$puHsCEPh6FYGbh_ZI4aUTB6ZTgg;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$puHsCEPh6FYGbh_ZI4aUTB6ZTgg;-><init>(Lcom/squareup/datafetch/Rx1AbstractLoader;Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/datafetch/Rx1AbstractLoader;->fetch(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$SgxO-FH2F3C--NZA6Jd9cxKi37I;

    invoke-direct {v1, p1, p2}, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$SgxO-FH2F3C--NZA6Jd9cxKi37I;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;)V

    .line 367
    invoke-virtual {v0, v1}, Lrx/Observable;->onErrorReturn(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$5$Rx1AbstractLoader(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;)V
    .locals 2

    .line 366
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->progress:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;

    invoke-direct {v1, p1, p2}, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;)V

    invoke-static {v1}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$Rx1AbstractLoader(Lcom/squareup/connectivity/InternetState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 284
    invoke-direct {p0}, Lcom/squareup/datafetch/Rx1AbstractLoader;->retryIfLastError()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$Rx1AbstractLoader(Lmortar/MortarScope;Ljava/lang/Object;)Lrx/Observable;
    .locals 0

    .line 289
    invoke-direct {p0, p1, p2}, Lcom/squareup/datafetch/Rx1AbstractLoader;->doRequest(Lmortar/MortarScope;Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public loadMore(Ljava/lang/Integer;)V
    .locals 1

    .line 324
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 325
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->pageSizes:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jakewharton/rxrelay/BehaviorRelay;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->defaultPageSize:Ljava/lang/Integer;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 6

    .line 280
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 281
    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 282
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$cl1OUpTcizTED5trPkgzgnaG5FQ;->INSTANCE:Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$cl1OUpTcizTED5trPkgzgnaG5FQ;

    .line 283
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$DpGvHZCu4taZM1olM6nj5lGv5tA;

    invoke-direct {v1, p0}, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$DpGvHZCu4taZM1olM6nj5lGv5tA;-><init>(Lcom/squareup/datafetch/Rx1AbstractLoader;)V

    .line 284
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 280
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 287
    invoke-virtual {p0}, Lcom/squareup/datafetch/Rx1AbstractLoader;->input()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->onRefresh:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->mainScheduler:Lrx/Scheduler;

    const-wide/16 v4, 0x4

    .line 288
    invoke-static {v1, v4, v5, v2, v3}, Lcom/squareup/util/rx/RxTransformers;->refreshWhen(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$y10WdghyNEQQ92KteDOGPH2LFco;

    invoke-direct {v1, p0, p1}, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$y10WdghyNEQQ92KteDOGPH2LFco;-><init>(Lcom/squareup/datafetch/Rx1AbstractLoader;Lmortar/MortarScope;)V

    .line 289
    invoke-virtual {v0, v1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$_Ltg36VovvhYhFXQemKz0IF7zL4;

    invoke-direct {v1, p0}, Lcom/squareup/datafetch/-$$Lambda$Rx1AbstractLoader$_Ltg36VovvhYhFXQemKz0IF7zL4;-><init>(Lcom/squareup/datafetch/Rx1AbstractLoader;)V

    .line 290
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 286
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public pagingParams()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 300
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->pagingKeys:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 301
    invoke-static {v0}, Lrx/Observable;->switchOnNext(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->pageSizes:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 302
    invoke-static {v1}, Lrx/Observable;->switchOnNext(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/datafetch/-$$Lambda$l7W7Ka72iTZQI6wmFC0dkUBGIWA;->INSTANCE:Lcom/squareup/datafetch/-$$Lambda$l7W7Ka72iTZQI6wmFC0dkUBGIWA;

    .line 300
    invoke-static {v0, v1, v2}, Lrx/Observable;->zip(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public progress()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Progress<",
            "TTInput;>;>;>;"
        }
    .end annotation

    .line 315
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->progress:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public refresh()V
    .locals 2

    .line 329
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 330
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->onRefresh:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public results()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Results<",
            "TTInput;TTItem;>;>;"
        }
    .end annotation

    .line 307
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->results:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public setDefaultPageSize(Ljava/lang/Integer;)V
    .locals 1

    if-eqz p1, :cond_1

    if-eqz p1, :cond_0

    .line 319
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 320
    iput-object p1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader;->defaultPageSize:Ljava/lang/Integer;

    return-void
.end method
