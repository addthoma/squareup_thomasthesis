.class final Lcom/squareup/datafetch/AbstractLoader$doRequest$3;
.super Ljava/lang/Object;
.source "AbstractLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/datafetch/AbstractLoader;->doRequest(Lmortar/MortarScope;Ljava/lang/Object;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u00032\u000e\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "TInput",
        "",
        "TItem",
        "it",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/datafetch/AbstractLoader;


# direct methods
.method constructor <init>(Lcom/squareup/datafetch/AbstractLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$3;->this$0:Lcom/squareup/datafetch/AbstractLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lio/reactivex/disposables/Disposable;)V
    .locals 1

    .line 179
    iget-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$3;->this$0:Lcom/squareup/datafetch/AbstractLoader;

    invoke-static {p1}, Lcom/squareup/datafetch/AbstractLoader;->access$getPagingKeys$p(Lcom/squareup/datafetch/AbstractLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/subjects/BehaviorSubject;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v0}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 180
    :cond_0
    iget-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$3;->this$0:Lcom/squareup/datafetch/AbstractLoader;

    invoke-static {p1}, Lcom/squareup/datafetch/AbstractLoader;->access$getDefaultPageSize$p(Lcom/squareup/datafetch/AbstractLoader;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/datafetch/AbstractLoader;->loadMore(Ljava/lang/Integer;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 69
    check-cast p1, Lio/reactivex/disposables/Disposable;

    invoke-virtual {p0, p1}, Lcom/squareup/datafetch/AbstractLoader$doRequest$3;->accept(Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
