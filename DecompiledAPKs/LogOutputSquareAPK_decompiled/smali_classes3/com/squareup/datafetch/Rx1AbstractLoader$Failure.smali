.class public Lcom/squareup/datafetch/Rx1AbstractLoader$Failure;
.super Ljava/lang/Object;
.source "Rx1AbstractLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/datafetch/Rx1AbstractLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Failure"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TInput:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final error:Lcom/squareup/datafetch/LoaderError;

.field public final input:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTInput;"
        }
    .end annotation
.end field

.field public final isFirstPage:Z


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lcom/squareup/datafetch/LoaderError;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;",
            "Lcom/squareup/datafetch/LoaderError;",
            ")V"
        }
    .end annotation

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    iput-object p1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Failure;->input:Ljava/lang/Object;

    .line 163
    iget-object p1, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Failure;->isFirstPage:Z

    .line 164
    iput-object p3, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Failure;->error:Lcom/squareup/datafetch/LoaderError;

    return-void
.end method
