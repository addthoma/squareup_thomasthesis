.class public final Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;
.super Ljava/lang/Object;
.source "RealConnectedScalesRepository.kt"

# interfaces
.implements Lcom/squareup/connectedscalesdata/ConnectedScalesRepository;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u00082\u0006\u0010\u0019\u001a\u00020\u000fH\u0002J\u0014\u0010\u001a\u001a\u00020\n*\u00020\u000f2\u0006\u0010\u001b\u001a\u00020\u0018H\u0002R \u0010\u0007\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\t0\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR \u0010\r\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000e0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000f0\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0012\u001a\u00020\u0013*\u00020\u00148BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;",
        "Lcom/squareup/connectedscalesdata/ConnectedScalesRepository;",
        "scaleTracker",
        "Lcom/squareup/scales/ScaleTracker;",
        "resources",
        "Landroid/content/res/Resources;",
        "(Lcom/squareup/scales/ScaleTracker;Landroid/content/res/Resources;)V",
        "connectedScales",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/connectedscalesdata/ConnectedScale;",
        "getConnectedScales",
        "()Lio/reactivex/Observable;",
        "latestConnectedScales",
        "",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "Lcom/squareup/scales/Scale;",
        "supportedHardwareScales",
        "readableName",
        "",
        "Lcom/squareup/scales/ScaleTracker$Manufacturer;",
        "getReadableName",
        "(Lcom/squareup/scales/ScaleTracker$Manufacturer;)Ljava/lang/String;",
        "scaleUnitEvents",
        "Lcom/squareup/scales/UnitOfMeasurement;",
        "hardwareScale",
        "toConnectedScale",
        "unitOfMeasurement",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connectedScales:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/connectedscalesdata/ConnectedScale;",
            ">;>;"
        }
    .end annotation
.end field

.field private final latestConnectedScales:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/util/Map<",
            "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
            "Lcom/squareup/scales/Scale;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resources:Landroid/content/res/Resources;

.field private final supportedHardwareScales:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/scales/ScaleTracker;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "scaleTracker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;->resources:Landroid/content/res/Resources;

    .line 30
    invoke-interface {p1}, Lcom/squareup/scales/ScaleTracker;->getConnectedScales()Lio/reactivex/Observable;

    move-result-object p1

    const/4 p2, 0x1

    .line 31
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lio/reactivex/observables/ConnectableObservable;->refCount()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "scaleTracker.connectedSc\u2026ay(1)\n        .refCount()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;->latestConnectedScales:Lio/reactivex/Observable;

    .line 39
    iget-object p1, p0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;->latestConnectedScales:Lio/reactivex/Observable;

    .line 40
    sget-object p2, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$supportedHardwareScales$1;->INSTANCE:Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$supportedHardwareScales$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "latestConnectedScales\n  \u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;->supportedHardwareScales:Lio/reactivex/Observable;

    .line 62
    iget-object p1, p0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;->supportedHardwareScales:Lio/reactivex/Observable;

    .line 63
    new-instance p2, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;

    invoke-direct {p2, p0}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;-><init>(Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 77
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    .line 78
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "supportedHardwareScales\n\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;->connectedScales:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$scaleUnitEvents(Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;Lcom/squareup/scales/ScaleTracker$HardwareScale;)Lio/reactivex/Observable;
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;->scaleUnitEvents(Lcom/squareup/scales/ScaleTracker$HardwareScale;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toConnectedScale(Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/UnitOfMeasurement;)Lcom/squareup/connectedscalesdata/ConnectedScale;
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;->toConnectedScale(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/UnitOfMeasurement;)Lcom/squareup/connectedscalesdata/ConnectedScale;

    move-result-object p0

    return-object p0
.end method

.method private final getReadableName(Lcom/squareup/scales/ScaleTracker$Manufacturer;)Ljava/lang/String;
    .locals 1

    .line 82
    sget-object v0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/scales/ScaleTracker$Manufacturer;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 85
    sget p1, Lcom/squareup/connectedscalesdata/R$string;->scale_manufacturer_cas:I

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 84
    :cond_1
    sget p1, Lcom/squareup/connectedscalesdata/R$string;->scale_manufacturer_starmicronics:I

    goto :goto_0

    .line 83
    :cond_2
    sget p1, Lcom/squareup/connectedscalesdata/R$string;->scale_manufacturer_brecknell:I

    .line 88
    :goto_0
    iget-object v0, p0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;->resources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(resId)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final scaleUnitEvents(Lcom/squareup/scales/ScaleTracker$HardwareScale;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/scales/UnitOfMeasurement;",
            ">;"
        }
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;->latestConnectedScales:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$scaleUnitEvents$1;

    invoke-direct {v1, p1}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$scaleUnitEvents$1;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "latestConnectedScales.sw\u2026 Observable.empty()\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final toConnectedScale(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/UnitOfMeasurement;)Lcom/squareup/connectedscalesdata/ConnectedScale;
    .locals 2

    .line 92
    new-instance v0, Lcom/squareup/connectedscalesdata/ConnectedScale;

    .line 93
    invoke-virtual {p1}, Lcom/squareup/scales/ScaleTracker$HardwareScale;->getManufacturer()Lcom/squareup/scales/ScaleTracker$Manufacturer;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;->getReadableName(Lcom/squareup/scales/ScaleTracker$Manufacturer;)Ljava/lang/String;

    move-result-object v1

    .line 94
    invoke-virtual {p1}, Lcom/squareup/scales/ScaleTracker$HardwareScale;->getConnectionType()Lcom/squareup/scales/ScaleTracker$ConnectionType;

    move-result-object p1

    .line 92
    invoke-direct {v0, v1, p1, p2}, Lcom/squareup/connectedscalesdata/ConnectedScale;-><init>(Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$ConnectionType;Lcom/squareup/scales/UnitOfMeasurement;)V

    return-object v0
.end method


# virtual methods
.method public getConnectedScales()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/connectedscalesdata/ConnectedScale;",
            ">;>;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;->connectedScales:Lio/reactivex/Observable;

    return-object v0
.end method
