.class final Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$supportedHardwareScales$1;
.super Ljava/lang/Object;
.source "RealConnectedScalesRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;-><init>(Lcom/squareup/scales/ScaleTracker;Landroid/content/res/Resources;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealConnectedScalesRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealConnectedScalesRepository.kt\ncom/squareup/connectedscalesdata/RealConnectedScalesRepository$supportedHardwareScales$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,103:1\n704#2:104\n777#2,2:105\n*E\n*S KotlinDebug\n*F\n+ 1 RealConnectedScalesRepository.kt\ncom/squareup/connectedscalesdata/RealConnectedScalesRepository$supportedHardwareScales$1\n*L\n42#1:104\n42#1,2:105\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "it",
        "",
        "Lcom/squareup/scales/Scale;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$supportedHardwareScales$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$supportedHardwareScales$1;

    invoke-direct {v0}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$supportedHardwareScales$1;-><init>()V

    sput-object v0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$supportedHardwareScales$1;->INSTANCE:Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$supportedHardwareScales$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$supportedHardwareScales$1;->apply(Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Ljava/util/Map;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
            "+",
            "Lcom/squareup/scales/Scale;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 105
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/scales/ScaleTracker$HardwareScale;

    .line 42
    invoke-static {v2}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepositoryKt;->access$isSupported$p(Lcom/squareup/scales/ScaleTracker$HardwareScale;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 106
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 43
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
