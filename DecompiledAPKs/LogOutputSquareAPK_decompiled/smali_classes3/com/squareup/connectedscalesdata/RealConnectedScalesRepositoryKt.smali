.class public final Lcom/squareup/connectedscalesdata/RealConnectedScalesRepositoryKt;
.super Ljava/lang/Object;
.source "RealConnectedScalesRepository.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0000\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "isSupported",
        "",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "(Lcom/squareup/scales/ScaleTracker$HardwareScale;)Z",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$isSupported$p(Lcom/squareup/scales/ScaleTracker$HardwareScale;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepositoryKt;->isSupported(Lcom/squareup/scales/ScaleTracker$HardwareScale;)Z

    move-result p0

    return p0
.end method

.method private static final isSupported(Lcom/squareup/scales/ScaleTracker$HardwareScale;)Z
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/scales/ScaleTracker$HardwareScale;->getConnectionType()Lcom/squareup/scales/ScaleTracker$ConnectionType;

    move-result-object p0

    sget-object v0, Lcom/squareup/scales/ScaleTracker$ConnectionType;->USB:Lcom/squareup/scales/ScaleTracker$ConnectionType;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
