.class public interface abstract Lcom/squareup/connectedscalesdata/log/ScaleAnalytics;
.super Ljava/lang/Object;
.source "ScaleAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/connectedscalesdata/log/ScaleAnalytics$WeighedItemizationType;,
        Lcom/squareup/connectedscalesdata/log/ScaleAnalytics$MeasurementMethod;,
        Lcom/squareup/connectedscalesdata/log/ScaleAnalytics$MeasurementType;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001:\u0003\u0015\u0016\u0017J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0008\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J@\u0010\n\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H&\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/connectedscalesdata/log/ScaleAnalytics;",
        "",
        "logScaleConnected",
        "",
        "connectedScale",
        "Lcom/squareup/connectedscalesdata/ConnectedScale;",
        "logScaleDisconnected",
        "logScaleError",
        "logScaleReadFail",
        "logScaleReadSuccess",
        "logWeighedItemization",
        "type",
        "Lcom/squareup/connectedscalesdata/log/ScaleAnalytics$WeighedItemizationType;",
        "weightRecorded",
        "Ljava/math/BigDecimal;",
        "unitSelected",
        "Lcom/squareup/scales/UnitOfMeasurement;",
        "measurementType",
        "Lcom/squareup/connectedscalesdata/log/ScaleAnalytics$MeasurementType;",
        "measurementMethod",
        "Lcom/squareup/connectedscalesdata/log/ScaleAnalytics$MeasurementMethod;",
        "MeasurementMethod",
        "MeasurementType",
        "WeighedItemizationType",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract logScaleConnected(Lcom/squareup/connectedscalesdata/ConnectedScale;)V
.end method

.method public abstract logScaleDisconnected(Lcom/squareup/connectedscalesdata/ConnectedScale;)V
.end method

.method public abstract logScaleError(Lcom/squareup/connectedscalesdata/ConnectedScale;)V
.end method

.method public abstract logScaleReadFail(Lcom/squareup/connectedscalesdata/ConnectedScale;)V
.end method

.method public abstract logScaleReadSuccess(Lcom/squareup/connectedscalesdata/ConnectedScale;)V
.end method

.method public abstract logWeighedItemization(Lcom/squareup/connectedscalesdata/ConnectedScale;Lcom/squareup/connectedscalesdata/log/ScaleAnalytics$WeighedItemizationType;Ljava/math/BigDecimal;Lcom/squareup/scales/UnitOfMeasurement;Lcom/squareup/connectedscalesdata/log/ScaleAnalytics$MeasurementType;Lcom/squareup/connectedscalesdata/log/ScaleAnalytics$MeasurementMethod;)V
.end method
