.class final Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$latestUnits$1;
.super Ljava/lang/Object;
.source "RealConnectedScalesRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;->apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealConnectedScalesRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealConnectedScalesRepository.kt\ncom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$latestUnits$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,103:1\n1360#2:104\n1429#2,3:105\n73#3:108\n*E\n*S KotlinDebug\n*F\n+ 1 RealConnectedScalesRepository.kt\ncom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$latestUnits$1\n*L\n67#1:104\n67#1,3:105\n67#1:108\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0002H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/scales/UnitOfMeasurement;",
        "scales",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;


# direct methods
.method constructor <init>(Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$latestUnits$1;->this$0:Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/util/List;)Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
            ">;)",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/scales/UnitOfMeasurement;",
            ">;>;"
        }
    .end annotation

    const-string v0, "scales"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    check-cast p1, Ljava/lang/Iterable;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 105
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 106
    check-cast v1, Lcom/squareup/scales/ScaleTracker$HardwareScale;

    .line 67
    iget-object v2, p0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$latestUnits$1;->this$0:Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;

    iget-object v2, v2, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;->this$0:Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;

    invoke-static {v2, v1}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;->access$scaleUnitEvents(Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;Lcom/squareup/scales/ScaleTracker$HardwareScale;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 107
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 108
    new-instance p1, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$latestUnits$1$$special$$inlined$combineLatest$1;

    invoke-direct {p1}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$latestUnits$1$$special$$inlined$combineLatest$1;-><init>()V

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-static {v0, p1}, Lio/reactivex/Observable;->combineLatest(Ljava/lang/Iterable;Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "Observable.combineLatest\u2026ements.map { it as T }) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$latestUnits$1;->apply(Ljava/util/List;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
