.class public final Lflow/path/PathContext;
.super Landroid/content/ContextWrapper;
.source "PathContext.java"


# static fields
.field private static final SERVICE_NAME:Ljava/lang/String; = "PATH_CONTEXT"


# instance fields
.field private final contexts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lflow/path/Path;",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private inflater:Landroid/view/LayoutInflater;

.field private final path:Lflow/path/Path;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lflow/path/Path;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lflow/path/Path;",
            "Ljava/util/Map<",
            "Lflow/path/Path;",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "leaf context must not be null"

    .line 40
    invoke-static {p1, v2, v1}, Lflow/path/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    invoke-virtual {p2}, Lflow/path/Path;->elements()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p3}, Ljava/util/Map;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 43
    invoke-virtual {p2}, Lflow/path/Path;->elements()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-interface {p3}, Ljava/util/Map;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v4, "Path and context map are not the same size, path has %d elements and there are %d contexts"

    .line 41
    invoke-static {v1, v4, v2}, Lflow/path/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 44
    invoke-virtual {p2}, Lflow/path/Path;->isRoot()Z

    move-result v1

    if-nez v1, :cond_2

    .line 45
    invoke-virtual {p2}, Lflow/path/Path;->elements()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p2}, Lflow/path/Path;->elements()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v3

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflow/path/Path;

    .line 46
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "For a non-root Path, baseContext must be Path leaf\'s context."

    invoke-static {v3, v0, p1}, Lflow/path/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 49
    :cond_2
    iput-object p2, p0, Lflow/path/PathContext;->path:Lflow/path/Path;

    .line 50
    iput-object p3, p0, Lflow/path/PathContext;->contexts:Ljava/util/Map;

    return-void
.end method

.method public static varargs create(Lflow/path/Path;Lflow/path/PathContextFactory;Lflow/path/PathContext;[Lflow/path/PathContext;)Lflow/path/PathContext;
    .locals 4

    .line 60
    sget-object v0, Lflow/path/Path;->ROOT:Lflow/path/Path;

    if-eq p0, v0, :cond_4

    .line 62
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object p2, p2, Lflow/path/PathContext;->contexts:Ljava/util/Map;

    invoke-direct {v0, p2}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 63
    array-length p2, p3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_0

    aget-object v2, p3, v1

    .line 64
    iget-object v2, v2, Lflow/path/PathContext;->contexts:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 67
    :cond_0
    invoke-virtual {p0}, Lflow/path/Path;->elements()Ljava/util/List;

    move-result-object p2

    .line 68
    new-instance p3, Ljava/util/LinkedHashMap;

    invoke-direct {p3}, Ljava/util/LinkedHashMap;-><init>()V

    const/4 v1, 0x0

    .line 73
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    .line 75
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 76
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflow/path/Path;

    .line 78
    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 79
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 80
    invoke-interface {p3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 82
    :cond_1
    invoke-interface {p1, v2, v1}, Lflow/path/PathContextFactory;->setUpContext(Lflow/path/Path;Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 83
    invoke-interface {p3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    :cond_2
    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 89
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/path/Path;

    .line 90
    invoke-interface {p1, v0, v1}, Lflow/path/PathContextFactory;->setUpContext(Lflow/path/Path;Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 91
    invoke-interface {p3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 94
    :cond_3
    new-instance p1, Lflow/path/PathContext;

    invoke-direct {p1, v1, p0, p3}, Lflow/path/PathContext;-><init>(Landroid/content/Context;Lflow/path/Path;Ljava/util/Map;)V

    return-object p1

    .line 60
    :cond_4
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Path is empty."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static get(Landroid/content/Context;)Lflow/path/PathContext;
    .locals 2

    const-string v0, "PATH_CONTEXT"

    .line 114
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lflow/path/PathContext;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Expected to find a PathContext but did not."

    invoke-static {p0, v1, v0}, Lflow/path/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lflow/path/PathContext;

    return-object p0
.end method

.method public static root(Landroid/content/Context;)Lflow/path/PathContext;
    .locals 3

    .line 54
    new-instance v0, Lflow/path/PathContext;

    sget-object v1, Lflow/path/Path;->ROOT:Lflow/path/Path;

    sget-object v2, Lflow/path/Path;->ROOT:Lflow/path/Path;

    .line 55
    invoke-static {v2, p0}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lflow/path/PathContext;-><init>(Landroid/content/Context;Lflow/path/Path;Ljava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method public varargs destroyNotIn(Lflow/path/PathContextFactory;Lflow/path/PathContext;[Lflow/path/PathContext;)V
    .locals 3

    .line 100
    new-instance v0, Ljava/util/LinkedHashSet;

    iget-object p2, p2, Lflow/path/PathContext;->path:Lflow/path/Path;

    invoke-virtual {p2}, Lflow/path/Path;->elements()Ljava/util/List;

    move-result-object p2

    invoke-direct {v0, p2}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 101
    array-length p2, p3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_0

    aget-object v2, p3, v1

    .line 102
    iget-object v2, v2, Lflow/path/PathContext;->path:Lflow/path/Path;

    invoke-virtual {v2}, Lflow/path/Path;->elements()Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 105
    :cond_0
    iget-object p2, p0, Lflow/path/PathContext;->path:Lflow/path/Path;

    invoke-virtual {p2}, Lflow/path/Path;->elements()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lflow/path/Path;

    .line 106
    invoke-interface {v0, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 107
    iget-object p2, p0, Lflow/path/PathContext;->contexts:Ljava/util/Map;

    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/content/Context;

    invoke-interface {p1, p2}, Lflow/path/PathContextFactory;->tearDownContext(Landroid/content/Context;)V

    :cond_2
    return-void
.end method

.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    const-string v0, "PATH_CONTEXT"

    .line 119
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    const-string v0, "layout_inflater"

    .line 122
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 123
    iget-object p1, p0, Lflow/path/PathContext;->inflater:Landroid/view/LayoutInflater;

    if-nez p1, :cond_1

    .line 124
    invoke-virtual {p0}, Lflow/path/PathContext;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lflow/path/PathContext;->inflater:Landroid/view/LayoutInflater;

    .line 126
    :cond_1
    iget-object p1, p0, Lflow/path/PathContext;->inflater:Landroid/view/LayoutInflater;

    return-object p1

    .line 128
    :cond_2
    invoke-super {p0, p1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
