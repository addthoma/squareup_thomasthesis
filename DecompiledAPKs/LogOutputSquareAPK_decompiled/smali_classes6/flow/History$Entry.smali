.class final Lflow/History$Entry;
.super Ljava/lang/Object;
.source "History.java"

# interfaces
.implements Lflow/ViewState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/History;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Entry"
.end annotation


# instance fields
.field final state:Ljava/lang/Object;

.field viewState:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    iput-object p1, p0, Lflow/History$Entry;->state:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    .line 202
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 203
    :cond_1
    check-cast p1, Lflow/History$Entry;

    .line 204
    iget-object v0, p0, Lflow/History$Entry;->state:Ljava/lang/Object;

    iget-object p1, p1, Lflow/History$Entry;->state:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method getBundle(Lflow/KeyParceler;)Landroid/os/Bundle;
    .locals 2

    .line 194
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 195
    iget-object v1, p0, Lflow/History$Entry;->state:Ljava/lang/Object;

    invoke-interface {p1, v1}, Lflow/KeyParceler;->wrap(Ljava/lang/Object;)Landroid/os/Parcelable;

    move-result-object p1

    const-string v1, "OBJECT"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 196
    iget-object p1, p0, Lflow/History$Entry;->viewState:Landroid/util/SparseArray;

    const-string v1, "VIEW_STATE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 208
    iget-object v0, p0, Lflow/History$Entry;->state:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public restore(Landroid/view/View;)V
    .locals 1

    .line 188
    iget-object v0, p0, Lflow/History$Entry;->viewState:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {p1, v0}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    :cond_0
    return-void
.end method

.method public save(Landroid/view/View;)V
    .locals 1

    .line 182
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 183
    invoke-virtual {p1, v0}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 184
    iput-object v0, p0, Lflow/History$Entry;->viewState:Landroid/util/SparseArray;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 212
    iget-object v0, p0, Lflow/History$Entry;->state:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
