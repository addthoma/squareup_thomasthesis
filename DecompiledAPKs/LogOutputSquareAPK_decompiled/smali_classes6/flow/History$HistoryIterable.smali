.class Lflow/History$HistoryIterable;
.super Ljava/lang/Object;
.source "History.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/History;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HistoryIterable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final fromBottom:Z

.field private final history:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lflow/History$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lflow/History$Entry;",
            ">;Z)V"
        }
    .end annotation

    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335
    iput-object p1, p0, Lflow/History$HistoryIterable;->history:Ljava/util/List;

    .line 336
    iput-boolean p2, p0, Lflow/History$HistoryIterable;->fromBottom:Z

    return-void
.end method

.method private static iterateFromBottom(Ljava/util/List;)Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "Lflow/History$Entry;",
            ">;)",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    .line 348
    new-instance v0, Lflow/History$ReadStateIterator;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-direct {v0, p0}, Lflow/History$ReadStateIterator;-><init>(Ljava/util/Iterator;)V

    return-object v0
.end method

.method private static iterateFromTop(Ljava/util/List;)Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "Lflow/History$Entry;",
            ">;)",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    .line 352
    new-instance v0, Lflow/History$ReadStateIterator;

    new-instance v1, Lflow/History$ReverseIterator;

    invoke-direct {v1, p0}, Lflow/History$ReverseIterator;-><init>(Ljava/util/List;)V

    invoke-direct {v0, v1}, Lflow/History$ReadStateIterator;-><init>(Ljava/util/Iterator;)V

    return-object v0
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    .line 340
    iget-boolean v0, p0, Lflow/History$HistoryIterable;->fromBottom:Z

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lflow/History$HistoryIterable;->history:Ljava/util/List;

    invoke-static {v0}, Lflow/History$HistoryIterable;->iterateFromBottom(Ljava/util/List;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0

    .line 343
    :cond_0
    iget-object v0, p0, Lflow/History$HistoryIterable;->history:Ljava/util/List;

    invoke-static {v0}, Lflow/History$HistoryIterable;->iterateFromTop(Ljava/util/List;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
