.class Lflow/Flow$3;
.super Lflow/Flow$PendingTraversal;
.source "Flow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lflow/Flow;->goBack()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lflow/Flow;


# direct methods
.method constructor <init>(Lflow/Flow;)V
    .locals 1

    .line 183
    iput-object p1, p0, Lflow/Flow$3;->this$0:Lflow/Flow;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflow/Flow$PendingTraversal;-><init>(Lflow/Flow;Lflow/Flow$1;)V

    return-void
.end method


# virtual methods
.method protected doExecute()V
    .locals 2

    .line 185
    iget-object v0, p0, Lflow/Flow$3;->this$0:Lflow/Flow;

    invoke-static {v0}, Lflow/Flow;->access$200(Lflow/Flow;)Lflow/History;

    move-result-object v0

    invoke-virtual {v0}, Lflow/History;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 187
    invoke-virtual {p0}, Lflow/Flow$3;->onTraversalCompleted()V

    goto :goto_0

    .line 189
    :cond_0
    iget-object v0, p0, Lflow/Flow$3;->this$0:Lflow/Flow;

    invoke-static {v0}, Lflow/Flow;->access$200(Lflow/Flow;)Lflow/History;

    move-result-object v0

    invoke-virtual {v0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    .line 190
    invoke-virtual {v0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    .line 191
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v0

    .line 192
    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-virtual {p0, v0, v1}, Lflow/Flow$3;->dispatch(Lflow/History;Lflow/Direction;)V

    :goto_0
    return-void
.end method
