.class final Lhu/akarnokd/rxjava/interop/CompletableV1ToCompletableV2$SourceCompletableSubscriber;
.super Ljava/lang/Object;
.source "CompletableV1ToCompletableV2.java"

# interfaces
.implements Lrx/CompletableSubscriber;
.implements Lio/reactivex/disposables/Disposable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhu/akarnokd/rxjava/interop/CompletableV1ToCompletableV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SourceCompletableSubscriber"
.end annotation


# instance fields
.field final observer:Lio/reactivex/CompletableObserver;

.field s:Lrx/Subscription;


# direct methods
.method constructor <init>(Lio/reactivex/CompletableObserver;)V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/CompletableV1ToCompletableV2$SourceCompletableSubscriber;->observer:Lio/reactivex/CompletableObserver;

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 64
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/CompletableV1ToCompletableV2$SourceCompletableSubscriber;->s:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .line 69
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/CompletableV1ToCompletableV2$SourceCompletableSubscriber;->s:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->isUnsubscribed()Z

    move-result v0

    return v0
.end method

.method public onCompleted()V
    .locals 1

    .line 54
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/CompletableV1ToCompletableV2$SourceCompletableSubscriber;->observer:Lio/reactivex/CompletableObserver;

    invoke-interface {v0}, Lio/reactivex/CompletableObserver;->onComplete()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 59
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/CompletableV1ToCompletableV2$SourceCompletableSubscriber;->observer:Lio/reactivex/CompletableObserver;

    invoke-interface {v0, p1}, Lio/reactivex/CompletableObserver;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onSubscribe(Lrx/Subscription;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/CompletableV1ToCompletableV2$SourceCompletableSubscriber;->s:Lrx/Subscription;

    .line 49
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/CompletableV1ToCompletableV2$SourceCompletableSubscriber;->observer:Lio/reactivex/CompletableObserver;

    invoke-interface {p1, p0}, Lio/reactivex/CompletableObserver;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
