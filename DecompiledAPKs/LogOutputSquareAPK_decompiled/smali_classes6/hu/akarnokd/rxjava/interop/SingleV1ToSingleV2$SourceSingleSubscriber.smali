.class final Lhu/akarnokd/rxjava/interop/SingleV1ToSingleV2$SourceSingleSubscriber;
.super Lrx/SingleSubscriber;
.source "SingleV1ToSingleV2.java"

# interfaces
.implements Lio/reactivex/disposables/Disposable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhu/akarnokd/rxjava/interop/SingleV1ToSingleV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SourceSingleSubscriber"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/SingleSubscriber<",
        "TT;>;",
        "Lio/reactivex/disposables/Disposable;"
    }
.end annotation


# instance fields
.field final observer:Lio/reactivex/SingleObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/SingleObserver<",
            "-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/SingleObserver;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/SingleObserver<",
            "-TT;>;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Lrx/SingleSubscriber;-><init>()V

    .line 45
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/SingleV1ToSingleV2$SourceSingleSubscriber;->observer:Lio/reactivex/SingleObserver;

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 0

    .line 65
    invoke-virtual {p0}, Lhu/akarnokd/rxjava/interop/SingleV1ToSingleV2$SourceSingleSubscriber;->unsubscribe()V

    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .line 70
    invoke-virtual {p0}, Lhu/akarnokd/rxjava/interop/SingleV1ToSingleV2$SourceSingleSubscriber;->isUnsubscribed()Z

    move-result v0

    return v0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 60
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SingleV1ToSingleV2$SourceSingleSubscriber;->observer:Lio/reactivex/SingleObserver;

    invoke-interface {v0, p1}, Lio/reactivex/SingleObserver;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 51
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/SingleV1ToSingleV2$SourceSingleSubscriber;->observer:Lio/reactivex/SingleObserver;

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "The upstream 1.x Single signalled a null value which is not supported in 2.x"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lio/reactivex/SingleObserver;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 54
    :cond_0
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SingleV1ToSingleV2$SourceSingleSubscriber;->observer:Lio/reactivex/SingleObserver;

    invoke-interface {v0, p1}, Lio/reactivex/SingleObserver;->onSuccess(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
