.class final Lhu/akarnokd/rxjava/interop/RxJavaInterop$6;
.super Ljava/lang/Object;
.source "RxJavaInterop.java"

# interfaces
.implements Lrx/Observable$Transformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Transformer(Lio/reactivex/FlowableTransformer;)Lrx/Observable$Transformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observable$Transformer<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic val$transformer:Lio/reactivex/FlowableTransformer;


# direct methods
.method constructor <init>(Lio/reactivex/FlowableTransformer;)V
    .locals 0

    .line 522
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$6;->val$transformer:Lio/reactivex/FlowableTransformer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 522
    check-cast p1, Lrx/Observable;

    invoke-virtual {p0, p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$6;->call(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public call(Lrx/Observable;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "TT;>;)",
            "Lrx/Observable<",
            "TR;>;"
        }
    .end annotation

    .line 525
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$6;->val$transformer:Lio/reactivex/FlowableTransformer;

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Flowable(Lrx/Observable;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-interface {v0, p1}, Lio/reactivex/FlowableTransformer;->apply(Lio/reactivex/Flowable;)Lorg/reactivestreams/Publisher;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lorg/reactivestreams/Publisher;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
