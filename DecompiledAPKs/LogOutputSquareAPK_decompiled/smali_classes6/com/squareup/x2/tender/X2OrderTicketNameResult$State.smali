.class public final enum Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;
.super Ljava/lang/Enum;
.source "X2OrderTicketNameResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/x2/tender/X2OrderTicketNameResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

.field public static final enum CHIP_CARD_REMOVED:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

.field public static final enum CHIP_CARD_SWIPED:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

.field public static final enum FAILURE:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

.field public static final enum LOADING:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

.field public static final enum SUCCESS:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

.field public static final enum SWIPE_STRAIGHT:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 8
    new-instance v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    const/4 v1, 0x0

    const-string v2, "SUCCESS"

    invoke-direct {v0, v2, v1}, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->SUCCESS:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    .line 9
    new-instance v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    const/4 v2, 0x1

    const-string v3, "FAILURE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->FAILURE:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    .line 10
    new-instance v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    const/4 v3, 0x2

    const-string v4, "SWIPE_STRAIGHT"

    invoke-direct {v0, v4, v3}, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->SWIPE_STRAIGHT:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    .line 11
    new-instance v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    const/4 v4, 0x3

    const-string v5, "CHIP_CARD_SWIPED"

    invoke-direct {v0, v5, v4}, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->CHIP_CARD_SWIPED:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    .line 12
    new-instance v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    const/4 v5, 0x4

    const-string v6, "LOADING"

    invoke-direct {v0, v6, v5}, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->LOADING:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    .line 13
    new-instance v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    const/4 v6, 0x5

    const-string v7, "CHIP_CARD_REMOVED"

    invoke-direct {v0, v7, v6}, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->CHIP_CARD_REMOVED:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    .line 7
    sget-object v7, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->SUCCESS:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->FAILURE:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->SWIPE_STRAIGHT:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->CHIP_CARD_SWIPED:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->LOADING:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->CHIP_CARD_REMOVED:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->$VALUES:[Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;
    .locals 1

    .line 7
    const-class v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;
    .locals 1

    .line 7
    sget-object v0, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->$VALUES:[Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    invoke-virtual {v0}, [Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    return-object v0
.end method
