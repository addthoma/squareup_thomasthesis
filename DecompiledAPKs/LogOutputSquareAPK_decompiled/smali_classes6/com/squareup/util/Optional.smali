.class public abstract Lcom/squareup/util/Optional;
.super Ljava/lang/Object;
.source "Optional.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/Optional$Empty;,
        Lcom/squareup/util/Optional$Present;,
        Lcom/squareup/util/Optional$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u0003\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 $*\n\u0008\u0000\u0010\u0001 \u0001*\u00020\u00022\u00020\u0002:\u0003$%&B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\"\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00050\u000eH&J2\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u00100\u0000\"\u0008\u0008\u0001\u0010\u0010*\u00020\u00022\u0018\u0010\u0011\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00100\u00000\u000eH&J\u001c\u0010\u0012\u001a\u00020\u00132\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00130\u000eH&J*\u0010\u0015\u001a\u00020\u00132\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00130\u000e2\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0017H&J.\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u0002H\u00100\u0000\"\u0008\u0008\u0001\u0010\u0010*\u00020\u00022\u0014\u0010\u0011\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0006\u0012\u0004\u0018\u0001H\u00100\u000eH&J\"\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0012\u0010\u001a\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u00000\u0017H&J\u0013\u0010\u001b\u001a\u00028\u00002\u0006\u0010\u001c\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u001dJ\u0019\u0010\u001e\u001a\u00028\u00002\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0017\u00a2\u0006\u0002\u0010\u001fJ%\u0010 \u001a\u00028\u0000\"\u0008\u0008\u0001\u0010!*\u00020\"2\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u0002H!0\u0017H&\u00a2\u0006\u0002\u0010\u001fR\u0012\u0010\u0004\u001a\u00020\u0005X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0006R\u0012\u0010\u0007\u001a\u00028\u0000X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0013\u0010\n\u001a\u0004\u0018\u00018\u00008F\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\t\u0082\u0001\u0002\'(\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/util/Optional;",
        "T",
        "",
        "()V",
        "isPresent",
        "",
        "()Z",
        "value",
        "getValue",
        "()Ljava/lang/Object;",
        "valueOrNull",
        "getValueOrNull",
        "filter",
        "predicate",
        "Lkotlin/Function1;",
        "flatMap",
        "U",
        "mapper",
        "ifPresent",
        "",
        "action",
        "ifPresentOrElse",
        "emptyAction",
        "Lkotlin/Function0;",
        "map",
        "or",
        "supplier",
        "orElse",
        "other",
        "(Ljava/lang/Object;)Ljava/lang/Object;",
        "orElseGet",
        "(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;",
        "orElseThrow",
        "X",
        "",
        "exceptionSupplier",
        "Companion",
        "Empty",
        "Present",
        "Lcom/squareup/util/Optional$Empty;",
        "Lcom/squareup/util/Optional$Present;",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/util/Optional$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/util/Optional$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/util/Optional$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/util/Optional;-><init>()V

    return-void
.end method

.method public static final empty()Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/squareup/util/Optional<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v0}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    return-object v0
.end method

.method public static final of(Ljava/lang/Object;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/squareup/util/Optional<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p0

    return-object p0
.end method

.method public static final ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/squareup/util/Optional<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/util/Optional$Companion;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract filter(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/util/Optional<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract flatMap(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lcom/squareup/util/Optional<",
            "+TU;>;>;)",
            "Lcom/squareup/util/Optional<",
            "TU;>;"
        }
    .end annotation
.end method

.method public abstract getValue()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final getValueOrNull()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 49
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public abstract ifPresent(Lkotlin/jvm/functions/Function1;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract ifPresentOrElse(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract isPresent()Z
.end method

.method public abstract map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+TU;>;)",
            "Lcom/squareup/util/Optional<",
            "TU;>;"
        }
    .end annotation
.end method

.method public abstract or(Lkotlin/jvm/functions/Function0;)Lcom/squareup/util/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Lcom/squareup/util/Optional<",
            "+TT;>;>;)",
            "Lcom/squareup/util/Optional<",
            "TT;>;"
        }
    .end annotation
.end method

.method public final orElse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    instance-of v0, p0, Lcom/squareup/util/Optional$Present;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 129
    :cond_0
    instance-of v0, p0, Lcom/squareup/util/Optional$Empty;

    if-eqz v0, :cond_1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final orElseGet(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "+TT;>;)TT;"
        }
    .end annotation

    const-string v0, "supplier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    instance-of v0, p0, Lcom/squareup/util/Optional$Present;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 141
    :cond_0
    instance-of v0, p0, Lcom/squareup/util/Optional$Empty;

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public abstract orElseThrow(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<X:",
            "Ljava/lang/Throwable;",
            ">(",
            "Lkotlin/jvm/functions/Function0<",
            "+TX;>;)TT;"
        }
    .end annotation
.end method
