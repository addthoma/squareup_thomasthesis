.class public Lcom/squareup/util/ISO8601Dates;
.super Ljava/lang/Object;
.source "ISO8601Dates.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static now()Lcom/squareup/protos/client/ISO8601Date;
    .locals 2

    .line 29
    new-instance v0, Lcom/squareup/protos/client/ISO8601Date$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ISO8601Date$Builder;-><init>()V

    .line 30
    invoke-static {}, Lcom/squareup/util/Times;->nowAsIso8601()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/ISO8601Date$Builder;->date_string(Ljava/lang/String;)Lcom/squareup/protos/client/ISO8601Date$Builder;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/squareup/protos/client/ISO8601Date$Builder;->build()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v0

    return-object v0
.end method

.method public static tryBuildISO8601Date(Ljava/util/Date;)Lcom/squareup/protos/client/ISO8601Date;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 16
    :cond_0
    new-instance v0, Lcom/squareup/protos/client/ISO8601Date$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ISO8601Date$Builder;-><init>()V

    .line 17
    invoke-static {p0}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/ISO8601Date$Builder;->date_string(Ljava/lang/String;)Lcom/squareup/protos/client/ISO8601Date$Builder;

    move-result-object p0

    .line 18
    invoke-virtual {p0}, Lcom/squareup/protos/client/ISO8601Date$Builder;->build()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object p0

    return-object p0
.end method

.method public static tryBuildISO8601Date(Ljava/util/Date;Ljava/util/TimeZone;)Lcom/squareup/protos/client/ISO8601Date;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 23
    :cond_0
    new-instance v0, Lcom/squareup/protos/client/ISO8601Date$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ISO8601Date$Builder;-><init>()V

    .line 24
    invoke-static {p0, p1}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/ISO8601Date$Builder;->date_string(Ljava/lang/String;)Lcom/squareup/protos/client/ISO8601Date$Builder;

    move-result-object p0

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/ISO8601Date$Builder;->build()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object p0

    return-object p0
.end method
