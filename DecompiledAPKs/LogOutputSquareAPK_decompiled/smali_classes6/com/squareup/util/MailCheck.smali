.class public Lcom/squareup/util/MailCheck;
.super Ljava/lang/Object;
.source "MailCheck.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/MailCheck$EmailSplit;
    }
.end annotation


# static fields
.field private static final ALL_TLDS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEFAULT_DOMAINS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEFAULT_TLDS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final EMAIL_FORMAT:Ljava/lang/String; = "%s@%s"

.field private static final WORLD_TLDS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 177

    const-string v0, "gmail.com"

    const-string v1, "yahoo.com"

    const-string v2, "hotmail.com"

    const-string v3, "aol.com"

    const-string v4, "comcast.net"

    const-string v5, "me.com"

    const-string v6, "live.com"

    const-string v7, "msn.com"

    const-string v8, "icloud.com"

    const-string v9, "sbcglobal.net"

    const-string v10, "outlook.com"

    const-string v11, "att.net"

    const-string v12, "ymail.com"

    const-string v13, "verizon.net"

    const-string v14, "mac.com"

    const-string v15, "cox.net"

    const-string v16, "bellsouth.net"

    const-string v17, "163.com"

    const-string v18, "rocketmail.com"

    const-string v19, "tom.com"

    const-string v20, "mail.com"

    const-string v21, "charter.net"

    const-string v22, "earthlink.net"

    const-string v23, "sina.com"

    const-string v24, "yahoo.ca"

    const-string v25, "126.com"

    const-string v26, "aim.com"

    const-string v27, "shaw.ca"

    const-string v28, "yahoo.co.jp"

    const-string v29, "live.ca"

    const-string v30, "optonline.net"

    const-string v31, "squareup.com"

    const-string v32, "i.softbank.jp"

    const-string v33, "hotmail.ca"

    const-string v34, "windstream.net"

    const-string v35, "juno.com"

    const-string v36, "rogers.com"

    const-string v37, "roadrunner.com"

    const-string v38, "yopmail.com"

    const-string v39, "embarqmail.com"

    const-string v40, "frontier.com"

    const-string v41, "gmx.com"

    const-string v42, "telus.net"

    const-string v43, "q.com"

    const-string v44, "marykay.com"

    const-string v45, "ezweb.ne.jp"

    const-string v46, "mail.ru"

    const-string v47, "docomo.ne.jp"

    const-string v48, "cfl.rr.com"

    const-string v49, "videotron.ca"

    const-string v50, "suddenlink.net"

    const-string v51, "tampabay.rr.com"

    const-string v52, "hotmail.co.uk"

    const-string v53, "hotmail.fr"

    const-string v54, "yahoo.fr"

    const-string v55, "cableone.net"

    const-string v56, "sympatico.ca"

    const-string v57, "centurylink.net"

    const-string v58, "square.com"

    const-string v59, "yahoo.es"

    const-string v60, "yahoo.co.uk"

    const-string v61, "mindspring.com"

    const-string v62, "netzero.com"

    const-string v63, "pacbell.net"

    const-string v64, "netzero.net"

    const-string v65, "mchsi.com"

    const-string v66, "email.com"

    const-string v67, "frontiernet.net"

    const-string v68, "googlemail.com"

    const-string v69, "nc.rr.com"

    const-string v70, "umich.edu"

    const-string v71, "centurytel.net"

    const-string v72, "123.com"

    const-string v73, "tds.net"

    const-string v74, "usa.com"

    const-string v75, "netscape.net"

    const-string v76, "swbell.net"

    const-string v77, "austin.rr.com"

    const-string v78, "google.com"

    const-string v79, "facebook.com"

    const-string v80, "inbox.com"

    const-string v81, "wi.rr.com"

    const-string v82, "insightbb.com"

    const-string v83, "hughes.net"

    const-string v84, "carolina.rr.com"

    const-string v85, "mailinator.com"

    const-string v86, "bell.net"

    const-string v87, "prodigy.net"

    const-string v88, "rochester.rr.com"

    const-string v89, "ptd.net"

    const-string v90, "hawaii.rr.com"

    const-string v91, "softbank.ne.jp"

    const-string v92, "hushmail.com"

    .line 38
    filled-new-array/range {v0 .. v92}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/MailCheck;->DEFAULT_DOMAINS:Ljava/util/List;

    const-string v1, "ae"

    const-string v2, "ag"

    const-string v3, "ai"

    const-string v4, "al"

    const-string v5, "am"

    const-string v6, "ao"

    const-string v7, "ar"

    const-string v8, "at"

    const-string v9, "au"

    const-string v10, "aw"

    const-string v11, "az"

    const-string v12, "ba"

    const-string v13, "bb"

    const-string v14, "bd"

    const-string v15, "be"

    const-string v16, "bf"

    const-string v17, "bg"

    const-string v18, "bh"

    const-string v19, "bj"

    const-string v20, "bm"

    const-string v21, "bn"

    const-string v22, "bo"

    const-string v23, "br"

    const-string v24, "bs"

    const-string v25, "bt"

    const-string v26, "bw"

    const-string v27, "by"

    const-string v28, "bz"

    const-string v29, "ca"

    const-string v30, "cg"

    const-string v31, "ch"

    const-string v32, "ci"

    const-string v33, "cl"

    const-string v34, "cm"

    const-string v35, "cn"

    const-string v36, "co"

    const-string v37, "cr"

    const-string v38, "cv"

    const-string v39, "cy"

    const-string v40, "cz"

    const-string v41, "de"

    const-string v42, "dk"

    const-string v43, "dm"

    const-string v44, "do"

    const-string v45, "dz"

    const-string v46, "ec"

    const-string v47, "ee"

    const-string v48, "eg"

    const-string v49, "es"

    const-string v50, "et"

    const-string v51, "eu"

    const-string v52, "fi"

    const-string v53, "fj"

    const-string v54, "fm"

    const-string v55, "fr"

    const-string v56, "ga"

    const-string v57, "gb"

    const-string v58, "gd"

    const-string v59, "ge"

    const-string v60, "gh"

    const-string v61, "gi"

    const-string v62, "gm"

    const-string v63, "gr"

    const-string v64, "gt"

    const-string v65, "gw"

    const-string v66, "gy"

    const-string v67, "hk"

    const-string v68, "hn"

    const-string v69, "hr"

    const-string v70, "ht"

    const-string v71, "hu"

    const-string v72, "id"

    const-string v73, "ie"

    const-string v74, "il"

    const-string v75, "in"

    const-string v76, "is"

    const-string v77, "it"

    const-string v78, "jm"

    const-string v79, "jo"

    const-string v80, "jp"

    const-string v81, "ke"

    const-string v82, "kg"

    const-string v83, "kh"

    const-string v84, "kn"

    const-string v85, "kr"

    const-string v86, "kw"

    const-string v87, "ky"

    const-string v88, "kz"

    const-string v89, "la"

    const-string v90, "lb"

    const-string v91, "lc"

    const-string v92, "li"

    const-string v93, "lk"

    const-string v94, "lr"

    const-string v95, "ls"

    const-string v96, "lt"

    const-string v97, "lu"

    const-string v98, "lv"

    const-string v99, "ma"

    const-string v100, "md"

    const-string v101, "mg"

    const-string v102, "mk"

    const-string v103, "ml"

    const-string v104, "mm"

    const-string v105, "mn"

    const-string v106, "mo"

    const-string v107, "mr"

    const-string v108, "ms"

    const-string v109, "mt"

    const-string v110, "mu"

    const-string v111, "mw"

    const-string v112, "mx"

    const-string v113, "my"

    const-string v114, "mz"

    const-string v115, "na"

    const-string v116, "ne"

    const-string v117, "ng"

    const-string v118, "ni"

    const-string v119, "nl"

    const-string v120, "no"

    const-string v121, "np"

    const-string v122, "nz"

    const-string v123, "om"

    const-string v124, "pa"

    const-string v125, "pe"

    const-string v126, "pg"

    const-string v127, "ph"

    const-string v128, "pk"

    const-string v129, "pl"

    const-string v130, "pr"

    const-string v131, "pt"

    const-string v132, "pw"

    const-string v133, "py"

    const-string v134, "qa"

    const-string v135, "ro"

    const-string v136, "rs"

    const-string v137, "ru"

    const-string v138, "rw"

    const-string v139, "sa"

    const-string v140, "sb"

    const-string v141, "sc"

    const-string v142, "se"

    const-string v143, "sg"

    const-string v144, "si"

    const-string v145, "sk"

    const-string v146, "sl"

    const-string v147, "sn"

    const-string v148, "sr"

    const-string v149, "st"

    const-string v150, "sv"

    const-string v151, "sz"

    const-string v152, "tc"

    const-string v153, "td"

    const-string v154, "tg"

    const-string v155, "th"

    const-string v156, "tj"

    const-string v157, "tm"

    const-string v158, "tn"

    const-string v159, "tr"

    const-string v160, "tt"

    const-string v161, "tw"

    const-string v162, "tz"

    const-string v163, "ua"

    const-string v164, "ug"

    const-string v165, "uk"

    const-string v166, "us"

    const-string v167, "uy"

    const-string v168, "uz"

    const-string v169, "vc"

    const-string v170, "ve"

    const-string v171, "vg"

    const-string v172, "vn"

    const-string v173, "ye"

    const-string v174, "za"

    const-string v175, "zm"

    const-string v176, "zw"

    .line 56
    filled-new-array/range {v1 .. v176}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/MailCheck;->WORLD_TLDS:Ljava/util/List;

    const-string v1, "au"

    const-string v2, "com.au"

    const-string v3, "net.au"

    const-string v4, "org.au"

    const-string v5, "edu.au"

    const-string v6, "ca"

    const-string v7, "co.uk"

    const-string v8, "ac.uk"

    const-string v9, "com"

    const-string v10, "edu"

    const-string v11, "gov"

    const-string v12, "info"

    const-string v13, "jp"

    const-string v14, "ac.jp"

    const-string v15, "co.jp"

    const-string v16, "mil"

    const-string v17, "net"

    const-string v18, "org"

    const-string v19, "us"

    .line 239
    filled-new-array/range {v1 .. v19}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/MailCheck;->DEFAULT_TLDS:Ljava/util/List;

    .line 243
    invoke-static {}, Lcom/squareup/util/MailCheck;->allTLDs()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/MailCheck;->ALL_TLDS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static allTLDs()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 246
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/squareup/util/MailCheck;->WORLD_TLDS:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 247
    sget-object v1, Lcom/squareup/util/MailCheck;->DEFAULT_TLDS:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 248
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static check(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 8

    .line 307
    invoke-static {p0}, Lcom/squareup/util/MailCheck;->splitEmail(Ljava/lang/String;)Lcom/squareup/util/MailCheck$EmailSplit;

    move-result-object p0

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 313
    :cond_0
    iget-object v1, p0, Lcom/squareup/util/MailCheck$EmailSplit;->domain:Ljava/lang/String;

    sget-object v2, Lcom/squareup/util/MailCheck;->DEFAULT_DOMAINS:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/squareup/util/MailCheck;->suggestFromList(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 315
    iget-object v2, p0, Lcom/squareup/util/MailCheck$EmailSplit;->domain:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v0

    :cond_1
    const/4 v2, 0x1

    const/4 v3, 0x2

    const-string v4, "%s@%s"

    const/4 v5, 0x0

    if-eqz v1, :cond_2

    new-array p1, v3, [Ljava/lang/Object;

    .line 322
    iget-object p0, p0, Lcom/squareup/util/MailCheck$EmailSplit;->localpart:Ljava/lang/String;

    aput-object p0, p1, v5

    aput-object v1, p1, v2

    invoke-static {v4, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 326
    :cond_2
    iget-object v1, p0, Lcom/squareup/util/MailCheck$EmailSplit;->tld:Ljava/lang/String;

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/util/MailCheck;->ALL_TLDS:Ljava/util/List;

    goto :goto_0

    :cond_3
    sget-object p1, Lcom/squareup/util/MailCheck;->DEFAULT_TLDS:Ljava/util/List;

    :goto_0
    invoke-static {v1, p1}, Lcom/squareup/util/MailCheck;->suggestFromList(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 328
    iget-object v1, p0, Lcom/squareup/util/MailCheck$EmailSplit;->tld:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_1

    .line 334
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/util/MailCheck$EmailSplit;->domain:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/util/MailCheck$EmailSplit;->domain:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/util/MailCheck$EmailSplit;->tld:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v0, v3, [Ljava/lang/Object;

    .line 337
    iget-object p0, p0, Lcom/squareup/util/MailCheck$EmailSplit;->localpart:Ljava/lang/String;

    aput-object p0, v0, v5

    aput-object p1, v0, v2

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_5
    :goto_1
    return-object v0
.end method

.method public static containsTld(Ljava/lang/String;Z)Z
    .locals 4

    .line 268
    invoke-static {p0}, Lcom/squareup/util/MailCheck;->splitEmail(Ljava/lang/String;)Lcom/squareup/util/MailCheck$EmailSplit;

    move-result-object p0

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 274
    :cond_0
    iget-object v1, p0, Lcom/squareup/util/MailCheck$EmailSplit;->tld:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    return v0

    :cond_1
    if-eqz p1, :cond_2

    .line 278
    sget-object p1, Lcom/squareup/util/MailCheck;->ALL_TLDS:Ljava/util/List;

    goto :goto_0

    :cond_2
    sget-object p1, Lcom/squareup/util/MailCheck;->DEFAULT_TLDS:Ljava/util/List;

    :goto_0
    iget-object v1, p0, Lcom/squareup/util/MailCheck$EmailSplit;->tld:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    return v2

    .line 282
    :cond_3
    sget-object p1, Lcom/squareup/util/MailCheck;->DEFAULT_TLDS:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 283
    iget-object v3, p0, Lcom/squareup/util/MailCheck$EmailSplit;->tld:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    return v0

    :cond_5
    return v2
.end method

.method private static splitEmail(Ljava/lang/String;)Lcom/squareup/util/MailCheck$EmailSplit;
    .locals 6

    .line 370
    invoke-static {p0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    const-string v0, "@"

    .line 374
    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    .line 376
    array-length v0, p0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    return-object v1

    :cond_1
    const/4 v0, 0x0

    .line 380
    aget-object v0, p0, v0

    .line 381
    array-length v2, p0

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    aget-object p0, p0, v2

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    .line 383
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    const-string v2, "\\."

    .line 387
    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 389
    array-length v4, v2

    if-gt v4, v3, :cond_3

    .line 390
    new-instance v2, Lcom/squareup/util/MailCheck$EmailSplit;

    const-string v3, ""

    invoke-direct {v2, v0, p0, v3, v1}, Lcom/squareup/util/MailCheck$EmailSplit;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/util/MailCheck$1;)V

    return-object v2

    .line 393
    :cond_3
    array-length v4, v2

    const-string v5, "."

    invoke-static {v2, v5, v3, v4}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v2

    .line 394
    new-instance v3, Lcom/squareup/util/MailCheck$EmailSplit;

    invoke-direct {v3, v0, p0, v2, v1}, Lcom/squareup/util/MailCheck$EmailSplit;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/util/MailCheck$1;)V

    return-object v3

    :cond_4
    :goto_0
    return-object v1
.end method

.method private static suggestFromList(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 341
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    const/16 v0, 0x63

    .line 347
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object v3, v1

    :goto_0
    if-ltz v2, :cond_4

    .line 348
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 349
    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    return-object p0

    .line 350
    :cond_1
    invoke-static {p0, v4}, Lcom/squareup/util/Strings;->getDistance(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    if-ge v5, v0, :cond_2

    move-object v3, v4

    move v0, v5

    goto :goto_1

    :cond_2
    if-ne v5, v0, :cond_3

    .line 354
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v5, v6, :cond_3

    move-object v3, v4

    :cond_3
    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_4
    const/4 p0, 0x3

    if-gt v0, p0, :cond_5

    move-object v1, v3

    :cond_5
    return-object v1
.end method
