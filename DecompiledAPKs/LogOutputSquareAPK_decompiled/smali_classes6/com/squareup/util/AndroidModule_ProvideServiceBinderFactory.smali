.class public final Lcom/squareup/util/AndroidModule_ProvideServiceBinderFactory;
.super Ljava/lang/Object;
.source "AndroidModule_ProvideServiceBinderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/util/ServiceBinder;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/util/AndroidModule_ProvideServiceBinderFactory;->contextProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/util/AndroidModule_ProvideServiceBinderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/util/AndroidModule_ProvideServiceBinderFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/util/AndroidModule_ProvideServiceBinderFactory;

    invoke-direct {v0, p0}, Lcom/squareup/util/AndroidModule_ProvideServiceBinderFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideServiceBinder(Landroid/app/Application;)Lcom/squareup/util/ServiceBinder;
    .locals 1

    .line 35
    invoke-static {p0}, Lcom/squareup/util/AndroidModule;->provideServiceBinder(Landroid/app/Application;)Lcom/squareup/util/ServiceBinder;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/util/ServiceBinder;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/util/ServiceBinder;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/util/AndroidModule_ProvideServiceBinderFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/util/AndroidModule_ProvideServiceBinderFactory;->provideServiceBinder(Landroid/app/Application;)Lcom/squareup/util/ServiceBinder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/util/AndroidModule_ProvideServiceBinderFactory;->get()Lcom/squareup/util/ServiceBinder;

    move-result-object v0

    return-object v0
.end method
