.class public final Lcom/squareup/util/NoAppNameFormatter_Factory;
.super Ljava/lang/Object;
.source "NoAppNameFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/util/NoAppNameFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/util/NoAppNameFormatter_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/util/NoAppNameFormatter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/util/NoAppNameFormatter_Factory;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/util/NoAppNameFormatter_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/util/NoAppNameFormatter_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;)Lcom/squareup/util/NoAppNameFormatter;
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/util/NoAppNameFormatter;

    invoke-direct {v0, p0}, Lcom/squareup/util/NoAppNameFormatter;-><init>(Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/util/NoAppNameFormatter;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/util/NoAppNameFormatter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {v0}, Lcom/squareup/util/NoAppNameFormatter_Factory;->newInstance(Lcom/squareup/util/Res;)Lcom/squareup/util/NoAppNameFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/util/NoAppNameFormatter_Factory;->get()Lcom/squareup/util/NoAppNameFormatter;

    move-result-object v0

    return-object v0
.end method
