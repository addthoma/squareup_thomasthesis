.class public final Lcom/squareup/util/Res$RealRes;
.super Ljava/lang/Object;
.source "Res.kt"

# interfaces
.implements Lcom/squareup/util/Res;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/Res;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RealRes"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRes.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Res.kt\ncom/squareup/util/Res$RealRes\n*L\n1#1,101:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0001\u0010\r\u001a\u00020\u000eH\u0016J\u0012\u0010\u000f\u001a\u00020\u000e2\u0008\u0008\u0001\u0010\r\u001a\u00020\u000eH\u0017J\u0012\u0010\u0010\u001a\u00020\u00112\u0008\u0008\u0001\u0010\r\u001a\u00020\u000eH\u0016J\u0012\u0010\u0012\u001a\u00020\u00132\u0008\u0008\u0001\u0010\r\u001a\u00020\u000eH\u0016J\u0012\u0010\u0014\u001a\u00020\u000e2\u0008\u0008\u0001\u0010\r\u001a\u00020\u000eH\u0016J\u0012\u0010\u0015\u001a\u00020\u00162\u0008\u0008\u0001\u0010\r\u001a\u00020\u000eH\u0016J\u0012\u0010\u0017\u001a\u00020\u000e2\u0008\u0008\u0001\u0010\r\u001a\u00020\u000eH\u0016J\u0012\u0010\u0018\u001a\u00020\u00192\u0008\u0008\u0001\u0010\r\u001a\u00020\u000eH\u0016J\u001d\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u001b2\u0008\u0008\u0001\u0010\r\u001a\u00020\u000eH\u0016\u00a2\u0006\u0002\u0010\u001cJ\u0012\u0010\u001d\u001a\u00020\u001e2\u0008\u0008\u0001\u0010\r\u001a\u00020\u000eH\u0016J\u0012\u0010\u001f\u001a\u00020 2\u0008\u0008\u0001\u0010!\u001a\u00020\u000eH\u0016R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/util/Res$RealRes;",
        "Lcom/squareup/util/Res;",
        "resources",
        "Landroid/content/res/Resources;",
        "(Landroid/content/res/Resources;)V",
        "assets",
        "Landroid/content/res/AssetManager;",
        "getAssets",
        "()Landroid/content/res/AssetManager;",
        "getResources",
        "()Landroid/content/res/Resources;",
        "getBoolean",
        "",
        "id",
        "",
        "getColor",
        "getColorStateList",
        "Landroid/content/res/ColorStateList;",
        "getDimension",
        "",
        "getDimensionPixelSize",
        "getDrawable",
        "Landroid/graphics/drawable/Drawable;",
        "getInteger",
        "getString",
        "",
        "getStringArray",
        "",
        "(I)[Ljava/lang/String;",
        "getText",
        "",
        "phrase",
        "Lcom/squareup/phrase/Phrase;",
        "patternResourceId",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assets:Landroid/content/res/AssetManager;

.field private final resources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/util/Res$RealRes;->resources:Landroid/content/res/Resources;

    .line 80
    invoke-virtual {p0}, Lcom/squareup/util/Res$RealRes;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object p1

    const-string v0, "resources.assets"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/util/Res$RealRes;->assets:Landroid/content/res/AssetManager;

    return-void
.end method


# virtual methods
.method public getAssets()Landroid/content/res/AssetManager;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/util/Res$RealRes;->assets:Landroid/content/res/AssetManager;

    return-object v0
.end method

.method public getBoolean(I)Z
    .locals 1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/util/Res$RealRes;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p1

    return p1
.end method

.method public getColor(I)I
    .locals 2

    .line 86
    invoke-virtual {p0}, Lcom/squareup/util/Res$RealRes;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroidx/core/content/res/ResourcesCompat;->getColor(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)I

    move-result p1

    return p1
.end method

.method public getColorStateList(I)Landroid/content/res/ColorStateList;
    .locals 2

    .line 89
    invoke-virtual {p0}, Lcom/squareup/util/Res$RealRes;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroidx/core/content/res/ResourcesCompat;->getColorStateList(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/content/res/ColorStateList;

    move-result-object p1

    if-eqz p1, :cond_0

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public getDimension(I)F
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/util/Res$RealRes;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    return p1
.end method

.method public getDimensionPixelSize(I)I
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/util/Res$RealRes;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    return p1
.end method

.method public getDrawable(I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .line 98
    invoke-virtual {p0}, Lcom/squareup/util/Res$RealRes;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_0

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public getInteger(I)I
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/util/Res$RealRes;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    return p1
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/util/Res$RealRes;->resources:Landroid/content/res/Resources;

    return-object v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/util/Res$RealRes;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(id)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getStringArray(I)[Ljava/lang/String;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/util/Res$RealRes;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getStringArray(id)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getText(I)Ljava/lang/CharSequence;
    .locals 1

    .line 85
    invoke-virtual {p0}, Lcom/squareup/util/Res$RealRes;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "resources.getText(id)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public phrase(I)Lcom/squareup/phrase/Phrase;
    .locals 1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/util/Res$RealRes;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v0, "Phrase.from(resources, patternResourceId)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
