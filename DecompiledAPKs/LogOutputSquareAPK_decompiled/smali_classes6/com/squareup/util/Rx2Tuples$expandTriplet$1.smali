.class final Lcom/squareup/util/Rx2Tuples$expandTriplet$1;
.super Ljava/lang/Object;
.source "Rx2Tuples.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/Rx2Tuples;->expandTriplet(Lio/reactivex/functions/Function3;)Lio/reactivex/functions/Consumer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/util/tuple/Triplet<",
        "TA;TB;TC;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u000322\u0010\u0006\u001a.\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005 \u0008*\u0016\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005\u0018\u00010\u00070\u0007H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "A",
        "",
        "B",
        "C",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/util/tuple/Triplet;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_expandTriplet:Lio/reactivex/functions/Function3;


# direct methods
.method constructor <init>(Lio/reactivex/functions/Function3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/Rx2Tuples$expandTriplet$1;->$this_expandTriplet:Lio/reactivex/functions/Function3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/util/tuple/Triplet;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/tuple/Triplet<",
            "TA;TB;TC;>;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Triplet;->component1()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Triplet;->component2()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Triplet;->component3()Ljava/lang/Object;

    move-result-object p1

    .line 31
    iget-object v2, p0, Lcom/squareup/util/Rx2Tuples$expandTriplet$1;->$this_expandTriplet:Lio/reactivex/functions/Function3;

    invoke-interface {v2, v0, v1, p1}, Lio/reactivex/functions/Function3;->apply(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/util/tuple/Triplet;

    invoke-virtual {p0, p1}, Lcom/squareup/util/Rx2Tuples$expandTriplet$1;->accept(Lcom/squareup/util/tuple/Triplet;)V

    return-void
.end method
