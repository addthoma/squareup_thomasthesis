.class final Lcom/squareup/util/VoidParcelable$1;
.super Ljava/lang/Object;
.source "VoidParcelable.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/VoidParcelable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/util/VoidParcelable;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/util/VoidParcelable;
    .locals 0

    .line 20
    new-instance p1, Lcom/squareup/util/VoidParcelable;

    invoke-direct {p1}, Lcom/squareup/util/VoidParcelable;-><init>()V

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 18
    invoke-virtual {p0, p1}, Lcom/squareup/util/VoidParcelable$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/util/VoidParcelable;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/util/VoidParcelable;
    .locals 0

    .line 24
    new-array p1, p1, [Lcom/squareup/util/VoidParcelable;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 18
    invoke-virtual {p0, p1}, Lcom/squareup/util/VoidParcelable$1;->newArray(I)[Lcom/squareup/util/VoidParcelable;

    move-result-object p1

    return-object p1
.end method
