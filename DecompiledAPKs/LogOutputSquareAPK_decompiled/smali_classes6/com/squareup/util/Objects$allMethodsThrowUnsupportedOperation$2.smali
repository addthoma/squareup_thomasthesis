.class final Lcom/squareup/util/Objects$allMethodsThrowUnsupportedOperation$2;
.super Ljava/lang/Object;
.source "Objects.kt"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation(Ljava/lang/Class;Ljava/lang/String;Z)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0001\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u00022\u000e\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u00042\u000e\u0010\u0006\u001a\n \u0005*\u0004\u0018\u00010\u00070\u00072,\u0010\u0008\u001a(\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0014\u0012\u000e\u0008\u0001\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\t0\tH\n\u00a2\u0006\u0004\u0008\n\u0010\u000b"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "<anonymous parameter 0>",
        "",
        "kotlin.jvm.PlatformType",
        "method",
        "Ljava/lang/reflect/Method;",
        "args",
        "",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Void;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $klass:Ljava/lang/Class;

.field final synthetic $message:Ljava/lang/String;

.field final synthetic $proxyCreator:Lcom/squareup/util/ProxyCreatorTraceException;


# direct methods
.method constructor <init>(Ljava/lang/Class;Ljava/lang/String;Lcom/squareup/util/ProxyCreatorTraceException;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/Objects$allMethodsThrowUnsupportedOperation$2;->$klass:Ljava/lang/Class;

    iput-object p2, p0, Lcom/squareup/util/Objects$allMethodsThrowUnsupportedOperation$2;->$message:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/util/Objects$allMethodsThrowUnsupportedOperation$2;->$proxyCreator:Lcom/squareup/util/ProxyCreatorTraceException;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/util/Objects$allMethodsThrowUnsupportedOperation$2;->invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Void;
    .locals 1

    .line 175
    iget-object p1, p0, Lcom/squareup/util/Objects$allMethodsThrowUnsupportedOperation$2;->$klass:Ljava/lang/Class;

    const-string v0, "method"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p2, p3}, Lcom/squareup/util/Objects;->access$formatMethodDescription(Ljava/lang/Class;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 177
    iget-object p2, p0, Lcom/squareup/util/Objects$allMethodsThrowUnsupportedOperation$2;->$message:Ljava/lang/String;

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p3, p0, Lcom/squareup/util/Objects$allMethodsThrowUnsupportedOperation$2;->$message:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " \u2013 "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 178
    :goto_0
    new-instance p2, Ljava/lang/UnsupportedOperationException;

    iget-object p3, p0, Lcom/squareup/util/Objects$allMethodsThrowUnsupportedOperation$2;->$proxyCreator:Lcom/squareup/util/ProxyCreatorTraceException;

    check-cast p3, Ljava/lang/Throwable;

    invoke-direct {p2, p1, p3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method
