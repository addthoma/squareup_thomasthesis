.class public Lcom/squareup/util/InputInjector;
.super Ljava/lang/Object;
.source "InputInjector.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createKeyEvent(JII)Landroid/view/KeyEvent;
    .locals 14

    .line 39
    new-instance v13, Landroid/view/KeyEvent;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x101

    move-object v0, v13

    move-wide v1, p0

    move-wide v3, p0

    move/from16 v5, p2

    move/from16 v6, p3

    invoke-direct/range {v0 .. v12}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    return-object v13
.end method

.method private static injectEvent(Landroid/hardware/input/InputManager;Landroid/view/KeyEvent;)V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 45
    :try_start_0
    const-class v2, Landroid/hardware/input/InputManager;

    const-string v3, "injectInputEvent"

    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/Class;

    const-class v6, Landroid/view/InputEvent;

    aput-object v6, v5, v1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v5, v0

    .line 46
    invoke-virtual {v2, v3, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    aput-object p1, v3, v1

    .line 47
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    goto :goto_0

    :catch_1
    move-exception p0

    goto :goto_0

    :catch_2
    move-exception p0

    :goto_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 49
    invoke-virtual {p1}, Landroid/view/KeyEvent;->toString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "Unable to inject KeyEvent: %s"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method

.method public static sendKeyPress(Landroid/hardware/input/InputManager;I)V
    .locals 3

    .line 30
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    const/4 v2, 0x0

    .line 31
    invoke-static {v0, v1, v2, p1}, Lcom/squareup/util/InputInjector;->createKeyEvent(JII)Landroid/view/KeyEvent;

    move-result-object v2

    .line 32
    invoke-static {p0, v2}, Lcom/squareup/util/InputInjector;->injectEvent(Landroid/hardware/input/InputManager;Landroid/view/KeyEvent;)V

    const/4 v2, 0x1

    .line 34
    invoke-static {v0, v1, v2, p1}, Lcom/squareup/util/InputInjector;->createKeyEvent(JII)Landroid/view/KeyEvent;

    move-result-object p1

    .line 35
    invoke-static {p0, p1}, Lcom/squareup/util/InputInjector;->injectEvent(Landroid/hardware/input/InputManager;Landroid/view/KeyEvent;)V

    return-void
.end method
