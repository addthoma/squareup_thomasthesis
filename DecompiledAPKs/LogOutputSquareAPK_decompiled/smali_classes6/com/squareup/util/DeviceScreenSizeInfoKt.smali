.class public final Lcom/squareup/util/DeviceScreenSizeInfoKt;
.super Ljava/lang/Object;
.source "DeviceScreenSizeInfo.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDeviceScreenSizeInfo.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DeviceScreenSizeInfo.kt\ncom/squareup/util/DeviceScreenSizeInfoKt\n*L\n1#1,115:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0016\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r\u001a\u000e\u0010\u000e\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u0010\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u0015\u0010\u0003\u001a\u00020\u0004*\u00020\u00058F\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0011"
    }
    d2 = {
        "TOO_TALL",
        "",
        "TOO_WIDE",
        "deviceScreenSizeInfo",
        "Lcom/squareup/util/DeviceScreenSizeInfo;",
        "Landroid/content/Context;",
        "getDeviceScreenSizeInfo",
        "(Landroid/content/Context;)Lcom/squareup/util/DeviceScreenSizeInfo;",
        "isAndroidOWithBadAspectRatio",
        "",
        "sdkInt",
        "",
        "displayMetrics",
        "Landroid/util/DisplayMetrics;",
        "isWidescreen",
        "screenDimens",
        "Landroid/graphics/Point;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final TOO_TALL:F = 1.86f

.field private static final TOO_WIDE:F = 0.53763443f


# direct methods
.method public static final getDeviceScreenSizeInfo(Landroid/content/Context;)Lcom/squareup/util/DeviceScreenSizeInfo;
    .locals 8

    const-string v0, "$this$deviceScreenSizeInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 74
    new-instance v7, Lcom/squareup/util/DeviceScreenSizeInfo;

    .line 75
    sget v1, Lcom/squareup/utilities/ui/R$bool;->sq_is_portrait:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    .line 76
    sget v1, Lcom/squareup/utilities/ui/R$bool;->sq_is_tablet:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    .line 77
    sget v1, Lcom/squareup/utilities/ui/R$bool;->sq_is_tablet_10inch:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    .line 78
    invoke-static {p0}, Lcom/squareup/util/ScreenParameters;->getScreenDimens(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/util/DeviceScreenSizeInfoKt;->isWidescreen(Landroid/graphics/Point;)Z

    move-result v5

    .line 79
    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const-string v1, "displayMetrics"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/squareup/util/DeviceScreenSizeInfoKt;->isAndroidOWithBadAspectRatio(ILandroid/util/DisplayMetrics;)Z

    move-result v6

    move-object v1, v7

    .line 74
    invoke-direct/range {v1 .. v6}, Lcom/squareup/util/DeviceScreenSizeInfo;-><init>(ZZZZZ)V

    return-object v7
.end method

.method public static final isAndroidOWithBadAspectRatio(ILandroid/util/DisplayMetrics;)Z
    .locals 2

    const-string v0, "displayMetrics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/16 v1, 0x1a

    if-eq p0, v1, :cond_0

    return v0

    .line 112
    :cond_0
    iget p0, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float p0, p0

    iget p1, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float p1, p1

    div-float/2addr p0, p1

    const p1, 0x3fee147b    # 1.86f

    cmpl-float p1, p0, p1

    if-gez p1, :cond_1

    const p1, 0x3f09a269

    cmpg-float p0, p0, p1

    if-gtz p0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public static final isWidescreen(Landroid/graphics/Point;)Z
    .locals 4

    const-string v0, "screenDimens"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iget v0, p0, Landroid/graphics/Point;->x:I

    mul-int/lit8 v0, v0, 0x3

    iget v1, p0, Landroid/graphics/Point;->y:I

    mul-int/lit8 v1, v1, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 95
    :goto_0
    iget v1, p0, Landroid/graphics/Point;->y:I

    mul-int/lit8 v1, v1, 0x3

    iget p0, p0, Landroid/graphics/Point;->x:I

    mul-int/lit8 p0, p0, 0x4

    if-le v1, p0, :cond_1

    const/4 p0, 0x1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :goto_1
    if-nez v0, :cond_3

    if-eqz p0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :cond_3
    :goto_2
    return v2
.end method
