.class Lcom/squareup/util/RxFiles$RxFileObserver;
.super Ljava/lang/Object;
.source "RxFiles.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/RxFiles;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RxFileObserver"
.end annotation


# instance fields
.field private final file:Ljava/io/File;

.field private fileObserver:Landroid/os/FileObserver;


# direct methods
.method private constructor <init>(Ljava/io/File;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/util/RxFiles$RxFileObserver;->file:Ljava/io/File;

    return-void
.end method

.method synthetic constructor <init>(Ljava/io/File;Lcom/squareup/util/RxFiles$1;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/squareup/util/RxFiles$RxFileObserver;-><init>(Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/util/RxFiles$RxFileObserver;)Lrx/Observable;
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/squareup/util/RxFiles$RxFileObserver;->watchForAllEvents()Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/util/RxFiles$RxFileObserver;)Lrx/Observable;
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/squareup/util/RxFiles$RxFileObserver;->watchForModifyEvents()Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/util/RxFiles$RxFileObserver;)Ljava/io/File;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/util/RxFiles$RxFileObserver;->file:Ljava/io/File;

    return-object p0
.end method

.method private watchForAllEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 59
    new-instance v0, Lcom/squareup/util/-$$Lambda$RxFiles$RxFileObserver$M1fWJaF5GgSY2zIPJqJB03mNN2E;

    invoke-direct {v0, p0}, Lcom/squareup/util/-$$Lambda$RxFiles$RxFileObserver$M1fWJaF5GgSY2zIPJqJB03mNN2E;-><init>(Lcom/squareup/util/RxFiles$RxFileObserver;)V

    sget-object v1, Lrx/Emitter$BackpressureMode;->LATEST:Lrx/Emitter$BackpressureMode;

    invoke-static {v0, v1}, Lrx/Observable;->create(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lrx/Observable;->share()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method private watchForModifyEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 80
    new-instance v0, Lcom/squareup/util/-$$Lambda$RxFiles$RxFileObserver$FpYllCeenIshMhsBg_zd_m12u90;

    invoke-direct {v0, p0}, Lcom/squareup/util/-$$Lambda$RxFiles$RxFileObserver$FpYllCeenIshMhsBg_zd_m12u90;-><init>(Lcom/squareup/util/RxFiles$RxFileObserver;)V

    sget-object v1, Lrx/Emitter$BackpressureMode;->LATEST:Lrx/Emitter$BackpressureMode;

    invoke-static {v0, v1}, Lrx/Observable;->create(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lrx/Observable;->share()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public synthetic lambda$null$0$RxFiles$RxFileObserver()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/squareup/util/RxFiles$RxFileObserver;->fileObserver:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->stopWatching()V

    const/4 v0, 0x0

    .line 69
    iput-object v0, p0, Lcom/squareup/util/RxFiles$RxFileObserver;->fileObserver:Landroid/os/FileObserver;

    return-void
.end method

.method public synthetic lambda$null$2$RxFiles$RxFileObserver()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/squareup/util/RxFiles$RxFileObserver;->fileObserver:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->stopWatching()V

    const/4 v0, 0x0

    .line 90
    iput-object v0, p0, Lcom/squareup/util/RxFiles$RxFileObserver;->fileObserver:Landroid/os/FileObserver;

    return-void
.end method

.method public synthetic lambda$watchForAllEvents$1$RxFiles$RxFileObserver(Lrx/Emitter;)V
    .locals 2

    .line 61
    new-instance v0, Lcom/squareup/util/RxFiles$RxFileObserver$1;

    iget-object v1, p0, Lcom/squareup/util/RxFiles$RxFileObserver;->file:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/util/RxFiles$RxFileObserver$1;-><init>(Lcom/squareup/util/RxFiles$RxFileObserver;Ljava/lang/String;Lrx/Emitter;)V

    iput-object v0, p0, Lcom/squareup/util/RxFiles$RxFileObserver;->fileObserver:Landroid/os/FileObserver;

    .line 67
    new-instance v0, Lcom/squareup/util/-$$Lambda$RxFiles$RxFileObserver$rdT3ZZpbsgwQ2grR16fufNFkdys;

    invoke-direct {v0, p0}, Lcom/squareup/util/-$$Lambda$RxFiles$RxFileObserver$rdT3ZZpbsgwQ2grR16fufNFkdys;-><init>(Lcom/squareup/util/RxFiles$RxFileObserver;)V

    invoke-interface {p1, v0}, Lrx/Emitter;->setCancellation(Lrx/functions/Cancellable;)V

    .line 72
    iget-object p1, p0, Lcom/squareup/util/RxFiles$RxFileObserver;->fileObserver:Landroid/os/FileObserver;

    invoke-virtual {p1}, Landroid/os/FileObserver;->startWatching()V

    return-void
.end method

.method public synthetic lambda$watchForModifyEvents$3$RxFiles$RxFileObserver(Lrx/Emitter;)V
    .locals 3

    .line 82
    new-instance v0, Lcom/squareup/util/RxFiles$RxFileObserver$2;

    iget-object v1, p0, Lcom/squareup/util/RxFiles$RxFileObserver;->file:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/squareup/util/RxFiles$RxFileObserver$2;-><init>(Lcom/squareup/util/RxFiles$RxFileObserver;Ljava/lang/String;ILrx/Emitter;)V

    iput-object v0, p0, Lcom/squareup/util/RxFiles$RxFileObserver;->fileObserver:Landroid/os/FileObserver;

    .line 88
    new-instance v0, Lcom/squareup/util/-$$Lambda$RxFiles$RxFileObserver$rd0YnS15g6OR3hTKp4JjNjWCHSI;

    invoke-direct {v0, p0}, Lcom/squareup/util/-$$Lambda$RxFiles$RxFileObserver$rd0YnS15g6OR3hTKp4JjNjWCHSI;-><init>(Lcom/squareup/util/RxFiles$RxFileObserver;)V

    invoke-interface {p1, v0}, Lrx/Emitter;->setCancellation(Lrx/functions/Cancellable;)V

    .line 93
    iget-object p1, p0, Lcom/squareup/util/RxFiles$RxFileObserver;->fileObserver:Landroid/os/FileObserver;

    invoke-virtual {p1}, Landroid/os/FileObserver;->startWatching()V

    return-void
.end method
