.class Lcom/squareup/util/SharingTouchDelegate;
.super Landroid/view/TouchDelegate;
.source "SharingTouchDelegate.java"


# instance fields
.field private final bigView:Landroid/view/View;

.field private final delegateView:Landroid/view/View;

.field private final touchBoth:Z


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/view/View;Z)V
    .locals 4

    .line 25
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-direct {p0, v0, p2}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    .line 26
    iput-object p1, p0, Lcom/squareup/util/SharingTouchDelegate;->bigView:Landroid/view/View;

    .line 27
    iput-object p2, p0, Lcom/squareup/util/SharingTouchDelegate;->delegateView:Landroid/view/View;

    .line 28
    iput-boolean p3, p0, Lcom/squareup/util/SharingTouchDelegate;->touchBoth:Z

    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 34
    iget-object v1, p0, Lcom/squareup/util/SharingTouchDelegate;->bigView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v1, 0x0

    .line 35
    aget v2, v0, v1

    const/4 v3, 0x1

    aget v4, v0, v3

    .line 36
    iget-object v5, p0, Lcom/squareup/util/SharingTouchDelegate;->delegateView:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 37
    aget v5, v0, v1

    aget v0, v0, v3

    sub-int/2addr v2, v5

    int-to-float v2, v2

    sub-int/2addr v4, v0

    int-to-float v0, v4

    .line 38
    invoke-virtual {p1, v2, v0}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 40
    iget-object v0, p0, Lcom/squareup/util/SharingTouchDelegate;->delegateView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    .line 42
    iget-boolean v0, p0, Lcom/squareup/util/SharingTouchDelegate;->touchBoth:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method
