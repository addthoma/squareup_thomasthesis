.class final Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$1;
.super Lkotlin/jvm/internal/Lambda;
.source "PrefsBackedContentProvider.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->onCreate()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/util/prefsbackedcontentprovider/ContractItem<",
        "*>;",
        "Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\n\u0010\u0002\u001a\u0006\u0012\u0002\u0008\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "getHelper",
        "Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;",
        "contractItem",
        "Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $myContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;


# direct methods
.method constructor <init>(Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$1;->this$0:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;

    iput-object p2, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$1;->$myContext:Landroid/content/Context;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;)Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/prefsbackedcontentprovider/ContractItem<",
            "*>;)",
            "Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;"
        }
    .end annotation

    const-string v0, "contractItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-interface {p1}, Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;->getPref()Ljava/lang/String;

    move-result-object v4

    .line 85
    invoke-interface {p1}, Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;->getDefault()Ljava/lang/Object;

    move-result-object v5

    .line 87
    instance-of p1, v5, Ljava/lang/Boolean;

    if-eqz p1, :cond_0

    new-instance p1, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;

    iget-object v2, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$1;->$myContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$1;->this$0:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;

    invoke-static {v0}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->access$getFileName$p(Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;)Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/squareup/util/prefsbackedcontentprovider/BoolStrategy;->INSTANCE:Lcom/squareup/util/prefsbackedcontentprovider/BoolStrategy;

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;)V

    check-cast p1, Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;

    goto :goto_0

    .line 88
    :cond_0
    instance-of p1, v5, Ljava/lang/String;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;

    iget-object v2, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$1;->$myContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$1;->this$0:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;

    invoke-static {v0}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->access$getFileName$p(Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;)Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/squareup/util/prefsbackedcontentprovider/StringStrategy;->INSTANCE:Lcom/squareup/util/prefsbackedcontentprovider/StringStrategy;

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/util/prefsbackedcontentprovider/RealPrefsHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/util/prefsbackedcontentprovider/PrefsStrategy;)V

    check-cast p1, Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;

    :goto_0
    return-object p1

    .line 89
    :cond_1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "unsupported type"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 70
    check-cast p1, Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;

    invoke-virtual {p0, p1}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$1;->invoke(Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;)Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;

    move-result-object p1

    return-object p1
.end method
