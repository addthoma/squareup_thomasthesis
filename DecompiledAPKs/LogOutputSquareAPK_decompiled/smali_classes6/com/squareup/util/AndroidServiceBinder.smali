.class Lcom/squareup/util/AndroidServiceBinder;
.super Ljava/lang/Object;
.source "AndroidServiceBinder.java"

# interfaces
.implements Lcom/squareup/util/ServiceBinder;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/squareup/util/AndroidServiceBinder;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/util/AndroidServiceBinder;->context:Landroid/content/Context;

    invoke-virtual {v0, p1, p2, p3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result p1

    return p1
.end method

.method public unbindService(Landroid/content/ServiceConnection;)V
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/util/AndroidServiceBinder;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    return-void
.end method
