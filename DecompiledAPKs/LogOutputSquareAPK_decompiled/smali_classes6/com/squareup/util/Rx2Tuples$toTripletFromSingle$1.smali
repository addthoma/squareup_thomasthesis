.class final Lcom/squareup/util/Rx2Tuples$toTripletFromSingle$1;
.super Ljava/lang/Object;
.source "Rx2Tuples.kt"

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/Rx2Tuples;->toTripletFromSingle()Lio/reactivex/functions/BiFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/BiFunction<",
        "TC;",
        "Lkotlin/Pair<",
        "+TA;+TB;>;",
        "Lcom/squareup/util/tuple/Triplet<",
        "TC;TA;TB;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0002*\u00020\u00052\u0006\u0010\u0006\u001a\u0002H\u00022\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0008H\n\u00a2\u0006\u0004\u0008\t\u0010\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/tuple/Triplet;",
        "C",
        "A",
        "B",
        "",
        "t",
        "<name for destructuring parameter 1>",
        "Lkotlin/Pair;",
        "apply",
        "(Ljava/lang/Object;Lkotlin/Pair;)Lcom/squareup/util/tuple/Triplet;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/Rx2Tuples$toTripletFromSingle$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/util/Rx2Tuples$toTripletFromSingle$1;

    invoke-direct {v0}, Lcom/squareup/util/Rx2Tuples$toTripletFromSingle$1;-><init>()V

    sput-object v0, Lcom/squareup/util/Rx2Tuples$toTripletFromSingle$1;->INSTANCE:Lcom/squareup/util/Rx2Tuples$toTripletFromSingle$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;Lkotlin/Pair;)Lcom/squareup/util/tuple/Triplet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;",
            "Lkotlin/Pair<",
            "+TA;+TB;>;)",
            "Lcom/squareup/util/tuple/Triplet<",
            "TC;TA;TB;>;"
        }
    .end annotation

    const-string v0, "t"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<name for destructuring parameter 1>"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p2

    .line 73
    new-instance v1, Lcom/squareup/util/tuple/Triplet;

    invoke-direct {v1, p1, v0, p2}, Lcom/squareup/util/tuple/Triplet;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p2, Lkotlin/Pair;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/util/Rx2Tuples$toTripletFromSingle$1;->apply(Ljava/lang/Object;Lkotlin/Pair;)Lcom/squareup/util/tuple/Triplet;

    move-result-object p1

    return-object p1
.end method
