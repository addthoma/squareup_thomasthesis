.class public final Lcom/squareup/util/AndroidModule_ProvideUsbManagerFactory;
.super Ljava/lang/Object;
.source "AndroidModule_ProvideUsbManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/hardware/usb/UsbManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/util/AndroidModule_ProvideUsbManagerFactory;->contextProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/util/AndroidModule_ProvideUsbManagerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/util/AndroidModule_ProvideUsbManagerFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/util/AndroidModule_ProvideUsbManagerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/util/AndroidModule_ProvideUsbManagerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideUsbManager(Landroid/app/Application;)Lcom/squareup/hardware/usb/UsbManager;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/util/AndroidModule;->provideUsbManager(Landroid/app/Application;)Lcom/squareup/hardware/usb/UsbManager;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/hardware/usb/UsbManager;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/hardware/usb/UsbManager;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/util/AndroidModule_ProvideUsbManagerFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/util/AndroidModule_ProvideUsbManagerFactory;->provideUsbManager(Landroid/app/Application;)Lcom/squareup/hardware/usb/UsbManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/util/AndroidModule_ProvideUsbManagerFactory;->get()Lcom/squareup/hardware/usb/UsbManager;

    move-result-object v0

    return-object v0
.end method
