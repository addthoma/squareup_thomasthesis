.class final Lcom/squareup/util/RxViews$debouncedOnEditorAction$1;
.super Ljava/lang/Object;
.source "RxViews.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/RxViews;->debouncedOnEditorAction(Landroid/widget/TextView;I)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lrx/Emitter<",
        "TT;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxViews.kt\ncom/squareup/util/RxViews$debouncedOnEditorAction$1\n*L\n1#1,258:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00010\u0001 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00010\u0001\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lrx/Emitter;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $imeMaskAction:I

.field final synthetic $this_debouncedOnEditorAction:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/RxViews$debouncedOnEditorAction$1;->$this_debouncedOnEditorAction:Landroid/widget/TextView;

    iput p2, p0, Lcom/squareup/util/RxViews$debouncedOnEditorAction$1;->$imeMaskAction:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lrx/Emitter;

    invoke-virtual {p0, p1}, Lcom/squareup/util/RxViews$debouncedOnEditorAction$1;->call(Lrx/Emitter;)V

    return-void
.end method

.method public final call(Lrx/Emitter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Emitter<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 82
    iget v0, p0, Lcom/squareup/util/RxViews$debouncedOnEditorAction$1;->$imeMaskAction:I

    const/16 v1, 0xff

    if-ge v0, v1, :cond_0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/squareup/util/RxViews$debouncedOnEditorAction$1;->$this_debouncedOnEditorAction:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/util/RxViews$debouncedOnEditorAction$1$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/util/RxViews$debouncedOnEditorAction$1$2;-><init>(Lcom/squareup/util/RxViews$debouncedOnEditorAction$1;Lrx/Emitter;)V

    check-cast v1, Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    const-string v0, "emitter"

    .line 102
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/util/RxViews$debouncedOnEditorAction$1$3;

    invoke-direct {v0, p0}, Lcom/squareup/util/RxViews$debouncedOnEditorAction$1$3;-><init>(Lcom/squareup/util/RxViews$debouncedOnEditorAction$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnMainThread(Lrx/Emitter;Lkotlin/jvm/functions/Function0;)V

    return-void

    .line 80
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Invalid IME_MASK_ACTION selected"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
