.class public final Lcom/squareup/util/kotlin/interop/UnitFunctions;
.super Ljava/lang/Object;
.source "UnitFunctions.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a8\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u00020\u00040\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u00032\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0006\u001a&\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0008\u0012\u0004\u0012\u00020\u00040\u0007\"\u0004\u0008\u0000\u0010\u00082\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\t\u001a\u0014\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00040\n2\u0006\u0010\u0005\u001a\u00020\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "kotlinRunnable",
        "Lkotlin/Function2;",
        "T1",
        "T2",
        "",
        "runnable",
        "Lcom/squareup/util/kotlin/interop/KotlinRunnable2;",
        "Lkotlin/Function1;",
        "T",
        "Lcom/squareup/util/kotlin/interop/KotlinRunnable;",
        "Lkotlin/Function0;",
        "Ljava/lang/Runnable;",
        "public"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final kotlinRunnable(Ljava/lang/Runnable;)Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            ")",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "runnable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lcom/squareup/util/kotlin/interop/UnitFunctions$kotlinRunnable$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/kotlin/interop/UnitFunctions$kotlinRunnable$1;-><init>(Ljava/lang/Runnable;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public static final kotlinRunnable(Lcom/squareup/util/kotlin/interop/KotlinRunnable;)Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/util/kotlin/interop/KotlinRunnable<",
            "TT;>;)",
            "Lkotlin/jvm/functions/Function1<",
            "TT;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "runnable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    new-instance v0, Lcom/squareup/util/kotlin/interop/UnitFunctions$kotlinRunnable$2;

    invoke-direct {v0, p0}, Lcom/squareup/util/kotlin/interop/UnitFunctions$kotlinRunnable$2;-><init>(Lcom/squareup/util/kotlin/interop/KotlinRunnable;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public static final kotlinRunnable(Lcom/squareup/util/kotlin/interop/KotlinRunnable2;)Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/util/kotlin/interop/KotlinRunnable2<",
            "TT1;TT2;>;)",
            "Lkotlin/jvm/functions/Function2<",
            "TT1;TT2;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "runnable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lcom/squareup/util/kotlin/interop/UnitFunctions$kotlinRunnable$3;

    invoke-direct {v0, p0}, Lcom/squareup/util/kotlin/interop/UnitFunctions$kotlinRunnable$3;-><init>(Lcom/squareup/util/kotlin/interop/KotlinRunnable2;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    return-object v0
.end method
