.class public abstract Lcom/squareup/util/RunnableOnce;
.super Ljava/lang/Object;
.source "RunnableOnce.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final ranOrCanceled:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/squareup/util/RunnableOnce;->ranOrCanceled:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method


# virtual methods
.method public final cancel()V
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/util/RunnableOnce;->ranOrCanceled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/squareup/util/RunnableOnce;->onCancel()V

    :cond_0
    return-void
.end method

.method protected onCancel()V
    .locals 0

    return-void
.end method

.method public final run()V
    .locals 3

    .line 20
    iget-object v0, p0, Lcom/squareup/util/RunnableOnce;->ranOrCanceled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    invoke-virtual {p0}, Lcom/squareup/util/RunnableOnce;->runOnce()V

    :cond_0
    return-void
.end method

.method protected abstract runOnce()V
.end method
