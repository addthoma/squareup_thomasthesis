.class public final Lcom/squareup/util/BigDecimals;
.super Ljava/lang/Object;
.source "BigDecimals.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBigDecimals.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BigDecimals.kt\ncom/squareup/util/BigDecimals\n+ 2 Strings.kt\nkotlin/text/StringsKt__StringsKt\n*L\n1#1,161:1\n79#2:162\n68#2,5:163\n79#2:168\n68#2,5:169\n*E\n*S KotlinDebug\n*F\n+ 1 BigDecimals.kt\ncom/squareup/util/BigDecimals\n*L\n49#1:162\n49#1,5:163\n50#1:168\n50#1,5:169\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u000b\u001a\u0018\u0010\u0007\u001a\u0004\u0018\u00010\u0003*\u0004\u0018\u00010\u00032\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u001a\n\u0010\t\u001a\u00020\u0001*\u00020\u0003\u001a\u0016\u0010\n\u001a\u00020\u0001*\u0004\u0018\u00010\u00032\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u001a\n\u0010\u000b\u001a\u00020\u0003*\u00020\u0003\u001a\n\u0010\u000c\u001a\u00020\r*\u00020\u0003\u001a\u0012\u0010\u000e\u001a\u00020\u000f*\u00020\u00032\u0006\u0010\u0008\u001a\u00020\u0010\u001a\u0012\u0010\u0011\u001a\u00020\u000f*\u00020\u00032\u0006\u0010\u0008\u001a\u00020\u0003\u001a\u0012\u0010\u0011\u001a\u00020\u000f*\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0013\u001a\u0012\u0010\u0014\u001a\u00020\u000f*\u00020\u00032\u0006\u0010\u0008\u001a\u00020\u0003\u001a\u0012\u0010\u0014\u001a\u00020\u000f*\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0013\u001a\n\u0010\u0015\u001a\u00020\u0001*\u00020\u0003\u001a\n\u0010\u0016\u001a\u00020\u000f*\u00020\u0003\u001a\n\u0010\u0017\u001a\u00020\u000f*\u00020\u0003\u001a\u0012\u0010\u0018\u001a\u00020\u000f*\u00020\u00032\u0006\u0010\u0008\u001a\u00020\u0003\u001a\u0012\u0010\u0019\u001a\u00020\u000f*\u00020\u00032\u0006\u0010\u0008\u001a\u00020\u0003\u001a\u0012\u0010\u0019\u001a\u00020\u000f*\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0013\u001a\n\u0010\u001a\u001a\u00020\u0003*\u00020\u0003\u001a\u001a\u0010\u001b\u001a\u00020\u0003*\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0001H\u0087\u0002\u00a2\u0006\u0002\u0008\u0007\u001a\u001a\u0010\u001c\u001a\u00020\u0003*\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0001H\u0087\u0002\u00a2\u0006\u0002\u0008\u001d\u001a\u001a\u0010\u001c\u001a\u00020\u0003*\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0013H\u0087\u0002\u00a2\u0006\u0002\u0008\u001d\"\u0010\u0010\u0000\u001a\u00020\u00018\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\u0005\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\u0006\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "DEFAULT_ROUNDING_SCALE",
        "",
        "FIVE",
        "Ljava/math/BigDecimal;",
        "FOUR",
        "THREE",
        "TWO",
        "add",
        "other",
        "asIntOr1",
        "compareTo",
        "correctedStripTrailingZeros",
        "decimalPart",
        "",
        "equalsIgnoringScale",
        "",
        "",
        "greaterThan",
        "a",
        "",
        "greaterThanOrEqualTo",
        "hashCodeIgnoringScale",
        "isIntegral",
        "isZero",
        "lessThan",
        "lessThanOrEqualTo",
        "normalizeScale",
        "plus",
        "times",
        "multiply",
        "public"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final DEFAULT_ROUNDING_SCALE:I

.field public static final FIVE:Ljava/math/BigDecimal;

.field public static final FOUR:Ljava/math/BigDecimal;

.field public static final THREE:Ljava/math/BigDecimal;

.field public static final TWO:Ljava/math/BigDecimal;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 13
    sget-object v0, Ljava/math/MathContext;->DECIMAL128:Ljava/math/MathContext;

    const-string v1, "MathContext.DECIMAL128"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/math/MathContext;->getPrecision()I

    move-result v0

    sput v0, Lcom/squareup/util/BigDecimals;->DEFAULT_ROUNDING_SCALE:I

    .line 16
    new-instance v0, Ljava/math/BigDecimal;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    sput-object v0, Lcom/squareup/util/BigDecimals;->TWO:Ljava/math/BigDecimal;

    .line 17
    new-instance v0, Ljava/math/BigDecimal;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    sput-object v0, Lcom/squareup/util/BigDecimals;->THREE:Ljava/math/BigDecimal;

    .line 18
    new-instance v0, Ljava/math/BigDecimal;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    sput-object v0, Lcom/squareup/util/BigDecimals;->FOUR:Ljava/math/BigDecimal;

    .line 19
    new-instance v0, Ljava/math/BigDecimal;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    sput-object v0, Lcom/squareup/util/BigDecimals;->FIVE:Ljava/math/BigDecimal;

    return-void
.end method

.method public static final add(Ljava/math/BigDecimal;I)Ljava/math/BigDecimal;
    .locals 1

    const-string v0, "$this$plus"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {p0, v0}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p0

    const-string p1, "this.add(other)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final add(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 0

    if-eqz p0, :cond_0

    goto :goto_0

    .line 145
    :cond_0
    sget-object p0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    .line 146
    :cond_1
    sget-object p1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 147
    :goto_1
    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p0

    return-object p0
.end method

.method public static final asIntOr1(Ljava/math/BigDecimal;)I
    .locals 1

    const-string v0, "$this$asIntOr1"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-static {p0}, Lcom/squareup/util/BigDecimals;->isIntegral(Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/math/BigDecimal;->intValueExact()I

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x1

    :goto_0
    return p0
.end method

.method public static final compareTo(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)I
    .locals 0

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    if-nez p0, :cond_1

    const/4 p0, -0x1

    return p0

    :cond_1
    if-nez p1, :cond_2

    const/4 p0, 0x1

    return p0

    .line 159
    :cond_2
    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p0

    return p0
.end method

.method public static final correctedStripTrailingZeros(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 1

    const-string v0, "$this$correctedStripTrailingZeros"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p0, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_0

    .line 120
    sget-object p0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v0, "ZERO"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    .line 122
    :cond_0
    invoke-virtual {p0}, Ljava/math/BigDecimal;->stripTrailingZeros()Ljava/math/BigDecimal;

    move-result-object p0

    const-string v0, "stripTrailingZeros()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final decimalPart(Ljava/math/BigDecimal;)Ljava/lang/String;
    .locals 5

    const-string v0, "$this$decimalPart"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-virtual {p0}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "plainString"

    .line 135
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x2

    const/16 v2, 0x2e

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v2, v4, v1, v3}, Lkotlin/text/StringsKt;->contains$default(Ljava/lang/CharSequence;CZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    invoke-static {p0, v2, v3, v1, v3}, Lkotlin/text/StringsKt;->substringAfter$default(Ljava/lang/String;CLjava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const-string p0, ""

    return-object p0
.end method

.method public static final equalsIgnoringScale(Ljava/math/BigDecimal;Ljava/lang/Object;)Z
    .locals 2

    const-string v0, "$this$equalsIgnoringScale"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    instance-of v0, p1, Ljava/math/BigDecimal;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    .line 90
    :cond_0
    check-cast p1, Ljava/math/BigDecimal;

    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p0

    if-nez p0, :cond_1

    const/4 v1, 0x1

    :cond_1
    :goto_0
    return v1
.end method

.method public static final greaterThan(Ljava/math/BigDecimal;J)Z
    .locals 1

    const-string v0, "$this$greaterThan"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p0

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final greaterThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z
    .locals 1

    const-string v0, "$this$greaterThan"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p0

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final greaterThanOrEqualTo(Ljava/math/BigDecimal;J)Z
    .locals 1

    const-string v0, "$this$greaterThanOrEqualTo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p0

    if-ltz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final greaterThanOrEqualTo(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z
    .locals 1

    const-string v0, "$this$greaterThanOrEqualTo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p0

    if-ltz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final hashCodeIgnoringScale(Ljava/math/BigDecimal;)I
    .locals 1

    const-string v0, "$this$hashCodeIgnoringScale"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    invoke-static {p0}, Lcom/squareup/util/BigDecimals;->normalizeScale(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p0

    invoke-virtual {p0}, Ljava/math/BigDecimal;->hashCode()I

    move-result p0

    return p0
.end method

.method public static final isIntegral(Ljava/math/BigDecimal;)Z
    .locals 1

    const-string v0, "$this$isIntegral"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    invoke-static {p0}, Lcom/squareup/util/BigDecimals;->correctedStripTrailingZeros(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p0

    invoke-virtual {p0}, Ljava/math/BigDecimal;->scale()I

    move-result p0

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isZero(Ljava/math/BigDecimal;)Z
    .locals 2

    const-string v0, "$this$isZero"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v1, "ZERO"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/squareup/util/BigDecimals;->equalsIgnoringScale(Ljava/math/BigDecimal;Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static final lessThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z
    .locals 1

    const-string v0, "$this$lessThan"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p0

    if-gez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final lessThanOrEqualTo(Ljava/math/BigDecimal;J)Z
    .locals 1

    const-string v0, "$this$lessThanOrEqualTo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p0

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final lessThanOrEqualTo(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z
    .locals 1

    const-string v0, "$this$lessThanOrEqualTo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-virtual {p0, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p0

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final multiply(Ljava/math/BigDecimal;I)Ljava/math/BigDecimal;
    .locals 1

    const-string v0, "$this$times"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {p0, v0}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p0

    const-string p1, "this.multiply(other)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final multiply(Ljava/math/BigDecimal;J)Ljava/math/BigDecimal;
    .locals 1

    const-string v0, "$this$times"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1, p2}, Ljava/math/BigDecimal;-><init>(J)V

    invoke-virtual {p0, v0}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p0

    const-string p1, "this.multiply(other)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final normalizeScale(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 6

    const-string v0, "$this$normalizeScale"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "asString"

    .line 47
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    check-cast v0, Ljava/lang/CharSequence;

    const/16 v1, 0x2e

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lkotlin/text/StringsKt;->contains$default(Ljava/lang/CharSequence;CZILjava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 163
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result p0

    :cond_0
    add-int/lit8 p0, p0, -0x1

    const-string v3, ""

    if-ltz p0, :cond_1

    .line 164
    invoke-interface {v0, p0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    .line 49
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    const/16 v5, 0x30

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    add-int/lit8 p0, p0, 0x1

    .line 165
    invoke-interface {v0, v2, p0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0

    .line 167
    :cond_1
    move-object p0, v3

    check-cast p0, Ljava/lang/CharSequence;

    .line 162
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    .line 168
    check-cast p0, Ljava/lang/CharSequence;

    .line 169
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    :goto_1
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_3

    .line 170
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    .line 50
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    add-int/lit8 v0, v0, 0x1

    .line 171
    invoke-interface {p0, v2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_2

    :cond_2
    goto :goto_1

    .line 173
    :cond_3
    move-object p0, v3

    check-cast p0, Ljava/lang/CharSequence;

    .line 168
    :goto_2
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    .line 51
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 53
    :cond_4
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
