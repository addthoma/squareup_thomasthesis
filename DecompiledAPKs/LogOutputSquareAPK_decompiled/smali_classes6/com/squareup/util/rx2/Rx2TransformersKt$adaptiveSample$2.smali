.class final Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;
.super Ljava/lang/Object;
.source "Rx2Transformers.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2TransformersKt;->adaptiveSample(Lio/reactivex/Observable;JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Lio/reactivex/Observable<",
        "TT;>;",
        "Lio/reactivex/ObservableSource<",
        "TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u0001H\u0002H\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u0001H\u0002H\u0002\u0018\u00010\u00010\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00042\u0014\u0010\u0005\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u0001H\u0002H\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "T",
        "kotlin.jvm.PlatformType",
        "",
        "shared",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $scheduler:Lio/reactivex/Scheduler;

.field final synthetic $time:J

.field final synthetic $unit:Ljava/util/concurrent/TimeUnit;


# direct methods
.method constructor <init>(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)V
    .locals 0

    iput-wide p1, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;->$time:J

    iput-object p3, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;->$unit:Ljava/util/concurrent/TimeUnit;

    iput-object p4, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;->$scheduler:Lio/reactivex/Scheduler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "TT;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "shared"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    iget-wide v0, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;->$time:J

    iget-object v2, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;->$unit:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;->$scheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0, v1, v2, v3}, Lio/reactivex/Observable;->debounce(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 94
    sget-object v1, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2$1;->INSTANCE:Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 95
    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 100
    new-instance v1, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2$2;-><init>(Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;Lio/reactivex/Observable;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapDelayError(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lio/reactivex/Observable;

    invoke-virtual {p0, p1}, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;->apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
