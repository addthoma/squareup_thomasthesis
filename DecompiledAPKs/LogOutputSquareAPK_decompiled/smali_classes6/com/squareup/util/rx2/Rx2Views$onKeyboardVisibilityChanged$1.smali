.class final Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;
.super Ljava/lang/Object;
.source "Rx2Views.kt"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2Views;->onKeyboardVisibilityChanged(Landroid/view/View;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_onKeyboardVisibilityChanged:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;->$this_onKeyboardVisibilityChanged:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    new-instance v0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$listener$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$listener$1;-><init>(Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;Lio/reactivex/ObservableEmitter;)V

    .line 113
    iget-object v1, p0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;->$this_onKeyboardVisibilityChanged:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    move-object v2, v0

    check-cast v2, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 114
    new-instance v1, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$1;-><init>(Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$listener$1;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnMainThread(Lio/reactivex/ObservableEmitter;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
