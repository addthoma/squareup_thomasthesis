.class public final Lcom/squareup/util/rx2/Rx2BlockingSupport;
.super Ljava/lang/Object;
.source "Rx2BlockingSupport.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u001a)\u0010\u0004\u001a\u0002H\u0005\"\u0004\u0008\u0000\u0010\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u00012\u0006\u0010\u0007\u001a\u0002H\u0005H\u0007\u00a2\u0006\u0002\u0010\u0008\u001a-\u0010\t\u001a\u0002H\u0005\"\u0004\u0008\u0000\u0010\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u00012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u000b\u00a2\u0006\u0002\u0010\u000c\u001a!\u0010\r\u001a\u0002H\u0005\"\u0004\u0008\u0000\u0010\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0001H\u0007\u00a2\u0006\u0002\u0010\u000e\"\u001c\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "NO_ITEM_AVAILABLE_OBSERVABLE",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/util/rx2/NoItemAvailable;",
        "kotlin.jvm.PlatformType",
        "getValueOrDefault",
        "T",
        "source",
        "defaultValue",
        "(Lio/reactivex/Observable;Ljava/lang/Object;)Ljava/lang/Object;",
        "getValueOrProvideDefault",
        "defaultValueProvider",
        "Lkotlin/Function0;",
        "(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;",
        "getValueOrThrow",
        "(Lio/reactivex/Observable;)Ljava/lang/Object;",
        "public"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final NO_ITEM_AVAILABLE_OBSERVABLE:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/rx2/NoItemAvailable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 14
    sget-object v0, Lcom/squareup/util/rx2/NoItemAvailable;->INSTANCE:Lcom/squareup/util/rx2/NoItemAvailable;

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.just(NoItemAvailable)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/util/rx2/Rx2BlockingSupport;->NO_ITEM_AVAILABLE_OBSERVABLE:Lio/reactivex/Observable;

    return-void
.end method

.method public static final getValueOrDefault(Lio/reactivex/Observable;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;TT;)TT;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Write properly reactive code instead."
    .end annotation

    const-string v0, "source"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    new-instance v0, Lcom/squareup/util/rx2/Rx2BlockingSupport$getValueOrDefault$1;

    invoke-direct {v0, p1}, Lcom/squareup/util/rx2/Rx2BlockingSupport$getValueOrDefault$1;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrProvideDefault(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static final getValueOrProvideDefault(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;",
            "Lkotlin/jvm/functions/Function0<",
            "+TT;>;)TT;"
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValueProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    sget-object v0, Lcom/squareup/util/rx2/Rx2BlockingSupport;->NO_ITEM_AVAILABLE_OBSERVABLE:Lio/reactivex/Observable;

    if-eqz v0, :cond_0

    check-cast v0, Lio/reactivex/ObservableSource;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->ambWith(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object p0

    .line 67
    new-instance v0, Lcom/squareup/util/rx2/Rx2BlockingSupport$getValueOrProvideDefault$1;

    invoke-direct {v0, p1}, Lcom/squareup/util/rx2/Rx2BlockingSupport$getValueOrProvideDefault$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    .line 68
    invoke-virtual {p0}, Lio/reactivex/Observable;->blockingFirst()Ljava/lang/Object;

    move-result-object p0

    return-object p0

    .line 66
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type io.reactivex.Observable<T>"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final getValueOrThrow(Lio/reactivex/Observable;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;)TT;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Write properly reactive code instead."
    .end annotation

    const-string v0, "source"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    sget-object v0, Lcom/squareup/util/rx2/Rx2BlockingSupport$getValueOrThrow$1;->INSTANCE:Lcom/squareup/util/rx2/Rx2BlockingSupport$getValueOrThrow$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrProvideDefault(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method
