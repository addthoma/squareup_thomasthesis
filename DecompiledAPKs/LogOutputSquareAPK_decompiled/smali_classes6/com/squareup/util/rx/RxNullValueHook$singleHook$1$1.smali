.class final Lcom/squareup/util/rx/RxNullValueHook$singleHook$1$1;
.super Ljava/lang/Object;
.source "RxNullValueHook.kt"

# interfaces
.implements Lrx/Single$OnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx/RxNullValueHook$singleHook$1;->call(Lrx/Single;Lrx/Single$OnSubscribe;)Lrx/Single$OnSubscribe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Single$OnSubscribe<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "subscriber",
        "Lrx/SingleSubscriber;",
        "",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $onSubscribe:Lrx/Single$OnSubscribe;


# direct methods
.method constructor <init>(Lrx/Single$OnSubscribe;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1$1;->$onSubscribe:Lrx/Single$OnSubscribe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 80
    check-cast p1, Lrx/SingleSubscriber;

    invoke-virtual {p0, p1}, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1$1;->call(Lrx/SingleSubscriber;)V

    return-void
.end method

.method public final call(Lrx/SingleSubscriber;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/SingleSubscriber<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "subscriber"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    new-instance v0, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1$1$proxy$1;

    invoke-direct {v0, p1}, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1$1$proxy$1;-><init>(Lrx/SingleSubscriber;)V

    .line 104
    iget-object p1, p0, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1$1;->$onSubscribe:Lrx/Single$OnSubscribe;

    invoke-interface {p1, v0}, Lrx/Single$OnSubscribe;->call(Ljava/lang/Object;)V

    return-void
.end method
