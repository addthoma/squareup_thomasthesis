.class final Lcom/squareup/util/rx/RxNullValueHook$singleHook$1;
.super Ljava/lang/Object;
.source "RxNullValueHook.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/rx/RxNullValueHook;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "Lrx/Single<",
        "*>;",
        "Lrx/Single$OnSubscribe<",
        "*>;",
        "Lrx/Single$OnSubscribe<",
        "*>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0016\u0010\u0003\u001a\u0012\u0012\u0002\u0008\u0003 \u0005*\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00040\u00042\u0016\u0010\u0006\u001a\u0012\u0012\u0002\u0008\u0003 \u0005*\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Single$OnSubscribe;",
        "",
        "<anonymous parameter 0>",
        "Lrx/Single;",
        "kotlin.jvm.PlatformType",
        "onSubscribe",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/rx/RxNullValueHook$singleHook$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1;

    invoke-direct {v0}, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1;-><init>()V

    sput-object v0, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1;->INSTANCE:Lcom/squareup/util/rx/RxNullValueHook$singleHook$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lrx/Single;

    check-cast p2, Lrx/Single$OnSubscribe;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1;->call(Lrx/Single;Lrx/Single$OnSubscribe;)Lrx/Single$OnSubscribe;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lrx/Single;Lrx/Single$OnSubscribe;)Lrx/Single$OnSubscribe;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Single<",
            "*>;",
            "Lrx/Single$OnSubscribe<",
            "*>;)",
            "Lrx/Single$OnSubscribe<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 80
    new-instance p1, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1$1;

    invoke-direct {p1, p2}, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1$1;-><init>(Lrx/Single$OnSubscribe;)V

    check-cast p1, Lrx/Single$OnSubscribe;

    return-object p1
.end method
