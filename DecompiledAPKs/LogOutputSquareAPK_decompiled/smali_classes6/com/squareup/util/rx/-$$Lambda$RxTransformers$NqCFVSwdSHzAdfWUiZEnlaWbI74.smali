.class public final synthetic Lcom/squareup/util/rx/-$$Lambda$RxTransformers$NqCFVSwdSHzAdfWUiZEnlaWbI74;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/Observable$Transformer;


# instance fields
.field private final synthetic f$0:J

.field private final synthetic f$1:Ljava/util/concurrent/TimeUnit;

.field private final synthetic f$2:Lrx/Scheduler;


# direct methods
.method public synthetic constructor <init>(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$NqCFVSwdSHzAdfWUiZEnlaWbI74;->f$0:J

    iput-object p3, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$NqCFVSwdSHzAdfWUiZEnlaWbI74;->f$1:Ljava/util/concurrent/TimeUnit;

    iput-object p4, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$NqCFVSwdSHzAdfWUiZEnlaWbI74;->f$2:Lrx/Scheduler;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    iget-wide v0, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$NqCFVSwdSHzAdfWUiZEnlaWbI74;->f$0:J

    iget-object v2, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$NqCFVSwdSHzAdfWUiZEnlaWbI74;->f$1:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$NqCFVSwdSHzAdfWUiZEnlaWbI74;->f$2:Lrx/Scheduler;

    check-cast p1, Lrx/Observable;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/squareup/util/rx/RxTransformers;->lambda$adaptiveSample$3(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
