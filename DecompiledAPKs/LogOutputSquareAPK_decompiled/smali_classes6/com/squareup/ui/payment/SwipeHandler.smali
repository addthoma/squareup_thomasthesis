.class public Lcom/squareup/ui/payment/SwipeHandler;
.super Ljava/lang/Object;
.source "SwipeHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;
    }
.end annotation


# instance fields
.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field private final application:Landroid/app/Application;

.field private final badKeyboardHiderIsBad:Lcom/squareup/ui/main/BadKeyboardHider;

.field private final defaultSwipeHandler:Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;

.field private final flow:Lflow/Flow;

.field private final giftCards:Lcom/squareup/giftcard/GiftCards;

.field private final glassConfirmController:Lcom/squareup/widgets/glass/GlassConfirmController;

.field private final isTablet:Z

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

.field private final paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

.field private final paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private swipeHandler:Lcom/squareup/ui/seller/SwipedCardHandler;

.field private final swipeValidator:Lcom/squareup/swipe/SwipeValidator;

.field private final tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final workflow:Lcom/squareup/tenderpayment/CardSellerWorkflow;


# direct methods
.method constructor <init>(Landroid/app/Application;Lcom/squareup/onboarding/OnboardingDiverter;Lflow/Flow;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/payment/Transaction;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/BadKeyboardHider;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/util/Device;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/tenderpayment/CardSellerWorkflow;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/ui/tender/TenderStarter;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v1, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;-><init>(Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/ui/payment/SwipeHandler$1;)V

    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->defaultSwipeHandler:Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;

    .line 72
    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->defaultSwipeHandler:Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;

    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->swipeHandler:Lcom/squareup/ui/seller/SwipedCardHandler;

    move-object v1, p1

    .line 84
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->application:Landroid/app/Application;

    move-object v1, p2

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    move-object v1, p3

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->flow:Lflow/Flow;

    move-object v1, p4

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    move-object v1, p5

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p7

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

    move-object v1, p6

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    move-object v1, p8

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p9

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->badKeyboardHiderIsBad:Lcom/squareup/ui/main/BadKeyboardHider;

    move-object v1, p10

    .line 93
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    move-object/from16 v1, p18

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    .line 95
    invoke-interface {p11}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->isTablet:Z

    move-object v1, p12

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->giftCards:Lcom/squareup/giftcard/GiftCards;

    move-object/from16 v1, p13

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->glassConfirmController:Lcom/squareup/widgets/glass/GlassConfirmController;

    move-object/from16 v1, p14

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    move-object/from16 v1, p15

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    move-object/from16 v1, p16

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    move-object/from16 v1, p17

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->workflow:Lcom/squareup/tenderpayment/CardSellerWorkflow;

    move-object/from16 v1, p19

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    move-object/from16 v1, p20

    .line 103
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    move-object/from16 v1, p21

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->swipeValidator:Lcom/squareup/swipe/SwipeValidator;

    move-object/from16 v1, p22

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    return-void
.end method

.method private abortGlassIfPresent()V
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->glassConfirmController:Lcom/squareup/widgets/glass/GlassConfirmController;

    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmController;->maybeCancelAndRemoveGlass()Z

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/ui/seller/SwipedCardHandler;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/payment/SwipeHandler;->swipeHandler:Lcom/squareup/ui/seller/SwipedCardHandler;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/tickets/OpenTicketsSettings;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/payment/SwipeHandler;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    return-object p0
.end method

.method static synthetic access$102(Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/ui/seller/SwipedCardHandler;)Lcom/squareup/ui/seller/SwipedCardHandler;
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler;->swipeHandler:Lcom/squareup/ui/seller/SwipedCardHandler;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/payment/SwipeHandler;->defaultSwipeHandler:Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/payment/SwipeHandler;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/swipe/SwipeValidator;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/payment/SwipeHandler;->swipeValidator:Lcom/squareup/swipe/SwipeValidator;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/cardreader/PaymentCounter;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/payment/SwipeHandler;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/ui/main/BadKeyboardHider;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/payment/SwipeHandler;->badKeyboardHiderIsBad:Lcom/squareup/ui/main/BadKeyboardHider;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/Card;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/squareup/ui/payment/SwipeHandler;->toastPaymentOutOfRange(Lcom/squareup/Card;)V

    return-void
.end method

.method static synthetic access$800(Lcom/squareup/ui/payment/SwipeHandler;)Z
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/squareup/ui/payment/SwipeHandler;->swipeEnabled()Z

    move-result p0

    return p0
.end method

.method static synthetic access$900(Lcom/squareup/ui/payment/SwipeHandler;)Z
    .locals 0

    .line 46
    iget-boolean p0, p0, Lcom/squareup/ui/payment/SwipeHandler;->isTablet:Z

    return p0
.end method

.method private canAuthorize()Z
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 246
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasTenderInFlight()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private canSwipe()Z
    .locals 1

    .line 238
    invoke-direct {p0}, Lcom/squareup/ui/payment/SwipeHandler;->swipeEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 241
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/payment/SwipeHandler;->canAuthorize()Z

    move-result v0

    return v0
.end method

.method private processSwipedCard(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/Card;)V
    .locals 1

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->swipeHandler:Lcom/squareup/ui/seller/SwipedCardHandler;

    invoke-interface {v0, p2}, Lcom/squareup/ui/seller/SwipedCardHandler;->ignoreSwipeFor(Lcom/squareup/Card;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 225
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/Card;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    .line 226
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler;->paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

    invoke-virtual {p1}, Lcom/squareup/payment/PaymentHudToaster;->toastInvalidCard()V

    return-void

    .line 230
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->cancelPaymentOnAllContactlessReaders()V

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->application:Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/util/Views;->vibrate(Landroid/content/Context;)V

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->swipeHandler:Lcom/squareup/ui/seller/SwipedCardHandler;

    invoke-interface {v0, p1, p2}, Lcom/squareup/ui/seller/SwipedCardHandler;->processSwipedCard(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/Card;)V

    return-void
.end method

.method private swipeEnabled()Z
    .locals 3

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v2, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-eq v0, v2, :cond_1

    return v1

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->cardSupported()Z

    move-result v0

    if-nez v0, :cond_2

    return v1

    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method private toastPaymentOutOfRange(Lcom/squareup/Card;)V
    .locals 2

    .line 291
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 292
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler;->paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 293
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardTransactionMinimum()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 292
    invoke-virtual {p1, v0}, Lcom/squareup/payment/PaymentHudToaster;->toastOutOfRangeGiftCard(Ljava/lang/Long;)Z

    goto :goto_0

    .line 295
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler;->paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1, v0}, Lcom/squareup/payment/PaymentHudToaster;->toastPaymentOutOfRange(Lcom/squareup/payment/Transaction;)Z

    :goto_0
    return-void
.end method


# virtual methods
.method public authorize()V
    .locals 4

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lcom/squareup/ui/payment/SwipeHandler;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    iget-object v2, p0, Lcom/squareup/ui/payment/SwipeHandler;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v3, p0, Lcom/squareup/ui/payment/SwipeHandler;->transaction:Lcom/squareup/payment/Transaction;

    .line 209
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getCard()Lcom/squareup/Card;

    move-result-object v3

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender$Builder;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Lcom/squareup/tenderpayment/TenderCompleter;->payCardTender(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object v0

    .line 208
    invoke-interface {v1, v0}, Lcom/squareup/ui/tender/TenderStarter;->advanceToNextFlow(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Z

    goto :goto_0

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    iget-object v1, p0, Lcom/squareup/ui/payment/SwipeHandler;->workflow:Lcom/squareup/tenderpayment/CardSellerWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/payment/SwipeHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getCard()Lcom/squareup/Card;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/tenderpayment/CardSellerWorkflow;->authorize(Lcom/squareup/Card;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/tender/TenderStarter;->advanceToNextFlow(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Z

    :goto_0
    return-void
.end method

.method public onFailedSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onUserInteraction()V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    invoke-interface {v0}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 173
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/payment/SwipeHandler;->swipeEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->paymentIsBelowMinimum()Z

    move-result v0

    if-nez v0, :cond_2

    .line 174
    invoke-direct {p0}, Lcom/squareup/ui/payment/SwipeHandler;->abortGlassIfPresent()V

    .line 178
    iget-boolean p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;->swipeStraight:Z

    if-eqz p1, :cond_1

    .line 179
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler;->paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

    invoke-virtual {p1}, Lcom/squareup/payment/PaymentHudToaster;->toastSwipeStraight()V

    goto :goto_0

    .line 181
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler;->paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

    invoke-virtual {p1}, Lcom/squareup/payment/PaymentHudToaster;->toastTryAgain()V

    :cond_2
    :goto_0
    return-void
.end method

.method public onSuccessfulSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onUserInteraction()V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    invoke-interface {v0}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 134
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/payment/SwipeHandler;->canSwipe()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 138
    :cond_1
    iget-object v0, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    invoke-virtual {v0}, Lcom/squareup/Card;->getInputType()Lcom/squareup/Card$InputType;

    move-result-object v0

    sget-object v1, Lcom/squareup/Card$InputType;->GEN2_TRACK2:Lcom/squareup/Card$InputType;

    if-ne v0, v1, :cond_2

    .line 139
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/gen2/Gen2DenialDialog;->INSTANCE:Lcom/squareup/gen2/Gen2DenialDialog;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 143
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/payment/SwipeHandler;->abortGlassIfPresent()V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->giftCards:Lcom/squareup/giftcard/GiftCards;

    iget-object v1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    invoke-virtual {v0, v1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    invoke-interface {v0}, Lcom/squareup/onboarding/OnboardingDiverter;->shouldDivertToOnboarding()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 149
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    sget-object v0, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->PROMPT:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    invoke-interface {p1, v0}, Lcom/squareup/onboarding/OnboardingDiverter;->divertToOnboarding(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V

    return-void

    .line 154
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->swipeValidator:Lcom/squareup/swipe/SwipeValidator;

    invoke-virtual {v0, p1}, Lcom/squareup/swipe/SwipeValidator;->swipeHasEnoughData(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 155
    iget-object v0, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iget-object p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/payment/SwipeHandler;->processSwipedCard(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/Card;)V

    goto :goto_0

    .line 157
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler;->paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

    invoke-virtual {p1}, Lcom/squareup/payment/PaymentHudToaster;->toastSwipeStraight()V

    :goto_0
    return-void
.end method

.method public readyToAuthorize()Z
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->workflow:Lcom/squareup/tenderpayment/CardSellerWorkflow;

    iget-object v1, p0, Lcom/squareup/ui/payment/SwipeHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCard()Lcom/squareup/Card;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/tenderpayment/CardSellerWorkflow;->readyToAuthorize(Lcom/squareup/Card;)Z

    move-result v0

    return v0
.end method

.method public setSwipeHandler(Lmortar/MortarScope;Lcom/squareup/ui/seller/SwipedCardHandler;)V
    .locals 1

    const-string v0, "swipeHandler"

    .line 113
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/seller/SwipedCardHandler;

    iput-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler;->swipeHandler:Lcom/squareup/ui/seller/SwipedCardHandler;

    .line 114
    new-instance v0, Lcom/squareup/ui/payment/SwipeHandler$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/payment/SwipeHandler$1;-><init>(Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/ui/seller/SwipedCardHandler;)V

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method testProcessCard(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/Card;)V
    .locals 0

    .line 216
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/payment/SwipeHandler;->processSwipedCard(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/Card;)V

    return-void
.end method
