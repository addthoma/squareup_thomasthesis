.class public abstract Lcom/squareup/ui/onboarding/OnboardingContainer$Module;
.super Ljava/lang/Object;
.source "OnboardingContainer.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/camerahelper/CameraHelperModule;,
        Lcom/squareup/depositschedule/DepositScheduleModule;,
        Lcom/squareup/instantdeposit/InstantDepositModule;,
        Lcom/squareup/banklinking/LinkBankAccountModule;,
        Lcom/squareup/debitcard/LinkDebitCardModule;,
        Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedInUi;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/OnboardingContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideActivationLaunchMode(Lcom/squareup/ui/onboarding/OnboardingModel;)Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 284
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getActivationLaunchMode()Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    move-result-object p0

    return-object p0
.end method

.method static provideContainerActivityDelegate(Lcom/squareup/util/Device;)Lcom/squareup/container/ContainerActivityDelegate;
    .locals 6
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 298
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingContainer$Module$1;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/OnboardingContainer$Module$1;-><init>()V

    .line 312
    new-instance v1, Lcom/squareup/container/ContainerActivityDelegate;

    new-instance v2, Lcom/squareup/container/WorkflowRunnerViewFactory;

    invoke-direct {v2}, Lcom/squareup/container/WorkflowRunnerViewFactory;-><init>()V

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/squareup/container/ContainerViewFactory;

    new-instance v4, Lcom/squareup/container/ContainerTreeKeyViewFactory;

    invoke-direct {v4}, Lcom/squareup/container/ContainerTreeKeyViewFactory;-><init>()V

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-direct {v1, p0, v0, v2, v3}, Lcom/squareup/container/ContainerActivityDelegate;-><init>(Lcom/squareup/util/Device;Lcom/squareup/container/ContainerBackgroundsProvider;Lcom/squareup/container/ContainerViewFactory;[Lcom/squareup/container/ContainerViewFactory;)V

    return-object v1
.end method

.method static provideFlow(Lcom/squareup/ui/onboarding/OnboardingContainer;)Lflow/Flow;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 317
    invoke-static {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer;->access$000(Lcom/squareup/ui/onboarding/OnboardingContainer;)Lflow/Flow;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 319
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 321
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v2

    const-string p0, "Cannot inject flow before %s loads, try injecting Lazy<Flow>."

    .line 320
    invoke-static {p0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static provideModel(Lcom/squareup/ui/onboarding/ModelHolder;)Lcom/squareup/ui/onboarding/OnboardingModel;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 278
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ModelHolder;->getModel()Lcom/squareup/ui/onboarding/OnboardingModel;

    move-result-object p0

    return-object p0
.end method

.method static provideOnboardingModelBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation

    .line 293
    const-class v0, Lcom/squareup/ui/onboarding/OnboardingModel;

    const-string v1, "onboarding"

    invoke-static {p0, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method abstract bindActivationResourcesServiceAsScoped(Lcom/squareup/onboarding/ActivationResourcesService;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/onboarding/ForOnboarding;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindBrowserLauncher(Lcom/squareup/util/PosBrowserLauncher;)Lcom/squareup/util/BrowserLauncher;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindFreeReaderStatusAsScoped(Lcom/squareup/ui/onboarding/FreeReaderStatus;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/onboarding/ForOnboarding;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindOnboardingExiter(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;)Lcom/squareup/onboarding/OnboardingExiter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindOnboardingServices()Ljava/util/Set;
    .annotation runtime Lcom/squareup/onboarding/ForOnboarding;
    .end annotation

    .annotation runtime Ldagger/multibindings/Multibinds;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation
.end method

.method abstract bindReferralListener(Lcom/squareup/ui/onboarding/OnboardingReferralListener;)Lcom/squareup/referrals/ReferralListener;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
