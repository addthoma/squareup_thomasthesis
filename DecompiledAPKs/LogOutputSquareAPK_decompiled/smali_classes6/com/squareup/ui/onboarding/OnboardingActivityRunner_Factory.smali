.class public final Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;
.super Ljava/lang/Object;
.source "OnboardingActivityRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final adAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final bankAccountSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final freeReaderStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/FreeReaderStatus;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingFinisherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingFinisher;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingModelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingScopedServicesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;"
        }
    .end annotation
.end field

.field private final profileUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantprofile/MerchantProfileUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final setupGuideIntegrationRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final verticalSelectionRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final whatsNewSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/FreeReaderStatus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantprofile/MerchantProfileUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingFinisher;",
            ">;)V"
        }
    .end annotation

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->adAnalyticsProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p3, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p4, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->onboardingModelProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p5, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p6, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->whatsNewSettingsProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p7, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p8, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p9, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->freeReaderStatusProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p10, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->onboardingScopedServicesProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p11, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->setupGuideIntegrationRunnerProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p12, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->verticalSelectionRunnerProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p13, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->profileUpdaterProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p14, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->onboardingFinisherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/FreeReaderStatus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantprofile/MerchantProfileUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingFinisher;",
            ">;)",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;"
        }
    .end annotation

    .line 103
    new-instance v15, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static newInstance(Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/analytics/Analytics;Ldagger/Lazy;Ljavax/inject/Provider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tour/WhatsNewSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/ui/onboarding/FreeReaderStatus;Ljava/util/Set;Ldagger/Lazy;Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;Lcom/squareup/merchantprofile/MerchantProfileUpdater;Lcom/squareup/ui/onboarding/OnboardingFinisher;)Lcom/squareup/ui/onboarding/OnboardingActivityRunner;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            "Lcom/squareup/analytics/Analytics;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tour/WhatsNewSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            "Lcom/squareup/ui/onboarding/FreeReaderStatus;",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;",
            "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;",
            "Lcom/squareup/merchantprofile/MerchantProfileUpdater;",
            "Lcom/squareup/ui/onboarding/OnboardingFinisher;",
            ")",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;"
        }
    .end annotation

    .line 114
    new-instance v15, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;-><init>(Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/analytics/Analytics;Ldagger/Lazy;Ljavax/inject/Provider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tour/WhatsNewSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/ui/onboarding/FreeReaderStatus;Ljava/util/Set;Ldagger/Lazy;Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;Lcom/squareup/merchantprofile/MerchantProfileUpdater;Lcom/squareup/ui/onboarding/OnboardingFinisher;)V

    return-object v15
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/OnboardingActivityRunner;
    .locals 15

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->adAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/adanalytics/AdAnalytics;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->onboardingModelProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->whatsNewSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/tour/WhatsNewSettings;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/banklinking/BankAccountSettings;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->freeReaderStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/onboarding/FreeReaderStatus;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->onboardingScopedServicesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Ljava/util/Set;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->setupGuideIntegrationRunnerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v11

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->verticalSelectionRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->profileUpdaterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/merchantprofile/MerchantProfileUpdater;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->onboardingFinisherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/squareup/ui/onboarding/OnboardingFinisher;

    invoke-static/range {v1 .. v14}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->newInstance(Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/analytics/Analytics;Ldagger/Lazy;Ljavax/inject/Provider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tour/WhatsNewSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/ui/onboarding/FreeReaderStatus;Ljava/util/Set;Ldagger/Lazy;Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;Lcom/squareup/merchantprofile/MerchantProfileUpdater;Lcom/squareup/ui/onboarding/OnboardingFinisher;)Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner_Factory;->get()Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    move-result-object v0

    return-object v0
.end method
