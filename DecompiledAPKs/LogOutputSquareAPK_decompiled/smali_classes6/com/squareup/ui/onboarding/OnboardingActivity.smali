.class public Lcom/squareup/ui/onboarding/OnboardingActivity;
.super Lcom/squareup/ui/SquareActivity;
.source "OnboardingActivity.java"

# interfaces
.implements Lcom/squareup/ui/onboarding/OnboardingFinisher$View;
.implements Lcom/squareup/container/ContainerActivity;
.implements Lcom/squareup/camerahelper/CameraHelper$View;


# static fields
.field static final ACTIVATION_MODE_KEY:Ljava/lang/String; = "ACTIVATION_MODE_KEY"

.field static final DESTINATION_AFTER_KEY:Ljava/lang/String; = "DESTINATION_AFTER_KEY"


# instance fields
.field activationResourcesService:Lcom/squareup/onboarding/ActivationResourcesService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field browserLauncher:Lcom/squareup/util/BrowserLauncher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field cameraHelper:Lcom/squareup/camerahelper/CameraHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field container:Lcom/squareup/ui/onboarding/OnboardingContainer;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field featureFlagFeatures:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field modelHolder:Lcom/squareup/ui/onboarding/ModelHolder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field onboardingFinisher:Lcom/squareup/ui/onboarding/OnboardingFinisher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field toastFactory:Lcom/squareup/util/ToastFactory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/ui/SquareActivity$Preconditions;->LOGGED_IN:Lcom/squareup/ui/SquareActivity$Preconditions;

    invoke-direct {p0, v0}, Lcom/squareup/ui/SquareActivity;-><init>(Lcom/squareup/ui/SquareActivity$Preconditions;)V

    return-void
.end method

.method private getDestinationAfterOnboarding()Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;
    .locals 2

    .line 119
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "DESTINATION_AFTER_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;

    return-object v0
.end method

.method private getLaunchMode()Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;
    .locals 2

    .line 115
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ACTIVATION_MODE_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    return-object v0
.end method

.method private preventScreenshots()Z
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->featureFlagFeatures:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_ALLOW_SCREENSHOT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method protected doBackPressed()V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingContainer;->onBackPressed()V

    return-void
.end method

.method protected doCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 52
    invoke-super {p0, p1}, Lcom/squareup/ui/SquareActivity;->doCreate(Landroid/os/Bundle;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->modelHolder:Lcom/squareup/ui/onboarding/ModelHolder;

    .line 56
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivity;->getLaunchMode()Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    move-result-object v1

    .line 57
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivity;->getDestinationAfterOnboarding()Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;

    move-result-object v2

    .line 54
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/ui/onboarding/ModelHolder;->maybeRestoreFromBundle(Landroid/os/Bundle;Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;)V

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    invoke-virtual {p1, p0}, Lcom/squareup/ui/SoftInputPresenter;->takeView(Ljava/lang/Object;)V

    .line 60
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->onboardingFinisher:Lcom/squareup/ui/onboarding/OnboardingFinisher;

    invoke-virtual {p1, p0}, Lcom/squareup/ui/onboarding/OnboardingFinisher;->takeView(Ljava/lang/Object;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    invoke-interface {p1, p0}, Lcom/squareup/camerahelper/CameraHelper;->takeView(Lcom/squareup/camerahelper/CameraHelper$View;)V

    .line 62
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {p1, p0}, Lcom/squareup/util/BrowserLauncher;->takeActivity(Landroid/app/Activity;)V

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;

    invoke-virtual {p1, p0}, Lcom/squareup/container/ContainerActivityDelegate;->takeActivity(Lcom/squareup/container/ContainerActivity;)V

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;

    sget-object v0, Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;->INSTANCE:Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;

    sget v1, Lcom/squareup/onboarding/flow/R$layout;->onboarding_container:I

    .line 65
    invoke-virtual {p1, v0, p0, v1}, Lcom/squareup/container/ContainerActivityDelegate;->inflateRootScreen(Lflow/path/Path;Landroid/content/Context;I)Landroid/view/View;

    move-result-object p1

    .line 64
    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/OnboardingActivity;->setContentView(Landroid/view/View;)V

    .line 68
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->activationResourcesService:Lcom/squareup/onboarding/ActivationResourcesService;

    invoke-virtual {p1}, Lcom/squareup/onboarding/ActivationResourcesService;->refresh()V

    .line 70
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/OnboardingActivity;->preventScreenshots()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingActivity;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/16 v0, 0x2000

    invoke-virtual {p1, v0}, Landroid/view/Window;->addFlags(I)V

    :cond_0
    return-void
.end method

.method protected doSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 76
    invoke-super {p0, p1}, Lcom/squareup/ui/SquareActivity;->doSaveInstanceState(Landroid/os/Bundle;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->modelHolder:Lcom/squareup/ui/onboarding/ModelHolder;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/ModelHolder;->toBundle(Landroid/os/Bundle;)V

    return-void
.end method

.method public finish()V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingContainer;->onActivityFinish()V

    .line 103
    :cond_0
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->finish()V

    return-void
.end method

.method public getBundleService()Lmortar/bundler/BundleService;
    .locals 1

    .line 111
    invoke-static {p0}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object v0

    return-object v0
.end method

.method protected inject(Lmortar/MortarScope;)V
    .locals 1

    .line 48
    const-class v0, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;

    invoke-interface {p1, p0}, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;->inject(Lcom/squareup/ui/onboarding/OnboardingActivity;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->onboardingFinisher:Lcom/squareup/ui/onboarding/OnboardingFinisher;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;

    invoke-virtual {v0, p0}, Lcom/squareup/container/ContainerActivityDelegate;->dropActivity(Lcom/squareup/container/ContainerActivity;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {v0, p0}, Lcom/squareup/util/BrowserLauncher;->dropActivity(Landroid/app/Activity;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    invoke-interface {v0, p0}, Lcom/squareup/camerahelper/CameraHelper;->dropView(Lcom/squareup/camerahelper/CameraHelper$View;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->onboardingFinisher:Lcom/squareup/ui/onboarding/OnboardingFinisher;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingFinisher;->dropView(Ljava/lang/Object;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/SoftInputPresenter;->dropView(Ljava/lang/Object;)V

    .line 89
    :cond_0
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->onDestroy()V

    return-void
.end method

.method public toast(I)V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->toastFactory:Lcom/squareup/util/ToastFactory;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/squareup/util/ToastFactory;->showText(II)V

    return-void
.end method
