.class public Lcom/squareup/ui/onboarding/postalvalidation/USPostalValidator;
.super Ljava/lang/Object;
.source "USPostalValidator.java"

# interfaces
.implements Lcom/squareup/ui/onboarding/postalvalidation/PostalValidator;


# static fields
.field private static final US_POSTAL_CODE_EXT_LENGTH:I = 0xa

.field private static final US_POSTAL_CODE_LENGTH:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public precheck(Ljava/lang/String;I)Lcom/squareup/ui/onboarding/ValidationError;
    .locals 2

    .line 22
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    .line 25
    new-instance p1, Lcom/squareup/ui/onboarding/ValidationError;

    sget v0, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    sget v1, Lcom/squareup/onboarding/flow/R$string;->invalid_postal_code:I

    invoke-direct {p1, v0, v1, p2}, Lcom/squareup/ui/onboarding/ValidationError;-><init>(III)V

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method
