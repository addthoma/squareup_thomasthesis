.class public final Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideConfirmIdentityReceivedResponseFactory;
.super Ljava/lang/Object;
.source "ActivationCallModule_ProvideConfirmIdentityReceivedResponseFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onboarding/ShareableReceivedResponse<",
        "Lcom/squareup/server/activation/AnswersBody;",
        "Lcom/squareup/server/SimpleResponse;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final activationServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/activation/ActivationService;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/ui/onboarding/ActivationCallModule;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/onboarding/ActivationCallModule;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/ActivationCallModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/activation/ActivationService;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideConfirmIdentityReceivedResponseFactory;->module:Lcom/squareup/ui/onboarding/ActivationCallModule;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideConfirmIdentityReceivedResponseFactory;->activationServiceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/onboarding/ActivationCallModule;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideConfirmIdentityReceivedResponseFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/ActivationCallModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/activation/ActivationService;",
            ">;)",
            "Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideConfirmIdentityReceivedResponseFactory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideConfirmIdentityReceivedResponseFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideConfirmIdentityReceivedResponseFactory;-><init>(Lcom/squareup/ui/onboarding/ActivationCallModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideConfirmIdentityReceivedResponse(Lcom/squareup/ui/onboarding/ActivationCallModule;Lcom/squareup/server/activation/ActivationService;)Lcom/squareup/onboarding/ShareableReceivedResponse;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/ActivationCallModule;",
            "Lcom/squareup/server/activation/ActivationService;",
            ")",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/AnswersBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/ActivationCallModule;->provideConfirmIdentityReceivedResponse(Lcom/squareup/server/activation/ActivationService;)Lcom/squareup/onboarding/ShareableReceivedResponse;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/onboarding/ShareableReceivedResponse;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/onboarding/ShareableReceivedResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/AnswersBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideConfirmIdentityReceivedResponseFactory;->module:Lcom/squareup/ui/onboarding/ActivationCallModule;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideConfirmIdentityReceivedResponseFactory;->activationServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/activation/ActivationService;

    invoke-static {v0, v1}, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideConfirmIdentityReceivedResponseFactory;->provideConfirmIdentityReceivedResponse(Lcom/squareup/ui/onboarding/ActivationCallModule;Lcom/squareup/server/activation/ActivationService;)Lcom/squareup/onboarding/ShareableReceivedResponse;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideConfirmIdentityReceivedResponseFactory;->get()Lcom/squareup/onboarding/ShareableReceivedResponse;

    move-result-object v0

    return-object v0
.end method
