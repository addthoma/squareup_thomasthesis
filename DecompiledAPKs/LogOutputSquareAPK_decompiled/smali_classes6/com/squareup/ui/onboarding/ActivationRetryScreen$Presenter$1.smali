.class Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter$1;
.super Lcom/squareup/mortar/PopupPresenter;
.source "ActivationRetryScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/register/widgets/Confirmation;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter$1;->this$0:Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;

    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPopupResult(Ljava/lang/Boolean;)V
    .locals 1

    .line 48
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter$1;->this$0:Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->access$000(Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;)Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onRetryLater()V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 46
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter$1;->onPopupResult(Ljava/lang/Boolean;)V

    return-void
.end method
