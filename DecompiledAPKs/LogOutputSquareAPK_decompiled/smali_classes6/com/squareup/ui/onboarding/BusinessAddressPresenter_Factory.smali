.class public final Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;
.super Ljava/lang/Object;
.source "BusinessAddressPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/BusinessAddressPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final businessAddressMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final modelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final spinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p2, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->modelProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p3, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p4, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p5, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p6, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->businessAddressMonitorProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p7, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->spinnerProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p8, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;"
        }
    .end annotation

    .line 63
    new-instance v9, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/onboarding/BusinessAddressPresenter;
    .locals 10

    .line 70
    new-instance v9, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/analytics/Analytics;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/BusinessAddressPresenter;
    .locals 9

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->modelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/onboarding/OnboardingModel;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->businessAddressMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->spinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/register/widgets/GlassSpinner;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/analytics/Analytics;

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/onboarding/BusinessAddressPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/BusinessAddressPresenter_Factory;->get()Lcom/squareup/ui/onboarding/BusinessAddressPresenter;

    move-result-object v0

    return-object v0
.end method
