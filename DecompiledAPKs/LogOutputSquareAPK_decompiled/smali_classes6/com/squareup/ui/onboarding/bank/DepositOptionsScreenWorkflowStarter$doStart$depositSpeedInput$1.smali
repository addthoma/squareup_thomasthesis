.class final Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$depositSpeedInput$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DepositOptionsScreenWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->doStart(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
        "it",
        "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$depositSpeedInput$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$depositSpeedInput$1;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$depositSpeedInput$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$depositSpeedInput$1;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$depositSpeedInput$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    sget-object v0, Lcom/squareup/ui/onboarding/bank/LaterClicked;->INSTANCE:Lcom/squareup/ui/onboarding/bank/LaterClicked;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnLaterClickedFromDepositSpeedScreen;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnLaterClickedFromDepositSpeedScreen;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;

    goto :goto_0

    .line 147
    :cond_0
    sget-object v0, Lcom/squareup/ui/onboarding/bank/NextBusinessDayDepositSelected;->INSTANCE:Lcom/squareup/ui/onboarding/bank/NextBusinessDayDepositSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnNextBusinessDayDepositSelected;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnNextBusinessDayDepositSelected;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;

    goto :goto_0

    .line 148
    :cond_1
    sget-object v0, Lcom/squareup/ui/onboarding/bank/SameDayDepositSelected;->INSTANCE:Lcom/squareup/ui/onboarding/bank/SameDayDepositSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnSameDayDepositSelected;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnSameDayDepositSelected;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;

    goto :goto_0

    .line 149
    :cond_2
    sget-object v0, Lcom/squareup/ui/onboarding/bank/BackPressed;->INSTANCE:Lcom/squareup/ui/onboarding/bank/BackPressed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnBackPressedFromDepositSpeedScreen;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnBackPressedFromDepositSpeedScreen;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 93
    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$depositSpeedInput$1;->invoke(Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;

    move-result-object p1

    return-object p1
.end method
