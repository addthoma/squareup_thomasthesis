.class final Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkBankAccount$1;
.super Ljava/lang/Object;
.source "DepositOptionsReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->linkBankAccount(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
        "it",
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;

.field final synthetic this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkBankAccount$1;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkBankAccount$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;
    .locals 4

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 356
    iget-object v0, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 361
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;

    .line 362
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkBankAccount$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;->getSameDayDepositSelected()Z

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkBankAccount$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;

    invoke-virtual {v2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;->getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;

    move-result-object v2

    iget-object p1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 361
    :cond_0
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;-><init>(ZLcom/squareup/protos/client/bankaccount/BankAccountType;Lcom/squareup/receiving/FailureMessage;)V

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto :goto_1

    .line 360
    :cond_1
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankFailure;

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto :goto_1

    .line 357
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkBankAccount$1;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;

    .line 358
    iget-object v2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkBankAccount$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;

    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->verificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p1

    sget-object v3, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFIED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    if-ne p1, v3, :cond_3

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    .line 357
    :goto_0
    invoke-static {v0, v2, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->access$bankSuccessOrPending(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;Z)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 81
    check-cast p1, Lcom/squareup/banklinking/BankAccountSettings$State;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkBankAccount$1;->apply(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    move-result-object p1

    return-object p1
.end method
