.class final Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$attach$2;
.super Ljava/lang/Object;
.source "DepositBankLinkingCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;",
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u000120\u0010\u0002\u001a,\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0007*\u0016\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003j\u0004\u0018\u0001`\u00060\u0003j\u0002`\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;",
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinkingScreen;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$attach$2;->this$0:Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
            ">;)V"
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$attach$2;->this$0:Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->access$onScreen(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$attach$2;->accept(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method
