.class public abstract Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "DepositOptionsBootstrapScreen.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen$Onboarding;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen$MainActivity;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008&\u0018\u00002\u00020\u0001:\u0002\u0007\u0008B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "()V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "MainActivity",
        "Onboarding",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public final doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;->Companion:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Companion;->startNewWorkflow(Lmortar/MortarScope;)V

    return-void
.end method
