.class public Lcom/squareup/ui/onboarding/BusinessInfoScreen;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "BusinessInfoScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/onboarding/BusinessInfoScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/BusinessInfoScreen$Component;,
        Lcom/squareup/ui/onboarding/BusinessInfoScreen$Module;,
        Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;,
        Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/BusinessInfoScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/BusinessInfoScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/ui/onboarding/BusinessInfoScreen;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/BusinessInfoScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/BusinessInfoScreen;->INSTANCE:Lcom/squareup/ui/onboarding/BusinessInfoScreen;

    .line 40
    sget-object v0, Lcom/squareup/ui/onboarding/BusinessInfoScreen;->INSTANCE:Lcom/squareup/ui/onboarding/BusinessInfoScreen;

    .line 41
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/BusinessInfoScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_BUSINESS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 189
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->business_info_view:I

    return v0
.end method
