.class public final Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideModelFactory;
.super Ljava/lang/Object;
.source "OnboardingContainer_Module_ProvideModelFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/OnboardingModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final holderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ModelHolder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ModelHolder;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideModelFactory;->holderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideModelFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ModelHolder;",
            ">;)",
            "Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideModelFactory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideModelFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideModelFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideModel(Lcom/squareup/ui/onboarding/ModelHolder;)Lcom/squareup/ui/onboarding/OnboardingModel;
    .locals 1

    .line 34
    invoke-static {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer$Module;->provideModel(Lcom/squareup/ui/onboarding/ModelHolder;)Lcom/squareup/ui/onboarding/OnboardingModel;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/onboarding/OnboardingModel;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/OnboardingModel;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideModelFactory;->holderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/ModelHolder;

    invoke-static {v0}, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideModelFactory;->provideModel(Lcom/squareup/ui/onboarding/ModelHolder;)Lcom/squareup/ui/onboarding/OnboardingModel;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideModelFactory;->get()Lcom/squareup/ui/onboarding/OnboardingModel;

    move-result-object v0

    return-object v0
.end method
