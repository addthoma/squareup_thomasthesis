.class public Lcom/squareup/ui/onboarding/PersonalInfoPresenter;
.super Lmortar/ViewPresenter;
.source "PersonalInfoPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/PersonalInfoView;",
        ">;"
    }
.end annotation


# static fields
.field private static final SSN_LENGTH:I = 0x4


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field final confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/register/widgets/Confirmation;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final container:Lcom/squareup/ui/onboarding/OnboardingContainer;

.field private final dateFormat:Ljava/text/DateFormat;

.field final dateOfBirthPopupPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/SquareDate;",
            "Lcom/squareup/ui/SquareDate;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceIdProvider:Lcom/squareup/settings/DeviceIdProvider;

.field private final installationIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final model:Lcom/squareup/ui/onboarding/OnboardingModel;

.field private final phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

.field protected final serverCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/ApplyBody;",
            ">;"
        }
    .end annotation
.end field

.field private final shareableReceivedResponse:Lcom/squareup/onboarding/ShareableReceivedResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final ssnStrings:Lcom/squareup/ui/onboarding/SSNStrings;

.field final warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/OnboardingContainer;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Ljava/text/DateFormat;Lcom/squareup/ui/onboarding/SSNStrings;Lcom/squareup/settings/DeviceIdProvider;Ljavax/inject/Provider;Lcom/squareup/onboarding/ShareableReceivedResponse;Lcom/squareup/analytics/Analytics;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/ui/onboarding/ActivationStatusPresenter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            "Lcom/squareup/ui/onboarding/OnboardingContainer;",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/ui/onboarding/SSNStrings;",
            "Lcom/squareup/settings/DeviceIdProvider;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/ApplyBody;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 85
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 53
    new-instance v0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter$1;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoPresenter;)V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->dateOfBirthPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    .line 70
    new-instance v0, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 71
    new-instance v0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter$2;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoPresenter;)V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    .line 86
    iput-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->res:Lcom/squareup/util/Res;

    .line 87
    iput-object p2, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 88
    iput-object p3, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    .line 89
    iput-object p4, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    .line 90
    iput-object p5, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->dateFormat:Ljava/text/DateFormat;

    .line 91
    iput-object p6, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->ssnStrings:Lcom/squareup/ui/onboarding/SSNStrings;

    .line 92
    iput-object p7, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->deviceIdProvider:Lcom/squareup/settings/DeviceIdProvider;

    .line 93
    iput-object p8, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->installationIdProvider:Ljavax/inject/Provider;

    .line 94
    iput-object p9, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->shareableReceivedResponse:Lcom/squareup/onboarding/ShareableReceivedResponse;

    .line 95
    iput-object p10, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 96
    iput-object p11, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    .line 97
    iput-object p12, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->serverCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/onboarding/PersonalInfoPresenter;)Lcom/squareup/ui/onboarding/OnboardingActivityRunner;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    return-object p0
.end method

.method private isValidInput()Z
    .locals 6

    .line 215
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/PersonalInfoView;

    .line 217
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->isNameSet()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 218
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v3, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_MISSING:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v1, v3}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 219
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v3, Lcom/squareup/widgets/warning/WarningIds;

    sget v4, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    sget v5, Lcom/squareup/onboarding/flow/R$string;->missing_name_fields:I

    invoke-direct {v3, v4, v5}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {v1, v3}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 222
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->focusFirstName()V

    return v2

    .line 226
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->isPersonalAddressSet()Z

    move-result v1

    if-nez v1, :cond_1

    .line 227
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v3, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_MISSING:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v1, v3}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 228
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v3, Lcom/squareup/widgets/warning/WarningIds;

    sget v4, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    sget v5, Lcom/squareup/onboarding/flow/R$string;->missing_address_fields:I

    invoke-direct {v3, v4, v5}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {v1, v3}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 231
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->focusPersonalAddress()V

    return v2

    .line 235
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->isPhoneNumberSet()Z

    move-result v1

    if-nez v1, :cond_2

    .line 236
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v3, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_MISSING:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v1, v3}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 237
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v3, Lcom/squareup/widgets/warning/WarningIds;

    sget v4, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    sget v5, Lcom/squareup/onboarding/flow/R$string;->missing_phone_number_field:I

    invoke-direct {v3, v4, v5}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {v1, v3}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 240
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->focusPhoneNumber()V

    return v2

    .line 244
    :cond_2
    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->isValidPhoneNumber(Lcom/squareup/ui/onboarding/PersonalInfoView;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 245
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v3, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID_PHONE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v1, v3}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 246
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v3, Lcom/squareup/widgets/warning/WarningIds;

    sget v4, Lcom/squareup/onboarding/flow/R$string;->invalid_phone_number:I

    sget v5, Lcom/squareup/onboarding/flow/R$string;->invalid_phone_number_message:I

    invoke-direct {v3, v4, v5}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {v1, v3}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 248
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->focusPhoneNumber()V

    return v2

    .line 252
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->isBirthDateSet()Z

    move-result v1

    if-nez v1, :cond_4

    .line 253
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v3, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_MISSING:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v1, v3}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 254
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v3, Lcom/squareup/widgets/warning/WarningIds;

    sget v4, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    sget v5, Lcom/squareup/onboarding/flow/R$string;->missing_date_of_birth:I

    invoke-direct {v3, v4, v5}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {v1, v3}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 257
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->focusBirthDate()V

    return v2

    .line 261
    :cond_4
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->isLastSsnSet()Z

    move-result v1

    if-nez v1, :cond_5

    .line 262
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v3, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_MISSING:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v1, v3}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 263
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v3, Lcom/squareup/widgets/warning/WarningIds;

    sget v4, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    iget-object v5, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->ssnStrings:Lcom/squareup/ui/onboarding/SSNStrings;

    iget v5, v5, Lcom/squareup/ui/onboarding/SSNStrings;->invalid_last4_ssn_message:I

    invoke-direct {v3, v4, v5}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {v1, v3}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 266
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->focusLastSsn()V

    return v2

    .line 270
    :cond_5
    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->isValidSsn(Lcom/squareup/ui/onboarding/PersonalInfoView;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 271
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v3, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID_SSN:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v1, v3}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 272
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->focusLastSsn()V

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    sget v3, Lcom/squareup/onboarding/flow/R$string;->invalid_ssn:I

    iget-object v4, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->ssnStrings:Lcom/squareup/ui/onboarding/SSNStrings;

    iget v4, v4, Lcom/squareup/ui/onboarding/SSNStrings;->invalid_last4_ssn_message:I

    invoke-direct {v1, v3, v4}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return v2

    :cond_6
    const/4 v0, 0x1

    return v0
.end method

.method private isValidPhoneNumber(Lcom/squareup/ui/onboarding/PersonalInfoView;)Z
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/PersonalInfoView;->getPhoneNumber()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/PhoneNumberHelper;->isValid(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method private isValidSsn(Lcom/squareup/ui/onboarding/PersonalInfoView;)Z
    .locals 1

    .line 281
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/PersonalInfoView;->getLastSsn()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public static synthetic lambda$bgJPb1ZvDv0zpJTruhyqBTugkyM(Lcom/squareup/ui/onboarding/PersonalInfoPresenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->onSkipButtonTapped()V

    return-void
.end method

.method private onSkipButtonTapped()V
    .locals 5

    .line 128
    new-instance v0, Lcom/squareup/register/widgets/ConfirmationIds;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->onboarding_shipping_confirm_have_reader_title:I

    sget v2, Lcom/squareup/onboarding/flow/R$string;->onboarding_shipping_confirm_later:I

    sget v3, Lcom/squareup/common/strings/R$string;->continue_label:I

    sget v4, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/register/widgets/ConfirmationIds;-><init>(IIII)V

    .line 133
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method private setBirthDate(Lcom/squareup/ui/SquareDate;Lcom/squareup/ui/onboarding/PersonalInfoView;)V
    .locals 1

    .line 289
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {p1}, Lcom/squareup/ui/SquareDate;->asDate()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/onboarding/PersonalInfoView;->setBirthDateText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onAdvanced$0$PersonalInfoPresenter()V
    .locals 6

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->installationIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 185
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->deviceIdProvider:Lcom/squareup/settings/DeviceIdProvider;

    invoke-interface {v1}, Lcom/squareup/settings/DeviceIdProvider;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 186
    iget-object v2, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v2}, Lcom/squareup/ui/onboarding/OnboardingModel;->getPersonalAddress()Lcom/squareup/address/Address;

    move-result-object v2

    .line 187
    iget-object v3, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v3}, Lcom/squareup/ui/onboarding/OnboardingModel;->getRevenue()Lcom/squareup/server/activation/ActivationResources$RevenueEntry;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 188
    iget-object v3, v3, Lcom/squareup/server/activation/ActivationResources$RevenueEntry;->key:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 190
    :goto_0
    new-instance v4, Lcom/squareup/server/activation/ApplyBody$Builder;

    invoke-direct {v4}, Lcom/squareup/server/activation/ApplyBody$Builder;-><init>()V

    iget-object v5, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v5}, Lcom/squareup/ui/onboarding/OnboardingModel;->getPersonalFirstName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/server/activation/ApplyBody$Builder;->setFirstName(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 191
    invoke-virtual {v5}, Lcom/squareup/ui/onboarding/OnboardingModel;->getPersonalLastName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/server/activation/ApplyBody$Builder;->setLastName(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 192
    invoke-virtual {v5}, Lcom/squareup/ui/onboarding/OnboardingModel;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/server/activation/ApplyBody$Builder;->setPhoneNumber(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v4

    iget-object v5, v2, Lcom/squareup/address/Address;->street:Ljava/lang/String;

    .line 193
    invoke-virtual {v4, v5}, Lcom/squareup/server/activation/ApplyBody$Builder;->setStreet1(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v4

    iget-object v5, v2, Lcom/squareup/address/Address;->apartment:Ljava/lang/String;

    .line 194
    invoke-virtual {v4, v5}, Lcom/squareup/server/activation/ApplyBody$Builder;->setStreet2(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v4

    iget-object v5, v2, Lcom/squareup/address/Address;->city:Ljava/lang/String;

    .line 195
    invoke-virtual {v4, v5}, Lcom/squareup/server/activation/ApplyBody$Builder;->setCity(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v4

    iget-object v5, v2, Lcom/squareup/address/Address;->state:Ljava/lang/String;

    .line 196
    invoke-virtual {v4, v5}, Lcom/squareup/server/activation/ApplyBody$Builder;->setState(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v4

    iget-object v2, v2, Lcom/squareup/address/Address;->postalCode:Ljava/lang/String;

    .line 197
    invoke-virtual {v4, v2}, Lcom/squareup/server/activation/ApplyBody$Builder;->setPostalCode(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v2

    iget-object v4, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 198
    invoke-virtual {v4}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBirthDate()Lcom/squareup/ui/SquareDate;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/ui/SquareDate;->apiDate:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/squareup/server/activation/ApplyBody$Builder;->setCirca(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v2

    iget-object v4, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 199
    invoke-virtual {v4}, Lcom/squareup/ui/onboarding/OnboardingModel;->getLastSsn()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/squareup/server/activation/ApplyBody$Builder;->setSsnLastFour(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v2

    iget-object v4, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 200
    invoke-virtual {v4}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBusinessKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/squareup/server/activation/ApplyBody$Builder;->setBusinessType(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v2

    iget-object v4, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 201
    invoke-virtual {v4}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBusinessName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/squareup/server/activation/ApplyBody$Builder;->setBusinessName(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v2

    .line 202
    invoke-virtual {v2, v0}, Lcom/squareup/server/activation/ApplyBody$Builder;->setInstallation(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v0

    .line 203
    invoke-virtual {v0, v1}, Lcom/squareup/server/activation/ApplyBody$Builder;->setDevice(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v0

    .line 204
    invoke-virtual {v0, v3}, Lcom/squareup/server/activation/ApplyBody$Builder;->setEstimatedRevenue(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 205
    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getEin()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/activation/ApplyBody$Builder;->setEin(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;

    move-result-object v0

    .line 206
    invoke-virtual {v0}, Lcom/squareup/server/activation/ApplyBody$Builder;->build()Lcom/squareup/server/activation/ApplyBody;

    move-result-object v0

    .line 207
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->shareableReceivedResponse:Lcom/squareup/onboarding/ShareableReceivedResponse;

    invoke-virtual {v1, v0}, Lcom/squareup/onboarding/ShareableReceivedResponse;->send(Ljava/lang/Object;)V

    return-void
.end method

.method protected onAdvanced()V
    .locals 3

    .line 173
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/PersonalInfoView;

    .line 175
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->isValidInput()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 177
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->getLastSsn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/onboarding/OnboardingModel;->setLastSsn(Ljava/lang/String;)V

    .line 178
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->getFirstName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/onboarding/OnboardingModel;->setPersonalFirstName(Ljava/lang/String;)V

    .line 179
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->getLastName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/onboarding/OnboardingModel;->setPersonalLastName(Ljava/lang/String;)V

    .line 180
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->getPersonalAddress()Lcom/squareup/address/Address;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/onboarding/OnboardingModel;->setPersonalAddress(Lcom/squareup/address/Address;)V

    .line 181
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->setPhoneNumber(Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->serverCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$PersonalInfoPresenter$DwBke4RsNPNFZC7kcUYTxTiffKU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$PersonalInfoPresenter$DwBke4RsNPNFZC7kcUYTxTiffKU;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoPresenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->call(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_PERSONAL_INFO_CONTINUE_INVALID:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    :goto_0
    return-void
.end method

.method protected onBackPressed()Z
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->showLaterInPersonalInfo()Z

    move-result v0

    return v0
.end method

.method onFirstNameChanged()V
    .locals 2

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_FIRST_NAME:Lcom/squareup/analytics/RegisterSelectName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logSelect(Lcom/squareup/analytics/RegisterSelectName;)V

    return-void
.end method

.method onLastNameChanged()V
    .locals 2

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_LAST_NAME:Lcom/squareup/analytics/RegisterSelectName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logSelect(Lcom/squareup/analytics/RegisterSelectName;)V

    return-void
.end method

.method onLastSsnChanged()V
    .locals 2

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_LAST_SSN:Lcom/squareup/analytics/RegisterSelectName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logSelect(Lcom/squareup/analytics/RegisterSelectName;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 101
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 103
    new-instance p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$5ZQ0I9OVAgq3phbZJSSrfxeiYdE;

    invoke-direct {v1, v0}, Lcom/squareup/ui/onboarding/-$$Lambda$5ZQ0I9OVAgq3phbZJSSrfxeiYdE;-><init>(Lcom/squareup/ui/onboarding/OnboardingContainer;)V

    .line 104
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 105
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->updateUpButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    .line 106
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$7ztLhMWxGWezulym_tXeNNiRJ5A;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$7ztLhMWxGWezulym_tXeNNiRJ5A;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoPresenter;)V

    .line 107
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->showLaterInPersonalInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 111
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_later:I

    .line 112
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$PersonalInfoPresenter$bgJPb1ZvDv0zpJTruhyqBTugkyM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$PersonalInfoPresenter$bgJPb1ZvDv0zpJTruhyqBTugkyM;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoPresenter;)V

    .line 113
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 115
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/PersonalInfoView;

    .line 116
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBirthDate()Lcom/squareup/ui/SquareDate;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 119
    iget-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBirthDate()Lcom/squareup/ui/SquareDate;

    move-result-object p1

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->setBirthDate(Lcom/squareup/ui/SquareDate;Lcom/squareup/ui/onboarding/PersonalInfoView;)V

    .line 122
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getPersonalFirstName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/PersonalInfoView;->setFirstName(Ljava/lang/String;)V

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getPersonalLastName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/PersonalInfoView;->setLastName(Ljava/lang/String;)V

    .line 124
    iget-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getPhoneNumber()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/PersonalInfoView;->setPhoneNumber(Ljava/lang/String;)V

    return-void
.end method

.method onPhoneNumberChanged()V
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_PHONE_NUMBER:Lcom/squareup/analytics/RegisterSelectName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logSelect(Lcom/squareup/analytics/RegisterSelectName;)V

    return-void
.end method

.method onPostalChanged()V
    .locals 2

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_POSTAL:Lcom/squareup/analytics/RegisterSelectName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logSelect(Lcom/squareup/analytics/RegisterSelectName;)V

    return-void
.end method

.method onStreetChanged()V
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_STREET:Lcom/squareup/analytics/RegisterSelectName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logSelect(Lcom/squareup/analytics/RegisterSelectName;)V

    return-void
.end method

.method setBirthDate(Lcom/squareup/ui/SquareDate;)V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/OnboardingModel;->setBirthDate(Lcom/squareup/ui/SquareDate;)V

    .line 65
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/PersonalInfoView;

    .line 66
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->setBirthDate(Lcom/squareup/ui/SquareDate;Lcom/squareup/ui/onboarding/PersonalInfoView;)V

    .line 67
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->focusLastSsn()V

    return-void
.end method

.method public showBirthDatePicker()V
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BIRTH_DATE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBirthDate()Lcom/squareup/ui/SquareDate;

    move-result-object v0

    if-nez v0, :cond_0

    .line 139
    invoke-static {}, Lcom/squareup/ui/SquareDate;->defaultBirthDate()Lcom/squareup/ui/SquareDate;

    move-result-object v0

    .line 140
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->dateOfBirthPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method
