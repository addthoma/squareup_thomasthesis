.class public final Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "ActivateYourAccountScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Component;,
        Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;->INSTANCE:Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;

    .line 23
    sget-object v0, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;->INSTANCE:Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;

    .line 24
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_ACTIVATE_YOUR_ACCOUNT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 55
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->activate_your_account_view:I

    return v0
.end method
