.class public Lcom/squareup/ui/onboarding/OnboardingContainerView;
.super Lcom/squareup/container/ContainerRelativeLayout;
.source "OnboardingContainerView.java"


# instance fields
.field badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field container:Lcom/squareup/ui/onboarding/OnboardingContainer;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final sessionExpiredPopup:Lcom/squareup/flowlegacy/WarningPopup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/container/ContainerRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const-class p2, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;

    invoke-interface {p2, p0}, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;->inject(Lcom/squareup/ui/onboarding/OnboardingContainerView;)V

    .line 29
    new-instance p2, Lcom/squareup/flowlegacy/WarningPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/WarningPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView;->sessionExpiredPopup:Lcom/squareup/flowlegacy/WarningPopup;

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/OnboardingContainer;->sessionExpired:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView;->sessionExpiredPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingContainer;->dropView(Lcom/squareup/container/ContainerView;)V

    .line 43
    invoke-super {p0}, Lcom/squareup/container/ContainerRelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 33
    invoke-super {p0}, Lcom/squareup/container/ContainerRelativeLayout;->onFinishInflate()V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingContainer;->takeView(Ljava/lang/Object;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/OnboardingContainer;->sessionExpired:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView;->sessionExpiredPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->IGNORE_ACCESSIBILITY_SCRUBBER:Lcom/squareup/settings/server/Features$Feature;

    .line 48
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    invoke-static {p2}, Lcom/squareup/accessibility/AccessibilityScrubber;->getPiiRecord(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityRecord;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 51
    invoke-static {p0, p2, v0}, Lcom/squareup/accessibility/AccessibilityScrubber;->sendPiiScrubbedEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;Landroid/view/accessibility/AccessibilityRecord;)V

    const/4 p1, 0x0

    return p1

    .line 56
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/squareup/container/ContainerRelativeLayout;->onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result p1

    return p1
.end method
