.class public abstract Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;
.super Lcom/squareup/widgets/ResponsiveScrollView;
.source "OnboardingPromptBaseView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# static fields
.field protected static ROTATE_LEFT:I = -0x5a


# instance fields
.field protected firstButton:Landroid/widget/Button;

.field protected glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field protected messageView:Lcom/squareup/widgets/MessageView;

.field protected secondButton:Landroid/widget/Button;

.field protected titleView:Lcom/squareup/widgets/MessageView;

.field protected vectorView:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 36
    invoke-static {p1}, Lcom/squareup/util/Views;->vectorFriendly(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/ResponsiveScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 40
    invoke-super {p0}, Lcom/squareup/widgets/ResponsiveScrollView;->onFinishInflate()V

    .line 42
    sget v0, Lcom/squareup/onboarding/flow/R$id;->first_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->firstButton:Landroid/widget/Button;

    .line 43
    sget v0, Lcom/squareup/onboarding/flow/R$id;->second_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->secondButton:Landroid/widget/Button;

    .line 44
    sget v0, Lcom/squareup/onboarding/flow/R$id;->glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 45
    sget v0, Lcom/squareup/onboarding/flow/R$id;->vector:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->vectorView:Landroid/widget/ImageView;

    .line 46
    sget v0, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->titleView:Lcom/squareup/widgets/MessageView;

    .line 47
    sget v0, Lcom/squareup/onboarding/flow/R$id;->message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->messageView:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public setBottomButton(I)V
    .locals 1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->secondButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->secondButton:Landroid/widget/Button;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 89
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->secondButton:Landroid/widget/Button;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public setBottomButtonOnClicked(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->secondButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;F)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->vectorView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz p1, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$integer;->noho_message_image_visibility:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    .line 102
    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setRotation(F)V

    goto :goto_0

    .line 106
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public setMessageText(I)V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    return-void
.end method

.method public setMessageText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitleText(I)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->titleView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    return-void
.end method

.method public setTopButton(I)V
    .locals 1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->firstButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->firstButton:Landroid/widget/Button;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 72
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->firstButton:Landroid/widget/Button;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public setTopButtonOnClicked(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->firstButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setVector(IF)V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 114
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$integer;->noho_message_image_visibility:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    .line 115
    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->vectorView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->vectorView:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setRotation(F)V

    .line 118
    iget-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->vectorView:Landroid/widget/ImageView;

    invoke-virtual {p2}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->vectorView:Landroid/widget/ImageView;

    .line 119
    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 118
    invoke-static {p2, p1, v0}, Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;->create(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;

    move-result-object p1

    .line 120
    iget-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->vectorView:Landroid/widget/ImageView;

    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
