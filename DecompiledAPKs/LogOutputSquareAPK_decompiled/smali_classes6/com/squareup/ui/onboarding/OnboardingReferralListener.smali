.class Lcom/squareup/ui/onboarding/OnboardingReferralListener;
.super Ljava/lang/Object;
.source "OnboardingReferralListener.java"

# interfaces
.implements Lcom/squareup/referrals/ReferralListener;


# instance fields
.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingReferralListener;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    return-void
.end method


# virtual methods
.method public onLoadFailure()V
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingReferralListener;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onReferralLaterOrLoadFailure()V

    return-void
.end method

.method public onUserDismissed()V
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingReferralListener;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onReferralLaterOrLoadFailure()V

    return-void
.end method
