.class public final Lcom/squareup/ui/onboarding/MainOnboardingStarter;
.super Ljava/lang/Object;
.source "MainOnboardingStarter.kt"

# interfaces
.implements Lcom/squareup/onboarding/OnboardingStarter;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\u0019B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0010\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u000fH\u0016J\u0010\u0010\u000b\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\rH\u0016J\u0008\u0010\u0017\u001a\u00020\u0018H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/MainOnboardingStarter;",
        "Lcom/squareup/onboarding/OnboardingStarter;",
        "onboardingType",
        "Lcom/squareup/onboarding/OnboardingType;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "onboardingWorkflowRunner",
        "Lcom/squareup/onboarding/OnboardingWorkflowRunner;",
        "onboardingActivityStarter",
        "Lcom/squareup/onboarding/OnboardingActivityStarter;",
        "(Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingWorkflowRunner;Lcom/squareup/onboarding/OnboardingActivityStarter;)V",
        "startActivation",
        "Lcom/jakewharton/rxrelay/PublishRelay;",
        "Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;",
        "doStartActivation",
        "",
        "method",
        "Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod;",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "params",
        "startArgs",
        "Lcom/squareup/onboarding/OnboardingFlow;",
        "OnboardingMethod",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final onboardingActivityStarter:Lcom/squareup/onboarding/OnboardingActivityStarter;

.field private final onboardingType:Lcom/squareup/onboarding/OnboardingType;

.field private final onboardingWorkflowRunner:Lcom/squareup/onboarding/OnboardingWorkflowRunner;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final startActivation:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingWorkflowRunner;Lcom/squareup/onboarding/OnboardingActivityStarter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "onboardingType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingWorkflowRunner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingActivityStarter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/MainOnboardingStarter;->onboardingType:Lcom/squareup/onboarding/OnboardingType;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/MainOnboardingStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p3, p0, Lcom/squareup/ui/onboarding/MainOnboardingStarter;->onboardingWorkflowRunner:Lcom/squareup/onboarding/OnboardingWorkflowRunner;

    iput-object p4, p0, Lcom/squareup/ui/onboarding/MainOnboardingStarter;->onboardingActivityStarter:Lcom/squareup/onboarding/OnboardingActivityStarter;

    .line 38
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/MainOnboardingStarter;->startActivation:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method

.method public static final synthetic access$doStartActivation(Lcom/squareup/ui/onboarding/MainOnboardingStarter;Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/MainOnboardingStarter;->doStartActivation(Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod;)V

    return-void
.end method

.method public static final synthetic access$getOnboardingType$p(Lcom/squareup/ui/onboarding/MainOnboardingStarter;)Lcom/squareup/onboarding/OnboardingType;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/ui/onboarding/MainOnboardingStarter;->onboardingType:Lcom/squareup/onboarding/OnboardingType;

    return-object p0
.end method

.method private final doStartActivation(Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod;)V
    .locals 1

    .line 61
    instance-of v0, p1, Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod$ServerDriven;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/onboarding/MainOnboardingStarter;->onboardingWorkflowRunner:Lcom/squareup/onboarding/OnboardingWorkflowRunner;

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/MainOnboardingStarter;->startArgs()Lcom/squareup/onboarding/OnboardingFlow;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/onboarding/OnboardingWorkflowRunner;->startWorkflow(Lcom/squareup/onboarding/OnboardingFlow;)V

    goto :goto_0

    .line 62
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod$LegacyMode;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/onboarding/MainOnboardingStarter;->onboardingActivityStarter:Lcom/squareup/onboarding/OnboardingActivityStarter;

    check-cast p1, Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod$LegacyMode;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod$LegacyMode;->getParams()Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/onboarding/OnboardingActivityStarter;->startOnboardingActivity(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)V

    :goto_0
    return-void

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final startArgs()Lcom/squareup/onboarding/OnboardingFlow;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MainOnboardingStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    invoke-static {}, Lcom/squareup/onboarding/OnboardingFlowKt;->getPOS_ONBOARDING()Lcom/squareup/onboarding/OnboardingFlow;

    move-result-object v0

    goto :goto_0

    .line 71
    :cond_0
    invoke-static {}, Lcom/squareup/onboarding/OnboardingFlowKt;->getLINK_BANK_ACCOUNT()Lcom/squareup/onboarding/OnboardingFlow;

    move-result-object v0

    :goto_0
    return-object v0
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MainOnboardingStarter;->startActivation:Lcom/jakewharton/rxrelay/PublishRelay;

    check-cast v0, Lrx/Observable;

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 42
    new-instance v1, Lcom/squareup/ui/onboarding/MainOnboardingStarter$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/MainOnboardingStarter$onEnterScope$1;-><init>(Lcom/squareup/ui/onboarding/MainOnboardingStarter;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 47
    new-instance v1, Lcom/squareup/ui/onboarding/MainOnboardingStarter$onEnterScope$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/onboarding/MainOnboardingStarter;

    invoke-direct {v1, v2}, Lcom/squareup/ui/onboarding/MainOnboardingStarter$onEnterScope$2;-><init>(Lcom/squareup/ui/onboarding/MainOnboardingStarter;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/onboarding/MainOnboardingStarter$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/onboarding/MainOnboardingStarter$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "startActivation.toV2Obse\u2026ribe(::doStartActivation)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public startActivation(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)V
    .locals 1

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MainOnboardingStarter;->startActivation:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
