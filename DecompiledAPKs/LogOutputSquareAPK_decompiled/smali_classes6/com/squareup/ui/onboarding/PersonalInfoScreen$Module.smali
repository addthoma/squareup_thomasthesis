.class public Lcom/squareup/ui/onboarding/PersonalInfoScreen$Module;
.super Ljava/lang/Object;
.source "PersonalInfoScreen.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ui/onboarding/ActivationCallModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/PersonalInfoScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideSsnStrings(Lcom/squareup/CountryCode;Ljava/util/Locale;)Lcom/squareup/ui/onboarding/SSNStrings;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 51
    invoke-static {p0, p1}, Lcom/squareup/ui/onboarding/SSNStrings;->getFor(Lcom/squareup/CountryCode;Ljava/util/Locale;)Lcom/squareup/ui/onboarding/SSNStrings;

    move-result-object p0

    return-object p0
.end method
