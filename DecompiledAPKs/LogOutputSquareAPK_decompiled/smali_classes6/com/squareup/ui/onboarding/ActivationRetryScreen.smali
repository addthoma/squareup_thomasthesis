.class public Lcom/squareup/ui/onboarding/ActivationRetryScreen;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "ActivationRetryScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/onboarding/ActivationRetryScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/ActivationRetryScreen$Component;,
        Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/ActivationRetryScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/ActivationRetryScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/ui/onboarding/ActivationRetryScreen;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/ActivationRetryScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/ActivationRetryScreen;->INSTANCE:Lcom/squareup/ui/onboarding/ActivationRetryScreen;

    .line 30
    sget-object v0, Lcom/squareup/ui/onboarding/ActivationRetryScreen;->INSTANCE:Lcom/squareup/ui/onboarding/ActivationRetryScreen;

    .line 31
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/ActivationRetryScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_RETRY:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 107
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->activation_retry_view:I

    return v0
.end method
