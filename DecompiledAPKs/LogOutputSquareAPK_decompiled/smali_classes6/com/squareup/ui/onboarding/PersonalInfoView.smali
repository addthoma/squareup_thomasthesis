.class public Lcom/squareup/ui/onboarding/PersonalInfoView;
.super Landroid/widget/LinearLayout;
.source "PersonalInfoView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private birthDateView:Landroid/widget/TextView;

.field private final confirmLaterPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

.field private final dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

.field private final failurePopup:Lcom/squareup/caller/FailurePopup;

.field private firstName:Landroid/widget/EditText;

.field private lastName:Landroid/widget/EditText;

.field private lastSsn:Landroid/widget/EditText;

.field private personalAddress:Lcom/squareup/address/AddressLayout;

.field private phoneNumber:Lcom/squareup/widgets/SelectableEditText;

.field phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field ssnStrings:Lcom/squareup/ui/onboarding/SSNStrings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final warningPopup:Lcom/squareup/flowlegacy/WarningPopup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    const-class p2, Lcom/squareup/ui/onboarding/PersonalInfoScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/onboarding/PersonalInfoScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/onboarding/PersonalInfoScreen$Component;->inject(Lcom/squareup/ui/onboarding/PersonalInfoView;)V

    .line 64
    new-instance p2, Lcom/squareup/flowlegacy/DateOfBirthPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/DateOfBirthPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

    .line 65
    new-instance p2, Lcom/squareup/flowlegacy/WarningPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/WarningPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    .line 66
    new-instance p2, Lcom/squareup/caller/FailurePopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/FailurePopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    .line 67
    new-instance p2, Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/ConfirmationPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->confirmLaterPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/onboarding/PersonalInfoView;)Landroid/widget/TextView;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->birthDateView:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/onboarding/PersonalInfoView;)Lcom/squareup/widgets/SelectableEditText;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->phoneNumber:Lcom/squareup/widgets/SelectableEditText;

    return-object p0
.end method

.method private bindAdvanceOnDone(Landroid/widget/EditText;)V
    .locals 1

    const/4 v0, 0x2

    .line 300
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 301
    new-instance v0, Lcom/squareup/ui/onboarding/PersonalInfoView$10;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/PersonalInfoView$10;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoView;Landroid/widget/EditText;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method static synthetic lambda$onFinishInflate$0(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .line 122
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    move-result p1

    if-nez p1, :cond_0

    .line 123
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    .line 124
    invoke-virtual {p0}, Landroid/view/View;->performClick()Z

    return v0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method


# virtual methods
.method public fieldsAreSet()Z
    .locals 5

    const/4 v0, 0x5

    new-array v0, v0, [Landroid/widget/TextView;

    .line 257
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->birthDateView:Landroid/widget/TextView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastSsn:Landroid/widget/EditText;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->firstName:Landroid/widget/EditText;

    const/4 v4, 0x2

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastName:Landroid/widget/EditText;

    const/4 v4, 0x3

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->phoneNumber:Lcom/squareup/widgets/SelectableEditText;

    const/4 v4, 0x4

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/squareup/util/Views;->isSet([Landroid/widget/TextView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->personalAddress:Lcom/squareup/address/AddressLayout;

    .line 258
    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method public focusBirthDate()V
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->birthDateView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    return-void
.end method

.method public focusFirstName()V
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->firstName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method

.method public focusLastSsn()V
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastSsn:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method

.method public focusPersonalAddress()V
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->personalAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->requestFocus()Z

    return-void
.end method

.method public focusPhoneNumber()V
    .locals 1

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->phoneNumber:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 314
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 315
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public getEmptyView()Landroid/view/View;
    .locals 3

    const/16 v0, 0x9

    new-array v0, v0, [Landroid/widget/TextView;

    .line 282
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->firstName:Landroid/widget/EditText;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastName:Landroid/widget/EditText;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->phoneNumber:Lcom/squareup/widgets/SelectableEditText;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->personalAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v1}, Lcom/squareup/address/AddressLayout;->getStreet()Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->personalAddress:Lcom/squareup/address/AddressLayout;

    .line 283
    invoke-virtual {v1}, Lcom/squareup/address/AddressLayout;->getPostal()Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->personalAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v1}, Lcom/squareup/address/AddressLayout;->getCity()Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->personalAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v1}, Lcom/squareup/address/AddressLayout;->getState()Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->birthDateView:Landroid/widget/TextView;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastSsn:Landroid/widget/EditText;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    .line 282
    invoke-static {v0}, Lcom/squareup/util/Views;->getEmptyView([Landroid/widget/TextView;)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 1

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->firstName:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastName:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastSsn()Ljava/lang/String;
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastSsn:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPersonalAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->personalAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getAddress()Lcom/squareup/address/Address;

    move-result-object v0

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->phoneNumber:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isBirthDateSet()Z
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->birthDateView:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/util/Views;->isSet(Landroid/widget/TextView;)Z

    move-result v0

    return v0
.end method

.method public isLastSsnSet()Z
    .locals 1

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastSsn:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->isSet(Landroid/widget/TextView;)Z

    move-result v0

    return v0
.end method

.method public isNameSet()Z
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/widget/TextView;

    .line 270
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->firstName:Landroid/widget/EditText;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastName:Landroid/widget/EditText;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Views;->isSet([Landroid/widget/TextView;)Z

    move-result v0

    return v0
.end method

.method public isPersonalAddressSet()Z
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->personalAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->isSet()Z

    move-result v0

    return v0
.end method

.method public isPhoneNumberSet()Z
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->phoneNumber:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->isSet(Landroid/widget/TextView;)Z

    move-result v0

    return v0
.end method

.method public onBackPressed()Z
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->confirmLaterPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->serverCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->dateOfBirthPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->dropView(Ljava/lang/Object;)V

    .line 177
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 71
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 73
    sget v0, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 74
    sget v1, Lcom/squareup/onboarding/flow/R$string;->personal_info_heading:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 75
    sget v0, Lcom/squareup/onboarding/flow/R$id;->subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 76
    sget v1, Lcom/squareup/onboarding/flow/R$string;->personal_info_subheading:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 78
    sget v0, Lcom/squareup/onboarding/flow/R$id;->personal_info_first_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->firstName:Landroid/widget/EditText;

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->firstName:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/onboarding/PersonalInfoView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/PersonalInfoView$1;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 85
    sget v0, Lcom/squareup/onboarding/flow/R$id;->personal_info_last_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastName:Landroid/widget/EditText;

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastName:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/onboarding/PersonalInfoView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/PersonalInfoView$2;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 92
    sget v0, Lcom/squareup/onboarding/flow/R$id;->personal_info_phone_number:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->phoneNumber:Lcom/squareup/widgets/SelectableEditText;

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->phoneNumber:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/text/ScrubbingTextWatcher;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    invoke-direct {v1, v2, v0}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->phoneNumber:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/onboarding/PersonalInfoView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/PersonalInfoView$3;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 100
    sget v0, Lcom/squareup/onboarding/flow/R$id;->personal_info_birth_date:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->birthDateView:Landroid/widget/TextView;

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->phoneNumber:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/onboarding/PersonalInfoView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/PersonalInfoView$4;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->birthDateView:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/onboarding/PersonalInfoView$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/PersonalInfoView$5;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->birthDateView:Landroid/widget/TextView;

    sget-object v1, Lcom/squareup/ui/onboarding/-$$Lambda$PersonalInfoView$0NwQqYNKVxuvePpmwKVPQTwpl4w;->INSTANCE:Lcom/squareup/ui/onboarding/-$$Lambda$PersonalInfoView$0NwQqYNKVxuvePpmwKVPQTwpl4w;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 130
    sget v0, Lcom/squareup/onboarding/flow/R$id;->personal_info_personal_address:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/address/AddressLayout;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->personalAddress:Lcom/squareup/address/AddressLayout;

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->personalAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getPostal()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->personalAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getPostal()Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/PersonalInfoView$6;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/PersonalInfoView$6;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->personalAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getStreet()Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/PersonalInfoView$7;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/PersonalInfoView$7;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->personalAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getPostal()Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/PersonalInfoView$8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/PersonalInfoView$8;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 152
    sget v0, Lcom/squareup/onboarding/flow/R$id;->personal_info_last_4_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 153
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->ssnStrings:Lcom/squareup/ui/onboarding/SSNStrings;

    iget v1, v1, Lcom/squareup/ui/onboarding/SSNStrings;->uppercase_header_last_4_ssn:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 155
    sget v0, Lcom/squareup/onboarding/flow/R$id;->personal_info_last_ssn:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastSsn:Landroid/widget/EditText;

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastSsn:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/PersonalInfoView;->bindAdvanceOnDone(Landroid/widget/EditText;)V

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastSsn:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/onboarding/PersonalInfoView$9;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/PersonalInfoView$9;-><init>(Lcom/squareup/ui/onboarding/PersonalInfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->takeView(Ljava/lang/Object;)V

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->dateOfBirthPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->serverCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->takeView(Ljava/lang/Object;)V

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->confirmLaterPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .line 193
    instance-of v0, p1, Lcom/squareup/mortar/BundleSavedState;

    if-eqz v0, :cond_1

    .line 194
    check-cast p1, Lcom/squareup/mortar/BundleSavedState;

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

    invoke-virtual {v0}, Lcom/squareup/flowlegacy/DateOfBirthPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

    iget-object v1, p1, Lcom/squareup/mortar/BundleSavedState;->bundle:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/DateOfBirthPopup;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 198
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/mortar/BundleSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 201
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 185
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 186
    iget-object v1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

    invoke-virtual {v1}, Lcom/squareup/flowlegacy/DateOfBirthPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    new-instance v1, Lcom/squareup/mortar/BundleSavedState;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->dateOfBirthPopup:Lcom/squareup/flowlegacy/DateOfBirthPopup;

    invoke-virtual {v2}, Lcom/squareup/flowlegacy/DateOfBirthPopup;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/squareup/mortar/BundleSavedState;-><init>(Landroid/os/Parcelable;Landroid/os/Bundle;)V

    return-object v1

    :cond_0
    return-object v0
.end method

.method public setBirthDateDebug(Lcom/squareup/ui/SquareDate;)V
    .locals 1

    .line 292
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->setBirthDate(Lcom/squareup/ui/SquareDate;)V

    return-void
.end method

.method protected setBirthDateText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->birthDateView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setFirstName(Ljava/lang/String;)V
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->firstName:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setLastName(Ljava/lang/String;)V
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->lastName:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->phoneNumber:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
