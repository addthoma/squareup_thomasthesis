.class public final Lcom/squareup/ui/onboarding/BusinessAddressAnalytics;
.super Ljava/lang/Object;
.source "BusinessAddressAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u001a\u000c\u0010\u0006\u001a\u00020\u0007*\u00020\u0008H\u0000\u001a\u000c\u0010\t\u001a\u00020\u0007*\u00020\u0008H\u0000\u001a\u000c\u0010\n\u001a\u00020\u0007*\u00020\u0008H\u0000\u001a\u000c\u0010\u000b\u001a\u00020\u0007*\u00020\u0008H\u0000\u001a\u000c\u0010\u000c\u001a\u00020\u0007*\u00020\u0008H\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "ONBOARD_DIFFERENT_ADDRESS_CONTINUE",
        "",
        "ONBOARD_MOBILE_ADDRESS_CONTINUE",
        "ONBOARD_SAME_ADDRESS_CONTINUE",
        "ONBOARD_VIEW_ADDRESS_MISSING_INFO",
        "ONBOARD_VIEW_INVALID_ADDRESS",
        "logContinueDifferent",
        "",
        "Lcom/squareup/analytics/Analytics;",
        "logContinueMobile",
        "logContinueSame",
        "logInvalidAddress",
        "logMissingInformation",
        "onboarding_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ONBOARD_DIFFERENT_ADDRESS_CONTINUE:Ljava/lang/String; = "Onboard: Activation Flow Business Address Different Address"

.field public static final ONBOARD_MOBILE_ADDRESS_CONTINUE:Ljava/lang/String; = "Onboard: Activation Flow Business Address Mobile Business"

.field public static final ONBOARD_SAME_ADDRESS_CONTINUE:Ljava/lang/String; = "Onboard: Activation Flow Business Address Same Address"

.field public static final ONBOARD_VIEW_ADDRESS_MISSING_INFO:Ljava/lang/String; = "Onboard: Business Address Missing Information"

.field public static final ONBOARD_VIEW_INVALID_ADDRESS:Ljava/lang/String; = "Onboard: Business Address Invalid Address"


# direct methods
.method public static final logContinueDifferent(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logContinueDifferent"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Onboard: Activation Flow Business Address Different Address"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logContinueMobile(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logContinueMobile"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Onboard: Activation Flow Business Address Mobile Business"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logContinueSame(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logContinueSame"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Onboard: Activation Flow Business Address Same Address"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logInvalidAddress(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logInvalidAddress"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance v0, Lcom/squareup/analytics/event/ViewEvent;

    const-string v1, "Onboard: Business Address Invalid Address"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logMissingInformation(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logMissingInformation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lcom/squareup/analytics/event/ViewEvent;

    const-string v1, "Onboard: Business Address Missing Information"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method
