.class public Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "MerchantSubcategoryScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Component;,
        Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;->INSTANCE:Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;

    .line 30
    sget-object v0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;->INSTANCE:Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;

    .line 31
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_SUBCATEGORY:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 97
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->merchant_subcategory_view:I

    return v0
.end method
