.class public Lcom/squareup/ui/onboarding/BusinessInfoScreen$Module;
.super Ljava/lang/Object;
.source "BusinessInfoScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/BusinessInfoScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static provideRevenueContainerKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;",
            ">;"
        }
    .end annotation

    .line 178
    const-class v0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$RevenueContainer;

    const-string v1, "revenueEntries"

    invoke-static {p0, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method
