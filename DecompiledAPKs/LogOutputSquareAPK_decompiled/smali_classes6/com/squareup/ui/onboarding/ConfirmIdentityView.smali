.class public Lcom/squareup/ui/onboarding/ConfirmIdentityView;
.super Landroid/widget/LinearLayout;
.source "ConfirmIdentityView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/ConfirmIdentityView$NextQuestionAnimation;
    }
.end annotation


# instance fields
.field private final decelerate:Landroid/view/animation/Interpolator;

.field private final failurePopup:Lcom/squareup/caller/FailurePopup;

.field private layoutContent:Landroid/widget/LinearLayout;

.field localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field questionGroups:[Lcom/squareup/widgets/CheckableGroup;

.field private scrollAnchors:[Landroid/view/View;

.field private scrollContent:Landroid/widget/ScrollView;

.field private final warningPopup:Lcom/squareup/flowlegacy/WarningPopup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->decelerate:Landroid/view/animation/Interpolator;

    .line 52
    const-class p2, Lcom/squareup/ui/onboarding/ConfirmIdentityScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/onboarding/ConfirmIdentityScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/onboarding/ConfirmIdentityScreen$Component;->inject(Lcom/squareup/ui/onboarding/ConfirmIdentityView;)V

    .line 54
    new-instance p2, Lcom/squareup/caller/FailurePopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/FailurePopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    .line 55
    new-instance p2, Lcom/squareup/flowlegacy/WarningPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/WarningPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/onboarding/ConfirmIdentityView;)Landroid/widget/ScrollView;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->scrollContent:Landroid/widget/ScrollView;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/onboarding/ConfirmIdentityView;)Landroid/view/animation/Interpolator;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->decelerate:Landroid/view/animation/Interpolator;

    return-object p0
.end method

.method private showNextQuestion()V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 157
    :goto_0
    iget-object v4, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->questionGroups:[Lcom/squareup/widgets/CheckableGroup;

    array-length v5, v4

    if-ge v2, v5, :cond_1

    .line 158
    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/squareup/widgets/CheckableGroup;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v2

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    if-ne v3, v1, :cond_2

    return-void

    :cond_2
    move v1, v3

    .line 163
    :goto_1
    iget-object v2, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->questionGroups:[Lcom/squareup/widgets/CheckableGroup;

    array-length v4, v2

    if-ge v1, v4, :cond_4

    .line 164
    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/squareup/widgets/CheckableGroup;->isChecked()Z

    move-result v2

    if-nez v2, :cond_3

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->scrollAnchors:[Landroid/view/View;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    :goto_2
    if-nez v0, :cond_6

    :goto_3
    if-ltz v3, :cond_6

    .line 171
    iget-object v1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->questionGroups:[Lcom/squareup/widgets/CheckableGroup;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lcom/squareup/widgets/CheckableGroup;->isChecked()Z

    move-result v1

    if-nez v1, :cond_5

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->scrollAnchors:[Landroid/view/View;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_4

    :cond_5
    add-int/lit8 v3, v3, -0x1

    goto :goto_3

    :cond_6
    :goto_4
    if-eqz v0, :cond_7

    .line 178
    new-instance v1, Lcom/squareup/ui/onboarding/ConfirmIdentityView$NextQuestionAnimation;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, Lcom/squareup/ui/onboarding/ConfirmIdentityView$NextQuestionAnimation;-><init>(Lcom/squareup/ui/onboarding/ConfirmIdentityView;ILcom/squareup/ui/onboarding/ConfirmIdentityView$1;)V

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->scrollContent:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_7
    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 184
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 185
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedAnswerIndices()[I
    .locals 4

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->questionGroups:[Lcom/squareup/widgets/CheckableGroup;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    new-array v0, v1, [I

    return-object v0

    .line 136
    :cond_0
    array-length v0, v0

    new-array v0, v0, [I

    .line 137
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->questionGroups:[Lcom/squareup/widgets/CheckableGroup;

    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 138
    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/squareup/widgets/CheckableGroup;->getCheckedId()I

    move-result v2

    aput v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public isEveryQuestionAnswered()Z
    .locals 4

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->questionGroups:[Lcom/squareup/widgets/CheckableGroup;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    .line 147
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->questionGroups:[Lcom/squareup/widgets/CheckableGroup;

    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 148
    aget-object v2, v2, v0

    .line 149
    invoke-virtual {v2}, Lcom/squareup/widgets/CheckableGroup;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic lambda$setQuestions$0$ConfirmIdentityView(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 88
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->isEveryQuestionAnswered()Z

    move-result p1

    if-nez p1, :cond_0

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->presenter:Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->onAnswerRadioTapped()V

    .line 90
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->showNextQuestion()V

    goto :goto_0

    .line 92
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->presenter:Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->onLastAnswerRadioTapped()V

    :goto_0
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->presenter:Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->presenter:Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->presenter:Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->updateCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->presenter:Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->dropView(Ljava/lang/Object;)V

    .line 78
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 59
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 61
    sget v0, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 62
    sget v1, Lcom/squareup/onboarding/flow/R$string;->confirm_identity_heading:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 63
    sget v0, Lcom/squareup/onboarding/flow/R$id;->subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 64
    sget v1, Lcom/squareup/onboarding/flow/R$string;->confirm_identity_subheading:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 65
    sget v0, Lcom/squareup/onboarding/flow/R$id;->scroll_content:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->scrollContent:Landroid/widget/ScrollView;

    .line 66
    sget v0, Lcom/squareup/onboarding/flow/R$id;->layout_content:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->layoutContent:Landroid/widget/LinearLayout;

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->presenter:Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->takeView(Ljava/lang/Object;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->presenter:Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->updateCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->takeView(Ljava/lang/Object;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->presenter:Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setQuestions([Lcom/squareup/server/activation/QuizQuestion;[I)V
    .locals 9

    .line 87
    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$ConfirmIdentityView$EWDFD-6fegvjFXdAVH0pl-ULRnE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ConfirmIdentityView$EWDFD-6fegvjFXdAVH0pl-ULRnE;-><init>(Lcom/squareup/ui/onboarding/ConfirmIdentityView;)V

    .line 96
    array-length v1, p1

    new-array v1, v1, [Lcom/squareup/widgets/CheckableGroup;

    iput-object v1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->questionGroups:[Lcom/squareup/widgets/CheckableGroup;

    .line 97
    iget-object v1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->questionGroups:[Lcom/squareup/widgets/CheckableGroup;

    array-length v1, v1

    new-array v1, v1, [Landroid/view/View;

    iput-object v1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->scrollAnchors:[Landroid/view/View;

    .line 98
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 99
    :goto_0
    iget-object v4, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->questionGroups:[Lcom/squareup/widgets/CheckableGroup;

    array-length v5, v4

    if-ge v3, v5, :cond_1

    .line 101
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/squareup/onboarding/flow/R$layout;->confirm_identity_question:I

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 102
    sget v5, Lcom/squareup/onboarding/flow/R$id;->question_radios:I

    invoke-static {v4, v5}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/squareup/widgets/CheckableGroup;

    .line 103
    aget-object v6, p1, v3

    .line 104
    iget-object v7, v6, Lcom/squareup/server/activation/QuizQuestion;->answers:[Ljava/lang/String;

    const/4 v8, 0x1

    invoke-static {v1, v5, v7, v8}, Lcom/squareup/ui/CheckableGroups;->addAsRows(Landroid/view/LayoutInflater;Lcom/squareup/widgets/CheckableGroup;[Ljava/lang/Object;Z)V

    .line 105
    invoke-virtual {v5, v0}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 106
    iget-object v7, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->questionGroups:[Lcom/squareup/widgets/CheckableGroup;

    aput-object v5, v7, v3

    if-nez v3, :cond_0

    .line 109
    sget v5, Lcom/squareup/onboarding/flow/R$id;->question_divider:I

    invoke-static {v4, v5}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    const/16 v7, 0x8

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 111
    :cond_0
    sget v5, Lcom/squareup/onboarding/flow/R$id;->question_title:I

    invoke-static {v4, v5}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 112
    iget-object v6, v6, Lcom/squareup/server/activation/QuizQuestion;->text:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v5, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->scrollAnchors:[Landroid/view/View;

    aput-object v4, v5, v3

    .line 116
    iget-object v5, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->layoutContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    .line 120
    array-length p1, p2

    array-length v0, v4

    if-ne p1, v0, :cond_2

    .line 121
    :goto_1
    array-length p1, p2

    if-ge v2, p1, :cond_2

    .line 122
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->questionGroups:[Lcom/squareup/widgets/CheckableGroup;

    aget-object p1, p1, v2

    aget v0, p2, v2

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method
