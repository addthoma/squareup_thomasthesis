.class Lcom/squareup/ui/onboarding/PersonalInfoPresenter$1;
.super Lcom/squareup/mortar/PopupPresenter;
.source "PersonalInfoPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/PersonalInfoPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/ui/SquareDate;",
        "Lcom/squareup/ui/SquareDate;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/PersonalInfoPresenter;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter$1;->this$0:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPopupResult(Lcom/squareup/ui/SquareDate;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter$1;->this$0:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;->setBirthDate(Lcom/squareup/ui/SquareDate;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 54
    check-cast p1, Lcom/squareup/ui/SquareDate;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter$1;->onPopupResult(Lcom/squareup/ui/SquareDate;)V

    return-void
.end method
