.class public final Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;
.super Ljava/lang/Object;
.source "MoveTicketScreen_Module_ProvideTicketActionSessionFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/TicketActionScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final inEditModeKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

.field private final moveTicketListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketSelectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final voidControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/compvoidcontroller/VoidController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/compvoidcontroller/VoidController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;)V"
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    .line 63
    iput-object p2, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->ticketsLoaderProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p3, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->ticketSelectionProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p4, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->ticketsProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p5, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->moveTicketListenerProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p6, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p7, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->voidControllerProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p8, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->transactionProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p9, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->mainContainerProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p10, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->resProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p11, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->inEditModeKeyProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p12, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p13, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->ticketStoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/compvoidcontroller/VoidController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;)",
            "Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;"
        }
    .end annotation

    .line 92
    new-instance v14, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;-><init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static provideTicketActionSession(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/tickets/Tickets;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/compvoidcontroller/VoidController;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/util/Res;Lcom/squareup/BundleKey;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/ui/ticket/TicketActionScopeRunner;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            "Lcom/squareup/tickets/Tickets;",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/compvoidcontroller/VoidController;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            "Lcom/squareup/tickets/TicketStore;",
            ")",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner;"
        }
    .end annotation

    .line 102
    invoke-virtual/range {p0 .. p12}, Lcom/squareup/ui/ticket/MoveTicketScreen$Module;->provideTicketActionSession(Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/tickets/Tickets;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/compvoidcontroller/VoidController;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/util/Res;Lcom/squareup/BundleKey;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/TicketActionScopeRunner;
    .locals 13

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->ticketsLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ticket/TicketsLoader;

    iget-object v2, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->ticketSelectionProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/ticket/TicketSelection;

    iget-object v3, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/tickets/Tickets;

    iget-object v4, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->moveTicketListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

    iget-object v5, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v6, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->voidControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/compvoidcontroller/VoidController;

    iget-object v7, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/payment/Transaction;

    iget-object v8, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v8}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/ui/main/PosContainer;

    iget-object v9, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v9}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/util/Res;

    iget-object v10, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->inEditModeKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v10}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/BundleKey;

    iget-object v11, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v11}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v12, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->ticketStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v12}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/tickets/TicketStore;

    invoke-static/range {v0 .. v12}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->provideTicketActionSession(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/tickets/Tickets;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/compvoidcontroller/VoidController;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/util/Res;Lcom/squareup/BundleKey;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketActionSessionFactory;->get()Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    move-result-object v0

    return-object v0
.end method
