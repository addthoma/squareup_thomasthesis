.class public Lcom/squareup/ui/ticket/TicketListView;
.super Lcom/squareup/ui/ticket/BaseTicketListView;
.source "TicketListView.java"


# instance fields
.field presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/BaseTicketListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListView;->isInEditMode()Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    .line 21
    :cond_0
    const-class p2, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$Component;->inject(Lcom/squareup/ui/ticket/TicketListView;)V

    return-void
.end method


# virtual methods
.method protected onBindTicketRowTablet(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;Landroid/widget/CompoundButton;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isMergingWithTransactionTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    .line 45
    invoke-virtual {p2, p1}, Landroid/widget/CompoundButton;->setVisibility(I)V

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 42
    invoke-virtual {p2, v0}, Landroid/widget/CompoundButton;->setVisibility(I)V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/TicketSelection;->ticketInfoFromRow(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;)Lcom/squareup/ui/ticket/TicketInfo;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isTicketSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z

    move-result p1

    invoke-virtual {p2, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_1
    return-void
.end method

.method protected onHideProgress()V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->onTicketsProgressHidden()V

    return-void
.end method

.method protected onNewTicketClicked(Landroid/view/View;)V
    .locals 0

    .line 25
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->onNewTicketClicked()V

    return-void
.end method

.method protected onStartSearch(Ljava/lang/String;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->onStartSearch(Ljava/lang/String;)V

    return-void
.end method

.method protected onTicketClicked(Landroid/widget/CompoundButton;Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->onTicketClicked(Lcom/squareup/ui/ticket/TicketInfo;)V

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isTicketSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :cond_0
    return-void
.end method
