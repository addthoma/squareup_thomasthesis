.class public interface abstract Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Component;
.super Ljava/lang/Object;
.source "TicketBulkDeleteDialogScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract hudToaster()Lcom/squareup/hudtoaster/HudToaster;
.end method

.method public abstract openTicketsRunner()Lcom/squareup/ui/ticket/OpenTicketsRunner;
.end method

.method public abstract ticketActionSession()Lcom/squareup/ui/ticket/TicketActionScopeRunner;
.end method
