.class Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoResultsViewHolder;
.super Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$BaseViewHolder;
.source "NewTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NoResultsViewHolder"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 124
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoResultsViewHolder;->this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    .line 125
    sget v0, Lcom/squareup/librarylist/R$layout;->no_search_results:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$BaseViewHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;I)V

    .line 126
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoResultsViewHolder;->itemView:Landroid/view/View;

    sget p2, Lcom/squareup/librarylist/R$id;->no_search_results:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/NewTicketView$1;)V
    .locals 0

    .line 122
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$NoResultsViewHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;)V

    return-void
.end method
