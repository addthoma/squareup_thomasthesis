.class Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;
.super Lcom/squareup/ui/RangerRecyclerAdapter;
.source "GroupListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/GroupListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GroupListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;,
        Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$CustomTicketsRowHolder;,
        Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$AllTicketsRowHolder;,
        Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$SearchRowHolder;,
        Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/GroupListView$GroupRow;",
        "Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final ALL_TICKETS_ROW_POSITION:I = 0x1

.field private static final SEARCH_ROW_POSITION:I = 0x0

.field private static final TOP_ROW_COUNT:I = 0x2


# instance fields
.field private final groupRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

.field private selectedSectionPosition:I

.field private ticketGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/GroupListPresenter;Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1

    .line 221
    const-class v0, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    invoke-direct {p0, v0}, Lcom/squareup/ui/RangerRecyclerAdapter;-><init>(Ljava/lang/Class;)V

    .line 222
    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    .line 223
    iput-object p2, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->groupRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 224
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->ticketGroups:Ljava/util/List;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;)Lcom/squareup/ui/ticket/GroupListPresenter;
    .locals 0

    .line 71
    iget-object p0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;)Ljava/util/List;
    .locals 0

    .line 71
    iget-object p0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->ticketGroups:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Lcom/squareup/ui/Ranger;Ljava/util/List;)V
    .locals 0

    .line 71
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->setList(Lcom/squareup/ui/Ranger;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;)I
    .locals 0

    .line 71
    iget p0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->selectedSectionPosition:I

    return p0
.end method

.method private initSelectedSectionPosition()V
    .locals 6

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/GroupListPresenter;->getSelectedSection()Ljava/lang/String;

    move-result-object v0

    .line 262
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x5300cf84

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eq v1, v2, :cond_2

    const v2, -0x50eee4a1

    if-eq v1, v2, :cond_1

    const v2, 0x7668fc67

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "group-list-presenter-selected-section-all-tickets"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const-string v1, "group-list-presenter-selected-section-custom-tickets"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x2

    goto :goto_1

    :cond_2
    const-string v1, "group-list-presenter-selected-section-search"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v1, -0x1

    :goto_1
    if-eqz v1, :cond_8

    if-eq v1, v5, :cond_7

    if-eq v1, v4, :cond_6

    .line 274
    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->ticketGroups:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 275
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 276
    iput v4, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->selectedSectionPosition:I

    return-void

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 287
    :cond_5
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/GroupListPresenter;->selectAllTicketsSection()V

    .line 288
    iput v5, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->selectedSectionPosition:I

    .line 289
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->groupRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget v1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->selectedSectionPosition:I

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    goto :goto_3

    .line 270
    :cond_6
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->ticketGroups:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v4

    iput v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->selectedSectionPosition:I

    goto :goto_3

    .line 267
    :cond_7
    iput v5, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->selectedSectionPosition:I

    goto :goto_3

    .line 264
    :cond_8
    iput v3, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->selectedSectionPosition:I

    :goto_3
    return-void
.end method

.method private setList(Lcom/squareup/ui/Ranger;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/Ranger<",
            "Lcom/squareup/ui/ticket/GroupListView$GroupRow;",
            "Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;)V"
        }
    .end annotation

    .line 254
    iput-object p2, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->ticketGroups:Ljava/util/List;

    .line 255
    invoke-direct {p0}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->initSelectedSectionPosition()V

    .line 256
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->setRanger(Lcom/squareup/ui/Ranger;)V

    return-void
.end method


# virtual methods
.method isSectionSelected(I)Z
    .locals 1

    .line 249
    iget v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->selectedSectionPosition:I

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;",
            ")",
            "Lcom/squareup/ui/RangerRecyclerAdapter<",
            "Lcom/squareup/ui/ticket/GroupListView$GroupRow;",
            "Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;",
            ">.RangerHolder;"
        }
    .end annotation

    .line 229
    sget-object v0, Lcom/squareup/ui/ticket/GroupListView$2;->$SwitchMap$com$squareup$ui$ticket$GroupListView$GroupViewHolder:[I

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 237
    new-instance p2, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;

    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-direct {p2, p0, p1, p0, v0}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;-><init>(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Lcom/squareup/ui/ticket/GroupListPresenter;)V

    return-object p2

    .line 239
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No holder defined for HolderType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 235
    :cond_1
    new-instance p2, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$CustomTicketsRowHolder;

    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-direct {p2, p0, p1, p0, v0}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$CustomTicketsRowHolder;-><init>(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Lcom/squareup/ui/ticket/GroupListPresenter;)V

    return-object p2

    .line 233
    :cond_2
    new-instance p2, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$AllTicketsRowHolder;

    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-direct {p2, p0, p1, p0, v0}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$AllTicketsRowHolder;-><init>(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Lcom/squareup/ui/ticket/GroupListPresenter;)V

    return-object p2

    .line 231
    :cond_3
    new-instance p2, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$SearchRowHolder;

    invoke-direct {p2, p0, p1, p0}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$SearchRowHolder;-><init>(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;)V

    return-object p2
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;Ljava/lang/Enum;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
    .locals 0

    .line 71
    check-cast p2, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;

    move-result-object p1

    return-object p1
.end method

.method setSelectedSectionPosition(I)V
    .locals 0

    .line 244
    iput p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->selectedSectionPosition:I

    .line 245
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->notifyDataSetChanged()V

    return-void
.end method
