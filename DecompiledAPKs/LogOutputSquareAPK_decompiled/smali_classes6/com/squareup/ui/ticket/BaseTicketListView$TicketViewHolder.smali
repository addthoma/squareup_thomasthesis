.class final enum Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;
.super Ljava/lang/Enum;
.source "BaseTicketListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/BaseTicketListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "TicketViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

.field public static final enum BUTTON_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

.field public static final enum ERROR_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

.field public static final enum NO_RESULTS_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

.field public static final enum NO_TICKETS_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

.field public static final enum SECTION_HEADER_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

.field public static final enum SORT_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

.field public static final enum TEMPLATE_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

.field public static final enum TEXT_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

.field public static final enum TICKET_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 100
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v1, 0x0

    const-string v2, "TICKET_HOLDER"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->TICKET_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    .line 101
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v2, 0x1

    const-string v3, "TEMPLATE_HOLDER"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->TEMPLATE_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    .line 102
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v3, 0x2

    const-string v4, "TEXT_HOLDER"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->TEXT_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    .line 103
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v4, 0x3

    const-string v5, "SORT_HOLDER"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->SORT_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    .line 104
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v5, 0x4

    const-string v6, "BUTTON_HOLDER"

    invoke-direct {v0, v6, v5}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->BUTTON_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    .line 105
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v6, 0x5

    const-string v7, "SECTION_HEADER_HOLDER"

    invoke-direct {v0, v7, v6}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->SECTION_HEADER_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    .line 106
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v7, 0x6

    const-string v8, "NO_TICKETS_HOLDER"

    invoke-direct {v0, v8, v7}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->NO_TICKETS_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    .line 107
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v8, 0x7

    const-string v9, "NO_RESULTS_HOLDER"

    invoke-direct {v0, v9, v8}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->NO_RESULTS_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    .line 108
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/16 v9, 0x8

    const-string v10, "ERROR_HOLDER"

    invoke-direct {v0, v10, v9}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->ERROR_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    .line 99
    sget-object v10, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->TICKET_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->TEMPLATE_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->TEXT_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->SORT_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->BUTTON_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->SECTION_HEADER_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->NO_TICKETS_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->NO_RESULTS_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->ERROR_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->$VALUES:[Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 99
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;
    .locals 1

    .line 99
    const-class v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;
    .locals 1

    .line 99
    sget-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->$VALUES:[Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    invoke-virtual {v0}, [Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    return-object v0
.end method
