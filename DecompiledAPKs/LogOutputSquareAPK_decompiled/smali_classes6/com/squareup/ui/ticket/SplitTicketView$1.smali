.class Lcom/squareup/ui/ticket/SplitTicketView$1;
.super Ljava/lang/Object;
.source "SplitTicketView.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/SplitTicketView;->scrollTicketIntoView(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/SplitTicketView;

.field final synthetic val$index:I

.field final synthetic val$ticketView:Lcom/squareup/ui/ticket/TicketView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/SplitTicketView;Lcom/squareup/ui/ticket/TicketView;I)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketView$1;->this$0:Lcom/squareup/ui/ticket/SplitTicketView;

    iput-object p2, p0, Lcom/squareup/ui/ticket/SplitTicketView$1;->val$ticketView:Lcom/squareup/ui/ticket/TicketView;

    iput p3, p0, Lcom/squareup/ui/ticket/SplitTicketView$1;->val$index:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 10

    move-object v0, p0

    .line 103
    iget-object v1, v0, Lcom/squareup/ui/ticket/SplitTicketView$1;->val$ticketView:Lcom/squareup/ui/ticket/TicketView;

    invoke-virtual {v1, p0}, Lcom/squareup/ui/ticket/TicketView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 104
    iget-object v1, v0, Lcom/squareup/ui/ticket/SplitTicketView$1;->this$0:Lcom/squareup/ui/ticket/SplitTicketView;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketView;->access$000(Lcom/squareup/ui/ticket/SplitTicketView;)I

    move-result v2

    iget-object v1, v0, Lcom/squareup/ui/ticket/SplitTicketView$1;->this$0:Lcom/squareup/ui/ticket/SplitTicketView;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketView;->access$100(Lcom/squareup/ui/ticket/SplitTicketView;)Lcom/squareup/ui/NullStateHorizontalScrollView;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/ui/ticket/SplitTicketView$1;->this$0:Lcom/squareup/ui/ticket/SplitTicketView;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketView;->access$200(Lcom/squareup/ui/ticket/SplitTicketView;)Ljava/util/Map;

    move-result-object v4

    iget v5, v0, Lcom/squareup/ui/ticket/SplitTicketView$1;->val$index:I

    move v6, p2

    move v7, p4

    move/from16 v8, p6

    move/from16 v9, p8

    invoke-static/range {v2 .. v9}, Lcom/squareup/ui/ticket/SplitTicketView;->doScrollTicketIntoView(ILcom/squareup/ui/NullStateHorizontalScrollView;Ljava/util/Map;IIIII)V

    return-void
.end method
