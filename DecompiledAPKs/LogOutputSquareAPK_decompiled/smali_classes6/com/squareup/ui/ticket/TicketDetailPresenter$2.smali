.class Lcom/squareup/ui/ticket/TicketDetailPresenter$2;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "TicketDetailPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/TicketDetailPresenter;->onCompClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/TicketDetailPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/TicketDetailPresenter;)V
    .locals 0

    .line 204
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter$2;->this$0:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 6

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter$2;->this$0:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->access$100(Lcom/squareup/ui/ticket/TicketDetailPresenter;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COMP_CART_STARTED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter$2;->this$0:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/ticket/TicketCompScreen;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter$2;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter$2;->this$0:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-static {v3}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->access$200(Lcom/squareup/ui/ticket/TicketDetailPresenter;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/ticket/EditTicketState;->getName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter$2;->this$0:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    .line 208
    invoke-static {v4}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->access$200(Lcom/squareup/ui/ticket/TicketDetailPresenter;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/ui/ticket/EditTicketState;->getNote()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter$2;->this$0:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-static {v5}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->access$200(Lcom/squareup/ui/ticket/TicketDetailPresenter;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/ui/ticket/EditTicketState;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/squareup/ui/ticket/TicketCompScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    .line 207
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
