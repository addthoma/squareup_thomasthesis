.class public final Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;
.super Ljava/lang/Object;
.source "MoveTicketScreen_Module_ProvideConfigurationFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final displayModeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketModeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsToMoveProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    .line 42
    iput-object p2, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->displayModeProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->ticketModeProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->ticketsToMoveProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->transactionProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->deviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;"
        }
    .end annotation

    .line 62
    new-instance v8, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;-><init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static provideConfiguration(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Ljava/lang/Object;Ljava/util/List;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Device;)Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Device;",
            ")",
            "Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;"
        }
    .end annotation

    .line 69
    move-object v2, p2

    check-cast v2, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/ui/ticket/MoveTicketScreen$Module;->provideConfiguration(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;Ljava/util/List;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Device;)Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;
    .locals 7

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->displayModeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    iget-object v2, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->ticketModeProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->ticketsToMoveProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v5, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/payment/Transaction;

    iget-object v6, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/util/Device;

    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->provideConfiguration(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Ljava/lang/Object;Ljava/util/List;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Device;)Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideConfigurationFactory;->get()Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    move-result-object v0

    return-object v0
.end method
