.class public final Lcom/squareup/ui/ticket/api/NoOpenTicketsHomeScreenSelector;
.super Ljava/lang/Object;
.source "NoOpenTicketsHomeScreenSelector.kt"

# interfaces
.implements Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u0006H\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/ticket/api/NoOpenTicketsHomeScreenSelector;",
        "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
        "()V",
        "isOpenTicketsHomeScreen",
        "",
        "screen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "leavingSplitTicketScreen",
        "traversal",
        "Lflow/Traversal;",
        "updateHomeForOpenTickets",
        "Lflow/History;",
        "currentHistory",
        "openTicketsParentScreen",
        "impl-noop_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isOpenTicketsHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method public leavingSplitTicketScreen(Lflow/Traversal;)Z
    .locals 1

    const-string v0, "traversal"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method public updateHomeForOpenTickets(Lflow/History;Lcom/squareup/container/ContainerTreeKey;)Lflow/History;
    .locals 1

    const-string v0, "currentHistory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "openTicketsParentScreen"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
