.class public abstract Lcom/squareup/ui/ticket/api/NoOpenTicketsModule;
.super Ljava/lang/Object;
.source "NoOpenTicketsModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/compvoidcontroller/NoopCompVoidControllerModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideHomeScreenSelector(Lcom/squareup/ui/main/NoopHomeScreenSelector;)Lcom/squareup/ui/main/HomeScreenSelector;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideOpenTicketsHomeScreenSelector(Lcom/squareup/ui/ticket/api/NoOpenTicketsHomeScreenSelector;)Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTicketCardNameHandler(Lcom/squareup/tickets/NoOpTicketCardNameHandler;)Lcom/squareup/tickets/TicketCardNameHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
