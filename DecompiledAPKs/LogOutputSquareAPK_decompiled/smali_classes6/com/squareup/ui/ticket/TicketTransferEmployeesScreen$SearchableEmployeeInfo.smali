.class final Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;
.super Ljava/lang/Object;
.source "TicketTransferEmployeesScreen.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SearchableEmployeeInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final displayName:Ljava/lang/String;

.field final info:Lcom/squareup/permissions/EmployeeInfo;

.field final searchableDisplayName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/permissions/EmployeeInfo;Lcom/squareup/util/Res;)V
    .locals 0

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;->info:Lcom/squareup/permissions/EmployeeInfo;

    .line 93
    invoke-static {p2, p1}, Lcom/squareup/permissions/Employee;->getShortDisplayName(Lcom/squareup/util/Res;Lcom/squareup/permissions/EmployeeInfo;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;->displayName:Ljava/lang/String;

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;->displayName:Ljava/lang/String;

    sget-object p2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, p2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;->searchableDisplayName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;)I
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;->displayName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;->displayName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 86
    check-cast p1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;->compareTo(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;)I

    move-result p1

    return p1
.end method
