.class public final Lcom/squareup/ui/ticket/MoveTicketView_MembersInjector;
.super Ljava/lang/Object;
.source "MoveTicketView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/ticket/MoveTicketView;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider2:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/ui/ticket/MoveTicketView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/ui/ticket/MoveTicketView_MembersInjector;->presenterProvider2:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/ui/ticket/MoveTicketView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/ticket/MoveTicketView;",
            ">;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/ui/ticket/MoveTicketView_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/ticket/MoveTicketView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/ticket/MoveTicketView;Lcom/squareup/ui/ticket/MoveTicketPresenter;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketView;->presenter:Lcom/squareup/ui/ticket/MoveTicketPresenter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/ticket/MoveTicketView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketView;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/ticket/MoveTicketView;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/ticket/BaseTicketListView_MembersInjector;->injectPresenter(Lcom/squareup/ui/ticket/BaseTicketListView;Lcom/squareup/ui/ticket/TicketListPresenter;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/ticket/BaseTicketListView_MembersInjector;->injectDevice(Lcom/squareup/ui/ticket/BaseTicketListView;Lcom/squareup/util/Device;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView_MembersInjector;->presenterProvider2:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/ticket/MoveTicketView_MembersInjector;->injectPresenter(Lcom/squareup/ui/ticket/MoveTicketView;Lcom/squareup/ui/ticket/MoveTicketPresenter;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/ticket/MoveTicketView_MembersInjector;->injectRes(Lcom/squareup/ui/ticket/MoveTicketView;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/ticket/MoveTicketView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/MoveTicketView_MembersInjector;->injectMembers(Lcom/squareup/ui/ticket/MoveTicketView;)V

    return-void
.end method
