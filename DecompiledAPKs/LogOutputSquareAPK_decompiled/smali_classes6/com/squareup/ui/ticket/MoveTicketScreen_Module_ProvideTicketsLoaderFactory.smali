.class public final Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;
.super Ljava/lang/Object;
.source "MoveTicketScreen_Module_ProvideTicketsLoaderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/TicketsLoader;",
        ">;"
    }
.end annotation


# instance fields
.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

.field private final ticketsListSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsListScheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsListScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;->ticketsProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;->ticketsListSchedulerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;->ticketsLoggerProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsListScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;)",
            "Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;"
        }
    .end annotation

    .line 53
    new-instance v6, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;-><init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static provideTicketsLoader(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/connectivity/ConnectivityMonitor;)Lcom/squareup/ui/ticket/TicketsLoader;
    .locals 0

    .line 59
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/ticket/MoveTicketScreen$Module;->provideTicketsLoader(Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/connectivity/ConnectivityMonitor;)Lcom/squareup/ui/ticket/TicketsLoader;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/TicketsLoader;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/TicketsLoader;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tickets/Tickets;

    iget-object v2, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;->ticketsListSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/opentickets/TicketsListScheduler;

    iget-object v3, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;->ticketsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/log/tickets/OpenTicketsLogger;

    iget-object v4, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;->provideTicketsLoader(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/connectivity/ConnectivityMonitor;)Lcom/squareup/ui/ticket/TicketsLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsLoaderFactory;->get()Lcom/squareup/ui/ticket/TicketsLoader;

    move-result-object v0

    return-object v0
.end method
