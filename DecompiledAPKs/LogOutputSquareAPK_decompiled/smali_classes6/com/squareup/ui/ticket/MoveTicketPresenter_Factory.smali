.class public final Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;
.super Ljava/lang/Object;
.source "MoveTicketPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/MoveTicketPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final moveTicketListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketActionScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketGroupsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketListListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsToMoveProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 76
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->ticketGroupsCacheProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 77
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->ticketPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 78
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->ticketActionScopeRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 79
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->ticketsToMoveProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 80
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->ticketsProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 81
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->ticketListListenerProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 82
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->moveTicketListenerProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 83
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 84
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->openTicketsRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;)",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 115
    new-instance v18, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v18
.end method

.method public static newInstance(Lcom/squareup/tickets/TicketGroupsCache;Lcom/squareup/ui/ticket/TicketListPresenter;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Ljava/util/List;Lcom/squareup/tickets/Tickets;Ljava/lang/Object;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/payment/Transaction;Lflow/Flow;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;)Lcom/squareup/ui/ticket/MoveTicketPresenter;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            "Lcom/squareup/ui/ticket/TicketListPresenter;",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;",
            "Lcom/squareup/tickets/Tickets;",
            "Ljava/lang/Object;",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
            "Lcom/squareup/payment/Transaction;",
            "Lflow/Flow;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ")",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 126
    new-instance v18, Lcom/squareup/ui/ticket/MoveTicketPresenter;

    move-object/from16 v0, v18

    move-object/from16 v6, p5

    check-cast v6, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

    invoke-direct/range {v0 .. v17}, Lcom/squareup/ui/ticket/MoveTicketPresenter;-><init>(Lcom/squareup/tickets/TicketGroupsCache;Lcom/squareup/ui/ticket/TicketListPresenter;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Ljava/util/List;Lcom/squareup/tickets/Tickets;Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/payment/Transaction;Lflow/Flow;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;)V

    return-object v18
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/MoveTicketPresenter;
    .locals 19

    move-object/from16 v0, p0

    .line 97
    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->ticketGroupsCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/tickets/TicketGroupsCache;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->ticketPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/ui/ticket/TicketListPresenter;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->ticketActionScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->ticketsToMoveProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Ljava/util/List;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/tickets/Tickets;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->ticketListListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->moveTicketListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->openTicketsRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/ticket/OpenTicketsRunner;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/thread/executor/MainThread;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/hudtoaster/HudToaster;

    invoke-static/range {v2 .. v18}, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->newInstance(Lcom/squareup/tickets/TicketGroupsCache;Lcom/squareup/ui/ticket/TicketListPresenter;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Ljava/util/List;Lcom/squareup/tickets/Tickets;Ljava/lang/Object;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/payment/Transaction;Lflow/Flow;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;)Lcom/squareup/ui/ticket/MoveTicketPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketPresenter_Factory;->get()Lcom/squareup/ui/ticket/MoveTicketPresenter;

    move-result-object v0

    return-object v0
.end method
