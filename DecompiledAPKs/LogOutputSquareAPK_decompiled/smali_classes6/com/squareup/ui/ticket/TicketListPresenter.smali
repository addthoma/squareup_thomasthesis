.class public Lcom/squareup/ui/ticket/TicketListPresenter;
.super Lmortar/ViewPresenter;
.source "TicketListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/TicketListPresenter$Component;,
        Lcom/squareup/ui/ticket/TicketListPresenter$Module;,
        Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;,
        Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;,
        Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/ticket/BaseTicketListView;",
        ">;"
    }
.end annotation


# instance fields
.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final device:Lcom/squareup/util/Device;

.field private final employeeFilteringEnabled:Z

.field private employeeTokenOrNull:Ljava/lang/String;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private permittedToViewAllTickets:Z

.field private final predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

.field private final readyToShowTicketList:Lkotlin/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/Pair<",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private selectionFilter:Ljava/lang/String;

.field private ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

.field private final ticketListListener:Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

.field private final ticketScopeRunner:Lcom/squareup/ui/ticket/TicketScopeRunner;

.field private final ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

.field private final ticketSortSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketTemplates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;"
        }
    .end annotation
.end field

.field private ticketTotals:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

.field private ticketsRefreshedCallback:Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .line 130
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 131
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    .line 132
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

    .line 133
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    .line 134
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    .line 135
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketScopeRunner:Lcom/squareup/ui/ticket/TicketScopeRunner;

    .line 136
    iput-object p6, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSortSetting:Lcom/squareup/settings/LocalSetting;

    .line 137
    iput-object p7, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketListListener:Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

    .line 138
    iput-object p9, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->passcodeGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 139
    iput-object p10, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    .line 140
    iput-object p11, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->device:Lcom/squareup/util/Device;

    .line 141
    iput-object p12, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 142
    iput-object p13, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 143
    iput-object p14, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->res:Lcom/squareup/util/Res;

    .line 145
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketTemplates:Ljava/util/List;

    .line 146
    invoke-interface {p8}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEmployeeFilteringEnabled()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->employeeFilteringEnabled:Z

    const-string p1, "group-list-presenter-selected-section-all-tickets"

    .line 147
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->selectionFilter:Ljava/lang/String;

    .line 148
    new-instance p1, Lkotlin/Pair;

    new-instance p2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    new-instance p3, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    invoke-direct {p1, p2, p3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->readyToShowTicketList:Lkotlin/Pair;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/TicketListPresenter;)Lcom/squareup/ui/ticket/TicketScopeRunner;
    .locals 0

    .line 94
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketScopeRunner:Lcom/squareup/ui/ticket/TicketScopeRunner;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/TicketListPresenter;)Lcom/squareup/util/Res;
    .locals 0

    .line 94
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method private buildRanger()Lcom/squareup/ui/Ranger;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/ui/Ranger<",
            "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
            "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
            ">;"
        }
    .end annotation

    .line 640
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketTotals:Ljava/util/Map;

    .line 641
    new-instance v0, Lcom/squareup/ui/Ranger$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/Ranger$Builder;-><init>()V

    .line 643
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketRowCursorList;->hasSearch()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 645
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketRowCursorList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 646
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->NO_RESULTS:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    goto :goto_0

    .line 648
    :cond_0
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->TICKET:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v2}, Lcom/squareup/tickets/TicketRowCursorList;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/Ranger$Builder;->addRange(Ljava/lang/Enum;I)Lcom/squareup/ui/Ranger$Builder;

    .line 650
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/ui/Ranger$Builder;->build()Lcom/squareup/ui/Ranger;

    move-result-object v0

    return-object v0

    .line 653
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketRowCursorList;->hasError()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 654
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->ERROR:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 657
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->showTextRow()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 658
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->TEXT:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 661
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 662
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->SORT:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 665
    :cond_4
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->showNewTicketButton()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 666
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->NEW_TICKET_BUTTON:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 669
    :cond_5
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketRowCursorList;->isEmpty()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    .line 670
    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    invoke-virtual {v3}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->showTicketTemplates()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketTemplates:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    :goto_1
    if-nez v1, :cond_7

    if-nez v2, :cond_7

    .line 673
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->NO_TICKETS:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 674
    invoke-virtual {v0}, Lcom/squareup/ui/Ranger$Builder;->build()Lcom/squareup/ui/Ranger;

    move-result-object v0

    return-object v0

    .line 677
    :cond_7
    iget-boolean v3, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->employeeFilteringEnabled:Z

    if-nez v3, :cond_b

    if-eqz v1, :cond_9

    .line 679
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->showTicketTemplates()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 680
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->OPEN_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 682
    :cond_8
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->TICKET:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v3}, Lcom/squareup/tickets/TicketRowCursorList;->size()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/squareup/ui/Ranger$Builder;->addRange(Ljava/lang/Enum;I)Lcom/squareup/ui/Ranger$Builder;

    :cond_9
    if-eqz v2, :cond_a

    .line 685
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->AVAILABLE_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 686
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->TEMPLATE:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketTemplates:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/Ranger$Builder;->addRange(Ljava/lang/Enum;I)Lcom/squareup/ui/Ranger$Builder;

    .line 688
    :cond_a
    invoke-virtual {v0}, Lcom/squareup/ui/Ranger$Builder;->build()Lcom/squareup/ui/Ranger;

    move-result-object v0

    return-object v0

    .line 691
    :cond_b
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketRowCursorList;->countForCurrentEmployee()I

    move-result v1

    if-lez v1, :cond_c

    .line 693
    sget-object v3, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->YOUR_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 694
    sget-object v3, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->TICKET:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0, v3, v1}, Lcom/squareup/ui/Ranger$Builder;->addRange(Ljava/lang/Enum;I)Lcom/squareup/ui/Ranger$Builder;

    .line 695
    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    invoke-virtual {v3}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->showTicketTemplates()Z

    move-result v3

    if-nez v3, :cond_c

    .line 696
    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketTotals:Ljava/util/Map;

    sget-object v5, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->YOUR_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-direct {p0, v4, v1}, Lcom/squareup/ui/ticket/TicketListPresenter;->getTicketsTotal(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_c
    if-eqz v2, :cond_d

    .line 700
    sget-object v2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->AVAILABLE_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 701
    sget-object v2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->TEMPLATE:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketTemplates:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/squareup/ui/Ranger$Builder;->addRange(Ljava/lang/Enum;I)Lcom/squareup/ui/Ranger$Builder;

    .line 703
    :cond_d
    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v2}, Lcom/squareup/tickets/TicketRowCursorList;->countForOtherEmployees()I

    move-result v2

    if-lez v2, :cond_f

    .line 705
    sget-object v3, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->OTHER_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 706
    iget-boolean v3, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->permittedToViewAllTickets:Z

    if-eqz v3, :cond_e

    .line 707
    sget-object v3, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->TICKET:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0, v3, v2}, Lcom/squareup/ui/Ranger$Builder;->addRange(Ljava/lang/Enum;I)Lcom/squareup/ui/Ranger$Builder;

    .line 708
    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    invoke-virtual {v3}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->showTicketTemplates()Z

    move-result v3

    if-nez v3, :cond_f

    .line 709
    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketTotals:Ljava/util/Map;

    sget-object v4, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->OTHER_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    add-int/2addr v2, v1

    .line 710
    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/ticket/TicketListPresenter;->getTicketsTotal(II)Ljava/lang/String;

    move-result-object v1

    .line 709
    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 713
    :cond_e
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->VIEW_ALL_TICKETS_BUTTON:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 717
    :cond_f
    :goto_2
    invoke-virtual {v0}, Lcom/squareup/ui/Ranger$Builder;->build()Lcom/squareup/ui/Ranger;

    move-result-object v0

    return-object v0
.end method

.method private cancelTicketSearchCallback()V
    .locals 1

    .line 499
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketsLoader;->cancelTicketSearchCallback()V

    return-void
.end method

.method private cancelTicketsRefreshedCallback()V
    .locals 1

    .line 593
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsRefreshedCallback:Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;

    if-eqz v0, :cond_0

    .line 594
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;->cancel()V

    const/4 v0, 0x0

    .line 595
    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsRefreshedCallback:Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;

    :cond_0
    return-void
.end method

.method private getEmployeeAccessMode()Lcom/squareup/tickets/TicketStore$EmployeeAccess;
    .locals 1

    .line 351
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->employeeFilteringEnabled:Z

    if-eqz v0, :cond_1

    .line 352
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->permittedToViewAllTickets:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->ALL_EMPLOYEES:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->ONE_EMPLOYEE:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    :goto_0
    return-object v0

    .line 354
    :cond_1
    sget-object v0, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->IGNORE_EMPLOYEES:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    return-object v0
.end method

.method private getTicketsTotal(II)Ljava/lang/String;
    .locals 4

    const-wide/16 v0, 0x0

    :goto_0
    if-ge p1, p2, :cond_0

    .line 630
    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v2, p1}, Lcom/squareup/tickets/TicketRowCursorList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;

    invoke-interface {v2}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getPriceAmount()J

    move-result-wide v2

    add-long/2addr v0, v2

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 633
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/orderentry/R$string;->open_tickets_total_amount:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 634
    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/ticket/TicketListPresenter;->getPriceString(J)Ljava/lang/String;

    move-result-object p2

    const-string v0, "amount"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 635
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 636
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private hasTicketsRefreshedCallback()Z
    .locals 1

    .line 589
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsRefreshedCallback:Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic lambda$QN4TvEWUOn8AAneNMH7zXg6YGcE(Lcom/squareup/ui/ticket/TicketListPresenter;Lcom/squareup/tickets/TicketRowCursorList;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->setTicketCursor(Lcom/squareup/tickets/TicketRowCursorList;)V

    return-void
.end method

.method private listenForTicketLoaderUpdates(Lcom/squareup/ui/ticket/BaseTicketListView;)V
    .locals 1

    .line 453
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$NeSd9ixwciFLbCJmUQLgaa7qhKY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$NeSd9ixwciFLbCJmUQLgaa7qhKY;-><init>(Lcom/squareup/ui/ticket/TicketListPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private listenForTicketPermissionUpdates(Lcom/squareup/ui/ticket/BaseTicketListView;)V
    .locals 1

    .line 429
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$90clRUf_sEXJvJd6i1EjuVwl7ZI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$90clRUf_sEXJvJd6i1EjuVwl7ZI;-><init>(Lcom/squareup/ui/ticket/TicketListPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 438
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$ye1hDJYBbtq4OUpH9y-JUYi_gFk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$ye1hDJYBbtq4OUpH9y-JUYi_gFk;-><init>(Lcom/squareup/ui/ticket/TicketListPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private loadingAfterProcessDeath(Landroid/os/Bundle;)Z
    .locals 0

    if-eqz p1, :cond_0

    .line 725
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private restartTicketSync(Ljava/lang/String;)V
    .locals 4

    .line 474
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x5300cf84

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq v0, v1, :cond_2

    const v1, -0x50eee4a1

    if-eq v0, v1, :cond_1

    const v1, 0x7668fc67

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "group-list-presenter-selected-section-all-tickets"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const-string v0, "group-list-presenter-selected-section-custom-tickets"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    goto :goto_1

    :cond_2
    const-string v0, "group-list-presenter-selected-section-search"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_5

    if-eq v0, v3, :cond_5

    if-eq v0, v2, :cond_4

    .line 485
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getSanitizedSortType()Lcom/squareup/tickets/TicketSort;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->employeeTokenOrNull:Ljava/lang/String;

    .line 486
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getEmployeeAccessMode()Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    move-result-object v3

    .line 485
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/ui/ticket/TicketsLoader;->syncTicketsForGroup(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V

    goto :goto_2

    .line 481
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getSanitizedSortType()Lcom/squareup/tickets/TicketSort;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->employeeTokenOrNull:Ljava/lang/String;

    .line 482
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getEmployeeAccessMode()Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    move-result-object v2

    .line 481
    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/ui/ticket/TicketsLoader;->syncCustomTickets(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V

    goto :goto_2

    .line 477
    :cond_5
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->getExcludedTicketIds()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getSanitizedSortType()Lcom/squareup/tickets/TicketSort;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->employeeTokenOrNull:Ljava/lang/String;

    .line 478
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getEmployeeAccessMode()Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    move-result-object v3

    .line 477
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/squareup/ui/ticket/TicketsLoader;->syncAllTickets(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V

    :goto_2
    return-void
.end method

.method private setTicketCursor(Lcom/squareup/tickets/TicketRowCursorList;)V
    .locals 2

    .line 534
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    .line 535
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->readyToShowTicketList:Lkotlin/Pair;

    invoke-virtual {v0}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 537
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketListListener:Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

    invoke-static {v0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;->access$200(Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;Lcom/squareup/tickets/TicketRowCursorList;)V

    .line 539
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->readyToShowTicketList:Lkotlin/Pair;

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 540
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->updateView()V

    .line 541
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/BaseTicketListView;->hideSpinner()V

    .line 544
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketsRefreshedCallback()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 545
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsRefreshedCallback:Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;->call()V

    .line 546
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->cancelTicketsRefreshedCallback()V

    :cond_1
    return-void
.end method


# virtual methods
.method addSelectedTicket(Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 1

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketSelection;->addSelectedTicket(Lcom/squareup/ui/ticket/TicketInfo;)V

    return-void
.end method

.method applyAllTicketsSelectionFilter()V
    .locals 1

    .line 231
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->cancelTicketSearchCallback()V

    .line 232
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/BaseTicketListView;->hideSoftKeyboard()V

    .line 233
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->loadAllTickets()V

    const-string v0, "group-list-presenter-selected-section-all-tickets"

    .line 234
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->restartTicketSync(Ljava/lang/String;)V

    return-void
.end method

.method applyCustomTicketsSelectionFilter()V
    .locals 1

    .line 238
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->cancelTicketSearchCallback()V

    .line 239
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->loadCustomTickets()V

    const-string v0, "group-list-presenter-selected-section-custom-tickets"

    .line 240
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->restartTicketSync(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/BaseTicketListView;->hideSoftKeyboard()V

    return-void
.end method

.method applyGroupSelectionFilter(Ljava/lang/String;)V
    .locals 0

    .line 245
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->cancelTicketSearchCallback()V

    .line 246
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->loadTicketsForGroup(Ljava/lang/String;)V

    .line 247
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->restartTicketSync(Ljava/lang/String;)V

    .line 248
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/BaseTicketListView;->hideSoftKeyboard()V

    return-void
.end method

.method applySearchSelectionFilter(Ljava/lang/String;)V
    .locals 0

    .line 225
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->cancelTicketQueryCallback()V

    .line 226
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->performSearch(Ljava/lang/String;)V

    const-string p1, "group-list-presenter-selected-section-search"

    .line 227
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->restartTicketSync(Ljava/lang/String;)V

    return-void
.end method

.method cancelTicketQueryCallback()V
    .locals 1

    .line 491
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketsLoader;->cancelTicketQueryCallback()V

    return-void
.end method

.method clearSelectedTickets()V
    .locals 1

    .line 298
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketSelection;->clearSelectedTickets()V

    return-void
.end method

.method getErrorMessage()Ljava/lang/CharSequence;
    .locals 11

    .line 375
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->isOffline()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_error_offline_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 379
    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 380
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketRowCursorList;->hasUnsyncedFixableTickets()Z

    move-result v1

    .line 381
    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v2}, Lcom/squareup/tickets/TicketRowCursorList;->hasUnsyncedUnfixableTickets()Z

    move-result v2

    const-string v3, "number"

    const-wide/16 v4, 0x1

    if-eqz v1, :cond_2

    .line 383
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketRowCursorList;->getUnsyncableFixableTicketCount()J

    move-result-wide v6

    .line 384
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/BaseTicketListView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 385
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getView()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-virtual {v8}, Lcom/squareup/ui/ticket/BaseTicketListView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 386
    sget v9, Lcom/squareup/ui/LinkSpan;->DEFAULT_COLOR_ID:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 387
    iget-object v9, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->res:Lcom/squareup/util/Res;

    cmp-long v10, v6, v4

    if-gtz v10, :cond_1

    sget v10, Lcom/squareup/orderentry/R$string;->open_tickets_error_sync_message_one:I

    goto :goto_0

    :cond_1
    sget v10, Lcom/squareup/orderentry/R$string;->open_tickets_error_sync_message_many:I

    :goto_0
    invoke-interface {v9, v10}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v9

    .line 390
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v3, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v6

    new-instance v7, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v7, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->res:Lcom/squareup/util/Res;

    sget v9, Lcom/squareup/orderentry/R$string;->open_tickets_error_sync_message_update_register:I

    .line 393
    invoke-interface {v1, v9}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 392
    invoke-virtual {v7, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    new-instance v7, Lcom/squareup/ui/ticket/TicketListPresenter$2;

    invoke-direct {v7, p0, v8}, Lcom/squareup/ui/ticket/TicketListPresenter$2;-><init>(Lcom/squareup/ui/ticket/TicketListPresenter;I)V

    .line 394
    invoke-virtual {v1, v7}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 411
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asSpannable()Landroid/text/Spannable;

    move-result-object v1

    const-string v7, "update_register"

    .line 391
    invoke-virtual {v6, v7, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 411
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 387
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    if-eqz v2, :cond_5

    .line 416
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 418
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketRowCursorList;->getUnsyncableUnfixableTicketCount()J

    move-result-wide v1

    .line 419
    iget-object v6, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->res:Lcom/squareup/util/Res;

    cmp-long v7, v1, v4

    if-gtz v7, :cond_4

    sget v4, Lcom/squareup/orderentry/R$string;->open_tickets_error_sync_unfixable_message_one:I

    goto :goto_1

    :cond_4
    sget v4, Lcom/squareup/orderentry/R$string;->open_tickets_error_sync_unfixable_message_many:I

    :goto_1
    invoke-interface {v6, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    .line 422
    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 423
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 419
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_5
    return-object v0
.end method

.method getErrorTitle()Ljava/lang/String;
    .locals 2

    .line 359
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->isOffline()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_error_offline_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_error_offline_title_short:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 366
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->hasUnsyncedFixableTickets()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    .line 367
    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->hasUnsyncedUnfixableTickets()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    return-object v0

    .line 368
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_error_sync_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getPriceString(J)Ljava/lang/String;
    .locals 1

    .line 342
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, p2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 343
    iget-object p2, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getSanitizedSortType()Lcom/squareup/tickets/TicketSort;
    .locals 2

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSortSetting:Lcom/squareup/settings/LocalSetting;

    sget-object v1, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/TicketSort;

    .line 321
    sget-object v1, Lcom/squareup/tickets/TicketSort;->EMPLOYEE_NAME:Lcom/squareup/tickets/TicketSort;

    if-ne v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->permittedToViewAllTickets:Z

    if-nez v1, :cond_0

    .line 322
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSortSetting:Lcom/squareup/settings/LocalSetting;

    sget-object v1, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 323
    sget-object v0, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    :cond_0
    return-object v0
.end method

.method getSearchFilter()Ljava/lang/String;
    .locals 1

    .line 334
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketsLoader;->getSearchFilter()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getSelectedCount()I
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketSelection;->getSelectedCount()I

    move-result v0

    return v0
.end method

.method getSelectedTicketsInfo()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;"
        }
    .end annotation

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketSelection;->getSelectedTicketsInfo()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method getTicketCursorSearchText()Ljava/lang/String;
    .locals 1

    .line 522
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->getSearchText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method getTicketCursorSize()I
    .locals 1

    .line 508
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method getTicketTemplates()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;"
        }
    .end annotation

    .line 338
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketTemplates:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method getTimeSinceString(Ljava/util/Date;)Ljava/lang/String;
    .locals 8

    .line 347
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->res:Lcom/squareup/util/Res;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    sget-object v6, Lcom/squareup/util/ShortTimes$MaxUnit;->DAY:Lcom/squareup/util/ShortTimes$MaxUnit;

    const/4 v5, 0x1

    const/4 v7, 0x1

    invoke-static/range {v0 .. v7}, Lcom/squareup/util/ShortTimes;->shortTimeSince(Lcom/squareup/util/Res;JJZLcom/squareup/util/ShortTimes$MaxUnit;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method hasPendingTicketSearch()Z
    .locals 1

    .line 495
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketsLoader;->hasPendingTicketSearch()Z

    move-result v0

    return v0
.end method

.method hasSelectedTickets()Z
    .locals 1

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketSelection;->hasSelectedTickets()Z

    move-result v0

    return v0
.end method

.method hasTicketCursor()Z
    .locals 1

    .line 503
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isTicketCheckboxSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketSelection;->isTicketSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z

    move-result p1

    return p1
.end method

.method isTicketCursorEmpty()Z
    .locals 1

    .line 513
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public synthetic lambda$listenForTicketLoaderUpdates$5$TicketListPresenter()Lrx/Subscription;
    .locals 2

    .line 454
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketsLoader;->readOnlyTicketCursor()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$QN4TvEWUOn8AAneNMH7zXg6YGcE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$QN4TvEWUOn8AAneNMH7zXg6YGcE;-><init>(Lcom/squareup/ui/ticket/TicketListPresenter;)V

    .line 455
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$listenForTicketPermissionUpdates$2$TicketListPresenter()Lrx/Subscription;
    .locals 2

    .line 430
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketScopeRunner:Lcom/squareup/ui/ticket/TicketScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketScopeRunner;->onPermittedToViewAllTicketsChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$Sr2LE54Qf2VkJHLSqivS38CNqgM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$Sr2LE54Qf2VkJHLSqivS38CNqgM;-><init>(Lcom/squareup/ui/ticket/TicketListPresenter;)V

    .line 431
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$listenForTicketPermissionUpdates$4$TicketListPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 439
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketScopeRunner:Lcom/squareup/ui/ticket/TicketScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketScopeRunner;->employeeToken()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$DXC9jJdwGS1WWDLUzDGcQn5hUmc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$DXC9jJdwGS1WWDLUzDGcQn5hUmc;-><init>(Lcom/squareup/ui/ticket/TicketListPresenter;)V

    .line 440
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$1$TicketListPresenter(Ljava/lang/Boolean;)V
    .locals 0

    .line 432
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->permittedToViewAllTickets:Z

    .line 434
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->selectionFilter:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->restartTicketSync(Ljava/lang/String;)V

    .line 435
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->updateView()V

    return-void
.end method

.method public synthetic lambda$null$3$TicketListPresenter(Lcom/squareup/util/Optional;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 441
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->employeeTokenOrNull:Ljava/lang/String;

    .line 446
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->employeeTokenOrNull:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 447
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->refreshTickets()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$6$TicketListPresenter(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 608
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketTemplates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 609
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketTemplates:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 610
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->readyToShowTicketList:Lkotlin/Pair;

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 612
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->readyToShowTicketList:Lkotlin/Pair;

    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 613
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->updateView()V

    .line 614
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/BaseTicketListView;->hideSpinner()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onLoad$0$TicketListPresenter(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 0

    .line 205
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    .line 206
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->refreshTicketTemplates()V

    return-void
.end method

.method public synthetic lambda$refreshTicketTemplates$7$TicketListPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 606
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

    invoke-interface {v0}, Lcom/squareup/opentickets/PredefinedTickets;->getAllAvailableTicketTemplates()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$C6JAzQ9BeKJTYdIiB92UtUHBWIM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$C6JAzQ9BeKJTYdIiB92UtUHBWIM;-><init>(Lcom/squareup/ui/ticket/TicketListPresenter;)V

    .line 607
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method loadAllTickets()V
    .locals 5

    .line 459
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->getExcludedTicketIds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getSanitizedSortType()Lcom/squareup/tickets/TicketSort;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->employeeTokenOrNull:Ljava/lang/String;

    .line 460
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getEmployeeAccessMode()Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    move-result-object v4

    .line 459
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/ui/ticket/TicketsLoader;->loadAllTickets(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V

    return-void
.end method

.method loadCustomTickets()V
    .locals 4

    .line 464
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getSanitizedSortType()Lcom/squareup/tickets/TicketSort;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->employeeTokenOrNull:Ljava/lang/String;

    .line 465
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getEmployeeAccessMode()Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    move-result-object v3

    .line 464
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/ui/ticket/TicketsLoader;->loadCustomTickets(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V

    return-void
.end method

.method loadTicketsForGroup(Ljava/lang/String;)V
    .locals 4

    .line 469
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getSanitizedSortType()Lcom/squareup/tickets/TicketSort;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->employeeTokenOrNull:Ljava/lang/String;

    .line 470
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getEmployeeAccessMode()Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    move-result-object v3

    .line 469
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/ui/ticket/TicketsLoader;->loadTicketsForGroup(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 0

    .line 213
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->cancelTicketsRefreshedCallback()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 152
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 154
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/BaseTicketListView;

    .line 155
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->listenForTicketPermissionUpdates(Lcom/squareup/ui/ticket/BaseTicketListView;)V

    .line 156
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->listenForTicketLoaderUpdates(Lcom/squareup/ui/ticket/BaseTicketListView;)V

    .line 158
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->showTicketTemplates()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 159
    invoke-virtual {v0, v2}, Lcom/squareup/ui/ticket/BaseTicketListView;->setSearchBarVisible(Z)V

    :cond_0
    if-eqz p1, :cond_1

    .line 166
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->showTicketTemplates()Z

    move-result v0

    if-nez v0, :cond_3

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->readyToShowTicketList:Lkotlin/Pair;

    invoke-virtual {v0}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->readyToShowTicketList:Lkotlin/Pair;

    invoke-virtual {v0}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_3
    if-nez p1, :cond_4

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSortSetting:Lcom/squareup/settings/LocalSetting;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->resetSortSettingAsNeeded(Lcom/squareup/settings/LocalSetting;)V

    .line 185
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->refreshTickets()V

    goto :goto_0

    .line 187
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->selectionFilter:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->restartTicketSync(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->updateView()V

    .line 191
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/BaseTicketListView;->hideSpinner()V

    goto :goto_0

    .line 194
    :cond_5
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->refreshTickets()V

    .line 198
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->showTicketTemplates()Z

    move-result v0

    if-eqz v0, :cond_7

    if-nez p1, :cond_6

    .line 200
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->refreshTicketTemplates()V

    .line 202
    :cond_6
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->loadingAfterProcessDeath(Landroid/os/Bundle;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 204
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$77CfRQdl26VpdImcBidl0yYQY9g;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$77CfRQdl26VpdImcBidl0yYQY9g;-><init>(Lcom/squareup/ui/ticket/TicketListPresenter;)V

    invoke-interface {p1, v0, v2}, Lcom/squareup/cogs/Cogs;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    :cond_7
    return-void
.end method

.method onSelectedTicketsChanged()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;"
        }
    .end annotation

    .line 302
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketSelection;->onSelectedTicketsChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method onSortChange(Lcom/squareup/tickets/TicketSort;)V
    .locals 4

    .line 306
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getSanitizedSortType()Lcom/squareup/tickets/TicketSort;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 307
    invoke-virtual {v0}, Lcom/squareup/tickets/TicketSort;->name()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 308
    invoke-virtual {p1}, Lcom/squareup/tickets/TicketSort;->name()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-string v2, "[Ticket_List_Presenter] Sort changed from %s to %s."

    .line 307
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eq p1, v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSortSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 311
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->refreshTickets()V

    :cond_0
    return-void
.end method

.method onViewAllTicketsButtonClicked()V
    .locals 3

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->passcodeGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_TICKET_MANAGE_ALL:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/ticket/TicketListPresenter$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ticket/TicketListPresenter$1;-><init>(Lcom/squareup/ui/ticket/TicketListPresenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method performSearch(Ljava/lang/String;)V
    .locals 6

    .line 329
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->getExcludedTicketIds()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSortSetting:Lcom/squareup/settings/LocalSetting;

    sget-object v3, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    invoke-interface {v2, v3}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/tickets/TicketSort;

    iget-object v4, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->employeeTokenOrNull:Ljava/lang/String;

    .line 330
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getEmployeeAccessMode()Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    move-result-object v5

    move-object v3, p1

    .line 329
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/ticket/TicketsLoader;->loadTicketsForSearch(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V

    return-void
.end method

.method pruneSelectedTickets()V
    .locals 2

    .line 292
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/TicketSelection;->pruneSelectedTicketsAgainst(Lcom/squareup/tickets/TicketRowCursorList;)V

    :cond_0
    return-void
.end method

.method refreshTicketTemplates()V
    .locals 2

    .line 600
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "[Ticket_List_Presenter] Hitting cogs to get ticket templates to set..."

    .line 604
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 605
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$SPrHPDjD6enffZDaEkQ7YtvbVnU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketListPresenter$SPrHPDjD6enffZDaEkQ7YtvbVnU;-><init>(Lcom/squareup/ui/ticket/TicketListPresenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method refreshTickets()V
    .locals 1

    const/4 v0, 0x0

    .line 551
    invoke-virtual {p0, v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->refreshTickets(Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;)V

    return-void
.end method

.method refreshTickets(Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;)V
    .locals 6

    .line 555
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 560
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->cancelTicketsRefreshedCallback()V

    .line 561
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketsRefreshedCallback:Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;

    :cond_1
    const/4 p1, 0x0

    new-array v0, p1, [Ljava/lang/Object;

    const-string v1, "[Ticket_List_Presenter] Hitting local store to get a ticket cursor to set..."

    .line 564
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 565
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->selectionFilter:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, -0x5300cf84

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eq v2, v3, :cond_4

    const p1, -0x50eee4a1

    if-eq v2, p1, :cond_3

    const p1, 0x7668fc67

    if-eq v2, p1, :cond_2

    goto :goto_0

    :cond_2
    const-string p1, "group-list-presenter-selected-section-all-tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    const/4 p1, 0x1

    goto :goto_1

    :cond_3
    const-string p1, "group-list-presenter-selected-section-custom-tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    const/4 p1, 0x2

    goto :goto_1

    :cond_4
    const-string v2, "group-list-presenter-selected-section-search"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_1

    :cond_5
    :goto_0
    const/4 p1, -0x1

    :goto_1
    const/4 v0, 0x0

    if-eqz p1, :cond_a

    if-eq p1, v5, :cond_7

    if-eq p1, v4, :cond_6

    .line 584
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->selectionFilter:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->applyGroupSelectionFilter(Ljava/lang/String;)V

    goto :goto_2

    .line 581
    :cond_6
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->applyCustomTicketsSelectionFilter()V

    goto :goto_2

    .line 570
    :cond_7
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->config:Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->usesDefaultAllTicketsFilter()Z

    move-result p1

    if-eqz p1, :cond_9

    .line 575
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result p1

    if-eqz p1, :cond_8

    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->getSearchText()Ljava/lang/String;

    move-result-object v0

    :cond_8
    invoke-virtual {p0, v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->applySearchSelectionFilter(Ljava/lang/String;)V

    goto :goto_2

    .line 577
    :cond_9
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->applyAllTicketsSelectionFilter()V

    goto :goto_2

    .line 567
    :cond_a
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result p1

    if-eqz p1, :cond_b

    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->getSearchText()Ljava/lang/String;

    move-result-object v0

    :cond_b
    invoke-virtual {p0, v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->applySearchSelectionFilter(Ljava/lang/String;)V

    :goto_2
    return-void
.end method

.method setSelectionFilter(Ljava/lang/String;)V
    .locals 0

    .line 221
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->selectionFilter:Ljava/lang/String;

    return-void
.end method

.method ticketCursorHasError()Z
    .locals 1

    .line 526
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->hasError()Z

    move-result v0

    return v0
.end method

.method ticketCursorHasSearchText()Z
    .locals 1

    .line 518
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->hasSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method toggleSelected(Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 1

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketSelection;->toggleSelected(Lcom/squareup/ui/ticket/TicketInfo;)V

    return-void
.end method

.method toggleUniqueSelected(Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 1

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketSelection;->toggleUniqueSelected(Lcom/squareup/ui/ticket/TicketInfo;)V

    return-void
.end method

.method updateView()V
    .locals 7

    .line 620
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 623
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->buildRanger()Lcom/squareup/ui/Ranger;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursor:Lcom/squareup/tickets/TicketRowCursorList;

    iget-object v4, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketTotals:Ljava/util/Map;

    iget-object v5, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketTemplates:Ljava/util/List;

    iget-boolean v6, p0, Lcom/squareup/ui/ticket/TicketListPresenter;->permittedToViewAllTickets:Z

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/ticket/BaseTicketListView;->setList(Lcom/squareup/ui/Ranger;Lcom/squareup/tickets/TicketRowCursorList;Ljava/util/Map;Ljava/util/List;Z)V

    :cond_1
    :goto_0
    return-void
.end method
