.class public interface abstract Lcom/squareup/ui/ticket/TicketActionScope$Component;
.super Ljava/lang/Object;
.source "TicketActionScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/ticket/TicketActionScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketActionScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract masterDetailTicket(Lcom/squareup/ui/ticket/MasterDetailTicketScreen$Module;)Lcom/squareup/ui/ticket/MasterDetailTicketScreen$Component;
.end method

.method public abstract mergeTicket()Lcom/squareup/ui/ticket/MergeTicketScreen$Component;
.end method

.method public abstract ticketActionScopeRunner()Lcom/squareup/ui/ticket/TicketActionScopeRunner;
.end method

.method public abstract ticketBulkDeleteDialog()Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Component;
.end method

.method public abstract ticketBulkVoid()Lcom/squareup/ui/ticket/TicketBulkVoidScreen$Component;
.end method

.method public abstract ticketList(Lcom/squareup/ui/ticket/TicketListScreen$Module;)Lcom/squareup/ui/ticket/TicketListScreen$Component;
.end method

.method public abstract ticketTransferEmployees()Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Component;
.end method
