.class public final Lcom/squareup/ui/ticket/SplitTicketCoordinator_Factory;
.super Ljava/lang/Object;
.source "SplitTicketCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/SplitTicketCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final ticketSplitterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/splitticket/TicketSplitter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/splitticket/TicketSplitter;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator_Factory;->ticketSplitterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/SplitTicketCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/splitticket/TicketSplitter;",
            ">;)",
            "Lcom/squareup/ui/ticket/SplitTicketCoordinator_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/ui/ticket/SplitTicketCoordinator_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/SplitTicketCoordinator_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/splitticket/TicketSplitter;)Lcom/squareup/ui/ticket/SplitTicketCoordinator;
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;-><init>(Lcom/squareup/splitticket/TicketSplitter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/SplitTicketCoordinator;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator_Factory;->ticketSplitterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/splitticket/TicketSplitter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/SplitTicketCoordinator_Factory;->newInstance(Lcom/squareup/splitticket/TicketSplitter;)Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketCoordinator_Factory;->get()Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    move-result-object v0

    return-object v0
.end method
