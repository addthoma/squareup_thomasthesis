.class public Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Factory;
.super Ljava/lang/Object;
.source "TicketBulkDeleteDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/hudtoaster/HudToaster;Landroid/content/Context;ILcom/squareup/ui/ticket/OpenTicketsRunner;Lflow/Flow;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 68
    invoke-static {p0}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->access$400(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;)Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    move-result-object p7

    sget-object p8, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->DELETE:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    if-ne p7, p8, :cond_0

    .line 69
    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->deleteSelectedTickets()V

    .line 70
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-static {p0, p3, p4}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->access$500(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p0

    const/4 p3, 0x0

    invoke-interface {p2, p1, p0, p3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 71
    invoke-virtual {p5, p6}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->finishDeleteTicketsFromDialog(Lflow/Flow;)V

    goto :goto_0

    .line 74
    :cond_0
    invoke-static {p0}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->access$600(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/ui/ticket/TicketBulkVoidScreen;->forAuthorizedEmployee(Ljava/lang/String;)Lcom/squareup/ui/ticket/TicketBulkVoidScreen;

    move-result-object p0

    sget-object p1, Lflow/Direction;->FORWARD:Lflow/Direction;

    .line 73
    invoke-static {p6, p0, p1}, Lcom/squareup/container/Flows;->replaceTop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    :goto_0
    return-void
.end method

.method static synthetic lambda$create$1(Lflow/Flow;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 79
    invoke-virtual {p0}, Lflow/Flow;->goBack()Z

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 58
    const-class v0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Component;

    .line 59
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;

    .line 60
    invoke-interface {v0}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Component;->hudToaster()Lcom/squareup/hudtoaster/HudToaster;

    move-result-object v5

    .line 61
    invoke-interface {v0}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Component;->openTicketsRunner()Lcom/squareup/ui/ticket/OpenTicketsRunner;

    move-result-object v8

    .line 62
    invoke-interface {v0}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Component;->ticketActionSession()Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    move-result-object v4

    .line 63
    invoke-virtual {v4}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->getSelectedCount()I

    move-result v0

    .line 64
    invoke-static {p1}, Lflow/Flow;->get(Landroid/content/Context;)Lflow/Flow;

    move-result-object v10

    .line 66
    new-instance v11, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v11, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 67
    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->access$300(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;)I

    move-result v12

    new-instance v13, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;

    move-object v2, v13

    move-object v3, v1

    move-object v6, p1

    move v7, v0

    move-object v9, v10

    invoke-direct/range {v2 .. v9}, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$yqg2Y2ZsQeE25chvq0CjSXJFug8;-><init>(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/hudtoaster/HudToaster;Landroid/content/Context;ILcom/squareup/ui/ticket/OpenTicketsRunner;Lflow/Flow;)V

    invoke-virtual {v11, v12, v13}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v2

    .line 78
    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->access$200(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;)I

    move-result v3

    new-instance v4, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$KJUmvucSGbBYu7fEYM7tGjky80c;

    invoke-direct {v4, v10}, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$Factory$KJUmvucSGbBYu7fEYM7tGjky80c;-><init>(Lflow/Flow;)V

    invoke-virtual {v2, v3, v4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v2

    .line 80
    invoke-static {v1, p1, v0}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->access$100(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v2

    .line 81
    invoke-static {v1, p1, v0}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->access$000(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 82
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 83
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 66
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
