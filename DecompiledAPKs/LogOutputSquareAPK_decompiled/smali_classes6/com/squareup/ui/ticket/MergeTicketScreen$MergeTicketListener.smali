.class public Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;
.super Ljava/lang/Object;
.source "MergeTicketScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MergeTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MergeTicketListener"
.end annotation


# instance fields
.field private final mergeCompleted:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 368
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;->mergeCompleted:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;)V
    .locals 0

    .line 366
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;->publishMergeCompleted()V

    return-void
.end method

.method private publishMergeCompleted()V
    .locals 2

    .line 371
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;->mergeCompleted:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method onMergeCompleted()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 375
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;->mergeCompleted:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method
