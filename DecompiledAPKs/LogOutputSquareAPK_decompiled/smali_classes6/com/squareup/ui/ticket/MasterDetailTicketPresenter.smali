.class public Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;
.super Lmortar/ViewPresenter;
.source "MasterDetailTicketPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$Component;,
        Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$ProcessDeathHelper;,
        Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;,
        Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/ticket/MasterDetailTicketView;",
        ">;"
    }
.end annotation


# static fields
.field private static final CAN_EDIT_TICKETS_AFTER_PROCESS_DEATH_KEY:Ljava/lang/String; = "master-detail-ticket-presenter-can-edit-tickets-after-process-death-key"

.field private static final CLEAR_SEARCH_BAR_DELAY:J = 0xc8L

.field private static final HAS_TICKETS_KEY:Ljava/lang/String; = "master-detail-ticket-presenter-has-tickets-key"

.field static final OPEN_TICKET_LIMIT:I = 0x1f4

.field private static final RESETTING_SCREEN_AFTER_ADDITIVE_MERGE_KEY:Ljava/lang/String; = "master-detail-ticket-presenter-resetting-screen-after-additive-merge-key"

.field private static final SEARCH_FILTER_KEY:Ljava/lang/String; = "master-detail-ticket-presenter-search-filter-key"


# instance fields
.field private final blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

.field private final bulkDeleteAllowed:Z

.field private final bulkMergeAllowed:Z

.field private final bulkMoveAllowed:Z

.field private final bulkTransferAllowed:Z

.field private final bulkVoidAllowed:Z

.field private final bus:Lcom/squareup/badbus/BadEventSink;

.field private canEditTicketsAfterProcessDeath:Z

.field private final cardNameHandler:Lcom/squareup/tickets/TicketCardNameHandler;

.field private currentScreen:Lcom/squareup/container/ContainerTreeKey;

.field private final device:Lcom/squareup/util/Device;

.field private final displayMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

.field private firstTimeLoadingTickets:Z

.field private final flow:Lflow/Flow;

.field private final groupListListener:Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

.field private final groupPresenter:Lcom/squareup/ui/ticket/GroupListPresenter;

.field private hasTickets:Z

.field private final loadingTicketPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            ">;"
        }
    .end annotation
.end field

.field private final lockOrClockButtonHelper:Lcom/squareup/ui/main/LockOrClockButtonHelper;

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private final mergeTicketListener:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;

.field private final moveTicketListener:Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final passcodeGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private permittedToViewAllTickets:Z

.field private final predefinedTicketsEnabled:Z

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private processDeathHelper:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$ProcessDeathHelper;

.field private final res:Lcom/squareup/util/Res;

.field private resettingScreenAfterAdditiveMerge:Z

.field private searchFilter:Ljava/lang/String;

.field private selectedSection:Ljava/lang/String;

.field private final ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

.field private final ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

.field private final ticketCreatedListener:Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;

.field private final ticketListListener:Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

.field private final ticketLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

.field private ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

.field private ticketModeScreen:Lcom/squareup/ui/ticket/TicketModeScreen;

.field private final ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

.field private final ticketScopeRunner:Lcom/squareup/ui/ticket/TicketScopeRunner;

.field private final tickets:Lcom/squareup/tickets/Tickets;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private final useOpenTicketsHomeScreen:Z


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/GroupListPresenter;Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;Lcom/squareup/ui/ticket/TicketListPresenter;Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/Tickets;Lcom/squareup/tickets/TicketCountsCache;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/tickets/TicketCardNameHandler;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Lcom/squareup/ui/main/PosContainer;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/main/LockOrClockButtonHelper;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 275
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    move-object v1, p1

    .line 276
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->groupPresenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    move-object v1, p2

    .line 277
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->groupListListener:Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

    move-object v1, p3

    .line 278
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    move-object v1, p4

    .line 279
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketListListener:Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

    move-object v1, p5

    .line 280
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketCreatedListener:Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;

    move-object v1, p6

    .line 281
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->mergeTicketListener:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;

    move-object v1, p7

    .line 282
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->moveTicketListener:Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

    move-object v1, p8

    .line 283
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->tickets:Lcom/squareup/tickets/Tickets;

    move-object v1, p9

    .line 284
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    move-object v1, p10

    .line 285
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    move-object v1, p11

    .line 286
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketScopeRunner:Lcom/squareup/ui/ticket/TicketScopeRunner;

    move-object v1, p12

    .line 287
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->cardNameHandler:Lcom/squareup/tickets/TicketCardNameHandler;

    move-object/from16 v1, p14

    .line 288
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    move-object/from16 v1, p15

    .line 289
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    move-object/from16 v1, p16

    .line 290
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->displayMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    move-object/from16 v1, p17

    .line 291
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    move-object/from16 v1, p18

    .line 292
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->flow:Lflow/Flow;

    move-object/from16 v1, p19

    .line 293
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    move-object/from16 v1, p20

    .line 294
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->bus:Lcom/squareup/badbus/BadEventSink;

    move-object/from16 v1, p22

    .line 295
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->device:Lcom/squareup/util/Device;

    move-object/from16 v1, p23

    .line 296
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->res:Lcom/squareup/util/Res;

    move-object/from16 v1, p24

    .line 297
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    move-object/from16 v1, p25

    .line 298
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->passcodeGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object/from16 v1, p26

    .line 299
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->lockOrClockButtonHelper:Lcom/squareup/ui/main/LockOrClockButtonHelper;

    move-object/from16 v1, p27

    .line 300
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    .line 302
    new-instance v1, Lcom/squareup/caller/BlockingPopupPresenter;

    move-object/from16 v2, p21

    invoke-direct {v1, v2}, Lcom/squareup/caller/BlockingPopupPresenter;-><init>(Lcom/squareup/thread/executor/MainThread;)V

    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    move-object/from16 v1, p28

    .line 303
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->printerStations:Lcom/squareup/print/PrinterStations;

    move-object/from16 v1, p29

    .line 304
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    .line 305
    new-instance v1, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->loadingTicketPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 307
    invoke-interface/range {p13 .. p13}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->useOpenTicketsHomeScreen:Z

    .line 308
    invoke-interface/range {p13 .. p13}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->predefinedTicketsEnabled:Z

    .line 310
    invoke-interface/range {p13 .. p13}, Lcom/squareup/tickets/OpenTicketsSettings;->isBulkDeleteAllowed()Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->bulkDeleteAllowed:Z

    .line 311
    invoke-interface/range {p13 .. p13}, Lcom/squareup/tickets/OpenTicketsSettings;->isBulkVoidAllowed()Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->bulkVoidAllowed:Z

    .line 312
    invoke-interface/range {p13 .. p13}, Lcom/squareup/tickets/OpenTicketsSettings;->isMergeTicketsAllowed()Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->bulkMergeAllowed:Z

    .line 313
    invoke-interface/range {p13 .. p13}, Lcom/squareup/tickets/OpenTicketsSettings;->isMoveTicketsAllowed()Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->bulkMoveAllowed:Z

    .line 314
    invoke-interface/range {p13 .. p13}, Lcom/squareup/tickets/OpenTicketsSettings;->isTicketTransferAllowed()Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->bulkTransferAllowed:Z

    const-string v1, ""

    .line 316
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->searchFilter:Ljava/lang/String;

    .line 317
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->selectedSection:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)Ljava/lang/String;
    .locals 0

    .line 101
    iget-object p0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->searchFilter:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;
    .locals 0

    .line 101
    iget-object p0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->displayMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)Ljava/lang/String;
    .locals 0

    .line 101
    iget-object p0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->selectedSection:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)Lflow/Flow;
    .locals 0

    .line 101
    iget-object p0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->flow:Lflow/Flow;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)Z
    .locals 0

    .line 101
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasView()Z

    move-result p0

    return p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)Ljava/lang/Object;
    .locals 0

    .line 101
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V
    .locals 0

    .line 101
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->refreshEditBarAndTicketList(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    return-void
.end method

.method private additivelyMergeToTicket(Ljava/lang/String;)V
    .locals 4

    .line 991
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setTicketListClickable(Z)V

    .line 997
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$_dIgDk0fK8XxNX5CkV1o8t7euiQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$_dIgDk0fK8XxNX5CkV1o8t7euiQ;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/caller/BlockingPopupPresenter;->show(Ljava/lang/Runnable;)V

    .line 1000
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    .line 1003
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->res:Lcom/squareup/util/Res;

    new-instance v3, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$Xw_Ti3_gs6-o8N1DOpn-aoQaz2k;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$Xw_Ti3_gs6-o8N1DOpn-aoQaz2k;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/payment/Order;)V

    invoke-interface {v1, p1, v0, v2, v3}, Lcom/squareup/tickets/Tickets;->additiveMergeToTicket(Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method private allTicketsRemoved(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 647
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    .line 651
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getTicketCursorSize()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-gt v0, p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    .line 652
    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursorHasSearchText()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private alwaysShowTicketCheckboxes()Z
    .locals 2

    .line 731
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private canEditTickets()Z
    .locals 1

    .line 553
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 554
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isBulkDeleteAllowed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 555
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isBulkVoidAllowed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 556
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isBulkMergeAllowed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 557
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isBulkMoveAllowed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 558
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isBulkTransferAllowed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private hasTicketPrinter()Z
    .locals 4

    .line 1139
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    return v0
.end method

.method private isBulkDeleteAllowed()Z
    .locals 2

    .line 634
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->LOAD_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->BULK_EDIT_TICKETS:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->bulkDeleteAllowed:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isBulkMergeAllowed()Z
    .locals 3

    .line 684
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->LOAD_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    const/4 v2, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->BULK_EDIT_TICKETS:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->bulkMergeAllowed:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    .line 686
    invoke-virtual {v0}, Lcom/squareup/tickets/TicketCountsCache;->getAllTicketsCount()I

    move-result v0

    if-le v0, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method private isBulkMoveAllowed()Z
    .locals 2

    .line 701
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->LOAD_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->BULK_EDIT_TICKETS:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->bulkMoveAllowed:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isBulkTransferAllowed()Z
    .locals 2

    .line 710
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->LOAD_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->BULK_EDIT_TICKETS:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->bulkTransferAllowed:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isBulkVoidAllowed()Z
    .locals 2

    .line 662
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->LOAD_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->BULK_EDIT_TICKETS:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->bulkVoidAllowed:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isResettingScreenAfterAdditiveMerge()Z
    .locals 2

    .line 1074
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->resettingScreenAfterAdditiveMerge:Z

    const/4 v1, 0x0

    .line 1075
    iput-boolean v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->resettingScreenAfterAdditiveMerge:Z

    return v0
.end method

.method static synthetic lambda$null$1(Lcom/squareup/ui/ticket/MasterDetailTicketView;Ljava/util/List;)V
    .locals 0

    .line 492
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 491
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setMergeTransactionTicketButtonEnabled(Z)V

    return-void
.end method

.method private listenForAdditiveMergeUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V
    .locals 1

    .line 970
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$fW0njySWpl4m-cANlekTHznSylI;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$fW0njySWpl4m-cANlekTHznSylI;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private listenForEditModeUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V
    .locals 1

    .line 512
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$zTB_k__klcMyU4XnLZkYV9YVPj8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$zTB_k__klcMyU4XnLZkYV9YVPj8;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 533
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$Dd_oTF216WQz6GxFRYZw5M9IAMg;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$Dd_oTF216WQz6GxFRYZw5M9IAMg;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 537
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$YljFWvriznA6cNUlRMXFCU--_GM;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$YljFWvriznA6cNUlRMXFCU--_GM;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 542
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$Ce7q43JOJebeRcycIs0yrL8a-CI;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$Ce7q43JOJebeRcycIs0yrL8a-CI;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private listenForGroupListUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V
    .locals 1

    .line 767
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$rSyrmwM_Xjf6wuV8Xs4XZgeT5JA;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$rSyrmwM_Xjf6wuV8Xs4XZgeT5JA;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private listenForMergeTransactionTicketUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V
    .locals 1

    .line 489
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$hX3G10KOgVEo8POcajsyFrUWDlY;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$hX3G10KOgVEo8POcajsyFrUWDlY;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private listenForMergeUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V
    .locals 1

    .line 674
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$6PqHeZDFh5Nb13v3q_JyZiyIPIg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$6PqHeZDFh5Nb13v3q_JyZiyIPIg;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private listenForMoveUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V
    .locals 1

    .line 695
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$yOtrwgB1qYJyYlnrs3Mr-jaLkis;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$yOtrwgB1qYJyYlnrs3Mr-jaLkis;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private listenForNavigationUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V
    .locals 1

    .line 735
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$8zBUqB1n3FCbSjk4M6ZedtfejWA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$8zBUqB1n3FCbSjk4M6ZedtfejWA;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private listenForTicketCountUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V
    .locals 1

    .line 786
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$Jtb_HTZhBdhgbbmVL2UF9O8d8hE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$Jtb_HTZhBdhgbbmVL2UF9O8d8hE;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private listenForTicketListUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V
    .locals 1

    .line 814
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$2jjVdhzuOD0cL0RYMVmTSrvan_s;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$2jjVdhzuOD0cL0RYMVmTSrvan_s;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private listenForTicketPermissionUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V
    .locals 1

    .line 756
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$WjkcXK4noSjt3amD0I35TbrlTFo;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$WjkcXK4noSjt3amD0I35TbrlTFo;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private loadAsTransactionTicket(Ljava/lang/String;)V
    .locals 2

    .line 1081
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setTicketListClickable(Z)V

    .line 1087
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$QJhWnbCoc7Lux4h_wxCTr8Fa8I4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$QJhWnbCoc7Lux4h_wxCTr8Fa8I4;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/caller/BlockingPopupPresenter;->show(Ljava/lang/Runnable;)V

    .line 1091
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->tickets:Lcom/squareup/tickets/Tickets;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$uGijwBnvWpq5ZSO7phhyGLuw9Dg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$uGijwBnvWpq5ZSO7phhyGLuw9Dg;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    invoke-interface {v0, p1, v1}, Lcom/squareup/tickets/Tickets;->getTicketAndLock(Ljava/lang/String;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method private loadingAfterProcessDeath(Landroid/os/Bundle;)Z
    .locals 0

    if-eqz p1, :cond_0

    .line 1114
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private onStartSearch(Ljava/lang/String;Z)V
    .locals 2

    const-string v0, "possibleFilter"

    .line 911
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 912
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isTabletAndPredefinedTicketsEnabled()Z

    move-result v0

    const-string v1, "group-list-presenter-selected-section-search"

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->selectedSection:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 916
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->processDeathHelper:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$ProcessDeathHelper;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$ProcessDeathHelper;->shouldReapplySearchFilter()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->searchFilter:Ljava/lang/String;

    .line 919
    :cond_1
    iput-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->searchFilter:Ljava/lang/String;

    .line 921
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->selectedSection:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 928
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 930
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 931
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->showMasterDetailSearchView(Z)V

    :cond_2
    return-void

    .line 938
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasPendingTicketSearch()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    .line 939
    invoke-virtual {v1}, Lcom/squareup/ui/ticket/TicketListPresenter;->getSearchFilter()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    return-void

    :cond_4
    if-nez p2, :cond_6

    .line 948
    iget-object p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    .line 949
    invoke-virtual {p2}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result p2

    if-eqz p2, :cond_6

    iget-object p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    .line 950
    invoke-virtual {p2}, Lcom/squareup/ui/ticket/TicketListPresenter;->getTicketCursorSearchText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_6

    if-eqz v0, :cond_5

    .line 958
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 959
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->showMasterDetailSearchView(Z)V

    :cond_5
    return-void

    .line 966
    :cond_6
    iget-object p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->performSearch(Ljava/lang/String;)V

    return-void
.end method

.method private refreshEditActionButtons(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V
    .locals 3

    .line 589
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isExitingTicketActionPath()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 594
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasSelectedTickets()Z

    move-result v0

    .line 596
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isBulkDeleteAllowed()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setBulkDeleteButtonVisible(Z)V

    .line 597
    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setBulkDeleteButtonEnabled(Z)V

    .line 599
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasTicketPrinter()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setBulkReprintTicketButtonVisible(Z)V

    .line 600
    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setBulkReprintTicketButtonEnabled(Z)V

    .line 602
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isBulkVoidAllowed()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setBulkVoidButtonVisible(Z)V

    .line 603
    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setBulkVoidButtonEnabled(Z)V

    .line 605
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isBulkMergeAllowed()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setBulkMergeButtonVisible(Z)V

    .line 606
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/TicketListPresenter;->getSelectedCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p1, v2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setBulkMergeButtonEnabled(Z)V

    .line 608
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isBulkMoveAllowed()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setBulkMoveButtonVisible(Z)V

    .line 609
    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setBulkMoveButtonEnabled(Z)V

    .line 611
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isBulkTransferAllowed()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setBulkTransferButtonVisible(Z)V

    .line 612
    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setBulkTransferButtonEnabled(Z)V

    return-void
.end method

.method private refreshEditBarAndTicketList(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V
    .locals 3

    .line 570
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isExitingTicketActionPath()Z

    move-result v0

    if-nez v0, :cond_2

    .line 571
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isResettingScreenAfterAdditiveMerge()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 575
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isInEditMode()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 576
    invoke-virtual {p1, v2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setActionBarVisible(Z)V

    .line 577
    invoke-virtual {p1, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setEditBarVisible(Z)V

    .line 578
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->refreshEditActionButtons(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    goto :goto_0

    .line 580
    :cond_1
    invoke-virtual {p1, v2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setEditBarVisible(Z)V

    .line 581
    invoke-virtual {p1, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setActionBarVisible(Z)V

    .line 582
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->clearSelectedTickets()V

    .line 585
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->updateView()V

    :cond_2
    :goto_1
    return-void
.end method

.method private refreshEditButtonUponTicketRemoval(Lcom/squareup/ui/ticket/MasterDetailTicketView;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MasterDetailTicketView;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 623
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->useOpenTicketsHomeScreen:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->allTicketsRemoved(Ljava/util/List;)Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x0

    const/4 v0, 0x1

    .line 624
    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setEditButtonVisible(ZZ)V

    :cond_0
    return-void
.end method

.method private resetScreenAfterAdditiveMerge()V
    .locals 2

    const/4 v0, 0x1

    .line 1058
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->resettingScreenAfterAdditiveMerge:Z

    .line 1059
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketModeScreen:Lcom/squareup/ui/ticket/TicketModeScreen;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->LOAD_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    invoke-interface {v0, v1}, Lcom/squareup/ui/ticket/TicketModeScreen;->setTicketMode(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;)V

    .line 1060
    sget-object v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->LOAD_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    .line 1061
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1064
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    .line 1065
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->canEditTickets()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1066
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->listenForEditModeUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 1068
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->shouldShowLogOutButton()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1069
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeInLogOutButton()V

    :cond_2
    return-void
.end method

.method private shouldShowLogOutButton()Z
    .locals 2

    .line 806
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->useOpenTicketsHomeScreen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->predefinedTicketsEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->lockOrClockButtonHelper:Lcom/squareup/ui/main/LockOrClockButtonHelper;

    .line 809
    invoke-virtual {v0}, Lcom/squareup/ui/main/LockOrClockButtonHelper;->shouldShowLockButtonWithoutClock()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->LOAD_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private updateViewForTicketCounts()V
    .locals 2

    .line 792
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 796
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketCountsCache;->hasTickets()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasTickets:Z

    .line 797
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasTickets:Z

    if-nez v0, :cond_1

    .line 798
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->setInEditMode(Z)V

    .line 800
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->canEditTickets()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_2

    .line 801
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    iget-boolean v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasTickets:Z

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setEditButtonVisible(Z)V

    :cond_2
    return-void
.end method


# virtual methods
.method assertConsistentState()V
    .locals 4

    .line 1118
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->displayMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;->MASTER_DETAIL:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-eq v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->useOpenTicketsHomeScreen:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "DisplayMode.DETAIL_ONLY is incompatible with the open tickets home screen (unless merging with the currently loaded transaction ticket!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 1123
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->displayMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;->MASTER_DETAIL:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-eq v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->predefinedTicketsEnabled:Z

    if-nez v0, :cond_3

    :cond_2
    const/4 v2, 0x1

    :cond_3
    const-string v0, "DisplayMode.DETAIL_ONLY is incompatible with predefined tickets (unless merging with the currently loaded transaction ticket)!"

    invoke-static {v2, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    return-void
.end method

.method exit()V
    .locals 4

    .line 749
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 750
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 752
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method getActionBarTitle()Ljava/lang/String;
    .locals 3

    .line 440
    sget-object v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$5;->$SwitchMap$com$squareup$ui$ticket$MasterDetailTicketPresenter$TicketMode:[I

    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 451
    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_merge_with:I

    goto :goto_0

    .line 454
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized ticket mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 448
    :cond_1
    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_save_items_to:I

    goto :goto_0

    .line 445
    :cond_2
    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_tickets:I

    goto :goto_0

    .line 442
    :cond_3
    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_tickets:I

    .line 456
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getNoTicketsTitle()Ljava/lang/String;
    .locals 5

    .line 858
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->selectedSection:Ljava/lang/String;

    const-string v1, "group-list-presenter-selected-section-all-tickets"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 859
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->predefined_tickets_no_tickets_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 862
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->selectedSection:Ljava/lang/String;

    const-string v1, "group-list-presenter-selected-section-custom-tickets"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "group"

    if-eqz v0, :cond_1

    .line 863
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->predefined_tickets_no_group_tickets_title:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->predefined_tickets_custom:I

    .line 864
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 865
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 868
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->groupPresenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/GroupListPresenter;->getGroupEntries()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 869
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->selectedSection:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 870
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->predefined_tickets_no_group_tickets_title:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 871
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 872
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 876
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->predefined_tickets_no_tickets_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getSanitizedSortType()Lcom/squareup/tickets/TicketSort;
    .locals 1

    .line 723
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getSanitizedSortType()Lcom/squareup/tickets/TicketSort;

    move-result-object v0

    return-object v0
.end method

.method isInEditMode()Z
    .locals 1

    .line 549
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isInEditMode()Z

    move-result v0

    return v0
.end method

.method isMergingWithTransactionTicket()Z
    .locals 2

    .line 496
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isTabletAndPredefinedTicketsEnabled()Z
    .locals 2

    .line 782
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->displayMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;->MASTER_DETAIL:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->predefinedTicketsEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isTicketSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z
    .locals 1

    .line 833
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->isTicketCheckboxSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$additivelyMergeToTicket$27$MasterDetailTicketPresenter()V
    .locals 4

    .line 997
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->loadingTicketPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v1, Lcom/squareup/caller/ProgressPopup$Progress;

    iget-object v2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->open_tickets_saving:I

    .line 998
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/caller/ProgressPopup$Progress;-><init>(Ljava/lang/String;)V

    .line 997
    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method public synthetic lambda$additivelyMergeToTicket$28$MasterDetailTicketPresenter(Lcom/squareup/payment/Order;Lcom/squareup/tickets/TicketsResult;)V
    .locals 5

    .line 1004
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/caller/BlockingPopupPresenter;->dismiss()V

    .line 1005
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->loadingTicketPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dismiss()V

    .line 1006
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1007
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 1011
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    invoke-virtual {v0}, Lcom/squareup/log/tickets/OpenTicketsLogger;->stopTicketInCartUi()V

    .line 1012
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-interface {p2}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/tickets/Tickets$MergeResults;

    iget-object p2, p2, Lcom/squareup/tickets/Tickets$MergeResults;->ticketName:Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->printStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    .line 1014
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhone()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1015
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryScreenState;->setPendingTicketSavedAlert()V

    .line 1018
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->bus:Lcom/squareup/badbus/BadEventSink;

    new-instance p2, Lcom/squareup/payment/OrderEntryEvents$TicketSaved;

    invoke-direct {p2}, Lcom/squareup/payment/OrderEntryEvents$TicketSaved;-><init>()V

    invoke-interface {p1, p2}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 1019
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->reset()V

    .line 1021
    iget-boolean p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->useOpenTicketsHomeScreen:Z

    if-eqz p1, :cond_9

    .line 1022
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setTicketListClickable(Z)V

    .line 1025
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->resetScreenAfterAdditiveMerge()V

    const/4 p1, 0x0

    new-array v0, p1, [Ljava/lang/Object;

    const-string v1, "[Master_Detail_Ticket_Presenter] Hitting local store to get a ticket cursor to set in connection with additive merge..."

    .line 1027
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1030
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->selectedSection:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, -0x5300cf84

    const/4 v4, 0x2

    if-eq v2, v3, :cond_4

    const p1, -0x50eee4a1

    if-eq v2, p1, :cond_3

    const p1, 0x7668fc67

    if-eq v2, p1, :cond_2

    goto :goto_0

    :cond_2
    const-string p1, "group-list-presenter-selected-section-all-tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    const/4 p1, 0x1

    goto :goto_1

    :cond_3
    const-string p1, "group-list-presenter-selected-section-custom-tickets"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    const/4 p1, 0x2

    goto :goto_1

    :cond_4
    const-string v2, "group-list-presenter-selected-section-search"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_1

    :cond_5
    :goto_0
    const/4 p1, -0x1

    :goto_1
    if-eqz p1, :cond_8

    if-eq p1, p2, :cond_7

    if-eq p1, v4, :cond_6

    .line 1042
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->selectedSection:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->loadTicketsForGroup(Ljava/lang/String;)V

    goto :goto_2

    .line 1039
    :cond_6
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->loadCustomTickets()V

    goto :goto_2

    .line 1036
    :cond_7
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->loadAllTickets()V

    goto :goto_2

    .line 1032
    :cond_8
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->cancelTicketQueryCallback()V

    .line 1033
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->searchFilter:Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->onStartSearch(Ljava/lang/String;Z)V

    .line 1045
    :goto_2
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    .line 1046
    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->updateActionBarTitle(Z)V

    .line 1048
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->canEditTickets()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasTickets:Z

    if-eqz v0, :cond_a

    .line 1049
    invoke-virtual {p1, p2, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setEditButtonVisible(ZZ)V

    goto :goto_3

    .line 1052
    :cond_9
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->exit()V

    :cond_a
    :goto_3
    return-void
.end method

.method public synthetic lambda$listenForAdditiveMergeUpdates$26$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;)Lrx/Subscription;
    .locals 2

    .line 971
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketCreatedListener:Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;->onNewTicketCreated()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$5yraHa1BJ1oeqSileggj9_Lgbro;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$5yraHa1BJ1oeqSileggj9_Lgbro;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 972
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$listenForEditModeUpdates$10$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;)Lrx/Subscription;
    .locals 2

    .line 543
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->onVoidTicketsRequested()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$aQQyWPMoqKINeztosMBwilWSUCg;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$aQQyWPMoqKINeztosMBwilWSUCg;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 544
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$listenForEditModeUpdates$4$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;)Lrx/Subscription;
    .locals 2

    .line 513
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->onEditModeChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$cwBzj-KlPNqx8Q8MUYQ_fGpajZc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$cwBzj-KlPNqx8Q8MUYQ_fGpajZc;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 514
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$listenForEditModeUpdates$6$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;)Lrx/Subscription;
    .locals 2

    .line 534
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->onSelectedTicketsChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$mrCW60NC1ZrOCuLnaJTlVuwTQSE;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$mrCW60NC1ZrOCuLnaJTlVuwTQSE;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 535
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$listenForEditModeUpdates$8$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;)Lrx/Subscription;
    .locals 2

    .line 538
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->onDeleteTicketsRequested()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$9_setitLwHQC2JUiJnLPBWNe7JM;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$9_setitLwHQC2JUiJnLPBWNe7JM;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 539
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$listenForGroupListUpdates$20$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;)Lrx/Subscription;
    .locals 2

    .line 768
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->groupListListener:Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;->onSectionSelected()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$5GdFbCu-9p8JLJHdWiYONfQKj8Q;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$5GdFbCu-9p8JLJHdWiYONfQKj8Q;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 769
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$listenForMergeTransactionTicketUpdates$2$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;)Lrx/Subscription;
    .locals 2

    .line 490
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->onSelectedTicketsChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$4rK9-XIX3lbLyd4bSYKd_0ogR9k;

    invoke-direct {v1, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$4rK9-XIX3lbLyd4bSYKd_0ogR9k;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 491
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$listenForMergeUpdates$12$MasterDetailTicketPresenter()Lrx/Subscription;
    .locals 2

    .line 675
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->mergeTicketListener:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;->onMergeCompleted()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$S95mMtng2iF4jBf63XUdhcavjkk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$S95mMtng2iF4jBf63XUdhcavjkk;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    .line 676
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$listenForMoveUpdates$14$MasterDetailTicketPresenter()Lrx/Subscription;
    .locals 2

    .line 696
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->moveTicketListener:Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;->onMoveTicketsCompleted()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$kOp-ZkqI8RicMnHhqQPnJPqcQSI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$kOp-ZkqI8RicMnHhqQPnJPqcQSI;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    .line 697
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$listenForNavigationUpdates$16$MasterDetailTicketPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 736
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$APQ4eV-zFxUtQN8dqt5lKIpeMxk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$APQ4eV-zFxUtQN8dqt5lKIpeMxk;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$listenForTicketCountUpdates$22$MasterDetailTicketPresenter()Lrx/Subscription;
    .locals 2

    .line 787
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketCountsCache;->onTicketCountsRefreshed()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$Vs4rw2veyhcR1XJ4beO-hhbunSM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$Vs4rw2veyhcR1XJ4beO-hhbunSM;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    .line 788
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$listenForTicketListUpdates$24$MasterDetailTicketPresenter()Lrx/Subscription;
    .locals 2

    .line 815
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketListListener:Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;->readOnlyTicketCursor()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$Zja13jk2B-vELmQMxpc-48ZZIBE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$Zja13jk2B-vELmQMxpc-48ZZIBE;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    .line 816
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$listenForTicketPermissionUpdates$18$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;)Lrx/Subscription;
    .locals 2

    .line 757
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketScopeRunner:Lcom/squareup/ui/ticket/TicketScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketScopeRunner;->onPermittedToViewAllTicketsChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$rhti_32VRZF_YbXDeuPD-YCzz5M;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$rhti_32VRZF_YbXDeuPD-YCzz5M;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 758
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$loadAsTransactionTicket$29$MasterDetailTicketPresenter()V
    .locals 4

    .line 1087
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->loadingTicketPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v1, Lcom/squareup/caller/ProgressPopup$Progress;

    iget-object v2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->open_tickets_loading:I

    .line 1088
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/caller/ProgressPopup$Progress;-><init>(Ljava/lang/String;)V

    .line 1087
    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method public synthetic lambda$loadAsTransactionTicket$30$MasterDetailTicketPresenter(Lcom/squareup/tickets/TicketsResult;)V
    .locals 4

    .line 1092
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/caller/BlockingPopupPresenter;->dismiss()V

    .line 1093
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->loadingTicketPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dismiss()V

    .line 1094
    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tickets/OpenTicket;

    .line 1095
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1096
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 1098
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 1105
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->setTicket(Lcom/squareup/tickets/OpenTicket;)V

    return-void
.end method

.method public synthetic lambda$null$11$MasterDetailTicketPresenter(Ljava/lang/Boolean;)V
    .locals 0

    .line 677
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 678
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->refreshTickets()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$13$MasterDetailTicketPresenter(Ljava/lang/Boolean;)V
    .locals 0

    .line 697
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->refreshTickets()V

    return-void
.end method

.method public synthetic lambda$null$15$MasterDetailTicketPresenter(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 736
    iput-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->currentScreen:Lcom/squareup/container/ContainerTreeKey;

    return-void
.end method

.method public synthetic lambda$null$17$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;Ljava/lang/Boolean;)V
    .locals 1

    .line 759
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->permittedToViewAllTickets:Z

    .line 760
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    .line 761
    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setPhoneEmployeesFilterVisible(Z)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$19$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;Ljava/lang/String;)V
    .locals 3

    const-string v0, "group-list-presenter-selected-section-search"

    .line 770
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ""

    .line 771
    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->searchFilter:Ljava/lang/String;

    .line 773
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$9PyBfzihCE8kAw_XfNtx0BCmtJw;

    invoke-direct {v0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$9PyBfzihCE8kAw_XfNtx0BCmtJw;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    const-wide/16 v1, 0xc8

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 775
    :cond_0
    iput-object p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->selectedSection:Ljava/lang/String;

    .line 776
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/TicketListPresenter;->setSelectionFilter(Ljava/lang/String;)V

    .line 777
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->refreshTickets()V

    return-void
.end method

.method public synthetic lambda$null$21$MasterDetailTicketPresenter(Lkotlin/Unit;)V
    .locals 0

    .line 788
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->updateViewForTicketCounts()V

    return-void
.end method

.method public synthetic lambda$null$23$MasterDetailTicketPresenter(Lcom/squareup/tickets/TicketRowCursorList;)V
    .locals 1

    .line 817
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->currentScreen:Lcom/squareup/container/ContainerTreeKey;

    instance-of v0, p1, Lcom/squareup/ui/ticket/MergeTicketScreen;

    if-nez v0, :cond_0

    instance-of p1, p1, Lcom/squareup/ui/ticket/MoveTicketScreen;

    if-eqz p1, :cond_1

    .line 827
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->pruneSelectedTickets()V

    :cond_1
    return-void
.end method

.method public synthetic lambda$null$25$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;Ljava/lang/Boolean;)V
    .locals 0

    .line 973
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_0

    iget-boolean p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->useOpenTicketsHomeScreen:Z

    if-eqz p2, :cond_0

    .line 981
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->resetScreenAfterAdditiveMerge()V

    .line 982
    iget-object p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/TicketListPresenter;->refreshTickets()V

    const/4 p2, 0x1

    .line 983
    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->updateActionBarTitle(Z)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$3$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;Ljava/lang/Boolean;)V
    .locals 1

    .line 515
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->useOpenTicketsHomeScreen:Z

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-nez p2, :cond_0

    .line 521
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    new-instance p2, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$1;

    invoke-direct {p2, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$1;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/TicketListPresenter;->refreshTickets(Lcom/squareup/ui/ticket/TicketListPresenter$TicketsRefreshedCallback;)V

    goto :goto_0

    .line 529
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->refreshEditBarAndTicketList(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$5$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;Ljava/util/List;)V
    .locals 0

    .line 535
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->refreshEditActionButtons(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    return-void
.end method

.method public synthetic lambda$null$7$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;Ljava/util/List;)V
    .locals 0

    .line 540
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->refreshEditButtonUponTicketRemoval(Lcom/squareup/ui/ticket/MasterDetailTicketView;Ljava/util/List;)V

    return-void
.end method

.method public synthetic lambda$null$9$MasterDetailTicketPresenter(Lcom/squareup/ui/ticket/MasterDetailTicketView;Ljava/util/List;)V
    .locals 0

    .line 545
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->refreshEditButtonUponTicketRemoval(Lcom/squareup/ui/ticket/MasterDetailTicketView;Ljava/util/List;)V

    return-void
.end method

.method public synthetic lambda$onLoad$0$MasterDetailTicketPresenter(Ljava/lang/String;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 3

    .line 349
    iget-object p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->LOAD_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne p2, v0, :cond_0

    .line 350
    invoke-static {}, Lcom/squareup/ui/ticket/EditTicketScreen;->forCreatingEmptyNewTicket()Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object p2

    goto :goto_0

    .line 351
    :cond_0
    invoke-static {}, Lcom/squareup/ui/ticket/EditTicketScreen;->forSavingTransactionToNewTicket()Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object p2

    .line 352
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->getSelectedGroup()Lcom/squareup/api/items/TicketGroup;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    .line 353
    :goto_1
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->flow:Lflow/Flow;

    .line 354
    invoke-virtual {p2, p1}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->startFromSwipe(Ljava/lang/String;)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->groupPresenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    .line 355
    invoke-virtual {p2}, Lcom/squareup/ui/ticket/GroupListPresenter;->getGroupCount()I

    move-result p2

    const/4 v2, 0x1

    if-ne p2, v2, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {p1, v0, v2}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->preselectedGroup(Lcom/squareup/api/items/TicketGroup;Z)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object p1

    .line 356
    invoke-virtual {p1}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->buildTicketDetailScreen()Lcom/squareup/ui/ticket/TicketDetailScreen;

    move-result-object p1

    .line 353
    invoke-virtual {v1, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 357
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method onBackPressed()Z
    .locals 2

    .line 740
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->BULK_EDIT_TICKETS:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-eq v0, v1, :cond_0

    .line 741
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->onCancelEditClicked()V

    goto :goto_0

    .line 743
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->exit()V

    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method onBulkDeleteClicked()V
    .locals 3

    .line 638
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_DELETE:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 639
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->passcodeGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_TICKET_VOID:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$2;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method onBulkMergeClicked()V
    .locals 2

    .line 690
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_MERGE:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 691
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/ticket/MergeTicketScreen;->INSTANCE:Lcom/squareup/ui/ticket/MergeTicketScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onBulkMoveClicked()V
    .locals 3

    .line 705
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_MOVE:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 706
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/ticket/MoveTicketScreen;

    iget-object v2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v2}, Lcom/squareup/ui/ticket/TicketListPresenter;->getSelectedTicketsInfo()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/ticket/MoveTicketScreen;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onBulkReprintTicketClicked()V
    .locals 2

    .line 656
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_REPRINT_TICKET:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 657
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->reprintSelectedTickets()V

    .line 658
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->setInEditMode(Z)V

    return-void
.end method

.method onBulkTransferClicked()V
    .locals 3

    .line 714
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_TRANSFER:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 715
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->passcodeGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_TICKET_MANAGE_ALL:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$4;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$4;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method onBulkVoidClicked()V
    .locals 3

    .line 666
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->passcodeGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_TICKET_VOID:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$3;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$3;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method onCancelEditClicked()V
    .locals 2

    .line 566
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->setInEditMode(Z)V

    return-void
.end method

.method onEditClicked()V
    .locals 2

    .line 562
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->setInEditMode(Z)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 321
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->assertConsistentState()V

    .line 322
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->cardNameHandler:Lcom/squareup/tickets/TicketCardNameHandler;

    invoke-interface {v0, p1}, Lcom/squareup/tickets/TicketCardNameHandler;->registerToScope(Lmortar/MortarScope;)V

    .line 323
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketModeScreen;

    iput-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketModeScreen:Lcom/squareup/ui/ticket/TicketModeScreen;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 327
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 328
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Shown MasterDetailTicketScreen"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    const-string v0, ""

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p1, :cond_0

    const-string v3, "master-detail-ticket-presenter-has-tickets-key"

    .line 331
    invoke-virtual {p1, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasTickets:Z

    const-string v3, "master-detail-ticket-presenter-search-filter-key"

    .line 332
    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->searchFilter:Ljava/lang/String;

    const-string v0, "master-detail-ticket-presenter-resetting-screen-after-additive-merge-key"

    .line 334
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->resettingScreenAfterAdditiveMerge:Z

    .line 335
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasTicketCursor()Z

    move-result v0

    if-nez v0, :cond_1

    .line 336
    iput-boolean v2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->firstTimeLoadingTickets:Z

    const-string v0, "master-detail-ticket-presenter-can-edit-tickets-after-process-death-key"

    .line 338
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->canEditTicketsAfterProcessDeath:Z

    goto :goto_0

    .line 341
    :cond_0
    iput-boolean v2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->firstTimeLoadingTickets:Z

    .line 342
    iget-object v3, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    invoke-virtual {v3}, Lcom/squareup/tickets/TicketCountsCache;->hasTickets()Z

    move-result v3

    iput-boolean v3, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasTickets:Z

    .line 343
    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->searchFilter:Ljava/lang/String;

    .line 344
    iput-boolean v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->resettingScreenAfterAdditiveMerge:Z

    .line 347
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v0

    .line 348
    iget-object v3, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->cardNameHandler:Lcom/squareup/tickets/TicketCardNameHandler;

    new-instance v4, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$G2Sw8ndIh9VpMHTRFmP2hdK30H4;

    invoke-direct {v4, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketPresenter$G2Sw8ndIh9VpMHTRFmP2hdK30H4;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    invoke-interface {v3, v0, v4}, Lcom/squareup/tickets/TicketCardNameHandler;->setCallback(Lmortar/MortarScope;Lkotlin/jvm/functions/Function2;)V

    .line 360
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    .line 361
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->getSelectedSection()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->selectedSection:Ljava/lang/String;

    .line 363
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isTabletAndPredefinedTicketsEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 365
    invoke-virtual {v0, v2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setNewTicketButtonVisible(Z)V

    .line 366
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->listenForGroupListUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 369
    :cond_2
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->listenForTicketPermissionUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 370
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->listenForTicketCountUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 371
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->listenForTicketListUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 372
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->listenForAdditiveMergeUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 373
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->listenForMergeUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 374
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->listenForMoveUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 375
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->listenForNavigationUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 383
    iget-object v3, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v4, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v3, v4, :cond_3

    .line 384
    invoke-virtual {v0, v2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setMergeTransactionTicketButtonVisible(Z)V

    .line 385
    iget-object v3, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v3}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasSelectedTickets()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setMergeTransactionTicketButtonEnabled(Z)V

    .line 386
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->listenForMergeTransactionTicketUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 393
    :cond_3
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->canEditTickets()Z

    move-result v3

    if-nez v3, :cond_4

    iget-boolean v3, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->canEditTicketsAfterProcessDeath:Z

    if-eqz v3, :cond_6

    .line 394
    :cond_4
    iput-boolean v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->canEditTicketsAfterProcessDeath:Z

    .line 396
    iget-object v3, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v4, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->BULK_EDIT_TICKETS:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v3, v4, :cond_5

    .line 397
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->setInEditMode(Z)V

    .line 398
    invoke-virtual {v0, v2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setEditBarAlwaysVisible(Z)V

    goto :goto_1

    .line 400
    :cond_5
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setEditBarAlwaysVisible(Z)V

    .line 401
    iget-boolean v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasTickets:Z

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setEditButtonVisible(Z)V

    .line 404
    :goto_1
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->listenForEditModeUpdates(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 407
    :cond_6
    iget-boolean v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->permittedToViewAllTickets:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 408
    invoke-virtual {v0, v2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setPhoneEmployeesFilterVisible(Z)V

    .line 411
    :cond_7
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-boolean v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->predefinedTicketsEnabled:Z

    if-nez v1, :cond_8

    .line 413
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->hideGroupListContainer()V

    .line 416
    :cond_8
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->shouldShowLogOutButton()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 417
    invoke-virtual {v0, v2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setLogOutButtonVisible(Z)V

    .line 421
    :cond_9
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->loadingAfterProcessDeath(Landroid/os/Bundle;)Z

    move-result p1

    if-eqz p1, :cond_a

    .line 422
    new-instance p1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$ProcessDeathHelper;

    invoke-direct {p1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$ProcessDeathHelper;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    iput-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->processDeathHelper:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$ProcessDeathHelper;

    .line 423
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isTabletAndPredefinedTicketsEnabled()Z

    move-result p1

    if-nez p1, :cond_a

    .line 424
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->searchFilter:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->onStartSearch(Ljava/lang/String;)V

    :cond_a
    return-void
.end method

.method onMergeTransactionTicketClicked()V
    .locals 2

    .line 504
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketSelection;->ticketInfoFromTransaction(Lcom/squareup/payment/Transaction;)Lcom/squareup/ui/ticket/TicketInfo;

    move-result-object v0

    .line 505
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->isTicketCheckboxSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 506
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->addSelectedTicket(Lcom/squareup/ui/ticket/TicketInfo;)V

    .line 508
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/ticket/MergeTicketScreen;->INSTANCE:Lcom/squareup/ui/ticket/MergeTicketScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onNewTicketClicked()V
    .locals 4

    .line 462
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketCountsCache;->getAllTicketsCount()I

    move-result v0

    const/16 v1, 0x1f4

    if-lt v0, v1, :cond_0

    .line 463
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->flow:Lflow/Flow;

    invoke-static {v1}, Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen;->forLimit(I)Lcom/squareup/ui/ticket/TooManyTicketsErrorDialogScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 468
    :cond_0
    sget-object v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$5;->$SwitchMap$com$squareup$ui$ticket$MasterDetailTicketPresenter$TicketMode:[I

    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    .line 473
    invoke-static {}, Lcom/squareup/ui/ticket/EditTicketScreen;->forSavingTransactionToNewTicket()Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object v0

    goto :goto_0

    .line 476
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized ticket mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 470
    :cond_2
    invoke-static {}, Lcom/squareup/ui/ticket/EditTicketScreen;->forCreatingEmptyNewTicket()Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object v0

    .line 479
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->hasView()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    invoke-virtual {v2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->getSelectedGroup()Lcom/squareup/api/items/TicketGroup;

    move-result-object v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    .line 480
    :goto_1
    iget-object v3, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->groupPresenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {v3}, Lcom/squareup/ui/ticket/GroupListPresenter;->getGroupCount()I

    move-result v3

    if-ne v3, v1, :cond_4

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v0, v2, v1}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->preselectedGroup(Lcom/squareup/api/items/TicketGroup;Z)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    .line 481
    iget-boolean v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->predefinedTicketsEnabled:Z

    if-eqz v1, :cond_5

    .line 482
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->buildNewTicketScreen()Lcom/squareup/ui/ticket/NewTicketScreen;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_3

    .line 484
    :cond_5
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->buildTicketDetailScreen()Lcom/squareup/ui/ticket/TicketDetailScreen;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_3
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 430
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 431
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketCountsCache;->hasTickets()Z

    move-result v0

    const-string v1, "master-detail-ticket-presenter-has-tickets-key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 432
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->canEditTickets()Z

    move-result v0

    const-string v1, "master-detail-ticket-presenter-can-edit-tickets-after-process-death-key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 433
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->searchFilter:Ljava/lang/String;

    const-string v1, "master-detail-ticket-presenter-search-filter-key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->resettingScreenAfterAdditiveMerge:Z

    const-string v1, "master-detail-ticket-presenter-resetting-screen-after-additive-merge-key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method onSortChange(Lcom/squareup/tickets/TicketSort;)V
    .locals 1

    .line 727
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->onSortChange(Lcom/squareup/tickets/TicketSort;)V

    return-void
.end method

.method onStartSearch(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 900
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->onStartSearch(Ljava/lang/String;Z)V

    return-void
.end method

.method onTicketClicked(Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 4

    .line 837
    iget-object v0, p1, Lcom/squareup/ui/ticket/TicketInfo;->id:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const-string v3, "[Master_Detail_Ticket_Presenter] Ticket with id %s clicked."

    .line 838
    invoke-static {v3, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 839
    iget-object v2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v3, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->SAVE_TO_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v2, v3, :cond_0

    .line 840
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isInEditMode()Z

    move-result p1

    xor-int/2addr p1, v1

    const-string v1, "Can\'t save items to a ticket while in edit mode!"

    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 843
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->additivelyMergeToTicket(Ljava/lang/String;)V

    .line 844
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MERGED_CART_TO_EXISTING_TICKET:Lcom/squareup/analytics/RegisterActionName;

    invoke-virtual {p1, v0}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logTicketAction(Lcom/squareup/analytics/RegisterActionName;)V

    goto :goto_1

    .line 845
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->alwaysShowTicketCheckboxes()Z

    move-result v1

    if-nez v1, :cond_2

    .line 846
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->canEditTickets()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isInEditMode()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 850
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    invoke-virtual {p1, v0}, Lcom/squareup/log/tickets/OpenTicketsLogger;->startTicketSelectedToCartUi(Ljava/lang/String;)V

    .line 852
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->loadAsTransactionTicket(Ljava/lang/String;)V

    .line 853
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_LOADED_EXISTING_TICKET:Lcom/squareup/analytics/RegisterActionName;

    invoke-virtual {p1, v0}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logTicketAction(Lcom/squareup/analytics/RegisterActionName;)V

    goto :goto_1

    .line 847
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->toggleSelected(Lcom/squareup/ui/ticket/TicketInfo;)V

    .line 848
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->updateView()V

    :goto_1
    return-void
.end method

.method onTicketsProgressHidden()V
    .locals 4

    .line 885
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isTabletAndPredefinedTicketsEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 886
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    iget-boolean v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->firstTimeLoadingTickets:Z

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->showDetailOnlyTicketList(Z)V

    goto :goto_0

    .line 887
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->selectedSection:Ljava/lang/String;

    const-string v1, "group-list-presenter-selected-section-search"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 888
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->searchFilter:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 889
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    iget-boolean v2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->firstTimeLoadingTickets:Z

    invoke-virtual {v1, v0, v2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->showMasterDetailSearchView(ZZ)V

    goto :goto_0

    .line 890
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->isTicketCursorEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 891
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getNoTicketsTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v2}, Lcom/squareup/ui/ticket/TicketListPresenter;->ticketCursorHasError()Z

    move-result v2

    iget-boolean v3, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->firstTimeLoadingTickets:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->showNoTicketsMessage(Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 894
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MasterDetailTicketView;

    iget-boolean v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->firstTimeLoadingTickets:Z

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->showMasterDetailTicketList(Z)V

    :goto_0
    const/4 v0, 0x0

    .line 896
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->firstTimeLoadingTickets:Z

    return-void
.end method
