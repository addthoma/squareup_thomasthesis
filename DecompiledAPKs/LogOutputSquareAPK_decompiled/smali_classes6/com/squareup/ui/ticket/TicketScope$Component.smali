.class public interface abstract Lcom/squareup/ui/ticket/TicketScope$Component;
.super Ljava/lang/Object;
.source "TicketScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/ticket/TicketScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract moveTicket(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;)Lcom/squareup/ui/ticket/MoveTicketScreen$Component;
.end method

.method public abstract newTicketScreen()Lcom/squareup/ui/ticket/NewTicketScreen$Component;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/ticket/TicketScopeRunner;
.end method

.method public abstract splitTicketScreen()Lcom/squareup/ui/ticket/SplitTicketScreen$Component;
.end method

.method public abstract ticketActionPath()Lcom/squareup/ui/ticket/TicketActionScope$Component;
.end method

.method public abstract ticketComp()Lcom/squareup/ui/ticket/TicketCompScreen$Component;
.end method

.method public abstract ticketDetailScreen()Lcom/squareup/ui/ticket/TicketDetailScreen$Component;
.end method

.method public abstract ticketVoid()Lcom/squareup/ui/ticket/TicketVoidScreen$Component;
.end method
