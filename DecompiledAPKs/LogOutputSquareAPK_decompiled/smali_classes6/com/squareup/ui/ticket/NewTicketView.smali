.class public Lcom/squareup/ui/ticket/NewTicketView;
.super Landroid/widget/LinearLayout;
.source "NewTicketView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;,
        Lcom/squareup/ui/ticket/NewTicketView$HolderType;,
        Lcom/squareup/ui/ticket/NewTicketView$RowType;
    }
.end annotation


# instance fields
.field employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/ticket/NewTicketPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private recyclerViewAdapter:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

.field private searchBar:Lcom/squareup/ui/XableEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 312
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 313
    const-class p2, Lcom/squareup/ui/ticket/NewTicketScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/NewTicketScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/ticket/NewTicketScreen$Component;->inject(Lcom/squareup/ui/ticket/NewTicketView;)V

    return-void
.end method

.method static synthetic access$700(Lcom/squareup/ui/ticket/NewTicketView;)Lcom/squareup/ui/XableEditText;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/ui/ticket/NewTicketView;->searchBar:Lcom/squareup/ui/XableEditText;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 368
    sget v0, Lcom/squareup/orderentry/R$id;->ticket_template_search:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView;->searchBar:Lcom/squareup/ui/XableEditText;

    .line 369
    sget v0, Lcom/squareup/orderentry/R$id;->recycler_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 339
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 1

    .line 343
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView;->presenter:Lcom/squareup/ui/ticket/NewTicketPresenter;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/NewTicketView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;

    move-result-object p1

    return-object p1
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .line 317
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 318
    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketView;->bindViews()V

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/NewTicketView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 320
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketView;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v1}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;-><init>(Lcom/squareup/ui/ticket/NewTicketView;Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;)V

    iput-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView;->recyclerViewAdapter:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    .line 321
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketView;->recyclerViewAdapter:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView;->presenter:Lcom/squareup/ui/ticket/NewTicketPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->takeView(Ljava/lang/Object;)V

    .line 325
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView;->searchBar:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketView;->presenter:Lcom/squareup/ui/ticket/NewTicketPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/NewTicketPresenter;->getSearchText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 326
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView;->searchBar:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/ticket/NewTicketView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/NewTicketView$1;-><init>(Lcom/squareup/ui/ticket/NewTicketView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 347
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView;->presenter:Lcom/squareup/ui/ticket/NewTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->onPressUp()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 334
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView;->presenter:Lcom/squareup/ui/ticket/NewTicketPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->dropView(Ljava/lang/Object;)V

    .line 335
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected setCustomTicketButtonVisible(Z)V
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView;->recyclerViewAdapter:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->setCustomTicketButtonRowVisible(Z)V

    return-void
.end method

.method protected setSearchBarVisible(Z)V
    .locals 1

    .line 351
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method protected setTicketGroups(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;)V"
        }
    .end annotation

    .line 359
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView;->recyclerViewAdapter:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->setTicketGroups(Ljava/util/List;)V

    return-void
.end method

.method protected setTicketTemplates(Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;Z)V"
        }
    .end annotation

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView;->recyclerViewAdapter:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->setTicketTemplates(Ljava/util/List;Z)V

    return-void
.end method
