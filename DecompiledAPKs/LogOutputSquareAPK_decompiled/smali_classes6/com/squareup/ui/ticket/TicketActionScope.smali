.class public final Lcom/squareup/ui/ticket/TicketActionScope;
.super Lcom/squareup/ui/ticket/InTicketScope;
.source "TicketActionScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/ticket/TicketActionScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/TicketActionScope$Module;,
        Lcom/squareup/ui/ticket/TicketActionScope$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/TicketActionScope;",
            ">;"
        }
    .end annotation
.end field

.field static final INSTANCE:Lcom/squareup/ui/ticket/TicketActionScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/ticket/TicketActionScope;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/TicketActionScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/ticket/TicketActionScope;->INSTANCE:Lcom/squareup/ui/ticket/TicketActionScope;

    .line 98
    sget-object v0, Lcom/squareup/ui/ticket/TicketActionScope;->INSTANCE:Lcom/squareup/ui/ticket/TicketActionScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/ticket/TicketActionScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/ticket/InTicketScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 47
    const-class v0, Lcom/squareup/ui/ticket/TicketActionScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketActionScope$Component;

    .line 48
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p1

    .line 49
    invoke-interface {v0}, Lcom/squareup/ui/ticket/TicketActionScope$Component;->ticketActionScopeRunner()Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    return-void
.end method
