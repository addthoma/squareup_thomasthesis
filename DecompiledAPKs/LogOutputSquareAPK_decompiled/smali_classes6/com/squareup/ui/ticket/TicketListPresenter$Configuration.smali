.class Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;
.super Ljava/lang/Object;
.source "TicketListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Configuration"
.end annotation


# instance fields
.field private final displayMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

.field private final isPhone:Z

.field private final isSavedCartsMode:Z

.field private final predefinedTicketsEnabled:Z

.field private final ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

.field private final ticketsToMove:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;Ljava/util/List;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Device;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Device;",
            ")V"
        }
    .end annotation

    .line 745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 746
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->displayMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    .line 747
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    .line 748
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->ticketsToMove:Ljava/util/List;

    .line 749
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->transaction:Lcom/squareup/payment/Transaction;

    .line 750
    invoke-interface {p4}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->predefinedTicketsEnabled:Z

    .line 751
    invoke-interface {p4}, Lcom/squareup/tickets/OpenTicketsSettings;->isSavedCartsMode()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->isSavedCartsMode:Z

    .line 752
    invoke-interface {p6}, Lcom/squareup/util/Device;->isPhone()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->isPhone:Z

    return-void
.end method


# virtual methods
.method getExcludedTicketIds()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 798
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 799
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 800
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCurrentTicket()Lcom/squareup/tickets/OpenTicket;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0

    .line 804
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MOVE_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v0, v1, :cond_2

    .line 810
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 811
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 812
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCurrentTicket()Lcom/squareup/tickets/OpenTicket;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0

    .line 815
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->ticketsToMove:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 816
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->ticketsToMove:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketInfo;->extractIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method resetSortSettingAsNeeded(Lcom/squareup/settings/LocalSetting;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;)V"
        }
    .end annotation

    .line 756
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MOVE_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v0, v1, :cond_0

    .line 757
    sget-object v0, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method showInlineSearchBar()Z
    .locals 2

    .line 770
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->isPhone:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->displayMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;->MASTER_DETAIL:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->predefinedTicketsEnabled:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method showNewTicketButton()Z
    .locals 2

    .line 778
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->showInlineSearchBar()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MOVE_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->isSavedCartsMode:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method showTextRow()Z
    .locals 2

    .line 774
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MOVE_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method showTicketTemplates()Z
    .locals 2

    .line 785
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MOVE_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method usesDefaultAllTicketsFilter()Z
    .locals 1

    .line 762
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;->predefinedTicketsEnabled:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
