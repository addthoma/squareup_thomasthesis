.class Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;
.super Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
.source "BaseTicketListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ButtonRowHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
        ">.RangerHolder;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 340
    iput-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    .line 341
    sget v0, Lcom/squareup/orderentry/R$layout;->ticket_list_button_row:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;-><init>(Lcom/squareup/ui/RangerRecyclerAdapter;Landroid/view/ViewGroup;I)V

    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;II)V
    .locals 3

    .line 345
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;->itemView()Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 346
    invoke-virtual {p2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p3

    check-cast p3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 347
    sget-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketRow:[I

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 365
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_view_all_tickets:I

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(I)V

    .line 367
    iput v2, p3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 368
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-static {p1}, Lcom/squareup/ui/ticket/BaseTicketListView;->access$900(Lcom/squareup/ui/ticket/BaseTicketListView;)I

    move-result p1

    iput p1, p3, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 369
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 371
    new-instance p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder$2;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;)V

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 379
    :cond_0
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unrecognized row type: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 349
    :cond_1
    sget p1, Lcom/squareup/orderentry/R$id;->open_tickets_new_ticket_button:I

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setId(I)V

    .line 350
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_ticket_new_ticket:I

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(I)V

    .line 353
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object p1, p1, Lcom/squareup/ui/ticket/BaseTicketListView;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhone()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-static {p1}, Lcom/squareup/ui/ticket/BaseTicketListView;->access$900(Lcom/squareup/ui/ticket/BaseTicketListView;)I

    move-result p1

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-static {p1}, Lcom/squareup/ui/ticket/BaseTicketListView;->access$600(Lcom/squareup/ui/ticket/BaseTicketListView;)I

    move-result p1

    :goto_0
    iput p1, p3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 354
    iput v2, p3, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 355
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 357
    new-instance p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder$1;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;)V

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 338
    check-cast p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;->bindRow(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;II)V

    return-void
.end method
