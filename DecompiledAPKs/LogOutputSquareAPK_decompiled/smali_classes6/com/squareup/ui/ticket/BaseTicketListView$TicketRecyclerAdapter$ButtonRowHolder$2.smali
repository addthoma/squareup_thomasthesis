.class Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "BaseTicketListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;->bindRow(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;)V
    .locals 0

    .line 371
    iput-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder$2;->this$2:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 373
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder$2;->this$2:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;

    iget-object p1, p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ButtonRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object p1, p1, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->onViewAllTicketsButtonClicked()V

    return-void
.end method
