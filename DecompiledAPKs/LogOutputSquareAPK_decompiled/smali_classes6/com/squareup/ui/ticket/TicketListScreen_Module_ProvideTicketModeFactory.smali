.class public final Lcom/squareup/ui/ticket/TicketListScreen_Module_ProvideTicketModeFactory;
.super Ljava/lang/Object;
.source "TicketListScreen_Module_ProvideTicketModeFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/ticket/TicketListScreen$Module;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/ticket/TicketListScreen$Module;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListScreen_Module_ProvideTicketModeFactory;->module:Lcom/squareup/ui/ticket/TicketListScreen$Module;

    return-void
.end method

.method public static create(Lcom/squareup/ui/ticket/TicketListScreen$Module;)Lcom/squareup/ui/ticket/TicketListScreen_Module_ProvideTicketModeFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/ui/ticket/TicketListScreen_Module_ProvideTicketModeFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketListScreen_Module_ProvideTicketModeFactory;-><init>(Lcom/squareup/ui/ticket/TicketListScreen$Module;)V

    return-object v0
.end method

.method public static provideTicketMode(Lcom/squareup/ui/ticket/TicketListScreen$Module;)Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListScreen$Module;->provideTicketMode()Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListScreen_Module_ProvideTicketModeFactory;->module:Lcom/squareup/ui/ticket/TicketListScreen$Module;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketListScreen_Module_ProvideTicketModeFactory;->provideTicketMode(Lcom/squareup/ui/ticket/TicketListScreen$Module;)Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListScreen_Module_ProvideTicketModeFactory;->get()Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    move-result-object v0

    return-object v0
.end method
