.class public final Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideDisplayModeFactory;
.super Ljava/lang/Object;
.source "MoveTicketScreen_Module_ProvideDisplayModeFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideDisplayModeFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    return-void
.end method

.method public static create(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;)Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideDisplayModeFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideDisplayModeFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideDisplayModeFactory;-><init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;)V

    return-object v0
.end method

.method public static provideDisplayMode(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;)Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketScreen$Module;->provideDisplayMode()Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideDisplayModeFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    invoke-static {v0}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideDisplayModeFactory;->provideDisplayMode(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;)Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideDisplayModeFactory;->get()Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    move-result-object v0

    return-object v0
.end method
