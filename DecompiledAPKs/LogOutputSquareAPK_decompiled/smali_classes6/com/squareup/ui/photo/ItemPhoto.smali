.class public abstract Lcom/squareup/ui/photo/ItemPhoto;
.super Ljava/lang/Object;
.source "ItemPhoto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;,
        Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;,
        Lcom/squareup/ui/photo/ItemPhoto$Loader;,
        Lcom/squareup/ui/photo/ItemPhoto$Factory;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/photo/ItemPhoto$1;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/squareup/ui/photo/ItemPhoto;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract cancel(Lcom/squareup/picasso/Target;)V
.end method

.method public getItemId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract hasUrl()Z
.end method

.method public abstract into(ILandroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract into(ILcom/squareup/picasso/Target;)V
.end method

.method public abstract withNewUri(Landroid/net/Uri;)Lcom/squareup/ui/photo/ItemPhoto;
.end method
