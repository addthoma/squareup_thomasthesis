.class public final Lcom/squareup/ui/tour/LearnMoreTourPopupView_MembersInjector;
.super Ljava/lang/Object;
.source "LearnMoreTourPopupView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/tour/LearnMoreTourPopupView;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/tour/LearnMoreTourPopupView;",
            ">;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/ui/tour/LearnMoreTourPopupView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/tour/LearnMoreTourPopupView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectDevice(Lcom/squareup/ui/tour/LearnMoreTourPopupView;Lcom/squareup/util/Device;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/tour/LearnMoreTourPopupView;Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView_MembersInjector;->injectPresenter(Lcom/squareup/ui/tour/LearnMoreTourPopupView;Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView_MembersInjector;->injectDevice(Lcom/squareup/ui/tour/LearnMoreTourPopupView;Lcom/squareup/util/Device;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/tour/LearnMoreTourPopupView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/tour/LearnMoreTourPopupView_MembersInjector;->injectMembers(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)V

    return-void
.end method
