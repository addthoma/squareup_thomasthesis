.class Lcom/squareup/ui/print/PrintErrorPopupView$FooterRowHolder;
.super Lcom/squareup/ui/print/PrintErrorPopupView$PrinterErrorHolder;
.source "PrintErrorPopupView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/print/PrintErrorPopupView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FooterRowHolder"
.end annotation


# static fields
.field static final FOOTER_ROW:I = 0x1


# instance fields
.field private message:Lcom/squareup/widgets/MessageView;

.field final synthetic this$0:Lcom/squareup/ui/print/PrintErrorPopupView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/print/PrintErrorPopupView;Landroid/view/ViewGroup;)V
    .locals 0

    .line 263
    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$FooterRowHolder;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    .line 264
    sget p1, Lcom/squareup/print/popup/error/impl/R$layout;->print_error_footer_row:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/print/PrintErrorPopupView$PrinterErrorHolder;-><init>(Landroid/view/View;)V

    .line 265
    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$FooterRowHolder;->itemView:Landroid/view/View;

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$FooterRowHolder;->message:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method bindRow(I)V
    .locals 2

    .line 269
    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$FooterRowHolder;->message:Lcom/squareup/widgets/MessageView;

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$FooterRowHolder;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    iget-object v0, v0, Lcom/squareup/ui/print/PrintErrorPopupView;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$FooterRowHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->getHelpMessage(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
