.class Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;
.super Lcom/squareup/ui/print/PrintErrorPopupView$PrinterErrorHolder;
.source "PrintErrorPopupView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/print/PrintErrorPopupView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ErrorRowHolder"
.end annotation


# static fields
.field static final ERROR_ROW:I


# instance fields
.field private final affected:Landroid/widget/TextView;

.field private final button:Landroid/widget/TextView;

.field private final darkGrayTextColor:I

.field private final lightGrayTextColor:I

.field private final message:Landroid/widget/TextView;

.field private final redTextColor:I

.field private final reprint:Ljava/lang/String;

.field private final spinner:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

.field private final title:Landroid/widget/TextView;

.field private final tryAgain:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/print/PrintErrorPopupView;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 165
    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    .line 166
    invoke-direct {p0, p2}, Lcom/squareup/ui/print/PrintErrorPopupView$PrinterErrorHolder;-><init>(Landroid/view/View;)V

    .line 167
    iget-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/print/popup/error/impl/R$id;->error_retry_row_title:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->title:Landroid/widget/TextView;

    .line 168
    iget-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/print/popup/error/impl/R$id;->error_retry_row_message:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->message:Landroid/widget/TextView;

    .line 169
    iget-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/print/popup/error/impl/R$id;->error_retry_row_affected:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->affected:Landroid/widget/TextView;

    .line 170
    iget-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/print/popup/error/impl/R$id;->error_retry_row_spinner:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->spinner:Landroid/view/View;

    .line 171
    iget-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/print/popup/error/impl/R$id;->error_retry_row_button:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->button:Landroid/widget/TextView;

    .line 173
    invoke-virtual {p1}, Lcom/squareup/ui/print/PrintErrorPopupView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 174
    sget p2, Lcom/squareup/marin/R$color;->marin_red:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->redTextColor:I

    .line 175
    sget p2, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->lightGrayTextColor:I

    .line 176
    sget p2, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->darkGrayTextColor:I

    .line 178
    iput-object p3, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->reprint:Ljava/lang/String;

    .line 179
    iput-object p4, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->tryAgain:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;)Landroid/widget/TextView;
    .locals 0

    .line 148
    iget-object p0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->button:Landroid/widget/TextView;

    return-object p0
.end method

.method private setTitleGrayTextColor()V
    .locals 2

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->title:Landroid/widget/TextView;

    iget v1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->lightGrayTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private setTitleNormalTextColor()V
    .locals 2

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->title:Landroid/widget/TextView;

    iget v1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->darkGrayTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private setTitleRedTextColor()V
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->title:Landroid/widget/TextView;

    iget v1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->redTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private showButtonReprint()V
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->button:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->button:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->reprint:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->button:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->spinner:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private showButtonTryAgain()V
    .locals 2

    .line 250
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->button:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->button:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->tryAgain:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->button:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->spinner:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private showSpinner()V
    .locals 2

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->button:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->spinner:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method bindRow(I)V
    .locals 2

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    iget-object v0, v0, Lcom/squareup/ui/print/PrintErrorPopupView;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->shouldShowReprintWarning(I)Z

    move-result v0

    .line 198
    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    iget-object v1, v1, Lcom/squareup/ui/print/PrintErrorPopupView;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->showSpinner(I)Z

    move-result v1

    if-eqz v0, :cond_0

    .line 201
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->showButtonTryAgain()V

    .line 202
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->setTitleRedTextColor()V

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    .line 204
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->showSpinner()V

    .line 205
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->setTitleGrayTextColor()V

    goto :goto_0

    .line 207
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->setTitleNormalTextColor()V

    .line 208
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->showButtonReprint()V

    .line 211
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    iget-object v1, v1, Lcom/squareup/ui/print/PrintErrorPopupView;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->getTitleText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->message:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    iget-object v1, v1, Lcom/squareup/ui/print/PrintErrorPopupView;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->getMessageText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->affected:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    iget-object v1, v1, Lcom/squareup/ui/print/PrintErrorPopupView;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->getAffectedText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->button:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder$1;-><init>(Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;I)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
