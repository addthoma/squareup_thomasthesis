.class public Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "TimeTrackingSettingsCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final dashboardBaseUrl:Ljava/lang/String;

.field private mainScheduler:Lio/reactivex/Scheduler;

.field private res:Lcom/squareup/util/Res;

.field private scopeRunner:Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;

.field private timeTrackingSettingsEnableSwitch:Lcom/squareup/noho/NohoCheckableRow;

.field private timeTrackingSettingsEnableSwitchDescription:Lcom/squareup/widgets/MessageView;

.field private view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;Lio/reactivex/Scheduler;Lcom/squareup/http/Server;)V
    .locals 0
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 38
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->res:Lcom/squareup/util/Res;

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;

    .line 41
    iput-object p3, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 42
    sget-object p2, Lcom/squareup/http/Endpoints$Server;->STAGING:Lcom/squareup/http/Endpoints$Server;

    invoke-virtual {p4}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Lcom/squareup/http/Endpoints;->getServerFromUrl(Ljava/lang/String;)Lcom/squareup/http/Endpoints$Server;

    move-result-object p3

    if-ne p2, p3, :cond_0

    sget p2, Lcom/squareup/registerlib/R$string;->staging_base_url:I

    .line 43
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget p2, Lcom/squareup/registerlib/R$string;->prod_base_url:I

    .line 44
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->dashboardBaseUrl:Ljava/lang/String;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 62
    sget v0, Lcom/squareup/settingsapplet/R$id;->time_tracking_settings_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 65
    sget v0, Lcom/squareup/settingsapplet/R$id;->time_tracking_settings_enable_switch:I

    .line 66
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->timeTrackingSettingsEnableSwitch:Lcom/squareup/noho/NohoCheckableRow;

    .line 67
    sget v0, Lcom/squareup/settingsapplet/R$id;->time_tracking_settings_enable_switch_description:I

    .line 68
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->timeTrackingSettingsEnableSwitchDescription:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private getActionBarConfig(Z)Lcom/squareup/noho/NohoActionBar$Config;
    .locals 3

    .line 80
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/settingsapplet/R$string;->time_tracking_settings_section_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    .line 81
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 84
    sget-object p1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v1, Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsCoordinator$De6CjXzAW6Tndc5LwYcbuLStHeE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsCoordinator$De6CjXzAW6Tndc5LwYcbuLStHeE;-><init>(Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;)V

    invoke-virtual {v0, p1, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 89
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 91
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method private getTimeTrackingSwitchDescription()Ljava/lang/CharSequence;
    .locals 3

    .line 95
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/settingsapplet/R$string;->time_tracking_settings_enable_switch_description:I

    const-string v2, "link_to_dashboard"

    .line 96
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/registerlib/R$string;->dashboard_timecards_relative_url:I

    .line 97
    invoke-direct {p0, v1}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->getUrl(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/settingsapplet/R$string;->employee_management_passcode_description_link_text:I

    .line 98
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private getUrl(I)Ljava/lang/String;
    .locals 3

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/registerlib/R$string;->square_relative_url:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->dashboardBaseUrl:Ljava/lang/String;

    const-string v2, "base_url"

    .line 104
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->res:Lcom/squareup/util/Res;

    .line 105
    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "link_relative_url"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 106
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic lambda$yTOuXbPLouIv3Eh9OI6PKHWljcQ(Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen$Data;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->showScreenData(Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen$Data;)V

    return-void
.end method

.method private showScreenData(Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen$Data;)V
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    iget-boolean v1, p1, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen$Data;->showBackButton:Z

    invoke-direct {p0, v1}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->getActionBarConfig(Z)Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->timeTrackingSettingsEnableSwitch:Lcom/squareup/noho/NohoCheckableRow;

    iget-object p1, p1, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen$Data;->timeTrackingSettingsState:Lcom/squareup/permissions/TimeTrackingSettings$State;

    iget-boolean p1, p1, Lcom/squareup/permissions/TimeTrackingSettings$State;->timeTrackingEnabled:Z

    .line 75
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->timeTrackingSettingsEnableSwitchDescription:Lcom/squareup/widgets/MessageView;

    invoke-direct {p0}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->getTimeTrackingSwitchDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->view:Landroid/view/View;

    .line 49
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->bindViews(Landroid/view/View;)V

    .line 53
    new-instance v0, Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsCoordinator$NxMjAKTyzbH_bKad5SjjkTIPjQE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsCoordinator$NxMjAKTyzbH_bKad5SjjkTIPjQE;-><init>(Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 57
    iget-object p1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->timeTrackingSettingsEnableSwitch:Lcom/squareup/noho/NohoCheckableRow;

    new-instance v0, Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsCoordinator$ndcgefarKy6l3hE5uv0wrlyJJ7c;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsCoordinator$ndcgefarKy6l3hE5uv0wrlyJJ7c;-><init>(Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public synthetic lambda$attach$0$TimeTrackingSettingsCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;->timeTrackingSettingsScreenData()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 54
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsCoordinator$yTOuXbPLouIv3Eh9OI6PKHWljcQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsCoordinator$yTOuXbPLouIv3Eh9OI6PKHWljcQ;-><init>(Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;)V

    .line 55
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$1$TimeTrackingSettingsCoordinator(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0

    .line 58
    iget-object p1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;->onTimeTrackingSettingsSwitchChanged(Z)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$getActionBarConfig$2$TimeTrackingSettingsCoordinator()Lkotlin/Unit;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;->onTimeTrackingSettingsBackClicked()V

    .line 86
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method
