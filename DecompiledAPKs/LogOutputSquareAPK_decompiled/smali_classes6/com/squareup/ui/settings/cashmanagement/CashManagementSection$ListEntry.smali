.class public Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.source "CashManagementSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation


# instance fields
.field private final cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/cashmanagement/CashManagementSettings;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 38
    sget-object v2, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->CHECKOUT_OPTIONS:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    sget v3, Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;->TITLE_ID:I

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    return-void
.end method


# virtual methods
.method public getValueText()Ljava/lang/String;
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v1}, Lcom/squareup/cashmanagement/CashManagementSettings;->isCashManagementEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/settingsapplet/R$string;->cash_management_on:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/settingsapplet/R$string;->cash_management_off:I

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
