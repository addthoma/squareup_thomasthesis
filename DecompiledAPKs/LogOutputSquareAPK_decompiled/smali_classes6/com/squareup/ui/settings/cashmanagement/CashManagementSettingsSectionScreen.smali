.class public final Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen;
.super Lcom/squareup/settings/cashmanagement/InCashManagementScope;
.source "CashManagementSettingsSectionScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Component;,
        Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 134
    sget-object v0, Lcom/squareup/ui/settings/cashmanagement/-$$Lambda$CashManagementSettingsSectionScreen$1XxqUEl1zLvu8K2dW3cOzalVyuQ;->INSTANCE:Lcom/squareup/ui/settings/cashmanagement/-$$Lambda$CashManagementSettingsSectionScreen$1XxqUEl1zLvu8K2dW3cOzalVyuQ;

    .line 135
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/settings/cashmanagement/CashManagementScope;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/settings/cashmanagement/InCashManagementScope;-><init>(Lcom/squareup/settings/cashmanagement/CashManagementScope;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/settings/cashmanagement/CashManagementScope;

    invoke-direct {v0, p1}, Lcom/squareup/settings/cashmanagement/CashManagementScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen;-><init>(Lcom/squareup/settings/cashmanagement/CashManagementScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen;
    .locals 1

    .line 136
    const-class v0, Lcom/squareup/settings/cashmanagement/CashManagementScope;

    .line 137
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/cashmanagement/CashManagementScope;

    .line 138
    new-instance v0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen;-><init>(Lcom/squareup/settings/cashmanagement/CashManagementScope;)V

    return-object v0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 130
    invoke-super {p0, p1, p2}, Lcom/squareup/settings/cashmanagement/InCashManagementScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 131
    invoke-virtual {p0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen;->getParentKey()Lcom/squareup/settings/cashmanagement/CashManagementScope;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 126
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_CASH:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 43
    const-class v0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 122
    sget v0, Lcom/squareup/settingsapplet/R$layout;->cash_management_settings_section_view:I

    return v0
.end method
