.class public Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "CreateOrEditTeamPasscodeSuccessCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private continueButton:Lcom/squareup/noho/NohoButton;

.field private final device:Lcom/squareup/util/Device;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private progressBar:Landroid/widget/LinearLayout;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

.field private successContainer:Lcom/squareup/noho/NohoLinearLayout;

.field private successDescription:Lcom/squareup/marketfont/MarketTextView;

.field private view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lio/reactivex/Scheduler;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V
    .locals 0
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 41
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->res:Lcom/squareup/util/Res;

    .line 43
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 44
    iput-object p3, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->device:Lcom/squareup/util/Device;

    .line 45
    iput-object p4, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->view:Landroid/view/View;

    sget v1, Lcom/squareup/settingsapplet/R$id;->create_passcode_success_action_bar:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->view:Landroid/view/View;

    sget v1, Lcom/squareup/settingsapplet/R$id;->create_passcode_success_progress_bar:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->progressBar:Landroid/widget/LinearLayout;

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->view:Landroid/view/View;

    sget v1, Lcom/squareup/settingsapplet/R$id;->create_passcode_success_container:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->successContainer:Lcom/squareup/noho/NohoLinearLayout;

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->view:Landroid/view/View;

    sget v1, Lcom/squareup/settingsapplet/R$id;->create_passcode_success_description:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->successDescription:Lcom/squareup/marketfont/MarketTextView;

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->view:Landroid/view/View;

    sget v1, Lcom/squareup/settingsapplet/R$id;->create_passcode_success_button:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->continueButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method private getLoadingActionBarConfig(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;)Lcom/squareup/noho/NohoActionBar$Config;
    .locals 2

    .line 107
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 108
    iget-boolean p1, p1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;->passcodePreviouslyCreated:Z

    if-nez p1, :cond_0

    .line 109
    new-instance p1, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_team_passcode_title:I

    invoke-direct {p1, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 111
    :cond_0
    new-instance p1, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_edit_team_passcode_title:I

    invoke-direct {p1, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 113
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method private getSuccessActionBarConfig(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;)Lcom/squareup/noho/NohoActionBar$Config;
    .locals 3

    .line 118
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeSuccessCoordinator$6YRydCy8wQiaoWGecjAMOIyWiHY;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeSuccessCoordinator$6YRydCy8wQiaoWGecjAMOIyWiHY;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;)V

    .line 119
    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 121
    iget-boolean p1, p1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;->passcodePreviouslyCreated:Z

    if-nez p1, :cond_0

    .line 122
    new-instance p1, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_team_passcode_title:I

    invoke-direct {p1, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 124
    :cond_0
    new-instance p1, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_edit_team_passcode_title:I

    invoke-direct {p1, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 126
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$showScreenData$1()V
    .locals 0

    return-void
.end method

.method public static synthetic lambda$zwk-I1_AT8jmTFIiFh83lWN2GQE(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->showScreenData(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;)V

    return-void
.end method

.method private showScreenData(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;)V
    .locals 5

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 69
    sget-object v0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator$1;->$SwitchMap$com$squareup$permissions$PasscodesSettings$RequestState:[I

    iget-object v2, p1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;->status:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    invoke-virtual {v2}, Lcom/squareup/permissions/PasscodesSettings$RequestState;->ordinal()I

    move-result v2

    aget v0, v0, v2

    const/4 v2, 0x1

    const/16 v3, 0x8

    if-eq v0, v2, :cond_4

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 p1, 0x3

    if-eq v0, p1, :cond_0

    const/4 p1, 0x4

    if-eq v0, p1, :cond_0

    goto/16 :goto_0

    .line 101
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onCreateOrEditTeamPasscodeError()V

    goto/16 :goto_0

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->view:Landroid/view/View;

    new-instance v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeSuccessCoordinator$M1WE27ekFwZgKOrbzWAP_gUMjfI;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeSuccessCoordinator$M1WE27ekFwZgKOrbzWAP_gUMjfI;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;)V

    invoke-static {v0, v2}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->getSuccessActionBarConfig(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;)Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->progressBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->successContainer:Lcom/squareup/noho/NohoLinearLayout;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->successDescription:Lcom/squareup/marketfont/MarketTextView;

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_team_passcode_success_description:I

    invoke-interface {v2, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTabletLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->continueButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {p1, v3}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    goto :goto_0

    .line 93
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->continueButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->continueButton:Lcom/squareup/noho/NohoButton;

    new-instance v1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeSuccessCoordinator$MBVZWCRamBP_AusCYEIejZ-FIJU;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeSuccessCoordinator$MBVZWCRamBP_AusCYEIejZ-FIJU;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 73
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->view:Landroid/view/View;

    sget-object v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeSuccessCoordinator$sJZvATp5wKMD5alGTgsL9-XrCts;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeSuccessCoordinator$sJZvATp5wKMD5alGTgsL9-XrCts;

    invoke-static {v0, v2}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->getLoadingActionBarConfig(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;)Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->progressBar:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->successContainer:Lcom/squareup/noho/NohoLinearLayout;

    invoke-virtual {p1, v3}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->view:Landroid/view/View;

    .line 50
    invoke-direct {p0}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->bindViews()V

    .line 53
    new-instance v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeSuccessCoordinator$3jSICNCa76PRfn1rhAo5Z0yLK3o;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeSuccessCoordinator$3jSICNCa76PRfn1rhAo5Z0yLK3o;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$CreateOrEditTeamPasscodeSuccessCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->createOrEditTeamPasscodeSuccessScreenData()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 54
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeSuccessCoordinator$zwk-I1_AT8jmTFIiFh83lWN2GQE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeSuccessCoordinator$zwk-I1_AT8jmTFIiFh83lWN2GQE;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;)V

    .line 55
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$getSuccessActionBarConfig$4$CreateOrEditTeamPasscodeSuccessCoordinator(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;)Lkotlin/Unit;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    iget-boolean p1, p1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;->passcodePreviouslyCreated:Z

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onCreateOrEditTeamPasscodeFinish(Z)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$showScreenData$2$CreateOrEditTeamPasscodeSuccessCoordinator(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    iget-boolean p1, p1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;->passcodePreviouslyCreated:Z

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onCreateOrEditTeamPasscodeFinish(Z)Lkotlin/Unit;

    return-void
.end method

.method public synthetic lambda$showScreenData$3$CreateOrEditTeamPasscodeSuccessCoordinator(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;Landroid/view/View;)V
    .locals 0

    .line 95
    iget-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    iget-boolean p1, p1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;->passcodePreviouslyCreated:Z

    invoke-virtual {p2, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onCreateOrEditTeamPasscodeFinish(Z)Lkotlin/Unit;

    return-void
.end method
