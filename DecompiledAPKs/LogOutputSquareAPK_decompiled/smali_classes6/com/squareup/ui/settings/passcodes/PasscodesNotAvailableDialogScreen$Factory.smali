.class public Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen$Factory;
.super Ljava/lang/Object;
.source "PasscodesNotAvailableDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lflow/Flow;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 46
    invoke-virtual {p0}, Lflow/Flow;->goBack()Z

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 40
    const-class v0, Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen$Component;

    invoke-interface {v0}, Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen$Component;->flow()Lflow/Flow;

    move-result-object v0

    .line 42
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/common/strings/R$string;->network_error_title:I

    .line 43
    invoke-virtual {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_not_available_dialog_message:I

    .line 44
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_not_available_dialog_button_text:I

    new-instance v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesNotAvailableDialogScreen$Factory$iAMaYAJfZDJRP5k3HkvzjF7ipGI;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesNotAvailableDialogScreen$Factory$iAMaYAJfZDJRP5k3HkvzjF7ipGI;-><init>(Lflow/Flow;)V

    .line 45
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 47
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 42
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
