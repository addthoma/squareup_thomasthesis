.class public final Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;
.super Ljava/lang/Object;
.source "CreateOrEditTeamPasscodeScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# instance fields
.field final expectedPasscodeLength:I

.field final passcodePreviouslyCreated:Z

.field final requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

.field final unsavedTeamPasscode:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;ILcom/squareup/permissions/PasscodesSettings$RequestState;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-boolean p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;->passcodePreviouslyCreated:Z

    .line 45
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;->unsavedTeamPasscode:Ljava/lang/String;

    .line 46
    iput p3, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;->expectedPasscodeLength:I

    .line 47
    iput-object p4, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;->requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    return-void
.end method
