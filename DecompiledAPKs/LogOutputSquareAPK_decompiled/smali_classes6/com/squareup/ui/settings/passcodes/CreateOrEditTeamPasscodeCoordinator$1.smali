.class Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator$1;
.super Lcom/squareup/padlock/Padlock$OnKeyPressListenerAdapter;
.source "CreateOrEditTeamPasscodeCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->showScreenData(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator$1;->this$0:Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock$OnKeyPressListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackspaceClicked()V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator$1;->this$0:Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->access$000(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onTeamPasscodeBackspaceClicked()V

    return-void
.end method

.method public onDigitClicked(I)V
    .locals 1

    .line 73
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result p1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator$1;->this$0:Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->access$000(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onTeamPasscodeDigitEntered(C)V

    return-void
.end method
