.class public final Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection_Factory;
.super Ljava/lang/Object;
.source "OrderHubPrintingSettingsSection_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;-><init>(Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {v0}, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection_Factory;->newInstance(Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection_Factory;->get()Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;

    move-result-object v0

    return-object v0
.end method
