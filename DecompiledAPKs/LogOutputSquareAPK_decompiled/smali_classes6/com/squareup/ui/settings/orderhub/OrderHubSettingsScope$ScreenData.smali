.class public final Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;
.super Ljava/lang/Object;
.source "OrderHubSettingsScope.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\u000f\u001a\u00020\u0003H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0010J\u000e\u0010\u0011\u001a\u00020\u0005H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0012J\u000e\u0010\u0013\u001a\u00020\u0003H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0014J\u000e\u0010\u0015\u001a\u00020\u0003H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0016J1\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00032\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0006\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0014\u0010\u0007\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000c\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;",
        "",
        "notificationsEnabled",
        "",
        "notificationFrequency",
        "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
        "printingEnabled",
        "quickActionsEnabled",
        "(ZLcom/squareup/orderhub/settings/OrderHubAlertsFrequency;ZZ)V",
        "getNotificationFrequency$settings_applet_release",
        "()Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
        "getNotificationsEnabled$settings_applet_release",
        "()Z",
        "getPrintingEnabled$settings_applet_release",
        "getQuickActionsEnabled$settings_applet_release",
        "component1",
        "component1$settings_applet_release",
        "component2",
        "component2$settings_applet_release",
        "component3",
        "component3$settings_applet_release",
        "component4",
        "component4$settings_applet_release",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final notificationFrequency:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

.field private final notificationsEnabled:Z

.field private final printingEnabled:Z

.field private final quickActionsEnabled:Z


# direct methods
.method public constructor <init>(ZLcom/squareup/orderhub/settings/OrderHubAlertsFrequency;ZZ)V
    .locals 1

    const-string v0, "notificationFrequency"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationsEnabled:Z

    iput-object p2, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationFrequency:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    iput-boolean p3, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->printingEnabled:Z

    iput-boolean p4, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->quickActionsEnabled:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;ZLcom/squareup/orderhub/settings/OrderHubAlertsFrequency;ZZILjava/lang/Object;)Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-boolean p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationsEnabled:Z

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationFrequency:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-boolean p3, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->printingEnabled:Z

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->quickActionsEnabled:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->copy(ZLcom/squareup/orderhub/settings/OrderHubAlertsFrequency;ZZ)Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1$settings_applet_release()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationsEnabled:Z

    return v0
.end method

.method public final component2$settings_applet_release()Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationFrequency:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    return-object v0
.end method

.method public final component3$settings_applet_release()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->printingEnabled:Z

    return v0
.end method

.method public final component4$settings_applet_release()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->quickActionsEnabled:Z

    return v0
.end method

.method public final copy(ZLcom/squareup/orderhub/settings/OrderHubAlertsFrequency;ZZ)Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;
    .locals 1

    const-string v0, "notificationFrequency"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;-><init>(ZLcom/squareup/orderhub/settings/OrderHubAlertsFrequency;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;

    iget-boolean v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationsEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationsEnabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationFrequency:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    iget-object v1, p1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationFrequency:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->printingEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->printingEnabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->quickActionsEnabled:Z

    iget-boolean p1, p1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->quickActionsEnabled:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNotificationFrequency$settings_applet_release()Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationFrequency:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    return-object v0
.end method

.method public final getNotificationsEnabled$settings_applet_release()Z
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationsEnabled:Z

    return v0
.end method

.method public final getPrintingEnabled$settings_applet_release()Z
    .locals 1

    .line 19
    iget-boolean v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->printingEnabled:Z

    return v0
.end method

.method public final getQuickActionsEnabled$settings_applet_release()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->quickActionsEnabled:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationsEnabled:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationFrequency:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->printingEnabled:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->quickActionsEnabled:Z

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_3
    move v1, v2

    :goto_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ScreenData(notificationsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", notificationFrequency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->notificationFrequency:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", printingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->printingEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", quickActionsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->quickActionsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
