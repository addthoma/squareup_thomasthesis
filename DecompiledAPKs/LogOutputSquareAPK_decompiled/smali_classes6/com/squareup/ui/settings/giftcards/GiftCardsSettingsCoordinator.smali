.class public final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "GiftCardsSettingsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGiftCardsSettingsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GiftCardsSettingsCoordinator.kt\ncom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,260:1\n1103#2,7:261\n*E\n*S KotlinDebug\n*F\n+ 1 GiftCardsSettingsCoordinator.kt\ncom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator\n*L\n106#1,7:261\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008d\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001B\u0018\u00002\u00020\u0001:\u0001MB5\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0010\u0010H\u001a\u00020I2\u0006\u0010G\u001a\u00020\u001bH\u0016J\u0010\u0010J\u001a\u00020I2\u0006\u0010K\u001a\u00020LH\u0002R#\u0010\u000e\u001a\n \u0010*\u0004\u0018\u00010\u000f0\u000f8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0013\u0010\u0014\u001a\u0004\u0008\u0011\u0010\u0012R\u001b\u0010\u0015\u001a\u00020\u00168BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0019\u0010\u0014\u001a\u0004\u0008\u0017\u0010\u0018R\u001b\u0010\u001a\u001a\u00020\u001b8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001e\u0010\u0014\u001a\u0004\u0008\u001c\u0010\u001dR\u001b\u0010\u001f\u001a\u00020 8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008#\u0010\u0014\u001a\u0004\u0008!\u0010\"R\u001b\u0010$\u001a\u00020%8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008(\u0010\u0014\u001a\u0004\u0008&\u0010\'R\u001b\u0010)\u001a\u00020*8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008-\u0010\u0014\u001a\u0004\u0008+\u0010,R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010.\u001a\u00020/8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u00082\u0010\u0014\u001a\u0004\u00080\u00101R\u0012\u00103\u001a\u000604R\u00020\u0000X\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u00105\u001a\u00020/8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u00087\u0010\u0014\u001a\u0004\u00086\u00101R\u0012\u00108\u001a\u000604R\u00020\u0000X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u00109\u001a\u00020:8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008=\u0010\u0014\u001a\u0004\u0008;\u0010<R\u001b\u0010>\u001a\u00020/8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008@\u0010\u0014\u001a\u0004\u0008?\u00101R\u0010\u0010A\u001a\u00020BX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010CR\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010D\u001a\u00020\u001b8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008F\u0010\u0014\u001a\u0004\u0008E\u0010\u001dR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010G\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006N"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;",
        "res",
        "Lcom/squareup/util/Res;",
        "errorsBarPresenter",
        "Lcom/squareup/ui/ErrorsBarPresenter;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "kotlin.jvm.PlatformType",
        "getActionBar",
        "()Lcom/squareup/marin/widgets/MarinActionBar;",
        "actionBar$delegate",
        "Lkotlin/Lazy;",
        "animTimeMs",
        "",
        "getAnimTimeMs",
        "()I",
        "animTimeMs$delegate",
        "content",
        "Landroid/view/View;",
        "getContent",
        "()Landroid/view/View;",
        "content$delegate",
        "designsRow",
        "Lcom/squareup/noho/NohoRow;",
        "getDesignsRow",
        "()Lcom/squareup/noho/NohoRow;",
        "designsRow$delegate",
        "eGiftCardContent",
        "Landroid/widget/LinearLayout;",
        "getEGiftCardContent",
        "()Landroid/widget/LinearLayout;",
        "eGiftCardContent$delegate",
        "eGiftCardInPosToggle",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "getEGiftCardInPosToggle",
        "()Lcom/squareup/noho/NohoCheckableRow;",
        "eGiftCardInPosToggle$delegate",
        "maxAmount",
        "Lcom/squareup/noho/NohoEditText;",
        "getMaxAmount",
        "()Lcom/squareup/noho/NohoEditText;",
        "maxAmount$delegate",
        "maxWatcher",
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;",
        "minAmount",
        "getMinAmount",
        "minAmount$delegate",
        "minWatcher",
        "plasticLink",
        "Lcom/squareup/widgets/MessageView;",
        "getPlasticLink",
        "()Lcom/squareup/widgets/MessageView;",
        "plasticLink$delegate",
        "policy",
        "getPolicy",
        "policy$delegate",
        "policyWatcher",
        "com/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1",
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;",
        "progressBar",
        "getProgressBar",
        "progressBar$delegate",
        "view",
        "attach",
        "",
        "onScreenData",
        "state",
        "Lcom/squareup/ui/settings/giftcards/ScreenData;",
        "MinMaxTextWatcher",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar$delegate:Lkotlin/Lazy;

.field private final animTimeMs$delegate:Lkotlin/Lazy;

.field private final content$delegate:Lkotlin/Lazy;

.field private final designsRow$delegate:Lkotlin/Lazy;

.field private final eGiftCardContent$delegate:Lkotlin/Lazy;

.field private final eGiftCardInPosToggle$delegate:Lkotlin/Lazy;

.field private final errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final maxAmount$delegate:Lkotlin/Lazy;

.field private maxWatcher:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;

.field private final minAmount$delegate:Lkotlin/Lazy;

.field private minWatcher:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final plasticLink$delegate:Lkotlin/Lazy;

.field private final policy$delegate:Lkotlin/Lazy;

.field private final policyWatcher:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final progressBar$delegate:Lkotlin/Lazy;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorsBarPresenter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iput-object p4, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p5, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 48
    new-instance p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$animTimeMs$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$animTimeMs$2;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->animTimeMs$delegate:Lkotlin/Lazy;

    .line 54
    new-instance p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$actionBar$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$actionBar$2;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->actionBar$delegate:Lkotlin/Lazy;

    .line 55
    new-instance p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$content$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$content$2;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->content$delegate:Lkotlin/Lazy;

    .line 56
    new-instance p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$progressBar$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$progressBar$2;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->progressBar$delegate:Lkotlin/Lazy;

    .line 57
    new-instance p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$eGiftCardInPosToggle$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$eGiftCardInPosToggle$2;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->eGiftCardInPosToggle$delegate:Lkotlin/Lazy;

    .line 59
    new-instance p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$plasticLink$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$plasticLink$2;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->plasticLink$delegate:Lkotlin/Lazy;

    .line 62
    new-instance p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$eGiftCardContent$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$eGiftCardContent$2;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->eGiftCardContent$delegate:Lkotlin/Lazy;

    .line 64
    new-instance p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$designsRow$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$designsRow$2;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->designsRow$delegate:Lkotlin/Lazy;

    .line 65
    new-instance p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$minAmount$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$minAmount$2;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->minAmount$delegate:Lkotlin/Lazy;

    .line 66
    new-instance p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$maxAmount$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$maxAmount$2;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->maxAmount$delegate:Lkotlin/Lazy;

    .line 67
    new-instance p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policy$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policy$2;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->policy$delegate:Lkotlin/Lazy;

    .line 72
    new-instance p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;

    invoke-direct {p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->policyWatcher:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getMoneyFormatter$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$getPriceLocaleHelper$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Lcom/squareup/money/PriceLocaleHelper;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    return-object p0
.end method

.method public static final synthetic access$getView$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Landroid/view/View;
    .locals 1

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->view:Landroid/view/View;

    if-nez p0, :cond_0

    const-string v0, "view"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$onScreenData(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;Lcom/squareup/ui/settings/giftcards/ScreenData;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->onScreenData(Lcom/squareup/ui/settings/giftcards/ScreenData;)V

    return-void
.end method

.method public static final synthetic access$setView$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;Landroid/view/View;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->view:Landroid/view/View;

    return-void
.end method

.method private final getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->actionBar$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinActionBar;

    return-object v0
.end method

.method private final getAnimTimeMs()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->animTimeMs$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method private final getContent()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->content$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getDesignsRow()Lcom/squareup/noho/NohoRow;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->designsRow$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    return-object v0
.end method

.method private final getEGiftCardContent()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->eGiftCardContent$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private final getEGiftCardInPosToggle()Lcom/squareup/noho/NohoCheckableRow;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->eGiftCardInPosToggle$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    return-object v0
.end method

.method private final getMaxAmount()Lcom/squareup/noho/NohoEditText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->maxAmount$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    return-object v0
.end method

.method private final getMinAmount()Lcom/squareup/noho/NohoEditText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->minAmount$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    return-object v0
.end method

.method private final getPlasticLink()Lcom/squareup/widgets/MessageView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->plasticLink$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    return-object v0
.end method

.method private final getPolicy()Lcom/squareup/noho/NohoEditText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->policy$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    return-object v0
.end method

.method private final getProgressBar()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->progressBar$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final onScreenData(Lcom/squareup/ui/settings/giftcards/ScreenData;)V
    .locals 8

    .line 129
    instance-of v0, p1, Lcom/squareup/ui/settings/giftcards/ScreenData$Loading;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    .line 130
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getProgressBar()Landroid/view/View;

    move-result-object p1

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 131
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getContent()Landroid/view/View;

    move-result-object p1

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 132
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ErrorsBarPresenter;->clearErrors()V

    goto/16 :goto_6

    .line 135
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/settings/giftcards/ScreenData$ErrorLoading;

    const-string v3, "ERROR_LOAD_KEY"

    if-eqz v0, :cond_1

    .line 136
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getProgressBar()Landroid/view/View;

    move-result-object p1

    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getAnimTimeMs()I

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    .line 137
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->giftcards_settings_load_error:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 137
    invoke-virtual {p1, v3, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 142
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    if-eqz v0, :cond_d

    .line 143
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getProgressBar()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getAnimTimeMs()I

    move-result v4

    invoke-static {v0, v4}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    .line 144
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getContent()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getAnimTimeMs()I

    move-result v4

    invoke-static {v0, v4}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 145
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getFailedToSave()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 146
    iget-object v4, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    .line 147
    iget-object v5, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/settingsapplet/R$string;->giftcards_settings_save_error:I

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 146
    invoke-virtual {v4, v3, v5}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 150
    :cond_2
    iget-object v3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {v3}, Lcom/squareup/ui/ErrorsBarPresenter;->clearErrors()V

    .line 153
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->isEGiftCardVisible()Z

    move-result v3

    .line 156
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getEGiftCardInPosToggle()Lcom/squareup/noho/NohoCheckableRow;

    move-result-object v4

    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->isEGiftCardVisible()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 157
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getEGiftCardInPosToggle()Lcom/squareup/noho/NohoCheckableRow;

    move-result-object v4

    new-instance v5, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$onScreenData$1;

    invoke-direct {v5, p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$onScreenData$1;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;Lcom/squareup/ui/settings/giftcards/ScreenData;)V

    check-cast v5, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v4, v5}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 162
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getEGiftCardContent()Landroid/widget/LinearLayout;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-static {v4, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz v3, :cond_d

    .line 164
    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getOrder_configuration()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 167
    :cond_3
    iget-object v3, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->enabled_theme_tokens:Ljava/util/List;

    if-eqz v3, :cond_4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    .line 168
    :goto_1
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getDesignsRow()Lcom/squareup/noho/NohoRow;

    move-result-object v4

    if-ne v3, v2, :cond_5

    .line 169
    iget-object v3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/settingsapplet/R$string;->giftcards_settings_egiftcard_designs_amount_singular:I

    invoke-interface {v3, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    goto :goto_2

    .line 171
    :cond_5
    iget-object v5, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->view:Landroid/view/View;

    if-nez v5, :cond_6

    const-string v6, "view"

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    sget v6, Lcom/squareup/settingsapplet/R$string;->giftcards_settings_egiftcard_designs_amount_plural:I

    invoke-static {v5, v6}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    const-string v6, "amount"

    .line 172
    invoke-virtual {v5, v6, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 173
    invoke-virtual {v3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v3

    .line 174
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 168
    :goto_2
    invoke-virtual {v4, v3}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 176
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getDesignsRow()Lcom/squareup/noho/NohoRow;

    move-result-object v3

    sget-object v4, Lcom/squareup/noho/AccessoryType;->DISCLOSURE:Lcom/squareup/noho/AccessoryType;

    invoke-virtual {v3, v4}, Lcom/squareup/noho/NohoRow;->setAccessory(Lcom/squareup/noho/AccessoryType;)V

    .line 179
    iget-object v3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->minWatcher:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;

    if-nez v3, :cond_7

    const-string v4, "minWatcher"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 180
    :cond_7
    iget-object v4, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->min_load_amount:Lcom/squareup/protos/common/Money;

    iget-object v5, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->max_load_amount:Lcom/squareup/protos/common/Money;

    invoke-static {v4, v5}, Lcom/squareup/money/MoneyMath;->min(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    const-string v5, "MoneyMath.min(config.min\u2026, config.max_load_amount)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    iget-object v5, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-virtual {v5}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->getMinMinLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 182
    iget-object v6, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-virtual {v6}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->getMaxMaxLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object v6

    iget-object v7, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->max_load_amount:Lcom/squareup/protos/common/Money;

    invoke-static {v6, v7}, Lcom/squareup/money/MoneyMath;->min(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v6

    const-string v7, "MoneyMath.min(runner.max\u2026, config.max_load_amount)"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    new-instance v7, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$onScreenData$2;

    invoke-direct {v7, p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$onScreenData$2;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;Lcom/squareup/ui/settings/giftcards/ScreenData;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 179
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->update(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lkotlin/jvm/functions/Function1;)V

    .line 185
    iget-object v3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->maxWatcher:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;

    if-nez v3, :cond_8

    const-string v4, "maxWatcher"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 186
    :cond_8
    iget-object v4, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->min_load_amount:Lcom/squareup/protos/common/Money;

    iget-object v5, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->max_load_amount:Lcom/squareup/protos/common/Money;

    invoke-static {v4, v5}, Lcom/squareup/money/MoneyMath;->max(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    const-string v5, "MoneyMath.max(config.min\u2026, config.max_load_amount)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    iget-object v5, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-virtual {v5}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->getMinMinLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object v5

    iget-object v6, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->min_load_amount:Lcom/squareup/protos/common/Money;

    invoke-static {v5, v6}, Lcom/squareup/money/MoneyMath;->max(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v5

    const-string v6, "MoneyMath.max(runner.min\u2026, config.min_load_amount)"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 188
    iget-object v6, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-virtual {v6}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->getMaxMaxLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 189
    new-instance v7, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$onScreenData$3;

    invoke-direct {v7, p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$onScreenData$3;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;Lcom/squareup/ui/settings/giftcards/ScreenData;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 185
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->update(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lkotlin/jvm/functions/Function1;)V

    .line 193
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getPolicy()Lcom/squareup/noho/NohoEditText;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    if-eqz v3, :cond_a

    invoke-static {v3}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    goto :goto_3

    :cond_9
    const/4 v3, 0x0

    goto :goto_4

    :cond_a
    :goto_3
    const/4 v3, 0x1

    :goto_4
    if-eqz v3, :cond_c

    .line 195
    iget-object v3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->policyWatcher:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;

    invoke-virtual {v3, v2}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;->setPaused(Z)V

    .line 196
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getPolicy()Lcom/squareup/noho/NohoEditText;

    move-result-object v2

    iget-object v0, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->custom_policy:Ljava/lang/String;

    if-eqz v0, :cond_b

    goto :goto_5

    :cond_b
    const-string v0, ""

    :goto_5
    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->policyWatcher:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;->setPaused(Z)V

    .line 199
    :cond_c
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->policyWatcher:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;

    new-instance v1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$onScreenData$4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$onScreenData$4;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;Lcom/squareup/ui/settings/giftcards/ScreenData;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;->setOnChange(Lkotlin/jvm/functions/Function1;)V

    :cond_d
    :goto_6
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->view:Landroid/view/View;

    .line 90
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getPlasticLink()Lcom/squareup/widgets/MessageView;

    move-result-object v0

    .line 85
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 86
    sget v2, Lcom/squareup/settingsapplet/R$string;->giftcards_settings_plastic_description:I

    const-string v3, "square_dashboard"

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 87
    sget v2, Lcom/squareup/registerlib/R$string;->giftcards_url:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 88
    sget v2, Lcom/squareup/settingsapplet/R$string;->giftcards_settings_plastic_description_link_text:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 89
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asPhrase()Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 90
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    new-instance v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;

    .line 93
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getMinAmount()Lcom/squareup/noho/NohoEditText;

    move-result-object v1

    .line 94
    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->getMinMinLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 95
    iget-object v3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->getMaxMaxLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 92
    invoke-direct {v0, p0, v1, v2, v3}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;Lcom/squareup/noho/NohoEditText;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->minWatcher:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;

    .line 97
    new-instance v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;

    .line 98
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getMaxAmount()Lcom/squareup/noho/NohoEditText;

    move-result-object v1

    .line 99
    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->getMinMinLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 100
    iget-object v3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->getMaxMaxLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 97
    invoke-direct {v0, p0, v1, v2, v3}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;Lcom/squareup/noho/NohoEditText;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->maxWatcher:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;

    .line 102
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getPolicy()Lcom/squareup/noho/NohoEditText;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->policyWatcher:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->setMaxMessages(I)V

    .line 106
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->getDesignsRow()Lcom/squareup/noho/NohoRow;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 261
    new-instance v1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->showBackButton()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "runner.showBackButton()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    new-instance v1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$2;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->screenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 123
    new-instance v1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$attach$3;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
