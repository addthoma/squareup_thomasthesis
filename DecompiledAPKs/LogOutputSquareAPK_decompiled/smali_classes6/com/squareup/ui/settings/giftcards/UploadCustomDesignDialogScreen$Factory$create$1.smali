.class final Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory$create$1;
.super Ljava/lang/Object;
.source "UploadCustomDesignDialogScreen.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/Dialog;",
        "it",
        "Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;

.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory$create$1;->this$0:Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory;

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;)Landroid/app/Dialog;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory$create$1;->this$0:Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory;

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory$create$1;->$context:Landroid/content/Context;

    invoke-static {v0, v1, p1}, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory;->access$createDialog(Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory;Landroid/content/Context;Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory$create$1;->apply(Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method
