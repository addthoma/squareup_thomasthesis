.class final Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CropCustomDesignSettingsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "invoke",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$1;->this$0:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$1;->invoke(Ljava/lang/Boolean;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Boolean;)V
    .locals 5

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$1;->this$0:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->access$getActionBar$p(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    .line 60
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 61
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/settingsapplet/R$string;->giftcards_settings_egiftcard_designs_crop_photo_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 62
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$1$1;

    invoke-direct {v3, p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$1$1;-><init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$1;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 64
    sget-object v2, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 65
    new-instance v3, Lcom/squareup/util/ViewString$ResourceString;

    .line 66
    sget v4, Lcom/squareup/settingsapplet/R$string;->giftcards_settings_egiftcard_designs_crop_photo_action:I

    .line 65
    invoke-direct {v3, v4}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v3, Lcom/squareup/resources/TextModel;

    const-string v4, "it"

    .line 68
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    .line 69
    new-instance v4, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$1$2;

    invoke-direct {v4, p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$1$2;-><init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$1;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 63
    invoke-virtual {v1, v2, v3, p1, v4}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method
