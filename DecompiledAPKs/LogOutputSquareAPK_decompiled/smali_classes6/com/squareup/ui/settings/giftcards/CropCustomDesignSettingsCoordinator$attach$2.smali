.class public final Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$2;
.super Ljava/lang/Object;
.source "CropCustomDesignSettingsCoordinator.kt"

# interfaces
.implements Lcom/squareup/picasso/Target;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\'\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016J\u0018\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0012\u0010\u000b\u001a\u00020\u00032\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u0005H\u0016\u00a8\u0006\r"
    }
    d2 = {
        "com/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$2",
        "Lcom/squareup/picasso/Target;",
        "onBitmapFailed",
        "",
        "errorDrawable",
        "Landroid/graphics/drawable/Drawable;",
        "onBitmapLoaded",
        "bitmap",
        "Landroid/graphics/Bitmap;",
        "from",
        "Lcom/squareup/picasso/Picasso$LoadedFrom;",
        "onPrepareLoad",
        "placeHolderDrawable",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$2;->this$0:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$2;->this$0:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->access$getErrorView$p(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$2;->this$0:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->access$getProgressBar$p(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 1

    const-string v0, "bitmap"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    iget-object p2, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$2;->this$0:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;

    invoke-static {p2, p1}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->access$displayBitmap(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method
