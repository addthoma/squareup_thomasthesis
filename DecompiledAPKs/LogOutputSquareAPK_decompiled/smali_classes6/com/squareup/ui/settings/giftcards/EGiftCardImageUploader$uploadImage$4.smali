.class final Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$4;
.super Ljava/lang/Object;
.source "EGiftCardImageUploader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->uploadImage(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Landroid/graphics/Bitmap;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0014\u0010\u0004\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00060\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;",
        "kotlin.jvm.PlatformType",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/payment/GiftCardImageUploadResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $configSoFar:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$4;->this$0:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$4;->$configSoFar:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+",
            "Lcom/squareup/server/payment/GiftCardImageUploadResponse;",
            ">;)",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$4;->this$0:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    const-string v1, "it.response"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/server/payment/GiftCardImageUploadResponse;

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$4;->$configSoFar:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->access$saveNewThemeRequest(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Lcom/squareup/server/payment/GiftCardImageUploadResponse;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 52
    :cond_0
    sget-object p1, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult$Failed;->INSTANCE:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult$Failed;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(Failed)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$4;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
