.class public interface abstract Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;
.super Ljava/lang/Object;
.source "CropCustomDesignSettingsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0008H&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH&\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;",
        "",
        "exitCropCustomDesignsScreen",
        "",
        "imageUri",
        "Landroid/net/Uri;",
        "showUploadSpinnerEvents",
        "view",
        "Landroid/view/View;",
        "uploadImage",
        "bitmap",
        "Landroid/graphics/Bitmap;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract exitCropCustomDesignsScreen()V
.end method

.method public abstract imageUri()Landroid/net/Uri;
.end method

.method public abstract showUploadSpinnerEvents(Landroid/view/View;)V
.end method

.method public abstract uploadImage(Landroid/graphics/Bitmap;)V
.end method
