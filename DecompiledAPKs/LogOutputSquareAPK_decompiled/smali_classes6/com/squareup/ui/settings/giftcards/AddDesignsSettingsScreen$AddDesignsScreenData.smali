.class public final Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;
.super Ljava/lang/Object;
.source "AddDesignsSettingsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AddDesignsScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0011\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B1\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\nJ\u000f\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0008H\u00c6\u0003J\u000f\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J=\u0010\u0016\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u000e\u0008\u0002\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00062\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0017\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;",
        "",
        "disabledThemes",
        "",
        "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
        "uploadEnabled",
        "",
        "settings",
        "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
        "themesToBeAdded",
        "(Ljava/util/List;ZLcom/squareup/ui/settings/giftcards/ScreenData$Settings;Ljava/util/List;)V",
        "getDisabledThemes",
        "()Ljava/util/List;",
        "getSettings",
        "()Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
        "getThemesToBeAdded",
        "getUploadEnabled",
        "()Z",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final disabledThemes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

.field private final themesToBeAdded:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation
.end field

.field private final uploadEnabled:Z


# direct methods
.method public constructor <init>(Ljava/util/List;ZLcom/squareup/ui/settings/giftcards/ScreenData$Settings;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;Z",
            "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;)V"
        }
    .end annotation

    const-string v0, "disabledThemes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "themesToBeAdded"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->disabledThemes:Ljava/util/List;

    iput-boolean p2, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->uploadEnabled:Z

    iput-object p3, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->settings:Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    iput-object p4, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->themesToBeAdded:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;Ljava/util/List;ZLcom/squareup/ui/settings/giftcards/ScreenData$Settings;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->disabledThemes:Ljava/util/List;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-boolean p2, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->uploadEnabled:Z

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->settings:Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->themesToBeAdded:Ljava/util/List;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->copy(Ljava/util/List;ZLcom/squareup/ui/settings/giftcards/ScreenData$Settings;Ljava/util/List;)Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->disabledThemes:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->uploadEnabled:Z

    return v0
.end method

.method public final component3()Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->settings:Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->themesToBeAdded:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/util/List;ZLcom/squareup/ui/settings/giftcards/ScreenData$Settings;Ljava/util/List;)Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;Z",
            "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;)",
            "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;"
        }
    .end annotation

    const-string v0, "disabledThemes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "themesToBeAdded"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;-><init>(Ljava/util/List;ZLcom/squareup/ui/settings/giftcards/ScreenData$Settings;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->disabledThemes:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->disabledThemes:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->uploadEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->uploadEnabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->settings:Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    iget-object v1, p1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->settings:Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->themesToBeAdded:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->themesToBeAdded:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDisabledThemes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->disabledThemes:Ljava/util/List;

    return-object v0
.end method

.method public final getSettings()Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->settings:Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    return-object v0
.end method

.method public final getThemesToBeAdded()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->themesToBeAdded:Ljava/util/List;

    return-object v0
.end method

.method public final getUploadEnabled()Z
    .locals 1

    .line 60
    iget-boolean v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->uploadEnabled:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->disabledThemes:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->uploadEnabled:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->settings:Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->themesToBeAdded:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AddDesignsScreenData(disabledThemes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->disabledThemes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", uploadEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->uploadEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->settings:Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", themesToBeAdded="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->themesToBeAdded:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
