.class public final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;
.super Ljava/lang/Object;
.source "GiftCardsSettingsScopeRunner.kt"

# interfaces
.implements Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen$Runner;
.implements Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;
.implements Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;
.implements Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;
.implements Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Runner;
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00eb\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000*\u0001\u0019\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006BI\u0008\u0001\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0008\u0008\u0001\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0002\u0010\u0017J,\u00109\u001a&\u0012\u000c\u0012\n **\u0004\u0018\u00010:0: **\u0012\u0012\u000c\u0012\n **\u0004\u0018\u00010:0:\u0018\u00010,0,H\u0016J\u0010\u0010;\u001a\u00020<2\u0006\u0010=\u001a\u000201H\u0016J$\u0010>\u001a\u00020<2\u0006\u0010?\u001a\u0002042\u0012\u0010@\u001a\u000e\u0012\u0004\u0012\u00020B\u0012\u0004\u0012\u00020B0AH\u0002J\u0018\u0010C\u001a\u00020<2\u0006\u0010?\u001a\u0002042\u0006\u0010=\u001a\u000201H\u0016J\u0008\u0010D\u001a\u00020<H\u0016J\u0008\u0010E\u001a\u00020<H\u0016J\u0008\u0010F\u001a\u00020<H\u0016J\u0008\u0010G\u001a\u00020<H\u0016J,\u0010H\u001a&\u0012\u000c\u0012\n **\u0004\u0018\u00010J0J **\u0012\u0012\u000c\u0012\n **\u0004\u0018\u00010J0J\u0018\u00010I0IH\u0016J\u0008\u0010K\u001a\u00020\u001cH\u0016J\u0010\u0010L\u001a\u00020<2\u0006\u0010M\u001a\u00020NH\u0016J\u0008\u0010O\u001a\u00020<H\u0016J\u0010\u0010P\u001a\u00020<2\u0006\u0010=\u001a\u000201H\u0016J\u000e\u0010Q\u001a\u0008\u0012\u0004\u0012\u00020)0,H\u0002J\u0010\u0010R\u001a\u00020<2\u0006\u0010?\u001a\u000204H\u0016J,\u0010S\u001a&\u0012\u000c\u0012\n **\u0004\u0018\u00010404 **\u0012\u0012\u000c\u0012\n **\u0004\u0018\u00010404\u0018\u00010,0,H\u0002J\u000e\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020)0,H\u0016J\u0008\u0010T\u001a\u00020<H\u0016J,\u0010+\u001a&\u0012\u000c\u0012\n **\u0004\u0018\u00010-0- **\u0012\u0012\u000c\u0012\n **\u0004\u0018\u00010-0-\u0018\u00010,0,H\u0016J\u0008\u0010U\u001a\u00020<H\u0016J\u0010\u0010V\u001a\u00020<2\u0006\u0010W\u001a\u00020XH\u0016J\u0008\u0010Y\u001a\u00020<H\u0016J\u0018\u0010Z\u001a\u00020<2\u0006\u0010?\u001a\u0002042\u0006\u0010[\u001a\u00020-H\u0016J\u0018\u0010\\\u001a\u00020<2\u0006\u0010?\u001a\u0002042\u0006\u0010]\u001a\u00020\"H\u0016J\u0018\u0010^\u001a\u00020<2\u0006\u0010?\u001a\u0002042\u0006\u0010_\u001a\u00020\"H\u0016J\u0018\u0010`\u001a\u00020<2\u0006\u0010?\u001a\u0002042\u0006\u0010a\u001a\u00020bH\u0016J,\u0010c\u001a&\u0012\u000c\u0012\n **\u0004\u0018\u00010d0d **\u0012\u0012\u000c\u0012\n **\u0004\u0018\u00010d0d\u0018\u00010,0,H\u0016J\u0010\u0010e\u001a\u00020<2\u0006\u0010f\u001a\u000206H\u0016J\u000c\u0010g\u001a\u000204*\u00020hH\u0002R\u0010\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u001aR\u001c\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001d\u0010\u001e\"\u0004\u0008\u001f\u0010 R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010!\u001a\u00020\"\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010%\u001a\u00020\"\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010$R2\u0010\'\u001a&\u0012\u000c\u0012\n **\u0004\u0018\u00010)0) **\u0012\u0012\u000c\u0012\n **\u0004\u0018\u00010)0)\u0018\u00010(0(X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010+\u001a&\u0012\u000c\u0012\n **\u0004\u0018\u00010-0- **\u0012\u0012\u000c\u0012\n **\u0004\u0018\u00010-0-\u0018\u00010,0,X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010.\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u000201 **\n\u0012\u0004\u0012\u000201\u0018\u000100000/X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u00102\u001a\u0010\u0012\u000c\u0012\n **\u0004\u0018\u0001040403X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u00105\u001a\u0010\u0012\u000c\u0012\n **\u0004\u0018\u0001060603X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u00107\u001a&\u0012\u000c\u0012\n **\u0004\u0018\u00010808 **\u0012\u0012\u000c\u0012\n **\u0004\u0018\u00010808\u0018\u00010(0(X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006i"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;",
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen$Runner;",
        "Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;",
        "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;",
        "Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;",
        "Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Runner;",
        "Lmortar/Scoped;",
        "flow",
        "Lflow/Flow;",
        "giftCardServiceHelper",
        "Lcom/squareup/giftcard/GiftCardServiceHelper;",
        "giftCardsSettingsHelper",
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;",
        "eGiftCardImageUploadHelper",
        "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "device",
        "Lcom/squareup/util/Device;",
        "maybeCameraHelper",
        "Lcom/squareup/camerahelper/CameraHelper;",
        "uploadSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "(Lflow/Flow;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Lio/reactivex/Scheduler;Lcom/squareup/util/Device;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/register/widgets/GlassSpinner;)V",
        "cameraListener",
        "com/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$cameraListener$1",
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$cameraListener$1;",
        "customImageUri",
        "Landroid/net/Uri;",
        "getCustomImageUri",
        "()Landroid/net/Uri;",
        "setCustomImageUri",
        "(Landroid/net/Uri;)V",
        "maxMaxLoadAmount",
        "Lcom/squareup/protos/common/Money;",
        "getMaxMaxLoadAmount",
        "()Lcom/squareup/protos/common/Money;",
        "minMinLoadAmount",
        "getMinMinLoadAmount",
        "screenData",
        "Lio/reactivex/observables/ConnectableObservable;",
        "Lcom/squareup/ui/settings/giftcards/ScreenData;",
        "kotlin.jvm.PlatformType",
        "showBackButton",
        "Lio/reactivex/Observable;",
        "",
        "themesToBeAdded",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
        "updatedSetting",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
        "uploadBitmap",
        "Landroid/graphics/Bitmap;",
        "uploadingTheme",
        "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;",
        "addDesignsScreenData",
        "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;",
        "addTheme",
        "",
        "theme",
        "copyConfig",
        "oldState",
        "modify",
        "Lkotlin/Function1;",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;",
        "disableTheme",
        "exitAddDesignsScreen",
        "exitCropCustomDesignsScreen",
        "exitSettings",
        "exitViewDesignsSettings",
        "gridColumnCount",
        "Lio/reactivex/Single;",
        "",
        "imageUri",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "removeTheme",
        "requestConfig",
        "saveAddThemes",
        "saveChangesErrors",
        "showAddDesigns",
        "showUploadCustom",
        "showUploadSpinnerEvents",
        "view",
        "Landroid/view/View;",
        "showViewDesigns",
        "updateEnabledInPosSettings",
        "enabled",
        "updateMaxAmount",
        "newMax",
        "updateMinAmount",
        "newMin",
        "updatePolicy",
        "newPolicy",
        "",
        "uploadCustomDesignDialogScreenData",
        "Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;",
        "uploadImage",
        "bitmap",
        "toSettings",
        "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cameraListener:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$cameraListener$1;

.field private customImageUri:Landroid/net/Uri;

.field private final device:Lcom/squareup/util/Device;

.field private final eGiftCardImageUploadHelper:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;

.field private final flow:Lflow/Flow;

.field private final giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

.field private final giftCardsSettingsHelper:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final maxMaxLoadAmount:Lcom/squareup/protos/common/Money;

.field private final maybeCameraHelper:Lcom/squareup/camerahelper/CameraHelper;

.field private final minMinLoadAmount:Lcom/squareup/protos/common/Money;

.field private final screenData:Lio/reactivex/observables/ConnectableObservable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/observables/ConnectableObservable<",
            "Lcom/squareup/ui/settings/giftcards/ScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final showBackButton:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final themesToBeAdded:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;>;"
        }
    .end annotation
.end field

.field private final updatedSetting:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
            ">;"
        }
    .end annotation
.end field

.field private final uploadBitmap:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final uploadSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final uploadingTheme:Lio/reactivex/observables/ConnectableObservable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/observables/ConnectableObservable<",
            "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Lio/reactivex/Scheduler;Lcom/squareup/util/Device;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 1
    .param p5    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "giftCardServiceHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "giftCardsSettingsHelper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eGiftCardImageUploadHelper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maybeCameraHelper"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uploadSpinner"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

    iput-object p3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->giftCardsSettingsHelper:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;

    iput-object p4, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->eGiftCardImageUploadHelper:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;

    iput-object p5, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p6, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->device:Lcom/squareup/util/Device;

    iput-object p7, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->maybeCameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    iput-object p8, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->uploadSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 63
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Settings>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->updatedSetting:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 64
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026(emptyList<EGiftTheme>())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->themesToBeAdded:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 65
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Bitmap>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->uploadBitmap:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 67
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->getScreenSize()Lio/reactivex/Observable;

    move-result-object p1

    .line 68
    sget-object p2, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$showBackButton$1;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$showBackButton$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 69
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->showBackButton:Lio/reactivex/Observable;

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->giftCardsSettingsHelper:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;->getMinMinLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->minMinLoadAmount:Lcom/squareup/protos/common/Money;

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->giftCardsSettingsHelper:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;->getMaxMaxLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->maxMaxLoadAmount:Lcom/squareup/protos/common/Money;

    .line 76
    new-instance p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$cameraListener$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$cameraListener$1;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->cameraListener:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$cameraListener$1;

    .line 90
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->requestConfig()Lio/reactivex/Observable;

    move-result-object p1

    check-cast p1, Lio/reactivex/ObservableSource;

    .line 92
    iget-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->updatedSetting:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast p2, Lio/reactivex/ObservableSource;

    .line 94
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->saveChangesErrors()Lio/reactivex/Observable;

    move-result-object p3

    check-cast p3, Lio/reactivex/ObservableSource;

    .line 88
    invoke-static {p1, p2, p3}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object p1

    .line 96
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    const/4 p2, 0x1

    .line 97
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->screenData:Lio/reactivex/observables/ConnectableObservable;

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->uploadBitmap:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 125
    iget-object p3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->screenData:Lio/reactivex/observables/ConnectableObservable;

    const-class p4, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    invoke-virtual {p3, p4}, Lio/reactivex/observables/ConnectableObservable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p3

    .line 126
    sget-object p4, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadingTheme$1;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadingTheme$1;

    check-cast p4, Lio/reactivex/functions/Predicate;

    invoke-virtual {p3, p4}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p3

    .line 127
    sget-object p4, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadingTheme$2;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadingTheme$2;

    check-cast p4, Lio/reactivex/functions/Function;

    invoke-virtual {p3, p4}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p3

    check-cast p3, Lio/reactivex/ObservableSource;

    .line 128
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object p4

    .line 124
    invoke-virtual {p1, p3, p4}, Lcom/jakewharton/rxrelay2/PublishRelay;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    .line 129
    new-instance p3, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadingTheme$3;

    invoke-direct {p3, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadingTheme$3;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)V

    check-cast p3, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 135
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->uploadingTheme:Lio/reactivex/observables/ConnectableObservable;

    return-void
.end method

.method public static final synthetic access$getEGiftCardImageUploadHelper$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->eGiftCardImageUploadHelper:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;

    return-object p0
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lflow/Flow;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$getGiftCardServiceHelper$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/squareup/giftcard/GiftCardServiceHelper;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

    return-object p0
.end method

.method public static final synthetic access$getMaybeCameraHelper$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/squareup/camerahelper/CameraHelper;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->maybeCameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    return-object p0
.end method

.method public static final synthetic access$getThemesToBeAdded$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->themesToBeAdded:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getUploadSpinner$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/squareup/register/widgets/GlassSpinner;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->uploadSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-object p0
.end method

.method public static final synthetic access$getUploadingTheme$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lio/reactivex/observables/ConnectableObservable;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->uploadingTheme:Lio/reactivex/observables/ConnectableObservable;

    return-object p0
.end method

.method public static final synthetic access$toSettings(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;)Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->toSettings(Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;)Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    move-result-object p0

    return-object p0
.end method

.method private final copyConfig(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lkotlin/jvm/functions/Function1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;",
            ">;)V"
        }
    .end annotation

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->updatedSetting:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 238
    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getOrder_configuration()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->newBuilder()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    move-result-object v1

    const-string v2, "oldState.order_configuration!!.newBuilder()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    .line 239
    invoke-virtual {p2}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->build()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1b

    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v1, p1

    .line 237
    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->copy$default(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;ZZILjava/lang/Object;)Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    move-result-object p1

    .line 236
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final requestConfig()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/giftcards/ScreenData;",
            ">;"
        }
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

    invoke-virtual {v0}, Lcom/squareup/giftcard/GiftCardServiceHelper;->getEGiftCardOrderConfiguration()Lio/reactivex/Single;

    move-result-object v0

    .line 101
    new-instance v1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$requestConfig$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$requestConfig$1;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    .line 108
    sget-object v1, Lcom/squareup/ui/settings/giftcards/ScreenData$Loading;->INSTANCE:Lcom/squareup/ui/settings/giftcards/ScreenData$Loading;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 109
    new-instance v1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$requestConfig$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$requestConfig$2;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->repeatWhen(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "giftCardServiceHelper.eG\u2026lt.Success::class.java) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final saveChangesErrors()Lio/reactivex/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
            ">;"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->updatedSetting:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 114
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v3, 0xc8

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->debounce(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 115
    new-instance v1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private final toSettings(Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;)Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;
    .locals 7

    .line 276
    new-instance v6, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    .line 277
    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    const-string v0, "all_egift_themes"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 278
    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->denomination_amounts:Ljava/util/List;

    const-string v0, "denomination_amounts"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 279
    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    .line 280
    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    invoke-static {p1}, Lcom/squareup/egiftcard/activation/EGiftCardConfigKt;->isEGiftCardVisible(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Z

    move-result v4

    const/4 v5, 0x0

    move-object v0, v6

    .line 276
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;ZZ)V

    return-object v6
.end method


# virtual methods
.method public addDesignsScreenData()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;",
            ">;"
        }
    .end annotation

    .line 170
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 171
    invoke-virtual {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->screenData()Lio/reactivex/Observable;

    move-result-object v1

    const-class v2, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "screenData().ofType(Settings::class.java)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->themesToBeAdded:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "themesToBeAdded.distinctUntilChanged()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 174
    new-instance v1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$addDesignsScreenData$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$addDesignsScreenData$1;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public addTheme(Lcom/squareup/protos/client/giftcards/EGiftTheme;)V
    .locals 3

    const-string v0, "theme"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->themesToBeAdded:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v2, "themesToBeAdded.value!!"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public disableTheme(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lcom/squareup/protos/client/giftcards/EGiftTheme;)V
    .locals 3

    const-string v0, "oldState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "theme"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getOrder_configuration()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->enabled_theme_tokens:Ljava/util/List;

    .line 208
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 209
    new-instance v1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$disableTheme$1;

    invoke-direct {v1, v0, p2}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$disableTheme$1;-><init>(Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftTheme;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->copyConfig(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lkotlin/jvm/functions/Function1;)V

    :cond_1
    return-void
.end method

.method public exitAddDesignsScreen()V
    .locals 4

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->themesToBeAdded:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public exitCropCustomDesignsScreen()V
    .locals 4

    const/4 v0, 0x0

    .line 267
    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->customImageUri:Landroid/net/Uri;

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public exitSettings()V
    .locals 4

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public exitViewDesignsSettings()V
    .locals 4

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public final getCustomImageUri()Landroid/net/Uri;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->customImageUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getMaxMaxLoadAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->maxMaxLoadAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getMinMinLoadAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->minMinLoadAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public gridColumnCount()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->getScreenSize()Lio/reactivex/Observable;

    move-result-object v0

    .line 187
    sget-object v1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$gridColumnCount$1;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$gridColumnCount$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 188
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public imageUri()Landroid/net/Uri;
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->customImageUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->screenData:Lio/reactivex/observables/ConnectableObservable;

    invoke-virtual {v0}, Lio/reactivex/observables/ConnectableObservable;->connect()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screenData.connect()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->uploadingTheme:Lio/reactivex/observables/ConnectableObservable;

    invoke-virtual {v0}, Lio/reactivex/observables/ConnectableObservable;->connect()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "uploadingTheme.connect()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->uploadingTheme:Lio/reactivex/observables/ConnectableObservable;

    const-string v1, "uploadingTheme"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/Observable;

    .line 144
    new-instance v1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 161
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->maybeCameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->cameraListener:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$cameraListener$1;

    check-cast v0, Lcom/squareup/camerahelper/CameraHelper$Listener;

    invoke-interface {p1, v0}, Lcom/squareup/camerahelper/CameraHelper;->setListener(Lcom/squareup/camerahelper/CameraHelper$Listener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->maybeCameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->cameraListener:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$cameraListener$1;

    check-cast v1, Lcom/squareup/camerahelper/CameraHelper$Listener;

    invoke-interface {v0, v1}, Lcom/squareup/camerahelper/CameraHelper;->dropListener(Lcom/squareup/camerahelper/CameraHelper$Listener;)V

    return-void
.end method

.method public removeTheme(Lcom/squareup/protos/client/giftcards/EGiftTheme;)V
    .locals 3

    const-string v0, "theme"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->themesToBeAdded:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v2, "themesToBeAdded.value!!"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1, p1}, Lkotlin/collections/CollectionsKt;->minus(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public saveAddThemes(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;)V
    .locals 1

    const-string v0, "oldState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    new-instance v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveAddThemes$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveAddThemes$1;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->copyConfig(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lkotlin/jvm/functions/Function1;)V

    .line 228
    invoke-virtual {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->exitAddDesignsScreen()V

    return-void
.end method

.method public screenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/giftcards/ScreenData;",
            ">;"
        }
    .end annotation

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->screenData:Lio/reactivex/observables/ConnectableObservable;

    invoke-virtual {v0}, Lio/reactivex/observables/ConnectableObservable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screenData.distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final setCustomImageUri(Landroid/net/Uri;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->customImageUri:Landroid/net/Uri;

    return-void
.end method

.method public showAddDesigns()V
    .locals 2

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen;->Companion:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Companion;->getINSTANCE()Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showBackButton()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->showBackButton:Lio/reactivex/Observable;

    return-object v0
.end method

.method public showUploadCustom()V
    .locals 1

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->maybeCameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    invoke-interface {v0}, Lcom/squareup/camerahelper/CameraHelper;->startGallery()V

    return-void
.end method

.method public showUploadSpinnerEvents(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->uploadSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "uploadSpinner.showOrHideSpinner(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 263
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    return-void
.end method

.method public showViewDesigns()V
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen;->Companion:Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Companion;->getINSTANCE()Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public updateEnabledInPosSettings(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Z)V
    .locals 2

    const-string v0, "oldState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->updatedSetting:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->giftCardsSettingsHelper:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;

    invoke-virtual {v1, p1, p2}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;->enableSellEGiftCardInPos(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Z)Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public updateMaxAmount(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lcom/squareup/protos/common/Money;)V
    .locals 1

    const-string v0, "oldState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newMax"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    new-instance v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$updateMaxAmount$1;

    invoke-direct {v0, p2}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$updateMaxAmount$1;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->copyConfig(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public updateMinAmount(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lcom/squareup/protos/common/Money;)V
    .locals 1

    const-string v0, "oldState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newMin"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    new-instance v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$updateMinAmount$1;

    invoke-direct {v0, p2}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$updateMinAmount$1;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->copyConfig(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public updatePolicy(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Ljava/lang/String;)V
    .locals 1

    const-string v0, "oldState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newPolicy"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    new-instance v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$updatePolicy$1;

    invoke-direct {v0, p2}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$updatePolicy$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->copyConfig(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public uploadCustomDesignDialogScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;",
            ">;"
        }
    .end annotation

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->uploadingTheme:Lio/reactivex/observables/ConnectableObservable;

    .line 273
    sget-object v1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadCustomDesignDialogScreenData$1;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadCustomDesignDialogScreenData$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/observables/ConnectableObservable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public uploadImage(Landroid/graphics/Bitmap;)V
    .locals 1

    const-string v0, "bitmap"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->uploadBitmap:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
