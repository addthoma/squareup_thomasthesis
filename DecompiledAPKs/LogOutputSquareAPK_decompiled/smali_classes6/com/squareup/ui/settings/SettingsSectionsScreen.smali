.class public final Lcom/squareup/ui/settings/SettingsSectionsScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "SettingsSectionsScreen.java"

# interfaces
.implements Lcom/squareup/applet/CanShowBadge;
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/Master;
    applet = Lcom/squareup/ui/settings/SettingsApplet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/SettingsSectionsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/SettingsSectionsScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/SettingsSectionsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/SettingsSectionsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/ui/settings/SettingsSectionsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/SettingsSectionsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/SettingsSectionsScreen;->INSTANCE:Lcom/squareup/ui/settings/SettingsSectionsScreen;

    .line 25
    sget-object v0, Lcom/squareup/ui/settings/SettingsSectionsScreen;->INSTANCE:Lcom/squareup/ui/settings/SettingsSectionsScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/SettingsSectionsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 28
    sget v0, Lcom/squareup/settingsapplet/R$layout;->settings_sections_view:I

    return v0
.end method
