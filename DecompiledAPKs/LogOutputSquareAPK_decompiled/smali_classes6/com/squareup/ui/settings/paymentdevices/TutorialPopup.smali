.class public Lcom/squareup/ui/settings/paymentdevices/TutorialPopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "TutorialPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;,
        Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;,
        Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;",
        "Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final isCancelable:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    .line 36
    iput-boolean p2, p0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup;->isCancelable:Z

    return-void
.end method

.method public static createNonCancelable(Landroid/content/Context;)Lcom/squareup/ui/settings/paymentdevices/TutorialPopup;
    .locals 2

    .line 31
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup;-><init>(Landroid/content/Context;Z)V

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup;->createDialog(Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method protected createDialog(Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;",
            "Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;",
            ">;)",
            "Landroid/app/Dialog;"
        }
    .end annotation

    .line 41
    move-object v3, p3

    check-cast v3, Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;

    .line 42
    new-instance p2, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v4, p0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup;->isCancelable:Z

    const/4 v5, 0x0

    move-object v0, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;-><init>(Landroid/content/Context;Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;ZLcom/squareup/ui/settings/paymentdevices/TutorialPopup$1;)V

    return-object p2
.end method
