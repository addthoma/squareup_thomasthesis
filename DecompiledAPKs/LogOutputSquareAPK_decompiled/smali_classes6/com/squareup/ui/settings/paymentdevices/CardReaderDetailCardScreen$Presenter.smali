.class public Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "CardReaderDetailCardScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private final detailDelegate:Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;

.field private final flow:Lflow/Flow;

.field private mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

.field private noReadersRemain:Z

.field private final readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

.field private final res:Lcom/squareup/util/Res;

.field private final storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

.field private final watchdog:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/util/Res;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;Lcom/squareup/cardreader/BluetoothUtils;Lflow/Flow;Lcom/squareup/util/RxWatchdog;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/log/ReaderEventLogger;",
            "Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Lflow/Flow;",
            "Lcom/squareup/util/RxWatchdog<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 97
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 98
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    .line 99
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    .line 100
    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 101
    iput-object p4, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 102
    iput-object p5, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 103
    iput-object p6, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    .line 104
    iput-object p7, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->detailDelegate:Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;

    .line 105
    iput-object p8, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    .line 106
    iput-object p9, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    .line 107
    iput-object p10, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->flow:Lflow/Flow;

    .line 108
    iput-object p11, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->watchdog:Lcom/squareup/util/RxWatchdog;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;)V
    .locals 0

    .line 75
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->dismissCard()V

    return-void
.end method

.method static synthetic access$202(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    return-object p1
.end method

.method static synthetic access$300(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;)Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->detailDelegate:Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 0

    .line 75
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->checkSlowAudioConnectionTimeout(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    return-void
.end method

.method private buildDefaultConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 250
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 251
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 252
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    return-object v0
.end method

.method private checkSlowAudioConnectionTimeout(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 4

    .line 242
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    if-ne v0, v1, :cond_0

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->watchdog:Lcom/squareup/util/RxWatchdog;

    const-wide/16 v1, 0xf

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    goto :goto_0

    .line 245
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->watchdog:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {p1}, Lcom/squareup/util/RxWatchdog;->cancel()V

    :goto_0
    return-void
.end method

.method private dismissCard()V
    .locals 3

    .line 219
    iget-boolean v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->noReadersRemain:Z

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiReaderSettingsController;->handleReaderSettingsCanceled()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardScreen$Presenter$jG60GoPdAmPg0QUdhopVI078qUM;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardScreen$Presenter$jG60GoPdAmPg0QUdhopVI078qUM;

    invoke-direct {v1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    :goto_0
    return-void
.end method

.method public static synthetic lambda$7txYiLVH8ohJEJGfZPkirSsUPmM(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->dismissCard()V

    return-void
.end method

.method static synthetic lambda$dismissCard$5(Lflow/History;)Lcom/squareup/container/Command;
    .locals 2

    .line 227
    invoke-virtual {p0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p0

    .line 228
    invoke-virtual {p0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    .line 230
    invoke-virtual {p0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    move-result-object v0

    .line 231
    instance-of v0, v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;

    const-string v1, "Parent card screen is not an instance of CardReadersCardScreen"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 234
    invoke-virtual {p0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    sget-object v0, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-static {p0, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public forgetReader()V
    .locals 6

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->READER_SETTINGS_FORGET_READER:Lcom/squareup/analytics/ReaderEventName;

    .line 195
    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->logWirelessEventForReaderState(Lcom/squareup/ui/settings/paymentdevices/ReaderState;Lcom/squareup/analytics/ReaderEventName;)V

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    if-ne v0, v1, :cond_0

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderFactory;->destroyWirelessAutoConnect(Ljava/lang/String;)V

    goto :goto_1

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    .line 202
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    if-nez v1, :cond_1

    .line 204
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v5, v5, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 206
    invoke-virtual {v5}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v5

    iget v5, v5, Lcom/squareup/cardreader/CardReaderId;->id:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const-string v4, "CardReaderDetailSheet#forgetReader() on reader that doesn\'t exist (id=%d)"

    .line 204
    invoke-static {v2, v4, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;)V

    goto :goto_0

    .line 208
    :cond_1
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->forget()V

    .line 210
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/CardReaderFactory;->destroy(Lcom/squareup/cardreader/CardReaderId;)V

    .line 212
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/BluetoothUtils;->unpairDevice(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->getPairingEventListener()Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    .line 215
    invoke-interface {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;->failedPairing(Ljava/lang/String;)V

    return-void
.end method

.method getActionbarText()Ljava/lang/String;
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->nickname:Ljava/lang/String;

    return-object v0
.end method

.method public identifyReader()V
    .locals 5

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->READER_SETTINGS_IDENTIFY_READER:Lcom/squareup/analytics/ReaderEventName;

    .line 180
    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->logWirelessEventForReaderState(Lcom/squareup/ui/settings/paymentdevices/ReaderState;Lcom/squareup/analytics/ReaderEventName;)V

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 183
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v4, v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 187
    invoke-virtual {v4}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v4

    iget v4, v4, Lcom/squareup/cardreader/CardReaderId;->id:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v3, "CardReaderDetailSheet#identifyReader() on reader that doesn\'t exist (id=%d)"

    .line 185
    invoke-static {v1, v3, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/log/ReaderEventLogger;->addToOhSnapLog(Ljava/lang/String;)V

    goto :goto_0

    .line 189
    :cond_0
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->identify()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$0$CardReaderDetailCardScreen$Presenter(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    if-ne v0, p2, :cond_0

    .line 125
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->detailDelegate:Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 126
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    .line 125
    invoke-virtual {p2, p1, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->audioConnectionTakingLongTime(Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;Lcom/squareup/protos/client/bills/CardData$ReaderType;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$2$CardReaderDetailCardScreen$Presenter(Ljava/util/Collection;)V
    .locals 0

    .line 131
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->noReadersRemain:Z

    return-void
.end method

.method public synthetic lambda$onLoad$1$CardReaderDetailCardScreen$Presenter(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->watchdog:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardScreen$Presenter$0MvMyb2Z-FkSrsPPK1RW_e5_15A;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardScreen$Presenter$0MvMyb2Z-FkSrsPPK1RW_e5_15A;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;)V

    .line 123
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$CardReaderDetailCardScreen$Presenter(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;)Lrx/Subscription;
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardScreen$Presenter$sn2hBWeoBHCTk9jliDHlfqrvnrg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardScreen$Presenter$sn2hBWeoBHCTk9jliDHlfqrvnrg;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;)V

    .line 131
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    .line 132
    invoke-static {v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->filterByReaderAddress(Ljava/lang/String;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter$1;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;)V

    .line 133
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$4$CardReaderDetailCardScreen$Presenter(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;)V
    .locals 2

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    .line 163
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->getNickName()Ljava/lang/String;

    move-result-object p1

    .line 162
    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->updateStoredReaderName(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 164
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_SETTINGS_NAME_CHANGED:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/log/ReaderEventLogger;->logWirelessEventForReaderState(Lcom/squareup/ui/settings/paymentdevices/ReaderState;Lcom/squareup/analytics/ReaderEventName;)V

    .line 168
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->dismissCard()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 112
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 113
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen;

    .line 114
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen;->access$000(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 118
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 119
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;

    .line 121
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardScreen$Presenter$IXj6DvgWup9mvIlNJo0UsFRgUpg;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardScreen$Presenter$IXj6DvgWup9mvIlNJo0UsFRgUpg;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 130
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardScreen$Presenter$F09oVGxmIxXvTzKN-O2Vhigf9UU;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardScreen$Presenter$F09oVGxmIxXvTzKN-O2Vhigf9UU;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 155
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->buildDefaultConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->getActionbarText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardScreen$Presenter$7txYiLVH8ohJEJGfZPkirSsUPmM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardScreen$Presenter$7txYiLVH8ohJEJGfZPkirSsUPmM;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;)V

    .line 156
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->detailDelegate:Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->mostRecentReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->canEditReaderNickname(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    .line 159
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardScreen$Presenter$3TGwLqGM2xWjkR_II0jbFaE_9Ww;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardScreen$Presenter$3TGwLqGM2xWjkR_II0jbFaE_9Ww;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;)V

    .line 160
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 171
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
