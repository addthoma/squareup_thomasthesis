.class public final Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "CardReadersScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderPowerMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPowerMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final coreParametersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPowerMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->coreParametersProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->cardReaderPowerMonitorProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPowerMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;"
        }
    .end annotation

    .line 61
    new-instance v8, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lflow/Flow;)Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;
    .locals 9

    .line 68
    new-instance v8, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;-><init>(Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lflow/Flow;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;
    .locals 8

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->coreParametersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->cardReaderPowerMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/CardReaderPowerMonitor;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lflow/Flow;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->newInstance(Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lflow/Flow;)Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen_Presenter_Factory;->get()Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
