.class public Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;
.super Lmortar/ViewPresenter;
.source "PairingPresenter.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BluetoothStatusListener;
.implements Lcom/squareup/cardreader/BlePairingListener;
.implements Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;,
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$PairingWirelessSearcher;,
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$WaitForAdditionalReaders;,
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$SharedScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;",
        ">;",
        "Lcom/squareup/cardreader/ble/BluetoothStatusListener;",
        "Lcom/squareup/cardreader/BlePairingListener;",
        "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;"
    }
.end annotation


# static fields
.field static final WAIT_DELAY_MS:I = 0x3e8


# instance fields
.field private final activityVisibilityPresenter:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

.field private final apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

.field private final beginPairingRunnable:Ljava/lang/Runnable;

.field private final bleEventLogFilter:Lcom/squareup/cardreader/ble/BleEventLogFilter;

.field private bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

.field private final bluetoothStatusReceiver:Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

.field private final clock:Lcom/squareup/util/Clock;

.field private final flow:Lflow/Flow;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private newlyCreatedCardReaderContext:Lcom/squareup/cardreader/CardReaderContext;

.field private final pairingFailurePopup:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation
.end field

.field private pairingStartTime:J

.field private pendingPairingResult:Ljava/lang/Runnable;

.field private final progressPopup:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field private final readerUiEventSink:Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;

.field private screenKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field private selectedWirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

.field private final waitForAdditionalReaders:Z

.field private final wirelessConnections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/WirelessConnection;",
            ">;"
        }
    .end annotation
.end field

.field private final wirelessPairingEventListener:Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

.field private final wirelessSearcher:Lcom/squareup/cardreader/WirelessSearcher;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/WirelessSearcher;Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;Lcom/squareup/cardreader/ble/BleEventLogFilter;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;Lcom/squareup/cardreader/dipper/ReaderUiEventSink;Lcom/squareup/thread/executor/MainThread;ZLcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;Lcom/squareup/api/ApiReaderSettingsController;Lflow/Flow;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 142
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 112
    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$1;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)V

    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->beginPairingRunnable:Ljava/lang/Runnable;

    move-object v1, p1

    .line 143
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    move-object v1, p2

    .line 144
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->wirelessSearcher:Lcom/squareup/cardreader/WirelessSearcher;

    move-object v1, p3

    .line 145
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bluetoothStatusReceiver:Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    move-object v1, p4

    .line 146
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    move-object v1, p5

    .line 147
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    move-object v1, p6

    .line 148
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    move-object v1, p8

    .line 149
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->clock:Lcom/squareup/util/Clock;

    move-object v1, p9

    .line 150
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->res:Lcom/squareup/util/Res;

    move-object v1, p10

    .line 151
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleEventLogFilter:Lcom/squareup/cardreader/ble/BleEventLogFilter;

    .line 152
    invoke-virtual {p7}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->getPairingEventListener()Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->wirelessPairingEventListener:Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

    move-object v1, p11

    .line 153
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->runner:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;

    move-object v1, p12

    .line 154
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->readerUiEventSink:Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

    move-object/from16 v1, p17

    .line 155
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    move-object/from16 v1, p18

    .line 156
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->flow:Lflow/Flow;

    .line 157
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->buildProgressPopup()Lcom/squareup/mortar/PopupPresenter;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->progressPopup:Lcom/squareup/mortar/PopupPresenter;

    .line 158
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->buildFailurePopup()Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->pairingFailurePopup:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    move-object v1, p13

    .line 159
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    move/from16 v1, p14

    .line 160
    iput-boolean v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->waitForAdditionalReaders:Z

    move-object/from16 v1, p15

    .line 161
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-object/from16 v1, p16

    .line 162
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->activityVisibilityPresenter:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    .line 163
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->wirelessConnections:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/cardreader/WirelessSearcher;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->wirelessSearcher:Lcom/squareup/cardreader/WirelessSearcher;

    return-object p0
.end method

.method static synthetic access$102(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;J)J
    .locals 0

    .line 68
    iput-wide p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->pairingStartTime:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/util/Clock;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->clock:Lcom/squareup/util/Clock;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/cardreader/WirelessConnection;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->selectedWirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->wirelessPairingEventListener:Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

    return-object p0
.end method

.method static synthetic access$602(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;Lcom/squareup/cardreader/CardReaderContext;)Lcom/squareup/cardreader/CardReaderContext;
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->newlyCreatedCardReaderContext:Lcom/squareup/cardreader/CardReaderContext;

    return-object p1
.end method

.method static synthetic access$700(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/cardreader/CardReaderFactory;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    return-object p0
.end method

.method private beginPairing(Lcom/squareup/cardreader/WirelessConnection;Z)V
    .locals 3

    .line 394
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getProgressTitle(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/cardreader/WirelessConnection;)Ljava/lang/String;

    move-result-object v0

    .line 395
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->progressPopup:Lcom/squareup/mortar/PopupPresenter;

    new-instance v2, Lcom/squareup/caller/ProgressPopup$Progress;

    invoke-direct {v2, v0}, Lcom/squareup/caller/ProgressPopup$Progress;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 396
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->selectedWirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    if-eqz p2, :cond_0

    .line 398
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->beginPairingRunnable:Ljava/lang/Runnable;

    const-wide/16 v0, 0x3e8

    invoke-interface {p1, p2, v0, v1}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 400
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->beginPairingRunnable:Ljava/lang/Runnable;

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void
.end method

.method private finishCard()V
    .locals 4

    .line 367
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->stopListening()V

    .line 370
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiReaderSettingsController;->isApiReaderSettingsRequest()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->flow:Lflow/Flow;

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen;

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->flow:Lflow/Flow;

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope;

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method private forgetReaderAndMaybeRetry(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;)V
    .locals 3

    .line 343
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->runner:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;->onAutomaticPairingAttemptComplete()V

    .line 344
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->stopSearch()V

    .line 345
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->newlyCreatedCardReaderContext:Lcom/squareup/cardreader/CardReaderContext;

    if-eqz v0, :cond_0

    .line 347
    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderContext;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->forget()V

    .line 348
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v0, v0, Lcom/squareup/cardreader/CardReaderContext;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/CardReaderFactory;->destroy(Lcom/squareup/cardreader/CardReaderId;)V

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->selectedWirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    if-eqz v0, :cond_1

    .line 352
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/cardreader/BluetoothUtils;->unpairDevice(Ljava/lang/String;)V

    .line 353
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->wirelessPairingEventListener:Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;->failedPairing(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 354
    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->selectedWirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    .line 357
    :cond_1
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;->RETRY:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    if-ne p1, v0, :cond_2

    .line 358
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->startSearch()V

    .line 359
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->notifyDataSetChanged()V

    :cond_2
    return-void
.end method

.method private handlePairingFailure(Lcom/squareup/dipper/events/BleErrorType;)V
    .locals 3

    .line 429
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_FAILED:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->logEvent(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/dipper/events/BleErrorType;)V

    .line 431
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$4;->$SwitchMap$com$squareup$dipper$events$BleErrorType:[I

    invoke-virtual {p1}, Lcom/squareup/dipper/events/BleErrorType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 447
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unkown type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 442
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->ble_pairing_failed_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 443
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->selectedWirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    .line 444
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getPairingFailureMessage(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/cardreader/WirelessConnection;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 433
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->ble_pairing_failed_version_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 434
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->ble_pairing_failed_version_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 450
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->progressPopup:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v1}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    .line 451
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->pairingFailurePopup:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v2, Lcom/squareup/widgets/warning/WarningStrings;

    invoke-direct {v2, p1, v0}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 453
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;->RETRY:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->forgetReaderAndMaybeRetry(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private handlePairingSuccessAfterMinimumDelay()V
    .locals 5

    .line 405
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->progressPopup:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    .line 410
    iget-boolean v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->waitForAdditionalReaders:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->wirelessConnections:Ljava/util/List;

    .line 411
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->runner:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;

    .line 412
    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;->shouldManuallySelectReader()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 416
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_SUCCESS_WITH_CONFIRMATION:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->logEvent(Lcom/squareup/analytics/ReaderEventName;)V

    .line 417
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->selectedWirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessConnection;->getName()Ljava/lang/String;

    move-result-object v0

    .line 418
    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    iget-object v3, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->selectedWirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    invoke-virtual {v2, v3, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getConfirmationTitle(Lcom/squareup/cardreader/WirelessConnection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 419
    iget-object v3, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    invoke-virtual {v3, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getConfirmationContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 420
    iget-object v3, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->readerUiEventSink:Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

    invoke-virtual {v3, v1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->silenceReaderHuds(Z)V

    .line 421
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->flow:Lflow/Flow;

    new-instance v3, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;

    iget-object v4, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->screenKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v3, v2, v0, v4}, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v1, v3}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_1

    .line 413
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_NO_CONFIRMATION_NEEDED:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->logEvent(Lcom/squareup/analytics/ReaderEventName;)V

    .line 414
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->finishCard()V

    :goto_1
    return-void
.end method

.method private isSameReader(Lcom/squareup/cardreader/WirelessConnection;)Z
    .locals 1

    .line 388
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->selectedWirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->selectedWirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    .line 389
    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 388
    invoke-static {p1, v0}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public static synthetic lambda$73fgFyEODzYVu_xiYyeGWt94A1Q(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->onReaderFound(Lcom/squareup/cardreader/WirelessConnection;)V

    return-void
.end method

.method public static synthetic lambda$C5emLorhuS4vdwuPaaAa0t69fi0(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->handlePairingSuccessAfterMinimumDelay()V

    return-void
.end method

.method public static synthetic lambda$JDDAql7U8fleRDZWJLEgWaVm6Sc(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->finishCard()V

    return-void
.end method

.method private notifyDataSetChanged()V
    .locals 1

    .line 337
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method private onReaderFound(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 2

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_READER_DISCOVERED:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->logEvent(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)V

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderFactory;->hasCardReaderWithAddress(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->wirelessConnections:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->runner:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;->shouldManuallySelectReader()Z

    move-result v0

    if-nez v0, :cond_1

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_BEGIN_AUTOMATICALLY:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->logEvent(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)V

    .line 220
    iget-boolean v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->waitForAdditionalReaders:Z

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->beginPairing(Lcom/squareup/cardreader/WirelessConnection;Z)V

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->wirelessConnections:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->notifyDataSetChanged()V

    return-void
.end method

.method private pairingResultAfterMinimumDelay(Ljava/lang/Runnable;)V
    .locals 5

    .line 457
    iget-wide v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->pairingStartTime:J

    const-wide/16 v2, 0x3e8

    add-long/2addr v0, v2

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v2}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 459
    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->pendingPairingResult:Ljava/lang/Runnable;

    if-eqz v2, :cond_0

    .line 460
    iget-object v3, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v3, v2}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    const/4 v2, 0x0

    .line 461
    iput-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->pendingPairingResult:Ljava/lang/Runnable;

    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    .line 465
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->pendingPairingResult:Ljava/lang/Runnable;

    .line 466
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->pendingPairingResult:Ljava/lang/Runnable;

    invoke-interface {p1, v2, v0, v1}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 468
    :cond_1
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void
.end method

.method private startSearch()V
    .locals 2

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->wirelessSearcher:Lcom/squareup/cardreader/WirelessSearcher;

    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessSearcher;->isSearching()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 325
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->wirelessSearcher:Lcom/squareup/cardreader/WirelessSearcher;

    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessSearcher;->startSearch()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$PairingPresenter$73fgFyEODzYVu_xiYyeGWt94A1Q;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$PairingPresenter$73fgFyEODzYVu_xiYyeGWt94A1Q;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 326
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_SCANNING:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->logEvent(Lcom/squareup/analytics/ReaderEventName;)V

    return-void
.end method

.method private stopListening()V
    .locals 2

    .line 381
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/CardReaderListeners;->removeBlePairingListener(Lcom/squareup/cardreader/BlePairingListener;)V

    .line 382
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bluetoothStatusReceiver:Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->removeBluetoothStatusListener(Lcom/squareup/cardreader/ble/BluetoothStatusListener;)V

    .line 383
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->pendingPairingResult:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    .line 384
    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->pendingPairingResult:Ljava/lang/Runnable;

    return-void
.end method

.method private stopSearch()V
    .locals 2

    .line 330
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->beginPairingRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->wirelessSearcher:Lcom/squareup/cardreader/WirelessSearcher;

    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessSearcher;->stopSearch()V

    .line 332
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->wirelessConnections:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 333
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->notifyDataSetChanged()V

    return-void
.end method


# virtual methods
.method public activityNoLongerVisible()V
    .locals 0

    .line 244
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->stopSearch()V

    return-void
.end method

.method public activityVisible()V
    .locals 0

    .line 248
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->startSearch()V

    return-void
.end method

.method buildFailurePopup()Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation

    .line 275
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$3;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$3;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)V

    return-object v0
.end method

.method buildProgressPopup()Lcom/squareup/mortar/PopupPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 267
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$2;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)V

    return-object v0
.end method

.method getPairingFailurePopup()Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->pairingFailurePopup:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    return-object v0
.end method

.method getProgressPopup()Lcom/squareup/mortar/PopupPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->progressPopup:Lcom/squareup/mortar/PopupPresenter;

    return-object v0
.end method

.method getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object v0
.end method

.method getWirelessConnections()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/WirelessConnection;",
            ">;"
        }
    .end annotation

    .line 304
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->wirelessConnections:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onPairingFailed$0$PairingPresenter(Lcom/squareup/dipper/events/BleErrorType;)V
    .locals 0

    .line 235
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->handlePairingFailure(Lcom/squareup/dipper/events/BleErrorType;)V

    return-void
.end method

.method public onBluetoothDisabled()V
    .locals 1

    .line 198
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->stopSearch()V

    .line 199
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;->DO_NOT_RETRY:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->forgetReaderAndMaybeRetry(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;)V

    .line 200
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->finishCard()V

    return-void
.end method

.method public onBluetoothEnabled()V
    .locals 0

    .line 194
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->startSearch()V

    return-void
.end method

.method public onConfirmationPopupButtonTap(Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;)V
    .locals 3

    .line 252
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$4;->$SwitchMap$com$squareup$ui$settings$paymentdevices$TutorialPopup$ButtonTap:[I

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 258
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_CONFIRMATION_INCORRECT:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->logEvent(Lcom/squareup/analytics/ReaderEventName;)V

    .line 259
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;->RETRY:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->forgetReaderAndMaybeRetry(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;)V

    goto :goto_0

    .line 262
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 254
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_CONFIRMATION_OK:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->logEvent(Lcom/squareup/analytics/ReaderEventName;)V

    .line 255
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->finishCard()V

    :goto_0
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleEventLogFilter:Lcom/squareup/cardreader/ble/BleEventLogFilter;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleEventLogFilter;->getLogger()Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bluetoothStatusReceiver:Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->addBluetoothStatusListener(Lcom/squareup/cardreader/ble/BluetoothStatusListener;)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/CardReaderListeners;->addBlePairingListener(Lcom/squareup/cardreader/BlePairingListener;)V

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->activityVisibilityPresenter:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    invoke-virtual {v0, p1, p0}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->register(Lmortar/MortarScope;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;)V

    .line 171
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->screenKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method protected onExitScope()V
    .locals 2

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleEventLogFilter:Lcom/squareup/cardreader/ble/BleEventLogFilter;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ble/BleEventLogFilter;->releaseLogger(Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;)V

    .line 176
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->stopListening()V

    .line 177
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->stopSearch()V

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->readerUiEventSink:Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->silenceReaderHuds(Z)V

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->progressPopup:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    return-void
.end method

.method onFailurePopupResult()V
    .locals 0

    .line 284
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->startSearch()V

    return-void
.end method

.method onHelpClicked()V
    .locals 2

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiReaderSettingsController;->isApiReaderSettingsRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 183
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 184
    new-instance p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    .line 185
    invoke-virtual {v0}, Lcom/squareup/api/ApiReaderSettingsController;->isApiReaderSettingsRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->pairing_screen_title:I

    .line 187
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 184
    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$PairingPresenter$JDDAql7U8fleRDZWJLEgWaVm6Sc;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$PairingPresenter$JDDAql7U8fleRDZWJLEgWaVm6Sc;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)V

    .line 188
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 189
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 190
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method public onPairingFailed(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleErrorType;)V
    .locals 0

    .line 234
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->isSameReader(Lcom/squareup/cardreader/WirelessConnection;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 235
    new-instance p1, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$PairingPresenter$9QaaFFVpjKLyFF0TCZk57tATaGw;

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$PairingPresenter$9QaaFFVpjKLyFF0TCZk57tATaGw;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;Lcom/squareup/dipper/events/BleErrorType;)V

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->pairingResultAfterMinimumDelay(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onPairingSuccess(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 0

    .line 227
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->isSameReader(Lcom/squareup/cardreader/WirelessConnection;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 228
    new-instance p1, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$PairingPresenter$C5emLorhuS4vdwuPaaAa0t69fi0;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$PairingPresenter$C5emLorhuS4vdwuPaaAa0t69fi0;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)V

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->pairingResultAfterMinimumDelay(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onReaderForceUnPair(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 2

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_FORGETTING_DURING_AUTO_CONNECT:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->logEvent(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)V

    return-void
.end method

.method onWirelessConnectionSelected(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 2

    .line 312
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_SELECT_READER:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->logEvent(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)V

    const/4 v0, 0x0

    .line 313
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->beginPairing(Lcom/squareup/cardreader/WirelessConnection;Z)V

    return-void
.end method

.method shouldManuallySelectReader()Z
    .locals 1

    .line 308
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->runner:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;->shouldManuallySelectReader()Z

    move-result v0

    return v0
.end method
