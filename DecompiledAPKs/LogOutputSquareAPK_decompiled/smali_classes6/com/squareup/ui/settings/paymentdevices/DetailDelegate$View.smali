.class interface abstract Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;
.super Ljava/lang/Object;
.source "DetailDelegate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "View"
.end annotation


# virtual methods
.method public abstract blankBatteryLevel(Ljava/lang/String;)V
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract getResources()Landroid/content/res/Resources;
.end method

.method public abstract hideStatusSection()V
.end method

.method public abstract setAcceptsValue(Ljava/lang/CharSequence;)V
.end method

.method public abstract setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;)V
.end method

.method public abstract setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;Ljava/lang/CharSequence;)V
.end method

.method public abstract setConnectionValue(Ljava/lang/CharSequence;)V
.end method

.method public abstract setFirmwareVersion(Ljava/lang/String;)V
.end method

.method public abstract setHelpMessage(Ljava/lang/CharSequence;)V
.end method

.method public abstract setIdentifyButtonVisibility(Z)V
.end method

.method public abstract setName(Ljava/lang/String;)V
.end method

.method public abstract setNickNameEnabled(Z)V
.end method

.method public abstract setNickNameSectionVisible(Z)V
.end method

.method public abstract setSerialNumber(Ljava/lang/String;)V
.end method

.method public abstract setStatusValue(Ljava/lang/CharSequence;)V
.end method

.method public abstract showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
.end method

.method public abstract showStatusSectionAdvice(Ljava/lang/CharSequence;)V
.end method

.method public abstract showWirelessActions()V
.end method
