.class Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;
.super Ljava/lang/Object;
.source "CardReaderOracle.java"

# interfaces
.implements Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalWirelessPairingEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V
    .locals 0

    .line 430
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$1;)V
    .locals 0

    .line 430
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V

    return-void
.end method

.method private removeReader(Ljava/lang/String;)V
    .locals 2

    .line 482
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$800(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->removeStoredReader(Ljava/lang/String;)V

    .line 484
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 485
    invoke-static {v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$300(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 486
    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 487
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$300(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public failedPairing(Ljava/lang/String;)V
    .locals 0

    .line 478
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;->removeReader(Ljava/lang/String;)V

    return-void
.end method

.method public startedPairing(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 10

    .line 438
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$800(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->addOrUpdateStoredWirelessReader(Lcom/squareup/cardreader/WirelessConnection;)V

    .line 442
    new-instance p1, Ljava/util/LinkedHashMap;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 443
    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$300(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {p1, v0}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 444
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 445
    invoke-interface {p2}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v4

    sget-object v6, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_STILL_BLE_PAIRING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    invoke-static {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$900(Ljava/util/Map;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    .line 447
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {p2}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$300(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public succeededPairing(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 461
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$800(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->addOrUpdateStoredReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 463
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 464
    invoke-static {v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$300(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 465
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 466
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$300(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
