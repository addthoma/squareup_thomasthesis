.class Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "TutorialPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;-><init>(Landroid/content/Context;Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;

.field final synthetic val$presenter:Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;)V
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;

    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog$1;->val$presenter:Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 108
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog$1;->val$presenter:Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;

    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;->PRIMARY:Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;->onButtonTap(Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;)V

    return-void
.end method
